#version 330 core
layout (location = 0) in vec3 aPos;

out vec4 myColor;
out vec2 TexCoord;
  
uniform vec4 color;

void main()
{
    gl_Position = vec4(2.0f * aPos.x, 2.0f * aPos.y, aPos.z, 1.0f);
	myColor = color;
	vec2 texCoord = vec2(aPos.x + 0.5f, aPos.y + 0.5f);
    TexCoord = vec2(texCoord.x, texCoord.y);
} 