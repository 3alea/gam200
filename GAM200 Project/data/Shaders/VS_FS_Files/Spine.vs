#version 440 core
layout (location = 0) in vec2 aPos;
layout (location = 1) in vec2 aTex;
layout (location = 2) in vec4 aCol;

out vec2 TexCoord;
out vec4 vtxColor;
  
layout(location = 0) uniform mat4 viewProj;
layout(location = 1) uniform mat4 model;

void main()
{
    gl_Position = viewProj * model * vec4(aPos,0.0f, 1.0f);
     vtxColor = aCol;
    TexCoord = aTex;
} 