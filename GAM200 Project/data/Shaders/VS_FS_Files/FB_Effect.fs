#version 330 core
out vec4 FragColor;

in vec4 myColor;
in vec2 TexCoord;

uniform sampler2D ourTexture;
uniform float time;

void main(){
    FragColor = vec4(texture( ourTexture, TexCoord + 0.005*vec2( sin(time+1024.0*TexCoord.x),cos(time+768.0*TexCoord.y)) ).xyz, 1.0f);
}