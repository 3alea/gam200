#version 330 core
out vec4 FragColor;
  
in vec4 myColor;
in vec2 TexCoord;

uniform sampler2D ourTexture;

void main()
{
    FragColor = vec4(vec3(1.0 - texture(ourTexture, TexCoord)), 1.0);
}  