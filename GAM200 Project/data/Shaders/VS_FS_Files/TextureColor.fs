#version 330 core
out vec4 FragColor;
  
in vec4 ourColor;
in vec2 TexCoord;

uniform sampler2D ourTexture;

void main()
{
	vec4 color = texture(ourTexture, TexCoord) * ourColor;
	
	if ( color.a < 0.01 )
		discard;

    FragColor = color;
}