#version 330 core

out vec4 color;
in vec2 TexCoord;
in vec4 vtxColor;

uniform vec4 modulationColor;
uniform sampler2D ourTexture;

void main()
{
    color = texture(ourTexture, TexCoord) * modulationColor * vtxColor;

    if ( color.a < 0.01 )
        discard;
}