#version 440 core
layout (location = 0) in vec3 aPos;

out vec4 ourColor;
out vec2 TexCoord;
  
layout(location = 0) uniform mat4 viewProj;
layout(location = 1) uniform mat4 model;

uniform vec2 aspectRatio;
uniform vec2 texSize;
uniform vec4 color;
uniform mat3 texTransform;
uniform vec2 texOffset;

void main()
{
    gl_Position = viewProj * model * vec4(aPos.x * aspectRatio.x, aPos.y * aspectRatio.y, aPos.z, 1.0f);
	ourColor = color;
	vec2 texCoord = vec2(aPos.x + 0.5f, aPos.y + 0.5f);
	vec3 temp =  texTransform * vec3(texCoord, 1.0f);
    TexCoord = vec2(texSize.x * temp.x + texOffset.x, texSize.y * temp.y + texOffset.y);
} 