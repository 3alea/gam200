#version 440 core
layout (location = 0) in vec4 vertex; // <vec2 pos, vec2 tex>
out vec2 TexCoords;

layout(location = 0) uniform mat4 viewProj;
layout(location = 1) uniform mat4 model;

void main()
{
    gl_Position = viewProj * model * vec4(vertex.xy, 0.0, 1.0);
    TexCoords = vertex.zw;
}  