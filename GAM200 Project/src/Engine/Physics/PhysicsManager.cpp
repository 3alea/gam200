#include "PhysicsManager.h"
#include "Components\Rigidbody.h"
#include <iostream>

namespace AEX
{
	void PhysicsManager::Update()
	{
		auto it = mAllRigidbodyComps.begin();
		auto itEnd = mAllRigidbodyComps.end();

		// Go through the all the rigidbodys
		for (; it != itEnd; it++)
		{
			// Update the current rigidbody
			(*it)->Update();
		}
	}

	Rigidbody* PhysicsManager::AddComponent(Rigidbody* comp)
	{
		mAllRigidbodyComps.push_back(comp);
		return comp;
	}

	void PhysicsManager::RemoveComponent(Rigidbody* comp)
	{
		// Go through the rigid body vector
		for (auto it = mAllRigidbodyComps.begin(); it != mAllRigidbodyComps.end(); ++it)
		{
			// Check if the current element is the one to remove
			if (*it == comp)
			{
				// Take the pointer out of the vector
				mAllRigidbodyComps.erase(it);

				// Done
				return;
			}
		}
	}
}