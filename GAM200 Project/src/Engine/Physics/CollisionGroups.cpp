#include "CollisionGroups.h"
#include <iostream>
#include "src\Engine\Imgui\imgui.h"

namespace AEX
{
	bool CollisionTable::Initialize()
	{
		json t;
		LoadFromFile(t, "data/CollisionTable/table.json");
		FromJson(t);

		t = true;
		f = false;

		return true;
	}

	void CollisionTable::AddCollisionGroup(const char * name)
	{
		// Check if the collision group is already added
		if (std::find(collisionGroups.begin(), collisionGroups.end(), name) != collisionGroups.end())
			return;

		// Get the size of the vector
		int size = collisionGroups.size();

		// Add the collision group
		collisionGroups.push_back(std::string(name));

		// Make a relation between the same C.G
		table[std::pair<std::string, std::string>(name, name)] = DetectAndResolve;

		// Make relations with all other collision groups
		for (int i = 0; i < size; ++i)
		{
			// Make a relation with the current C.G
			// Default policy is detect and resolve
			table[std::pair<std::string, std::string>(collisionGroups[i], name)] = DetectAndResolve;
		}

		serializeTable();
	}

	void CollisionTable::serializeTable()
	{
		json value;
		ToJson(value);
		SaveToFile(value, "data/CollisionTable/table.json");
	}

	CollisionTable::CollisionPolicy CollisionTable::getPolicy(std::string & s1, std::string & s2)
	{
		auto end = table.end();

		for (auto it = table.begin(); it != end; ++it)
		{
			if ((it->first.first == s1 && it->first.second == s2) || (it->first.second == s1 && it->first.first == s2))
				return it->second;
		}

		// Should never get here
		//for (int i = 0; i < 100000; ++i)
		//	std::cout << "--------------------" << std::endl;

		return DetectAndResolve;
	}

	/////////////////////////////DEBUG////////////////////////////
	void CollisionTable::printCollisionGroups()
	{
		/*std::cout << "----------------------------" << std::endl;
		std::cout << "Collision groups : "          << std::endl;
		for (unsigned i = 0; i < collisionGroups.size(); ++i)
		{
			std::cout << i << " " << collisionGroups[i] << std::endl;
		}
		std::cout << "----------------------------" << std::endl;

		printPolicies();*/
	}

	void CollisionTable::printPolicies()
	{
		/*std::cout << "----------------------" << std::endl;
		std::cout << "Collision Policies:" << std::endl;
		int i = 1;
		for (auto it = table.begin(); it != table.end(); ++it)
		{
			std::cout << "----------------------" << std::endl;
			std::cout << i++ << " " << it->first.first << " and " << it->first.second << " should ";
			if (it->second == Detect) std::cout << "detect" << std::endl;
			else if (it->second == DetectAndResolve) std::cout << "detect and resolve" << std::endl;
			else std::cout << "ignore" << std::endl;
			std::cout << "----------------------" << std::endl;


		}*/
	}

	void CollisionTable::printChange(std::unordered_map<std::pair<std::string, std::string>, CollisionPolicy, pair_hash>::iterator it)
	{
		/*std::cout << "---------------------------------" << std::endl;
		std::cout << "Policy change: " << std::endl;
		std::cout << it->first.first << " and " << it->first.second << " should " << it->second << std::endl;
		std::cout << "---------------------------------" << std::endl;*/
	}
	/////////////////////////////DEBUG////////////////////////////

	/////////////////////////////GUI//////////////////////////////

	void CollisionTable::printTableIndex(int size)
	{
		f = false;

		// Identation
		ImGui::Selectable("", &f, 0, ImVec2(50, 20));
		ImGui::SameLine();

		t = true;

		// Top row. List all the collision groups
		for (int i = 0; i < size; ++i)
		{
			ImGui::Selectable(collisionGroups[i].c_str(), &t, 0, ImVec2(50, 20));
			ImGui::SameLine();
		}
		ImGui::NewLine();
	}

	void CollisionTable::printCollisionTable()
	{
		int id = 0;

		if (ImGui::CollapsingHeader("Collision Table"))
		{
			// Get the number of collision groups
			int size = collisionGroups.size();

			printTableIndex(size);

			t = true;

			for (int i = 0; i < size; ++i)
			{
				// current row
				std::string row = collisionGroups[i];

				// Print the name
				ImGui::Selectable(row.c_str(), &t, 0, ImVec2(50, 20));
				ImGui::SameLine();

				for (int j = 0; j < size; ++j)
				{
					// Write a blank box
					if (j < i)
					{
						ImGui::Selectable("", &t, 0, ImVec2(50, 20));
						ImGui::SameLine();
						continue;
					}

					// current column
					std::string column = collisionGroups[j];

					// Find the current pair in the map
					for (auto it = table.begin(); it != table.end(); ++it)
			    	{
			    		// Check if the current CG is in the current pair
			    		if ((it->first.first == row && it->first.second == column) || (it->first.first == column && it->first.second == row))
			    		{
							// Pair found
							ImGui::PushID(id++);
							printBox(it);
							ImGui::PopID();
							break;
			    		}
			    	}
				}
				ImGui::NewLine();
			}
		}
	}
	/////////////////////////////GUI//////////////////////////////

	void CollisionTable::printBox(std::unordered_map<std::pair<std::string, std::string>, CollisionPolicy, pair_hash>::iterator it)
	{
		f = false;

		if (it->second == CollisionPolicy::Detect)
		{
			if (ImGui::Selectable("Detect", &f, 0, ImVec2(50, 20)))
			{
				it->second = CollisionPolicy::DetectAndResolve;
				printChange(it);
				serializeTable();
			}
			ImGui::SameLine();
			return;
		}

		if (it->second == CollisionPolicy::DetectAndResolve)
		{
			if (ImGui::Selectable("D.R", &f, 0, ImVec2(50, 20)))
			{
				it->second = CollisionPolicy::Ignore;
				printChange(it);
				serializeTable();
			}
			ImGui::SameLine();
			return;
		}

		if (it->second == CollisionPolicy::Ignore)
		{
			if (ImGui::Selectable("Ignore", &f, 0, ImVec2(50, 20)))
			{
				it->second = CollisionPolicy::Detect;
				printChange(it);
				serializeTable();
			}
			ImGui::SameLine();
			return;
		}
	}
	std::vector<std::string>& CollisionTable::getCollisionGroups()
	{
		return collisionGroups;
	}

	void CollisionTable::FromJson(json & value)
	{
		int size = collisionGroups.size();
		
		json colGroups = value["collision groups"];
		
		FOR_EACH(it, colGroups)
		{
			collisionGroups.push_back(*it);
		}
		
		json t = value["table"];
		
		FOR_EACH(it, t)
		{
			int p;
			(*it)["policy"] >> p;
			table[std::pair<std::string, std::string>((*it)["first"].get<std::string>(), (*it)["second"].get<std::string>())] = (CollisionPolicy)p;
		}

	}

	void CollisionTable::ToJson(json & value)
	{
		int size = collisionGroups.size();

		for (int i = 0; i < size; ++i)
		{
			value["collision groups"].push_back(collisionGroups[i]);
		}

		auto end = table.end();

		for (auto it = table.begin(); it != end; ++it)
		{
			json element;

			element["first"]  << it->first.first;
			element["second"] << it->first.second;
			
			int p = it->second;
			element["policy"] << p;

			value["table"].push_back(element);
		}
	}
}