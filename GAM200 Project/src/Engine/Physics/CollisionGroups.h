#pragma once
#include <string>
#include <unordered_map>
#include <vector>
#include <Core/AEXCore.h>

namespace AEX
{
	struct pair_hash
	{
		template <class T1, class T2>
		std::size_t operator() (const std::pair<T1, T2> &pair) const
		{
			return std::hash<T1>()(pair.first) ^ std::hash<T2>()(pair.second);
		}
	};

	// Collision Group manager
	class CollisionTable : public ISystem, public ISerializable
	{
		AEX_RTTI_DECL(CollisionTable, ISystem);
		AEX_SINGLETON(CollisionTable);

		public:
			bool Initialize();

			enum CollisionPolicy { Ignore, Detect, DetectAndResolve};

			void AddCollisionGroup(const char* name);

			void serializeTable();

			CollisionPolicy getPolicy( std::string& s1, std::string& s2);

			void printCollisionGroups();

			void printPolicies();

			void printChange(std::unordered_map<std::pair<std::string, std::string>, CollisionPolicy, pair_hash>::iterator it);
			
			void printTableIndex(int size);

			void printCollisionTable();

			void printBox(std::unordered_map<std::pair<std::string, std::string>, CollisionPolicy, pair_hash>::iterator it);

			std::vector<std::string>& getCollisionGroups();

			void FromJson(json & value);
			void ToJson(json & value);

		private:
			// Store the policy between collision groups
			std::unordered_map<std::pair<std::string, std::string>, CollisionPolicy, pair_hash> table;
			
			// Store all collision groups
			std::vector<std::string> collisionGroups;

			// Dummy values for gui selectables
			bool t, f;
	};

	#define collision_table CollisionTable::Instance()
}
