#pragma once

#include <vector>
#include "src\Engine\Core\AEXSystem.h"

namespace AEX
{
	class Rigidbody;

	class PhysicsManager : public ISystem
	{
		AEX_RTTI_DECL(PhysicsManager, ISystem);
		AEX_SINGLETON(PhysicsManager);

		public:
			void Update();

			// More management methods need to be implemented
			// Talk to Miguel. Not sure what management methods are left.
			Rigidbody* AddComponent(Rigidbody* comp);
			void RemoveComponent(Rigidbody* comp);

			friend class Rigidbody;

		private:
			std::vector<Rigidbody*> mAllRigidbodyComps;
	};

	#define aexPhysicsMgr (PhysicsManager::Instance())
}