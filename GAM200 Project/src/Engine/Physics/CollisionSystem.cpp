// ----------------------------------------------------------------------------
// Copyright (C)DigiPen Institute of Technology.
// Reproduction or disclosure of this file or its contents without the prior
// written consent of DigiPen Institute of Technology is prohibited.
//
// File Name: CollisionSystem.cpp
// Purpose: Contains functions that resolve the penetration and velocity of 
//          two objects after a collision. Also, it contains a function that
//          checks the collisions of all objects.
// Project: CS230_diego.sanz_3
// Author: Diego Sanz / diego.sanz / 540001618
// ----------------------------------------------------------------------------
#include "src/Engine/Math/Collisions.h"
#include "src\Engine\Math\ContactCollisions.h"
#include "src\Engine\Components\Collider.h"
#include "CollisionSystem.h"
#include "src/Engine//Composition/AEXGameObject.h"
#include "src/Engine/Components/Logic.h"
#include "Physics/CollisionGroups.h"
#include <iostream>

// ----------------------------------------------------------------------------
#pragma region // @PROVIDED

namespace AEX
{

	// Wrapper functions
	bool CollideCircles(ICollider* body1, ICollider* body2, Contact * c);
	bool CollideAABBs(ICollider* body1, ICollider* body2, Contact * c);
	bool CollideOBBs(ICollider* body1, ICollider* body2, Contact * c);
	bool CollideAABBToCircle(ICollider* box, ICollider* circle, Contact * c);
	bool CollideOBBToCircle(ICollider* box, ICollider* circle, Contact * c);
	bool CollidePolygons(ICollider* body1, ICollider* body2, Contact * c);

	bool CollideBoxes(ICollider* body1, ICollider* body2, Contact * c)
	{
		if (body1->mOwner->GetBaseNameString() == "floor")
		{
			//std::cout << "Mypadre" << std::endl;
		}
		if (body2->mOwner->GetBaseNameString() == "floor")
		{
			//std::cout << "Mymadre" << std::endl;
		}
		// Both pointers point to a box collider
		BoxCollider* c1 = static_cast<BoxCollider*>(body1);
		BoxCollider* c2 = static_cast<BoxCollider*>(body2);

		// Check if both colliders are aabbs
		if (c1->mIsAABB & c2->mIsAABB)
			return CollideAABBs(c1, c2, c);

		// Otherwise, calls to collide obbs
		return CollideOBBs(body1, body2, c);
	}

	bool CollideBoxToCircle(ICollider* body1, ICollider* body2, Contact * c)
	{
		ICollider* box = dynamic_cast<BoxCollider*>(body1) ? body1 : body2;
		ICollider* circle = box == body1 ? body2 : body1;

		// Check if the box is an aabb
		if (static_cast<BoxCollider*>(box)->mIsAABB)
			return CollideAABBToCircle(box, circle, c);

		// Otherwise, it is an oobb
		return CollideOBBToCircle(box, circle, c);
	}


	AEVec2 ConvertToVec2(const AEVec3 * pos)
	{
		return AEVec2(pos->x, pos->y);
	}


	// @PROVIDED
	//!----------------------------------------------------------------------------
	// \fn		Init
	// \brief	Called at beginning of game. Sets default and adds the collision 
	//			test to the function pointer map. 
	// ----------------------------------------------------------------------------
	bool CollisionSystem::Initialize()
	{
		// default
		mCollisionIterations = 2;

		mAllFunctions[CircleCollider::TYPE().GetName()][CircleCollider::TYPE().GetName()] = &CollideCircles;
		mAllFunctions[CircleCollider::TYPE().GetName()][BoxCollider::TYPE().GetName()] = &CollideBoxToCircle;
		mAllFunctions[CircleCollider::TYPE().GetName()][BoxCollider::TYPE().GetName()] = &CollideBoxToCircle;
		mAllFunctions[BoxCollider::TYPE().GetName()][CircleCollider::TYPE().GetName()] = &CollideBoxToCircle;
		mAllFunctions[BoxCollider::TYPE().GetName()][BoxCollider::TYPE().GetName()] = &CollideBoxes;
		mAllFunctions[Polygon2D::TYPE().GetName()][Polygon2D::TYPE().GetName()] = &CollidePolygons;

		return true;
	}

	// @PROVIDED
	//!----------------------------------------------------------------------------
	// \fn		Update
	// \brief	Frame update. For now, only call ClearBodies.
	// ----------------------------------------------------------------------------
	void CollisionSystem::Update()
	{
		FOR_EACH(it, mAllColliders)
		{
			(*it)->Update();
		}
		CollideAllBodies();
		ManageEvents();
	}

	// @PROVIDED
	//!----------------------------------------------------------------------------
	// \fn		Shutdown
	// \brief	Shutdown operation at the end of a game. 
	//			For now, only call ClearBodies.
	// ----------------------------------------------------------------------------
	void CollisionSystem::Shutdown()
	{
		ClearBodies();
		currentContacts.clear();
		prevContacts.clear();
	}

	// @PROVIDED
	//!----------------------------------------------------------------------------
	// \fn		AddRigidBody
	// \brief	Adds the rigidbody to the appropriate container (based on is_dynamic). 
	// ----------------------------------------------------------------------------
	void CollisionSystem::AddCollider(ICollider* obj, Rigidbody::DynamicState state)
	{
		if (state == Rigidbody::DynamicState::DYNAMIC)
			mDynamicColliders.push_back(obj);
		else if (state == Rigidbody::DynamicState::STATIC)
			mStaticColliders.push_back(obj);
	}
	// @PROVIDED
	//!----------------------------------------------------------------------------
	// \fn		RemoveRigidBody
	// \brief	Removes the rigidbody from the containers (brute force). 
	// ----------------------------------------------------------------------------
	void CollisionSystem::RemoveCollider(ICollider* obj)
	{
		mDynamicColliders.remove(obj);
		mStaticColliders.remove(obj);
	}

	void AEX::CollisionSystem::RemoveFromPrevContacts(ICollider * c)
	{
		auto end = prevContacts.end();
		for (auto it = prevContacts.begin(); it != end;)
		{
			if (it->mBody1 == c || it->mBody2 == c)
			{
				it = prevContacts.erase(it);
				end = prevContacts.end();
			}
			else
				++it;
		}
	}

	// @PROVIDED
	//!----------------------------------------------------------------------------
	// \fn		ClearBodies
	// \brief	Clears the static and dynamic body containers.
	// ----------------------------------------------------------------------------
	void CollisionSystem::ClearBodies()
	{
		mDynamicColliders.clear();
		mStaticColliders.clear();
	}

	// @PROVIDED
	//!----------------------------------------------------------------------------
	// \fn		GetCollisionFn
	// \brief	returns the appropriate collision function based on the shape of the 
	//			passed rigid bodies, b1 and b2.
	// ----------------------------------------------------------------------------
	CollisionFn CollisionSystem::GetCollisionFn(ICollider * b1, ICollider * b2)
	{
		auto it = mAllFunctions.find(b1->GetType().GetName());

		if (it != mAllFunctions.end())
		{
			auto it2 = it->second.find(b2->GetType().GetName());

			if (it2 != it->second.end())
			{
				return it2->second;
			}
		}

		return nullptr;
	}

	bool CollideCircles(ICollider* body1, ICollider* body2, Contact * c)
	{
		AEVec2 pos1(body1->mOwnerPos->x + body1->mOffset.x, body1->mOwnerPos->y + body1->mOffset.y);
		AEVec2 pos2(body2->mOwnerPos->x + body2->mOffset.x, body2->mOwnerPos->y + body2->mOffset.y);
		return StaticCircleToStaticCircleEx(&pos1, body1->mRealScale.x, &pos2, body2->mRealScale.x, c);
	}
	bool CollideAABBs(ICollider* body1, ICollider* body2, Contact * c)
	{
		AEVec2 pos1(body1->mOwnerPos->x + body1->mOffset.x, body1->mOwnerPos->y + body1->mOffset.y);
		AEVec2 pos2(body2->mOwnerPos->x + body2->mOffset.x, body2->mOwnerPos->y + body2->mOffset.y);

		return StaticRectToStaticRectEx(&pos1, &body1->mRealScale, &pos2, &body2->mRealScale, c);
	}
	bool CollideOBBs(ICollider* body1, ICollider* body2, Contact * c)
	{
		AEVec2 pos1(body1->mOwnerPos->x + body1->mOffset.x, body1->mOwnerPos->y + body1->mOffset.y);
		AEVec2 pos2(body2->mOwnerPos->x + body2->mOffset.x, body2->mOwnerPos->y + body2->mOffset.y);

		Transform obb1(pos1, body1->mRealScale, body1->mOrientation);
		Transform obb2(pos2, body2->mRealScale, body2->mOrientation);
		return OrientedRectToOrientedRectEx(&obb1, &obb2, c);
	}
	bool CollideAABBToCircle(ICollider* box, ICollider* circle, Contact * c)
	{
		AEVec2 pos1(box->mOwnerPos->x + box->mOffset.x, box->mOwnerPos->y + box->mOffset.y);
		AEVec2 pos2(circle->mOwnerPos->x + circle->mOffset.x, circle->mOwnerPos->y + circle->mOffset.y);

		if (StaticRectToStaticCircleEx(&pos1, box->mRealScale.x, box->mRealScale.y, &pos2, circle->mRealScale.x, c))
		{
			//if (circle == box) // flip normal to match our convention
			c->mNormal = -c->mNormal;
			return true;
		}
		return false;
	}
	bool CollideOBBToCircle(ICollider* box, ICollider* circle, Contact * c)
	{
		AEVec2 pos1(box->mOwnerPos->x + box->mOffset.x, box->mOwnerPos->y + box->mOffset.y);
		AEVec2 pos2(circle->mOwnerPos->x + circle->mOffset.x, circle->mOwnerPos->y + circle->mOffset.y);

		Transform obbTr = Transform(pos1, box->mRealScale, box->mOrientation);

		if (StaticOBBToStaticCircleEx(&obbTr, &pos2, circle->mRealScale.x, c))
		{
			//if (circle == body1) // flip normal to match our convention
			c->mNormal = -c->mNormal;
			return true;
		}
		return false;
	}

	bool CollidePolygons(ICollider* body1, ICollider* body2, Contact * c)
	{
		Polygon2D * poly1 = static_cast<Polygon2D*>(body1);
		Polygon2D * poly2 = static_cast<Polygon2D*>(body2);

		AEVec2 pos1(body1->mOwnerPos->x + body1->mOffset.x, body1->mOwnerPos->y + body1->mOffset.y);
		AEVec2 pos2(body2->mOwnerPos->x + body2->mOffset.x, body2->mOwnerPos->y + body2->mOffset.y);

		Transform polyTr1 = Transform(pos1, body1->mRealScale, body1->mOrientation);
		Transform polyTr2 = Transform(pos2, body2->mRealScale, body2->mOrientation);

		if (PolygonToPolygon(poly1, &polyTr1, poly2, &polyTr2, c))
		{
			//if (circle == body1) // flip normal to match our convention
			//	c->mNormal = -c->mNormal;
			return true;
		}
		return false;
	}


#pragma endregion
	// ----------------------------------------------------------------------------

	// ----------------------------------------------------------------------------
#pragma region // @TODO
	// @TODO
	//!----------------------------------------------------------------------------
	// \fn		ResolveContactPenetration
	// \brief	Given the contact data, resolves the penetration of the two given bodies.
	//	\details
	//			- Contact viewer is always body1. 
	//			- After this function returns, the bodies position should be modified, such
	//			  that they are at the minimum distance where they are no longer overlapping.
	//			- The amount of penetration corrected for each body must depend on 
	//			  their relative masses. 
	//			- The bodies' positions are corrected along the contact normal.
	// ----------------------------------------------------------------------------
	void CollisionSystem::ResolveContactPenetration(Rigidbody * body1, Rigidbody * body2, Contact * contact)
	{
		AEVec2 pos1 = AEVec2(0, 0);
		AEVec2 pos2 = AEVec2(0, 0);

		if (body1 != NULL)
			pos1 = ConvertToVec2(body1->mPosition);
		if (body2 != NULL)
			pos2 = ConvertToVec2(body2->mPosition);

		f32 invMass1 = 0.0f;
		f32 invMass2 = 0.0f;

		if (body1 != NULL)
			invMass1 = body1->mInvMass;
		else
			invMass1 = 0.0f;

		if (body2 != NULL)
			invMass2 = body2->mInvMass;
		else
			invMass2 = 0.0f;

		// Calculate the total inverse mass
		float totalInvMass = invMass1 + invMass2;

		// Calculate the mass influence of each body
		float massInfluence_1 = invMass1 / totalInvMass;
		float massInfluence_2 = invMass2 / totalInvMass;

		// Resolve the penetration
		pos1 = pos1 - contact->mNormal * contact->mPenetration * massInfluence_1;
		pos2 = pos2 + contact->mNormal * contact->mPenetration * massInfluence_2;

		if (body1 != NULL)
		{
			body1->mPosition->x = pos1.x;
			body1->mPosition->y = pos1.y;

		}
		if (body2 != NULL)
		{
			body2->mPosition->x = pos2.x;
			body2->mPosition->y = pos2.y;
		}
	}
	// @TODO
	//!----------------------------------------------------------------------------
	// \fn		ResolveContactVelocity
	// \brief	Given the contact data, resolves the velocity of the two given bodies.
	//	\details
	//			- After this function returns, the bodies velocities should be modified
	//			  such that the bodies are separating. YOU MUST IMPLEMENT THE ALGORITHM DESCRIBED IN CLASS. 
	//			- the algorithm depends on a user-defined restitution. Here you should use
	//			  the maccro DFLT_RESTITUTION (default value is 0.908f).
	//			- Contact viewer is always body1. 
	//			- The amount of velocity corrected for each body must depend on 
	//			  their relative masses. 
	//			- The velocities are corrected along the contact normal.
	// ----------------------------------------------------------------------------
	void CollisionSystem::ResolveContactVelocity(Rigidbody * body1, Rigidbody * body2, Contact * contact)
	{
		// Calculate the relative velocity
		AEVec2 relativeVel;

		AEVec2 vel1 = AEVec2(0, 0);
		AEVec2 vel2 = AEVec2(0, 0);

		if (body1 != NULL)
			vel1 = body1->mVelocity;

		if (body2 != NULL)
			vel2 = body2->mVelocity;

		relativeVel = vel2 - vel1;

		// Calculate the separating velocity
		float sepBefore = relativeVel * contact->mNormal;

		// Calculate the separating velocity after collision
		float sepAfter = -sepBefore * DFLT_RESTITUTION;

		// Calculate the variation of separating velocity
		float sepVariation = sepAfter - sepBefore;

		// Calculate the total inverse mass
		f32 invMass1 = 0.0f;
		f32 invMass2 = 0.0f;

		if (body1 != NULL)
			invMass1 = body1->mInvMass;

		if (body2 != NULL)
			invMass2 = body2->mInvMass;

		float totalInvMass = invMass1 + invMass2;

		// Calculate the mass influence of each body
		float massInfluence_1 = invMass1 / totalInvMass;
		float massInfluence_2 = invMass2 / totalInvMass;

		// Aply impusle to velocities
		if (body1 != NULL)
		{
			if (body1->mBounce)
				body1->mVelocity = body1->mVelocity - contact->mNormal * sepVariation * massInfluence_1 * massInfluence_1;
			else
				body1->mVelocity.y = 0;
		}

		if (body2 != NULL)
		{
			if (body2->mBounce)
				body2->mVelocity = body2->mVelocity + contact->mNormal * sepVariation * massInfluence_2 * massInfluence_2;
			else
				body2->mVelocity.y = 0;
		}
	}

	void AEX::CollisionSystem::PrintColliders()
	{
		/*std::cout << "-------------------" << std::endl;
		for (unsigned i = 0; i < mAllColliders.size(); ++i)
		{
			std::cout << i << " " << mAllColliders[i]->mOwner->GetBaseNameString() << std::endl;
		}
		std::cout << "-------------------" << std::endl;*/
	}

	// @TODO
	//!----------------------------------------------------------------------------
	// \fn		CollideAllBodies
	// \brief	Perform brute force collision detection/resolution between dynamic and static
	//			bodies. Repeats N times, where N = mCollisionIterations.
	//	\details
	//			- Must store the number collisions that occured in mCollisionsThisFrame
	//			- Must use "GetCollisionFn" to retrieve the correct collision function.
	//			- Dynamic bodies check collision with EVERY OTHER dynamic body.
	//			- Dynamic bodies check collision with EVERY static body.
	//			- Static bodies should NOT check collisions against other bodies.
	//			- Checks should be in this order: Dyn-vs-Dyn, Dyn-vs-Static
	// ----------------------------------------------------------------------------
	void CollisionSystem::CollideAllBodies()
	{
		// Initialize the counter for the new frame
		mCollisionsThisFrame = 0;

		// Repeat N times the process
		for (unsigned i = 0; i < mCollisionIterations; ++i)
		{
			// Iterator pointing to the end of the dynamic bodies list
			auto end1 = mDynamicColliders.end();

			// Iterator that points to the first element of the dynamic bodies list
			auto it1 = mDynamicColliders.begin();

			// Go through all the dynamic bodies
			for (; it1 != end1; ++it1)
			{
				// Compare all dynamic bodies with every other dynamic body
				for (auto it2 = std::next(it1, 1); it2 != end1; ++it2)
				{
					// Check if the collider is ghost or if it is in another space
					if (!CollidersOfSameSpace(*it1, *it2))
						continue;

					// Check the collision groups
					// Sets the detect and resolve booleans
					checkCollisionGroups(*it1, *it2);

					// Ignore collision
					if (!resolve && !detect)
						continue;

					// Get the collision function for the current two bodies
					fp = GetCollisionFn((*it1), (*it2));
					
					// Call the collision function and check if they are colliding
					if (fp((*it1), (*it2), &contact))
					{
						// Add the contact information to the current vector

						if ((*it2)->mIsGhost || (*it1)->mIsGhost)
						{
							if (i == mCollisionIterations - 1 && detect)
							{
								contact.mBody1 = *it1;
								contact.mBody2 = *it2;
								currentContacts.push_back(contact);
							}

							continue;
						}

						// Resolve the contact velocity once
						if (resolve)
						{
							if (i == 0)
							{
								ResolveContactVelocity((*it1)->mRigid, (*it2)->mRigid, &contact);
							}

							// Resolve the penetration for all iteration
							ResolveContactPenetration((*it1)->mRigid, (*it2)->mRigid, &contact);

							mCollisionsThisFrame++;
						}

						if (i == mCollisionIterations - 1 && detect)
						{
							contact.mBody1 = *it1;
							contact.mBody2 = *it2;
							currentContacts.push_back(contact);
						}
					}
				}

				// Iterator that points to the first element of the static bodies list
				auto it2 = mStaticColliders.begin();

				// Iterator pointing to the end of the static bodies list
				auto end2 = mStaticColliders.end();

				// Compare all dynamic bodies with every static body
				for (; it2 != end2; ++it2)
				{
					// Check if the collider is ghost or if it is in another space
					if (!CollidersOfSameSpace(*it1, *it2))
						continue;

					// Check the collision groups
					// Sets the detect and resolve booleans
					checkCollisionGroups(*it1, *it2);

					// Ignore collision
					if (!resolve && !detect)
						continue;

					// Get the collision function for the current two bodies
					fp = GetCollisionFn((*it1), (*it2));
 
					if (!fp)
						continue;

					// Call the collision function and check if they are colliding
					if (fp((*it1), (*it2), &contact))
					{
						if ((*it2)->mIsGhost || (*it1)->mIsGhost)
						{
							if (i == mCollisionIterations - 1 && detect)
							{
								contact.mBody1 = *it1;
								contact.mBody2 = *it2;
								currentContacts.push_back(contact);
							}

							continue;
						}

						// Resolve the contact velocity once
						if (resolve)
						{
							if (i == 0)
							{
								ResolveContactVelocity((*it1)->mRigid, (*it2)->mRigid, &contact);
							}

							// Resolve the penetration for all iteration
							ResolveContactPenetration((*it1)->mRigid, (*it2)->mRigid, &contact);

							mCollisionsThisFrame++;
						}

						// Add the contact information to the current vector
						if (i == mCollisionIterations - 1 && detect)
						{
							contact.mBody1 = *it1;
							contact.mBody2 = *it2;
							currentContacts.push_back(contact);
						}
					}
				}
			}
		}
	}

	bool CollisionSystem::CollideLine(AEVec2 * p0, AEVec2 * p1)
	{
		unsigned size = mAllColliders.size();

		// Go through all the colliders
		for (unsigned i = 0; i < size; ++i)
		{
			// Check if the current collider is a rectangle
			if (BoxCollider* box = dynamic_cast<BoxCollider*>(mAllColliders[i]))
			{
				if (mAllColliders[i]->collisionGroup != "Floor" && mAllColliders[i]->collisionGroup != "Enemy")
					continue;

				// Check if the line is colliding with the box
				if (LineToRect(&AEVec2(mAllColliders[i]->mOwnerPos->x, mAllColliders[i]->mOwnerPos->y), mAllColliders[i]->mScale.x, mAllColliders[i]->mScale.y, mAllColliders[i]->mOrientation, p0, p1))
					return true;
			}
		}
		return false;
	}

	void CollisionSystem::ManageEvents()
	{
		int currentSize = currentContacts.size();
		int prevSize = prevContacts.size();
		bool match = false;

		// Go through the previous contacts
		for (int i = 0; i < prevSize; ++i)
		{
			match = false;

			// Go through the previous contacts
			for (int j = 0; j < currentSize; ++j)
			{
				if (prevContacts[i] == currentContacts[j])
					match = true;
			}

			// Not in current
			if (match == false)
			{
				eventDispatcher.trigger_event(CollisionEnded(prevContacts[i].mBody1->mOwner, prevContacts[i].mBody2->mOwner));
				eventDispatcher.trigger_event(CollisionEnded(prevContacts[i].mBody2->mOwner, prevContacts[i].mBody1->mOwner));
			}
		}

		// Go through the current contacts
		for (int i = 0; i < currentSize; ++i)
		{
			match = false;

			// Go through the previous contacts
			for (int j = 0; j < prevSize; ++j)
			{
				// Check if the current contact is in prev
				if (currentContacts[i] == prevContacts[j])
				{
					eventDispatcher.trigger_event(CollisionEvent(currentContacts[i].mBody1->mOwner, currentContacts[i].mBody2->mOwner));
					eventDispatcher.trigger_event(CollisionEvent(currentContacts[i].mBody2->mOwner, currentContacts[i].mBody1->mOwner));

					match = true;
					break;
				}
			}

			// Current is not in previous
			if (match == false)
			{
				eventDispatcher.trigger_event(CollisionStarted(currentContacts[i].mBody1->mOwner, currentContacts[i].mBody2->mOwner));
				eventDispatcher.trigger_event(CollisionStarted(currentContacts[i].mBody2->mOwner, currentContacts[i].mBody1->mOwner));
			}
			
		}

		prevContacts = currentContacts;
		currentContacts.clear();
	}

	bool CollisionSystem::CollidersOfSameSpace(ICollider * c1, ICollider * c2)
	{
		return c1->mOwner->GetParentSpace() == c2->mOwner->GetParentSpace();
	}

	void CollisionSystem::checkCollisionGroups(ICollider * c1, ICollider * c2)
	{
		// Check if one of the colliders is not in a collision group
		if (c1->collisionGroup == "none" || c2->collisionGroup == "none")
		{
			detect = true;
			resolve = true;
			return;
		}

		// Otherwise, both are in a collision group
		// Get the collision policy
		CollisionTable::CollisionPolicy p = collision_table->getPolicy(c1->collisionGroup, c2->collisionGroup);

		if (p == CollisionTable::CollisionPolicy::Detect)
		{
			detect = true;
			resolve = false;
		}

		else if (p == CollisionTable::CollisionPolicy::DetectAndResolve)
		{
			detect = true;
			resolve = true;
		}

		else
		{
			detect = false;
			resolve = false;
		}

	}
	
}

#pragma endregion
// ----------------------------------------------------------------------------