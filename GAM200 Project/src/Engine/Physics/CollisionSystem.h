#ifndef COLLISION_SYSTEM_H_
#define COLLISION_SYSTEM_H_
// ---------------------------------------------------------------------------

#include <Core/AEXCore.h>
#include "src\Engine\Components\Rigidbody.h"
#include "src/Engine/Math/ContactCollisions.h"

// Collision restitution for velocity resolution
#define DFLT_RESTITUTION 0.208f;

namespace AEX
{
	class Rigidbody;
	class ICollider;

	// typedef for function pointer CollisionFn
	typedef bool(*CollisionFn)(ICollider*, ICollider*, Contact *);

	AEVec2 ConvertToVec2(const AEVec3 * pos);

	class CollisionSystem : public ISystem
	{
		AEX_RTTI_DECL(CollisionSystem, ISystem);
		AEX_SINGLETON(CollisionSystem);

	public:
		// ------------------------------------------------------------------------
		// Member Variables
		std::list<ICollider*> mStaticColliders;
		std::list<ICollider*> mDynamicColliders;

		// Collision iterations
		u32 mCollisionIterations;
		u32 mCollisionsThisFrame; // collisions this frame

		// ------------------------------------------------------------------------
		// Member Functions

		// System Functions
		bool Initialize();
		void Update();
		void Shutdown();

		// Rigid Body Management
		void AddCollider(ICollider* obj, Rigidbody::DynamicState is_dynamic);
		void RemoveCollider(ICollider *obj);
		void RemoveFromPrevContacts(ICollider* c);
		void ClearBodies();

		// findeing the collision tests
		CollisionFn GetCollisionFn(ICollider * b1, ICollider * b2);

		// Collides and resolve all rigidbodies 
		void CollideAllBodies();

		bool CollideLine(AEVec2* p0, AEVec2* p1);

		void ManageEvents();

		bool CollidersOfSameSpace(ICollider* c1, ICollider* c2);
		void checkCollisionGroups(ICollider * c1, ICollider * c2);

		// Exposed solver
		void ResolveContactPenetration(Rigidbody * obj1, Rigidbody * obj2, Contact * contact);
		void ResolveContactVelocity(Rigidbody * obj1, Rigidbody * obj2, Contact * contact);

		void PrintColliders();

		std::vector<ICollider *> mAllColliders;
		std::map < std::string , std::map<std::string, CollisionFn> > mAllFunctions;

		bool detect, resolve;

		// Store the collision function
		CollisionFn fp;

		// Store the contact data of a collision
		Contact contact;

		std::vector<Contact>currentContacts;
		std::vector<Contact>prevContacts;
	};

	#define aexCollisionSystem (CollisionSystem::Instance())
}
// ---------------------------------------------------------------------------
#endif