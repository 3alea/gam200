#include "RNG.h"
#include <cstdlib> //srand
#include <ctime>



bool RandomNumberGenerator::Initialize()
{
	srand((unsigned)time(0));

	return true;
}


int RandomNumberGenerator::GetInt(int min, int max)
{
	srand((unsigned)time(0));
	return min + rand() % (max - min);
}

int RandomNumberGenerator::Get0or1()
{
	srand((unsigned)time(0));
	return rand() % 2;
}


float RandomNumberGenerator::GetFloat(float min, float max)
{
	return min + static_cast<float>(std::rand()) / static_cast<float>(RAND_MAX / (max - min));
}