#ifndef AEX_ASSERT_H_
#define AEX_ASSERT_H_

#include <intrin.h>



namespace AEX
{

#ifdef ASSERT_ACTIVE
	// After the expression, you can specify an optional message. If you don't
	// want to print any message, pass NULL to the second parameter.
	#define ASSERT(expression, message)								\
	if (!expression)												\
	{																\
		HandleFailedAssertion(__FILE__, __LINE__, message);			\
		__debugbreak();												\
	}																\

#else
	#ifdef DEBUG_MODE
		#define ASSERT(expression, message)								\
		if (!expression)												\
		{																\
			std::cout << "ASSERT_ACTIVE preprocessor definition " <<	\
						"doesn't exist. Be sure to activate it " <<		\
						"in project properties->C/C++->Preprocessor"	\
						<<"Definitions." << std::endl;					\
		}																\

	#else
		#define ASSERT(expression, message)
	#endif

#endif


#define CATCH_FS_ERROR								\
catch (const fs::filesystem_error & exception)		\
{													\
	ASSERT(0, exception.what());					\
}													\



void HandleFailedAssertion(const char * file, int line, const char * message);
}



#endif