#pragma once
#include "Core/AEXSystem.h"

class RandomNumberGenerator : public AEX::ISystem
{
	AEX_RTTI_DECL(RandomNumberGenerator, ISystem);
	AEX_SINGLETON(RandomNumberGenerator);

public:
	bool Initialize();

	int GetInt(int min = 0, int max = INT_MAX);
	int Get0or1();
	float GetFloat(float min, float max);
};

#define RNG (RandomNumberGenerator::Instance())
