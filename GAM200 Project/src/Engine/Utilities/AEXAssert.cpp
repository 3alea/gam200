#include <iostream>
#include "src/Engine/Utilities/AEXAssert.h"


namespace AEX
{
	void HandleFailedAssertion(const char * file, int line, const char * message)
	{
		std::cerr << "An assertion failed in the file " << file << " at line " << line << "." << std::endl;

		if (message != NULL)
			std::cerr << "Message: " << message << std::endl;
	}
}