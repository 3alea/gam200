#ifndef AEX_GAME_STATE_H_
#define AEX_GAME_STATE_H_


#include "..\Core\AEXBase.h"

namespace AEX
{
	class IGameState : public virtual IBase
	{
		AEX_RTTI_DECL(IGameState, IBase);

	public:
		// Virtual methods to do all the necessary
		// things for a particular gamestate
		virtual void Initialize() = 0;
		virtual void Update() = 0;	// Update and Initialize are pure virtual to force implementation
		virtual void Render();
		virtual void Shutdown();

		// Virtual destructor, does nothing for now
		virtual ~IGameState();

		// Helper virtual functions 
		virtual void LoadResources();
		virtual void FreeResources();
	};
}


#endif