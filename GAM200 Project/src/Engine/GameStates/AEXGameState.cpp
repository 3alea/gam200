#include "../Core/AEXBase.h"
#include "AEXGameState.h"

namespace AEX
{
	// All of these methods are empty because they are supposed to be overriden

	void IGameState::Initialize()
	{
	}

	void IGameState::Render()
	{
	}

	void IGameState::Shutdown()
	{
	}

	void IGameState::LoadResources()
	{
	}

	void IGameState::FreeResources()
	{
	}

	IGameState::~IGameState()
	{
	}
}