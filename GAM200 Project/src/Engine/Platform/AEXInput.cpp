// ---------------------------------------------------------------------------
// Project Name		:	Alpha Engine
// File Name		:	AEInput.cpp
// Author			:	Sun Tjen Fam, Antoine Abi Chakra
// Creation Date	:	2008/0131
// Purpose			:	Input wrapper
// History			:
// - 2008/01/31		:	- initial implementation
// ---------------------------------------------------------------------------
#include "AEXInput.h"

#include <iostream>

namespace AEX
{
	// ---------------------------------------------------------------------------
	/*Input::Input()
	{}*/

	// ---------------------------------------------------------------------------
	Input::~Input()
	{}
	
	// ---------------------------------------------------------------------------
	bool Input::Initialize()
	{
		std::memset(mKeyCurr, 0, sizeof(u8) * GLFW_KEY_LAST);
		std::memset(mKeyPrev, 0, sizeof(u8) * GLFW_KEY_LAST);

		std::memset(mMouseCurr, 0, sizeof(u8) * AEX_INPUT_MOUSE_NUM);
		std::memset(mMousePrev, 0, sizeof(u8) * AEX_INPUT_MOUSE_NUM);

		mAnyKey = false;
		mAnyKeyTriggered = false;
		mKeyPressed.clear();
		mKeyTriggered.clear();
		mMouseWheel = 0.0f;

		HandleGLFWMessage();

		return true;
	}

	// ---------------------------------------------------------------------------
	namespace internals
	{
		bool mouseWheelThisFrame = false;
	} using namespace internals;

	void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
	{
		aexInput->SetMouseButton(button, action);
	}

	void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
	{
		// from 0 to GLFW_KEY_LAST
		aexInput->SetKeyButton(key, action);
	}

	void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
	{
		aexInput->SetMouseWheel((f32)yoffset / 5.0f);
		mouseWheelThisFrame = true;
	}

	void Input::HandleGLFWMessage()
	{
		glfwSetMouseButtonCallback(*WindowMgr->GetCurrentWindow(), mouse_button_callback);
		glfwSetKeyCallback(*WindowMgr->GetCurrentWindow(), key_callback);
		glfwSetScrollCallback(*WindowMgr->GetCurrentWindow(), scroll_callback);
	}

	// Deprecated
	bool Input::HandleWin32Message(UINT msg, WPARAM wParam, LPARAM lParam)
	{
		AE_UNUSED(lParam);
		switch (msg)
		{
			// Keyboard
		case WM_KEYDOWN:
		case WM_SYSKEYDOWN:
			if (wParam == VK_MENU)
			{
				mKeyCurr[Input::eAlt] = 1;
			}
			else
				mKeyCurr[wParam] = 1;
			break;

		case WM_KEYUP:
		case WM_SYSKEYUP:
			if (wParam == VK_MENU)
			{
				mKeyCurr[Input::eAlt] = 0;
			}
			else
				mKeyCurr[wParam] = 0;
			break;

			// Mouse Right Button		
		case WM_RBUTTONDOWN:
			mMouseCurr[Input::eMouseRight] = 1;
			break;

		case WM_RBUTTONUP:
			mMouseCurr[Input::eMouseRight] = 0;
			break;
			// Mouse Left Button
		case WM_LBUTTONDOWN:
			mMouseCurr[Input::eMouseLeft] = 1;
			break;

		case WM_LBUTTONUP:
			mMouseCurr[Input::eMouseLeft] = 0;
			break;
			// Mouse Left Button
		case WM_MBUTTONDOWN:
			mMouseCurr[Input::eMouseMiddle] = 1;
			break;

		case WM_MBUTTONUP:
			mMouseCurr[Input::eMouseMiddle] = 0;
			break;
			// not an input message, return false
		case WM_MOUSEWHEEL:
		{
			short  delta = GET_WHEEL_DELTA_WPARAM(wParam);
			mMouseWheel = (f32)delta / 120.0f;
			mouseWheelThisFrame = true;
			break;
		}
		default:
			return false;
		}
		return true;
	}

	// ---------------------------------------------------------------------------

	void Input::Update()
	{
		int i;		// counter for loop
		POINT mp;
		f64 cursor_xpos, cursor_ypos; // to store the mouse position

		glfwPollEvents();	

		Window * mainWindow = WindowMgr->GetCurrentWindow();

		GetCursorPos(&mp);
		//Get the mouse position in screen coordinate
		glfwGetCursorPos(*mainWindow, &cursor_xpos, &cursor_ypos);

		//ScreenToClient(mainWindow->GetHandle(), &mp);

		mMousePrevPos = mMousePos;
		mMousePos.x = (float)mp.x;
		mMousePos.y = (float)mp.y;

		int xpos, ypos;
		glfwGetWindowPos(*mainWindow, &xpos, &ypos);

		f32 win_width = (f32)mainWindow->GetWidth();
		f32 win_height = (f32)mainWindow->GetHeight();
		
		mGLFWMousePos.x = (f32)(cursor_xpos) - win_width / 2.0f;
		mGLFWMousePos.y = (f32)(-cursor_ypos + win_height / 2.0f);

		/* Step 3:  Compute whether the mouse position is inside the window*/
		mMouseInWindow = !(mMousePos.x < (f32)xpos || mMousePos.x >  win_width + (f32)xpos ||
			mMousePos.y < (f32)ypos || mMousePos.y >  win_height + (f32)ypos);

		/* Step 4: Compute the mouse position in centered window coordinate */

		mMousePos.x = mMousePos.x - win_width / 2;
		mMousePos.y = -mMousePos.y + win_height / 2;

		//printf("Mouse pos: %i, %i = %f, %f\n", mp.x, mp.y, mMousePos.x, mMousePos.y);

		// compute mouse difference
		mMouseDiff.x = mMousePrevPos.x - mMousePos.x;
		mMouseDiff.y = mMousePrevPos.y - mMousePos.y;

		// reset the mouse wheel
		if (mouseWheelThisFrame)
			mouseWheelThisFrame = false;
		else
			mMouseWheel = 0.0f;


		// Clear the keys that were pressed last frame
		mKeyPressed.clear();
		mKeyTriggered.clear();
		mAnyKey = false;
		mAnyKeyTriggered = false;

		// Determine whether a key was triggered or not
		for (i = 0; i < GLFW_KEY_LAST; ++i)
		{
			if (mKeyCurr[i] == 3)
				mKeyCurr[i] = 0;

			if (mKeyCurr[i])
			{
				// any keys has been pressed
				mAnyKey = true;
				mKeyPressed.push_back((u16)i);

				// Key is being triggered
				if (!mKeyPrev[i])
				{
					mAnyKeyTriggered = true;
					mKeyTriggered.push_back((u16)i);
					mKeyCurr[i] = 2;
				}
				else
					mKeyCurr[i] = 1;

				mKeyPrev[i] = mKeyCurr[i];
			}

			// Key released
			else if (mKeyPrev[i])
			{
				mKeyCurr[i] = 3;
				mKeyPrev[i] = 0;
			}
		}

		//Determine whether a mouse button was triggered or not
		for (i = 0; i < AEX_INPUT_MOUSE_NUM; ++i)
		{
			if (mMouseCurr[i])
			{
				if (!mMousePrev[i])
				{
					mMouseCurr[i] = 2;
				}
				else
					mMouseCurr[i] = 1;
			}
			mMousePrev[i] = mMouseCurr[i];
		}

		// Update the Gamepad controller
		UpdateGamepad();
	}

	// ---------------------------------------------------------------------------

	const std::vector<u16> & Input::AllKeyPressed()
	{
		return mKeyPressed;
	}

	// ---------------------------------------------------------------------------

	const std::vector<u16> & Input::AllKeyTriggered()
	{
		return mKeyTriggered;
	}

	void Input::SetMouseButton(int key, int pressed)
	{
		mMouseCurr[key] = pressed;
	}

	void Input::SetKeyButton(int key, int pressed)
	{
		mKeyCurr[key] = pressed;
	}

	void Input::SetMouseWheel(f32 movement)
	{
		mMouseWheel = movement;
	}


	// ---------------------------------------------------------------------------

	bool Input::AnyKey()
	{
		return mAnyKey;
	}

	// ---------------------------------------------------------------------------

	bool Input::AnyKeyTriggered()
	{
		return mAnyKeyTriggered;
	}

	// ---------------------------------------------------------------------------

	bool Input::KeyPressed(int key)
	{
		if (key >= GLFW_KEY_LAST)
			return 0;

		if (key == eAny)
			return mAnyKey;

		return mKeyCurr[key] > 0;
	}

	// ---------------------------------------------------------------------------

	bool Input::KeyTriggered(int key)
	{
		if (key >= GLFW_KEY_LAST)
			return 0;

		if (key == eAny)
			return mAnyKey;
		return mKeyCurr[key] == 2;
	}

	// ---------------------------------------------------------------------------

	bool Input::KeyReleased(int key)
	{
		if (key >= GLFW_KEY_LAST)
			return 0;

		return mKeyCurr[key] == 3;
	}

	// ---------------------------------------------------------------------------

	bool Input::MousePressed(int button)
	{
		if (button > Input::eMouseMiddle)
			return 0;

		return mMouseCurr[button] > 0;
	}

	// ---------------------------------------------------------------------------

	bool Input::MouseTriggered(int button)
	{
		if (button > Input::eMouseMiddle)
			return 0;

		return mMouseCurr[button] == 2;
	}

	// ---------------------------------------------------------------------------

	AEVec2 Input::GetGLFWMousePos()
	{
		return mGLFWMousePos;
	}

	AEVec2 Input::GetMousePos()
	{
		return mMousePos;
	}
	// ---------------------------------------------------------------------------

	AEVec2 Input::GetPrevMousePos()
	{
		return mMousePrevPos;
	}

	// ---------------------------------------------------------------------------

	AEVec2 Input::GetMouseMovement()
	{
		return mMouseDiff;
	}

	// ---------------------------------------------------------------------------

	bool	  Input::MouseInWindow()
	{
		return mMouseInWindow;
	}

	void Input::UpdateGamepad()
	{
		mGamepadPresent = glfwJoystickPresent(GLFW_JOYSTICK_1);

		if (mGamepadPresent == 1)
		{
			static int gamepadAxesCount; // Total number of Stick triggers

			for (int i = 0; i < gamepadAxesCount; i++)
				mGamepadAxesPrev[i] = mGamepadAxesCurr[i];

			mGamepadAxesCurr = const_cast<float*>(glfwGetJoystickAxes(GLFW_JOYSTICK_1, &gamepadAxesCount));

			static int	gamepadButtonCount; // Total number of Button triggers

			mGamepadButtonsCurr = const_cast<u8*>(glfwGetJoystickButtons(GLFW_JOYSTICK_1, &gamepadButtonCount));

			for (int i = 0; i < gamepadButtonCount; i++)
			{
				// Being triggered
				if (!mGamepadButtonsPrev[i] && mGamepadButtonsCurr[i] == 1)
					mGamepadButtonsCurr[i] = 2;
				
				// Being released
				else if (mGamepadButtonsPrev[i] == 1 && !mGamepadButtonsCurr[i])
					mGamepadButtonsCurr[i] = 3;
			}

			for (int i = 0; i < gamepadButtonCount; i++)
			{
				mGamepadButtonsPrev[i] = mGamepadButtonsCurr[i];
			}
		}

		if (KeyPressed(GLFW_KEY_B))
			PrintGamepadStatus();
	}

	void Input::PrintGamepadStatus()
	{
		/*std::cout << "Gamepad 1 current status : " << mGamepadPresent << std::endl;
		if (mGamepadPresent == 1)
		{
			std::cout << "------------------------------------------------------" << std::endl;

			std::cout << "	 Left stick X axis : " << mGamepadAxesCurr[0] << std::endl;
			std::cout << "	 Left stick Y axis : " << mGamepadAxesCurr[1] << std::endl;
			std::cout << "	 Right stick X axis : " << mGamepadAxesCurr[2] << std::endl;
			std::cout << "	 Right stick Y axis : " << mGamepadAxesCurr[3] << std::endl;
			std::cout << "	 Left Trigger/L2 : " << mGamepadAxesCurr[4] << std::endl;
			std::cout << "	 Right Trigger/R2 : " << mGamepadAxesCurr[5] << std::endl;

			std::cout << "	 Button A : "	  << mGamepadButtonsCurr[0] << std::endl;
			std::cout << "	 Button B : "	  << mGamepadButtonsCurr[1] << std::endl;
			std::cout << "	 Button X : "	  << mGamepadButtonsCurr[2] << std::endl;
			std::cout << "	 Button Y : "	  << mGamepadButtonsCurr[3] << std::endl;
			std::cout << "	 Left Bumper : "  << mGamepadButtonsCurr[4] << std::endl;
			std::cout << "	 Right Bumper : " << mGamepadButtonsCurr[5] << std::endl;
			std::cout << "	 Back Button : "  << mGamepadButtonsCurr[6] << std::endl;
			std::cout << "	 Start Button : " << mGamepadButtonsCurr[7] << std::endl;
			std::cout << "	 Left thumb : "   << mGamepadButtonsCurr[8] << std::endl;
			std::cout << "	 Right thumb : "  << mGamepadButtonsCurr[9] << std::endl;
			std::cout << "	 Dpad - Up : "	  << mGamepadButtonsCurr[10] << std::endl;
			std::cout << "	 Dpad - Right : " << mGamepadButtonsCurr[11] << std::endl;
			std::cout << "	 Dpad - Down : "  << mGamepadButtonsCurr[12] << std::endl;
			std::cout << "	 Dpad - Left : "  << mGamepadButtonsCurr[13] << std::endl;
		}*/
	}

	// ---------------------------------------------------------------------------
	f32		Input::GetMouseWheel()
	{
		return mMouseWheel;
	}
}