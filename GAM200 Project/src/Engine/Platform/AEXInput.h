// ---------------------------------------------------------------------------
// Project Name		:	Alpha Engine
// File Name		:	AEInput.h
// Author			:	Sun Tjen Fam
// Creation Date	:	2008/01/31
// Purpose			:	declaration for input stuff
// History			:
// - 2008/01/31		:	- initial implementation
// ---------------------------------------------------------------------------

#ifndef AEX_INPUT_H
#define AEX_INPUT_H
// ---------------------------------------------------------------------------
// Defines/Enums


#define AEX_INPUT_KEY_NUM	257
#define AEX_INPUT_MOUSE_NUM 3
#define AEX_INPUT_GAMEPAD_AXES_NUM 6
#define AEX_INPUT_GAMEPAD_BUTTONS_NUM 14

#include "..\Utilities\AEXContainers.h"
#include "..\Core\AEXCore.h"
#include <Windows.h>
#include "extern\aexmath\aexmath\AEXVec2.h"
#include "..\Core\AEXTypedefs.h"
#include "../Graphics/WindowMgr.h"

#pragma warning (disable:4251) // dll and STL
namespace AEX
{
	// ---------------------------------------------------------------------------
	// class definitions
	class  Input : public ISystem
	{
		AEX_RTTI_DECL(Input, ISystem);
		AEX_SINGLETON(Input);

	public:
		enum Keys
		{
			eAny = -1,
			eMouseLeft = 0,
			eMouseRight = 1,
			eMouseMiddle = 2,
			eControl = GLFW_KEY_LEFT_CONTROL,
			eShift = GLFW_KEY_LEFT_SHIFT,
			eTab = GLFW_KEY_TAB,
			eAlt = GLFW_KEY_LEFT_ALT
		};

		enum GamepadSticks
		{
			eLeftStickXAxis  = 0,
			eLeftStickYAxis  = 1,
			eRightStickXAxis = 2,
			eRightStickYAxis = 3,
			eLeftTrigger	 = 4,
			eRightTrigger	 = 5,

			eStickCount		 = 6
		};

		enum GamepadButtons
		{
			//eAny = -1, (To Be Implemented)
			eGamepadButtonA				= 0,
			eGamepadButtonB				= 1,
			eGamepadButtonX				= 2,
			eGamepadButtonY				= 3,
			eGamepadButtonLeftBumper	= 4,
			eGamepadButtonRightBumper	= 5,
			eGamepadButtonBack			= 6,
			eGamepadButtonStart			= 7,
			eGamepadButtonLeftThumb		= 8,
			eGamepadButtonRightThumb	= 9,
			eGamepadButtonDPadUp		= 10,
			eGamepadButtonDPadRight		= 11,
			eGamepadButtonDPadDown		= 12,
			eGamepadButtonDPadLeft		= 13,

			eGamepadButtonCount			= 15
		};

	public:

		// ISystem
		virtual bool Initialize(); 
		virtual void Update();
		virtual ~Input();

		// Input
		void HandleGLFWMessage();
		bool HandleWin32Message(UINT msg, WPARAM wParam, LPARAM lParam);
		bool AnyKey();
		bool AnyKeyTriggered();
		bool KeyPressed(int key);
		bool KeyTriggered(int key);
		bool KeyReleased(int key);
		bool MousePressed(int button);
		bool MouseTriggered(int button);
		bool GamepadButtonTriggered(GamepadButtons index) { return (mGamepadPresent && mGamepadButtonsCurr[index] == 2); }
		bool GamepadButtonPressed(GamepadButtons index)   { return (mGamepadPresent && mGamepadButtonsCurr[index] == 1); }
		bool GamepadButtonReleased(GamepadButtons index)  { return (mGamepadPresent && mGamepadButtonsCurr[index] == 3); }

		float GetGamepadStick(GamepadSticks index) { return mGamepadPresent ? mGamepadAxesCurr[index] : 0; }
		unsigned char GetGamepadButton(GamepadButtons index) { return mGamepadPresent ? mGamepadButtonsCurr[index] : 0; }


		AEVec2 GetGLFWMousePos();
		AEVec2 GetMousePos();
		AEVec2 GetPrevMousePos();
		AEVec2 GetMouseMovement(); // Mouse diff
		f32	   GetMouseWheel();
		bool MouseInWindow();

		void UpdateGamepad();
		void PrintGamepadStatus();

		const std::vector<u16> & AllKeyPressed();
		const std::vector<u16> & AllKeyTriggered();

		void SetMouseButton(int key, int pressed);
		void SetKeyButton(int key, int pressed);
		void SetMouseWheel(f32 movement);

		bool isGamepadActive() { return mGamepadPresent; }

	private:

		u8	mKeyCurr[GLFW_KEY_LAST];
		u8	mKeyPrev[GLFW_KEY_LAST];
		u8	mMouseCurr[AEX_INPUT_MOUSE_NUM];
		u8	mMousePrev[AEX_INPUT_MOUSE_NUM];
		f32 mMouseWheel;
		AEVec2 mGLFWMousePos;		// Mouse Position in GLFW
		AEVec2 mMousePos;			// Mouse Position in Centered Coordinates
		AEVec2 mMousePrevPos;		// Previous Mouse Position in Centered Coordinates
		AEVec2 mMouseDiff;		// Mouse movement this update: Prev - Curr
		bool		 mMouseInWindow;	// Specifies whether the mouse is contained insisde the window


		bool				mAnyKey;	 // Any key is pressed. 
		bool				mAnyKeyTriggered;	 // Any key is pressed. 
		std::vector<u16>	mKeyPressed; // keys pressed last frame.
		std::vector<u16>	mKeyTriggered; // keys pressed last frame.


		int	mGamepadPresent; // Is the Gamepad currently plugged
		float* mGamepadAxesCurr;
		float mGamepadAxesPrev[AEX_INPUT_GAMEPAD_AXES_NUM];
		unsigned char* mGamepadButtonsCurr;
		unsigned char mGamepadButtonsPrev[AEX_INPUT_GAMEPAD_BUTTONS_NUM];
	};
}

#pragma warning (default:4251) // dll and STL

// Easy access to singleton
#define aexInput (Input::Instance())
// ---------------------------------------------------------------------------

#endif // AE_INPUT_H

