// ----------------------------------------------------------------------------
//	Copyright (C)DigiPen Institute of Technology.
//	Reproduction or disclosure of this file or its contents without the prior
//	written consent of DigiPen Institute of Technology is prohibited.
//	
//	File Name:		Collisions.cpp
//	Purpose:		Contains various function definitions that test wether
//                  basic figures like circles or rectangles are
//				    colliding or not. (Also tests with points)
//	Project:		cs230_diego.sanz_2_2
//	Author:			Diego Sanz (diego.sanz@digipen.edu)
// ----------------------------------------------------------------------------

#include "Collisions.h"
namespace AEX
{
	//! ---------------------------------------------------------------------------
	// \fn		StaticPointToStaticCircle
	// \brief	This function determines if a static point is inside a static circle
	// 			- P:		The point to test for circle containment
	// 			- Center:	Center of the circle
	// 			- Radius:	Radius of the circle
	// 
	//  \return	true if the point is contained in the circle, false otherwise.
	// ---------------------------------------------------------------------------
	bool StaticPointToStaticCircle(AEVec2 *P, AEVec2 *Center, float Radius)
	{
		// Calculate the distance between the point and the center of the circle
		float distance = P->DistanceSq(*Center);

		// If the distance is smaller than the radius of the circle 
		// the point is inside the circle
		if (distance <= Radius * Radius)
		{
			return true;
		}

		// If the distance is bigger than the radius the point is outside the circle
		else
		{
			return false;
		}
	}

	//! ---------------------------------------------------------------------------
	//	\fn		StaticPointToStaticRect
	//	\brief	This function checks if the point Pos is colliding with the rectangle
	//			whose center is Rect, width is "Width" and height is Height
	//
	//  \return	true if the point is contained in the rectangle, false otherwise.
	// ---------------------------------------------------------------------------
	bool StaticPointToStaticRect(AEVec2 *Pos, AEVec2 *Rect, float Width, float Height)
	{
		float Top = Rect->y + Height / 2; // Top point of the rectangle in the y axis
		float Bottom = Rect->y - Height / 2; // Bottom point of the rectangle in the y axis
		float Right = Rect->x + Width / 2; // Right point of the rectangle in the x axis
		float Left = Rect->x - Width / 2; // Left point of the rectangle in the x axis

		// Check if the point is inside the rectangle
		if (Pos->y <= Top && Pos->y >= Bottom && Pos->x <= Right && Pos->x >= Left)
		{
			return true;
		}

		// The point is outside the rectangle
		else
		{
			return false;
		}
	}

	//! ---------------------------------------------------------------------------
	//	\fn		StaticPointToOrientedRect
	//	\brief	This function checks if the point Pos is colliding with an oriented rectangle
	//			whose center is Rect, width is "Width", height is Height and rotation "AngleRad"
	//
	//  \return	true if the point is contained in the rectangle, false otherwise.
	// ---------------------------------------------------------------------------
	bool StaticPointToOrientedRect(AEVec2 *Pos, AEVec2 *Rect, float Width, float Height, float AngleRad)
	{
		// Create a transform with the position of the rectangle and its rotation
		// Scale 1 to avoid losing the width and height
		Transform invMatrix(*Rect, { 1,1 }, AngleRad);

		// Create an inverse matrix to align the rectangle with the x and y axis 
		// and translate it to the origin
		AEMtx33 finalMatrix = invMatrix.GetInvMatrix();

		// Make a copy of both positions
		// Avoid changing the original values
		AEVec2 PosTemp = *Pos;
		AEVec2 RectTemp = *Rect;

		// Multipy the copies by the final matrix
		PosTemp = finalMatrix * (*Rect);
		RectTemp = finalMatrix * (*Pos);

		// Check if the point is inside the static rectangle
		return StaticPointToStaticRect(&PosTemp, &RectTemp, Width, Height);
	}

	//! ---------------------------------------------------------------------------
	//	\fn		StaticCircleToStaticCircle
	//	\brief	This function checks for collision between 2 circles.
	//			Circle0: Center is Center0, radius is "Radius0"
	//			Circle1: Center is Center1, radius is "Radius1"
	//
	//  \return	true if both circles overlap, false otherwise.
	// ---------------------------------------------------------------------------
	bool StaticCircleToStaticCircle(AEVec2 *Center0, float Radius0, AEVec2 *Center1, float Radius1)
	{
		// Check the collision between a point and a circle with the radius = radius0 + radius1
		return StaticPointToStaticCircle(Center0, Center1, Radius0 + Radius1);
	}

	//! ---------------------------------------------------------------------------
	// \fn		StaticRectToStaticRect
	// \brief	This functions checks if 2 rectangles are colliding
	//			Rectangle0: Center is pRect0, width is "Width0" and height is "Height0"
	//			Rectangle1: Center is pRect1, width is "Width1" and height is "Height1"
	//
	//  \return	true if both rectangles overlap, false otherwise.
	// ---------------------------------------------------------------------------
	bool StaticRectToStaticRect(AEVec2 *Rect0, float Width0, float Height0, AEVec2 *Rect1, float Width1, float Height1)
	{
		// Check the collision between a point and a static rectangle with the width = width0 + width1
		// and the height = height0 + height1
		return StaticPointToStaticRect(Rect0, Rect1, Width0 + Width1, Height0 + Height1);
	}

	//! ---------------------------------------------------------------------------
	// \fn		StaticRectToStaticCirlce
	// \brief	This function checks if a circle is colliding with a rectangle
	//			Rectangle: Defined at center "Rect" of width and height "Width", "Height", respectively
	//			Center: Defined at center "Center", and of radius "Radius"
	//
	//  \return	true if both shapes overlap, false otherwise.
	// ---------------------------------------------------------------------------
	bool StaticRectToStaticCirlce(AEVec2 * Rect, float Width, float Height, AEVec2* Center, float Radius)
	{
		AEVec2 clamPoint;

		// Clamp the cirle's center
		clamPoint.x = Clamp(Center->x, Rect->x - Width / 2, Rect->x + Width / 2);
		clamPoint.y = Clamp(Center->y, Rect->y - Height / 2, Rect->y + Height / 2);

		// Check if the clamped point is inside the circle
		return StaticPointToStaticCircle(&clamPoint, Center, Radius);
	}

	//! ---------------------------------------------------------------------------
	// \fn		OrientedRectToStaticCirlce
	// \brief	This function checks if a circle is colliding with an oriented rectangle
	//			Rectangle: Defined at center "Rect" of width and height "Width", "Height", 
	//			and rotation "AngelRad" respectively
	//			Center: Defined at center "Center", and of radius "Radius"
	//
	//  \return	true if both shapes overlap, false otherwise.
	// ---------------------------------------------------------------------------
	bool OrientedRectToStaticCirlce(AEVec2 * Rect, float Width, float Height, float AngleRad, AEVec2* Center, float Radius)
	{
		// Create a transform with the position of the rectangle and its rotation
		// Scale 1 to avoid losing the original width and height of the rectangle
		Transform invMatrix(*Rect, { 1,1 }, AngleRad);

		// Create an inverse matrix to align the rectangle with the x and y axis 
		// and translate it to the origin
		AEMtx33 finalMatrix = invMatrix.GetInvMatrix();

		// Make a copy of both positions
		// Avoid changing the original values
		AEVec2 CenterTemp = *Center;
		AEVec2 RectTemp = *Rect;

		// Multipy the copies by the final matrix
		CenterTemp = finalMatrix * (*Rect);
		RectTemp = finalMatrix * (*Center);

		// Check if the point is inside the static rectangle
		return StaticRectToStaticCirlce(&RectTemp, Width, Height, &CenterTemp, Radius);
	}

	bool LineToRect(AEVec2 * Rect, float Width, float Height, float rot, AEVec2* p0, AEVec2* p1)
	{
		if (StaticPointToOrientedRect(p0, Rect, Width, Height, rot))
			return true;
		if (StaticPointToOrientedRect(p1, Rect, Width, Height, rot))
			return true;
		return false;
	}
}