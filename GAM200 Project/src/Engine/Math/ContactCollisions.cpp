// ----------------------------------------------------------------------------
//	Copyright (C)DigiPen Institute of Technology.
//	Reproduction or disclosure of this file or its contents without the prior 
//	written consent of DigiPen Institute of Technology is prohibited.
//	
//	File Name:		ContactCollisions.cpp
//	Purpose:		Contains test of collisions between different shapes and
//					that calculate the contact data.
//	Project:		CS230_diego.sanz_3
//	Author:			Diego Sanz / diego.sanz / 540001618
// ----------------------------------------------------------------------------
#include "Collisions.h"
#include "ContactCollisions.h"
#include <limits>	// UINT_MAX
#include <cfloat>	// FLT_MAX
// ---------------------------------------------------------------------------

namespace AEX
{
	// @TODO
	//! ---------------------------------------------------------------------------
	// \fn		StaticCircleToStaticCircleEx
	// \brief	Checks overlap between two circles. If an overla exists, it stores
	//			the contact data into pResult.
	//
	// \details	
	//			- You must compute ALL the contact data.
	//			- You should check for pResult not being equal to NULL.
	//			- Normal must be from Circle1 to Circle2.
	//			- Point of intersection must be on the edge of Circle1. It represents
	//			  The point where both circles touch (after collision is resolved).
	// 
	//  \return	true if the shapes overlap, false otherwise
	// ---------------------------------------------------------------------------
	bool StaticCircleToStaticCircleEx(AEVec2* Center1, float Radius1, AEVec2 * Center2, float Radius2, Contact * pResult)
	{
		// Check if the circles are not ovelaping
		if (StaticCircleToStaticCircle(Center1, Radius1, Center2, Radius2) == false)
		{
			return false; // Done
		}

		// Check if the Contact pointer is NULL
		if (pResult == NULL)
		{
			return true; // Done, the circles are overlaping
		}

		// Calculate the unitary vector that goes from center1 to center2
		pResult->mNormal = ((*Center2) - (*Center1)).Normalize();

		// Calculate the amount of penetration
		pResult->mPenetration = Radius1 + Radius2 - Center1->Distance(*Center2);

		// Calculate the point of intersection
		pResult->mPi = (*Center1) + pResult->mNormal * Radius1;

		return true; // Done, the circles are overlaping
	}

	// @TODO
	//! ---------------------------------------------------------------------------
	// \fn		StaticRectToStaticCircleEx
	// \brief	Checks overlap between a rectangle and a circle.If an overlap exists, 
	// 			it stores the contact data into pResult.
	//
	// \details	
	//			- You must handle the case when the circle is inside the rectangle
	//			  as described in class. 
	//			- You should check for pResult not being equal to NULL.
	//			- You must compute ALL the contact data.
	//			- You must assume that the contact viewer is the rectangle. 
	//			  (i.e. normal goes from Box to Circle).
	// 
	//  \return	true if the shapes overlap, false otherwise
	// ---------------------------------------------------------------------------
	bool StaticRectToStaticCircleEx(AEVec2 * Rect, float Width, float Height, AEVec2 * Center, float Radius, Contact * pResult)
	{
		// Check if the two shapes are not overlaping
		if (StaticRectToStaticCirlce(Rect, Width, Height, Center, Radius) == false)
		{
			return false; // Done
		}

		// Check if the Contact pointer is NULL
		if (pResult == NULL)
		{
			return true; // Done, both shapes are overlaping with each other
		}

		// Calculate the top, bottom, right and left limits of the rectangle
		float top = Rect->y + Height / 2.0f;
		float bottom = Rect->y - Height / 2.0f;
		float right = Rect->x + Width / 2.0f;
		float left = Rect->x - Width / 2.0f;

		// Case1 : The center of the circle is inside the rectangle
		if (Center->x <= right && Center->x >= left && Center->y <= top && Center->y >= bottom)
		{
			// Create a vector that goes from the center of the rectangle to the center of the circle
			AEVec2 BoxToCircle = (*Center) - (*Rect);

			// Create a copy of the vector
			AEVec2 BoxToCircleABS = BoxToCircle;

			// Calculate the absolute value of the vector
			if (BoxToCircle.x < 0.0f)	BoxToCircleABS.x *= -1.0f;
			if (BoxToCircle.y < 0.0f)	BoxToCircleABS.y *= -1.0f;

			AEVec2 penetration;

			// Calculate the penetration in the x and y axis
			penetration.x = Width / 2.0f - BoxToCircleABS.x;
			penetration.y = Height / 2.0f - BoxToCircleABS.y;

			// Check if the penetration of x is the minimum one
			if (penetration.x < penetration.y)
			{
				pResult->mPenetration = penetration.x + Radius;

				// Calculate the normal
				pResult->mNormal = { 1.0f, 0.0f };
			}

			// Otherwise, the penetration of y is the minimum one
			else
			{
				pResult->mPenetration = penetration.y + Radius;

				// Calculate the normal
				pResult->mNormal = { 0.0f, 1.0f };
			}

			// Check if the normal is pointing in the wrong direction
			if (BoxToCircle * pResult->mNormal < 0.0f)
			{
				// Flip the normal
				pResult->mNormal *= -1.0f;
			}

			// In this case, the point of intersection is the center of the circle
			pResult->mPi = *Center;

			return true; // Done, both shapes are overlaping with each other
		}

		// Case2 : The center of the cirlce is outside the rectangle
		else
		{
			AEVec2 clamPoint;

			// Clamp the cirle's center
			clamPoint.x = Clamp(Center->x, Rect->x - Width / 2.0f, Rect->x + Width / 2.0f);
			clamPoint.y = Clamp(Center->y, Rect->y - Height / 2.0f, Rect->y + Height / 2.0f);

			// Calculate the normal
			pResult->mNormal = ((*Center) - clamPoint).Normalize();

			// Calculate the penetration
			pResult->mPenetration = Radius - Center->Distance(clamPoint);

			// In this case, the point of intersection is the clamped center
			pResult->mPi = clamPoint;
		}

		return true; // Done, both shapes are overlaping with each other
	}
	// @TODO
	//! ---------------------------------------------------------------------------
	// \fn		StaticOBBToStaticCircleEx
	// \brief	Checks overlap between and oriented box and a circle.If an overlap exists, 
	// 			it stores the contact data into pResult.
	//
	// \details	
	//			- You should use StaticrectToStaticCircleEx.
	//			- You must compute ALL the contact data.
	//			- You should check for pResult not being equal to NULL.
	//			- You must assume that the contact viewer is the OBB. (i.e. normal goes from 
	//			  Box to Circle).
	// 
	//  \return	true if the shapes overlap, false otherwise
	// ---------------------------------------------------------------------------
	bool StaticOBBToStaticCircleEx(Transform * OBB, AEVec2 * Center, float Radius, Contact * pResult)
	{
		// Check if the two shapes are not overlaping
		if (!OrientedRectToStaticCirlce(&(OBB->mTranslation), OBB->mScale.x, OBB->mScale.y, OBB->mOrientation, Center, Radius))
		{
			return false; // Done, not overlaping
		}

		// Check if the Contact pointer is not NULL
		if (pResult == NULL)
		{
			return true; // Done, both shapes are overlaping with each other
		}

		// Create a transform to get the WorldToLocal and LocalToWorld matrices
		// mScale is 1 to avoid losing the width and height of the shapes
		Transform RectTransform(OBB->mTranslation, { 1.0f, 1.0f }, OBB->mOrientation);

		// Calculate the local to world matrix
		AEMtx33 LocalToWorld = RectTransform.GetMatrix();

		// Calculate the world to local matrix
		AEMtx33 WorldToLocal = RectTransform.GetInvMatrix();

		// The center of the rectangle in local space
		AEVec2 Rect = { 0.0f, 0.0f };

		// Make a copy of the center of the circle
		AEVec2 circleCenter = *Center;

		// Transform the circle to the rectangle's local space
		circleCenter = WorldToLocal * circleCenter;

		// Fill the contact data in the rectangle's local space
		StaticRectToStaticCircleEx(&Rect, OBB->mScale.x, OBB->mScale.y, &circleCenter, Radius, pResult);

		// Transform the normal to the world space without translating
		// Basically, just rotate the vector
		pResult->mNormal = LocalToWorld.MultVec(pResult->mNormal);

		// Transform the point of intersection to world space
		pResult->mPi = LocalToWorld * pResult->mPi;

		return true; // Done, both shapes are overlaping with each other
	}

	// @TODO
	//! ---------------------------------------------------------------------------
	// \fn		StaticRectToStaticRectEx
	// \brief	Checks overlap between two AABB , using the simplified Separating Axis
	// 			Theorem described in class. If an overlap exists, it stores the 
	// 			contact data into pResult.
	//
	// \details	
	//			- You must compute ALL the contact data.
	//			- You should check for pResult not being equal to NULL.
	//			- To determine the point of collision, use the following logic:
	//				* check if any corner of box2 is inside of box1, if so use 
	//				  it as the point of interesection. 
	//				* otherwise, check if any corner of box1 is inside of box2, use
	//				  it as the point of intersection.
	// 
	//  \return	true if the shapes overlap, false otherwise
	// ---------------------------------------------------------------------------
	bool StaticRectToStaticRectEx(AEVec2 *pos1, AEVec2 *size1, AEVec2 *pos2, AEVec2 *size2, Contact * pResult)
	{
		// Check if the rectangles are overlaping
		if (StaticRectToStaticRect(pos1, size1->x, size1->y, pos2, size2->x, size2->y) == false)
		{
			return false; // Done, the rectangles are not overlaping
		}

		if (pResult == NULL)
		{
			return true; // Done, the rectangles are overlaping
		}

		// Create a vector that goes from rectangle 1 to rectangle 2
		AEVec2 RectToRectVec = (*pos2) - (*pos1);

		// Make a copy of the vector
		AEVec2 RectToRectVecABS = RectToRectVec;

		// Calculate the absolute value of the vector
		if (RectToRectVecABS.x < 0.0f)	RectToRectVecABS.x *= -1.0f;
		if (RectToRectVecABS.y < 0.0f)	RectToRectVecABS.y *= -1.0f;

		AEVec2 penetration;

		// Calculate the penetration in the x and y axis
		penetration.x = (size1->x + size2->x) / 2.0f - RectToRectVecABS.x;
		penetration.y = (size1->y + size2->y) / 2.0f - RectToRectVecABS.y;

		// Check if the penetration of x is the minimum one
		if (penetration.x < penetration.y)
		{
			pResult->mPenetration = penetration.x;

			// Calculate the normal
			pResult->mNormal = { 1.0f, 0.0f };
		}

		// Otherwise, the penetration of y is the minimum one
		else
		{
			pResult->mPenetration = penetration.y;

			// Calculate the normal
			pResult->mNormal = { 0.0f, 1.0f };
		}

		// Check if the normal is pointing in the wrong direction
		if (RectToRectVec * pResult->mNormal < 0.0f)
		{
			// Flip the normal
			pResult->mNormal *= -1.0f;
		}

		// Calculate Width/2 and Height/2 of both rectangles
		float offSetX1 = size1->x / 2.0f;
		float offSetY1 = size1->y / 2.0f;
		float offSetX2 = size2->x / 2.0f;
		float offSetY2 = size2->y / 2.0f;

		// Create an array that contains the 4 vertices of each rectangle
		AEVec2 Vertices[8] = {

			// Rect2
			{pos2->x - offSetX2, pos2->y + offSetY2}, {pos2->x + offSetX2, pos2->y + offSetY2},
			{pos2->x - offSetX2, pos2->y - offSetY2}, {pos2->x + offSetX2, pos2->y - offSetY2},

			// Rect1
			{pos1->x - offSetX1, pos1->y + offSetY1}, {pos1->x + offSetX1, pos1->y + offSetY1},
			{pos1->x - offSetX1, pos1->y - offSetY1}, {pos1->x + offSetX1, pos1->y - offSetY1}
		};

		// Go through all the array
		for (int i = 0; i < 8; ++i)
		{
			// Check if the current vertice is of the first rectangle
			if (i < 4)
			{
				// Check if the point is contained in the second rectangle
				if (StaticPointToStaticRect(&Vertices[i], pos1, size1->x, size1->y))
				{
					// Set the point of intersection
					pResult->mPi = Vertices[i];

					return true; // Done, the rectangles overlap
				}
			}

			// Otherwise, the vertice is of the second rectangle
			else
			{
				// Check if the point is contained in the first rectangle
				if (StaticPointToStaticRect(&Vertices[i], pos2, size2->x, size2->y))
				{
					// Set the point of intersection
					pResult->mPi = Vertices[i];

					return true; // Done, the rectangles overlap
				}
			}
		}

		return true;
	}

	// @TODO
	//! ---------------------------------------------------------------------------
	// \fn		OrientedRectToOrientedRectEx
	// \brief	Checks overlap between two oriented box, using the Separating Axis
	// 			Theorem described in class. If an overlap exists, it stores the 
	// 			contact data into pResult.
	//
	// \details	
	//			- You must compute ALL the contact data.
	//			- You should check for pResult not being equal to NULL
	// 
	//  \return	true if the shapes overlap, false otherwise
	// ---------------------------------------------------------------------------
	bool OrientedRectToOrientedRectEx(Transform * OBB1, Transform * OBB2, Contact * pResult)
	{
		// Calculate the half extents of both OBBs
		AEVec2 halfExtent_X_OBB1 = AEMtx33::RotRad(OBB1->mOrientation) * AEVec2(OBB1->mScale.x / 2.0f, 0);
		AEVec2 halfExtent_X_OBB2 = AEMtx33::RotRad(OBB2->mOrientation) * AEVec2(OBB2->mScale.x / 2.0f, 0);
		AEVec2 halfExtent_Y_OBB1 = AEMtx33::RotRad(OBB1->mOrientation) * AEVec2(0, OBB1->mScale.y / 2.0f);
		AEVec2 halfExtent_Y_OBB2 = AEMtx33::RotRad(OBB2->mOrientation) * AEVec2(0, OBB2->mScale.y / 2.0f);

		// Calculate the axis to test the SAT theorem
		// The axis vectors are normalized
		AEVec2 axis_1_OBB1 = halfExtent_X_OBB1.Normalize();
		AEVec2 axis_2_OBB1 = halfExtent_Y_OBB1.Normalize();
		AEVec2 axis_1_OBB2 = halfExtent_X_OBB2.Normalize();
		AEVec2 axis_2_OBB2 = halfExtent_Y_OBB2.Normalize();

		// Add the half extents to an array
		AEVec2 halfExtents[4] = { halfExtent_X_OBB1, halfExtent_Y_OBB1, halfExtent_X_OBB2, halfExtent_Y_OBB2 };

		// Add the axis to an array
		AEVec2 axis[4] = { axis_1_OBB1, axis_2_OBB1, axis_1_OBB2, axis_2_OBB2 };

		// Project the distance between the two OBBs in the current axis
		float OBBs_Dist;

		// Stores the minimum penetration
		float minPenetration;

		// Stores the vector of the axis with minimum penetration
		AEVec2 minPentrationAxis;

		// Test the SAT with the 4 axis
		for (int i = 0; i < 4; ++i)
		{
			// Project the distance between the two OBBs in the current axis
			OBBs_Dist = ((OBB2->mTranslation) - (OBB1->mTranslation)) * axis[i];

			// Take the absolute value of the distance
			if (OBBs_Dist < 0)	OBBs_Dist *= -1.0f;

			// Store the sum of the projections of the half extents of both OBBs
			float projectedHalfExtents = 0;

			// Project the 4 half extents onto the current axis
			for (int j = 0; j < 4; ++j)
			{
				// Calculate the value of the projection of the current half extent
				float projection = halfExtents[j] * axis[i];

				// Take the absolute value
				if (projection < 0)	projection *= -1.0f;

				// Add the projection
				projectedHalfExtents += projection;
			}

			// Check if the current axis is a separating axis
			if (OBBs_Dist > projectedHalfExtents)
			{
				return false; // Done, the OBBs are not overlaping
			}

			else
			{
				// Calculate the penetration in the current axis
				float currentPenetration = projectedHalfExtents - OBBs_Dist;

				// Check if it is not the first iteration
				if (i != 0)
				{
					if (currentPenetration < minPenetration)
					{
						// Update the minimum penetration and its corresponding axis
						minPenetration = currentPenetration;
						minPentrationAxis = axis[i];
					}
				}

				else
				{
					// Initialize the variables for the first iteration
					minPenetration = currentPenetration;
					minPentrationAxis = axis[i];
				}
			}
		}

		if (pResult == NULL)
		{
			return true; // Done, the OBBs are overlaping
		}

		// Set the penetration
		pResult->mPenetration = minPenetration;

		// Check if the normal is pointing in the opposite direction
		if (minPentrationAxis * ((OBB2->mTranslation) - (OBB1->mTranslation)) < 0)	minPentrationAxis *= -1.0f;

		// Set the normal
		pResult->mNormal = minPentrationAxis;

		// Create a transform to get the WorldToLocal and LocalToWorld matrices
		// mScale is 1 to avoid losing the width and height of the shapes
		Transform OBB1Transform(OBB1->mTranslation, { 1.0f, 1.0f }, OBB1->mOrientation);
		Transform OBB2Transform(OBB2->mTranslation, { 1.0f, 1.0f }, OBB2->mOrientation);

		// Calculate the local to world matrix
		AEMtx33 OBB1LocalToWorld = OBB1Transform.GetMatrix();
		AEMtx33 OBB2LocalToWorld = OBB2Transform.GetMatrix();

		// Calculate Width/2 and Height/2 of both rectangles
		float offSetX1 = OBB1->mScale.x / 2.0f;
		float offSetY1 = OBB1->mScale.y / 2.0f;
		float offSetX2 = OBB2->mScale.x / 2.0f;
		float offSetY2 = OBB2->mScale.y / 2.0f;

		// Create an array that contains the 4 vertices of each rectangle
		AEVec2 Vertices[8] = {

			// OBB2
			OBB2LocalToWorld * AEVec2(-offSetX2,  offSetY2), OBB2LocalToWorld * AEVec2(offSetX2,  offSetY2),
			OBB2LocalToWorld * AEVec2(-offSetX2, -offSetY2), OBB2LocalToWorld * AEVec2(offSetX2, -offSetY2),

			// OBB1
			OBB1LocalToWorld * AEVec2(-offSetX1,  offSetY1), OBB1LocalToWorld * AEVec2(offSetX1,  offSetY1),
			OBB1LocalToWorld * AEVec2(-offSetX1, -offSetY1), OBB1LocalToWorld * AEVec2(offSetX1, -offSetY1)
		};

		// Go through all the array
		for (int i = 0; i < 8; ++i)
		{
			// Check if the current vertice is of OBB2
			if (i < 4)
			{
				// Check if the point is contained in the second rectangle
				if (StaticPointToOrientedRect(&Vertices[i], &(OBB1->mTranslation), OBB1->mScale.x, OBB1->mScale.y, OBB1->mOrientation))
				{
					// Set the point of intersection
					pResult->mPi = Vertices[i];

					return true; // Done, the OBBs overlap
				}
			}

			// Otherwise, the vertice is of the OBB1
			else
			{
				// Check if the point is contained in the first rectangle
				if (StaticPointToOrientedRect(&Vertices[i], &(OBB2->mTranslation), OBB2->mScale.x, OBB2->mScale.y, OBB2->mOrientation))
				{
					// Set the point of intersection
					pResult->mPi = Vertices[i];

					return true; // Done, the OBBs overlap
				}
			}
		}

		return true;
	}


	// @TODO @EXTRA_CREDIT
	//! ---------------------------------------------------------------------------
	// \fn		PolygonToPolygon
	// \brief	Checks overlap between two convex polygons, using the Separating Axis
	// 			Theorem described in class. If an overlap exists, it stores the 
	// 			contact data into pResult.
	//
	// \details	
	//			- IGNORE the point of intersection, only compute penetration and
	//			  contact normal.
	//			- You should check for pResult not being equal to NULL
	// 
	//  \return	true if the shapes overlap, false otherwise
	// ---------------------------------------------------------------------------
	bool PolygonToPolygon(Polygon2D * p1, Transform * tr1, Polygon2D * p2, Transform * tr2, Contact * pResult)
	{
		// Create a vector that contains the transformed vertices of the first polygon
		auto verticesP1 = p1->GetTransformedVertices(tr1->GetMatrix());

		// Store the number of sides of the first polygon
		int p1Sides = p1->GetSize();

		// Store the number of sides of the second polygon
		int p2Sides = p2->GetSize();

		// Create vector that will store the normals of the first and second polygon
		AEVec2 * normals = new AEVec2[p1Sides + p2Sides];

		// Calculate the normals of the first polygon
		for (int i = 0; i < p1Sides; ++i)
		{
			// Check if the current vertice is the last one
			if (i == p1Sides - 1)
			{
				normals[i] = (verticesP1[0] - verticesP1[i]).Perp().NormalizeThis();
			}

			else
			{
				normals[i] = (verticesP1[i + 1] - verticesP1[i]).Perp().NormalizeThis();
			}
		}

		// Create a vector that contains the transformed vertices of the second polygon
		auto verticesP2 = p2->GetTransformedVertices(tr2->GetMatrix());

		// Calculate the normals of the second polygon and add them to the normals array
		for (int i = 0; i < p2Sides; ++i)
		{
			// Check if the current vertice is the last one
			if (i == p2Sides - 1)
			{
				normals[p1Sides + i] = (verticesP2[0] - verticesP2[i]).Perp().NormalizeThis();
			}

			else
			{
				normals[p1Sides + i] = (verticesP2[i + 1] - verticesP2[i]).Perp().NormalizeThis();
			}
		}

		// Stores the minimum penetration
		float minimumPenetration;

		// Stores the axis of minimum penetration
		AEVec2 minimumPenetrationAxis;

		// Stores the min and max projected point of p1
		float minP1;
		float maxP1;

		// Stores the min and max projected point of p2
		float minP2;
		float maxP2;

		// Test the SAT with the normals of the first and second polygon
		for (int i = 0; i < p1Sides + p2Sides; ++i)
		{
			// Project all the vertices of p1 onto the normal
			for (int j = 0; j < p1Sides; ++j)
			{
				float currentProjection = normals[i] * verticesP1[j];

				// Check if it is the first projected vertice
				if (j == 0)
				{
					// Initialize the variable
					minP1 = currentProjection;
					maxP1 = currentProjection;

					continue;
				}

				// Check if the current projection is the biggest
				if (currentProjection > maxP1)
				{
					maxP1 = currentProjection;
				}

				// Otherwise, check if the current projection is the smallest
				else if (currentProjection < minP1)
				{
					minP1 = currentProjection;
				}
			}

			// Project all the vertices of p2 onto the normal
			for (int k = 0; k < p2Sides; ++k)
			{
				float currentProjection = normals[i] * verticesP2[k];

				// Check if it is the first projected vertice
				if (k == 0)
				{
					// Initialize the variable
					minP2 = currentProjection;
					maxP2 = currentProjection;

					continue;
				}

				// Check if the current projection is the biggest
				if (currentProjection > maxP2)
				{
					maxP2 = currentProjection;
				}

				// Otherwise, check if the current projection is the smallest
				else if (currentProjection < minP2)
				{
					minP2 = currentProjection;
				}
			}

			// Check if it is a separating axis
			if (maxP1 < minP2 || maxP2 < minP1)
			{
				// Free the array of normals
				delete normals;
				return false; // Done, the polygons don't overlap
			}

			// Calculate both possible overlaps
			float overlap1 = maxP1 - minP2;
			float overlap2 = maxP2 - minP1;

			// Store the minimum overlap
			float minimum_overlap;

			// Check for the minimum overlap
			if (overlap1 <= overlap2)
			{
				minimum_overlap = overlap1;
			}

			else
			{
				minimum_overlap = overlap2;
			}

			// Check if it is the first axis we are projecting onto
			if (i == 0)
			{
				// Initialize the variables
				minimumPenetration = minimum_overlap;
				minimumPenetrationAxis = normals[i];
			}

			// Otherwise
			else
			{
				// Check if the current overlap is the minimum one so far
				if (minimum_overlap < minimumPenetration)
				{
					minimumPenetration = minimum_overlap;
					minimumPenetrationAxis = normals[i];
				}
			}
		} // end of loop

		if (pResult == NULL)
		{
			// Free the array of normals
			delete normals;
			return true; // Done, the polygons overlap
		}

		// Create a vertor from p1 to p2
		AEVec2 P1_P2_dist = tr2->mTranslation - tr1->mTranslation;

		// Check if the normal is pointing to the opposite direction
		if (minimumPenetrationAxis * P1_P2_dist < 0)
		{
			minimumPenetrationAxis *= -1.0f;
		}

		// Set the penetration
		pResult->mPenetration = minimumPenetration;

		// Set the normal
		pResult->mNormal = minimumPenetrationAxis;

		// Free the array of normals
		delete normals;

		return true; // Done, the polygons overlap
	}
	
	bool Contact::operator==(const Contact & c) const
	{
		return (mBody1 == c.mBody1 && mBody2 == c.mBody2 || mBody1 == c.mBody2 && mBody2 == c.mBody1);
	}
}