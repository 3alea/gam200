// ----------------------------------------------------------------------------
//	Copyright (C)DigiPen Institute of Technology.
//	Reproduction or disclosure of this file or its contents without the prior 
//	written consent of DigiPen Institute of Technology is prohibited.
//	
//	File Name:		Polygon2D.cpp
//	Purpose:		Contains the definitions of the memember functions of the
//                  class polygon
//	Project:		CS230_diego.sanz_3
//	Author:			Diego Sanz / diego.sanz / 540001618
// ----------------------------------------------------------------------------

#include "Polygon2D.h"

namespace AEX
{
	/*! @PROVIDED
	*	\brief	Draws the polygon with the provided color and transform.
	*/
	void Polygon2D::Draw(u32 color, AEMtx33 * vtx_transform) const
	{
		// can't draw polygon if we don't have at least
		// 2 vertices
		if (mVertices.size() <= 1)
			return;

		// set transform if necessary transform 
		if (vtx_transform != NULL) {
			AEMtx33 mat = *vtx_transform;
			// AEGfxSetTransform(&mat); // Solve with Alejandro
		}

		// Draw line between each vertices
		for (u32 i = 0; i < mVertices.size() - 1; ++i)
		{
			auto & v1 = mVertices[i];
			auto & v2 = mVertices[i + 1];
			// AEGfxLine(v1.x, v1.y, 0, color, v2.x, v2.y, 0, color); // Solve with Alejandro
		}
		// Draw last line from last to first vertex
		auto & v1 = mVertices[0];
		auto & v2 = mVertices[mVertices.size() - 1];
		// AEGfxLine(v1.x, v1.y, 0, color, v2.x, v2.y, 0, color); // Solve with Alejandro

		// force draw on graphics system
		// AEGfxFlush(); // Solve with Alejandro
	}


	// ------------------------------------------------------------------------
	// STATIC INTERFACE
	// ------------------------------------------------------------------------
	/*! @PROVIDED
	*	\brief	Creates a quad polygon.
	*/
	Polygon2D Polygon2D::MakeQuad()
	{
		// result polygon
		Polygon2D res(4);

		// create vertices
		res[0] = { -0.5f, 0.5f };
		res[1] = { -0.5f, -0.5f };
		res[2] = { 0.5f, -0.5f };
		res[3] = { 0.5f, 0.5f };

		return res;
	}

	/*! @PROVIDED
	*	\brief	Creates a 5-sided polygon.
	*/
	Polygon2D Polygon2D::MakePentagon()
	{
		return MakeStandardPoly(5);
	}

	/*! @PROVIDED
	*	\brief	Creates a 6-sided polygon.
	*/
	Polygon2D Polygon2D::MakeHexagon()
	{
		return MakeStandardPoly(6);
	}

	/*! @PROVIDED
	*	\brief	Creates a 7-sided polygon.
	*/
	Polygon2D Polygon2D::MakeSeptagon()
	{
		return MakeStandardPoly(7);
	}

	/*! @PROVIDED
	*	\brief	Creates a 8-sided polygon.
	*/
	Polygon2D Polygon2D::MakeOctagon()
	{
		return MakeStandardPoly(8);
	}

	/*! @PROVIDED
	*	\brief	Creates a n-sided polygon.
	*/
	Polygon2D Polygon2D::MakeStandardPoly(u32 side)
	{
		Polygon2D res(side);
		res[0] = { 0.5f, 0.0f };
		f32 alpha = (2.0f*PI) / (f32)side;
		for (u32 i = 1; i < side; ++i) {

			// set current point to previous
			// before rotation
			res[i] = res[i - 1];

			// compute rotation vars
			f32 tmp = res[i].x;
			f32 cA = cosf(alpha);
			f32 sA = sinf(alpha);

			// apply rotation to get new point
			res[i].x = cA * res[i].x - sA * res[i].y;
			res[i].y = sA * tmp + cA * res[i].y;
		}
		return res;
	}

	/*!	@TODO
	*	\brief	Default constructor taking the size of the polygon.
	*	\remark	Does nothing.
	*/
	Polygon2D::Polygon2D()
	{
	}

	/*!	@TODO
	*	\brief	Custom constructor taking the size of the polygon.
	*	\remark	This function should call the SetSize method below.
	*/
	Polygon2D::Polygon2D(u32 size)
	{
		SetSize(size);
	}


	/*!	@TODO
	*	\brief	Adds a vertex to the polygon.
	*	\remark	The vertex should be added at the end of the polygon. Use the
	*			push_back() method of std::vector.
	*/
	void Polygon2D::AddVertex(const AEVec2 & vtx)
	{
		mVertices.push_back(vtx);
	}

	/*!	@TODO
	*	\brief	returns the vertex at the index specifed
	*	\remark	This function shouldn't have to do any sanity check as it returns by
	*			reference. If a erroneous index is passed. An error will be thrown.
	*/
	AEVec2 & Polygon2D::operator[](u32 idx)
	{
		// TODO: insert return statement here
		return mVertices[idx];
	}

	/*!	@TODO
	*	\brief	Removes all vertices in the polygon
	*	\remark	Use the clear method of std::vector. No need for more.
	*/
	void Polygon2D::Clear()
	{
		mVertices.clear();
	}

	/*!	@TODO
	*	\brief	Returns the size of the mVertices array
	*	\remark	Use the size method of std::vector. No need for more.
	*/
	u32 Polygon2D::GetSize() const
	{
		return mVertices.size();
	}

	/*! @TODO
	*	\brief	Sets the size of the mVertices array
	*	\remark	Use the resize method of std::vector. No need for more.
	*/
	void Polygon2D::SetSize(u32 size)
	{
		mVertices.resize(size);
	}

	/*! @TODO
	*	\brief	Returns a copy of the vertices transformed by the provided matrix
	*/
	std::vector<AEVec2> Polygon2D::GetTransformedVertices(const AEMtx33 & mat_transform) const
	{
		// Create a vector of vectors
		std::vector<AEVec2> transformedV;

		// Create an iterator to go through the vector of vertices
		std::vector<AEVec2>::const_iterator it;

		// Create an iterator that points to the last element of the vector of vertices
		std::vector<AEVec2>::const_iterator end = mVertices.end();

		// Go through the vector of vertices
		for (it = mVertices.begin(); it != end; ++it)
		{
			// Add each transformed vertice to the new vector
			transformedV.push_back(mat_transform * (*it));
		}

		// Return the new vector
		return transformedV;
	}
}
