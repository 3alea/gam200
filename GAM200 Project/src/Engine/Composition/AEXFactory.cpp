#include <string>
#include <map>
#include "src/Engine/Core/AEXRtti.h"
#include "AEXFactory.h"
#include "Composition/AEXSpace.h"
#include "Components/Renderable.h"
#include "Components/SpriteComps.h"
#include "Components/SpriteText.h"
#include "Components/Rigidbody.h"
#include "Components/Collider.h"
#include "Components/ParticleEmitter.h"
#include "src/Gameplay/Enemy/EnemyGrounded.h"
#include "src/Gameplay/Enemy/EnemyTurret.h"
#include "src/Gameplay/Enemy/EnemyBugger.h"
#include "src/Gameplay/Enemy/EnemyKamikaze.h"
#include "src/Gameplay/Enemy/EnemyFlying.h"
#include "src/Gameplay/Enemy/FinalBoss/FinalBoss.h"
#include "src/Gameplay/Activator/EnemyActivator.h"
#include "Activator\AcidActivator.h"
#include "Environment/StomachBackgroundController/BGController.h"
#include "Environment/StomachBackgroundController/FingerController.h"
#include "Environment/StomachBackgroundController/PipeController.h"
#include "src/Gameplay/Enemy/EnemySpawner.h"
#include "Player/Stats/PlayerStats.h"
#include "Player/Movement/PlayerController.h"
#include "Follow/Follower.h"
#include "Follow/CameraLimits.h"
#include "Follow/DestroyFollowerActivator.h"
//#include "Follow/CameraAxisLocker.h"
#include "Player/Movement/FootLogic.h"
#include "Environment/PlatformController.h"
#include "Environment\LevelLoader.h"
#include "Activator\LevelChanger.h"
#include "Environment/Checkpoint.h"
#include "Environment/VeinsPlatforms/VeinsPlatforms.h"
#include "Environment/PlatformOnAboveActivator.h"
#include "Activator\ActivateParticlesWithFloor.h"
#include "Dialogue/DialogueSystem.h"
#include "HUD/BossBarLogic.h"
#include "Enemy/FinalBoss/FinalWave.h"
#include "HUD/ComboLogic.h"
#include "Environment/Alveolus.h"
#include "Activator/CinematicActivator.h"
#include "Activator/FadeInOutActivator.h"
#include "Environment/Elevator/Elevator.h"
#include "BoilerRoom/PuzzleLogic.h"

// HUD files
#include "HUD\Heart.h"
#include "HUD\LiveController.h"
#include "HUD\Meter.h"
#include "HUD\HUDFader.h"
// Fade out
#include "Menu\Fader\Fader.h"
#include "Menu/Fader/FaderIn.h"

#include "Player/Mechanics/ShootController.h"
#include "Bullets/BulletLogic.h"
#include "Player\Mechanics\GrenadeThrow.h"
#include "Bullets\Grenade.h"
#include "Bullets/Explosion.h"
#include "Player/Mechanics/DashLogic.h"
#include "Components/IgnoreGravityEffect.h"
#include "Components\SpineAnimation.h"
#include "Components\RunningTrack.h"
#include "Components/Parallax.h"
#include "src/Engine/Audio/AudioManager.h"
#include "Composition/ComponentList.h"
#include "AI/Path2D.h"
#include "Enemy/EnemyMitosis.h"
#include "Environment/FloatingPlatform/FloatingPlatformLogic.h"
#include "Environment/Smasher/SmasherLogic.h"
#include "Follow/FollowAnObject.h"
#include "src/Gameplay/Environment/BoxSpawner/BoxSpawner.h"
#include "src/Gameplay/Environment/AcidController.h"
#include "Composition/EditorUpdateable.h"
#include "src/Gameplay/Environment/AirRisingMechanic/RisingAir.h"
#include "src\Gameplay\Environment\Snot.h"
#include "Menu/MenuIncludes.h"
#include "Menu/PauseMenu/PauseMenuLogic.h"
#include "Player\Movement\WalkOnSlope.h"
#include "Environment\Background\BackgroundMover.h"
#include "Environment\Background\VeinMovingPlatform.h"
#include "Environment/Background/MovingBackgroundActivator.h"
#include "Environment\Background\StationZoomOut.h"
#include "Follow/ChaseObject.h"
#include "Follow\ParentToCamera.h"
#include "Environment\Cholesterol.h"
#include "CameraLogic\CameraChanger.h"
#include "CameraLogic/CameraShaker.h"
#include "Activator/CarsActivators.h"
#include "Enemy/WaveSystem/WaveSystem.h"
#include "Enemy/WaveSystem/Spawner.h"
#include "Activator/KillerCarsActivator.h"
#include "Activator/KillerCar.h"
#include "Cholesterol/BlockedCar.h"
// BoxLogic
#include "Environment/BoxSpawner/Box.h"

#include "Enemy/WaveSystem/BoilerRoomWaveSystem.h"
#include "BoilerRoom/GearsLogic.h"
#include "BoilerRoom/HeartBeat.h"
#include "BoilerRoom/EnemyActivation.h"

namespace AEX
{
	/*Factory::Factory() : ISystem() {}*/

	bool Factory::Initialize()
	{
		aexFactory->Register<Space>();
		aexFactory->Register<GameObject>();

		aexFactory->Register<Font>();

		// ---------------------------REGISTER OF ALL COMPONENTS----------------------------------------------------
		// IMPORTANT: If you want the component to be able to be added to
		// a gameobject, you will need to do the same as in the following example
		aexFactory->Register<TransformComp>();
		aexCompNameList->AddCompName<TransformComp>("Transform");	// --> "Transform" (in this case) will be
																	// the name that will appear in the editor
																	// when you click on AddComponent

		// GRAPHICS COMPONENTS
		// We need to register Renderable to be able to instantiate it, but we don't
		// need to add it aexCompNameList, because we don't want the user adding
		// explicitly a renderable component in the editor.
		aexFactory->Register<Renderable>();
		
		aexFactory->Register<Camera>();
		aexCompNameList->AddCompName<Camera>("Camera");
		aexFactory->Register<SpriteComp>();
		aexCompNameList->AddCompName<SpriteComp>("Sprite");
		aexFactory->Register<SpriteAnimationComp>();
		aexCompNameList->AddCompName<SpriteAnimationComp>("Sprite Animation");
		aexFactory->Register<SpineAnimationComp>();
		aexCompNameList->AddCompName<SpineAnimationComp>("Spine Animation");
		aexFactory->Register<SpriteText>();
		aexCompNameList->AddCompName<SpriteText>("Sprite Text");
		aexFactory->Register<ParticleEmitter>();
		aexCompNameList->AddCompName<ParticleEmitter>("Particle Emitter");
		aexFactory->Register<ActivateParticles>();
		aexCompNameList->AddCompName<ActivateParticles>("Particle activator");

		// PHYSICS/COLLISIONS COMPONENTS
		aexFactory->Register<ICollider>();

		aexFactory->Register<CircleCollider>();
		aexCompNameList->AddCompName<CircleCollider>("Circle Collider");
		aexFactory->Register<BoxCollider>();
		aexCompNameList->AddCompName<BoxCollider>("Box Collider");
		aexFactory->Register<Rigidbody>();
		aexCompNameList->AddCompName<Rigidbody>("Rigidbody");
		aexFactory->Register<IgnoreGravityEffect>();
		aexCompNameList->AddCompName<IgnoreGravityEffect>("Ignore Gravity Effect");


		// AUDIO COMPONENTS
		aexFactory->Register<SoundEmitter>();
		aexCompNameList->AddCompName<SoundEmitter>("Sound Emitter");
		aexFactory->Register<SoundListener>();
		aexCompNameList->AddCompName<SoundListener>("Sound Listener");

		// GAMEPLAY COMPONENTS
		aexFactory->Register<EnemyActivator>();
		aexCompNameList->AddCompName<EnemyActivator>("Enemy Activator");
		aexFactory->Register<VeinsPlatforms>();
		aexCompNameList->AddCompName<VeinsPlatforms>("Vein platform");
		aexFactory->Register<KillerCarsActivator>();
		aexCompNameList->AddCompName<KillerCarsActivator>("Killer Cars Activator");
		aexFactory->Register<KillerCar>();
		aexCompNameList->AddCompName<KillerCar>("Killer Car");
		aexFactory->Register<AlveolusLogic>();
		aexCompNameList->AddCompName<AlveolusLogic>("Alveolus Logic");

		// Acid Activator (riser)
		aexFactory->Register<AcidActivator>();
		aexCompNameList->AddCompName<AcidActivator>("Acid Activator");

		// Finger controller (riser)
		aexFactory->Register<FingerController>();
		aexCompNameList->AddCompName<FingerController>("Finger Controller");

		// Acid Background controller (riser)
		aexFactory->Register<BGController>();
		aexCompNameList->AddCompName<BGController>("Acid Background Controller");

		// Pipe controller (riser)
		aexFactory->Register<PipeController>();
		aexCompNameList->AddCompName<PipeController>("Pipe Controller");

		aexFactory->Register<CarsActivator>();
		aexCompNameList->AddCompName<CarsActivator>("Cars Activator");

		aexFactory->Register<CinematicActivator>();
		aexCompNameList->AddCompName<CinematicActivator>("cinematic activator");

		aexFactory->Register<FadeInOutActivator>();
		aexCompNameList->AddCompName<FadeInOutActivator>("fadeInOut activator");

		aexFactory->Register<Elevator>();
		aexCompNameList->AddCompName<Elevator>("elevator");

		aexFactory->Register<CameraLimitActivator>();
		aexCompNameList->AddCompName<CameraLimitActivator>("Camera Limit Activator");
		aexFactory->Register<DestroyFollowerActivator>();
		aexCompNameList->AddCompName<DestroyFollowerActivator>("Destroy Follower Activator");

		// CHOLESTEROL
		aexFactory->Register<CholesterolLogic>();
		aexCompNameList->AddCompName<CholesterolLogic>("Cholesterol Logic");

		aexFactory->Register<LevelChanger>();
		aexCompNameList->AddCompName<LevelChanger>("Level Changer");

		aexFactory->Register<EnemyGrounded>();
		aexCompNameList->AddCompName<EnemyGrounded>("Enemy Grounded");
		aexFactory->Register<EnemyTurret>();
		aexCompNameList->AddCompName<EnemyTurret>("Enemy Turret");
		aexFactory->Register<EnemyFlying>();
		aexCompNameList->AddCompName<EnemyFlying>("Enemy Flying");
		aexFactory->Register<EnemyKamikaze>();
		aexCompNameList->AddCompName<EnemyKamikaze>("Enemy Kamikaze");
		aexFactory->Register<EnemySpawner>();
		aexCompNameList->AddCompName<EnemySpawner>("Enemy Spawner");
		aexFactory->Register<EnemyBugger>();
		aexCompNameList->AddCompName<EnemyBugger>("Enemy Bugger");
		aexFactory->Register<EnemyMitosis>();
		aexCompNameList->AddCompName<EnemyMitosis>("Enemy Mitosis");
		aexFactory->Register<PlayerStats>();
		aexCompNameList->AddCompName<PlayerStats>("Player Stats");
		aexFactory->Register<PlayerController>();
		aexCompNameList->AddCompName<PlayerController>("Player Controller");
		aexFactory->Register<Follower>();
		aexCompNameList->AddCompName<Follower>("Follower");
		//aexFactory->Register<CameraAxisLocker>();
		//aexCompNameList->AddCompName<CameraAxisLocker>("CameraAxisLocker");
		aexFactory->Register<PlatformController>();
		aexCompNameList->AddCompName<PlatformController>("Platform Controller");
		aexFactory->Register<ShootController>();
		aexCompNameList->AddCompName<ShootController>("Shoot Controller");
		aexFactory->Register<BulletLogic>();
		aexCompNameList->AddCompName<BulletLogic>("Bullet Logic");
		aexFactory->Register<RisingAir>();
		aexCompNameList->AddCompName<RisingAir>("Rising Air");
		aexFactory->Register<Snot>();
		aexCompNameList->AddCompName<Snot>("Snot");
		aexFactory->Register<WalkOnSlope>();
		aexCompNameList->AddCompName<WalkOnSlope>("Walk On Slopes");
		aexFactory->Register<FinalBoss>();
		aexCompNameList->AddCompName<FinalBoss>("FinalBoss");
		aexFactory->Register<PunchLogic>();
		aexCompNameList->AddCompName<PunchLogic>("PunchLogic");
		aexFactory->Register<FinalWave>();
		aexCompNameList->AddCompName<FinalWave>("FinalWave");

		aexFactory->Register<LifeTime>();
		aexCompNameList->AddCompName<LifeTime>("LifeTime");
		aexFactory->Register<BossBulletStruct>();
		aexCompNameList->AddCompName<BossBulletStruct>("BossBulletStruct");

		// CAMERA COMPS
		aexFactory->Register<CameraChanger>();
		aexCompNameList->AddCompName<CameraChanger>("Camera Changer");
		aexFactory->Register<CameraShaker>();
		aexCompNameList->AddCompName<CameraShaker>("Camera Shaker");
		aexFactory->Register<CameraMovement>();
		aexCompNameList->AddCompName<CameraMovement>("Camera Movement");
		aexFactory->Register<BlockedCar>();
		aexCompNameList->AddCompName<BlockedCar>("Blocked Car");

		// Grenade throw controller comp
		aexFactory->Register<GrenadeThrow>();
		aexCompNameList->AddCompName<GrenadeThrow>("Grenade Throw");

		// Grenade logic component
		aexFactory->Register<GrenadeLogic>();
		aexCompNameList->AddCompName<GrenadeLogic>("Grenade Logic");

		// Explosion logic component
		aexFactory->Register<ExplosionLogic>();
		aexCompNameList->AddCompName<ExplosionLogic>("Explosion Logic");

		aexFactory->Register<DashLogic>();
		aexCompNameList->AddCompName<DashLogic>("Dash Logic");
		aexFactory->Register<FootLogic>();
		aexCompNameList->AddCompName<FootLogic>("Foot Logic");

		// Checkpoint
		aexFactory->Register<Checkpoint>();
		aexCompNameList->AddCompName<Checkpoint>("Checkpoint");
		// Fadeout component
		aexFactory->Register<Fader>();
		aexCompNameList->AddCompName<Fader>("Fader");
		/*aexFactory->Register<FaderIn>();
		aexCompNameList->AddCompName<FaderIn>("FaderIn");*/

		// Level loader
		aexFactory->Register<LevelLoader>();
		aexCompNameList->AddCompName<LevelLoader>("LevelLoader");

		aexFactory->Register<FloatingPlatform>();
		aexCompNameList->AddCompName<FloatingPlatform>("Floating Platform");
		aexFactory->Register<PlatformOnAboveActivator>();
		aexCompNameList->AddCompName<PlatformOnAboveActivator>("Platform OnAbove Activator");
		aexFactory->Register<Smasher>();
		aexCompNameList->AddCompName<Smasher>("SMASHER");
		// Box Logic
		aexFactory->Register<Box>();
		aexCompNameList->AddCompName<Box>("Box");
		aexFactory->Register<BoxSpawner>();
		aexCompNameList->AddCompName<BoxSpawner>("Box Spawner");

		aexFactory->Register<FlyingSpawner>();
		aexCompNameList->AddCompName<FlyingSpawner>("Flying Spawner");
		aexFactory->Register<RunningTrack>();
		aexCompNameList->AddCompName<RunningTrack>("RunningTrack");
		aexFactory->Register<FollowAnObject>();
		aexCompNameList->AddCompName<FollowAnObject>("FollowAnObject");
		aexFactory->Register<ParallaxComp>();
		aexCompNameList->AddCompName<ParallaxComp>("Parallax");
		aexFactory->Register<RunningTrack>();
		aexCompNameList->AddCompName<RunningTrack>("Running Track");


		aexFactory->Register<AcidController>();
		aexCompNameList->AddCompName<AcidController>("AcidController");

		aexFactory->Register<PuzzleLogic>();
		aexCompNameList->AddCompName<PuzzleLogic>("PuzzleLogic");
		

		aexFactory->Register<EditorUpdateableComp>();

		// AI COMPONENTS
		aexFactory->Register<PathComponent>();
		aexCompNameList->AddCompName<PathComponent>("Path");
		aexFactory->Register<PathFollower>();
		aexCompNameList->AddCompName<PathFollower>("Path Follower");

		// DIALOGUE COMPONENTS
		aexFactory->Register<DialogueSystem>();
		aexCompNameList->AddCompName<DialogueSystem>("Dialogue System");

		// MENU COMPONENTS
		aexFactory->Register<TitleScreenLogic>();
		aexCompNameList->AddCompName<TitleScreenLogic>("Title Screen Logic");
		aexFactory->Register<HoverableButton>();
		aexCompNameList->AddCompName<HoverableButton>("Hoverable Button");
		aexFactory->Register<HoverableButton>();
		aexCompNameList->AddCompName<HoverableButton>("Hoverable Button");
		aexFactory->Register<PlayButtonLogic>();
		aexCompNameList->AddCompName<PlayButtonLogic>("Play Button Logic");
		aexFactory->Register<MouseFollower>();
		aexCompNameList->AddCompName<MouseFollower>("Mouse Follower");
		aexFactory->Register<ExitButtonLogic>();
		aexCompNameList->AddCompName<ExitButtonLogic>("Exit Button Logic");
		aexFactory->Register<OptionsButtonLogic>();
		aexCompNameList->AddCompName<OptionsButtonLogic>("Options Button Logic");
		aexFactory->Register<CreditsButtonLogic>();
		aexCompNameList->AddCompName<CreditsButtonLogic>("Credits Button Logic");
		aexFactory->Register<ControlsButtonLogic>();
		aexCompNameList->AddCompName<ControlsButtonLogic>("Controls Button Logic");
		aexFactory->Register<PauseMenuLogic>();
		aexCompNameList->AddCompName<PauseMenuLogic>("Pause Menu Logic");


		// MOVING BACKGROUND COMPS
		aexFactory->Register<BackgroundMover>();
		aexCompNameList->AddCompName<BackgroundMover>("Background Mover");
		aexFactory->Register<VeinMovingPlatform>();
		aexCompNameList->AddCompName<VeinMovingPlatform>("VeinMovingPlatform");
		aexFactory->Register<MovingBackgroundActivator>();
		aexCompNameList->AddCompName<MovingBackgroundActivator>("Moving Background Activator");


		// STATION COMPONENTS
		aexFactory->Register<StationZoomOut>();
		aexCompNameList->AddCompName<StationZoomOut>("Station Zoom Out");
		aexFactory->Register<ChaseObject>();
		aexCompNameList->AddCompName<ChaseObject>("Chase Object");


		// HUD COMPONENTS
		aexFactory->Register<HeartLogic>();
		aexCompNameList->AddCompName<HeartLogic>("Heart Logic");
		aexFactory->Register<LiveController>();
		aexCompNameList->AddCompName<LiveController>("Live Controller");
		aexFactory->Register<Meter>();
		aexCompNameList->AddCompName<Meter>("Meter");
		aexFactory->Register<Meter>();
		aexCompNameList->AddCompName<Meter>("Meter");
		aexFactory->Register<HUDFader>();
		aexCompNameList->AddCompName<HUDFader>("HUD Fader");
		aexFactory->Register<BossBarLogic>();
		aexCompNameList->AddCompName<BossBarLogic>("Boss Bar Logic");
		aexFactory->Register<ComboDisplayLogic>();
		aexCompNameList->AddCompName<ComboDisplayLogic>("Combo Display Logic");

		// WAVE SYSTEM
		aexFactory->Register<WaveSystem>();
		aexCompNameList->AddCompName<WaveSystem>("Wave System");
		aexFactory->Register<ExampleWaveSystem>();
		aexCompNameList->AddCompName<ExampleWaveSystem>("Example Wave System");
		aexFactory->Register<BoilerRoomWaveSystem>();
		aexCompNameList->AddCompName<BoilerRoomWaveSystem>("Boiler Room Wave System");
		aexFactory->Register<Spawner>();
		aexCompNameList->AddCompName<Spawner>("Spawner");

		// BOILER ROOM
		aexFactory->Register<GearsLogic>();
		aexCompNameList->AddCompName<GearsLogic>("Gears Logic");
		aexFactory->Register<HeartBeat>();
		aexCompNameList->AddCompName<HeartBeat>("HeartBeat");
		aexFactory->Register<EnemyActivation>();
		aexCompNameList->AddCompName<EnemyActivation>("Enemy Activation");

		aexFactory->Register<LogosScreenLogic>();
		aexCompNameList->AddCompName<LogosScreenLogic>("Logos Screen Logic");

		return true;
	}

	// Static variable
	std::map<std::string, ICreator*>Factory::mCreators;
	
	void Factory::AddCreator(const char * class_type, ICreator * creator)
	{
		// Add a new element to mCreators
		mCreators[class_type] = creator;
	}

} 