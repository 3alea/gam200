#pragma once
#include <list>
#include "Composition\AEXComponent.h"
#include "Core\AEXSystem.h"


namespace AEX
{
	class EditorUpdateableComp : public IComp
	{
	public:
		virtual void Initialize();
		virtual void Update();
		virtual void Shutdown();

		virtual ~EditorUpdateableComp();

		void FromJson(json & value);
		void ToJson(json & value);
	};

	class EditorUpdateables : public ISystem
	{
		AEX_RTTI_DECL(EditorUpdateables, ISystem);
		AEX_SINGLETON(EditorUpdateables);

	public:
		void Update();

		// component management
		void AddComp(IComp * comp);
		void RemoveComp(IComp * comp);
		void ClearComps();

	private:
		std::list<IComp *> mCompsToUpdate;
	};

	#define aexEditorUpdateableComps (EditorUpdateables::Instance())
}