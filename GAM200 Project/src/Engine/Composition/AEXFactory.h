#ifndef AEX_FACTORY_H_
#define AEX_FACTORY_H_


#include "../Core/AEXBase.h"
#include "../Core/AEXSystem.h"
#include <iostream>
#include "src\Engine\Utilities\AEXAssert.h"
#include "Components\AEXTransformComp.h"
#include "Resources/AEXResourceManager.h"


// Implemented by Diego Sanz
namespace AEX
{
	class ICreator
	{
		public:
			ICreator() {}
			virtual IBase* Create() = 0;
	};

	template<typename type>
	class TCreator : public ICreator
	{
		public:
			TCreator() : ICreator() {};

			virtual IBase* Create()
			{
				if (type::TYPE().SameTypeAs(TransformComp::TYPE()))
				{
					//std::cout << "TransformComp created through factory" << std::endl;
				}
				return dynamic_cast<IBase*>(new type);
			}
	};

	class Factory : public ISystem
	{
		AEX_RTTI_DECL(Factory, ISystem);
		AEX_SINGLETON(Factory);


		public:
			// Added by Miguel
			bool Initialize();

			void AddCreator(const char * class_type, ICreator * creator);

			IBase * Create(const char * typeName)
			{
				std::string & withoutAEX = aexResourceMgr->GetNameWithoutNamespaces(typeName);

				// IMPORTANT: FIND THE CREATOR HERE
				if (mCreators.find(withoutAEX) != mCreators.end())
				{
					// DEBUG
					//std::cout << "An object of class " << withoutAEX << " has been created" << std::endl;
					return mCreators[withoutAEX]->Create();
				}

				// DEBUG
				//std::cout << "The class " << withoutAEX << " is not registered" << std::endl;
				// You sure it is called withoutAEX? Try using the actual name of the component class
				ASSERT(0, NULL);
				// NO CREATOR REGISTERED
				return NULL;
			}

			// Adds a new creator to mCreators
			template<typename T>
			void Register(ICreator* creator = NULL)
			//void RegisterF5(ICreator* creator = NULL)
			{
				std::string & withoutAEX = aexResourceMgr->GetNameWithoutNamespaces(T::TYPE().GetName());

				// Check if the class is already registered
				if (mCreators.find(T::TYPE().GetName()) != mCreators.end())
				{
					// DEBUG
					//std::cout << "The class " << T::TYPE().GetName() << " is already registered" << std::endl;
					return;
				}

				AddCreator(withoutAEX.c_str(), creator ? creator : new TCreator<T>);

				// DEBUG
				if (mCreators.find(withoutAEX) != mCreators.end())
				{
					//std::cout << "The class " << T::TYPE().GetName() << " has been registered" << std::endl;
				}
			}

			template<typename T>
			T * Create()
			{
				return dynamic_cast<T*>(Create(T::TYPE().GetName()));
			}

			// Added by Miguel
			/*IBase * Create(const char * typeName)
			{
				std::map<std::string, ICreator*>::iterator it = mCreators.find(std::string(typeName));

				if (it == mCreators.end())
				{
					assert(false);
					return NULL;
				}

				else
				{
					// DEBUG
					std::cout << "An object of class " << typeName << " has been created" << std::endl;
					return it->second->Create();
				}
			}*/
			
		private:
			static std::map<std::string, ICreator*>mCreators;
	};

	#define aexFactory (Factory::Instance())
}


#endif