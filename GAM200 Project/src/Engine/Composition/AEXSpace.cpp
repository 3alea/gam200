#include <string>
#include <iostream>
#include "AEXSpace.h"
#include "AEXScene.h"
#include "AEXFactory.h"
#include "src/Engine/Imgui/imgui.h"
#include "src/Engine/Utilities/AEXAssert.h"
#include <algorithm>
#include "AI/Path2D.h"

namespace AEX {

	Space::~Space()
	{
		FOR_EACH(it, mallObjects)
		{
			FOR_EACH(vectorIt, it->second)
			{
				//if ((*vectorIt)->GetParent()->GetType().SameTypeAs(Space::TYPE()))
				//{
					delete (*vectorIt);
				//}
			}
		}

		mDeadObjects.clear();
	}

	AEX::Space::Space(const char * name)
	{
		// Set the name of the space
		SetBaseName(name);
	}

	void AEX::Space::Update()
	{
		FOR_EACH(it, mallObjects)
		{
			FOR_EACH(vectorIt, it->second)
			{
				if (TransformComp* pTrans = (*vectorIt)->GetComp<TransformComp>())
				{
					pTrans->Update();
				}
			}
		}
	}

	GameObject* Space::CreateGameObject(const char* name, GameObject* parent)
	{
		GameObject* go = new GameObject;
		go->SetBaseName(name);
		return AddObject(go, parent);
	}

	GameObject* Space::AddObject(GameObject * go, GameObject* parent)//if not parent is setted, the space will be the parents(it will be a base object)
	{
		if (!go) //sanity check
			return NULL;

		go->mParentSpace = this;

		if (parent == NULL)
			parent = this;

		// the object already has a parent
		if (go->mParent && go->mParent != parent) {
			go->mParent->RemoveChild(go); // need to implement
		}
		if(go->mParent == NULL)
			go->mParent = parent;

		if (go->mParent != parent) {
			parent->mChildren.push_back(go); //Add the object into the parents child list
			go->mParent = parent; // set the parent
		}

		auto it = mallObjects.find(std::string(go->GetBaseName()));

		if (it != mallObjects.end())
		{
			std::vector<GameObject *> & objects = it->second;

			// check for duplicate before adding. 
			for (unsigned int i = 0; i < objects.size(); ++i)
				if (objects[i] == go) // found it, return to avoid adding again
					return NULL;
		}

		// get the list of all objects with the specific name
		//std::vector<GameObject *> & objects = mallObjects[go->GetBaseName()];
		
		// add the object in the std::vector inside the map
		mallObjects[go->GetBaseName()].push_back(go);

		return go;
	}

	GameObject* Space::AddObject(const char* name, GameObject* parent)
	{
		GameObject* go = new GameObject;
		go->SetBaseName(name);
		return AddObject(go, parent);
	}

	void Space::DestroyObject(GameObject * go) // removes all the gameobject with the same name
	{
		if (go == nullptr)
			return;

		if (mDeadObjects.size() != 0)
		{
			if (std::find(mDeadObjects.begin(), mDeadObjects.end(), go) != mDeadObjects.end())
				return;
		}

		go->mbAlive = false;
		//std::cout << "Dead object added to the list" << std::endl;
		mDeadObjects.push_back(go);
		
		std::map<std::string, std::vector<GameObject*>>::iterator it = mallObjects.find(go->GetBaseNameString());

		if (it != mallObjects.end())
		{
			for (auto it2 = it->second.begin(); it2 != it->second.end(); it2++)
			{
				if (*it2 == go)
				{
					it->second.erase(it2);
					return;
				}
			}
		}
	}

	void AEX::Space::DestroyDeadObjects()
	{
		for (auto it = mDeadObjects.begin(); it != mDeadObjects.end(); )
		{
			GameObject * parent = (*it)->GetParent();
			parent->RemoveChild(*it);

			GameObject * go = *it;

			it = mDeadObjects.erase(it);

			delete go;
		}

	}

	void Space::OnGui()
	{
	
	}

	GameObject* Space::FindObject(const char* name)
	{
		std::map<std::string, std::vector<GameObject*>>::iterator it;
		it = mallObjects.find(name); // get a pointer to the object in the map
		if (it == mallObjects.end()) // if it hasn't find any object
			return NULL;
		if (it->second.empty()) // if the std::vector of GO* is empty
			return NULL;
		return it->second.front();
	}

	// returns the number of found objects
	int Space::FindAllObjects(const char* name, std::vector<GameObject*>& outVector)
	{
		std::vector<GameObject *> & objects = mallObjects[name];
		for (unsigned int i = 0; i < objects.size(); ++i)
			outVector.push_back(objects[i]);
		return objects.size();
	}

	GameObject* Space::FindObjectbyTag(std::string tag)
	{
		//could be improved??

		std::map<std::string, std::vector<GameObject*>>::iterator it;
		it = mallObjects.begin();

		while (it != mallObjects.end())
		{
			std::vector<GameObject *> & objects = it->second;
			std::vector<GameObject *>::iterator objit = objects.begin();
			while (objit != objects.end())
			{
				std::vector<std::string> tags = (*objit)->GetTags();
				FOR_EACH(tit, tags)
				{
					if (*tit == tag)
						return *objit;
				}

				objit++;
			}
			it++;
		}

		return NULL;
	}

	int Space::FindAllObjectsbyTag(std::string tag, std::vector<GameObject*>& outVector)
	{
		//could be improved??

		int flag = 0;

		std::map<std::string, std::vector<GameObject*>>::iterator it;
		it = mallObjects.begin();

		while (it != mallObjects.end()) // go through the map
		{
			std::vector<GameObject *> & objects = it->second;
			std::vector<GameObject *>::iterator objit = objects.begin();

			while (objit != objects.end()) // go through the GameObject* std::vector
			{
				std::vector<std::string> tags = (*objit)->GetTags();
				FOR_EACH(tit, tags)
				{
					if (*tit == tag)
					{
						outVector.push_back(*objit); // add it to the out vector
						flag = 1; // at least one found
					}
				}
				
				objit++;
			}

			it++;
		}

		return flag;
	}


	// Serialization functions (Miguel)
	void Space::FromJson(json & value)
	{
		SetBaseName((value["Name"].get<std::string>()).c_str());
		SetEnabled(value["Enabled"].get<bool>());
		mbActive = value["Active"].get<bool>();
		mbAlive = value["Alive"].get<bool>();

		json objVal = value["Objects"];
		FOR_EACH(jsonIt, objVal)
		{
			if (GameObject * obj = dynamic_cast<GameObject *>(aexFactory->Create((*jsonIt)["RTTI Type"].get<std::string>().c_str())))
			{
				obj->SetParent(this);
				obj->SetParentSpace(this);
				obj->FromJson(*jsonIt);
				AddObject(obj);//, obj->GetParent());
			}

			else ASSERT(0, NULL); // HARD CRASH
		}
	}

	void Space::ToJson(json & value)
	{
		json noTitleSpaceVal;

		noTitleSpaceVal["Name"] = GetBaseName();
		noTitleSpaceVal["Active"] = mbActive;
		noTitleSpaceVal["Alive"] = mbAlive;
		noTitleSpaceVal["Enabled"] = mbEnabled;

		//							Name			All GOs with that name
		// mallObjects = std::map<std::string, std::vector<GameObject *> >

		FOR_EACH(it, mallObjects)
		{
			FOR_EACH(it2, it->second)
			{
				if ((*it2)->GetParent()->GetType().SameTypeAs(Space::TYPE()))
				{
					json noTitleObjectVal;
					(*it2)->ToJson(noTitleObjectVal);
					noTitleSpaceVal["Objects"].push_back(noTitleObjectVal);
				}	
			}
		}

		value["Spaces"].push_back(noTitleSpaceVal);
	}

	GameObject * Space::InstantiateArchetype(const char * nameWithoutExtension, const AEVec3 & pos, f32 orientation)
	{
		json value;
		std::string path = "data/Archetypes/";
		std::string extension = ".json";
		path += nameWithoutExtension + extension;
		LoadFromFile(value, path);
		
		if (GameObject * obj = dynamic_cast<GameObject *>(aexFactory->Create(value["RTTI Type"].get<std::string>().c_str())))
		{
			obj->FromJson(value);
			AddObject(obj);
			TransformComp * tr = obj->GetComp<TransformComp>();

			if (tr)
			{
				tr->mLocal.mTranslationZ = pos;
				tr->mWorld.mTranslationZ = AEVec2(-1000, -1000);
				tr->SetRotationAngle(orientation);
			}

			if (PathComponent * path = obj->GetComp<PathComponent>())
			{
				if (path->mRespectToObject)
				{
					tr->translationChanged = true;
					tr->transDisplacement = AEVec2(tr->mLocal.mTranslationZ.x, tr->mLocal.mTranslationZ.y);
				}
			}

			return obj;
		}

		return NULL;
	}

	AEVec2 * AEX::Space::GetGravity()
	{
		return &mGravity;
	}
	void AEX::Space::SetGravity(const AEVec2 & newGravity)
	{
		mGravity = newGravity;
	}
}