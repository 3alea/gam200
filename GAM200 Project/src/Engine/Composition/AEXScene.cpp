#include <vector>
#include "AEXScene.h"
#include "src/Engine/Utilities/AEXContainers.h"
#include "src/Engine/Composition/AEXFactory.h"
#include "src/Engine/Core/AEXSerialization.h"
#include "src/Engine/Composition/AEXScene.h"
#include "src/Engine/Components/AEXComponents.h"
#include "Imgui\imgui.h"
#include "Components/Rigidbody.h"
#include "Components/Collider.h"
#include "Components/Logic.h"
//#include "Components/PlayerStats.h"
//#include "Components/PlayerController.h"
//#include "Components/Follower.h"
//#include "Components/PlatformController.h"
//#include "Components\ShootController.h"
//#include "Components\BulletLogic.h"
//#include "Components\DashLogic.h"
#include "Editor/Editor.h"
#include "Menu/PauseMenu/PauseMenuLogic.h"
#include "Audio/AudioManager.h"
#include "Resources/AEXResourceManager.h"
#include "Utilities/AEXAssert.h"
#include <experimental/filesystem>

namespace AEX
{
	namespace fs = std::experimental::filesystem;

	bool Scene::bExitGame = false;

	Scene::~Scene()
	{
		FOR_EACH(it, mSpaces)
		{
			delete *it;
		}
		GfxMgr->mShaderList.clear();
		GfxMgr->mCameraList.clear();
		GfxMgr->mRenderableList.clear();
	}

	bool Scene::Initialize()
	{
		aexScene->AddSpace("Main");
		doneOnce = false;
		return true;
	}

	void Scene::Shutdown()
	{
		FOR_EACH(it, mSpaces)
		{
			delete *it;
		}
		GfxMgr->mShaderList.clear();
		GfxMgr->mCameraList.clear();
		GfxMgr->mRenderableList.clear();
	}

	void Scene::Update()
	{
		FOR_EACH(it, mSpaces)
			(*it)->Update();

		if (bFadeTransition && !FindObjectInANYSpace("fadeout"))
		{
			GameObject * camera = FindObjectInANYSpace("CameraHUD");
			TransformComp * camTr = nullptr;
			if (camera)
				camTr = camera->GetComp<TransformComp>();

			GameObject * normalCamObj = aexScene->FindObjectInANYSpace("camera");
			TransformComp * normalCamTr = nullptr;
			if (normalCamObj)
				normalCamTr = normalCamObj->GetComp<TransformComp>();

			if (camTr)
			{
				GameObject* fadeout = mSpaces.front()->InstantiateArchetype("fadeout", AEVec3(camTr->mLocal.mTranslationZ.x, camTr->mLocal.mTranslationZ.y, 499.0f));
			}
			else if (normalCamTr)
			{
				GameObject* fadeout = mSpaces.front()->InstantiateArchetype("fadeout", AEVec3(normalCamTr->mLocal.mTranslationZ.x, normalCamTr->mLocal.mTranslationZ.y, 499.0f));
			}
			else
			{
				GameObject* fadeout = mSpaces.front()->InstantiateArchetype("fadeout");
			}
			bFadeTransition = false;
		}
	}

	// //FUNCTION TO WORK WITH SPACES--------------------------------------------------------------

	Space* Scene::AddSpace(Space* space)
	{
		mSpaces.push_back(space);
		return space;
	}

	Space* Scene::AddSpace(const char* name)
	{
		Space* space = new Space;
		space->SetBaseName(name);
		return AddSpace(space);
	}

	Space* Scene::FindSpace(const char* name)
	{
		std::string mname = name;
		std::vector<Space*>::iterator it = mSpaces.begin();
		while (it != mSpaces.end())
		{
			if ( mname == (*it)->GetBaseName())
			{
				return *it;
			}
			it++;
		}
		return NULL; // NOT FOUND
	}

	int Scene::DeleteSpace(const char* name)
	{
		std::string mname = name;
		std::vector<Space*>::iterator it = mSpaces.begin();
		while (it != mSpaces.end())
		{
			if (mname == (*it)->GetBaseName()) // if we find the space
			{
				auto mapit = (*it)->mallObjects.begin();
				while (mapit != (*it)->mallObjects.end())
				{
					std::vector<GameObject*>& obj = mapit->second;
					auto objit = obj.begin();
					while (objit != obj.end())
					{
						objit = obj.erase(objit);
						// I THINK THIS SHOULD ALSO DELETE THE OBJECT
						
					}
					obj.clear();
					mapit++;
				}
				//(*it)->mChildren.clear(); // delete the children list

				//(*it)->mallObjects
				it = mSpaces.erase(it); // delete the space fromt eh spaces list
				return 1;
			}

			it++;
		}

		return 0; // nothing got deleted
	}

	int Scene::DeleteSpace(Space* space)
	{
		return DeleteSpace(space->GetBaseName());
	}

	void Scene::FreeDeadObjects()
	{
		FOR_EACH(it, mSpaces)
		{
			(*it)->DestroyDeadObjects();
		}
	}

	// //FUNCTION TO WORK WITH OBJECTS --------------------------------------------------------------

	GameObject* Scene::FindObjectInSpace(const char* name, Space* space)
	{
		if (space == NULL) //main space
			space = FindSpace("Main");

		return(space->FindObject(name));
	}

	GameObject* Scene::FindObjectInSpace(const char* name, const char* spacename)
	{
		if (spacename == NULL)
			spacename = "Main";

		Space* space = FindSpace(spacename);
		if(space)
			return(space->FindObject(name));
		return NULL;
	}

	GameObject* Scene::FindObjectInANYSpace(const char* name)
	{
		std::vector<Space*>::iterator it = mSpaces.begin();
		while(it != mSpaces.end()) // iterator for the space list
		{
			GameObject* go = (*it)->FindObject(name);

			if (go)
				return go;

			it++;
		}
		return NULL; // NOT FOUND
	}

	void Scene::FindAllObjectsInANYSpace(const char* name, std::vector<GameObject*>& outVector)
	{
		std::vector<Space*>::iterator it = mSpaces.begin();
		while (it != mSpaces.end()) // iterator for the space list
		{
			int flag = (*it)->FindAllObjects(name, outVector);			
			it++;
		}

	}

	void AEX::Scene::OnGui()
	{
		//FOR_EACH(it, mSpaces)
		//	(*it)->OnGui();
	}


	bool Scene::bFadeTransition = false;

	//MIGUEL
	void Scene::FromJson(json & value)
	{
		json sceneVal = value["Scene"];
		aexScene->SetBaseName(sceneVal["Name"].get<std::string>().c_str());
		
		if (sceneVal["Already Saved"] != nullptr)
			sceneVal["Already Saved"] >> mbAlreadySaved;

		json spacesVal = sceneVal["Spaces"];
		FOR_EACH(jsonIt, spacesVal)
		{
			Space * space = aexFactory->Create<Space>();
			space->FromJson(*jsonIt);
			AddSpace(space);
		}

		Editor->archetypes_selectedSpace = aexScene->FindSpace("Main");

		audiomgr->StopAll();
		//static Voice * menuAudio1 = nullptr;
		//static Voice * menuAudio2 = nullptr;

		if (GetBaseNameString() != "Main Menu" && GetBaseNameString() != "LogosScreen")
		{
			//audiomgr->Loop(aexResourceMgr->getResource<Sound>("data/Audio/PlaytestingAudios/ENTAILS.wav"), Voice::Music)->SetVolume(0.4f);
		}

		if (GetBaseNameString() == "LogosScreen")
		{

		}
		else if (GetBaseNameString() == "Main Menu")
		{
			LoadMenuAudios();

			//audiomgr->FreeThisVoice(LevelMusic);
			//LevelMusic = nullptr;

			//if (Editorstate->IsPlaying())
			//{
				/*menuAudio1 = */audiomgr->Loop(aexResourceMgr->getResource<Sound>("data/Audio/Menu/MainMenu/Ambient/menuBackgroundSfx.mp3"), Voice::Music)->SetVolume(0.5f);
				//menuAudio1->SetVolume(0.5f);
				/*menuAudio2 = */audiomgr->Loop(aexResourceMgr->getResource<Sound>("data/Audio/Menu/MainMenu/Ambient/loopMusic.mp3"), Voice::Music)->SetVolume(0.1f);
				//menuAudio2->SetVolume(0.1f);
			//}
		}
		else if (GetBaseNameString() == "Heart(Tutorial)")
		{
			LoadTutorialAudios();
			//audiomgr->Loop(aexResourceMgr->getResource<Sound>("data/Audio/Tutorial/TutorialLoopMusic.mp3"), Voice::Music);

			audiomgr->Loop(aexResourceMgr->getResource<Sound>("data/Audio/PlaytestingAudios/ENTAILS.wav"), Voice::Music)->SetVolume(0.4f);

			//if (Editorstate->IsPlaying())
				//LevelMusic = audiomgr->Loop(aexResourceMgr->getResource<Sound>("data/Audio/PlaytestingAudios/ENTAILS.wav"), Voice::Music)->SetVolume(0.5f);
				//LevelMusic->SetVolume(0.5f);
			//std::cout << "working" << std::endl;
			//audiomgr->Loop(aexResourceMgr->getResource<Sound>("data\\Audio\\car level\\engineLoop.mp3"));
		}
		else if (GetBaseNameString() == "Veins")
		{
			LoadVeinsAudios();
			audiomgr->Loop(aexResourceMgr->getResource<Sound>("data/Audio/PlaytestingAudios/ENTAILS.wav"), Voice::Music)->SetVolume(0.4f);
			//audiomgr->Loop(aexResourceMgr->getResource<Sound>("data/Audio/Veins/VeinsLoopMusic.mp3"), Voice::Music);
		}
		else if (GetBaseNameString() == "Station")
		{
			LoadStationAndBoilerRoomAudios();

			audiomgr->Loop(aexResourceMgr->getResource<Sound>("data/Audio/PlaytestingAudios/ENTAILS.wav"), Voice::Music)->SetVolume(0.4f);
			//audiomgr->Loop(aexResourceMgr->getResource<Sound>("data/Audio/Station/StationLoopMusic.mp3"), Voice::Music);
		}
		else if (GetBaseNameString() == "Boiler room")
		{
			//audiomgr->Loop(aexResourceMgr->getResource<Sound>("data/Audio/BoilerRoom/LoopMusic.mp3"), Voice::Music);
			audiomgr->Loop(aexResourceMgr->getResource<Sound>("data/Audio/BoilerRoom/AmbienceLoopMusic.mp3"), Voice::SFX)->SetVolume(0.5f);
		}
		else if (GetBaseNameString() == "EndCredits")
		{
			//audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/Menu/MainMenu/Credits/EndCreditsMusic.mp3"), Voice::Music);
		}
		else if (GetBaseNameString() == "texture-repeat-test")
		{
			LoadCarLevel1Audios();
			audiomgr->Loop(aexResourceMgr->getResource<Sound>("data/Audio/car level/cancan.wav"), Voice::Music)->SetVolume(0.5f);
		}
		else if (GetBaseNameString() == "car level 2")
		{
			LoadCarLevel2Audios();
			audiomgr->Loop(aexResourceMgr->getResource<Sound>("data/Audio/car level/cancan.wav"), Voice::Music)->SetVolume(0.5f);
		}
		else if (GetBaseNameString() == "Stomach")
		{
			LoadStomach1Audios();
			audiomgr->Loop(aexResourceMgr->getResource<Sound>("data/Audio/PlaytestingAudios/ENTAILS.wav"), Voice::Music)->SetVolume(0.4f);
			//audiomgr->Loop(aexResourceMgr->getResource<Sound>("data/Audio/Stomach1/StomachLoopMusic.mp3"), Voice::Music);
		}
		else if (GetBaseNameString() == "StomachPart2")
		{
			LoadStomach2Audios();
			audiomgr->Loop(aexResourceMgr->getResource<Sound>("data/Audio/PlaytestingAudios/ENTAILS.wav"), Voice::Music)->SetVolume(0.4f);
			//audiomgr->Loop(aexResourceMgr->getResource<Sound>("data/Audio/Stomach2/MovingCastleLoopMusic.mp3"), Voice::Music);

		}
		else if (GetBaseNameString() == "Lungs")
		{
			LoadLungsAudios();
			audiomgr->Loop(aexResourceMgr->getResource<Sound>("data/Audio/Lungs/danubio.wav"), Voice::Music)->SetVolume(0.4f);
			//audiomgr->Loop(aexResourceMgr->getResource<Sound>("data/Audio/Lungs/LungsLoopMusic.mp3"), Voice::Music);
		}
		else if (GetBaseNameString() == "finalboss")
		{
			LoadFinalBossAudios();
			audiomgr->Loop(aexResourceMgr->getResource<Sound>("data/Audio/PlaytestingAudios/ENTAILS.wav"), Voice::Music)->SetVolume(0.4f);
			//audiomgr->Loop(aexResourceMgr->getResource<Sound>("data/Audio/FinalBoss/BossLoopMusic.mp3"), Voice::Music);
		}
	}

	void Scene::ToJson(json & value)
	{
		json sceneVal;
		sceneVal["Name"] = GetBaseName();
		sceneVal["Already Saved"] << mbAlreadySaved;

		FOR_EACH(it, mSpaces)
		{
			(*it)->ToJson(sceneVal);
		}

		value["Scene"] = sceneVal;
	}

	void Scene::LoadMenuAudios()
	{
		LoadAudioFolder("Menu");
	}
	void Scene::LoadTutorialAudios()
	{
		LoadAudioFolder("Tutorial");
		LoadAudioFolder("player");
		LoadAudioFolder("Enemies/EnemyGrounded");
		LoadAudioFolder("Enemies/EnemyFlying");
		LoadAudioFolder("Enemies/Bullets");
	}
	void Scene::LoadVeinsAudios()
	{
		LoadAudioFolder("Veins");
		LoadAudioFolder("Enemies/EnemyMitosis");
		LoadAudioFolder("Enemies/EnemyBugger");
	}
	void Scene::LoadStationAndBoilerRoomAudios()
	{
		LoadAudioFolder("Station");
		LoadAudioFolder("BoilerRoom");
	}
	void Scene::LoadCarLevel1Audios()
	{
		LoadAudioFolder("car level");
	}
	void Scene::LoadCarLevel2Audios()
	{
		//LoadAudioFolder("CarLevel2");
	}
	void Scene::LoadStomach1Audios()
	{
		LoadAudioFolder("Stomach1");
		LoadAudioFolder("Enemies/EnemyKamikaze");
		LoadAudioFolder("Enemies/EnemySpawner");
	}
	void Scene::LoadStomach2Audios()
	{
		LoadAudioFolder("Stomach2");
	}
	void Scene::LoadLungsAudios()
	{
		LoadAudioFolder("Lungs");
	}
	void Scene::LoadFinalBossAudios()
	{
		LoadAudioFolder("FinalBoss");
	}
	void Scene::LoadAudioFolder(const char * folderName)
	{
		try {
			std::string path = "data/Audio/";
			path += folderName;
			fs::path audioDir = fs::path(path);

			int count = 0;
			for (auto dirIt = fs::recursive_directory_iterator(audioDir);
				dirIt != fs::recursive_directory_iterator();
				dirIt++, ++count)
			{
				if (fs::is_directory(dirIt->path()))
					continue;

				if (dirIt->path().extension().string() == ".meta")
					continue;

				aexResourceMgr->getResource<Sound>(dirIt->path().relative_path().string().c_str());

				//std::cout << "Audio loaded in FromJson of " << GetBaseNameString() << " : " << dirIt->path().relative_path().string() << std::endl;
			}
		} CATCH_FS_ERROR
	}


	void Scene::ChangeLevel(const std::string & levelToLoad)
	{
		// Push to the queue of levels to load
		mLevelsToLoad.push(levelToLoad);
	}

	void Scene::SetPauseState(bool isPaused)
	{
		mGamePaused = isPaused;
	}

	// Use this function to pause/unpause the game.
	// Pauses the game if it is not paused.
	// Unpauses the game if it is paused.
	void Scene::PauseOrUnPause()
	{
		aexScene->SetPauseState(!aexScene->mGamePaused);

		GameObject * pauseObj = aexScene->FindObjectInANYSpace("PauseScreen");
		TransformComp * pauseScreenTr = nullptr;
		SpriteComp * pauseScreenSprite = nullptr;

		GameObject * pauseMenuObj = aexScene->FindObjectInANYSpace("PauseMenu");
		TransformComp * pauseMenuTr = nullptr;
		SpriteComp * pauseMenuSprite = nullptr;
		PauseMenuLogic * pauseMenuLogic = nullptr;

		GameObject * howToPlayObj = aexScene->FindObjectInANYSpace("howtoplay");
		TransformComp * howToPlayTr = nullptr;
		SpriteComp * howToPlaySprite = nullptr;

		if (pauseObj != nullptr)
		{
			pauseScreenTr = pauseObj->GetComp<TransformComp>();
			pauseScreenSprite = pauseObj->GetComp<SpriteComp>();
		}

		if (pauseMenuObj != nullptr)
		{
			pauseMenuTr = pauseMenuObj->GetComp<TransformComp>();
			pauseMenuSprite = pauseMenuObj->GetComp<SpriteComp>();
			pauseMenuLogic = pauseMenuObj->GetComp<PauseMenuLogic>();
		}

		if (howToPlayObj != nullptr)
		{
			howToPlayTr = howToPlayObj->GetComp<TransformComp>();
			howToPlaySprite = howToPlayObj->GetComp<SpriteComp>();
		}

		if (aexScene->mGamePaused)
		{
			GameObject * camObj = aexScene->FindObjectInANYSpace("CameraHUD");
			TransformComp * camTr = nullptr;

			if (camObj)
				camTr = camObj->GetComp<TransformComp>();

			if (pauseObj != nullptr && pauseMenuObj != nullptr && howToPlayObj != nullptr)
			{
				if (pauseMenuLogic != nullptr)
					pauseMenuLogic->Initialize();

				if (pauseScreenTr && pauseMenuTr && howToPlayTr && camTr)
				{
					pauseScreenTr->mLocal.mTranslationZ.x = camTr->mLocal.mTranslationZ.x;
					pauseScreenTr->mLocal.mTranslationZ.y = camTr->mLocal.mTranslationZ.y;

					pauseMenuTr->mLocal.mTranslationZ.x = camTr->mLocal.mTranslationZ.x;
					pauseMenuTr->mLocal.mTranslationZ.y = camTr->mLocal.mTranslationZ.y;

					howToPlayTr->mLocal.mTranslationZ.x = camTr->mLocal.mTranslationZ.x + 2.0f;	// Offset because sprite is on the left of the png
					howToPlayTr->mLocal.mTranslationZ.y = camTr->mLocal.mTranslationZ.y + 0.2f;	// Offset because sprite is on the bottom of the png
				}

				if (pauseScreenSprite && pauseMenuSprite && howToPlaySprite)
				{
					pauseScreenSprite->mColor[3] = 1.0f;
					pauseMenuSprite->mColor[3] = 1.0f;
					howToPlaySprite->mColor[3] = 0.0f;
				}
			}
			else
			{
				if (camTr != nullptr)
				{
					pauseObj = aexScene->mSpaces.front()->InstantiateArchetype("PauseScreen", AEVec3(camTr->mLocal.mTranslationZ.x, camTr->mLocal.mTranslationZ.y, 400.0f));
					pauseMenuObj = aexScene->mSpaces.front()->InstantiateArchetype("PauseMenu", AEVec3(camTr->mLocal.mTranslationZ.x, camTr->mLocal.mTranslationZ.y, 405.0f));
					howToPlayObj = aexScene->mSpaces.front()->InstantiateArchetype("HowToPlayBox", AEVec3(camTr->mLocal.mTranslationZ.x + 2.0f, camTr->mLocal.mTranslationZ.y + 0.2f, 405.0f));
				}

				if (pauseObj && pauseMenuObj && howToPlayObj)
				{
					pauseMenuLogic = pauseMenuObj->GetComp<PauseMenuLogic>();

					if (pauseMenuLogic != nullptr)
						pauseMenuLogic->Initialize();

					pauseScreenSprite = pauseObj->GetComp<SpriteComp>();
					pauseScreenSprite->mColor[3] = 1.0f;

					pauseMenuSprite = pauseMenuObj->GetComp<SpriteComp>();
					pauseMenuSprite->mColor[3] = 1.0f;

					howToPlaySprite = howToPlayObj->GetComp<SpriteComp>();
					howToPlaySprite->mColor[3] = 0.0f;
				}
			}
		}
		else
		{
			if (pauseScreenSprite && pauseMenuSprite && howToPlaySprite)
			{
				pauseScreenSprite->mColor[3] = 0.0f;
				pauseMenuSprite->mColor[3] = 0.0f;
				howToPlaySprite->mColor[3] = 0.0f;
			}

			mOnPauseMenu = false;
		}
	}
}
