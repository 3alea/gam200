#pragma once
#include "src\Engine\Core\AEXBase.h"

namespace AEX
{
	class ICloneable : public virtual IBase
	{
		//AEX_RTTI_DECL(ICloneable, IBase);

		public:
			virtual ICloneable* clone() = 0;

	};
}
