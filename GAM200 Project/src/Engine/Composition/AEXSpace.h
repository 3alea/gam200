#ifndef AEX_SPACE_H_
#define AEX_SPACE_H_

#include "AEXGameObject.h"
#include <map>
#include <iostream>

namespace AEX
{
	class Space : public GameObject
	{
		AEX_RTTI_DECL(Space, GameObject);
	public:
		// Default ctor, added by Miguel
		Space() : mGravity(0.0f, -70.0f) { SetParent(nullptr); }

		//constructor
		Space(const char* name);
		virtual ~Space();

		void Update();

		//ALL THE OBJECT IN THE SPACE
		std::map<std::string, std::vector<GameObject*>> mallObjects;
		std::list<GameObject*> mDeadObjects;


		//CREATE OBJECT FUNCTIONS
		GameObject* CreateGameObject(const char* name, GameObject* parent = NULL);
		
		//ADD OBJECT FUNCTIONS
		GameObject* AddObject(GameObject* go, GameObject* parent = NULL);
		GameObject* AddObject(const char* go, GameObject* parent = NULL);


		//FIND OBJECT FUNCTIONS
			// by name
		GameObject* FindObject(const char* name);
		int FindAllObjects(const char* name, std::vector<GameObject*>& outVector);
			// by tag
		GameObject* FindObjectbyTag(std::string tag);
		int FindAllObjectsbyTag(std::string tag, std::vector<GameObject*>& outVector);


		//DESTROY OBJECT FUNCTIONS
		void DestroyObject(GameObject* go); // notes: don't forget to update the parent, don't forget to also destroy the children.
		void DestroyDeadObjects();

		void OnGui();

		// Serialization functions (Miguel)
		void FromJson(json & value);
		void ToJson(json & value);
		GameObject * InstantiateArchetype(const char * nameWithoutExtension, const AEVec3 & pos = AEVec3(0, 0, 0), f32 orientation = 0.0f);

		// Getter
		unsigned int* getId();
		AEVec2* GetGravity();
		void SetGravity(const AEVec2& newGravity);

	//private:
		//const char* mname;

		private:
			AEVec2 mGravity;
	};
}

#endif