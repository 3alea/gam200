#pragma once
// ----------------------------------------------------------------------------
// Project: GAM300 - Sample Engine
// File:	IComp.h
// Purpose:	Header file for the IComp API
// Author:	Thomas Komair
//
// Copyright DigiPen Institute of Technology
// ----------------------------------------------------------------------------
#include "AEXComposition.h"
#include "AEXFactory.h"
#include "AEXObjectManager.h"
#include "..\Components\AEXComponents.h"
#include "..\Core\AEXGlobalVariables.h"
#include "..\Imgui\imgui.h"
#include "src/Engine/Core/AEXGlobalVariables.h"
#include "src/Engine/Composition/AEXSpace.h"
#include "src/Engine/Core/AEXTypedefs.h"
#include "src/Engine/Graphics/GfxMgr.h"
#include "AEXScene.h"
#include "src\Engine\Imgui\imgui.h"
#include "src/Engine/Utilities/AEXAssert.h"
#include "Resources\AEXResourceManager.h"
#include "AI\Raycast.h"
#include "Platform/AEXInput.h"
#include "Editor/Editor.h"
#include "Composition/ComponentList.h"


#include <iostream>
#include <sstream>

namespace AEX
{
	int GameObject::objectID = 0;
	// ----------------------------------------------------------------------------
	// ----------------------------------------------------------------------------
	// AEXOBJECT

	GameObject::GameObject() : mEditorArchetypeName("")
	{
		// This is not used, right? / You're right. Lemme remove it. Now people will know nothing :^)
		std::string ObjectName = GOdefaultName + std::to_string(objectID);
		ObjMgr->AddObject(this, ObjectName.c_str());
		mId = objectID;
		objectID++;

		mComps.clear();
		mParentSpace = aexScene->FindSpace("Main");
		mParent = NULL;

		mSavedToArchetype = false;
		SelectableLogic();
	}
	GameObject::~GameObject()
	{
		// delete all children
		//for (unsigned int i = 0; i < mChildren.size(); ++i)
		//{
		//	if (mChildren[i])
		//		delete mChildren[i];
		//}

		//std::cout << "Delete name : " <<  GetBaseNameString() << std::endl;

		if (mComps.size() != 0)
			for (unsigned int i = 0; i < mComps.size(); i++)
				delete mComps[i];

		// clear for good measure
		mChildren.clear();
	}

	// ----------------------------------------------------------------------------
	#pragma region// STATE METHODS
	
	void GameObject::SetEnabled(bool enabled) // Call Set Enabled on all components
	{
		// avoid redundant work
		if (mbEnabled == enabled)
			return;

		// call base method
		mbEnabled = enabled;

		// delegate to components
		FOR_EACH(it, mComps)
			(*it)->SetEnabled(enabled);
	}
	void GameObject::Initialize()
	{
		// Initialize all comps 
		FOR_EACH(it, mComps)
			(*it)->Initialize();
	}

	void GameObject::Render()
	{
		
	}

	void GameObject::Shutdown()
	{
		// shutdown all comps 
		FOR_EACH(it, mComps)
			(*it)->Shutdown();

		//
		//
		//
		//
		//delete mTex;
	}

	// Cloning
	GameObject* GameObject::clone()
	{
		json value;
		ToJson(value);

		// Create a new game object
		GameObject* newGO = aexFactory->Create<GameObject>();

		newGO->FromJson(value);

		GetBaseNameString().append(" copy");

		GetParentSpace()->AddObject(newGO, newGO->GetParent());


		return newGO;
	}

	void GameObject::DrawDebug()
	{
		GfxMgr->DrawTransform(GetTransformComp->mLocal);
	}

	#pragma endregion

	// ----------------------------------------------------------------------------
	#pragma region// COMPONENT MANAGEMENT

	// Find Component
	u32 GameObject::GetCompCount() const
	{
		return mComps.size();
	}
	IComp* GameObject::GetComp(u32 index) const
	{
		if (index < GetCompCount())
			return mComps[index];
		return NULL;
	}
	IComp* GameObject::GetComp(const char * type) const
	{
		// go throught the components and look for the same type
		for (auto it = mComps.begin(); it != mComps.end(); ++it)
		{
			std::string typeName = (*it)->GetType().GetName();
			if (typeName == type)
				return (*it);
		}
		return NULL;
	}
	IComp* GameObject::GetComp(const Rtti & type) const
	{
		// go throught the components and look for the same type
		for (auto it = mComps.begin(); it != mComps.end(); ++it)
		{
			if ((*it)->GetType().SameTypeAs(type))
				return (*it);
		}
		return NULL;
	}

	IComp* GameObject::GetCompName(const char * compName, const char *compType) const
	{
		for (auto it = mComps.begin(); it != mComps.end(); ++it)
		{
			// found a match with the name
			if (strcmp((*it)->GetBaseName(), compName) == 0)
			{
				// not same type -> continue
				if (compType && strcmp(compType, (*it)->GetType().GetName()) != 0)
					continue;
				// same type or don't care about type -> return
				return (*it);
			}
		}
		return NULL;
	}

	// Add/Remove by address
	void GameObject::AddComp(IComp* pComp)
	{
		if (pComp)
		{
			mComps.push_back(pComp);
			pComp->mOwner = this;
			pComp->Initialize();
		}
	}

	void GameObject::RemoveComp(IComp* pComp, bool del)
	{
		if (!pComp)
			return;
		// TODO: make sure that we indeed don't need that
		// NOTE this will create memory leaks.
		for (auto it = mComps.begin(); it != mComps.end(); ++it)
		{
			if ((*it) == pComp)
			{
				pComp->Shutdown();
				pComp->mOwner = NULL;
				mComps.erase(it);
				if(del)
					delete pComp;
				return;
			}
		}
	}

	// Removes first component encoutered that match the search criteria
	void GameObject::RemoveCompType(const char * compType)
	{
		RemoveComp(GetComp(compType));
	}
	void GameObject::RemoveCompType(const Rtti & compType)
	{
		RemoveComp(GetComp(compType));
	}
	void GameObject::RemoveCompName(const char * compName, const char * compType)
	{
		RemoveComp(GetCompName(compName, compType));
	}

	// Removes all components encoutered that match the search criteria
	void GameObject::RemoveAllCompType(const char * compType)
	{
		IComp* pComp = GetComp(compType);
		while (pComp)
		{
			RemoveComp(pComp);
			pComp = GetComp(compType);
		}
	}
	void GameObject::RemoveAllCompType(const Rtti & compType)
	{
		IComp* pComp = GetComp(compType);
		while (pComp)
		{
			RemoveComp(pComp);
			pComp = GetComp(compType);
		}
	}
	void GameObject::RemoveAllCompName(const char * compName, const char * compType)
	{
		IComp* pComp = GetCompName(compName, compType);
		while (pComp)
		{
			RemoveComp(pComp);
			pComp = GetCompName(compName, compType);
		}
	}

	void GameObject::RemoveAllComp()
	{
		while (mComps.size())
		{
			mComps.back()->Shutdown();
			delete mComps.back();
			mComps.pop_back();
		}
	}

	//json & GameObject::operator<<(json & j) const
	//{
	//	// serialize name (todo: implement this as a Property). 
	//	j["Name"] = mName;

	//	// serialize components
	//	json & comps = j["comps"];
	//	for (u32 i = 0; i < this->GetComps().size(); ++i)
	//	{
	//		json compJson;
	//		IComp * comp = GetComp(i);
	//		compJson["__type"] = comp->GetType().GetName();

	//		// write the component  using stream operators

	//		// note only IComp3 implement it
	//		if (auto comp2 = dynamic_cast<IComp*>(comp))
	//		{
	//			comp2->operator<<(compJson);
	//		}

	//		// add to json array
	//		comps.push_back(compJson);
	//	}

	//	return j;
	//}

	//void GameObject::operator >> (json & j)
	//{
	//	// get components
	//	Shutdown(); // clear everything before loading. 

	//				// read name (this should really be another property).
	//	SetName(j["Name"].get<std::string>().c_str());

	//	// read comps and allocate them
	//	json & comps = *j.find("comps");
	//	for (auto it = comps.begin(); it != comps.end(); ++it)
	//	{
	//		// get json object for that component
	//		json & compJson = *it;

	//		// read type and create with factory
	//		std::string typeName = compJson["__type"].get<std::string>();
	//		IBase * newComp = aexFactory->Create(typeName.c_str());

	//		// error check - Factory couldn't allocate memory
	//		if (newComp == nullptr)
	//			continue;

	//		// only stream components deriving from IComp2
	//		if (auto comp2 = dynamic_cast<IComp*>(newComp))
	//			comp2->operator >> (compJson);

	//		// add new comp object
	//		AddComp((IComp*)newComp);
	//	}
	//}

	/*std::ostream & GameObject::operator<<(std::ostream & o) const
	{
		json j;
		this->operator<<(j);
		return o << j;
	}*/

	void GameObject::AddIfNotRepeatedComp(const std::vector<IComp *> & compList, /*const std::string & namespaceString, */const std::string & compToCheck)
	{
		for (unsigned i = 0; i < compList.size(); i++)
		{
			std::string & compName = aexResourceMgr->GetNameWithoutNamespaces(compList[i]->GetType().GetName());
			if (compName == /*(namespaceString + */compToCheck)
				return;
		}

		AddComp(GetCompByName(compToCheck.c_str()));
	}

	void GameObject::AddIfNotRepeatedComybyComp(IComp* comp)
	{
		if (std::find(mComps.begin(), mComps.end(), comp) == mComps.end())
			AddComp(comp);
	}
	// SPACE AND SCENE IMPLEMENTATION
	GameObject * GameObject::GetChildObject(const char * name)
	{
		std::vector<GameObject*>::iterator it = mChildren.begin();
		while (it != mChildren.end())
		{
			if ((*it)->GetBaseName() == name)
				return *it;
			it++;
		}

		return NULL;
	}

	void GameObject::AddChild(GameObject * obj)
	{
		mParentSpace->AddObject(obj, this);
	}

	void GameObject::AddChild(const char* name)
	{
		mParentSpace->AddObject(name, this);
	}

	void GameObject::RemoveChild(GameObject * obj)
	{
		std::vector<GameObject*>::iterator it = mChildren.begin();
		while (it != mChildren.end())
		{
			if (*it == obj)
			{
				mChildren.erase(it);
				return;
			}

			it++;
		}

	}

	void GameObject::RemoveChild(const char* name)
	{
		std::vector<GameObject*>::iterator it = mChildren.begin();
		while (it != mChildren.end())
		{
			if ((*it)->GetBaseName() == name)
			{
				mChildren.erase(it);
				return;
			}

			it++;
		}

	}

	void GameObject::RemoveTag(std::string tag)
	{
		FOR_EACH(it, mtags)
		{
			if (*it == tag)
			{
				mtags.erase(it);
				return;
			}
		}
	}

	bool GameObject::OnGui()
	{
		// Get components vector
		std::vector<IComp*> & Comps = GetComps();
		bool changed = false;

		for (u32 i = 0; i < Comps.size(); ++i)
		{
			std::string typeName = aexCompNameList->GetEditorName(aexResourceMgr->GetNameWithoutNamespaces(Comps[i]->GetType().GetName()).c_str());
			//typeName = aexResourceMgr->GetNameWithoutNamespaces(typeName);
			if (ImGui::TreeNode(typeName.c_str()))
			{
				std::string delMsg = std::string("Delete ") + typeName;

				if (ImGui::Button(delMsg.c_str()))
					ImGui::OpenPopup("Delete Comp");

				if (ImGui::BeginPopupModal("Delete Comp", NULL, ImGuiWindowFlags_AlwaysAutoResize))
				{
					delMsg = std::string("Are you sure you want to remove ") + typeName + std::string("component?\n");
					ImGui::Text(delMsg.c_str());
					ImGui::Separator();

					if (ImGui::Button("OK", ImVec2(120, 0)))
					{
						RemoveComp(Comps[i]);
						ImGui::CloseCurrentPopup();
						ImGui::EndPopup();
						ImGui::TreePop();
						continue;
					}

					ImGui::SetItemDefaultFocus();
					ImGui::SameLine();
					if (ImGui::Button("Cancel", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); }
					ImGui::EndPopup();
				}

				changed = Comps[i]->OnGui();
				ImGui::TreePop();
				if (changed)
					break;
			}
			ImGui::Separator();
		}
		ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(0.0f, 0.6f, 0.6f));
		ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(0.0f, 0.7f, 0.7f));
		ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(0.0f, 0.8f, 0.8f));

		static bool addCompClicked = false;
		if (ImGui::Button("                    Add Component                    "))
			addCompClicked = !addCompClicked;

		static bool inputingText = false;
		static ImGuiTextFilter myFilter;

		static const char* item_current = "Transform";//items[0];            // Here our selection is a single pointer stored outside the object.

		if (addCompClicked)
		{
			static ImGuiComboFlags flags = 0;

			//inputingText = inputingText == false ? (temp == true ? ) : ;
			/*std::string aexNamespace = "AEX::";
			const char* items[] = { "TransformComp", "SpriteComp", "SpriteAnimation", "SpineAnimation", "Camera", "Rigidbody", "IgnoreGravityEffect",
				"CircleCollider", "BoxCollider","ParticleEmitter", "EnemyGrounded", "EnemyKamikaze", "EnemyActivator" , "PlayerStats", "PlayerController",
				"Follower", "PlatformController", "ShootController", "BulletLogic", "DashLogic", "FootLogic", "SoundEmitter", "SoundListener"};*/

			if (ImGui::BeginCombo("Component list", item_current, flags)) // The second parameter is the label previewed before opening the combo.
			{
				const std::map<std::string, const char *> & map = aexCompNameList->GetNames();
				auto endIt = map.cend();
				//int n;
				//for (n = 0; n < IM_ARRAYSIZE(items); n++)
				for (auto it = map.cbegin(); it != endIt; it++)
				{
					if (myFilter.PassFilter(it->second))
					{
						bool is_selected = (item_current == it->second);//items[n]);
						if (ImGui::Selectable(it->second/*items[n]*/, is_selected))
						{
							item_current = it->second;//items[n];
							//std::string itemName_str = item_current;
							AddIfNotRepeatedComp(Comps,/* aexNamespace, */it->first);
							changed = true;
							addCompClicked = false;
							inputingText = false;
						}
					}
				}

				ImGui::EndCombo();
			}

			bool temp = myFilter.Draw("Search Component");
			if (inputingText == false)
				inputingText = temp;

			if (inputingText)
			{
				//ImGui::BeginCombo("Component list", item_current, flags);

				const std::map<std::string, const char *> & map = aexCompNameList->GetNames();
				auto endIt = map.cend();
				//int n;
				//for (n = 0; n < IM_ARRAYSIZE(items); n++)
				for (auto it = map.cbegin(); it != endIt; it++)
				{
					if (myFilter.PassFilter(it->second))
					{
						bool is_selected = (item_current == it->second);//items[n]);
						if (ImGui::Selectable(it->second/*items[n]*/, is_selected))
						{
							item_current = it->second;//items[n];
							//std::string itemName_str = item_current;
							AddIfNotRepeatedComp(Comps,/* aexNamespace, */it->first);
							changed = true;
							addCompClicked = false;
							inputingText = false;
						}
					}
				}

				//ImGui::EndCombo();
			}
		}
		else
		{
			inputingText = false;
			myFilter.Clear();
			item_current = "Transform";
		}

		ImGui::PopStyleColor(3);

		return changed;
	}

	bool AEX::GameObject::IsGameObjectInScreen()
	{
		GameObject * cam = aexScene->FindObjectInANYSpace("camera");
		if (cam == NULL)
			return false;
		f32 Wwidht = (f32)WindowMgr->GetCurrentWindow()->GetWidth();
		f32 Wheight = (f32)WindowMgr->GetCurrentWindow()->GetHeight();

		f32 AspectRatio = Wwidht / Wheight;

		TransformComp* transform = GetComp<TransformComp>();
		if (transform == NULL) //NO TRANSFORM
			return false;

		return StaticRectToStaticRect(&AEVec2(transform->GetPosition3D().x, transform->GetPosition3D().y), transform->GetScale().x, transform->GetScale().y, 
			&AEVec2(cam->GetComp<TransformComp>()->GetPosition3D().x, cam->GetComp<TransformComp>()->GetPosition3D().y), 2 * AspectRatio * cam->GetComp<Camera>()->mViewRectangleSize,
			2 * cam->GetComp<Camera>()->mViewRectangleSize);
	}

	#pragma endregion


	#pragma region // SERIALIZATION
	/*nlohmann::json & operator<<(nlohmann::json & j, const GameObject & obj)
	{
		obj.operator<<(j);
		return j;
	}
	GameObject & operator >> (nlohmann::json & j, GameObject & obj)
	{
		obj.operator >> (j);
		return obj;
	}
	std::ostream & operator<<(std::ostream & o, const GameObject & obj)
	{
		json j;
		obj.operator<<(j);
		return o << j;
	}*/

	// Serialization functions (Miguel)
	void GameObject::FromJson(json & value)
	{
		mComps.clear();
		mChildren.clear();
		mtags.clear();

		SetBaseName((value["Name"].get<std::string>()).c_str());
		SetEnabled(value["Enabled"].get<bool>());
		mbActive = value["Active"].get<bool>();
		mbAlive = value["Alive"].get<bool>();
		//mbVisible = value["Visible"].get<bool>();			Are we no longer using mbVisible?
		if (value["Saved To Archetype"] != nullptr)
			mSavedToArchetype = value["Saved To Archetype"].get<bool>();

		if (value["Selectable"] != nullptr)
			selectable = value["Selectable"].get<bool>();

		json tagsVal = value["Tags"];

		FOR_EACH(jsonIt, tagsVal)
		{
			mtags.push_back(*jsonIt);
		}

		/*if (std::string(GetBaseName()) == "player")
		{
			//__debugbreak();
		}*/

		json compsVal = value["Components"];
		//std::cout << "CompJsonSize = " << compsVal.size()<<std::endl;

		FOR_EACH(jsonIt, compsVal)
		{
			if ((*jsonIt)["RTTI Type"] == nullptr)
			{
				//std::cout << "This component's RTTI type was not serialized" << std::endl;
				//assert(0);
				continue;
			}

			if (IComp * newComp = dynamic_cast<IComp *>(aexFactory->Create((*jsonIt)["RTTI Type"].get<std::string>().c_str())))
			{
				//static int transformCount = 0;

				// This is used to be below the next 2 lines. If serialization unexpectedly starts crashing, this may be the problem.
				newComp->FromJson(*jsonIt);
				newComp->mOwner = this;
				AddComp(newComp);
			}
			else ASSERT(0, NULL); // HARD CRASH
		}

		//if (std::string(GetBaseName()) == "Main Object")// && newComp->GetType().SameTypeAs(TransformComp::TYPE()))
		//{
		
		//std::cout << std::endl;
			//std::cout << GetBaseName() << std::endl;
			//std::cout << "---------------------------------------------------------" << std::endl;
			//FOR_EACH(compIt, mComps)
			//{
			//	std::cout << (*compIt)->GetType().GetName() << std::endl;
			//}
			//transformCount++;
			//std::cout << "---------------------------------------------------------" << std::endl<<std::endl;

			//__debugbreak();
		//}

		json childsVal = value["Children"];
		FOR_EACH(jsonIt, childsVal)
		{
			if ((*jsonIt)["RTTI Type"] == nullptr)
			{
				//std::cout << "This child's RTTI type was not serialized" << std::endl;
				//assert(0);
				continue;
			}

			if (GameObject * childObj = dynamic_cast<GameObject *>(aexFactory->Create((*jsonIt)["RTTI Type"].get<std::string>().c_str())))
			{
				//childObj->SetParent(this);
				childObj->SetParentSpace(this->mParentSpace);
				childObj->FromJson((*jsonIt));
				AddChild(childObj);

				//std::cout << "Number of childs (inside loop) = " << mChildren.size() << std::endl;
			}
			else ASSERT(0, NULL); // HARD CRASH
		}
		//std::cout << "Number of children: "<<GetAllChildren().size() << std::endl;
		//FOR_EACH(childrenIt, GetAllChildren())
		//{
		//	//std::cout << "-------------------------------------------" << std::endl;
		//	//std::cout << "Child name: " << (*childrenIt)->GetBaseName() << std::endl;
		//	//std::cout << "-------------------------------------------" << std::endl;
		//}
	}

	void GameObject::ToJson(json & value)
	{
		value["Name"] = GetBaseName();
		value["RTTI Type"] = GetType().GetName();
		value["Active"] = mbActive;
		value["Alive"] = mbAlive;
		value["Enabled"] = mbEnabled;
		value["Saved To Archetype"] = mSavedToArchetype;
		value["Selectable"] = selectable; 

		const std::vector<std::string> & tags = GetTags();

		FOR_EACH(it, tags)
		{
			json tagVal;
			value["Tags"].push_back(*it);
		}

		FOR_EACH(it, mComps)
		{
			json compVal;
			(*it)->ToJson(compVal);
			value["Components"].push_back(compVal);
		}

		/*json parentVal;
		mParent->ToJson(parentVal);
		value["Parent Object"] = parentVal;*/

		// We don't need to serialize this
		/*json parentSpaceVal;
		mParentSpace->ToJson(parentSpaceVal);
		value["Parent Space"] = parentSpaceVal;*/

		FOR_EACH(it, mChildren)
		{
			json childVal;
			(*it)->ToJson(childVal);
			value["Children"].push_back(childVal);
		}
	}


	void GameObject::SaveToArchetype(const char * fileName)
	{
		std::string path = "data/Archetypes/";
		path += fileName;
		json value;
		ToJson(value);
		SaveToFile(value, path.c_str());
		mSavedToArchetype = true;
	}

	#pragma endregion


	//const f32 RAY_OFFSET = 2.0f;


	// This function is always called from the selected go. The transform passed
	// as parameter is the transform of each of the objects to check collision with.
	void GameObject::CheckSnapping(TransformComp * trComp)
	{
		static AEVec2 initMousePos;
		static AEVec2 mousePos;
		static bool dragStarted = false;

		static AEVec2 thisCenters[4];
		static AEVec2 centers[4];
		static int thisCenterIndex = -1;

		std::string name = trComp->mOwner->GetBaseName();

		if (aexInput->MouseTriggered(Input::Keys::eMouseLeft))
		{
			initMousePos = Editorstate->mEditorCamera->PointToWorld(aexInput->GetGLFWMousePos());

			TransformComp * tr = GetComp<TransformComp>();

			if (tr == nullptr)
				return;

			SpriteComp* objSprite = GetComp<SpriteComp>();
			AEVec2 texSize = objSprite ? AEVec2(static_cast<f32>(objSprite->mTex->get()->GetWidth()) / 100.0f, static_cast<f32>(objSprite->mTex->get()->GetHeight() / 100.0f)) : AEVec2(1.0f, 1.0f);
			AEVec2 thisScale(tr->mLocal.mScale.x * texSize.x, tr->mLocal.mScale.y * texSize.y);

			AEVec2 thisPos(tr->mLocal.mTranslationZ.x, tr->mLocal.mTranslationZ.y);
			
			f32 thisRotation = tr->mLocal.mOrientation;

			AEMtx33 trMtx = AEMtx33::Translate(thisPos.x, thisPos.y);
			AEMtx33 rotMtx = AEMtx33::RotRad(thisRotation);
			AEMtx33 scaleMtx = AEMtx33::Scale(thisScale.x / 2.0f, thisScale.y / 2.0f);

			thisCenters[0] = { 1,1 };
			thisCenters[1] = { 1,-1 };
			thisCenters[2] = { -1,-1 };
			thisCenters[3] = { -1,1 };

			f32 smallerScale = thisScale.x < thisScale.y ? thisScale.x : thisScale.y;
			f32 radius = smallerScale / SNAP_RADIUS_DIVISOR;

			for (int i = 0; i < 4; i++)
			{
				thisCenters[i] = trMtx * rotMtx * scaleMtx * thisCenters[i];

				if (StaticPointToStaticCircle(&initMousePos, thisCenters + i, radius))
				{
					thisCenterIndex = i;
					break;
				}
			}

			dragStarted = true;
		}

		if (thisCenterIndex == -1)
			return;

		if (aexInput->MousePressed(Input::Keys::eMouseLeft))
		{
			trComp->DrawCornerCircles();
			GfxMgr->DrawRectangle(trComp->mWorld, AEVec4(255, 255, 255, 1.0f));

			mousePos = Editorstate->mEditorCamera->PointToWorld(aexInput->GetGLFWMousePos());
			GfxMgr->DrawLine(DebugLine(AEVec4(0,1,0,1), initMousePos, mousePos));
		}
		else if (dragStarted == true)
		{
			// Here I compute the centers of the other object's circles
			AEVec2 pos(trComp->mWorld.mTranslationZ.x, trComp->mWorld.mTranslationZ.y);

			SpriteComp* objSprite = trComp->mOwner->GetComp<SpriteComp>();
			AEVec2 texSize = objSprite ? AEVec2(static_cast<f32>(objSprite->mTex->get()->GetWidth()) / 100.0f, static_cast<f32>(objSprite->mTex->get()->GetHeight() / 100.0f)) : AEVec2(1.0f, 1.0f);
			AEVec2 scale(trComp->mWorld.mScale.x * texSize.x, trComp->mWorld.mScale.y * texSize.y);

			f32 rot = trComp->mWorld.mOrientation;
			
			AEMtx33 trMtx = AEMtx33::Translate(pos.x, pos.y);
			AEMtx33 rotMtx = AEMtx33::RotRad(rot);
			AEMtx33 scaleMtx = AEMtx33::Scale(scale.x / 2.0f, scale.y / 2.0f);

			centers[0] = { 1, 1 };
			centers[1] = { 1, -1 };
			centers[2] = { -1, -1 };
			centers[3] = { -1, 1 };

			f32 smallerScale = scale.x < scale.y ? scale.x : scale.y;
			f32 radius = smallerScale / SNAP_RADIUS_DIVISOR;

			//AEVec2 prevMousePos = Editorstate->mEditorCamera->PointToWorld(aexInput->GetPrevMousePos());

			for (int i = 0; i < 4; i++)
			{
				centers[i] = trMtx * rotMtx * scaleMtx * centers[i];

				if (StaticPointToStaticCircle(&mousePos, centers + i, radius))
				{
					AEVec2 displacement = centers[i] - thisCenters[thisCenterIndex];

					// If we have reached this point of the function, tr is never going to be NULL
					TransformComp * tr = GetComp<TransformComp>();
					tr->mLocal.mTranslationZ.x += displacement.x;
					tr->mLocal.mTranslationZ.y += displacement.y;
					tr->translationChanged = true;
					tr->transDisplacement = displacement;
					thisCenterIndex = -1;
					dragStarted = false;
					break;
				}
			}
		}
	}


	void GameObject::CopyObject()
	{

		// Create a json to store the GO
		json GO;
		ToJson(GO);
		std::stringstream ss;
		ss << "data/Copied" << GetBaseName() << ".json";
		// Serialize it to a file
		SaveToFile(GO, ss.str());
	}

	void GameObject::PasteObject()
	{
		json GO;

		std::stringstream ss;
		ss << "data/Copied" << GetBaseName() << ".json";

		LoadFromFile(GO, ss.str());

		GameObject* newGO = aexFactory->Create<GameObject>();

		newGO->FromJson(GO);

		newGO->GetBaseNameString().append(" copy");

		/*TransformComp* thistransform = GetComp<TransformComp>();  THIS WILL BE NICE TO PASTE THE OBJECT IN THE POSITION OF THE MOUSE
		TransformComp* transform = newGO->GetComp<TransformComp>();
		if (thistransform)
		{
			transform->SetPosition3D(Editor->mgizmos->mousepos.x, Editor->mgizmos->mousepos.y, thistransform->GetPosition3D().z);
		}*/
		GetParentSpace()->AddObject(newGO, GetParentSpace());
	}
}