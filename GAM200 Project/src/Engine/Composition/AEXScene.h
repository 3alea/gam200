#ifndef AEX_SCENE_H_
#define AEX_SCENE_H_
#include "Player/Stats/PlayerStats.h"
#include "AEXGameObject.h"
#include "AEXSpace.h"
#include "../Core/AEXSerialization.h"
#include "src/Engine/Core/AEXSystem.h"
#include <queue>


namespace AEX
{
	class Voice;

	class Scene : public ISerializable, public ISystem
	{
		AEX_RTTI_DECL(Scene, ISystem);
		AEX_SINGLETON(Scene);
	public:
		// delete all spaces
		virtual ~Scene();

		bool Initialize();
		void Shutdown();
		void Update();

		std::vector<Space*> mSpaces;


		//FUNCTION TO WORK WITH SPACES
			//adds the space
		Space* AddSpace(Space* space);
		Space* AddSpace(const char* name);
			//finds the space
		Space* FindSpace(const char* name);
			//deletes the space
		int DeleteSpace(const char* name);
		int DeleteSpace(Space* space);

		void FreeDeadObjects();

		//FUNCTION TO WORK WITH OBJECTS
			//finds object in the specific space
		GameObject* FindObjectInSpace(const char* name, Space* space = NULL);
		GameObject* FindObjectInSpace(const char* name, const char* spacename = NULL);

			// finds objects in all the spaces
		GameObject* FindObjectInANYSpace(const char* name);
		void FindAllObjectsInANYSpace(const char* name, std::vector<GameObject*>& outVector);

		void OnGui();

		// Serialization functions (Miguel)
		void FromJson(json & value);
		void ToJson(json & value);

		bool mbAlreadySaved = false;

		bool mPathsUpdatedOnce = false;

		// Added by Miguel

		// Use this function if you want to change the level inside the game.
		// You need to pass the path to the json file (with the extension).
		void ChangeLevel(const std::string & levelToLoad);

		void SetPauseState(bool isPaused);

		std::queue<std::string> mLevelsToLoad;
		bool mGamePaused = false;
		static bool bFadeTransition;

		bool mOnPauseMenu = false;

		// Set this boolean to true at any point during the game to exit the application
		static bool bExitGame;

		// Use this function to pause/unpause the game.
		// Pauses the game if it is not paused.
		// Unpauses the game if it is paused.
		void PauseOrUnPause();

		Voice * LevelMusic = nullptr;

		bool doneOnce = false;

	private:

		void LoadMenuAudios();
		void LoadTutorialAudios();
		void LoadVeinsAudios();
		void LoadStationAndBoilerRoomAudios();
		void LoadCarLevel1Audios();
		void LoadCarLevel2Audios();
		void LoadStomach1Audios();
		void LoadStomach2Audios();
		void LoadLungsAudios();
		void LoadFinalBossAudios();
		void LoadAudioFolder(const char * folderName);
	};

	#define aexScene Scene::Instance()
}

/*	SOME USAGE CODE TO COMPLY WITH:

aexScene->FindObject("anObject"); // looks in the default space
aexSCene->FindObject("anObject", aexScene->GetSpace("HUD");

// default space
aexScene->NewObject("newObjectName");
aexScene->NewObjectFile("newObjectName", "myObject.json"); // same as archetype since archetype are in json

aexScene->NewObject("newObjectName", aexScene->GetSpace("HUD"));
aexScene->NewObjectFile("newObjectName", "myObject.json", aexScene->GetSpace("HUD")); // same as archetype since archetype are in json

// ultimate final thing 
aexScene->Load("level_1.json");



*/


#endif