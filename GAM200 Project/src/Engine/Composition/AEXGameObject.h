// ----------------------------------------------------------------------------
// Project: GAM300 - Sample Engine
// File:	IComp.h
// Purpose:	Header file for the IComp API
// Author:	Thomas Komair
//
// Copyright DigiPen Institute of Technology
// ----------------------------------------------------------------------------
#ifndef AEX_OBJECT_H_
#define AEX_OBJECT_H_
// ----------------------------------------------------------------------------
#include "AEXComponent.h"
#include <string>// this must be in another file
#include "src\Engine\Utilities\AEXAssert.h"
#include "src/Engine/Composition/AEXFactory.h"
#include "src\Engine\Composition\AEXCloning.h"
#include <stack>

#pragma warning (disable:4251) // dll and STL
namespace AEX
{
	class TransformComp;
	class Spawner;

	class GameObject : public virtual IBase, public ISerializable
	{
		AEX_RTTI_DECL(GameObject, IBase);

		friend class Space;

	public:

		GameObject();
		virtual ~GameObject();

		// State Methods
		virtual void SetEnabled(bool enabled); // Call Set Enabled on all components
		virtual void Initialize();	// Calls initialize on all components
		virtual void Render();
		virtual void Shutdown();

		void DrawDebug();

		// --------------------------------------------------------------------
		#pragma region// COMPONENT MANAGEMENT
		// Getters
		u32 GetCompCount() const;
		IComp* GetComp(u32 index)const;
		IComp* GetComp(const char * type)const;
		template<class T>
		T* GetComp()const
		{
			// go throught the components and look for the same type
			for (auto it = mComps.begin(); it != mComps.end(); ++it)
			{
				std::string typeName = (*it)->GetType().GetName();
				if (typeName == T::TYPE().GetName())
				{
					return dynamic_cast<T*>(*it);
				}
			}

			ASSERT(0, "Component not found");
			return NULL;
		}

		IComp* GetComp(const Rtti & type)const;
		IComp* GetCompName(const char * compName, const char *compType = NULL)const;
		u32 GetID() { return mId; }
		//char* GetName() { return mName; }

		// Setters
		//void SetName(const char * newName) { strcpy_s(mName, newName); }

		// template
		template<class T>
		T* GetComp(); 
		template<class T>
		T* GetCompDerived(const char * name = NULL);

		// Add/Remove by address
		void AddComp(IComp * pComp);
		void RemoveComp(IComp * pComp, bool del = true);

		// Removes first component encoutered that match the search criteria
		void RemoveCompType(const char * compType);
		void RemoveCompType(const Rtti & compType);
		void RemoveCompName(const char * compName, const char * compType = NULL);

		// Removes all components encoutered that match the search criteria
		void RemoveAllCompType(const char * compType);
		void RemoveAllCompType(const Rtti & compType);
		void RemoveAllCompName(const char * compName, const char * compType = NULL);

		// Remove all components
		void RemoveAllComp();
		#pragma endregion

		#pragma region // SERIALIZATION
		/*virtual json& operator<< (json&j)  const;
		virtual void operator >> (json&j);
		friend nlohmann::json& operator<<(nlohmann::json& j, const GameObject& obj);
		friend GameObject& operator >> (nlohmann::json& j, GameObject& obj);
		virtual std::ostream& operator << (std::ostream & o) const;
		friend std::ostream& operator<< (std::ostream & o, const GameObject& obj);*/


		//Serialization functions (Miguel)
		void FromJson(json & value);
		void ToJson(json & value);
		void SaveToArchetype(const char * fileName);

		#pragma endregion
		// Cloning
		virtual GameObject* clone();

		// debug only!!
		std::vector<IComp*> &GetComps()  { return mComps; }
		const std::vector<IComp*> &GetComps() const { return mComps; }
		//GameObject* GetChildObj() { return mChildObj; }
		//void		SetChildObj(GameObject* childObj) { mChildObj = childObj; }

		//Scene and space application
		GameObject * GetParent() { return mParent;  }
		std::vector< GameObject*>& GetAllChildren() { return mChildren;  }
		GameObject * GetChildObject(const char * name);
		//Gettor added by Miguel. Needed it for serialization.
		Space * GetParentSpace() { return mParentSpace; }

		// Settors added by Miguel because needed them to complete serialization. Need to talk with Unai.
		void SetParent(GameObject * newParent) { mParent = newParent; }
		void SetParentSpace(Space * newSpace) { mParentSpace = newSpace; }


		//Tag
			//getter
		const std::vector<std::string> & GetTags() const { return mtags; }
			//setter
		void SetTag(std::string tag) { mtags.push_back(tag); }
		void RemoveTag(std::string tag);

		//add remove children implementation
		void		AddChild(GameObject * obj);
		void		AddChild(const char* name);  //add by name
		void		RemoveChild(GameObject * obj);	// detach child only, doesn't destroy memory
		void		RemoveChild(const char* name); // remove by name


		// Added by Miguel for snapping
		void CheckSnapping(TransformComp * trComp);

	public:

		bool OnGui();
		bool IsGameObjectInScreen();

		// multiple selection(unai)
		AEVec2 diffRespecttoBox;
		void SelectableLogic()
		{
			if (GetTags().size() == 0)
			return;
			
			for(unsigned i = 0 ; i < GetTags().size(); i++)
			{

				if (GetTags().at(i) == "background")
					selectable = false;
				// add more if u want
			}
		}
		bool selectable = true;
		// --------------------------------------------------------------------

	public:
		void AddIfNotRepeatedComp(const std::vector<IComp *> & compList, /*const std::string & namespaceString, */const std::string & compToCheck);
		void AddIfNotRepeatedComybyComp(IComp* comp);
	public:
		// Management
		bool			mbActive;		// Specifies whether the object should be updated or not.
		bool			mbAlive;		// Specifies whether the object is alive or not.

		static int		objectID;		// Object ID

		// Used by Miguel in archetypes
		bool			mSavedToArchetype;
		char			mEditorArchetypeName[128];

		// Added by Miguel for snapping
		bool mbSnapped = false;

		// Added by Miguel for wave system
		Spawner * mSpawnerOwner = nullptr;

	protected:
		std::vector<IComp*> mComps;	// Component list
		bool mbEnabled;

		std::vector<GameObject*> mChildren;
		GameObject* mParent;		
		Space * mParentSpace;

	private:
		u32				mId;			// Integer Id generated by the ObjectManager when created. Guaranteed to be unique.
		std::vector<std::string> mtags;

		//unsigned int VAO;
		//unsigned int VBO;
		//unsigned int EBO;

		public:
			void CopyObject(); 
			void PasteObject();
	};



// ----------------------------------------------------------------------------
	template<class T>
	T* GameObject::GetComp()
	{
		if (this == nullptr) // porsiaca de los acas
			return nullptr;

		if (mComps.empty())
			return nullptr;

		// go throught the components
		for (auto it = mComps.begin(); it != mComps.end(); ++it)
		{
			T* temp;
			temp = dynamic_cast<T*>(*it);
			if (temp)// found
				return temp;
		}
		return nullptr; // not found
	}

	template<class T>
	T* GameObject::GetCompDerived(const char * compName)
	{
		// go throught the components and look for the same type
		for (auto it = mComps.begin(); it != mComps.end(); ++it)
		{
			if ((*it)->GetType().IsDerived(T::TYPE()))
			{
				// not same name -> continue
				if (compName && strcmp(compName, (*it)->GetName()) != 0)
					continue;

				// same name or don't care about the name
				// return
				return (T*)(*it);
			}
		}

		return NULL;
	}

	typedef std::list<GameObject*> OBJECT_PTR_LIST;
	typedef std::vector<GameObject*> OBJECT_PTR_VECTOR;
}

#pragma warning (default:4251) // dll and STL
// ----------------------------------------------------------------------------
#endif