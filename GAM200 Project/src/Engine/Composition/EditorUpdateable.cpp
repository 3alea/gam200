#include "EditorUpdateable.h"
#include "Utilities\AEXContainers.h"


namespace AEX
{
	EditorUpdateableComp::~EditorUpdateableComp()
	{
		Shutdown();
	}

	void EditorUpdateableComp::Update() {}

	void EditorUpdateableComp::Initialize()
	{
		aexEditorUpdateableComps->AddComp(this);
	}

	void EditorUpdateableComp::Shutdown()
	{
		aexEditorUpdateableComps->RemoveComp(this);
	}

	void EditorUpdateableComp::FromJson(json & value)
	{
		//this->IComp::FromJson(value);
		SetBaseName(value["Name"].get<std::string>().c_str());
		SetEnabled(value["Enabled"].get<bool>());
		mbRemovable = value["Removable"].get<bool>();
		mbGlobalComp = value["GlobalComponent"].get<bool>();
	}
	void EditorUpdateableComp::ToJson(json & value)
	{
		//this->IComp::ToJson(value);
		value["Name"] = GetBaseName();
		value["RTTI Type"] = GetType().GetName();
		value["Enabled"] = mbEnabled;
		value["Removable"] = mbRemovable;
		value["GlobalComponent"] = mbGlobalComp;
	}

	void EditorUpdateables::Update()
	{
		FOR_EACH(it, mCompsToUpdate)
		{
			(*it)->Update();
		}
	}
	
	void EditorUpdateables::AddComp(IComp * comp)
	{
		mCompsToUpdate.remove(comp); // no duplicates
		mCompsToUpdate.push_back(comp);
	}

	void EditorUpdateables::RemoveComp(IComp * comp)
	{
		mCompsToUpdate.remove(comp);
	}

	void EditorUpdateables::ClearComps()
	{
		mCompsToUpdate.clear();
	}
}