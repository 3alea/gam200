// ----------------------------------------------------------------------------
// Project: GAM300 - Sample Engine
// File:	IComp.h
// Purpose:	Header file for the IComp API
// Author:	Thomas Komair
//
// Copyright DigiPen Institute of Technology
// ----------------------------------------------------------------------------
#include "AEXComponent.h"
#include "AEXGameObject.h"
#include "Composition/AEXFactory.h"
#include "../Imgui/imgui.h"
#include "../Components/AEXComponents.h"
#include "Components\Collider.h"
#include "Components\Rigidbody.h"
#include "Player/Stats/PlayerStats.h"

#include "Player/Movement/PlayerController.h"

#include "Follow/Follower.h"

#include "Player/Movement/FootLogic.h"

#include "Environment/PlatformController.h"

#include "Player/Mechanics/ShootController.h"

#include "Bullets/BulletLogic.h"

#include "Player/Mechanics/DashLogic.h"
#include "Components/IgnoreGravityEffect.h"
#include "Components\SpineAnimation.h"
#include "Components\RunningTrack.h"

#include "AEXComponent.h"
#include "src/Gameplay/Enemy/EnemyGrounded.h"
#include "src/Gameplay/Enemy/EnemyFlying.h"
#include "src/Gameplay/Enemy/EnemyKamikaze.h"
#include "src/Gameplay/Enemy/EnemyBullet.h"
#include "src/Gameplay/Activator/EnemyActivator.h"
#include "src/Engine/Audio/AudioManager.h"

namespace AEX
{
	// ----------------------------------------------------------------------------
	// Constructors
	IComp::IComp() : mOwner(NULL), mbEnabled(true) { /*PushIfNewComponent();*/ }

	IComp::~IComp() {}

	GameObject* IComp::GetOwner(void)
	{
		// return a shared pointer
		return mOwner;
	}

	/*json & IComp::operator<<(json & j) const { return j; }

	void IComp::operator >> (json & j) { }

	std::ostream & IComp::operator<<(std::ostream & o) const
	{
		json j;
		this->operator<<(j);
		return o << j;
	}*/

	// @TODO
	void IComp::PushIfNewComponent()
	{
		/*for (unsigned i = 0; i < mAllComponentList.size(); i++)
			if (mAllComponentList[i] == this->GetType().GetName())
				return;

		mAllComponentList.push_back(this->GetType().GetName());*/
	}

	// ----------------------------------------------------------------------------
	// PUBLIC - State Methods - By default they do nothing in this component
	//void IComp::OnCreate()
	//{}
	void IComp::Initialize()
	{}
	void IComp::Shutdown()
	{}
	void IComp::Update()
	{}

	// ----------------------------------------------------------------------------
	bool IComp::IsEnabled()
	{
		return mbEnabled;
	}

	void IComp::SetEnabled(bool enabled)
	{
		mbEnabled = enabled;
	}

	/*nlohmann::json & operator<<(nlohmann::json & j, const IComp & comp)
	{
		comp.operator<<(j);
		return j;
	}

	IComp & operator >> (nlohmann::json & j, IComp & comp)
	{
		comp.operator >> (j);
		return comp;
	}

	std::ostream & operator<<(std::ostream & o, const IComp & comp)
	{
		json j;
		comp.operator<<(j);
		return o << j;
	}*/

	IComp * GetCompByName(const char * name)
	{
		return dynamic_cast<IComp *>(aexFactory->Create(name));

		/*if (!strcmp(name, "TransformComp"))
			return new TransformComp;
		if (!strcmp(name, "Sprite"))
			return new SpriteComp;
		if (!strcmp(name, "SpriteAnimation"))
			return new SpriteAnimationComp;
		if (!strcmp(name, "Camera"))
			return new Camera;
		if (!strcmp(name, "Rigidbody"))
			return new Rigidbody;
		if (!strcmp(name, "CircleCollider"))
			return new CircleCollider;
		if (!strcmp(name, "BoxCollider"))
			return new BoxCollider;
		if (!strcmp(name, "PlayerController"))
			return new PlayerController;
		if (!strcmp(name, "PlayerStats"))
			return new PlayerStats;
		if (!strcmp(name, "Follower"))
			return new Follower;
		if (!strcmp(name, "PlatformController"))
			return new PlatformController;
		if (!strcmp(name, "ShootController"))
			return new ShootController;
		if (!strcmp(name, "BulletLogic"))
			return new BulletLogic;
		if (!strcmp(name, "DashLogic"))
			return new DashLogic;
		if (!strcmp(name, "EnemyGrounded"))
			return new EnemyGrounded;
		if (!strcmp(name, "EnemyKamikaze"))
			return new EnemyKamikaze;
		if (!strcmp(name, "BulletComp"))
			return new BulletComp;
		if (!strcmp(name, "EnemyActivator"))
			return new EnemyActivator;
		if (!strcmp(name, "FootLogic"))
			return new FootLogic;
		if (!strcmp(name, "SoundEmitter"))
			return new SoundEmitter;
		if (!strcmp(name, "SoundListener"))
			return new SoundListener;
		if (!strcmp(name, "IgnoreGravityEffect"))
			return new IgnoreGravityEffect;
		if (!strcmp(name, "SpineAnimation"))
			return new SpineAnimationComp;
		if (!strcmp(name, "ParticleEmitter"))
			return new ParticleEmitter;*/
		
		/*if (!strcmp(name, "EnemyBullet"))
			return new EnemyBullet;*/


		//if (!strcmp(name, "AABBCollider"))
		//	return new AABBCollider;
		//if (!strcmp(name, "OBBCollider"))
		//	return new OBBCollider;

		return NULL;
	}

	void IComp::DeleteCompOnGui(const char* name)
	{
		if (ImGui::Button("Delete %s"), name)
			ImGui::OpenPopup("Delete %s?");

		if (ImGui::BeginPopupModal("Delete?", NULL, ImGuiWindowFlags_AlwaysAutoResize))
		{
			ImGui::Text("All those beautiful files will be deleted.\nThis operation cannot be undone!\n\n");
			ImGui::Separator();

			//static int dummy_i = 0;
			//ImGui::Combo("Combo", &dummy_i, "Delete\0Delete harder\0");

			static bool dont_ask_me_next_time = false;
			ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0, 0));
			ImGui::Checkbox("Don't ask me next time", &dont_ask_me_next_time);
			ImGui::PopStyleVar();

			if (ImGui::Button("OK", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); }
			ImGui::SetItemDefaultFocus();
			ImGui::SameLine();
			if (ImGui::Button("Cancel", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); }
			ImGui::EndPopup();
		}
	}

	// Serialization functions (Miguel)
	void IComp::FromJson(json & value)
	{
		//std::cout << "FromJson of IComp" << std::endl;
		SetBaseName(value["Name"].get<std::string>().c_str());
		SetEnabled(value["Enabled"].get<bool>());
		mbRemovable = value["Removable"].get<bool>();
		mbGlobalComp = value["GlobalComponent"].get<bool>();
	}

	void IComp::ToJson(json & value)
	{
		value["Name"] = GetBaseName();
		value["RTTI Type"] = GetType().GetName();
		value["Enabled"] = mbEnabled;
		value["Removable"] = mbRemovable;
		value["GlobalComponent"] = mbGlobalComp;
	}
}