#pragma once
//#include <list>
#include "Core/AEXSystem.h"
#include "Resources/AEXResourceManager.h"


namespace AEX
{
	class ComponentList
	{
		AEX_SINGLETON(ComponentList);
	
	public:
	
		// AddCompName assumes user won't introduce two times the same name

		template <typename T>
		void AddCompName(const char * editorName)
		{
			mNames[aexResourceMgr->GetNameWithoutNamespaces(T::TYPE().GetName())] = editorName;
		}

		template <typename T>
		void RemoveCompName(const char * editorName)
		{
			std::string & noNamespacesName = aexResourceMgr->GetNameWithoutNamespaces(T::TYPE.GetName());

			auto found = mNames.find(noNamespacesName);

			if (found != mNames.end())
				mNames.erase(found);
		}

		const char * GetEditorName(const char * key_RttiType);

		// Use this getter to get the map of names to be able to loop through it.
		const std::map<std::string, const char *> & GetNames() const;
	
	private:
		//		  RTTI Type   Name of editor
		std::map<std::string, const char *> mNames;
	};
	
	#define aexCompNameList (ComponentList::Instance())

}