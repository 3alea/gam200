#include "ComponentList.h"


namespace AEX
{
	/*void ComponentList::AddCompName(const char * nameWithoutNamespaces)
	{
		mNames.push_back(nameWithoutNamespaces);
	}*/

	const char * ComponentList::GetEditorName(const char * key_RttiType)
	{
		auto found = mNames.find(key_RttiType);

		if (found == mNames.end())
			return nullptr;

		return found->second;
	}

	const std::map<std::string, const char *> & ComponentList::GetNames() const
	{
		return mNames;
	}

	/*void ComponentList::RemoveCompName(const char * nameWithoutNamespaces)
	{
		FOR_EACH(it, mNames)
		{
			if (!strcmp(nameWithoutNamespaces, *it))
			{
				mNames.erase(it);
				return;
			}
		}
	}*/
}