#include "AEX.h"
#include "Core/AEXSystem.h"
#include "Platform/AEXInput.h"
#include "SRC/Engine/Editor/Editor.h"
#include "src/Engine/Composition/AEXScene.h"
#include "src/Engine/Composition/AEXFactory.h"
#include "Composition\EditorUpdateable.h"
#include "Physics/CollisionGroups.h"
#include "Utilities\RNG.h"
#include "Components/PausedLogic.h"
#include "Menu/PauseMenu/PauseMenuLogic.h"


namespace AEX {

bool ActivateAudio = true;
	// Helper function to load the level
	static void actual_change_level(const std::string & levelToLoad)
	{
		// Free all the necessary stuff
		Scene::ReleaseInstance();

		Editor->imGui_selectedGO = NULL;

		// Load the level
		json levelJson;
		LoadFromFile(levelJson, levelToLoad);

		aexScene->Shutdown();
		aexCollisionSystem->Shutdown();
		aexScene->FromJson(levelJson);
	}

	// Helper function to pause the game
	static void PauseGameLogic()
	{

		std::string & sceneName = aexScene->GetBaseNameString();

		if (((aexInput->KeyTriggered(GLFW_KEY_ESCAPE) && PauseMenuLogic::mSelectionTimer == 0.0f) || aexInput->GamepadButtonTriggered(Input::GamepadButtons::eGamepadButtonStart)) && sceneName != "Main Menu" && sceneName != "EndCredits" && PauseMenuLogic::mAreYouSureActive == false && PauseMenuLogic::mHowToPlayActive == false && PauseMenuLogic::mOptionsActive == false)
		{
			audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/OptionSelection/14 - Gun Insert Magazine Into Ak-47.wav"), Voice::SFX);
			aexScene->PauseOrUnPause();
		}
	}



	AEXEngine::~AEXEngine()
	{
		FRC::ReleaseInstance();
		Input::ReleaseInstance();
	}

	bool AEXEngine::Initialize()
	{
		// Initialize all the systems of the engine

		aexScene->Initialize();

		RNG->Initialize();

		//Initialize AudioManager
		audiomgr->Initialize();

		collision_table->Initialize();

		if (!aexResourceMgr->Initialize()) return false;

		GfxMgr->Initialize();


		// note, here'were creating and initializing at the same
		// time. by typing the maccro, we're creating the singleton
		// pointer which is returned, we then call initialize on it.
		if (!aexInput->Initialize())return false;
		if (!aexTime->Initialize())return false;

		// Frame rate controller options.
		aexTime->LockFrameRate(true);
		aexTime->SetMaxFrameRate(60.0);

		if (!aexFactory->Initialize()) return false;

		aexCollisionSystem->Initialize();

		Editorstate->Initialize();
		Editor->Initialize();

		// all good -> return true
		return true;
	}
	void AEXEngine::Run(IGameState *gameState)
	{
		// Sanity check
		if (!gameState)
			return;

		// load game state resources
		gameState->LoadResources();

		// initialize game state
		gameState->Initialize();

		aexLogic->Initialize();
		aexPhysicsMgr->Initialize();
		//aexCollisionSystem->Initialize();

		// Reset timer
		aexTime->Reset();

		// run the game loop
		while (/*(aexInput->KeyTriggered(GLFW_KEY_ESCAPE) == false) &&*/
			WindowMgr->GetCurrentWindow())
		{
			WindowMgr->Update();		// Process OS messages and respond to window events.
			aexInput->Update();			// Process Input specific messages. 

			aexTime->StartFrame();

			//

			//std::cout << "About to call Scene::Update()" << std::endl;
			aexScene->Update();			// Update Scene (transform, etc)
			//std::cout << "Scene::Update() call finished" << std::endl;

			//static bool audiosPausedOnce = false;
			//static bool audiosPlayedOnce = false;

			if (!Editorstate->IsPlaying())
			{
				//audiomgr->StopAll();
				//audiosPlayedOnce = false;
				//if (audiosPausedOnce == false)
				//{
					audiomgr->SetPauseStateForAll(true);
				//	audiosPausedOnce = true;
				//}

				aexEditorUpdateableComps->Update();
				ActivateAudio = true;
			}
			else
			{
				if (ActivateAudio)
				{
					//audiomgr->Loop(aexResourceMgr->getResource<Sound>("data\\Audio\\PlaytestingAudios\\ENTAILS.wav"), Voice::Music);

					ActivateAudio = false;
				}

				//audiosPausedOnce = false;
				//if (audiosPlayedOnce == false)
				//{
				if (aexScene->mGamePaused == false)
					audiomgr->SetPauseStateForAll(false);

				//	audiosPlayedOnce = true;
				//}

				if (aexScene->mPathsUpdatedOnce == false)
					aexEditorUpdateableComps->Update();

				if (aexScene->mGamePaused == false)
				{
					aexLogic->Update();
					aexPhysicsMgr->Update();	// Update all the rigidbody comps
					aexCollisionSystem->Update();
				}
				else
				{
					aexPausedLogic->Update();
				}

				// To be able to pause the game with the P key or with the start button
				PauseGameLogic();
			}

			//std::cout << "About to call GameState::Update()" << std::endl;
			// For pause functionality
			if (aexScene->mGamePaused == false)
				gameState->Update();
			//std::cout << "GameState::Update() call finished" << std::endl;

			//Updates audio manager
			audiomgr->Update();

			//std::cout << "About to call GameState::Render()" << std::endl;
			gameState->Render();
			//std::cout << "GameState::Render() call finished" << std::endl;
			aexTime->EndFrame();

			// poor man's reset
			/*if (Input::Instance()->KeyPressed('R'))
			{
				gameState->Shutdown();
				gameState->Initialize();
			}*/

			aexScene->FreeDeadObjects();

			// Apparently, front already pops the first element
			if (aexScene->mLevelsToLoad.empty() == false/* && (Scene::bFadeTransition == false ||
				(Scene::bFadeTransition && aexScene->FindObjectInANYSpace("fadein") &&
				aexScene->FindObjectInANYSpace("fadein")->GetComp<SpriteComp>() &&
				aexScene->FindObjectInANYSpace("fadein")->GetComp<SpriteComp>()->mColor[3] >= 1.0f))*/)
			{
				Scene::bFadeTransition = false;
				actual_change_level(aexScene->mLevelsToLoad.front().c_str());
			}

			if (Scene::bExitGame)
				break;
		}

		//Shutdown AudioManager
		audiomgr->Shutdown();

		gameState->Shutdown();

		// unload resources
		gameState->FreeResources();

		// delete gameState
		delete gameState;
	}
}