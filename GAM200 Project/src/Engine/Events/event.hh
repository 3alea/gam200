/*! 
******************************************************************************
\file    event.hh
\author  Diego Sanz
\par     DP email: diego.sanz@digipen.edu
\par     Course: cs225
\date    10-13-2019

\brief Contains various class declarations
*******************************************************************************/
#pragma once
#include <map>
#include "type_info.hh"

namespace AEX
{
    class Event
    {
        public:        
			virtual ~Event() {};
    };

    class HandlerFunction
    {
        public:
            //! ---------------------------------------------------------------------------
            // \fn		handle
            // \brief	Calls a private function.
            // \param   event : A reference to an event
            // \return	void
            // ---------------------------------------------------------------------------
			void handle(const Event& event)
			{
				// Call the actual function
				call(event);
			}

            //! ---------------------------------------------------------------------------
            // \fn		~HandlerFunction
            // \brief	Virtual destructor. Calls to the actual destructor
            // \param   
            // \return	
            // ---------------------------------------------------------------------------
			virtual ~HandlerFunction() {}


        private:
            //! ---------------------------------------------------------------------------
            // \fn		call
            // \brief	Virtual function (NVI). Calls the actual function to handle 
            //          the event
            // \param   event : A reference to the event to handle
            // \return	void
            // ---------------------------------------------------------------------------
            virtual void call(const Event&) = 0;

    };

    template<class T, class EVENT>
    class MemberFunctionHandler : public HandlerFunction
    {
        public:
            typedef void (T::*fPtr)(const EVENT&);
            
            //! ---------------------------------------------------------------------------
            // \fn		MemberFunctionHandler
            // \brief	Non default constructor. Sets the member variables
            // \param   obj : A pointer to a class object
            //          fn : A pointer to a member function of "obj"
            // \return	
            // ---------------------------------------------------------------------------
            MemberFunctionHandler(T* obj, fPtr fn) : object(obj), ptrFunction(fn) {}

            //! ---------------------------------------------------------------------------
            // \fn		call
            // \brief	Calls the event handler function of the object
            // \param   obj : A pointer to a class object
            //          fn : A pointer to a member function of "obj"
            // \return	void
            // ---------------------------------------------------------------------------
            virtual void call(const Event& event)
            {
                (object->*ptrFunction)(static_cast<const EVENT&>(event));
            }
        
        private:
           T* object;
           fPtr ptrFunction;
    };

    class EventHandler
    {
        public:
            //! ---------------------------------------------------------------------------
            // \fn		register_handler
            // \brief	Adds a new event and a pointer to the handler function in the
            //          handlers map
            // \param   obj : A pointer to a class object
            //          fn : A pointer to a member function of "obj"
            // \return	void
            // ---------------------------------------------------------------------------
            template<typename T, typename EVENT>
            void register_handler(T& object, void (T::*fn)(const EVENT&))
            {
                if (handlers[TypeInfo(typeid(EVENT))] == 0)
                    handlers[TypeInfo(typeid(EVENT))] = new MemberFunctionHandler<T, EVENT>(&object, fn);
            }

            //! ---------------------------------------------------------------------------
            // \fn		handle
            // \brief	Looks for the event in the map and call the corresponding 
            //          function.
            // \param   event : A reference to an event
            // \return	void
            // ---------------------------------------------------------------------------
            void handle(const Event& event)
			{
				// Iterator to go through the handlers map
				std::map<TypeInfo, HandlerFunction*>::iterator it;

				// Find the event in the handlers map
				it = handlers.find(TypeInfo(event));

				// Check if the event was found
				if (it != handlers.end())
					it->second->handle(event);
			}

            //! ---------------------------------------------------------------------------
            // \fn		handle
            // \brief	Destructor. Frees the dynamically allocated handler functions
            // \param   
            // \return	
            // ---------------------------------------------------------------------------
			~EventHandler() 
			{
				// Iterator to go through the handlers map
				std::map<TypeInfo, HandlerFunction*>::iterator it = handlers.begin();
				std::map<TypeInfo, HandlerFunction*>::iterator end = handlers.end();

				for (; it != end; ++it)
				{
					delete it->second;
				}

				handlers.clear();
			}

        private:
            std::map<TypeInfo, HandlerFunction*> handlers;
    };
}