/*! 
******************************************************************************
\file    event_dispatcher.hh
\author  Diego Sanz
\par     DP email: diego.sanz@digipen.edu
\par     Course: cs225
\date    10-13-2019

\brief Contains various class declarations
*******************************************************************************/
#pragma once
#include "event.hh"
#include <sstream>
#include <vector>

namespace AEX
{
    class Listener
    {
        public:
            //! ---------------------------------------------------------------------------
            // \fn		handle_event
            // \brief	Virtual function. Handles the passed event
            // \param   A reference to an event
            // \return	void
            // ---------------------------------------------------------------------------
            virtual void handle_event(const Event&) = 0;

			virtual ~Listener();
    };

    class EventDispatcher
    {
        public:
            //! ---------------------------------------------------------------------------
            // \fn		get_instance
            // \brief	Static member function. Returns the only instance of the class
            // \param   
            // \return	Reference to the class
            // ---------------------------------------------------------------------------
            static EventDispatcher& get_instance();
            
            //! ---------------------------------------------------------------------------
            // \fn		subscribe
            // \brief	Adds a new listener object to an event
            // \param   listener : A reference to a listener class
            //          eventType : A reference to a TypeInfo class
            // \return	void
            // ---------------------------------------------------------------------------
            void subscribe(Listener& listener, const TypeInfo& eventType);

            //! ---------------------------------------------------------------------------
            // \fn		operator
            // \brief   Friend function. Passes the class data to a stringstream
            // \param   ss : A reference to a stringstream
            //          ed : A reference to a event dispatcher class
            // \return	A reference to a stringstream
            // ---------------------------------------------------------------------------
            friend std::stringstream& operator<< (std::stringstream& ss, EventDispatcher& ed);

            //! ---------------------------------------------------------------------------
            // \fn		clear
            // \brief	Clears the map
            // \param   
            // \return	
            // ---------------------------------------------------------------------------
            void clear();

            //! ---------------------------------------------------------------------------
            // \fn		trigger_event
            // \brief   Calls to the handle event functions of all the subscribers to
            //          the event
            // \param   event : A reference to an event
            // \return	void
            // ---------------------------------------------------------------------------
            void trigger_event(const Event& event);

            //! ---------------------------------------------------------------------------
            // \fn		unsubscribe
            // \brief   Removes a subscriber from the map
            // \param   listener : A reference to a listener. The one to erase
            //          eventType : The event that the listener is subscribed to
            // \return	void
            // ---------------------------------------------------------------------------
            void unsubscribe(const Listener& listener, const TypeInfo& eventType);

        private:
            //! ---------------------------------------------------------------------------
            // \fn		EventDispatcher
            // \brief	Default constructor. Private (Singleton class)
            // \param   
            // \return	
            // ---------------------------------------------------------------------------
			EventDispatcher();
            static EventDispatcher instance;
            std::map<TypeInfo, std::vector<Listener*> > subscribers;
    };

    //! ---------------------------------------------------------------------------
    // \fn		trigger_event
    // \brief   Calls to the handle event functions of all the subscribers to
    //          the event (proxy function)
    // \param   event : A reference to an event
    // \return	void
    // ---------------------------------------------------------------------------
    void trigger_event(const Event& event);
}
	#define eventDispatcher (AEX::EventDispatcher::get_instance())