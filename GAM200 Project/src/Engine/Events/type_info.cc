/*! 
******************************************************************************
\file    type_info.cc
\author  Diego Sanz
\par     DP email: diego.sanz@digipen.edu
\par     Course: cs225
\date    10-13-2019

\brief Contains various class member function definitions
*******************************************************************************/
#include "type_info.hh"
#include <string>

namespace AEX
{
    TypeInfo::TypeInfo(const std::type_info& t) : mTypeName(t.name())
    {
    }

    bool TypeInfo::operator==(const TypeInfo& rhs) const
    {
        // If strcmp returns 0 the strings are the same
        //return !(strcmp(mTypeName, rhs.mTypeName));

        return std::string(mTypeName) == std::string(rhs.mTypeName);
    }

    bool TypeInfo::operator!=(const TypeInfo& rhs) const
    {
        // Call to operator==
        return !(*this == rhs);
    }

    bool TypeInfo::operator<(const TypeInfo& rhs) const
    {
        // Create strings and compare them
        return std::string(mTypeName) < std::string(rhs.mTypeName);
    }

    const char* TypeInfo::get_name() const
    {
        return mTypeName;
    }
}