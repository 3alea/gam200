/*! 
******************************************************************************
\file    type_info.hh
\author  Diego Sanz
\par     DP email: diego.sanz@digipen.edu
\par     Course: cs225
\date    10-13-2019

\brief Contains various class declarations
*******************************************************************************/
#pragma once
#include <typeinfo>

namespace AEX
{
    class TypeInfo
    {
        public:
            //! ---------------------------------------------------------------------------
            // \fn		TypeInfo
            // \brief	Templated conversion constructor. Sets member variable
            // \param   t : Reference to a class to get the type name from
            // \return	
            // ---------------------------------------------------------------------------
            template<typename T>
            TypeInfo(const T& t) : mTypeName(typeid(t).name()) {}

            //! ---------------------------------------------------------------------------
            // \fn		TypeInfo
            // \brief	Conversion constructor. Sets member variable
            // \param   t : Reference to a type_info to get the name from
            // \return	
            // ---------------------------------------------------------------------------
            TypeInfo(const std::type_info& t);

            //! ---------------------------------------------------------------------------
            // \fn		operator==
            // \brief	Overloaded operator. Returns true if both classes have the same
            //          type name
            // \param   rhs : Reference to a type info class to check with
            // \return	bool
            // ---------------------------------------------------------------------------
            bool operator==(const TypeInfo& rhs) const;

            //! ---------------------------------------------------------------------------
            // \fn		operator!=
            // \brief	Overloaded operator. Returns true if both classes have a different
            //          type name
            // \param   rhs : Reference to a type info class to check with
            // \return	bool
            // ---------------------------------------------------------------------------
            bool operator!=(const TypeInfo& rhs) const;

            //! ---------------------------------------------------------------------------
            // \fn		operator<
            // \brief	Overloaded operator. Returns true if the type name of this 
            //          is shorter
            // \param   rhs : Reference to a type info class to check with
            // \return	bool
            // ---------------------------------------------------------------------------
            bool operator<(const TypeInfo& rhs) const;

            //! ---------------------------------------------------------------------------
            // \fn		get_name
            // \brief	Returns the member variable "type". (Getter)
            // \param   
            // \return	const char*
            // ---------------------------------------------------------------------------
            const char* get_name() const;

        private:
            const char* mTypeName;
    };


    //! ---------------------------------------------------------------------------
    // \fn		type_of
    // \brief	Template function. Creates a type info class.
    // \param   T : A reference to a class to get the type name from
    // \return	Returns a type info class
    // ---------------------------------------------------------------------------
    template<typename T>
    TypeInfo type_of(const T& type)
    {
        return TypeInfo(type);
    }

    //! ---------------------------------------------------------------------------
    // \fn		type_of
    // \brief	Template function. Creates a type info class.
    // \param   T : A class type to get the type name from
    // \return	Returns a type info class
    // ---------------------------------------------------------------------------
    template<typename T>
    TypeInfo type_of()
    {
        return TypeInfo(T());
    }
}