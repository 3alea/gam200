/*! 
******************************************************************************
\file    event_dispatcher.cc
\author  Diego Sanz
\par     DP email: diego.sanz@digipen.edu
\par     Course: cs225
\date    10-13-2019

\brief Contains various class member function definitions
*******************************************************************************/
#include "event_dispatcher.hh"
#include <iostream>
#include <vector>

namespace AEX
{
    EventDispatcher EventDispatcher::instance;

	Listener::~Listener() {}

    EventDispatcher& EventDispatcher::get_instance()
    {
        return instance;
    }

    void EventDispatcher::subscribe(Listener& listener, const TypeInfo& eventType)
    {
        // Add a new subscriber to the event
        subscribers[eventType].push_back(&listener);
    }

    void EventDispatcher::clear()
    {
        // Clear the map
        subscribers.clear();
    }

    std::stringstream& operator<< (std::stringstream& ss, EventDispatcher& ed)
    {
        std::map<TypeInfo, std::vector<Listener*> >::iterator it = ed.subscribers.begin();
        std::map<TypeInfo, std::vector<Listener*> >::iterator end = ed.subscribers.end();

        for(; it != end; ++it)
        {
            ss << "The event type " << it->first.get_name() << " has the following subscribers:\n";

            int size = it->second.size();

            for (int i = 0; i < size; ++i)
            {
                ss << "\tAn instance of type " << TypeInfo(*it->second[i]).get_name() << "\n";
            }
        }

        return ss;
    }

    void EventDispatcher::trigger_event(const Event& event)
    {
        // Iterator to go through the map
        std::map<TypeInfo, std::vector<Listener*> >::iterator it;

        // Get the triggered event
        it = subscribers.find(TypeInfo(event));

        if (it != subscribers.end())
        {
            // Get the number of subscribers
            int size = it->second.size();

            // Call all the subscribers
            for (int i = 0; i < size; ++i)
            {
                it->second[i]->handle_event(event);
            }
        }
    }

    void EventDispatcher::unsubscribe(const Listener& listener, const TypeInfo& eventType)
    {
        // Create an iterator to go through the map
        std::map<TypeInfo, std::vector<Listener*> >::iterator it;
        
        // Find the event
        it = subscribers.find(eventType);

        if (it != subscribers.end())
        {
            // Create an iterator to go through the vector
            std::vector<Listener*>::iterator itVec = it->second.begin();
            std::vector<Listener*>::iterator itVecEnd = it->second.end();

            // Go through the vector and take out the subscriber
            for (; itVec != itVecEnd; ++itVec)
            {
                // Check if it is the listener to erase
                if (*itVec == &listener)
                {
                    it->second.erase(itVec);
                    break;
                } 
            }
        }
    }

	EventDispatcher::EventDispatcher() 
    {
    }

    void trigger_event(const Event& event)
    {
        EventDispatcher::get_instance().trigger_event(event);
    }
}