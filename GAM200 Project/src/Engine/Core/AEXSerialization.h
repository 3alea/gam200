#ifndef AEX_SERIALIZATION_H_
#define AEX_SERIALIZATION_H_


//#include <fstream>
#include <extern/Json/json.hpp>
//#include "../Core/AEXBase.h"
//#include "../Core/AEXRtti.h"
#include <extern/aexmath/aexmath/aexmath.h>
#include <extern/glm/vec2.hpp>
#include <extern/glm/vec3.hpp>


namespace AEX
{
	using json = nlohmann::json;

	class ISerializable
	{
		//AEX_RTTI_DECL(ISerializable, IBase);

	public:
		virtual void FromJson(json & value) = 0;
		virtual void ToJson(json & value) = 0;
	};

	json & operator<<(json & value, const u8 type);
	json & operator>>(json & value, u8 & type);
	json & operator<<(json & value, const s8 type);
	json & operator>>(json & value, s8 & type);
	json & operator<<(json & value, const u16 type);
	json & operator>>(json & value, u16 & type);
	json & operator<<(json & value, const s16 type);
	json & operator>>(json & value, s16 & type);
	json & operator<<(json & value, const u32 type);
	json & operator>>(json & value, u32 & type);
	json & operator<<(json & value, const s32 type);
	json & operator>>(json & value, s32 & type);
	json & operator<<(json & value, const u64 type);
	json & operator>>(json & value, u64 & type);
	json & operator<<(json & value, const s64 type);
	json & operator>>(json & value, s64 & type);
	json & operator<<(json & value, const f32 type);
	json & operator>>(json & value, f32 & type);
	json & operator<<(json & value, const f64 type);
	json & operator>>(json & value, f64 & type);

	json & operator<<(json & value, const char * type);
	json & operator>>(json & value, const char *& type);
	json & operator<<(json & value, const std::string & type);
	json & operator>>(json & value, std::string & type);

	json & operator<<(json & value, const AEVec2 & vec2);
	json & operator>>(json & value, AEVec2 & vec2);
	json & operator<<(json & value, const AEVec3 & vec3);
	json & operator>>(json & value, AEVec3 & vec3);

	json & operator<<(json & value, const glm::vec2 & vec2);
	json & operator>>(json & value, glm::vec2 & vec2);
	json & operator<<(json & value, const glm::vec3 & vec3);
	json & operator>>(json & value, glm::vec3 & vec3);

	json & operator<<(json & value, const bool boolean);
	json & operator>>(json & value, bool & boolean);

	void SaveToFile(json & value, const std::string & path);
	void LoadFromFile(json & value, const std::string & path);
}


#endif