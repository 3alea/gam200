#ifndef AEX_RTTI_H_
#define AEX_RTTI_H_

#include <string>
#include <map>

namespace AEX
{
	// Implemented by Diego Sanz
	class Rtti
	{
		public:
			// Stores all the types that are created
			static std::map<std::string, Rtti>Types;

			// Non default constructor, initializes the name and the base type pointer
			Rtti(const char * name, const Rtti * baseTypePtr);
			
			// Default constructor, initializes an unnamed type
			Rtti();

			// Returns true if the type is the same as the one passed as a reference
			bool SameTypeAs(const Rtti & otherType) const;
			
			// Returns true if the type derives from the one passed as a reference 
			bool DerivesFrom(const Rtti & otherType);

			// Getter
			const char * GetName() const;

			// Adds a type to the Types map. Returns a reference to the added type
			static const Rtti & Add(const char * name, const char * parentName);

			// TEMPLATE FUNCTIONS WILL WORK WITH ANY TYPE

			// Gets the name of the type. It removes the "class" or "struct" prefix
			template<typename Type>
			static std::string GetTypeName()
			{
				// Should I return a const char*? More efficient?

				// remove class
				std::string out = typeid(Type).name();
				std::size_t c = out.find("class ");
				while (c != std::string::npos)
				{
					auto s = out.begin() + c;
					auto e = s + strlen("class ");
					out.replace(s, e, "");
					c = out.find("class ");
				}
				// remove struct
				c = out.find("struct ");
				while (c != std::string::npos)
				{
					auto s = out.begin() + c;
					auto e = s + strlen("struct ");
					out.replace(s, e, "");
					c = out.find("struct ");
				}
				return out;
			}
			
			// Calls the add function passing the type name as string
			template<typename Type>
			static const Rtti & RttiAdd()
			{
				// Adds a new type to the Types map and its parent
				return Add(GetTypeName<Type>().c_str(), "NULL");
			}

			// Calls the add function passing the type and parent type names as strings
			template<typename Type, typename parentType>
			static const Rtti & RttiAdd()
			{
				// Adds a new type to the Types map and its parent
				return Add(GetTypeName<Type>().c_str(), GetTypeName<parentType>().c_str());
			}

		private:
			std::string mName;
			const Rtti * mBaseTypePtr;
			std::map<std::string, Rtti*>mDerivedTypes; // Stores the derived types
	};
}

// MACROS to include in any class that supports Rtti
// For derived classes
#define AEX_RTTI_DECL(type, parentType)\
	public:\
		virtual const AEX::Rtti & GetType() const\
		{\
			return AEX::Rtti::RttiAdd<type, parentType>();\
		}\
		static const AEX::Rtti& TYPE()\
		{\
			return AEX::Rtti::RttiAdd<type, parentType>();\
		}\

// For Base classes
#define AEX_RTTI_DECL_BASE(type)\
	public:\
		virtual const AEX::Rtti& GetType() const\
		{\
			return AEX::Rtti::RttiAdd<type>();\
		}\
		static const AEX::Rtti& TYPE()\
		{\
			return AEX::Rtti::RttiAdd<type>();\
		}\

#pragma warning (default:4251) // dll and STL
// ----------------------------------------------------------------------------

#endif