#include <string>
#include "AEXTypedefs.h"
#include "AEXBase.h"


namespace AEX
{
	u32 IBase::mIDGenerator = 0;

	// Returns the base name as a const char pointer
	const char * IBase::GetBaseName()
	{
		return mName.c_str();
	}

	std::string& IBase::GetBaseNameString()
	{
		return mName;
	}

	// Returns the ID of the object
	u32 IBase::GetBaseID()
	{
		return mID;
	}

	// Returns the total number of objects that derive
	// from base that have been created so far
	u32 IBase::InstancesCreated()
	{
		return mIDGenerator;
	}

	// Sets the base name to the one passed as parameter
	void IBase::SetBaseName(const char * newName)
	{
		mName = newName;
	}

	void IBase::SetBaseId(u32 id)
	{
		mID = id;
	}

	// Protected constructor, so that an instance can't be created
	IBase::IBase() : mName("Unnamed"), mID(mIDGenerator++)
	{
	}

	// Virtual destructor. For now, does nothing
	IBase::~IBase()
	{
	}
}