#ifndef AEX_BASE_H_
#define AEX_BASE_H_

#include <string>				// string
#include "AEXTypedefs.h"
#include "AEXRtti.h"			// RTTI

namespace AEX
{
	class IBase
	{
	public:

		// Gettors
		const char * GetBaseName();
		std::string& GetBaseNameString();

		u32 GetBaseID();
		static u32 InstancesCreated();

		// Settor
		void SetBaseName(const char * newName);

		// Virtual destructor
		virtual ~IBase();

	protected:
		IBase();		// Constructor protected so that an IBase can't be created
		void SetBaseId(u32 id);

	private:
		std::string mName;			// Name
		u32 mID;					// Unique id
		static u32 mIDGenerator;	// ID generator
	};
}

#endif