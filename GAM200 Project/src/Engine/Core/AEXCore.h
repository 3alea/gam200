#ifndef AEX_FOUNDATION_H_
#define AEX_FOUNDATION_H_

// Contains all the .h includes from the core folder

#include "AEXTypedefs.h"
#include "AEXBase.h"
#include "AEXRtti.h"			// Rtti
#include "AEXSerialization.h"
#include "AEXSystem.h"			// Base system interface
#include <list>
#include <map>
#include <vector>
#include <string>
#endif