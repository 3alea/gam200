#include "AEXRtti.h"

namespace AEX
{
	// Initialize Types
	std::map<std::string, Rtti>Rtti::Types;

	// NDC
	Rtti::Rtti(const char * name, const Rtti * baseTypePtr)
	{
		// Set the name
		mName = name;
		// remove the "class " prefix, returned by typeid().name() function
		mName = mName.substr(mName.find_first_of(" ") + 1);

		// Set the base type pointer
		mBaseTypePtr = baseTypePtr;

		// Why Thomas doesn't add the new instance to the types map?
	}

	// DC
	Rtti::Rtti()
	{
		// Set the name to unnamed
		mName = "unnamed";

		// Set the base type pointer to null
		mBaseTypePtr = NULL;
	}

	bool Rtti::SameTypeAs(const Rtti & otherType) const
	{
		// Check if they have the same address
		if (this == &otherType)
			return true;
		else
			return false;
	}

	bool Rtti::DerivesFrom(const Rtti & otherType)
	{
		// Create a pointer to this
		const Rtti * parent = this;

		// Go through the parents of this
		while (parent)
		{
			// Check if the current parent has the same address as otherType
			if (parent == &otherType)
				return true;

			// Go to the next parent
			parent = parent->mBaseTypePtr;
		}

		// Does not derive from otherType
		return false;
	}

	// Getter
	const char * Rtti::GetName() const
	{
		// Should I return a const char*? More efficient?
		return mName.c_str();
	}

	//CHECK THIS FUNCTION WITH THOMAS
	const Rtti & Rtti::Add(const char * name, const char * parentName)
	{
		// Add the parent to the types map and create a reference
		Rtti & parent = Types[parentName];
	
		// Set the name of the parent
		parent.mName = parentName;
	
		// Add the child to the types map
		Rtti & child = Types[name];
	
		// Set the parent of the child
		child.mBaseTypePtr = &parent;
	
		// Set the name of the child
		child.mName = name;
	
		// Add the child to the parent's derived types map
		parent.mDerivedTypes[name] = &child;
	
		return child;
	}


	//const Rtti & Rtti::Add(const char * name, const char * parentName) // Thomas makes this function static. why?
	//{
	//	// TODO: insert return statement here
	//
	//		// parent isn't assumed to exists. so this might be the first time
	//		// the parent type is encountered; (it depends on the call). 
	//	auto & parentIt = Types[parentName];
	//	parentIt.mName = parentName;
	//
	//	std::map<std::string, Rtti>::iterator it = Types.find(name);
	//	if (it == Types.end()) {
	//
	//		auto & ref = Types[name];
	//		ref.mBaseTypePtr = &parentIt;
	//		ref.mName = name;
	//
	//		// restore iterator
	//		it = Types.find(name);
	//	}
	//
	//	// add to parent
	//	parentIt.mDerivedTypes[name] = &it->second;
	//
	//	return it->second;
	//}

	

}