#include <fstream>
#include "AEXSerialization.h"



namespace AEX
{
	void SaveToFile(json & value, const std::string & path)
	{
		std::ofstream outFile(path);

		if (outFile.is_open() && outFile.good())
		{
			outFile << std::setw(4) << value;
		}
	}

	void LoadFromFile(json & value, const std::string & path)
	{
		std::ifstream inFile(path);

		if (inFile.is_open() && inFile.good())
		{
			inFile >> value;
		}
	}


	json & operator<<(json & value, const u8 type)
	{
		value = type;
		return value;
	}
	json & operator>>(json & value, u8 & type)
	{
		if (value != nullptr)
			type = value.get<u8>();

		return value;
	}
	json & operator<<(json & value, const s8 type)
	{
		value = type;
		return value;
	}
	json & operator>>(json & value, s8 & type)
	{
		if (value != nullptr)
			type = value.get<s8>();

		return value;
	}
	json & operator<<(json & value, const u16 type)
	{
		value = type;
		return value;
	}
	json & operator>>(json & value, u16 & type)
	{
		if (value != nullptr)
			type = value.get<u16>();

		return value;
	}
	json & operator<<(json & value, const s16 type)
	{
		value = type;
		return value;
	}
	json & operator>>(json & value, s16 & type)
	{
		if (value != nullptr)
			type = value.get<s16>();

		return value;
	}
	json & operator<<(json & value, const u32 type)
	{
		value = type;
		return value;
	}
	json & operator>>(json & value, u32 & type)
	{
		if (value != nullptr)
			type = value.get<u32>();

		return value;
	}
	json & operator<<(json & value, const s32 type)
	{
		value = type;
		return value;
	}
	json & operator>>(json & value, s32 & type)
	{
		if (value != nullptr)
			type = value.get<s32>();
		
		return value;
	}
	json & operator<<(json & value, const u64 type)
	{
		value = type;
		return value;
	}
	json & operator>>(json & value, u64 & type)
	{
		if (value != nullptr)
			type = value.get<u64>();

		return value;
	}
	json & operator<<(json & value, const s64 type)
	{
		value = type;
		return value;
	}
	json & operator>>(json & value, s64 & type)
	{
		if (value != nullptr)
			type = value.get<s64>();

		return value;
	}
	json & operator<<(json & value, const f32 type)
	{
		value = type;
		return value;
	}
	json & operator>>(json & value, f32 & type)
	{
		if (value != nullptr)
			type = value.get<f32>();

		return value;
	}
	json & operator<<(json & value, const f64 type)
	{
		value = type;
		return value;
	}
	json & operator>>(json & value, f64 & type)
	{
		if (value != nullptr)
			type = value.get<f64>();

		return value;
	}


	json & operator<<(json & value, const char * type)
	{
		value = type;
		return value;
	}
	json & operator>>(json & value, const char *& type)
	{
		if (value != nullptr)
			type = value.get_ptr<json::string_t*>()->c_str();

		return value;
	}
	json & operator<<(json & value, const std::string & type)
	{
		value = type;
		return value;
	}
	json & operator>>(json & value, std::string & type)
	{
		if (value != nullptr)
			type = value.get<std::string>();

		return value;
	}


	json & operator<<(json & value, const AEVec3 & vec3)
	{
		json val;
		val["x"] = vec3.x;
		val["y"] = vec3.y;
		val["z"] = vec3.z;
		value = val;
		return value;
	}
	json & operator>>(json & value, AEVec3 & vec3)
	{
		if (value != nullptr)
		{
			vec3.x = value["x"].get<f32>();
			vec3.y = value["y"].get<f32>();
			vec3.z = value["z"].get<f32>();
		}

		return value;
	}
	json & operator>>(json & value, AEVec2 & vec2)
	{
		if (value != nullptr)
		{
			vec2.x = value["x"].get<f32>();
			vec2.y = value["y"].get<f32>();
		}

		return value;
	}
	json & operator<<(json & value, const AEVec2 & vec2)
	{
		json val;
		val["x"] = vec2.x;
		val["y"] = vec2.y;
		value = val;

		return value;
	}


	json & operator<<(json & value, const glm::vec2 & vec2)
	{
		json val;
		val["x"] = vec2.x;
		val["y"] = vec2.x;
		value = val;
		return value;
	}
	json & operator>>(json & value, glm::vec2 & vec2)
	{
		if (value != nullptr)
		{
			vec2.x = value["x"].get<f32>();
			vec2.y = value["y"].get<f32>();
		}

		return value;
	}
	json & operator<<(json & value, const glm::vec3 & vec3)
	{
		json val;
		val["x"] = vec3.x;
		val["y"] = vec3.y;
		val["z"] = vec3.z;
		value = val;
		return value;
	}
	json & operator>>(json & value, glm::vec3 & vec3)
	{
		if (value != nullptr)
		{
			vec3.x = value["x"].get<f32>();
			vec3.y = value["y"].get<f32>();
			vec3.z = value["z"].get<f32>();
		}

		return value;
	}


	json & operator<<(json & value, const bool boolean)
	{
		value = boolean;
		return value;
	}
	json & operator>>(json & value, bool & boolean)
	{
		if (value != nullptr)
			boolean = value.get<bool>();

		return value;
	}
}