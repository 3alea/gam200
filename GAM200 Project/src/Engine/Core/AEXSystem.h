#ifndef AEX_SYSTEM_H_
#define AEX_SYSTEM_H_


#include "..\Utilities\AEXContainers.h"
#include "AEXBase.h"

namespace AEX
{
	class ISystem : public virtual IBase
	{
		AEX_RTTI_DECL(ISystem, IBase);

	public:

		// Virtual methods to manage all the systems of the game. Meant to be overriden.
		virtual bool Initialize() { return true; };
		virtual void Update() {};

		// Virtual destructor
		virtual ~ISystem() {}

		// Singleton support
		virtual AEX::ISystem** RetrieveInstancePtr() { return nullptr; }
		virtual AEX::ISystem* RetrieveInstance() { return nullptr; }
	};
}

// Add this macro to make the class singleton. Although there are better ways
// to implement the singleton pattern, this one is quick and easy. Warning!
// the default, copy constructor and assignment operators are made private
#define AEX_SINGLETON(classname)\
	public:\
	static classname** InstancePtr()\
	{\
		static classname * singleton = nullptr;\
		if(singleton == nullptr)\
			singleton = new classname();\
		return &singleton;\
	}\
	static classname* Instance()\
	{\
		classname * singleton = *InstancePtr();\
		return singleton;\
	}\
	virtual AEX::ISystem** RetrieveInstancePtr()\
	{\
		return reinterpret_cast<AEX::ISystem**>(classname::InstancePtr());\
	}\
	virtual AEX::ISystem* RetrieveInstance()\
	{\
		return dynamic_cast<AEX::ISystem*>(classname::Instance());\
	}\
	static void ReleaseInstance()\
	{\
		delete Instance();\
		(*InstancePtr()) = NULL;\
	}\
	private:\
	classname() {}\
	classname(const classname &);\
	const classname & operator=(const classname &);


#endif