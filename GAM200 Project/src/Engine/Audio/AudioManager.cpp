#include "AudioManager.h"
#include "Resources\AEXResourceManager.h"
#include "Menu/MainMenu/OptionsButtonLogic.h"

#include <experimental\filesystem>


namespace AEX
{
#define MAX_VOICE_COUNT 100

	// -----------------------------------------------------------------------------------------------------------------------

	Voice::Voice() : pChannel(NULL), isLooping(false), isPaused(true), volume(1.0f), mIsMuted(false), mVoiceType(SFX)
	{}
	Voice::~Voice()
	{}
	void Voice::SetVolume(float vol)
	{
		if (pChannel == NULL)
			return;

		volume = vol;

		if (volume < 0.0f)
			volume = 0.0f;
		else if (volume > 1.0f)
			volume = 1.0f;

		float generalVolFactor = mVoiceType == SFX ? audiomgr->GetSFXVolume() : audiomgr->GetMusicVolume();

		pChannel->setVolume(volume * generalVolFactor);
	}
	float  Voice::GetVolume()
	{
		return volume;
	}
	float  Voice::GetRealVolume()
	{
		float generalVolFactor = mVoiceType == SFX ? audiomgr->GetSFXVolume() : audiomgr->GetMusicVolume();

		return volume * generalVolFactor;
	}
	void Voice::SetMute(bool isMuted)
	{
		mIsMuted = isMuted;
		pChannel->setMute(mIsMuted);
	}
	bool Voice::IsMuted()
	{
		return mIsMuted;
	}
	void Voice::SetPause(bool paused)
	{
		if (pChannel == NULL)
			return;
		isPaused = paused;
		pChannel->setPaused(paused);
	}
	bool Voice::IsPaused()
	{
		return isPaused;
	}
	void Voice::SetLoop(bool isLoop)
	{
		if (pChannel == NULL)
			return;

		isLooping = isLoop;
		if (isLooping)
			pChannel->setLoopCount(-1);
		else
			pChannel->setLoopCount(0);
	}
	bool Voice::IsValid()
	{
		if (pChannel == NULL)
			return false;

		bool pl;
		pChannel->isPlaying(&pl);
		return pl;
	}
	bool Voice::IsLooping()
	{
		return isLooping;
	}

	void Voice::SetVoiceType(VoiceType type)
	{
		mVoiceType = type;
	}
	Voice::VoiceType Voice::GetVoiceType()
	{
		return mVoiceType;
	}
	// -----------------------------------------------------------------------------------------------------------------------

	Sound::Sound() : pSound(NULL)
	{}

	// -----------------------------------------------------------------------------------------------------------------------
	/*AudioManager::AudioManager()
		: pFMOD(NULL)
		, soundCount(0)
		, voiceCount(MAX_VOICE_COUNT)
		, voices(0)
	{
	}*/
	/*AudioManager::~AudioManager()
	{
		Shutdown();
	}*/
	bool	AudioManager::Initialize()
	{
		FMOD_RESULT result;
		mSFXVolume = 1.0f;
		mMusicVolume = 1.0f;

		result = FMOD::System_Create(&pFMOD);
		if (result != FMOD_OK)
		{
			//AE_ASSERT_MESG(result == FMOD_OK, "AUDIO: Error creating FMOD system! (%d) %s\n", result, FMOD_ErrorString(result));
			return false;
		}

		// Initialize FMOD
		result = pFMOD->init(MAX_VOICE_COUNT, FMOD_INIT_NORMAL, 0);
		if (result != FMOD_OK)
		{
			pFMOD->release();
			pFMOD = NULL;
			return false;
		}

		AllocVoices();

		return true;
	}
	void	AudioManager::Shutdown()
	{
		if (NULL == pFMOD)
			return;

		pFMOD->release();
		pFMOD = NULL;
		FreeVoices();
	}
	void	AudioManager::Update()
	{

		if (NULL == pFMOD)
			return;

		// Update FMOD -> play audio
		pFMOD->update();

		// loop through the voices
		for (Voice::PTR_LIST::iterator it = usedVoices.begin(); it != usedVoices.end(); ++it)
		{
			// this voice is no longer playing
			if (!(*it)->IsValid())
			{
				// erase and push to free list
				freeVoices.push_back(*it);
				it = usedVoices.erase(it);
				if (it == usedVoices.end())
					break;
			}
		}

	}
	FMOD::System * AudioManager::GetFMOD()
	{
		return this->pFMOD;
	}
	// -----------------------------------------------------------------------------------------------------------------------

	void AudioManager::FreeThisVoice(Voice *pVoice)
	{
		if (NULL == pFMOD)
			return;
		if (pVoice == NULL)
			return;

		pVoice->pChannel->stop();
		usedVoices.remove(pVoice);
		freeVoices.push_back(pVoice);
	}
	Voice *	AudioManager::GetFreeVoice()
	{
		if (NULL == pFMOD)
			return NULL;
		if (freeVoices.empty())
			return NULL;

		Voice * pv = freeVoices.back();
		freeVoices.pop_back();
		usedVoices.push_back(pv);

		return pv;
	}
	void	AudioManager::AllocVoices()
	{
		if (NULL == pFMOD)
			return;

		// free the voices if necessary
		//FreeVoices();

		// allocate new array of voices
		voiceCount = MAX_VOICE_COUNT;
		voices = new Voice[voiceCount];

		// push all onto the free voices
		for (unsigned int i = 0; i < voiceCount; ++i)
			freeVoices.push_back(voices + i);
	}
	void	AudioManager::FreeVoices()
	{
		//if (NULL == pFMOD)
		//	return;
		if (voices)
		{
			delete[] voices;
			voiceCount = 0;
			freeVoices.clear();
			usedVoices.clear();
		}
	}

	// -----------------------------------------------------------------------------------------------------------------------
	TResource<Sound> *	AudioManager::CreateSound(const char * filename)
	{
		// Just in case someone still calls this funciton
		return aexResourceMgr->getResource<Sound>(filename);

		/*if (NULL == pFMOD)
			return NULL;

		// Allocate new memory for the sound
		Sound * pSound = new Sound();

		FMOD_RESULT result = pFMOD->createSound(filename, FMOD_LOOP_NORMAL | FMOD_2D, 0, &pSound->pSound);

		switch (result)
		{
		case FMOD_ERR_FILE_NOTFOUND:
			std::cout << "FMOD ERROR : File \"" << filename << "\" not found." << std::endl;
			break;
		case FMOD_ERR_FORMAT:
			std::cout << "FMOD ERROR : Unsupported file or audio format." << std::endl;
			break;
		}

		// save the name of the 
		pSound->filename = filename;

		// error check
		if (pSound->pSound == NULL)
		{
			// make sure to delete the sound pointer
			delete pSound;
			return NULL;
		}

		// All is good
		++soundCount;	// Stats update
		return pSound;*/
	}
	void AudioManager::FreeSound(TResource<Sound> * resource)
	{
		if (NULL == pFMOD)
			return;
		if (!resource)
			return;

		if (resource->get() && resource->get()->pSound)
		{
			resource->get()->pSound->release();
			resource->get()->pSound = 0;
		}

		// Stats update
		--soundCount;

		if (resource->get())
			delete resource->get();

		/*auto soundRttiIt = aexResourceMgr->mAllResources.find(Sound::TYPE().GetName());

		auto found = soundRttiIt->second.find(resource->GetRelativePath());

		if (found != soundRttiIt->second.end())
		{
			delete resource;
			resource = nullptr;
		}*/
	}
	Voice *	AudioManager::Play(TResource<Sound> * pSound, Voice::VoiceType voiceType, bool paused)
	{
		// make sure we can actually play the sound
		if (pFMOD == NULL || pSound->get() == NULL)
			return NULL;

		// look for a free voice
		Voice * pVoice = GetFreeVoice();

		// this voice is valid
		if (pVoice)
		{
			// we found an available voice
			pFMOD->playSound(pSound->get()->pSound, 0, paused, &pVoice->pChannel);
			pVoice->SetPause(paused);
			pVoice->SetLoop(false);
			pVoice->SetVoiceType(voiceType);
			pVoice->SetVolume(1.0f);// * (voiceType == Voice::SFX ? audiomgr->GetSFXVolume() : audiomgr->GetMusicVolume()));	// 1.0f just in case the volume was not reset
			pVoice->SetMute(!OptionsButtonLogic::mIsVolumeOn);
		}

		// Return the voice (either NULL or correct)
		return pVoice;
	}
	Voice *	AudioManager::Loop(TResource<Sound> * pSound, Voice::VoiceType voiceType, bool paused)
	{
		Voice * pv = Play(pSound, voiceType, paused);
		if (pv)
			pv->SetLoop(true);
		return pv;
	}
	void		AudioManager::StopAll()
	{
		if (NULL == pFMOD)
			return;

		// loop through the voices
		while (!usedVoices.empty())
		{
			// erase and push to free list
			usedVoices.back()->pChannel->stop();
			freeVoices.push_back(usedVoices.back());
			usedVoices.pop_back();
		}
	}


	// Added by Miguel for menu options
	void AudioManager::SetMuteStateForAll(bool isMuted)
	{
		if (pFMOD == nullptr)
			return;

		for (auto & currVoice : usedVoices)
		{
			currVoice->SetMute(isMuted);
		}
	}
	void AudioManager::SetPauseStateForAll(bool isPaused)
	{
		if (pFMOD == nullptr)
			return;

		for (auto & currVoice : usedVoices)
		{
			currVoice->SetPause(isPaused);
		}
	}
	void AudioManager::SetVolumeForAll(float newVolume)
	{
		if (pFMOD == nullptr)
			return;

		for (auto & currVoice : usedVoices)
		{
			currVoice->SetVolume(newVolume);
		}
	}

	void AudioManager::SetSFXVolume(float newVolume)
	{
		mSFXVolume = AEX::Clamp(newVolume, 0.0f, 1.0f);

		for (auto & currVoice : usedVoices)
		{
			currVoice->SetVolume(currVoice->GetVolume());
		}
	}
	void AudioManager::SetMusicVolume(float newVolume)
	{
		mMusicVolume = AEX::Clamp(newVolume, 0.0f, 1.0f);

		for (auto & currVoice : usedVoices)
		{
			currVoice->SetVolume(currVoice->GetVolume());
		}
	}
	float AudioManager::GetSFXVolume()
	{
		return mSFXVolume;
	}
	float AudioManager::GetMusicVolume()
	{
		return mMusicVolume;
	}

	// -----------------------------------------------------------------------------------------------------------------------
	void SoundEmitter::Initialize()
	{
		/*if(!DefaultVoiceSelected)
			CurrentSound = aexResourceMgr->getResource<Sound>("data\\Audio\\Lost_Civilization.mp3");*/
		
		CurrentSound = aexResourceMgr->getResource<Sound>(actualName.c_str());
		CurrentSoundEditor = aexResourceMgr->getResource<Sound>(actualName.c_str());
		//Paused = false;
		//PausedOnEditor = true;
		//Looping = false;
		//Volume = 1.0f;
		//MaxDistanceFromListener = 10.0f;
		//MinDistanceFromListener = 0.0f;
		//soundtype = SFX;
		//currentsoundstate = PlayOnGame;
		//SoundGroupButton = 0;
		//SoundStateButton = 0;
	}

	void SoundEmitter::Update()
	{
		if (CurrentVoice==nullptr)
			return;

		if (CurrentSound == nullptr)
			return;

		/*if (!PausedOnEditor)
		{
			CurrentVoice->SetPause(true);
			PausedOnEditor = true;
		}*/

		if (!Paused)
		{
			Play(CurrentSound);
			Paused = true;
		}

		else if (Looping)
		{
			Loop(CurrentSound);
			Looping = false;
		}

		if (soundtype == SFX)
		{
			GameObject * found = aexScene->FindObjectInANYSpace("player");
			ASSERT(found, "The gameobject with name player was not found")
				CurrentListener = found->GetComp<SoundListener>()->ListenerPosition;
			EmitterPosition = mOwner->GetComp<TransformComp>()->mLocal.mTranslationZ;

			AEVec3 distance = CurrentListener - EmitterPosition;
			CurrentDistanceFromListener = fabs(distance.Length());

			//std::cout << "DISTANCE FROM LISTENER TO EMITTER: "<<CurrentDistanceFromListener << std::endl;
			//std::cout << "Listerner: " << CurrentListener.x << std::endl;
			if (CurrentDistanceFromListener > MaxDistanceFromListener)
				Volume = 0.0f;

			else if (CurrentDistanceFromListener < MinDistanceFromListener)
				Volume = 1.0f;

			else
				Volume = 1 - (CurrentDistanceFromListener - MinDistanceFromListener) /
				(MaxDistanceFromListener - MinDistanceFromListener);
		}

		if (soundtype == Music)
		{
			Volume = SliderVolume;
		}

		CurrentVoice->SetVolume(Volume);

	//	if (CurrentVoice->IsPaused())
			//Paused = true;
	}

	void SoundEmitter::Shutdown()
	{
		
	}

	bool SoundEmitter::OnGui()
	{
		//std::cout << soundtype << std::endl;

		bool isChanged = false;

		namespace fs = std::experimental::filesystem;

		// Miguel
		//actualName = "Lost_Civilization.mp3";
		static bool isPathShown = false;
		std::string previewName;

		if (ImGui::Checkbox("Show Paths", &isPathShown)) isChanged = true;

		// To solve bug where currently selected texture wouldn't change
		// to only the name as soon as you unclicked the checkbox
		if (!isPathShown)
			previewName = aexResourceMgr->GetNameWithExtension(actualName);
		else
			previewName = actualName;

		if (ImGui::BeginCombo("Sound", previewName.c_str()))
		{
			try {
				fs::path soundDir = fs::path("data\\Audio");

				int count = 0;
				for (auto dirIt = fs::recursive_directory_iterator(soundDir);
					dirIt != fs::recursive_directory_iterator();
					dirIt++, ++count)
				{
					if (fs::is_directory(dirIt->path()))
						continue;

					if (dirIt->path().extension().string() == ".meta")
						continue;

					std::string currentFileName;

					if (isPathShown)
						currentFileName = dirIt->path().relative_path().string();
					else
						currentFileName = dirIt->path().filename().string();

					if (ImGui::Selectable((currentFileName + "##" + std::to_string(count)).c_str()))
					{
						actualName = dirIt->path().relative_path().string().c_str();
						//mTex = aexResourceMgr->getResource<Texture>(actualName.c_str());
						CurrentSound = aexResourceMgr->getResource<Sound>(actualName.c_str());
						CurrentSoundEditor = aexResourceMgr->getResource<Sound>(actualName.c_str());
					}
				}
			} CATCH_FS_ERROR

			ImGui::EndCombo();
		}

		//ImGui::Checkbox("Pitch", &CheckBoxPitch);

		if (ImGui::RadioButton("SFX", &SoundGroupButton, 0))
		{
			soundtype = SFX;
			//std::cout << "SFX" << std::endl;
		}
		ImGui::SameLine();
		
		if (ImGui::RadioButton("Music", &SoundGroupButton, 1))
		{
			soundtype = Music;
			//std::cout << "Music" << std::endl;
		}

		ImGui::SliderFloat("Volume", &SliderVolume, 0.0f, 1.0f);

		ImGui::SliderFloat("Pitch", &SliderPitch, 0.0f, 2.0f);

		if (ImGui::Button("Reset pitch"))
			SliderPitch = 1.0f;

		if (ImGui::InputFloat("Max distance", &MaxDistanceFromListener))
			isChanged = true;

		if (ImGui::InputFloat("Min distance", &MinDistanceFromListener))
			isChanged = true;

		if (ImGui::RadioButton("Pause", &SoundStateButton, 0))
		{
			currentsoundstate = Pause;

			Paused = true;

			Looping = false;
			
			audiomgr->StopAll();
			
			isChanged = true;

		}

		ImGui::SameLine();

		if (ImGui::RadioButton("PlayOnEditor", &SoundStateButton, 1))
		{
			currentsoundstate = PlayOnEditor;

			PausedOnEditor = false;

			Looping = false;

			Paused = true;

			Volume = SliderVolume;

			//CurrentVoice->SetVolume(Volume);

			if (!PausedOnEditor)
				Play(CurrentSoundEditor);

			isChanged = true;
		}

		ImGui::SameLine();

		if (ImGui::RadioButton("PlayOnGame", &SoundStateButton, 2))
		{
			currentsoundstate = PlayOnGame;

			Paused = false;

			Looping = false;

			audiomgr->StopAll();

			isChanged = true;

		}

		ImGui::SameLine();

		if (ImGui::RadioButton("PlayOnLoop", &SoundStateButton, 3))
		{
			currentsoundstate = LoopOnGame;

			Looping = true;

			Paused = true;

			audiomgr->StopAll();

			isChanged = true;

		}

		return isChanged;
	}

	Voice * SoundEmitter::Play(TResource<Sound> * pSound, bool paused)
	{
		if (CurrentVoice == nullptr)
			return nullptr;
		audiomgr->soundCount++;
		audiomgr->voiceCount++;

		Paused = false;
		Looping = false;

		Voice::VoiceType voiceType = soundtype == SFX ? Voice::SFX : Voice::Music;
		CurrentVoice = audiomgr->Play(pSound, voiceType, paused);
		CurrentVoice->pChannel->setPitch(SliderPitch);
		audiomgr->usedVoices.push_back(CurrentVoice);
		return CurrentVoice;
	}
	Voice * SoundEmitter::Loop(TResource<Sound> * pSound, bool paused)
	{
		if (CurrentVoice == nullptr)
			return nullptr;

		Paused = true;
		Looping = true;
		Voice::VoiceType voiceType = soundtype == SFX ? Voice::SFX : Voice::Music;
		CurrentVoice = audiomgr->Loop(pSound, voiceType, paused);
		CurrentVoice->pChannel->setPitch(SliderPitch);

		audiomgr->usedVoices.push_back(CurrentVoice);

		return CurrentVoice;
	}

	void SoundEmitter::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);
		value["Emitter Position"] >> EmitterPosition;
		value["Paused"] >> Paused;
		value["Looping"] >> Looping;
		value["Volume"] >> Volume;
		value["MaxDistanceFromListener"] >> MaxDistanceFromListener;
		value["MinDistanceFromListener"] >> MinDistanceFromListener;
		value["CurrentDistanceFromListener"] >> CurrentDistanceFromListener;

		int soundTypeInt = -1;
		int soundStateInt = -1;

		value["SoundType"] >> soundTypeInt;
		value["CurrentSoundState"] >> soundStateInt;

		soundtype = (SoundType)soundTypeInt;
		currentsoundstate = (CurrentSoundState)soundStateInt;

		value["SoundGroupButton"] >> SoundGroupButton;
		value["SoundStateButton"] >> SoundStateButton;
		value["SliderVolume"] >> SliderVolume;
		//value["CurrentSoundEditor"] >> CurrentSoundEditor;
		//value["CurrentSound"] >> CurrentSound;
		value["actualName"] >> actualName;
		value["SliderPitch"] >> SliderPitch;
		
		std::string soundname = SoundName;
		value["Sound name"] >> soundname;
		strcpy_s(SoundName, soundname.c_str());

	}
	void SoundEmitter::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);
		value["Emitter Position"] << EmitterPosition;
		value["Paused"] << Paused;
		value["Looping"] << Looping;
		value["Volume"] << Volume;
		value["MaxDistanceFromListener"] << MaxDistanceFromListener;
		value["MinDistanceFromListener"] << MinDistanceFromListener;
		value["CurrentDistanceFromListener"] << CurrentDistanceFromListener;

		int soundTypeInt = soundtype;
		int soundStateInt = currentsoundstate;

		value["SoundType"] << soundTypeInt;
		value["CurrentSoundState"] << soundStateInt;
		value["Sound name"] << SoundName;
		value["SoundGroupButton"] << SoundGroupButton;
		value["SoundStateButton"] << SoundStateButton;
		value["SliderVolume"] << SliderVolume;
		//value["CurrentSoundEditor"] << CurrentSoundEditor;
		//value["CurrentSound"] << CurrentSound;
		value["actualName"] << actualName;
		value["SliderPitch"] << SliderPitch;
	}

	// -----------------------------------------------------------------------------------------------------------------------
	void SoundListener::Initialize()
	{
		
	}

	void SoundListener::Update()
	{
		ListenerPosition = mOwner->GetComp<TransformComp>()->mLocal.mTranslationZ;
	}

	bool SoundListener::OnGui()
	{
		return false;
	}

	void SoundListener::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);

		value["Listener Position"] >> ListenerPosition;
	}
	void SoundListener::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);

		value["Listener Position"] << ListenerPosition;
	}
}