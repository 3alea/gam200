#pragma once

#include "src/Engine/Core/AEXSystem.h"
#include "src/Engine/Components/AEXComponents.h"
#include "Imgui/imgui.h"
#include "src/Engine/Composition/AEXScene.h"
#include "extern/fmod/inc/fmod.hpp"
#include "Components/Logic.h"
#include <list>

// Forward declare the FMOD classes
// so that I don't have to include fmod.hpp 
// in Audio.h, and therefore create an unnecessary 
// dependency. 
namespace FMOD
{
	class Sound;
	class Channel;
	class System;
}

namespace AEX
{
	struct Sound
	{
		AEX_RTTI_DECL_BASE(Sound);

		FMOD::Sound		*pSound;
		std::string		 filename;

		Sound();
	};

	class Voice
	{
		// friends have access to privates
		friend class AudioManager;

	public:

		enum VoiceType
		{
			SFX,
			Music
		};

		Voice();
		~Voice();

		// Setters and Getters
		void SetVolume(float volume);		// Normalized [0, 1]
		void SetMute(bool isMuted);	// Don't use this function to unmute a sound. You could be unmuting a sound, when the volume might be turned off. Instead, change the volume.
		void SetPause(bool paused);
		void SetLoop(bool isLoop);
		void SetVoiceType(VoiceType type);
		bool IsMuted();
		bool IsPaused();
		bool IsValid();						// Returns whether this voice is currently being played
		bool IsLooping();
		float  GetVolume();
		float  GetRealVolume();			// With the sfx and music volume controller of the menu applied
		VoiceType GetVoiceType();

		// STL
		typedef std::list<Voice *> PTR_LIST;



		FMOD::Channel * pChannel;
	private:
		float			volume;
		bool			isLooping;
		bool			isPaused;
		bool			mIsMuted;
		VoiceType		mVoiceType;
	};

	class AudioManager : public ISystem
	{
	public:

		//~AudioManager();

		bool Initialize();	// Initializes fmod and allocates all the available voices
		void Update();		// Updates fmod and moves all the voices that have finished back to the freeVoices array
		void Shutdown();	// Releases the fmod pointer and deletes all the voices that were allocated in Initialize

		
		// Use this functions to play a sound
		Voice * Play(TResource<Sound> * pSound, Voice::VoiceType voiceType, bool paused = false);
		Voice * Loop(TResource<Sound> * pSound, Voice::VoiceType voiceType, bool paused = false);

		// Sound Management
		TResource<Sound> * CreateSound(const char * filename);	// Wrapper around a call to the resource mgr in order to obtain the resource
		void	FreeSound(TResource<Sound> * resource);			// Releases the sound and deletes the actual resource

		FMOD::System * GetFMOD();

		void SetMuteStateForAll(bool isMuted);		// Mute/Unmute all voices currently in use
		void SetPauseStateForAll(bool isPaused);	// Pause/Unpause all voices currently in use
		void SetVolumeForAll(float newVolume);		// Sets a volume for all the voices currently in use

		// For menu options (Miguel)
		void SetSFXVolume(float newVolume);		// Range [0, 1]
		void SetMusicVolume(float newVolume);	// Range [0, 1]
		float GetSFXVolume();
		float GetMusicVolume();

		// Stop All
		void StopAll();							// Stops all the voices in use, and moves them to the freeVoices array
		void FreeThisVoice(Voice *pVoice);		// Stops an specific voice, and moves it to the freeVoices array

		// keep track of the sounds created 
		unsigned int				soundCount;
		unsigned int				voiceCount;
		Voice::PTR_LIST				usedVoices;		// Contains the voices that are currently being used
		
		AEX_SINGLETON(AudioManager);

	private:

		// Helper methods
		void						AllocVoices();
		void						FreeVoices();

		// Voice manager... adds to free and removes from used
		Voice *						GetFreeVoice();

		// FMOD System
		FMOD::System*				pFMOD;

		// Voice Management List (equivalent to dead list and free list)
		Voice *						voices;				// Contains ALL the voices (determined by MAX_VOICE_COUNT in the cpp)
		Voice::PTR_LIST				freeVoices;			// Contains the voices that are not being used (at the begining, this
														// contains the same voices as the array on top)

		// Aded by Miguel for volume control in menus
		float mSFXVolume = 1.0f;
		float mMusicVolume = 1.0f;
	};
#define audiomgr AEX::AudioManager::Instance()

	struct SoundEmitter : public LogicComp
	{
		AEX_RTTI_DECL(SoundEmitter, LogicComp)

			SoundEmitter() {}

		virtual void Initialize();
		virtual void Update();
		virtual void Shutdown();

		Voice * Play(TResource<Sound> * pSound, bool paused = false);
		Voice * Loop(TResource<Sound> * pSound, bool paused = false);

		void FromJson(json & value);
		void ToJson(json & value);

		virtual bool OnGui();

		AEVec3 EmitterPosition;
		bool Paused;
		bool PausedOnEditor;
		bool Looping;
		float Volume;
		float MaxDistanceFromListener;
		float MinDistanceFromListener;
		float CurrentDistanceFromListener;

		char SoundName[64] = "";

		AEVec3 CurrentListener;

		
		Voice * CurrentVoice;
		TResource<Sound> * CurrentSound;
		TResource<Sound> * CurrentSoundEditor;
		bool DefaultVoiceSelected = false;

		enum SoundType { SFX, Music };
		enum CurrentSoundState { Pause, PlayOnEditor, PlayOnGame, LoopOnGame };

		SoundType soundtype;
		CurrentSoundState currentsoundstate;

		int SoundStateButton;
		int SoundGroupButton;
		float SliderVolume;
		float SliderPitch;
		bool CheckBoxPitch;

		std::string actualName;
	};

	struct SoundListener : public LogicComp
	{
		AEX_RTTI_DECL(SoundListener, LogicComp)

		SoundListener() {}
		SoundListener(AEVec3 pos) : ListenerPosition(pos) {}

		virtual void Initialize();
		virtual void Update();
		virtual bool OnGui();

		void FromJson(json & value);
		void ToJson(json & value);

		AEVec3 ListenerPosition;
	};
}