#include <fstream>
#include <sstream>
#include <Imgui/imgui.h>
#include <experimental\filesystem>
#include "src/Engine/Utilities/AEXAssert.h"
#include "AEXResourceManager.h"
#include "Graphics\Texture.h"
#include "Graphics\Sprite.h"
#include "Graphics\Shader.h"
#include "Graphics\SpineTexture.h"
#include "Dialogue\DialogueData.h"
#include "Audio\AudioManager.h"
#include "Core\AEXSystem.h"


namespace AEX
{
	namespace fs = std::experimental::filesystem;

	template IResource * TResourceImporter<Texture>::ImportFromFile(const char *);
	template IResource * TResourceImporter<SpriteAnim>::ImportFromFile(const char *);
	template IResource * TResourceImporter<Shader>::ImportFromFile(const char *);
	template IResource * TResourceImporter<Font>::ImportFromFile(const char *);

	template<>
	IResource * TResourceImporter<Texture>::ImportFromFile(const char * fileName)
	{
		TResource<Texture> * newRes = new TResource<Texture>();
		Texture * temp = new Texture(fileName);
		newRes->get() = temp;
		return newRes;
	}

	template<>
	IResource * TResourceImporter<SpriteAnim>::ImportFromFile(const char * fileName)
	{
		TResource<SpriteAnim> * newRes = new TResource<SpriteAnim>();
		SpriteAnim * temp = new SpriteAnim(fileName);
		newRes->get() = temp;
		return newRes;
	}

	template<>
	IResource * TResourceImporter<Shader>::ImportFromFile(const char * fileName)
	{
		TResource<Shader> * newRes = new TResource<Shader>();

		std::ifstream inFile(fileName);
		std::string vertexShader;
		std::string fragmentShader;

		if (inFile.is_open() && inFile.good() && !inFile.eof())
		{
			std::getline(inFile, vertexShader);
			std::getline(inFile, fragmentShader);
		}

		Shader * temp = new Shader(vertexShader.c_str(), fragmentShader.c_str());
		newRes->get() = temp;

		return newRes;
	}

	template <>
	IResource * TResourceImporter<Sound>::ImportFromFile(const char * fileName)
	{
		// Allocate new memory for the sound
		TResource<Sound> * resource = new TResource<Sound>;

		if (NULL == audiomgr->GetFMOD())
			return resource;

		resource->get() = new Sound();

		FMOD_RESULT result = audiomgr->GetFMOD()->createSound(fileName, FMOD_LOOP_NORMAL | FMOD_2D, 0, &(resource->get()->pSound));

		switch (result)
		{
		case FMOD_ERR_FILE_NOTFOUND:
			std::cout << "FMOD ERROR : File \"" << fileName << "\" not found." << std::endl;
			break;
		case FMOD_ERR_FORMAT:
			std::cout << "FMOD ERROR : Unsupported file or audio format." << std::endl;
			break;
		}

		// save the name of the
		//pSound->Set = filename;

		// error check
		if (resource->get()->pSound != NULL)
		{
			//// make sure to delete the sound pointer
			//delete pSound;

			// All is good
			audiomgr->soundCount++;	// Stats update
		}

		return resource;
	}


	// Pablo
	template<>
	IResource* TResourceImporter<SpineData>::ImportFromFile(const char* fileName)
	{
		TResource<SpineData>* newResource = new TResource<SpineData>();

		std::ifstream inFile(fileName);
		std::string atlas;
		std::string animData;

		if (inFile.is_open() && inFile.good() && !inFile.eof())
		{
			std::getline(inFile, atlas);
			std::getline(inFile, animData);
		}

		SpineData* temp = new SpineData(atlas.c_str(), animData.c_str());
		newResource->get() = temp;

		return newResource;
	}

	template<>
	IResource* TResourceImporter<Font>::ImportFromFile(const char* fileName)
	{
		TResource<Font>* newResource = new TResource<Font>();

		Font* temp = new Font(fileName);
		newResource->get() = temp;

		return newResource;
	}

	template<>
	IResource* TResourceImporter<DialogueData>::ImportFromFile(const char* fileName)
	{
		TResource<DialogueData>* newResource = new TResource<DialogueData>();

		DialogueData* temp = new DialogueData(fileName);
		newResource->get() = temp;

		return newResource;
	}



	bool ResourceManager::Initialize()
	{
		//std::cout << "Initialize of ResourceManager called" << std::endl;

		TResourceImporter<Texture> * texImporter = new TResourceImporter<Texture>;
		TResourceImporter<Sound> * soundImporter = new TResourceImporter<Sound>;

		// Here you should add all the resource importers, just like in the following line
		AddResourceImporter(texImporter, "png");
		AddResourceImporter(texImporter, "jpg");
		AddResourceImporter(new TResourceImporter<SpriteAnim>, "anim");
		AddResourceImporter(new TResourceImporter<Shader>, "shader");
		AddResourceImporter(new TResourceImporter<SpineData>, "spine");
		AddResourceImporter(soundImporter, "mp3");
		AddResourceImporter(soundImporter, "wav");
		AddResourceImporter(new TResourceImporter<Font>, "ttf");

		return true;
	}


	// Tested
	std::string ResourceManager::GetExtension(const std::string & resName)
	{
		// Find the positoin in the string of the last occurrence of a dot
		int pos = resName.find_last_of('.');

		// If a dot wasn't found, return NULL (no extension)
		if (pos == resName.npos)
		{
			std::cerr << "Filename has no extension" << std::endl;
			return "NoExtension";
		}

		std::string ext = resName.substr(pos + 1);
		return ext;
	}

	// Tested
	std::string ResourceManager::GetNameNoExtension(const std::string & resName)
	{
		// Find the positoin in the string of the last occurrence of a dot
		int lastDot = resName.find_last_of('.');

		// Find the positoin in the string of the last occurrence of a backslash
		int lastSlash = resName.find_last_of('\\');

		// If there are no backslashes or dots, return the given string
		if (lastSlash == resName.npos && lastDot == resName.npos)
			return resName;

		// Find the starting point and the end point for the loop
		int startingPoint = 0;
		int end = resName.length();

		if (lastSlash != resName.npos)
			startingPoint = lastSlash + 1;

		if (lastDot != resName.npos)
			end = lastDot;

		std::string str;

		// Add to the empty string all the characters of resName
		// in the range specified by startingPoint and end
		for (int i = startingPoint; i < end; i++)
			str.push_back(resName[i]);

		return str;
	}

	// Tested
	std::string ResourceManager::GetNameWithExtension(const std::string & resName)
	{
		// Find the positoin in the string of the last occurrence of a backslash
		int lastSlash = resName.find_last_of('\\');

		// If there are no backslashes or dots, return the given string
		if (lastSlash == resName.npos)
			return resName;

		int end = resName.length();

		std::string str;

		// Add to the empty string all the characters of resName
		// in the range specified by startingPoint and end
		for (int i = lastSlash + 1; i < end; i++)
			str.push_back(resName[i]);

		return str;
	}

	// Tested
	std::string ResourceManager::GetNameWithoutNamespaces(const std::string & resName)
	{
		int lastColon = resName.find_last_of(':');

		if (lastColon == resName.npos)
			return resName;

		int end = resName.length();

		std::string str;

		for (int i = lastColon + 1; i < end; i++)
			str.push_back(resName[i]);

		return str;
	}

	// Tested
	std::string ResourceManager::GetPathNoExtension(const std::string & resName)
	{
		// Find the positoin in the string of the last occurrence of a dot
		int lastDot = resName.find_last_of('.');

		// If there are no dots, return the given string
		if (lastDot == resName.npos)
			return resName;

		std::string str;

		// Add to the empty string all the characters of resName
		// in the range specified by startingPoint and end
		for (int i = 0; i < lastDot; i++)
			str.push_back(resName[i]);

		return str;
	}



	IResource * ResourceManager::AddResource(IResource * resource, const char * filename)
	{
		//std::cout << "AddResource function called with filename " << filename <<" and the resource pointer "<<resource<< std::endl;

		std::string rttiName = resource->getType().GetName();
		std::string fileName = filename;

		//std::cout << "rttiName = " << rttiName<<std::endl;

		resource->SetFileName(filename);
		resource->SetRelativePath(filename);
		resource->SetAbsolutePath(filename);

		if (mAllResources[rttiName].find(fileName) != mAllResources[rttiName].end())
			__debugbreak();


		mAllResources[rttiName][fileName] = resource;

		//std::cout << "Resource added to the map of resources" << std::endl;
		return resource;
	}



	ResourceManager::~ResourceManager()
	{
		//std::cout << "Destructor of resource manager called" << std::endl;

		FOR_EACH(it, mAllResources)
		{
			// Loop through the map of TResources
			FOR_EACH(it2, it->second)
			{
				// Delete the TResource (its destructor will delete the actual resource if it is a valid pointer)
				if (TResource<Sound> * tResSound = dynamic_cast<TResource<Sound> *>(it2->second))
				{
					audiomgr->FreeSound(tResSound);
					//it2->second = NULL;
					//continue;
				}

				delete it2->second;
				it2->second = NULL;
			}
		}

		FOR_EACH(it, mAllResImporters)
		{
			if (it->second != nullptr)
				delete it->second;

			it->second = nullptr;
		}
	}


	void ResourceManager::OnGui()
	{
		if (!ImGui::Begin("Loaded Resources"))
		{
			ImGui::End();
			return;
		}

		// Not adding this because messes up size of files
		//static bool showPaths = true;
		//ImGui::Checkbox("Show Paths", &showPaths);

		static bool shouldTexBePreviewed;
		ImGui::Checkbox("Preview Textures", &shouldTexBePreviewed);

		FOR_EACH(it1, mAllResources)
		{
			std::string rttiType = it1->first;

			if (ImGui::CollapsingHeader(aexResourceMgr->GetNameWithoutNamespaces(rttiType).c_str()))
			{
				FOR_EACH(resIt, it1->second)
				{
					// Relative path to each resource of the same rtti type
					std::string relPath;

					relPath = resIt->first;

					ImGui::Text(relPath.c_str());

					//u32 pathLength = relPath.length();

					// Hardcode the spacing for now
					ImGui::SameLine(500.0f/*pathLength * 15.0f*/);

					/*static */u32 fileSize = 0;

					try {
						if (rttiType == SpriteAnim::TYPE().GetName())
						{
							int pos = relPath.find("Animations");
							bool testing = pos != relPath.npos ? true : false;
							ASSERT(testing, "Animations folder is not in the path")
							relPath.replace(pos, 10, "Spritesheets");

							pos = relPath.find("anim");
							bool testing2 = pos != relPath.npos ? true : false;
							ASSERT(testing2, "Extension is not .anim")
							relPath.replace(pos, 4, "png");

							fileSize = (u32)fs::file_size(fs::path(relPath));
						}
						else if (rttiType == Shader::TYPE().GetName())
						{
							int pos = relPath.find("Shaders");
							bool testing = pos != relPath.npos ? true : false;
							ASSERT(testing, "Shaders folder is not in the path")
							std::string file = "VS_FS_Files\\";
							file += aexResourceMgr->GetNameNoExtension(relPath);
							relPath.replace(pos + 8, std::string::npos, file);

							std::string relPath2 = relPath;
							
							relPath += ".vs";
							relPath2 += ".fs";

							fileSize += (u32)fs::file_size(fs::path(relPath));
							fileSize += (u32)fs::file_size(fs::path(relPath2));
						}
						else
							fileSize = (u32)fs::file_size(fs::path(relPath));
					}CATCH_FS_ERROR

					std::stringstream strstream;
					strstream << fileSize << " bytes";
					ImGui::Text(strstream.str().c_str());

					if (shouldTexBePreviewed && rttiType == Texture::TYPE().GetName())
					{
						TResource<Texture> * tRes = dynamic_cast<TResource<Texture> *>(resIt->second);

						if (tRes != nullptr)
						{
							Texture * tex = tRes->get();

							if (tex != nullptr)
								ImGui::Image(ImTextureID(tex->GetID()), ImVec2(40, 40), ImVec2(0, 1), ImVec2(1, 0));

							//return;
						}

						//TResource<SpriteAnim> * tRes2 = dynamic_cast<TResource<SpriteAnim> *>(resIt->second);
						//
						//if (tRes2 != nullptr)
						//{
						//	SpriteAnim * anim = tRes2->get();
						//
						//	if (anim != nullptr)
						//	{
						//		if (anim->mpSheet != nullptr)
						//		{
						//			//Texture * currentTex = anim->mpSheet
						//			//ImGui::Image(ImTextureID(anim->GetID()), ImVec2(40, 40), ImVec2(0, 1), ImVec2(1, 0));
						//			//ImGui::Image	
						//			//return;
						//		}
						//	}
						//}

						std::cout << "Dynamic cast in OnGui of resource manager failed." << std::endl;
					}
				}
			}
		}

		ImGui::End();
	}
}