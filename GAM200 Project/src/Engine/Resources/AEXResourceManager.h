#ifndef AEX_RES_MGR_H_
#define AEX_RES_MGR_H_

#include <iostream>
#include <map>
#include <string>
#include "src/Engine/Core/AEXBase.h"
#include "src/Engine/Core/AEXSystem.h"
#include "src/Engine/Core/AEXRtti.h"
#include "src/Engine/Core/AEXSerialization.h"


namespace AEX
{

	class IResource : public IBase, public ISerializable
	{
		AEX_RTTI_DECL(IResource, IBase)

	public:
		virtual const Rtti & getType() = 0;
		//virtual IResource * Reload(const char * fileName) = 0;
		virtual void SetFileName(const char * relPath) = 0;
		virtual void SetRelativePath(const char * relPath) = 0;
		virtual void SetAbsolutePath(const char * relPath) = 0;

		virtual const std::string & GetRelativePath() = 0;
		/*virtual void SetRelPath(const char * relPath) = 0;
		virtual void SetAbsPath(const char * absPath) = 0;*/
		virtual ~IResource() {
			//std::cout << "---------------------------------------------------------------" << std::endl;
			//std::cout << "Destructor of IResource called" << std::endl;
			//std::cout << "---------------------------------------------------------------" << std::endl;
			//__debugbreak();
		}
	};


	template <typename T>
	class TResource : public IResource
	{
		AEX_RTTI_DECL(TResource<T>, IResource)

	public:
		TResource() : mActualResource(NULL) {}

		T *& get() { return mActualResource; }
		const Rtti & getType() { return T::TYPE(); __debugbreak(); }
		void SetFileName(const char * relPath);

		void SetRelativePath(const char * relPath)
		{
			mRelativePath = relPath;
		}
		void SetAbsolutePath(const char * relPath)
		{
			std::string absPath = PROJECT_DIR;
			absPath += relPath;
			mAbsolutePath = absPath;
		}

		const std::string & GetRelativePath()
		{
			return mRelativePath;
		}

		~TResource()
		{
			//std::cout << "Deleting the actual resource "<<mActualResource << std::endl;

			if (mActualResource != nullptr)
			{
				delete mActualResource;
				mActualResource = nullptr;
			}

			//std::cout << "End of TResource destructor" << std::endl;
		}
		
		//IResource * Reload(const char * fileName);

		// Serialization methods
		void FromJson(json & value);
		
		void ToJson(json & value);
	
		void SetActualResource(T*res, bool shouldDelete = true) { if (mActualResource && shouldDelete)delete mActualResource; mActualResource = res; }

	private:
		T * mActualResource;
		std::string mRelativePath;
		std::string mAbsolutePath;
	};


	class IResourceImporter : public IBase
	{
		AEX_RTTI_DECL(IResourceImporter, IBase)

	public:
		IResourceImporter() {}
		virtual ~IResourceImporter() {}
		virtual IResource * ImportFromFile(const char * fileName) = 0;
		virtual const char * GetResourceTypename() = 0;
	};

	

	template <typename T>
	class TResourceImporter : public IResourceImporter
	{
		AEX_RTTI_DECL(TResourceImporter<T>, IResourceImporter)

	public:
		
		IResource * ImportFromFile(const char * fileName);
		

		const char * GetResourceTypename()
		{
			return T::TYPE().GetName();
		}
	};



	class ResourceManager : public ISystem
	{
		AEX_RTTI_DECL(ResourceManager, ISystem)
		AEX_SINGLETON(ResourceManager)

		
	public:

		// Initialize: Here you will have to add the resource importers to the map by calling AddResourceImporter
		bool Initialize();

		~ResourceManager();

		std::string GetExtension(const std::string & resName);
		std::string GetNameNoExtension(const std::string & resName);
		std::string GetNameWithExtension(const std::string & resName);
		std::string GetNameWithoutNamespaces(const std::string & resName);
		std::string GetPathNoExtension(const std::string & resName);

		template <typename T>
		TResource<T>* Load(const char * nameWithExtension)
		{
			//std::cout << "Load function called with type " << T::TYPE().GetName() << " and filename: " << nameWithExtension << std::endl;

			const char * rttiName = T::TYPE().GetName();

			// Need to solve issue where the same resource is loaded more than once if it is on a different folder!!!

			auto it = mAllResources[rttiName].find(nameWithExtension);

			if (it != mAllResources[rttiName].end())
			{
				//std::cout << "No resource was loaded. The resource of type "<< rttiName<<
				//			" and filename "<<nameWithExtension<<" has already been loaded."<< std::endl;

				auto pair = *it;
				auto resource = pair.second;

				if (resource == nullptr)
					__debugbreak();

				IResource & res = *resource;
				res.GetType();

				//std::cout << resource->GetType().GetName() << std::endl;

				// Return the already loaded resource
				TResource<T> * tres = dynamic_cast<TResource<T>*>(resource);

				//if (tres != NULL)
				//	std::cout << "Dynamic cast to a TResource<"<<rttiName<<"> * of the already loaded resource was successful" << std::endl;
				//
				//std::cout << "The already loaded resource type is: " << it->second->GetType().GetName()
				//		  << "and its name is " << it->first << std::endl;

				return tres;
			}

			//std::cout << "This is the first time we are loading the resource " << nameWithExtension << " of type " << T::TYPE().GetName() << std::endl;

			// Get the correct importer and import the resource
			IResourceImporter * loader = GetImporterFromFileName(nameWithExtension);
			if (loader == NULL)
			{
				//std::cout << "No resource importer associated with this extension" << std::endl;
				TResource<T>* TRes = new TResource<T>;

				//std::cout << "Created default TResource. Adding it to map..." << std::endl;
				AddResource(TRes, nameWithExtension);

				return TRes;
			}

			//std::cout << "Resource importer exists" << std::endl;

			TResource<T> * tres = dynamic_cast<TResource<T>*>(AddResource(loader->ImportFromFile(nameWithExtension), nameWithExtension));

			//if (tres != NULL)
			//	std::cout << "Dynamic cast of the just loaded resource to a TResource<" << rttiName << "> * was successful" << std::endl;

			return tres;
		}


		IResourceImporter * GetImporterFromFileName(const char * fileName)
		{
			//std::cout << "GetImporterFromFileName function called with file " << fileName << " as parameter"<<std::endl;

			std::string extension = GetExtension(fileName);

			// If no extension, return NULL, as there is no importer for it
			if (mAllResImporters.find(extension) == mAllResImporters.end())
			{
				std::cout << "This is an unkown extension, returning NULL..." << std::endl;
				return NULL;
			}

			return mAllResImporters[extension];
		}

		IResource * AddResource(IResource * resource, const char * fileName);

		template <typename T>
		IResourceImporter * AddResourceImporter(TResourceImporter<T> * importer, const char * extension)
		{
			//std::cout << "AddResourceImporter function called for the extension " << extension
			//	<< ", with the resource importer pointer " << importer <<" for a "<<T::TYPE().GetName()<< std::endl;

			if (importer == NULL)
			{
				std::cout << "You are trying to add a NULL resource importer. Returning NULL..." << std::endl;
				return NULL;
			}

			if (mAllResImporters.find(extension) != mAllResImporters.end())
			{
				std::cout << "WARNING: You are changing an already existing resource importer associated with the extension "
					<< extension << std::endl;

				std::cout << "We are about to delete the resource importer for the extension " << extension << std::endl;
				if (importer != mAllResImporters[extension])
					delete mAllResImporters[extension];
				std::cout << "Resource importer for extension " << extension << "deleted successfully" << std::endl;
			}

			mAllResImporters[extension] = importer;
		//	std::cout << "Importer added to the map" << std::endl;
			return importer;
		}

		template <typename T>
		TResource<T>* getResource(const char * fileName)
		{
			// Calls load. If the resource already exists, won't do anything,
			// it will just get the TResource<T>*. If it doesn't exist, it will load it.
			//std::cout << "getResource function called with type "<< T::TYPE().GetName() <<" and filename: "<<fileName<< std::endl;
			return Load<T>(fileName);
		}


		void OnGui();

	public:
		//		  RTTI Type				FileName	 TResource
		std::map<std::string, std::map<std::string, IResource *> > mAllResources;

		//		  Extension		TResourceImporter
		std::map<std::string, IResourceImporter *> mAllResImporters;
	};


	#define aexResourceMgr (ResourceManager::Instance())


	template <typename T>
	void TResource<T>::SetFileName(const char * relPath)
	{
		SetBaseName(aexResourceMgr->GetNameWithExtension(relPath).c_str());
	}


	template <typename T>
	void TResource<T>::FromJson(json & value)
	{
		/*std::string metaFile;
		value["Meta file"] >> metaFile;

		json val;
		LoadFromFile(val, metaFile);

		std::string path;
		val["Relative Path"] >> path;

		TResource<T> * tRes = aexResourceMgr->getResource<T>(path.c_str());
		*/
		
		//if (tRes != NULL)
		//	mActualResource = tRes->get();
	}

	template <typename T>
	void TResource<T>::ToJson(json & value)
	{
		std::string pathNoExt = aexResourceMgr->GetPathNoExtension(mRelativePath);
		pathNoExt += ".meta";

		value["Meta File"] = pathNoExt;

		json metaVal;
		metaVal["FileName"] = GetBaseName();
		metaVal["RTTI Type"] = T::TYPE().GetName();
		metaVal["Relative Path"] = mRelativePath;
		metaVal["Absolute Path"] = mAbsolutePath;

		SaveToFile(metaVal, pathNoExt);
	}


	/*template <typename T>
	IResource * TResource<T>::Reload(const char * fileName)
	{
		std::string rttiName = T::TYPE().GetName();

		std::cout << "Reload function called with a " << rttiName << " of name " << fileName << std::endl;

		TResource<T> * tRes = aexResourceMgr->getResource<T>(fileName);
		mActualResource = tRes->get();
		return tRes;
	}*/
}


#endif