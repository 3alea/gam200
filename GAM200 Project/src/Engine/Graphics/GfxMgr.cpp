#include "../Composition/AEXObjectManager.h"
#include "WindowMgr.h"
#include "GfxMgr.h"
#include "Editor/Editor.h"
#include "FrameBuffer.h"
#include <extern/GLFW/glfw3.h>
#include <cassert>
#include <iostream>
#include "..\Gameplay\Follow\FollowAnObject.h"
#include "Menu/MainMenu/OptionsButtonLogic.h"

namespace AEX
{
	DebugLine::DebugLine(AEVec4 _color, AEVec2 _start, AEVec2 _end)
		: color(_color)
	{
		this->Line::start = _start;
		this->Line::end = _end;
	}

	DebugCircle::DebugCircle()
	{

	}

	DebugCircle::DebugCircle(AEVec2 _pos, /*GLfloat*/f32 _radius, /*GLuint*/u32 _numVertices, AEVec4 _color) : pos(_pos), radius(_radius), numVertices(_numVertices), color(_color)
	{

	}

	/*GraphicsManager::GraphicsManager() {}*/
	
	bool GraphicsManager::Initialize()
	{
		//std::cout << "GfxMgr : Initialize" << std::endl;
		//std::cout << "Initializing GLFW" << std::endl;
		glfwInit();
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);    // Required on Mac
	
		WindowMgr->Initialize();
	
		int nrAttributes;
		glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrAttributes);
		//std::cout << "Maximum nr of vertex attributes supported: " << nrAttributes << std::endl;

		// Freetype library
		//std::cout << "Initializing FreeType Library" << std::endl;
		if (FT_Init_FreeType(&mFreeType_LB))
			std::cout << "ERROR : Could not initialize FreeType Library" << std::endl;

		//std::cout << "Enabling GL_BLEND " << std::endl;
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		CompileLineShaders();

		mbWireframeMode = false;

		/*
			Create the default quad

			Model * quad = new Model();
			
			// add 6 vertices (0,1,2, 0,2,3)
			quad->AddVertex(...)
			quad->AddVertex(...)
			quad->AddVertex(...)
			quad->AddVertex(...)
			quad->AddVertex(...)
			quad->AddVertex(...)

			// upload to gpu
			quad->UploadToGPU();

			// example: update data
			quad->SetVertexPos(1, {10,10});
			quad->SetVertexPos(2, {10,10});
			quad->ReUploadToGPU(1,2); // only the data needed

			quad->SetVertexPos(1, {10,10});
			quad->SetVertexPos(500, {10,10});
			quad->ReUploadToGPU(1,500); // BE CAREFUL: 2,3,...,499 get reupploaded (redundant)
			// this is better:
			quad->ReUploadToGPU(1,1); // only reupload 1 
			quad->ReUploadToGPU(500,500); // only reupload 500 

			

			TResource<Model> * mQuadModelRes = new TResource<Model>();
			mQuadModelRes->SetActualResource(quad);
			ResourceManager->AddResource(mQuadModelRes, "Quad.model");
		*/

		// Added by Miguel
		GfxMgr->SetVSync(OptionsButtonLogic::mIsVSyncOn);
	
		return true; // 1
	}
	
	void GraphicsManager::Update()
	{
		// Update all cameras
		FOR_EACH(it, mCameraList) (*it)->Update();

		// Update all renderables
		FOR_EACH(it, mRenderableList) (*it)->Update();
	}
	
	void GraphicsManager::Render()
	{
		// Window Input
		//WindowMgr->ProcessInput(*WindowMgr->GetCurrentWindow());
		
		ClearFrameBuffer();


		RenderGOs();
	
		//DrawAllLines();
	
		SwapBuffers();
	}
	
	void GraphicsManager::Shutdown()
	{
		mCameraList.clear();
		mRenderableList.clear();
		FT_Done_FreeType(mFreeType_LB);
		glfwTerminate();
	}
	
	void GraphicsManager::ClearFrameBuffer()
	{
		if (!glfwWindowShouldClose(*WindowMgr->GetCurrentWindow()))
		{
			// clearing the whole frame buffer before rendering new frame. 
			glViewport(0, 0, WindowMgr->GetCurrentWindow()->GetWidth(), WindowMgr->GetCurrentWindow()->GetHeight());
			
			// Rendering commands
			glClearColor(84.f/255.f, 34.f / 255.f, 34.f / 255.f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT); // Clear the buffer
		}
		else
			exit(1);
	}

	void GraphicsManager::RenderRenderable(Renderable * pObj, glm::mat4& view, glm::mat4& proj, Camera* cam)
	{
		Shader * actualShader = nullptr;

		if (pObj->mShader)
		{
			actualShader = pObj->mShader->get();
		}

		if (actualShader != nullptr)
		{
			actualShader->use();
			// pass transformation matrices to the shader // note: currently we set the projection matrix each frame, but since the projection matrix rarely changes it's often best practice to set it outside the main loop only once.
			actualShader->setMat4(0, glm::value_ptr(proj * view));
		}

		pObj->Render(cam);
		check_gl_error();
	}

	void GraphicsManager::RenderGOs()
	{

		// void GraphicsManager::SortCameras(){
		struct cameraSorter {
			bool operator()(Camera* lhs, Camera*rhs) {
				return lhs->mRenderOrder < rhs->mRenderOrder;
			}
		};
		mCameraList.sort(cameraSorter());
		//}

		// get the current position of the followed


		std::list<Camera*>::iterator camIt;

		if (mbWireframeMode)
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		mRenderableList.sort([](Renderable* lhs, Renderable* rhs) { return lhs->GetOwner()->GetComp<TransformComp>()->mLocal.mTranslationZ.z < rhs->GetOwner()->GetComp<TransformComp>()->mLocal.mTranslationZ.z; });

		if (Editorstate->IsPlaying())
		{
			for (camIt = mCameraList.begin(); camIt != mCameraList.end(); camIt++)
			{
				if ((*camIt)->mbRenderCam)
				{
					Camera* cam = *camIt;
					Viewport camViewport = cam->mCamViewport;

					// create transformations
					glm::mat4 view = glm::mat4(1.0f); // make sure to initialize matrix to identity matrix first
					glm::mat4 projection = glm::mat4(1.0f);
					int width = WindowMgr->GetCurrentWindow()->GetWidth();
					int height = WindowMgr->GetCurrentWindow()->GetHeight();

					// set the viewport to limit screen
					if (cam->mbAdjustToWindow)
						cam->mCamViewport = Viewport(0.0f, f32(width), 0.0f, f32(height));

					float aspect = (float)width / height;
					projection = glm::ortho(-cam->mViewRectangleSize * aspect, cam->mViewRectangleSize * aspect, -cam->mViewRectangleSize, cam->mViewRectangleSize, 1000.f, -1000.0f);
					view = glm::translate(view, glm::vec3(cam->GetWorldPos().x, cam->GetWorldPos().y, 20.0f));
					view = glm::rotate(view, cam->mRotation, glm::vec3(0.f, 0.f, 1.f));

					for (auto shaderIt : mShaderList)
					{
						shaderIt->use();
						// pass transformation matrices to the shader
						shaderIt->setMat4(0, glm::value_ptr(projection * view)); // note: currently we set the projection matrix each frame, but since the projection matrix rarely changes it's often best practice to set it outside the main loop only once.

					}

					if(auto follow = cam->GetOwner()->GetComp<FollowAnObject>()) {
						if (follow->followedT)
						{
							auto diff = follow->followedT->mLocal.mTranslationZ - follow->mTargetPrevPos;
							auto myMove = diff * follow->mMoveRatio;
							follow->followerT->mLocal.mTranslationZ.x += myMove.x;
							follow->followerT->mLocal.mTranslationZ.y += myMove.y;
							follow->followerT->mLocal.mTranslationZ.x = follow->followedT->mLocal.mTranslationZ.x;
							follow->followerT->mLocal.mTranslationZ.y = follow->followedT->mLocal.mTranslationZ.y;
							follow->mTargetPrevPos = follow->followedT->mLocal.mTranslationZ;
						}
						
					}


					if (cam->mpFrameBuffer)
						cam->mpFrameBuffer->Bind();
					check_gl_error();

					std::list<Renderable*>::iterator renderIt;
					for (renderIt = mRenderableList.begin(); renderIt != mRenderableList.end(); renderIt++)
					{
						// ignore objects not in the same space of the camera
						/*auto renderableSpace = (*renderIt)->GetOwner()->GetParentSpace();
						auto camSpace = cam->GetOwner()->GetParentSpace();
						if (camSpace != renderableSpace)
							continue;*/
						
						/*if (dynamic_cast<ParticleEmitter*>(*renderIt) != nullptr && dynamic_cast<ParticleEmitter*>(*renderIt)->mEmitProperties->mFront == false)
						{
							if (SpriteComp* spriteComp = (*renderIt)->mOwner->GetComp<SpriteComp>())
								RenderRenderable(spriteComp, view, projection, *camIt);
							if (SpriteAnimationComp* spriteAnimComp = (*renderIt)->mOwner->GetComp<SpriteAnimationComp>())
								RenderRenderable(spriteAnimComp, view, projection, *camIt);
							if (SpineAnimationComp* spineComp = (*renderIt)->mOwner->GetComp<SpineAnimationComp>())
								RenderRenderable(spineComp, view, projection, *camIt);
							if (SpriteText* textComp = (*renderIt)->mOwner->GetComp<SpriteText>())
								RenderRenderable(textComp, view, projection, *camIt);
						}
						else if ((*re)
						{

						}*/
						//std::cout << (*renderIt)->mOwner->GetBaseNameString() << std::endl;
						RenderRenderable(*renderIt, view, projection, *camIt);

					}

					if (FrameBuffer* fb = cam->mpFrameBuffer)
					{
						fb->Unbind();

						//// this is a test, remove from here and add to editor workflow
						//{
						//	// create a renderable
						//	Sprite * testFB = new Sprite();

						//	testFB->mShader = aexResourceMgr->getResource<Shader>("TextureColor.shader");// get the shader 
						//	testFB->mTex = new TResource<Texture>();
						//	texFB->mTex->SetRawResource(fb->mColorTex);// get the shader 
						//	texFB->Render();

						glClearColor(0.3, 0.5, 1.0f, 1.0f);
						glClear(GL_COLOR_BUFFER_BIT);

						fb->Update();
						fb->mColorTex->Render(cam);
						check_gl_error();

						//	// clean up
						//	delete testFB;
						//}
					}
				}
			}
		}
		else
		{
			for (camIt = mCameraList.begin(); camIt != mCameraList.end(); camIt++)
			{
				AEVec2 transform;
				if ((*camIt)->mOwner)
					transform = AEVec2((*camIt)->mOwner->GetComp<TransformComp>()->mWorld.mTranslationZ.x, (*camIt)->mOwner->GetComp<TransformComp>()->mWorld.mTranslationZ.y);
				f32 aspectRatio = (f32)WindowMgr->GetCurrentWindow()->GetWidth() / (f32)WindowMgr->GetCurrentWindow()->GetHeight();
				Transform camTrans((*camIt)->mOffset + transform, AEVec2(2.0f * (*camIt)->mViewRectangleSize * aspectRatio, 2.0f * (*camIt)->mViewRectangleSize), 0.0f);
				DrawRectangle(camTrans, AEVec4(1.0f, 0.5f, 0.0f, 1.0f));
			}
			Camera* cam = Editorstate->mEditorCamera;

			Viewport camViewport = cam->mCamViewport;

			// create transformations
			glm::mat4 view = glm::mat4(1.0f); // make sure to initialize matrix to identity matrix first
			glm::mat4 projection = glm::mat4(1.0f);
			int width = WindowMgr->GetCurrentWindow()->GetWidth();
			int height = WindowMgr->GetCurrentWindow()->GetHeight();

				// set the viewport to limit screen
				if (cam->mbAdjustToWindow)
					cam->mCamViewport = Viewport(0.0f, f32(width), 0.0f, f32(height));

			float aspect = (float)width / height;
			projection = glm::ortho(-cam->mViewRectangleSize * aspect, cam->mViewRectangleSize * aspect, -cam->mViewRectangleSize, cam->mViewRectangleSize, 1000.f, -1000.f);
			view = glm::translate(view, glm::vec3(cam->mOffset.x, cam->mOffset.y, 20.0f));
			view = glm::rotate(view, cam->mRotation, glm::vec3(0.f, 0.f, 1.f));

			std::list<Renderable*>::iterator renderIt;
			for (renderIt = mRenderableList.begin(); renderIt != mRenderableList.end(); renderIt++)
				(*renderIt)->Render(cam);
		}

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	
	void GraphicsManager::SwapBuffers()
	{
		glfwSwapBuffers(*WindowMgr->GetCurrentWindow());
	}

	// Added by Miguel
	void GraphicsManager::SetVSync(bool vsyncActive)
	{
		if (mVSyncEnabled == vsyncActive)
			return;

		mVSyncEnabled = vsyncActive;
		glfwSwapInterval(vsyncActive ? 1 : 0);
	}
	bool GraphicsManager::IsVSyncActive() const
	{
		return mVSyncEnabled;
	}
	
	void GraphicsManager::DrawLine(DebugLine& line)
	{
		//line.start.x += WindowMgr->GetCurrentWindow()->GetWidth();
		mLineList.push_back(line);
	}
	
	void GraphicsManager::DrawCircle(const DebugCircle & circle)
	{
		AEVec2 coord = AEVec2(circle.radius / 2.0f, 0.0f);	// Second vertex of the line
		float currentAngle = 0;	// Angle to calculate the position of the vertex
	
		for (unsigned int i = 0; i < circle.numVertices; i++)
		{
			AEVec2 tempCoord(coord.x, coord.y); // First vertex of the line
	
			currentAngle += 2 * PI / circle.numVertices;	// Increment currentAngle the required amount of radians
	
												// Calculate the position of the second vertex
			coord.x = (circle.radius / 2) * cos(currentAngle);
			coord.y = (circle.radius / 2) * sin(currentAngle);
	
			// Draw the line with the calculated vertices + the position
			DebugLine line;
			line.start = AEVec2(circle.pos.x + tempCoord.x, circle.pos.y + tempCoord.y);
			line.end = AEVec2(circle.pos.x + coord.x, circle.pos.y + coord.y);
			line.color = circle.color;
			DrawLine(line);
		}
	}
	
	void GraphicsManager::DrawRectangle(const Transform & transform, const AEVec4 & color)
	{
		const f32 size = 1.0f;
		const AEVec2 pos(transform.mTranslationZ.x, transform.mTranslationZ.y);
	
		// Get the normals from each OBB
		AEVec2 normals[2];
		// OBB 1
		normals[0].FromAngle(transform.mOrientation);
		normals[1].FromAngle(transform.mOrientation + PI / 2);
	
		// Get the half extents from each OBB
		AEVec2 halfExtents[2];
		// OBB1
		halfExtents[0] = normals[0] * transform.mScale.x / 2 * size;
		halfExtents[1] = normals[1] * transform.mScale.y / 2 * size;
	
		// Get the corners from the first OBB
		AEVec2 vertices[4];
		vertices[0] = pos - halfExtents[0] + halfExtents[1];
		vertices[1] = pos - halfExtents[0] - halfExtents[1];
		vertices[2] = pos + halfExtents[0] - halfExtents[1];
		vertices[3] = pos + halfExtents[0] + halfExtents[1];
	
		for (int i = 0; i < 4; i++)
		{
			DebugLine line;
			line.start = vertices[i];
			line.end = vertices[(i + 1) % 4];
			line.color = color;
	
			DrawLine(line);
		}
	}
	
	void GraphicsManager::DrawTransform(const Transform & transform)
	{
		AEVec2 xAngle;
		xAngle.FromAngle(transform.mOrientation);
		DebugLine xLine;
		xLine.start = AEVec2(transform.mTranslationZ.x, transform.mTranslationZ.y);
		xLine.end   = AEVec2(transform.mTranslationZ.x, transform.mTranslationZ.y) + xAngle / 2.0f;
		xLine.color = AEVec4(1.0f, 0.0f, 0.0f, 1.0f);
		DrawLine(xLine);
	
		AEVec2 yAngle;
		yAngle.FromAngle(transform.mOrientation + DegToRad(90.0f));
		DebugLine yLine;
		yLine.start = AEVec2(transform.mTranslationZ.x, transform.mTranslationZ.y);
		yLine.end	= AEVec2(transform.mTranslationZ.x, transform.mTranslationZ.y) + yAngle / 2.0f;
		yLine.color = AEVec4(0.0f, 1.0f, 0.0f, 1.0f);
		DrawLine(yLine);
	
		DebugCircle rotCircle;
		rotCircle.pos = AEVec2(transform.mTranslationZ.x, transform.mTranslationZ.y);
		rotCircle.numVertices = 50;
		rotCircle.radius = 1.0f;
		rotCircle.color = AEVec4(0.25f, 0.25f, 1.0f, 1.0f);
		DrawCircle(rotCircle);
	}
	
	void GraphicsManager::SetViewport(s32 left, s32 bot, s32 right, s32 top)
	{
		glViewport(left, bot, right, top);
	}
	
	void GraphicsManager::DrawAllLines(Camera* cam, bool editorCam)
	{
		// create transformations
		glm::mat4 view = glm::mat4(1.0f); // make sure to initialize matrix to identity matrix first
 		glm::mat4 projection = glm::mat4(1.0f);
		int width = WindowMgr->GetCurrentWindow()->GetWidth();
		int height = WindowMgr->GetCurrentWindow()->GetHeight(); 
	
		float aspect = (float)width / height;
		projection = glm::ortho(-cam->mViewRectangleSize * aspect, cam->mViewRectangleSize * aspect, -cam->mViewRectangleSize, cam->mViewRectangleSize, cam->mNearPlane, cam->mFarPlane);
		//view = glm::translate(view, glm::vec3(cam->GetWorldPos().x, cam->GetWorldPos().y, -1.0f));
		view = glm::translate(view, glm::vec3(cam->mOffset.x, cam->mOffset.y, -1.0f));
		view = glm::rotate(view, cam->mRotation, glm::vec3(0.f, 0.f, 1.f));
	
		for (unsigned int i = 0; i < mLineList.size(); i++)
		{
			float vertices[] = {
				mLineList[i].start.x, mLineList[i].start.y,
				mLineList[i].end.x, mLineList[i].end.y
			};

			if (!editorCam && cam->GetOwner())
			{
				if (TransformComp* camTrans = cam->GetOwner()->GetComp<TransformComp>())
					for (unsigned vertex = 0u; vertex < 2; vertex++)
					{
						vertices[2 * vertex] -= camTrans->mLocal.mTranslationZ.x;
						vertices[2 * vertex + 1] -= camTrans->mLocal.mTranslationZ.y;
					}
			}	

			unsigned int indices[] = { 0, 1 };
			unsigned int VBO, VAO, EBO;
			glGenVertexArrays(1, &VAO);
			glGenBuffers(1, &VBO);
			glGenBuffers(1, &EBO);
			// bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
			glBindVertexArray(VAO);
	
			glBindBuffer(GL_ARRAY_BUFFER, VBO);
			glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
	
			glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), (void*)0);
			glEnableVertexAttribArray(0);
	
			// note that this is allowed, the call to glVertexAttribPointer registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
			glBindBuffer(GL_ARRAY_BUFFER, 0);
	
			// remember: do NOT unbind the EBO while a VAO is active as the bound element buffer object IS stored in the VAO; keep the EBO bound.
			//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	
			// You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
			// VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
			glBindVertexArray(0);
	
			// draw our first triangle
			glUseProgram(ID); //projection * view *
			glUniformMatrix4fv(glGetUniformLocation(ID, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
			glUniformMatrix4fv(glGetUniformLocation(ID, "view"), 1, GL_FALSE, glm::value_ptr(view));
			glUniform4f(glGetUniformLocation(ID, "Color"), mLineList[i].color.x, mLineList[i].color.y, mLineList[i].color.z, mLineList[i].color.w);
			glBindVertexArray(VAO); // seeing as we only have a single VAO there's no need to bind it every time, but we'll do so to keep things a bit more organized
									//glDrawArrays(GL_TRIANGLES, 0, 6);
			glDrawElements(GL_LINES, 6, GL_UNSIGNED_INT, 0);
	
			glDeleteVertexArrays(1, &VAO);
			glDeleteBuffers(1, &VBO);
			glDeleteBuffers(1, &EBO);
		}

		mLineList.clear();
	}
	
	void GraphicsManager::CompileLineShaders()
	{
		const GLchar *vertex_shader =
			"#version 330 core\n"
			"layout (location = 0) in vec2 aPos;\n"
			"uniform vec4 Color;\n"
			"uniform mat4 view;"
			"uniform mat4 projection;"
			"out vec4 Frag_Color;\n"
			"void main()\n"
			"{\n"
			"	Frag_Color = Color;\n"
			"	gl_Position = projection * view * vec4(aPos.x, aPos.y,0,1);\n"
			"}\0";
	
		const GLchar* fragment_shader =
			"#version 330\n"
			"in vec4 Frag_Color;\n"
			"out vec4 Out_Color;\n"
			"void main()\n"
			"{\n"
			"	Out_Color = Frag_Color;\n"
			"}\0";
	
		// 2. compile shaders
		unsigned int vertex, fragment;
		int success;
		char infoLog[512];
	
		// vertex Shader
		vertex = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertex, 1, &vertex_shader, NULL);
		glCompileShader(vertex);
		// print compile errors if any
		glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(vertex, 512, NULL, infoLog);
			std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
		};
	
		// similiar for Fragment Shader
		fragment = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragment, 1, &fragment_shader, NULL);
		glCompileShader(fragment);
		// print compile errors if any
		glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(fragment, 512, NULL, infoLog);
			std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
		};
	
		// shader Program
		ID = glCreateProgram();
		glAttachShader(ID, vertex);
		glAttachShader(ID, fragment);
		glLinkProgram(ID);
		// print linking errors if any
		glGetProgramiv(ID, GL_LINK_STATUS, &success);
		if (!success)
		{
			glGetProgramInfoLog(ID, 512, NULL, infoLog);
			std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
		}
	
		// delete the shaders as they're linked into our program now and no longer necessary
		glDeleteShader(vertex);
		glDeleteShader(fragment);
	}

	void  _check_gl_error(const char * file, int line)
	{
		GLenum err(glGetError());

		while (err != GL_NO_ERROR) {
			std::string error;

			switch (err) {
			case GL_INVALID_OPERATION:      error = "INVALID_OPERATION";      break;
			case GL_INVALID_ENUM:           error = "INVALID_ENUM";           break;
			case GL_INVALID_VALUE:          error = "INVALID_VALUE";          break;
			case GL_OUT_OF_MEMORY:          error = "OUT_OF_MEMORY";          break;
			case GL_INVALID_FRAMEBUFFER_OPERATION:  error = "INVALID_FRAMEBUFFER_OPERATION";  break;
			}

			// format error message
			std::stringstream errorMsg;
			errorMsg << "GL_" << error.c_str() << " - " << file << ":" << line << std::endl;

			// print to console
			std::cout << errorMsg.str();

			// send to VS outpput
			OutputDebugString(errorMsg.str().c_str());

			// repeat
			err = glGetError();
		}
	}
}