#pragma once
#include "src\Engine\Core\AEXSystem.h"

#define GLFW_EXPOSE_NATIVE_WIN32

#include <glad/glad.h>
#include <./extern/glfw/glfw3.h>
#include <./extern/glfw/glfw3native.h>
#include <windows.h>

namespace AEX
{

	enum Resolution
	{
		eRes_1920x1080 = 0,
		eRes_1600x900 = 1,
		eRes_1280x720 = 2
	};
	
	class Window : public IBase
	{
		AEX_RTTI_DECL(Window, IBase);
	
		Window(const char * windowName);
	
	public:
		void SetTitle(const char * title);
		void SetFullScreen(bool mFullScreen);
		void SetResolution(Resolution res);
		void ChangeFullsceenOrResolution(bool changedRes = false);

		void SetPosition(GLint xpos, GLint ypos) { glfwSetWindowPos(*this, xpos, ypos); }
		void GetPosition(GLint& xpos, GLint& ypos) { glfwGetWindowPos(*this, &xpos, &ypos); }
	
		Resolution GetResolution() { return mResolution; }
		bool GetFullScreen() { return mbFullScreen; }
		u32 GetWidth();
		u32 GetHeight();
	
		operator GLFWwindow*() { return mWindow; }
		operator HWND() { return mWindowHandle; }
		
	private:
		HWND mWindowHandle;
		GLFWwindow * mWindow;
		Resolution mResolution = eRes_1920x1080;
	
		bool mbFullScreen;
	};
	
	class WindowManager : public ISystem
	{
		AEX_RTTI_DECL(WindowManager, ISystem);
		AEX_SINGLETON(WindowManager);
	
	public:
		Window * CreateNewWindow(const char *windowName);
		void ResizeWindow(Window *window, int width, int height);
		void ProcessInput(Window & window);
	
		Window * GetCurrentWindow() { return mCurrentWindow; }
		void SetCurrentWindow(Window *currentWindow) { mCurrentWindow = currentWindow; }
	
		bool Initialize();
		void Update();
		void Shutdown();

		f32 GetAspectRatio() { return static_cast<f32>(mCurrentWindow->GetWidth()) / static_cast<f32>(mCurrentWindow->GetHeight()); }
	
	private:
		Window *mCurrentWindow;
		std::list<Window*> mWindowList;
	};
	
	#define WindowMgr (WindowManager::Instance())

}