#include <iostream>
#include <cassert>
#include "WindowMgr.h"
#include "Composition/AEXScene.h"
#include "Audio/AudioManager.h"

#include "Texture.h"

namespace AEX
{

	Window::Window(const char * windowName = "New window")
	{
		mWindow = glfwCreateWindow(1920, 1080, windowName, NULL, NULL);
		if (mWindow == NULL)
		{
			std::cout << "Failed to create GLFW window" << std::endl;
			glfwTerminate();
			assert(false);
		};
	
		glfwMakeContextCurrent(mWindow);
	
		mWindowHandle = glfwGetWin32Window(mWindow);
		mbFullScreen = false;
		ChangeFullsceenOrResolution();
	}
	
	void Window::SetTitle(const char * title)
	{
		if (title)
			::SetWindowText(mWindowHandle, title);
	}
	
	void Window::SetFullScreen(bool mFullScreen)
	{
		mbFullScreen = mFullScreen;
		ChangeFullsceenOrResolution(false);
	}
	
	void Window::SetResolution(Resolution res)
	{
		mResolution = res;
		ChangeFullsceenOrResolution(true);
	}
	
	void Window::ChangeFullsceenOrResolution(bool changedRes)
	{
		int resolution[2];
	
		/*if (changedRes)
			mbFullScreen = false;*/
		
		switch (mResolution)
		{
		case eRes_1280x720:
			resolution[0] = 1280;
			resolution[1] = 720;
			break;
		case eRes_1600x900:
			resolution[0] = 1600;
			resolution[1] = 900;
			break;
		case eRes_1920x1080:
			resolution[0] = 1920;
			resolution[1] = 1080;
			break;
		}

		GLFWmonitor* pMonitor = mbFullScreen ? glfwGetPrimaryMonitor() : NULL;
		glfwSetWindowMonitor(*this, pMonitor, 0, 0, resolution[0], resolution[1], GLFW_DONT_CARE);

		SetPosition(0, 25);
	}
	
	u32 Window::GetWidth()
	{
		s32 width, height;
		glfwGetWindowSize(*this, &width, &height);
		return width;
	}
	
	u32 Window::GetHeight()
	{
		s32 width, height;
		glfwGetWindowSize(*this, &width, &height);
		return height;
	}
	
	/*WindowManager::WindowManager() {}*/
	
	Window * WindowManager::CreateNewWindow(const char * windowName = "New window")
	{
		mCurrentWindow = new Window(windowName);
		mWindowList.push_front(mCurrentWindow);
		return mCurrentWindow;
	}
	
	void WindowManager::ResizeWindow(Window * window, int width, int height)
	{
		glViewport(0, 0, width, height);
	}
	
	void WindowManager::ProcessInput(Window & window)
	{
		if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
			glfwSetWindowShouldClose(window, true);
	}
	
	void printglfwError(int code, const char * errorStr) {
		std::cout << errorStr << std::endl;
	}

	void getglfwWindowFocus(GLFWwindow* window, int focused)
	{
		if (focused == GLFW_FALSE)
		{
			//Pause();
			aexScene->SetPauseState(true);
			audiomgr->SetPauseStateForAll(true);
		}
		else if (!aexScene->mOnPauseMenu)
		{
			//ResetAudioVolume();
			aexScene->SetPauseState(false);
			audiomgr->SetPauseStateForAll(false);
		}
	}
	
	bool WindowManager::Initialize()
	{
		//std::cout << "WindowMgr : Initialize" << std::endl;
	
		// set the error callback for error processing. 
		glfwSetErrorCallback(printglfwError);
		Window* window = WindowMgr->CreateNewWindow("Entails"); // Create window
	
		WindowMgr->SetCurrentWindow(window);
		SetMainWindowIcon("data//Icon//Entails.png");
	
		glfwSetWindowFocusCallback(*mCurrentWindow, getglfwWindowFocus);
		glfwSetWindowAspectRatio(window->operator GLFWwindow *(), 16, 9);

		//std::cout << "Initializing GLAD" << std::endl;
		if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
		{
			std::cout << "Failed to initialize GLAD" << std::endl;
			return false;
		}
	
		return true;
	}
	
	void WindowManager::Update()
	{
	}
	
	void WindowManager::Shutdown()
	{
		for (auto it = mWindowList.begin(); it != mWindowList.end(); it++)
			delete *it;
	}

}