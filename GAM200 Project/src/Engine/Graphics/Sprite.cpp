#include "Sprite.h"
#include "SpriteAnimation.h"
#include "Texture.h"
#include "../Platform/AEXTime.h"


namespace AEX
{

	/**************************************************************************/
	/*!
	\fn
	Sprite (default constructor)
	
	\brief
	Initializes all the values of the object to its default values.
	*/
	/**************************************************************************/
	SpriteAnim::SpriteAnim(const char * filename)
	{
		// Initializes all member variables to defaults:
		// mpSheet to NULL
		mpSheet = new Spritesheet(filename);
		// mAnimTimer to zero
		mAnimTimer = 0;
		// mAnimCurrentFrame to zero
		mAnimCurrentFrame = 0;
		// mAnimSpeed to 1.0f
		mAnimSpeed = 1.0f;
		// mbAnimLoop to true
		mbAnimLoop = true;
		// mbAnimPlaying to false
		mbAnimPlaying = false;
		// mbAnimReachedEnd to false
		mbAnimReachedEnd = false;
		mbAnimPingPong = false;
		isPingPonging = false;
	}
	
	/**************************************************************************/
	/*!
	\fn
	UpdateAnimation
	
	\brief
	Updates the animation depending of the animation speed. Then checks for
	the current frame of the animation, then tampering with it.
	*/
	/**************************************************************************/
	void SpriteAnim::UpdateAnimation()
	{
		u32 spriteMovement = (mbAnimPingPong && isPingPonging) ? -1 : 1;
		// Update the animation time
		mAnimTimer += f32(aexTime->GetFrameTime() * mAnimSpeed);
	
		// Reset mbAnimReachedEnd
		mbAnimReachedEnd = false;
	
		// SANITY CHECK: if mpSheet is NULL and mbAnimPlaying is true
		if (mpSheet && mbAnimPlaying)
		{
			// Compare the timer with the current frame's delay 
			if (mAnimTimer >= mpSheet->mDelays[mAnimCurrentFrame])
			{
				// Increment mAnimCurrentFrame
				mAnimCurrentFrame += spriteMovement;
	
				// If the current frame is less than the frame count
				if (u32(mAnimCurrentFrame) < mpSheet->mFrameCount && mAnimCurrentFrame > 0)
				{
					// Reset the timer to time properly the next frame
					mAnimTimer -= mpSheet->mDelays[mAnimCurrentFrame];
				}
				else if (mbAnimPingPong)
				{
					if (mAnimCurrentFrame > 0)
					{
						isPingPonging = true;
						mAnimCurrentFrame = mpSheet->mFrameCount - 1;
					}	
					else
					{
						mbAnimReachedEnd = true;
	
						// If the loop is activated
						if (mbAnimLoop)
						{
							isPingPonging = false;
							// Reset the timer to time properly the next frame
							mAnimTimer -= mpSheet->mDelays[mAnimCurrentFrame];
						}
						else
							// Set mbAnimPlaying to false
							mbAnimPlaying = false;
	
						// Reset the current frame of the animation
						mAnimCurrentFrame = 0;
					}
				}
				else if (u32(mAnimCurrentFrame) >= mpSheet->mFrameCount)
				{
	
					// Set mbAnimReachedEnd to true, since we reached the end of
					// the animation iteration.
					mbAnimReachedEnd = true;
	
					// If the loop is activated
					if (mbAnimLoop)
					{
						// Reset the current frame of the animation
						mAnimCurrentFrame = 0;
						// Reset the timer to time properly the next frame
						mAnimTimer -= mpSheet->mDelays[mAnimCurrentFrame];
					}
					else
						// Set mbAnimPlaying to false
						mbAnimPlaying = false;
				}
			}
		}
	}
	
	/**************************************************************************/
	/*!
	\fn
	SetAnimationFrame
	
	\brief
	Given a frame index, set the mAnimCurrentFrame to this value
	
	\param frameindex
	The current frame we want to set on in the animation.
	*/
	/**************************************************************************/
	void SpriteAnim::SetAnimationFrame(u32 frameIndex)
	{
		// If mpAnim is not NULL and the frameindex is between the possible
		// values from mFrameCount
		if (mpSheet && mpSheet->mFrameCount > frameIndex)
		{
			// Set the mAnimCurrentFrame to this value
			mAnimCurrentFrame = frameIndex;
			// Reset mAnimTimer to 0.0f
			mAnimTimer = 0.0f;
		}
	}
	
	/**************************************************************************/
	/*!
	\fn
	PlayAnimation
	
	\brief
	Sets the animation to a playing state.
	*/
	/**************************************************************************/
	void SpriteAnim::PlayAnimation()
	{
		// Sets mbAnimPlaying to true
		mbAnimPlaying = true;
	}
	
	/**************************************************************************/
	/*!
	\fn
	PauseAnimation
	
	\brief
	It simply pauses the animation.
	*/
	/**************************************************************************/
	void SpriteAnim::PauseAnimation()
	{
		// Sets mbAnimPlaying to false
		mbAnimPlaying = false;
	}
	
	/**************************************************************************/
	/*!
	\fn
	ResetAnimation
	
	\brief
	Sets the current animation frame to zero
	*/
	/**************************************************************************/
	void SpriteAnim::ResetAnimation()
	{
		// Call SetAnimationFrame with 0 as parameter
		SetAnimationFrame(0);
	}
	
	/**************************************************************************/
	/*!
	\fn
	SetLoop
	
	\brief
	Sets mbAnimLoop to the specified value.
	
	\param enable
	A boolean to determine if the animation will loop or not.
	*/
	/**************************************************************************/
	void SpriteAnim::SetLoop(bool enable)
	{
		// Change mbAnimLoop depending of enable
		mbAnimLoop = enable;
	}
	
	/**************************************************************************/
	/*!
	\fn
	SetAnimation
	
	\brief
	Sets mpSheet to the specified value and resets the animation.
	
	\param pSheet
	The new animation we want to use.
	*/
	/**************************************************************************/
	void SpriteAnim::SetSheet(Spritesheet * pSheet)
	{
		// Overwrite mpAnim as the value from pNewAnim
		mpSheet = pSheet;
		// Call ResetAnimation to reset the animation
		ResetAnimation();
	}
	
	/**************************************************************************/
	/*!
	\fn
	SetLoop
	
	\brief
	Sets mbAnimLoop to the specified value.
	
	\param collumns
	The total number of collumns in the Sprite.
	
	\param rows
	The total number of rows in the Sprite.
	
	\param currentFrame
	The current frame of the Sprite.
	*/
	/**************************************************************************/
	glm::mat3 SpriteAnim::GetFrame()
	{
		u32 currentFrame = mAnimCurrentFrame;
		u32 collumns = mpSheet->mColumns;
		u32 rows = mpSheet->mRows;
	
		// Get the current frame position
		AEVec2 FramePos(f32(currentFrame % collumns), f32(currentFrame / collumns));
		// Get the scaling vector
		AEVec2 scaleVec(f32(1 / (double)collumns), f32(1 / (double)rows));
		// Get the translation vector depending on the current frame of the sprite and its size
		AEVec2 transVec(scaleVec.x * FramePos.x, scaleVec.y * FramePos.y);
		// Compute the resultant matrix
		glm::mat3 texTransform(1.0f);
		texTransform = glm::translate(texTransform, glm::vec2(transVec.x, transVec.y));
		texTransform = glm::scale(texTransform, glm::vec2(scaleVec.x, scaleVec.y));
		return texTransform;
	}
	
	/*void Sprite::SetTextureTransform(AEMtx33 * mtx)
	{
	}*/

}
