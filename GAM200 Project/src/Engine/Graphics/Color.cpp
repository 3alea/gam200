#include "Core\AEXTypedefs.h"
#include "Color.h"

Color::Color(f32 rr, f32 gg, f32 bb, f32 aa) : r(rr), g(gg), b(bb), a(aa)
{
}

void Color::set(f32 rr, f32 gg, f32 bb, f32 aa)
{
	r = rr;
	g = gg;
	b = bb;
	a = aa;
}

void Color::set(const Color& _other)
{
	r = _other.r;
	g = _other.g;
	b = _other.b;
	a = _other.a;
}

void Color::set(const spine::Color& _other)
{
	r = _other.r;
	g = _other.g;
	b = _other.b;
	a = _other.a;
}

/*Color::Color(u32 color)
{
}*/

/*Color::operator u32()
{
}*/

Color Color::operator*(const Color & rhs)
{
	Color cpy(*this);
	return cpy *= rhs;
}

Color& Color::operator*=(const Color & rhs)
{
	this->r *= rhs.r;
	this->g *= rhs.g;
	this->b *= rhs.b;
	this->a *= rhs.a;
	return *this;
}

Color Color::operator*(const f32 & sc)
{
	Color cpy(*this);
	return cpy *= sc;
}

Color& Color::operator*=(const f32 & sc)
{
	this->r *= sc;
	this->g *= sc;
	this->b *= sc;
	this->a *= sc;
	return *this;
}

Color Color::operator+(const Color & rhs)
{
	Color cpy(*this);
	return cpy += rhs;
}

Color & Color::operator+=(const Color & rhs)
{
	this->r += rhs.r;
	this->g += rhs.g;
	this->b += rhs.b;
	this->a += rhs.a;
	return *this;
}

Color Color::operator-(const Color & rhs)
{
	Color cpy(*this);
	return cpy -= rhs;
}

Color & Color::operator-=(const Color & rhs)
{
	this->r -= rhs.r;
	this->g -= rhs.g;
	this->b -= rhs.b;
	this->a -= rhs.a;
	return *this;
}
