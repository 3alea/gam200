#ifndef SPRITE_ANIM_DATA_H_
#define SPRITE_ANIM_DATA_H_

#include "../Graphics/Texture.h"

#define ANIM_ERR_DUPLICATE_NAME			0xFFFFFFFF
#define ANIM_ERR_ANIMATION_EXISTS		0xFFFFFFFE
#define ANIM_ERR_NO_ERROR				0


namespace AEX
{

	struct SpriteAnimator // : public IBase
	{
		//AEX_RTTI_DECL(Texture, IBase);
	
		SpriteAnimator(const char * filename);
		~SpriteAnimator();
	
		void				SaveToFile  (const char * filename);
		/*SpriteAnimator *	GetAnimByName(const char * animName);
		void				FreeAllAnim();*/
	
		std::string					mName;			// name of the animation
		std::vector<Texture*>		mFrames;		// frame list
		std::vector<f32>			mDelays;		// Delays
		f32							mDuration;		// animation duration
		u32							mFrameCount;	// framecount
	
		//static std::vector<SpriteAnimator*> mAllAnim;
	};
	
	struct Spritesheet
	{
		Spritesheet(const char * filename);
	
		void SaveToFile(const char * filename);
	
		std::string			mName;			// Name of the animation
		Texture *			mTexture;		// Texture containing all the frames
		f32					mDuration;		// animation duration
		u32					mFrameCount;	// frame count
		u32					mRows;			// row count in the sprite sheet
		u32					mColumns;		// column count in the sprite sheet
		std::vector<f32>	mDelays;		// delays for each frame
	};
	
	// ---------------------------------------------------------------------------
	#endif

}