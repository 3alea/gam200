#pragma once

#include <spine\Color.h>

struct Color 
{
	union
	{
		struct
		{

			f32 r, g, b, a;
		};
		struct
		{

			f32 x, y, z, w;
		};
		f32 v[4];
	};
	Color(f32 rr = 1.0f, f32 gg = 1.0f, f32 bb = 1.0f, f32 aa = 1.0f);
	void set(f32 rr = 1.0f, f32 gg = 1.0f, f32 bb = 1.0f, f32 aa = 1.0f);
	void set(const spine::Color& _other);
	void set(const Color& _other);

	//Color(u32 color); // custom constructor
	//operator u32();	// conversion operator

	Color operator *(const Color & rhs);
	Color& operator *=(const Color & rhs);
	Color operator *(const f32 & sc);
	Color& operator *=(const f32 & sc);
	Color operator +(const Color & rhs);
	Color& operator +=(const Color & rhs);
	Color operator -(const Color & rhs);
	Color& operator -=(const Color & rhs);
};