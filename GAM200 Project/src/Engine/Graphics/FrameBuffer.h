#pragma once
#include "Components\SpriteComps.h"	

using namespace AEX;

struct FrameBuffer
{
	FrameBuffer(unsigned int width = -1, unsigned int height = -1);
	~FrameBuffer();

	void Bind();
	void Unbind();

	void Update();

	bool OnGui();

	Texture* bufferTex;
	TResource<Texture> * mColorTexture = nullptr;
	SpriteComp* mColorTex = nullptr;
	//TResource<Shader>* mpShader = nullptr;

public:
	unsigned int FB_ID, DB_ID;
	//TResource<Shader>* mpShader;
};