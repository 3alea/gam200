#pragma once

#include <Core\AEXCore.h>


#include <ft2build.h>
#include FT_FREETYPE_H
#include <extern/glad/include/glad/glad.h>

using namespace AEX;

struct Character {
	GLuint TextureID;   // ID handle of the glyph texture
	glm::ivec2 Size;    // Size of glyph
	glm::ivec2 Bearing;  // Offset from baseline to left/top of glyph
	GLuint Advance;    // Horizontal offset to advance to next glyph
};

class Font : public AEX::IBase
{
	AEX_RTTI_DECL(Font, AEX::IBase)
public:
	Font(const char * fontPath = "data\\Fonts\\ComicSans.ttf", size_t fontWidth = 0, size_t fontHeight = 48);
	~Font();

	void SetWidth (size_t width)  { mFontWidth  = width;  }
	void SetHeight(size_t heigth) { mFontHeight = heigth; }
	FT_Face GetFace() { return mFace; }
	Character& GetCharacter(const GLchar daChar) { return mCharacters[daChar]; }

private:
	FT_Face mFace;
	size_t  mFontWidth;
	size_t  mFontHeight;

	std::map<GLchar, Character> mCharacters;
};

