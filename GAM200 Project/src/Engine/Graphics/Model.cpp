#include "Core\AEXBase.h"
#include <vector>
#include "Model.h"

#include "GfxMgr.h"
#include <aexmath\aexmath.h>

Model::Model() : mPrimitiveType(eTriangleList), mDrawMode(eDM_Fill)
{
	CreateOpenGLModel();
}

Model::Model(EPrimitiveType primType) : mPrimitiveType(primType), mDrawMode(eDM_Fill)
{
	CreateOpenGLModel();
}

Model::~Model()
{
	Clear();
	DestroyOpenGLModel();
}

const Vertex * Model::GetVertex(u32 offset) const
{
	// Sanity Check
	if (offset >= GetVertexCount())
		return NULL;

	// return vertex
	return (mVertices.data() + offset);
}

Vertex * Model::GetVertex(u32 offset)
{
	// Sanity Check
	if (offset >= GetVertexCount())
		return NULL;

	// return vertex
	return (mVertices.data() + offset);
}

void Model::SetVertexPos(u32 offset, AEVec2 pos)
{
	if (Vertex * vtx = GetVertex(offset))
		vtx->mPosition = pos;
}

void Model::SetVertexPos(u32 offset, f32 x, f32 y)
{
	SetVertexPos(offset, AEVec2(x, y));
}

void Model::SetVertexTex(u32 offset, AEVec2 texCoord)
{
	if (Vertex * vtx = GetVertex(offset))
		vtx->mTexCoord = texCoord;
}

void Model::SetVertexTex(u32 offset, f32 u, f32 v)
{
	SetVertexTex(offset, AEVec2(u, v));
}

void Model::SetVertexColor(u32 offset, Color col)
{
	if (Vertex * vtx = GetVertex(offset))
		vtx->mColor = col;
}

void Model::SetVertexColor(u32 offset, f32 r, f32 g, f32 b, f32 a)
{
	SetVertexColor(offset, Color(r, g, b, a));
}

u32 Model::GetVertexCount() const
{
	return mVertices.size();
}

void Model::AddVertex(const Vertex & newVert)
{
	mVertices.push_back(newVert);
}

void Model::Clear()
{
	mVertices.clear();
}

void Model::Draw(s32 startOffset, s32 endOffset)
{

	// Sanity check
	if (endOffset < startOffset || startOffset >= (int)GetVertexCount())
		return;

	// draw all
	if (startOffset == -1 && endOffset == -1)
	{
		startOffset = 0;
		endOffset = (int)GetVertexCount() - 1; // set end to last vertex
	}

	// clamp to end
	if (endOffset >= (int)GetVertexCount())
		endOffset = (int)GetVertexCount() - 1;

	// count of vertices to draw
	u32 toDrawCount = endOffset - startOffset + 1;

	// Bind the vertex arrays
	Bind();
	//check_gl_error();

	// Set the fill mode
	// TODO(Thomas): Prevent redundant calls. 
	switch (mDrawMode)
	{
	case eDM_Wireframe:
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		break;
	case eDM_Fill:
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		break;
	}

	// Draw

	switch (mPrimitiveType)
	{
	case eTriangleList:
		glDrawElementsBaseVertex(GL_TRIANGLES, toDrawCount, GL_UNSIGNED_SHORT, 0, startOffset);
		break;
	case eLineList:
		glDrawElementsBaseVertex(GL_LINES, toDrawCount, GL_UNSIGNED_SHORT, 0, startOffset);
		break;
	}
	//check_gl_error();
}

void Model::DrawInstanced(u32 instanceCount, s32 startOffset, s32 endOffset)
{
	const int vtxCount = static_cast<int>(GetVertexCount());

	if (instanceCount == 0 && startOffset > endOffset || startOffset >= vtxCount)
		return;

	if (startOffset == -1)
		startOffset = 0;

	if (endOffset == -1 || endOffset >= vtxCount)
		endOffset = vtxCount - 1;

	u32 drawCount = endOffset - startOffset + 1;
	
	switch (mDrawMode)
	{
	case eDM_Wireframe:
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		break;
	case eDM_Fill:
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		break;
	}


	switch (mPrimitiveType)
	{
	case eTriangleList:
		glDrawElementsInstancedBaseVertex(GL_TRIANGLES, drawCount, GL_UNSIGNED_SHORT, 0, instanceCount, startOffset);
		break;
	case eLineList:
		glDrawElementsInstancedBaseVertex(GL_LINES, drawCount, GL_UNSIGNED_SHORT, 0, instanceCount, startOffset);
		break;
	}
}

Model Model::Transform(AEMtx33 & mtx) const
{
	Model newModel = *this;
	newModel.SetPrimitiveType(mPrimitiveType);
	newModel.SetDrawMode(mDrawMode);
	for (u32 i = 0; i < GetVertexCount(); i++)
	{
		const Vertex* pVtx = GetVertex(i);
		Vertex vtx = *pVtx;

		AEMtx33MultVec(&vtx.mPosition, &mtx, &vtx.mPosition);

		newModel.AddVertex(vtx);
	}

	return newModel;
}

Model Model::Transform(AEMtx33 & mtx)
{
	Model newModel = *this;
	newModel.SetPrimitiveType(mPrimitiveType);
	newModel.SetDrawMode(mDrawMode);
	for (u32 i = 0; i < GetVertexCount(); i++)
	{
		const Vertex* pVtx = GetVertex(i);
		Vertex vtx = *pVtx;

		AEMtx33MultVec(&vtx.mPosition, &mtx, &vtx.mPosition);

		newModel.AddVertex(vtx);
	}

	return newModel;
}

void Model::UploadVertexFormatToGPU()
{
	u32		vertexSize = sizeof(Vertex);
	// Specify how the data for the vertices is layed out
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, vertexSize, 0); // position
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, vertexSize, reinterpret_cast<void*>(sizeof(AEVec2))); // texture coord
	glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, vertexSize, reinterpret_cast<void*>(sizeof(AEVec2) * 2)); // color
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
}

void Model::UploadToGPU()
{
	// Sanity Checks
	if (mGLVAO == 0)
		return;

	// Bind
	Bind();

	// upload vertex data as is
	u32		vertexSize = sizeof(Vertex);
	u32		vertexBufferSize = vertexSize * mVertices.size();
	glBufferData(GL_ARRAY_BUFFER, vertexBufferSize, reinterpret_cast<void*>(mVertices.data()), GL_STATIC_DRAW);

	// Specify how the data for the vertices is layed out
	UploadVertexFormatToGPU();

	// compute index dataand upload to GL device
	u32	indexBufferSize = mVertices.size();
	u16 *indexBufferData = new u16[indexBufferSize];
	for (u32 i = 0; i < indexBufferSize; ++i)
		indexBufferData[i] = i;

	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(u16) * indexBufferSize, reinterpret_cast<void*>(indexBufferData), GL_STATIC_DRAW);

	// delete the index buffer data now that we've sent it to the GPU
	delete[] indexBufferData;

	// Unbind
	Unbind();
}

void Model::ReloadToGPU(s32 start, s32 end)
{
	// Sanity check
	if (end < start || start >= (int)GetVertexCount())
		return;

	// upload all
	if (start == -1 && end == -1)
		end = (int)GetVertexCount() - 1; // set end to last vertex

										 // clamp to end
	if (end >= (int)GetVertexCount())
		end = (int)GetVertexCount() - 1;

	// count of vertices to upload
	u32 toUploadCount = end - start + 1;

	// get the vertex buffer handles
	GLuint	vertexBuffer = (GLuint)(mGLVertexBuffer);

	// Bind
	Bind();

	// upload vertex data as is
	u32		vertexSize = sizeof(Vertex);
	u32		vertexBufferSize = vertexSize * toUploadCount;
	u32		startOffset = start * vertexSize;
	glBufferSubData(GL_ARRAY_BUFFER, startOffset, vertexBufferSize, reinterpret_cast<void*>(mVertices.data() + startOffset));
}

void Model::Bind()
{
	if (mGLVAO) 
	{
		glBindVertexArray(mGLVAO);
		glBindBuffer(GL_ARRAY_BUFFER, (GLuint)(mGLVertexBuffer));
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (GLuint)(mGLIndexBuffer));
	}
}

void Model::Unbind()
{
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

u32 Model::GetGLHandleVAO()
{
	return mGLVAO;
}

void Model::CreateOpenGLModel()
{
	// Create vertex array object and bind
	glGenVertexArrays(1, &mGLVAO);
	glBindVertexArray(mGLVAO);

	// Create vertex buffer
	glGenBuffers(1, &mGLVertexBuffer);

	// Create index buffer
	glGenBuffers(1, &mGLIndexBuffer);
}

void Model::DestroyOpenGLModel()
{
	glDeleteBuffers(1, &mGLIndexBuffer);
	glDeleteBuffers(1, &mGLVertexBuffer);
	glDeleteVertexArrays(1, &mGLVAO);
}
