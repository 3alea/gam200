#include "SpriteAnimation.h"
#include "Texture.h"
#include "../Utilities/AEXUtils.h"

#include <string>
#include <vector>
#include <iostream>


namespace AEX
{

	SpriteAnimator::SpriteAnimator(const char * filename)
	{
		// Open the file for reading
		FILE * fp;
		fopen_s(&fp, filename, "rt");
	
		// The first line is the number of frames we have in the file
		int frameCount;
		FIO_ReadInt(fp, &frameCount);
	
		mFrameCount = frameCount;
		mName = filename;
	
		// temporary buffer used to read the data from a file. 
		char buffer[256];
	
		// auxiliary variable to store the frame image file name
		std::string frameFileName;
	
		// auxiliary variable to store the frame delay
		f32 frameDelay;
	
		// loop: read the file for frame data
		for (int i = 0; i < frameCount; ++i)
		{
			// get the file name
			FIO_ReadLine(fp, buffer, 256);
			frameFileName = buffer;
	
			// get the frame duration
			FIO_ReadFloat(fp, &frameDelay);
	
			// Now that we have the data, we load the texture from the file
			Texture * frameTex = new Texture(buffer);
	
			// if the frame is good than we add it to the animation
			// along with the corresponding delay
			if (frameTex)
			{
				mFrames.push_back(frameTex);
				mDelays.push_back(frameDelay);
	
				// accumulate the duration
				mDuration += frameDelay;
			}
		}
	
		// close the file
		fclose(fp);
	}
	
	SpriteAnimator::~SpriteAnimator()
	{
		for (unsigned i = 0; i < mFrames.size(); i++)
			delete mFrames[i];
	}
	
	void SpriteAnimator::SaveToFile(const char * filename)
	{
		// Sanity check 
		if (filename == NULL)
			return;
	
		// Open the file for writing
		FILE * fp;
		fopen_s(&fp, filename, "wt");
	
		// Check that it opened correctly
		if (fp == NULL)
			return;
	
		// First write the number of frames
		FIO_WriteInt(fp, mFrameCount);
		FIO_WriteLine(fp, NULL);
		// write the data for each frame
		for (u32 i = 0; i < mFrameCount; ++i)
		{
			// write the texture file name
			//FIO_WriteLine(fp, mFrames[i]->mpName);
	
			// Write the frame delay
			FIO_WriteFloat(fp, mDelays[i]);
			FIO_WriteLine(fp, NULL);
		}
	
		// close the file
		fclose(fp);
	}
	
	Spritesheet::Spritesheet(const char * filename)
	{
		// temp string buffer
		char buffer[256];
		const int bufferSize = 256;
	
		mFrameCount = 0;
	
		// open new file for reading
		FILE *fp = NULL; 
		fopen_s(&fp, filename, "rt");
	
		if (!fp)
		{
			std::cout << "ERROR : No sprite file implemented" << std::endl;
			return;
		}
	
		// read the basic data
		int tmp;
		FIO_ReadInt(fp, &tmp); mFrameCount = tmp;
		FIO_ReadInt(fp, &tmp); mRows = tmp;
		FIO_ReadInt(fp, &tmp); mColumns = tmp;
	
		mTexture = NULL;
		// read the texture name
		FIO_ReadLine(fp, buffer, bufferSize);
		mTexture = new Texture(buffer);
	
		// reset duration
		mDelays.clear();
		mDuration = 0.0f;
		// read the frame data
		for (u32 i = 0; i < mFrameCount; ++i)
		{
			// Write the frame delay
			FIO_ReadLine(fp, buffer, bufferSize);
	
			// process to read the delay only
			std::string bufferStr = std::string(buffer);
			std::size_t pos = bufferStr.find_last_of(':');
			std::string delayStr = bufferStr.substr(pos + 1);
	
			// set the delay and accumulate to duration
			mDelays.push_back(f32(atof(delayStr.c_str())));
			mDuration += mDelays.back();
		}
	
		// close the file
		fclose(fp);
	}
	
	void Spritesheet::SaveToFile(const char * filename)
	{
	}

}