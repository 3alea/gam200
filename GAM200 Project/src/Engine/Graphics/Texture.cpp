#include "Texture.h"

#define STB_IMAGE_IMPLEMENTATION
#include "./extern/STB/stb_image.h"
#include <glad/glad.h>
#include <./extern/glfw/glfw3.h>

#include <iostream>

#include "WindowMgr.h"

namespace AEX
{

	Texture::Texture(const char * imagePath, TexWrappingMethod texWrapMethod, TexFilterMethod texFiltMethod) : eFilterMethod(texFiltMethod)
	{
		eWrapMethod[0] = texWrapMethod;
		eWrapMethod[1] = texWrapMethod;

		CheckTextureMethods();

		glGenTextures(1, &mID);
		glBindTexture(GL_TEXTURE_2D, mID);
	
		// set texture filtering parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	
		int nrChannels;
		stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.
		unsigned char *data = stbi_load(imagePath, &mWidth, &mHeight, &nrChannels, 4); // Load image
		if (data)
		{
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, mWidth, mHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, data); // Create texture
			glGenerateMipmap(GL_TEXTURE_2D); // Generate mipmaps
		}
		else
			std::cout << "Failed to load texture: "<<stbi_failure_reason() << std::endl;
	
		stbi_image_free(data);
	
		mTexCoord[0] = AEVec2(0.0f, 0.0f);
		mTexCoord[1] = AEVec2(1.0f, 1.0f);
	}

	Texture::Texture(unsigned int width, unsigned int height) : mWidth(width), mHeight(height)
	{
		glGenTextures(1, &mID);
		glBindTexture(GL_TEXTURE_2D, mID);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, mWidth, mHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
		// set texture filtering parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}
	
	void Texture::SetBorderColor(f32 * borderColor)
	{
		for (u32 i = 0; i < 4; i++)
			mBorderColor[i] = borderColor[i];
	
		glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, mBorderColor);
	}

	void Texture::Bind()
	{
		glBindTexture(GL_TEXTURE_2D, mID);
	}

	void Texture::Unbind()
	{
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	void Texture::FromJson(json & value)
	{
		json & wrappable = value["Wrappable"];
		wrappable["x"] >> mbWrappable[0];
		wrappable["y"] >> mbWrappable[1];

		json & wrapMethod = value["WrapMethod"];

		int wrap0 = -1;
		int wrap1 = -1;

		wrapMethod["x"] >> wrap0;
		wrapMethod["y"] >> wrap1;

		eWrapMethod[0] = (TexWrappingMethod)wrap0;
		eWrapMethod[1] = (TexWrappingMethod)wrap1;
	}

	void Texture::ToJson(json & value)
	{
		json wrappable;
		wrappable["x"] << mbWrappable[0];
		wrappable["y"] << mbWrappable[1];
		value["Wrappable"] = wrappable;

		json wrapMethod;

		int wrap0 = static_cast<int>(eWrapMethod[0]);
		int wrap1 = static_cast<int>(eWrapMethod[1]);

		wrapMethod["x"] << wrap0;
		wrapMethod["y"] << wrap1;
		value["WrapMethod"] = wrappable;
	}

	void Texture::CheckTextureMethods()
	{
		// set the texture wrapping parameters
		glBindTexture(GL_TEXTURE_2D, mID);

		if (mbWrappable[0])
		{
			switch (eWrapMethod[0])
			{
			case eRepeat:
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
				break;

			case eRepeat_Mirror:
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
				break;

			case eClamp:
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
				break;

			case eBorderCol:
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
				break;
			}
		}
		else
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);

		if (mbWrappable[1])
		{
			switch (eWrapMethod[1])
			{
			case eRepeat:
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
				break;

			case eRepeat_Mirror:
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
				break;

			case eClamp:
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
				break;

			case eBorderCol:
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
				break;
			}
		}
		else
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		
	
		// set texture filtering parameters
		switch (eFilterMethod)
		{
		case eLinear:
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			break;
		case eNearest:
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			break;
		}
	
		// Magnifying a texture doesn't alter the texture
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}

	void SetMainWindowIcon(const char * path)
	{
		GLFWimage icon; 
		icon.pixels = stbi_load(path, &icon.width, &icon.height, 0, 4);
		Window* mainWindow = WindowMgr->GetCurrentWindow();
		//rgba channels 
		glfwSetWindowIcon(*mainWindow, 1, &icon);
		stbi_image_free(icon.pixels);
	}
}