#include "Core/AEXBase.h"
#include "FrameBuffer.h"

#include "GfxMgr.h"
#include "WindowMgr.h"
#include "Imgui/imgui.h"
#include <experimental/filesystem>
#include "Resources\AEXResourceManager.h"

#include <iostream>

namespace fs = std::experimental::filesystem;

FrameBuffer::FrameBuffer(unsigned int texWidth, unsigned int texHeight)
{
	// framebuffer configuration
	// -------------------------
	glGenFramebuffers(1, &FB_ID);

	if (texWidth == -1)
		texWidth = WindowMgr->GetCurrentWindow()->GetWidth();
	if (texHeight == -1)
		texHeight = WindowMgr->GetCurrentWindow()->GetHeight();

	mColorTex = new SpriteComp(false);
	mColorTex->Initialize();
	bufferTex = new Texture(texWidth, texHeight);

	//mColorTexture = new TResource<Texture>;
	//mColorTexture->SetActualResource(bufferTex);

	mColorTex->mTex = new TResource<Texture>;
	mColorTex->mTex->SetActualResource(bufferTex);

	//mColorTex->mTex = aexResourceMgr->getResource<Texture>("data\\Textures\\Default.png");

	mColorTex->mShader = aexResourceMgr->getResource<Shader>("data\\Shaders\\FB_Blur.shader");

	Bind();
	/*FragColor = texture(ourTexture, TexCoord);
    float average = 0.2126 * FragColor.r + 0.7152 * FragColor.g + 0.0722 * FragColor.b;
    FragColor = vec4(average, average, average, 1.0);*/

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, bufferTex->GetID(), 0);

	// Set the list of draw buffers.
	GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, DrawBuffers);

	//glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, bufferTex->GetID(), 0);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "Framebuffer error : Framebuffer is not complete" << std::endl;

	Unbind();

	//mColorTex->Initialize();

	//mColorTex->mShader = aexResourceMgr->getResource<Shader>("data\\Textures\\FB.shader");ourTexture
}

FrameBuffer::~FrameBuffer()
{
	glDeleteFramebuffers(1, &FB_ID);

	//if (mColorTex)delete mColorTex;
}

void FrameBuffer::Bind()
{
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, FB_ID);
	check_gl_error();
}

void FrameBuffer::Unbind()
{
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
}

void FrameBuffer::Update()
{
	static float time = 0.0f;
	time += 0.128f;
	float val = -3 * cos(time) + 3;
	float val2 = 50.0f * cos(time) + 16;
	float tempVal = 16.0f;
	check_gl_error();

	mColorTex->mShader->get()->use();
	mColorTex->mShader->get()->setFloat("time", 10.0f);
	check_gl_error();

}

bool FrameBuffer::OnGui()
{
	bool changed = false;

	// Miguel
	static std::string actualName = "Select a shader...";
	static bool isPathShown = false;
	std::string previewName;

	if (ImGui::Checkbox("Show shader paths...", &isPathShown)) changed = true;

	// To solve bug where currently selected texture wouldn't change
	// to only the name as soon as you unclicked the checkbox
	if (!isPathShown)
		previewName = aexResourceMgr->GetNameWithExtension(actualName);
	else
		previewName = actualName;

	if (ImGui::BeginCombo("Framebuffer shaders", previewName.c_str()))
	{
		try {
			fs::path textureDir = fs::path("data\\Shaders");

			int count = 0;
			for (auto dirIt = fs::recursive_directory_iterator(textureDir);
				dirIt != fs::recursive_directory_iterator();
				dirIt++, ++count)
			{
				//if (dirIt->is_directory())
				//continue;
				if (fs::is_directory(dirIt->path()))
					continue;

				if (dirIt->path().extension().string() == ".meta")
					continue;

				std::string currentFileName;

				if (isPathShown)
					currentFileName = dirIt->path().relative_path().string();
				else
					currentFileName = dirIt->path().filename().string();

				if (ImGui::Selectable((currentFileName + "##" + std::to_string(count)).c_str()))
				{
					actualName = dirIt->path().relative_path().string().c_str();
					mColorTex->mShader = aexResourceMgr->getResource<Shader>(actualName.c_str());
				}
			}
		} CATCH_FS_ERROR

			ImGui::EndCombo();
	}

	return changed;
}
