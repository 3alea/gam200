#pragma once
#include "../Core/AEXBase.h"
#include <aexmath\AEXMath.h>
#include "Core/AEXSerialization.h"

namespace AEX
{
	enum TexWrappingMethod
	{
		eRepeat,		// Repeats the texture image
		eRepeat_Mirror, // Same as 'eRepeat' but mirrors the image with each repeat
		eClamp,			// Clamps the coordinates between 0 and 1. The result is that higher coordinates become clamped to the edge, resulting in a straight edge pattern
		eBorderCol		// Coordinates outside the range are now given a user-specified border color.
	};
	
	enum TexFilterMethod
	{
		eLinear,
		eNearest
	};
	
	class Texture : public IBase
	{
		AEX_RTTI_DECL(Texture, IBase); 
	
		Texture(const char * imagePath, TexWrappingMethod texWrapMethod = eRepeat, TexFilterMethod texFiltMethod = eLinear);
		Texture(unsigned int width, unsigned int height);
	
	public:
		unsigned int GetID() { return mID; }
		TexWrappingMethod* GetWrapMethod() { return eWrapMethod; }
		void SetWrapMethod(u32 val, TexWrappingMethod method) { eWrapMethod[val] = method; CheckTextureMethods(); }
		bool* GetWrappable() { return mbWrappable; }
		void SetWrappable(u32 val, bool wrap) { mbWrappable[val] = wrap; CheckTextureMethods(); }
		TexFilterMethod GetFilterMethod() { return eFilterMethod; }
		void SetFilterMethod(TexFilterMethod method) { eFilterMethod = method; }
	
		void SetBorderColor(f32* borderColor);
	
		AEVec2* GetTextureTransform() { return mTexCoord;  }
	
		// Getters
		u32 GetWidth() { return mWidth; }
		u32 GetHeight() { return mHeight; }

		void Bind();
		void Unbind();
	
		void FromJson(json & value);
		void ToJson(json & value);

		void CheckTextureMethods();   // Used in constructors, should not be used elsewhere
	
	private:
		unsigned int		mID;
		bool				mbWrappable[2];
		TexWrappingMethod	eWrapMethod[2];
		TexFilterMethod		eFilterMethod;
		f32					mBorderColor[4]; // Only used when eWrapMethod == eBorderCol
		AEVec2				mTexCoord[2];
		int                 mWidth;
		int                 mHeight;
	};

	void SetMainWindowIcon(const char* path);
}