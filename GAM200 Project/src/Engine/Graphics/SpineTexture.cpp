#include <iostream>
#include "SpineTexture.h"
#include "Graphics/Texture.h"

using namespace AEX;
using namespace spine;

void MyTextureLoader::load(AtlasPage& page, const String& path)
{
	Texture * texture = new Texture(path.buffer());

	// sanity check
	if (!texture)
	{
		std::cout << "ERROR: Could not load Spine Atlas Page \"" << path.buffer() << "\"" << std::endl;
		return;
	}

	// pass the texture pointer to the page. Spine doesn't actually do anything with it, 
	// it will be passed back to us when drawing the skeleton. 
	page.setRendererObject(texture);

	// set the texture width and height
	page.width = texture->GetWidth();
	page.height = texture->GetHeight();
}

void MyTextureLoader::unload(void* texture)
{
	delete (Texture*)texture;
}

SpineData::SpineData(const char * atlasFile, const char * skelFile)
{
	LoadSpineAnimations(atlasFile, skelFile);
}


SpineData::~SpineData()
{
	ClearData();
}

// Atlas loading: Loads atlas for specified Spine Animatiion
void SpineData::LoadAtlas(const char * filePath)
{
	// Sanity check - If there is aleady an existing atlas loaded, delete it
	if (atlas)
	{
		delete atlas;
	}

	// Create Atlas
	atlas = new spine::Atlas(filePath, &SpineData::loader);

	// Sanity check - If the atlas was not loaded correctly...
	if (!atlas)
	{
		std::cout << "ERROR: Could not load Atlas \"" << filePath << "\"" << std::endl;
	}
}

// Skeleton loading: since we are using json, we will only implement this loading function. Loading from binary will not be included
void SpineData::LoadSkeletonJson(const char * filePath)
{
	// Sanity check - If there is aleady an existing Skeleton loaded, delete it
	if (skelData)
	{
		delete skelData;
	}
	if (animStateData)
	{
		delete animStateData;
	}

	// Sanity check - Atlas must be previously loaded before the skeleton.
	//                If not, the skeleton will not know where to find its points
	if (!atlas)
	{
		std::cout << "ERROR: \"" << filePath << "\"'s atlas must be loaded before loading its skeleton" << std::endl;
		return;
	}

	// json variable
	SkeletonJson json(atlas);

	// read skeleton file
	skelData = json.readSkeletonDataFile(filePath);

	// loading failed
	if (!skelData)
	{
		std::cout << "Error loading skel data Json: " << json.getError().buffer() << std::endl;
		return;
	}

	// create a new animation state data
	animStateData = new AnimationStateData(skelData);
}

// Data clearing function that clears all of the data set at the moment
void SpineData::ClearData()
{
	// Delete data
	if (atlas)
	{
		delete atlas;
		atlas = 0;
	}

	// Delete skeleton data
	if (skelData)
	{
		delete skelData;
		skelData = 0;
	}

	// Delete animation state data
	if (animStateData)
	{
		delete animStateData;
		animStateData = 0;
	}
}

// Loading all of spine animation through a single function
void SpineData::LoadSpineAnimations(const char * atlasFile, const char * skelFile)
{
	// Clear any previously existing data
	ClearData();
	LoadAtlas(atlasFile);
	LoadSkeletonJson(skelFile);
}

// Texture loader singleton
MyTextureLoader SpineData::loader;
