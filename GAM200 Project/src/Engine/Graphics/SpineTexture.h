#pragma once
#include <spine/Atlas.h>
#include <spine/SkeletonData.h>
#include <spine/SkeletonJson.h>
#include <spine/AnimationStateData.h>
#include <spine/TextureLoader.h>
#include "Texture.h"

class MyTextureLoader : public spine::TextureLoader
{
public:
	virtual void load(spine::AtlasPage& page, const spine::String& path);

	virtual void unload(void* texture);
};

// SpineData class including all of the spine data required to render all Spine animations
class SpineData
{
	AEX_RTTI_DECL_BASE(SpineData);

public:
	SpineData() = default;

	SpineData(const char * atlasFile, const char * skelFile);

	~SpineData();

	// Atlas loading: Loads atlas for specified Spine Animatiion
	void LoadAtlas(const char * filePath);

	// Skeleton loading: since we are using json, we will only implement this loading function. Loading from binary will not be included
	void LoadSkeletonJson(const char * filePath);

	// Data clearing function that clears all of the data set at the moment
	void ClearData();

	// Loading all of spine animation through a single function
	void LoadSpineAnimations(const char * atlasFile, const char * skelFile);

	// Getters
	spine::SkeletonData* GetSkeleton() { return skelData; }

	spine::AnimationStateData* GetAnimation() { return animStateData; }

	spine::Atlas* GetAtlas() { return atlas; }

private:
	static MyTextureLoader loader;
	spine::Atlas* atlas = 0;
	spine::SkeletonData * skelData = 0;
	spine::AnimationStateData* animStateData = 0;
};