#pragma once


namespace AEX
{

	struct BlendingController
	{
		enum BlendFactor
		{
			eZero = GL_ZERO,
			eOne = GL_ONE,
			eSrcColor = GL_SRC_COLOR,
			eOneMinusSrcColor = GL_ONE_MINUS_SRC_COLOR,
			eDstColor = GL_DST_COLOR,
			eOneMinusDstColor = GL_ONE_MINUS_DST_COLOR,
			eSrcAlpha = GL_SRC_ALPHA,
			eOneMinusSrcAlpha = GL_ONE_MINUS_SRC_ALPHA,
			eDstAlpha = GL_DST_ALPHA,
			eOneMinusDstAlpha = GL_ONE_MINUS_DST_ALPHA,
			eConstColor = GL_CONSTANT_COLOR,
			eOneMinusConstColor = GL_ONE_MINUS_CONSTANT_COLOR,
			eConstAlpha = GL_CONSTANT_ALPHA,
			eOneMinusConstAlpha = GL_ONE_MINUS_CONSTANT_ALPHA
		};
	
		enum BlendEquation
		{
			eFuncAdd = GL_FUNC_ADD,
			eFuncSubs = GL_FUNC_SUBTRACT,
			eFuncReverseSubs = GL_FUNC_REVERSE_SUBTRACT
		};
	
		BlendingController(bool enabled = true, BlendFactor source = eSrcAlpha, BlendFactor dest = eOneMinusSrcAlpha, BlendEquation equation = eFuncAdd);
	
		void Enable(bool enable) { mEnabled = enable; }
	
		typedef void(BlendingController::*SetFactorfn)(BlendFactor);
		typedef void(BlendingController::*SetEquationfn)(BlendEquation);
		void BlendFactorOnGui(const char* BlandFactorName, BlendingController::SetFactorfn fn, const char* & selectedItem);
		void BlendEquationOnGui(const char* BlandEquationName, BlendingController::SetEquationfn fn, const char* & selectedItem);
	
		void SetSource(BlendFactor source) { mSource = source; }
		void SetDestination(BlendFactor destination) { mDestination = destination; }
		void SetEquation(BlendEquation equation) { mEquation = equation; }
	
		void SetBlendAttributes();
	
		bool OnGui(const char* &srcFactor_item_current, const char* &destFactor_item_current, const char* &blendEquation_item_current);
	
	private:
		bool mEnabled;
		BlendFactor mSource;
		BlendFactor mDestination;
		BlendEquation mEquation;
	};

}