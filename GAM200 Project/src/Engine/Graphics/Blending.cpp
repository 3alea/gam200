#include "Graphics/GfxMgr.h"
#include "Imgui/imgui.h"
#include "Blending.h"


namespace AEX
{

	#pragma region HELPER FUNCTIONS
	
	void BlendingController::BlendFactorOnGui(const char* BlandFactorName, BlendingController::SetFactorfn fn, const char* & selectedItem)
	{
		static ImGuiComboFlags blendingFlags = 0;
		const char* bFactor_items[] = { "Zero", "One", "SrcColor", "OneMinusSrcColor", "DstColor", "OneMinusDstColor",
										"SrcAlpha", "OneMinusSrcAlpha", "DstAlpha", "OneMinusDstAlpha", "ConstColor",
										"OneMinusConstColor", "ConstAlpha","OneMinusConstAlpha" };
	
		ImGui::Text(BlandFactorName);
	
		if (ImGui::BeginCombo(BlandFactorName, selectedItem, blendingFlags)) // The second parameter is the label previewed before opening the combo.
		{
			int n;
			for (n = 0; n < IM_ARRAYSIZE(bFactor_items); n++)
			{
				bool is_selected = (selectedItem == bFactor_items[n]);
				if (ImGui::Selectable(bFactor_items[n], is_selected))
				{
					selectedItem = bFactor_items[n];
	
					switch (n)
					{
					case 0:
						(this->*fn)(BlendingController::eZero);
						break;
					case 1:
						(this->*fn)(BlendingController::eOne);
						break;
					case 2:
						(this->*fn)(BlendingController::eSrcColor);
						break;
					case 3:
						(this->*fn)(BlendingController::eOneMinusSrcColor);
						break;
					case 4:
						(this->*fn)(BlendingController::eDstColor);
						break;
					case 5:
						(this->*fn)(BlendingController::eOneMinusDstColor);
						break;
					case 6:
						(this->*fn)(BlendingController::eSrcAlpha);
						break;
					case 7:
						(this->*fn)(BlendingController::eOneMinusSrcAlpha);
						break;
					case 8:
						(this->*fn)(BlendingController::eDstAlpha);
						break;
					case 9:
						(this->*fn)(BlendingController::eOneMinusDstAlpha);
						break;
					case 10:
						(this->*fn)(BlendingController::eConstColor);
						break;
					case 11:
						(this->*fn)(BlendingController::eOneMinusConstColor);
						break;
					case 12:
						(this->*fn)(BlendingController::eConstAlpha);
						break;
					case 13:
						(this->*fn)(BlendingController::eOneMinusConstAlpha);
						break;
					}
				}
				if (is_selected)
					ImGui::SetItemDefaultFocus();   // Set the initial focus when opening the combo (scrolling + for keyboard navigation support in the upcoming navigation branch)
			}
	
			ImGui::EndCombo();
		}
	}
	
	void BlendingController::BlendEquationOnGui(const char* BlandEquationName, BlendingController::SetEquationfn fn, const char* & selectedItem)
	{
		static ImGuiComboFlags blendingFlags = 0;
		const char* bEquation_items[] = { "Add", "Substract", "ReverseSubstract" };
	
		ImGui::Text(BlandEquationName);
	
		if (ImGui::BeginCombo(BlandEquationName, selectedItem, blendingFlags)) // The second parameter is the label previewed before opening the combo.
		{
			int n;
			for (n = 0; n < IM_ARRAYSIZE(bEquation_items); n++)
			{
				bool is_selected = (selectedItem == bEquation_items[n]);
				if (ImGui::Selectable(bEquation_items[n], is_selected))
				{
					selectedItem = bEquation_items[n];
	
					switch (n)
					{
					case 0:
						(this->*fn)(BlendingController::eFuncAdd);
						break;
					case 1:
						(this->*fn)(BlendingController::eFuncSubs);
						break;
					case 2:
						(this->*fn)(BlendingController::eFuncReverseSubs);
						break;
					}
				}
				if (is_selected)
					ImGui::SetItemDefaultFocus();   // Set the initial focus when opening the combo (scrolling + for keyboard navigation support in the upcoming navigation branch)
			}
	
			ImGui::EndCombo();
		}
	}
	
	#pragma endregion 
	
	BlendingController::BlendingController(bool enabled, BlendFactor source, BlendFactor dest, BlendEquation equation) : mEnabled(enabled), mSource(source), mDestination(dest), mEquation(equation)
	{
	}
	
	void BlendingController::SetBlendAttributes()
	{
		if (mEnabled)
		{
			glEnable(GL_BLEND);
			glBlendFunc(mSource, mDestination);
			glBlendEquation(mEquation);
		}
		else
		{
			glDisable(GL_BLEND);
		}
	}
	
	bool BlendingController::OnGui(const char* &srcFactor_item_current, const char* &destFactor_item_current, const char* &blendEquation_item_current)
	{
		bool changed = false;
		ImGui::Text("Blending");
		if(ImGui::Checkbox("Is Blending Enabled##3", &mEnabled)) changed = true;
		BlendFactorOnGui("Source Factor", &BlendingController::SetSource, srcFactor_item_current);
		BlendFactorOnGui("Destination Factor", &BlendingController::SetDestination, destFactor_item_current);
		BlendEquationOnGui("Blending Equation", &BlendingController::SetEquation, blendEquation_item_current);

		return changed;
	}

}
