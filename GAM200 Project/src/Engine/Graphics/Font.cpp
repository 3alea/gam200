#include "GfxMgr.h"
#include "Font.h"

Font::Font(const char * fontPath, size_t fontWidth, size_t fontHeight) : mFontWidth(fontWidth), mFontHeight(fontHeight)
{
	if (FT_New_Face(GfxMgr->mFreeType_LB, fontPath, 0, &mFace))
		std::cout << "Error : Font not properly loaded!" << std::endl;

	// Setting the width to 0 lets the face dynamically calculate the width based on the given height.
	FT_Set_Pixel_Sizes(mFace, mFontWidth, mFontHeight);

	// Disable byte-alignment restriction
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	// Load first 128 characters of ASCII set
	for (unsigned char c = 0; c < 128; c++)
	{
		// Load character glyph 
		if (FT_Load_Char(mFace, c, FT_LOAD_RENDER))
		{
			std::cout << "ERROR::FREETYTPE: Failed to load Glyph" << std::endl;
			continue;
		}
		// Generate texture
		GLuint texture;
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(
			GL_TEXTURE_2D,
			0,
			GL_RED,
			mFace->glyph->bitmap.width,
			mFace->glyph->bitmap.rows,
			0,
			GL_RED,
			GL_UNSIGNED_BYTE,
			mFace->glyph->bitmap.buffer
		);
		// Set texture options
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		// Now store character for later use
		Character character = {
			texture,
			glm::ivec2(mFace->glyph->bitmap.width, mFace->glyph->bitmap.rows),
			glm::ivec2(mFace->glyph->bitmap_left, mFace->glyph->bitmap_top),
			mFace->glyph->advance.x
		};
		mCharacters.insert(std::pair<GLchar, Character>(c, character));
	}
}

Font::~Font()
{
	FT_Done_Face(mFace);
}