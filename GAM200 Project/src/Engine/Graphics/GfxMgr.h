#pragma once
#include "../Composition/AEXComposition.h"
#include "Shader.h"
#include "../Core/AEXSystem.h"
#include "../Components/Camera.h"
#include "../Components/AEXTransformComp.h"
#include "../Components/Renderable.h"
#include <list>
#include <vector>

#include <ft2build.h>
#include FT_FREETYPE_H

namespace AEX
{

	struct DebugLine : public Line
	{
		DebugLine(AEVec4 _color = AEVec4(1, 1, 1, 1), AEVec2 _start = AEVec2(0,0), AEVec2 _end = AEVec2(10,0));

		AEVec4 color;
	};
	
	struct DebugCircle
	{
		DebugCircle();
		DebugCircle(AEVec2 _pos, /*GLfloat*/float _radius, /*GLuint*/unsigned _numVertices, AEVec4 _color);

		AEVec2 pos;
		/*GLfloat*/float radius;
		/*GLuint*/unsigned numVertices;
		AEVec4 color;
	};
	
	class GraphicsManager : public ISystem
	{
		AEX_RTTI_DECL(GraphicsManager, ISystem);
		AEX_SINGLETON(GraphicsManager)

		~GraphicsManager() { Shutdown(); }
	
	public:
		bool Initialize();
		void Update();
		void Render();
		void Shutdown();
	
		void ClearFrameBuffer();
		void RenderGOs();
		void SwapBuffers();
	
		// Added by Miguel
		void SetVSync(bool vsyncActive);
		bool IsVSyncActive() const;

		void DrawLine(DebugLine& line);
		void DrawCircle(const DebugCircle& circle);
		void DrawRectangle(const Transform& transform, const AEVec4 & color);
		void DrawTransform(const Transform& transform);
	
		void SetViewport(s32 left, s32 bot, s32 right, s32 top);
	
		std::list<Shader*> mShaderList;
		std::list<Camera*> mCameraList;
		std::list<Renderable*> mRenderableList;
	
		void DrawAllLines(Camera*,bool);

		FT_Library mFreeType_LB;

	public:
		bool mbWireframeMode;
	
	private:
		void CompileLineShaders();
		void RenderRenderable(Renderable* pObj, glm::mat4& view, glm::mat4& proj, Camera* cam);
	
		bool mVSyncEnabled = true;

		std::vector<DebugLine> mLineList;
		int ID;
	};
	
	void  _check_gl_error(const char * file, int line);
}
#define GfxMgr (AEX::GraphicsManager::Instance())
#define check_gl_error() AEX::_check_gl_error(__FILE__, __LINE__)