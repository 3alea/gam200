#pragma once
#include "Color.h"
#include <aexmath\AEXMath.h>

using namespace AEX;
struct Vertex
{
	Vertex()
		:mPosition(AEVec2(0)), mTexCoord(AEVec2(0)), mColor(Color(0, 0, 0)) {}
	Vertex(AEVec2 pos, AEVec2 tex, Color col)
		:mPosition(pos), mTexCoord(tex), mColor(col) {}
	AEVec2 mPosition;	//! x,y
	AEVec2 mTexCoord;	//! u,v
	Color  mColor;		//! r,g,b,a
};