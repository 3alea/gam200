#pragma once
#include "../Core/AEXBase.h"
#include <extern\glm\gtx\matrix_transform_2d.hpp>


namespace AEX
{

	// Forward declare the sprite animation data. It's fine to not include 
	// it in this file because we only are referencing the SpriteAnimationData
	// as pointers in the Sprite class(below). Since a pointer has a fixed size
	// in bytes regardless of the data type it points to, the compiler
	// won't need to know what SpriteAnimationData really is until we use it; and 
	// that's in the CPP
	struct SpriteAnimationData;
	struct Spritesheet;
	
	struct SpriteAnim : public IBase
	{
		AEX_RTTI_DECL(SpriteAnim, IBase);
	
		SpriteAnim(const char * filename);
	
		// Animation Methods
		void		PlayAnimation();
		void		PauseAnimation();
		void		ResetAnimation();
		void		SetLoop(bool enable);
		void		UpdateAnimation();
		void		SetAnimationFrame(u32 frameIndex);
		void		SetSheet(Spritesheet * pSheet);
		glm::mat3   GetFrame();
	
		//void		SetTextureTransform(AEMtx33* mtx);
	
	public:
		Spritesheet *	mpSheet;				//! Animation Resource: This should be loaded by the game state
		int				mAnimCurrentFrame;		//! Current frame of the sprite animatoin
		f32				mAnimTimer;				//! Current time (in seconds) of the animation
		f32				mAnimSpeed;				//! Scalar to multiply the animation speed, greater >= 0.0f;
		bool			mbAnimLoop;				//! Boolean specifying if the animation should loop
		bool			mbAnimPingPong;				//! Boolean specifying if the animation should ping-pong
		bool			mbAnimPlaying;			//! Boolean specifying if the animation is currently playing. 
		bool			mbAnimReachedEnd;		//! Boolean specifying if the animation has finished. 
	
	private:
		bool			isPingPonging;
	};
	// ----------------------------------------------------------------------------

}