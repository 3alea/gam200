/*!
*	\file		Path2Dcpp
*	\brief		Implementation of the functions to move an object along a path.
*	\details	Contains the definition of the functions declared in
*				Path2D.h for the part 3 of assignment 4 of CS230.
*
*	\author		Miguel Echeverria - miguel.echeverria@digipen.edu
*	\date		04/09/2019
*	\copyright	Copyright DigiPen Institute Of Technology. All Rights Reserved
*
*/

#include "Path2D.h"
#include "Imgui/imgui.h"
#include "Graphics/GfxMgr.h"
#include "Platform\AEXInput.h"
#include "Editor\Editor.h"
#include <cmath>


namespace AEX {
	// ----------------------------------------------------------------------------
	/*!@TODO
	\brief		Sets mTime and mPoint to zero.
	*/
	PathPoint2D::PathPoint2D()
	{
		// Default constructor of PathPoint2D

		mTime = 0.0f;						// Sets the time of the point to 0
		mPoint = AEVec2(0.0f, 0.0f);		// Sets the point to (0,0)
		mIsSelected = true;
	}

	// ----------------------------------------------------------------------------
	/*!@TODO
	\brief		Sets mTime to t and mPoint to zero.
	*/
	PathPoint2D::PathPoint2D(f32 t)
	{
		// Conversion constructor of PathPoint2D.

		mTime = t;						// Sets the time of the point to t (parameter)
		mPoint = AEVec2(0.0f, 0.0f);	// Sets the point to (0,0)
		mIsSelected = true;
	}

	// ----------------------------------------------------------------------------
	/*!@TODO
	\brief		Sets mTime to t and mPoint to pt.
	*/
	PathPoint2D::PathPoint2D(f32 t, const AEVec2 & pt)
	{
		// Non-default constructor of PathPoint2D

		mTime = t;		// Sets the time of the point to t (parameter)
		mPoint = pt;	// Sets the point to pt (parameter)
		mIsSelected = true;
	}

	// ----------------------------------------------------------------------------
	/*!@PROVIDED
	\brief		Debug prints to std::cout
	*/
	void PathPoint2D::Print() const
	{
		/*std::cout << "time = " << mTime << "; vals = "
			<< mPoint.x << "," << mPoint.y << ";";*/
	}

	// ----------------------------------------------------------------------------
	/*!@PROVIDED
	\brief		Writes the point to stream
	*/
	void PathPoint2D::ToStream(std::ostream & str) const
	{
		/*str << "time = " << mTime << "; vals = "
			<< mPoint.x << "," << mPoint.y << ";";*/
	}

	// ----------------------------------------------------------------------------
	/*!@PROVIDED
	\brief		Reads the point from stream
	*/
	void PathPoint2D::FromStream(std::istream &str)
	{
		char lineBuff[512];
		str.getline(lineBuff, 512, '\n');
		std::string line = lineBuff;

		// read time
		u32 p0 = line.find_first_of("=") + 1;
		u32 p1 = line.find_first_of(",") - 1;
		std::string tmp = line.substr(p0, p1 - p0);
		mTime = (f32)atof(tmp.c_str());

		// read next part
		line = line.substr(line.find_last_of("=") + 2);

		u32 count = 2;
		u32 i = 0;
		for (; i < count - 1; ++i)
		{
			p0 = line.find_first_of(',');
			tmp = line.substr(0, p0);
			line = line.substr(p0 + 1);
			mPoint.v[i] = (f32)atof(tmp.c_str());
		}
		mPoint.v[i] = (f32)atof(line.c_str());
	}

	// ----------------------------------------------------------------------------
	// PATH2D IMPLEMENTATION BELOW
	// ----------------------------------------------------------------------------

	Path2D::Path2D() : mPoints(0u)
	{

	}

	/*unsigned Path2D::FindPathPoint(unsigned index)
	{
		return mPoints.at(index);
	}*/

	unsigned Path2D::UnselectAllExcept(const PathPoint2D & point)
	{
		unsigned count = 0;
		FOR_EACH(pointIt, mPoints)
		{
			if (pointIt->mIsSelected && &point != &(*pointIt))
			{
				count++;
				pointIt->mIsSelected = false;
			}
		}

		return count;
	}

	unsigned Path2D::NumberOfPointsSelected() const
	{
		unsigned count = 0;
		FOR_EACH(pointIt, mPoints)
		{
			if (pointIt->mIsSelected)
				count++;
		}

		return count;
	}

	void Path2D::Draw(f32 pointCircleRaius, AEVec4 color, GameObject * owner)
	{
		int i = 0;
		AEMtx33 & doTr = AEMtx33::Translate(mPathOrigin.x, mPathOrigin.y);

		FOR_EACH(pointIt, mPoints)
		{
			GfxMgr->DrawCircle(DebugCircle(doTr * pointIt->mPoint, pointCircleRaius/*PATH_CIRCLE_RADIUS*/, 20, color));

			if (pointIt->mIsSelected)
			{
				AEVec2 & rectPos = doTr * pointIt->mPoint;
				Transform rect;
				rect.mTranslationZ = { rectPos.x, rectPos.y, 0.0f };
				rect.mScale = { pointCircleRaius, pointCircleRaius/*PATH_CIRCLE_RADIUS, PATH_CIRCLE_RADIUS*/ };
				// Should I set a different orientation?
				rect.mOrientation = 0.0f;
				GfxMgr->DrawRectangle(rect, AEVec4(1.0f, 0.0f, 0.0f, 1.0f));
			}

			if (i == 0)
			{
				i++;
				continue;
			}

			GfxMgr->DrawLine(DebugLine(color, doTr * std::prev(pointIt)->mPoint, doTr * pointIt->mPoint));
		}

		/*if (mRespectToObject && tr->translationChanged)//Editor->mgizmos->ObjectModified)
		{
			AEMtx33 & transformMtx = AEMtx33::Translate(tr->transDisplacement.x, tr->transDisplacement.y);//tr->mLocal.mTranslationZ.x, tr->mLocal.mTranslationZ.y);

			AEVec2 undoResult;

			//if (undoneOnce == false)
				//undoResult = undoTr * mPath.mPathOrigin;

			//mPath.mPathOrigin = transformMtx * undoResult;

			mPath.mPathOrigin = transformMtx * mPath.mPathOrigin;

			if (!aexInput->KeyPressed(GLFW_KEY_1))
			{
				FOR_EACH(poinIt, mPath.mPoints)
					poinIt->mPoint = transformMtx * poinIt->mPoint;

				tr->translationChanged = false;
			}

			//undoneOnce = true;
			//FOR_EACH(it, mPath.mPoints)
			//	it->mPoint = matrix * it->mPoint;
		}*/
	}

	// ----------------------------------------------------------------------------
	/*!@TODO
	\brief	Adds a new point on the path.

	Implementation Details:
	After a new point is added, this function should compute the length of the
	corresponding segment (this happens if you have 2+ points).
	*/
	PathPoint2D * Path2D::Push(f32 t, const AEVec2 &val)
	{
		if (Size() == 0)
		{
			t = 0.0f;
		}

		// Add the PathPoint2D formed by t and val to the mPoints vector
		mPoints.push_back(PathPoint2D(t, val));

		// If there are at least two points
		if (mPoints.size() > 1)
		{
			// Reverse iterator, points to the last element in the vector
			std::vector<PathPoint2D>::const_reverse_iterator rit = mPoints.crbegin();

			// Store in point2 the last point of mPoints
			AEVec2 point2 = rit->mPoint;

			// Go to the penultimate element of mPoints
			rit++;

			// Store in point1 the penultimate point of mPoints
			AEVec2 point1 = rit->mPoint;

			// Add to the mSegmentLengths vector, the distance between point 1 and 2
			mSegmentLengths.push_back((point2 - point1).Length());
		}

		return &mPoints.back();
	}

	// ----------------------------------------------------------------------------
	/*!@TODO
	\brief	Removes all points and segments.

	Implementation Details:
	Here you should use the clear function of the containers only.
	*/
	void Path2D::Clear()
	{
		// Use the clear method of std::vector to remove all the elements of mPoints and mSegmentLengths
		mPoints.clear();
		mSegmentLengths.clear();
		mPointsSelected = 0;
	}


	// ----------------------------------------------------------------------------
	/*!@TODO
	\brief	Returns the number of points in the path.

	Implemenation Details:
	Return whatwever mPoints.size() returns;
	*/
	u32 Path2D::Size()const
	{
		// Return the number of points in the path (size of mPoints)
		return mPoints.size();
	}

	// ----------------------------------------------------------------------------
	/*!@TODO
	\brief	Returns the duration of the whole path.

	Implementation Details:
	Return the time associated with the last point, if there are no points,
	then it should return zero.
	*/
	f32 Path2D::Duration()const
	{
		auto rbegin = mPoints.rbegin();

		if (rbegin == mPoints.rend())
			return 0.0f;

		// Returns the duration of the entire path (time value of the last point)
		return rbegin->mTime;
	}

	AEVec2 AEX::Path2D::LastPoint() const
	{
		if (mPoints.empty())
			return AEVec2(0, 0);

		return mPoints.back().mPoint;
	}

	// ----------------------------------------------------------------------------
	/*!@TODO
	\brief
	*/
	f32 Path2D::GetTotalLength()const
	{
		// Variable to accumulate the length of each segment
		f32 totalLength = 0.0f;

		// Loop to iterate through the mSegmentLengths vector
		for (std::vector<f32>::const_iterator it = mSegmentLengths.cbegin(); it != mSegmentLengths.cend(); it++)
		{
			// Add each segment length to totalLength
			totalLength += (*it);
		}

		return totalLength;	// Return the total length of the path
	}

	// ----------------------------------------------------------------------------
	/*!@TODO
	\brief	Returns the length from the start to the point represented
	by the normalized parameter u.

	Implementation Details:
	Use this function in SampleParameter() function below.
	*/
	f32 Path2D::GetLengthFromParam(f32 u)const
	{
		// Returns the length of the path until a point associated with the parameter u
		return u * GetTotalLength();
	}

	// ----------------------------------------------------------------------------
	/*!@TODO
	\brief	Gets point on the path from a given time t.

	Implementation Details:
	- Sanity Checks:
	-# If size is equal to zero, return a dummy point with values (0,0).
	-# If size is equal to 1, return the only point in the path.
	- Boundary Checks:
	-# If provided time t is less than zero, return first point in the path.
	-# If provided time t is greater than last point's time, return last point in the path.
	- Compute point on path:
	-# Search for the correct segment given the time t.
	-# Compute duration of the segment.
	-# Compute normalized time tn on the segment.
	-# Use linear interpolation to compute the point on the segment.
	-# return the computed point.
	*/
	AEVec2 Path2D::SampleTime(f32 t) const
	{
		// Sanity checks
		// If the mPoints vector is empty, return the point (0,0)
		if (mPoints.size() == 0)
			return AEVec2(0.0f, 0.0f);

		// If there is only point in the vector, return it
		if (mPoints.size() == 1)
			return mPoints[0].mPoint;


		// Boundary checks
		// If the time is less or equal than initial time, return the first point
		if (t <= mPoints[0].mTime)
			return mPoints[0].mPoint;

		// Reverse iterator pointing to the last element of the vector
		std::vector<PathPoint2D>::const_reverse_iterator rit = mPoints.crbegin();

		// If the time is bigger or equal than the last point's time, then return the last point
		if (t >= rit->mTime)
			return rit->mPoint;


		// Compute point on the path
		// Loop to iterate through the mPoints vector (backwards)
		for (; rit != mPoints.rend(); rit++)
		{
			// If the time is bigger or equal than the time of the current point, break out of the loop
			if (t >= rit->mTime)
				break;
		}

		// Calculate tn, by doing the time t minus the time of the current point,
		// all divided by the duration of the segment that t corresponds to
		f32 tn = (t - rit->mTime) / (std::prev(rit, 1)->mTime - rit->mTime);

		// Use linear interpolation to get the point and return it
		return (std::prev(rit, 1)->mPoint - rit->mPoint) * tn + rit->mPoint;
	}
	// ----------------------------------------------------------------------------
	/*!@TODO
	\brief	Gets a point on the path given the distance from the start of the path.

	Implementation Details:
	- Sanity Checks:
	-# If size is equal to zero, return a dummy point with values (0,0).
	-# If size is equal to 1, return the only point in the path.
	- Boundary Checks:
	-# If provided distance is less than zero, return first point in the path.
	-# If provided distance is greater than total path length, return last point in the path.
	- Compute point on path:
	-# Find segment that corresponds to the distance provided.\n
	You should use the mSegments container to search for the correct segment.
	-# Compoute the normalized parameter tn using the distances as shown in class.\n
	Compute the local distance from the given distance and divided by the length of the segment.
	-# Use Linear interpolation to compute the point on the path.
	*/
	AEVec2 Path2D::SampleDistance(f32 dist) const
	{
		// Sanity checks
		// If the mPoints vector is empty, return the point (0,0)
		if (mPoints.size() == 0)
			return AEVec2(0.0f, 0.0f);

		// If there is only one point, return it
		if (mPoints.size() == 1)
			return mPoints[0].mPoint;


		// Boundary checks
		// If dist is smaller or equal than 0, return the first point
		if (dist <= 0.0f)
			return mPoints[0].mPoint;

		// If dist is bigger or equal than the total length, return the last point
		if (dist >= GetTotalLength())
			return mPoints.rbegin()->mPoint;


		// Compute point in the path
		// To store the previous accumulated length
		f32 prevLength = 0;
		// To store the accumulated length
		f32 accumulatedLength = 0;

		// Iterator to go through the mSegmentLengths vector
		std::vector<f32>::const_iterator it;

		// Variable to be able to access after the loop, the element of mPoints that we want
		int i = 0;

		// Loop to iterate through the mSegmentLengths vector
		for (it = mSegmentLengths.cbegin(); it != mSegmentLengths.cend(); it++, i++)
		{
			// Store the accumulated length
			prevLength = accumulatedLength;

			// Increase the accumulated length by the length of the current segment
			accumulatedLength += (*it);

			// If the accumulated length is bigger than dist, break out of the loop
			if (accumulatedLength > dist)
				break;
		}

		// Calculate tn, by doing the distance dist minus the previous accumulated length,
		// all divided by the length of the segment dist belongs to
		f32 tn = (dist - prevLength) / (accumulatedLength - prevLength);

		// Use linear interpolation to get the point and return it
		//return AEVec2::Lerp(mPoints[i].mPoint, mPoints[i + 1].mPoint, tn);
		return (mPoints[i + 1].mPoint - mPoints[i].mPoint) * tn + mPoints[i].mPoint;
	}

	// ----------------------------------------------------------------------------
	/*!@TODO
	\brief	Gets a point on the path from a normalized distance parameter

	This function should use GetLenghtFromParam() and SampleDistance() to compute
	the point on the path.
	*/
	AEVec2 Path2D::SampleParameter(f32 u) const
	{
		// Use GetLengthFromParam() to get the length that u represents,
		// and use SampleDistance to get the point that we need to return
		return SampleDistance(GetLengthFromParam(u));
	}

	// ----------------------------------------------------------------------------
	/*!@PROVIDED
	\brief	Writes the path to a stream (file, string,etc...).
	*/
	void Path2D::ToStream(std::ostream & str) const
	{
		for (u32 i = 0; i < mPoints.size(); ++i) {
			str << "Point[" << i << "]" << ": ";
			mPoints[i].ToStream(str);
			str << std::endl;
		}
	}

	// ----------------------------------------------------------------------------
	/*!@PROVIDED
	\brief		Reads the path from a stream.
	*/
	void Path2D::FromStream(std::istream &str)
	{
		// read points
		Clear();
		char lineBuff[512];
		// read line until ':'
		str.getline(lineBuff, 512, ':');
		while (str.good())
		{
			PathPoint2D pathPt;
			pathPt.FromStream(str);
			Push(pathPt.mTime, pathPt.mPoint);

			// read line until ':'
			str.getline(lineBuff, 512, ':');
		}
	}


	void PathPoint2D::FromJson(json & value)
	{
		value["Time"] >> mTime;
		value["Point"] >> mPoint;
		value["Is Selected"] >> mIsSelected;
	}
	void PathPoint2D::ToJson(json & value)
	{
		value["Time"] << mTime;
		value["Point"] << mPoint;
		value["Is Selected"] << mIsSelected;
	}

	void Path2D::FromJson(json & value)
	{
		value["Points Selected"] >> mPointsSelected;

		if (value["Ease Function Name"] != nullptr)
			value["Ease Function Name"] >> mEaseFuncName;

		if (value["Ease Function ID"] != nullptr)
			value["Ease Function ID"] >> mEaseFuncID;

		switch (mEaseFuncID)
		{
		case 0:
			mEaseFunction = EaseLinear;
			break;
		case 1:
			mEaseFunction = EaseInQuad;
			break;
		case 2:
			mEaseFunction = EaseOutQuad;
			break;
		case 3:
			mEaseFunction = EaseInOutQuad;
			break;
		}

		if (value["Path Origin"] != nullptr)
			value["Path Origin"] >> mPathOrigin;

		if (value["Initial Path Origin"] != nullptr)
			value["Initial Path Origin"] >> initialPathOrigin;

		//bool changedPoint = (initialPathOrigin.x != mPathOrigin.x || initialPathOrigin.y != mPathOrigin.y);

		json pointVal = value["Path Points"];
		FOR_EACH(pointIt, pointVal)
		{
			PathPoint2D temp;
			temp.FromJson(*pointIt);

			/*if (Editorstate->IsPlaying() && changedPoint)
			{
				AEVec2 displacement = mPathOrigin - initialPathOrigin;
				AEMtx33 & trMtx = AEMtx33::Translate(displacement.x, displacement.y);
				temp.mPoint = trMtx * temp.mPoint;
			}*/

			mPoints.push_back(temp);
		}

		json segmentVal = value["Segment Lengths"];
		FOR_EACH(segmentIt, segmentVal)
		{
			mSegmentLengths.push_back(*segmentIt);
		}
	}
	void Path2D::ToJson(json & value)
	{
		value["Points Selected"] << mPointsSelected;
		value["Ease Function Name"] << mEaseFuncName;
		value["Ease Function ID"] << mEaseFuncID;
		value["Path Origin"] << mPathOrigin;
		value["Initial Path Origin"] << initialPathOrigin;

		FOR_EACH(pointIt, mPoints)
		{
			json pointVal;
			pointIt->ToJson(pointVal);
			value["Path Points"].push_back(pointVal);
		}

		FOR_EACH(segmentIt, mSegmentLengths)
		{
			// Not sure this works
			value["Segment Lengths"].push_back(*segmentIt);
		}
	}


	PathComponent::PathComponent() : mRenderAlways(false), mOnGuiOpened(false), mHidePath(true), mRespectToObject(true), respect_to_object(true)
	{
	}

	void PathComponent::Update()
	{
		if (doneOnce == false && Editorstate->IsPlaying())
		{
			if (mPath.initialPathOrigin.x != mPath.mPathOrigin.x ||
				mPath.initialPathOrigin.y != mPath.mPathOrigin.y)
			{
				FOR_EACH(pointIt, mPath.mPoints)
				{
					AEVec2 displacement = mPath.mPathOrigin - mPath.initialPathOrigin;
					AEMtx33 & trMtx = AEMtx33::Translate(displacement.x, displacement.y);
					pointIt->mPoint = trMtx * pointIt->mPoint;
				}
			}

			aexScene->mPathsUpdatedOnce = true;
			doneOnce = true;
		}

		if (Editorstate->IsPlaying())
			return;
		else
			doneOnce = false;

		if (mRenderAlways || mHidePath == false)
			mPath.Draw(mPathPointRadius, AEVec4(0,0,0,1), mOwner);

		if (Editor->imGui_selectedGO != mOwner)
			return;

		AEMtx33 & undoTr = AEMtx33::Translate(-mPath.mPathOrigin.x, -mPath.mPathOrigin.y);
		AEMtx33 & doTr = AEMtx33::Translate(mPath.mPathOrigin.x, mPath.mPathOrigin.y);

		if (aexInput->KeyPressed(Input::eControl) && aexInput->MouseTriggered(Input::eMouseMiddle))
		{
			mPath.UnselectAllExcept(*mPath.Push(mPath.Duration() + 0.5f, undoTr * Editorstate->mEditorCamera->PointToWorld(aexInput->GetGLFWMousePos())));
			mHidePath = false;
		}

		if (aexInput->KeyPressed(Input::eControl))
		{
			if (aexInput->MouseTriggered(Input::eMouseLeft))
			{
				AEVec2 & mousePos = Editorstate->mEditorCamera->PointToWorld(aexInput->GetGLFWMousePos());

				// For when you have more time, try setting a pointer to the selected point here, so
				// that you don't have to check containment of the mouse pos in every point every time.

				FOR_EACH(pointIt, mPath.mPoints)
				{
					AEVec2 translatedPoint = doTr * pointIt->mPoint;
					if (StaticPointToStaticCircle(&mousePos, &translatedPoint/*&pointIt->mPoint*/, mPathPointRadius/*PATH_CIRCLE_RADIUS*/))
					{
						pointIt->mIsSelected = true;
						UnselectAllExcept(*pointIt);
					}

				}
			}
			else if (aexInput->MousePressed(Input::eMouseLeft))
			{
				AEVec2 & mousePos = Editorstate->mEditorCamera->PointToWorld(aexInput->GetGLFWMousePos());
				PathPoint2D * selected = nullptr;
				bool movingPoint = false;
				unsigned int pointNumber = 0;

				FOR_EACH(pointIt, mPath.mPoints)
				{
					AEVec2 translatedPoint = doTr * pointIt->mPoint;
					if (StaticPointToStaticCircle(&mousePos, &translatedPoint/*&pointIt->mPoint*/, mPathPointRadius/*PATH_CIRCLE_RADIUS*/))
					{
						pointIt->mIsSelected = true;
						UnselectAllExcept(*pointIt);
						selected = &(*pointIt);
						movingPoint = true;
						break;
					}

					pointNumber++;
				}

				if (movingPoint && selected)
				{
					if (aexInput->KeyPressed(Input::eShift))
					{
						AEVec2 toProject;
						AEVec2 otherPoint;

						if (pointNumber == 0)
							otherPoint = doTr * mPath.mPoints[pointNumber + 1].mPoint;
						else
							otherPoint = doTr * mPath.mPoints[pointNumber - 1].mPoint;

						toProject = mousePos - otherPoint;

						f32 angle = (180.0f * toProject.GetAngle()) / PI;
						AEVec2 axis;

						//if (angle >= 135 && angle <= -135)
							//axis = { -1.0f, 0.0f };
						if (angle >= -135 && angle <= -45)
							axis = { 0.0f, -1.0f };
						else if (angle <= 45 && angle >= -45)
							axis = { 1.0f, 0.0f };
						else if (angle < 135 && angle > 45)
							axis = { 0.0f, 1.0f };
						else
							axis = { -1.0f, 0.0f };//axis = { 0.0f, -1.0f };
						//AEVec2 axis = (angle >= 135 && angle <= -135) || (angle <= 45 && angle >= -45) ? AEVec2(1.0f) : 360.0f + angle;
						selected->mPoint = undoTr * (otherPoint + toProject.Project(axis));
						//std::cout << "DEBUG Miguel : Angle returned = " << angle << std::endl;
					}
					else
						selected->mPoint = /*undoTr * */mousePos - mPath.mPathOrigin;
				}
			}
		}

		TransformComp * tr = mOwner->GetComp<TransformComp>();

		if (mRespectToObject && tr->translationChanged)//Editor->mgizmos->ObjectModified)
		{
			AEMtx33 & transformMtx = AEMtx33::Translate(tr->transDisplacement.x, tr->transDisplacement.y);//tr->mLocal.mTranslationZ.x, tr->mLocal.mTranslationZ.y);

			AEVec2 undoResult;

			//if (undoneOnce == false)
				//undoResult = undoTr * mPath.mPathOrigin;

			//mPath.mPathOrigin = transformMtx * undoResult;

			mPath.mPathOrigin = transformMtx * mPath.mPathOrigin;

			if (!aexInput->KeyPressed(GLFW_KEY_1))
			{
				/*FOR_EACH(poinIt, mPath.mPoints)
					poinIt->mPoint = transformMtx * poinIt->mPoint;*/

				tr->translationChanged = false;
			}
		}
	}

	Path2D & PathComponent::GetPath() //borrao const
	{
		return mPath;
	}

	void PathComponent::SetPathOrigin(f32 x, f32 y)
	{
		mPath.mPathOrigin = AEVec2(x, y);
	}

	unsigned PathComponent::UnselectAllExcept(const PathPoint2D & point)
	{
		return mPath.UnselectAllExcept(point);
	}

	void PathComponent::FromJson(json & value)
	{
		this->EditorUpdateableComp::FromJson(value);

		value["Render Always"] >> mRenderAlways;
		value["OnGui Opened"] >> mOnGuiOpened;
		value["Hide Path"] >> mHidePath;
		value["With Respect To Owner"] >> mRespectToObject;

		if (value["Circle Radius"] != nullptr)
			value["Circle Radius"] >> mPathPointRadius;

		mPath.FromJson(value);
	}
	void PathComponent::ToJson(json & value)
	{
		this->EditorUpdateableComp::ToJson(value);

		value["Render Always"] << mRenderAlways;
		value["OnGui Opened"] << mOnGuiOpened;
		value["Hide Path"] << mHidePath;
		value["With Respect To Owner"] << mRespectToObject;
		value["Circle Radius"] << mPathPointRadius;
		mPath.ToJson(value);
	}


	bool PathComponent::OnGui()
	{
		bool isChanged = false;

		mOnGuiOpened = true;

		ImGui::BulletText("Number of points: %d", Size());
		if (ImGui::Checkbox("Render Path Always", &mRenderAlways))
		{
			isChanged = true;
		}
		if (ImGui::Checkbox("Hide Path", &mHidePath))
		{
			isChanged = true;
		}
		if (ImGui::Checkbox("With Respect To Owner", &mRespectToObject))
		{
			isChanged = true;
		}

		if (ImGui::InputScalar("Circle radius", ImGuiDataType_Float, &mPathPointRadius))
		{
			isChanged = true;
		}
		/*ImGui::SameLine();
		if (ImGui::Button("Add New Point"))
		{
			AEVec2 pos(PATH_CIRCLE_RADIUS, 0.0f);

			if (Size() == 0)
				pos.x = mOwner->GetComp<TransformComp>()->mLocal.mTranslationZ.x;

			mPath.Push(mPath.Duration() + 1.0f, LastPoint() + pos);
			isChanged = true;
		}*/

		std::string str = " - ";
		int i = 0;
		f32 step = 0.1f;
		f32 step_fast = 1.0f;

		if (ImGui::TreeNode("Segments"))
		{
			FOR_EACH(pointIt, mPath.mPoints)
			{
				if (i == 0)
				{
					i++;
					continue;
				}

				if (ImGui::TreeNode((std::to_string(i - 1) + str + std::to_string(i)).c_str()))
				{
					f32 originalTime = pointIt->mTime;
					f32 timeToChange = pointIt->mTime - std::prev(pointIt)->mTime;
					if (ImGui::InputScalar("Time", ImGuiDataType_Float, &timeToChange, &step, &step_fast))
					{
						pointIt->mTime = std::prev(pointIt)->mTime + timeToChange;

						f32 timeIncrement = pointIt->mTime - originalTime;

						for (unsigned j = i + 1; j < mPath.Size(); j++)
						{
							mPath.mPoints[j].mTime += timeIncrement;
						}

						isChanged = true;
					}

					ImGui::TreePop();
				}

				i++;
			}
			ImGui::TreePop();
		}

		if (ImGui::BeginCombo("Ease Function", mPath.mEaseFuncName.c_str()))
		{
			if (ImGui::Selectable("EaseLinear"))
			{
				mPath.mEaseFuncName = "EaseLinear";
				mPath.mEaseFunction = EaseLinear;
				mPath.mEaseFuncID = 0;
			}
			if (ImGui::Selectable("EaseInQuad"))
			{
				mPath.mEaseFuncName = "EaseInQuad";
				mPath.mEaseFunction = EaseInQuad;
				mPath.mEaseFuncID = 1;
			}
			if (ImGui::Selectable("EaseOutQuad"))
			{
				mPath.mEaseFuncName = "EaseOutQuad";
				mPath.mEaseFunction = EaseOutQuad;
				mPath.mEaseFuncID = 2;
			}
			if (ImGui::Selectable("EaseInOutQuad"))
			{
				mPath.mEaseFuncName = "EaseInOutQuad";
				mPath.mEaseFunction = EaseInOutQuad;
				mPath.mEaseFuncID = 3;
			}

			ImGui::EndCombo();
		}

		// Draw the path, since we have selected the go that has it.
		if (mHidePath == false)
			mPath.Draw(mPathPointRadius, AEVec4(0, 0, 0, 1), mOwner);

		return isChanged;
	}



	AEVec2 PathComponent::SampleTime(f32 t) const
	{
		return mPath.SampleTime(t);
	}

	AEVec2 PathComponent::SampleParameter(f32 u)const
	{
		return mPath.SampleParameter(u);
	}
 
	AEVec2 PathComponent::SampleDistance(f32 dist) const
	{
		return mPath.SampleDistance(dist);
	}
 
	PathPoint2D * PathComponent::Push(f32 t, const AEVec2 &val)
	{
		return mPath.Push(t, val);
	}

	void PathComponent::Clear()
	{
		mPath.Clear();
	}

	u32 PathComponent::Size() const
	{
		return mPath.Size();
	}

	f32 PathComponent::Duration() const
	{
		return mPath.Duration();
	}

	AEVec2 AEX::PathComponent::LastPoint() const
	{
		if (mPath.mPoints.empty())
		{
			TransformComp * tr = mOwner->GetComp<TransformComp>();

			return AEVec2(tr->mLocal.mTranslationZ.x, tr->mLocal.mTranslationZ.y);
		}

		return mPath.LastPoint();
	}

	f32 PathComponent::GetTotalLength() const
	{
		return mPath.GetTotalLength();
	}

	f32 PathComponent::GetLengthFromParam(f32 u)const
	{
		return mPath.GetLengthFromParam(u);
	}

	EaseFunc PathComponent::GetEaseFunction() const
	{
		return mPath.mEaseFunction;
	}

	void PathFollower::Update()
	{
		if (mShouldStop)
			return;

		PathComponent * path = mOwner->GetComp<PathComponent>();
		
		if (path == nullptr)
		{
			timer = 0.0f;
			//std::cout << "You forgot to add the path component!!!" << std::endl;
			// Should I ASSERT?
			return;
		}

		f32 tn;
		timer += timerDir * (f32)aexTime->GetFrameTime();

		if (mFollowPolicy == Loop && timer > path->Duration())
			timer = 0.0f;
		else if (mFollowPolicy == PingPong && (timer > path->Duration() || timer < 0.0f))
			timerDir *= -1.0f;

		EaseFunc fn = path->GetEaseFunction();
		f32 duration = path->Duration();
		f32 notEased = timer / duration;
		
		tn = fn(notEased);

		if (notEased >= 1.0f)
		{
			mPathFinished = true;

			if (mFollowPolicy != Loop)
				tn = 1.0f;
		}
		else
			mPathFinished = false;

		AEVec2 pos = path->SampleParameter(tn);

		TransformComp * tr = mOwner->GetComp<TransformComp>();

		if(path->changey)
			tr->SetPosition3D(AEVec3(pos.x, pos.y + ChangeY(timer), tr->mLocal.mTranslationZ.z));
		else if(path->changex)
			tr->SetPosition3D(AEVec3(pos.x + ChangeY(timer), pos.y, tr->mLocal.mTranslationZ.z));
		else
			tr->SetPosition3D(AEVec3(pos.x , pos.y, tr->mLocal.mTranslationZ.z));
	}

	f32 PathFollower::ChangeY(f32 time)
	{
		return sin(3 *time) ;
	}

	bool PathFollower::OnGui()
	{
		bool isChanged = false;

		if (ImGui::BeginCombo("Follow Policy", mPreviewName.c_str()))
		{
			if (ImGui::Selectable("Follow And Stop"))
			{
				mFollowPolicy = FollowAndStop;
				mPreviewName = "Follow And Stop";
				isChanged = true;
			}
			if (ImGui::Selectable("Loop"))
			{
				mFollowPolicy = Loop;
				mPreviewName = "Loop";
				isChanged = true;
			}
			if (ImGui::Selectable("Ping Pong"))
			{
				mFollowPolicy = PingPong;
				mPreviewName = "Ping Pong";
				isChanged = true;
			}

			ImGui::EndCombo();
		}

		return isChanged;
	}

	PathFollower::PathFollower() : timer(0.0f), timerDir(1.0f), mShouldStop(false), mPathFinished(false),
								   mFollowPolicy(Policy::Loop), mPreviewName("Loop")
	{
	}

	void PathFollower::SetFollowPolicy(Policy policy)
	{
		mFollowPolicy = policy;
	}

	void PathFollower::ContinueFollowing()
	{
		mShouldStop = false;
	}

	void PathFollower::StopFollowing()
	{
		mShouldStop = true;
	}

	void PathFollower::ResetPosition()
	{
		timer = 0.0f;
	}

	bool PathFollower::HasFinishedPath()
	{
		return mPathFinished;
	}

	PathFollower::~PathFollower()
	{
		timer = 0.0f;
		timerDir = 1.0f;
	}

	void PathFollower::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);

		int policy = -1;
		value["Follow Policy"] >> policy;
		mFollowPolicy = (Policy)policy;

		value["Preview Name"] >> mPreviewName;
	}

	void PathFollower::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);

		int policy = mFollowPolicy;
		value["Follow Policy"] << policy;

		value["Preview Name"] << mPreviewName;
	}
}