/*!
*	\file		Easing.cpp
*	\brief		Implementation of the functions for the easing functionality.
*	\details	Contains the definition of the functions declared in
*				Easing.h for the part 3 of assignment 4 of CS230.
*
*	\author		Miguel Echeverria - miguel.echeverria@digipen.edu
*	\date		04/09/2019
*	\copyright	Copyright DigiPen Institute Of Technology. All Rights Reserved
*
*/

#include "Easing.h"

namespace AEX
{
	// @TODO
	//! ---------------------------------------------------------------------------
	// \fn		LinearEasing
	// \brief	Given an input value between [0,1], returns eased value using:
	//			linear easing (no effect).
	// \return	new time value.
	// ---------------------------------------------------------------------------
	f32 EaseLinear(f32 tn)
	{
		return tn;	// Linear easing, so return value without modifying it
	}

	// @TODO
	//! ---------------------------------------------------------------------------
	// \fn		EaseInQuad
	// \brief	Given an input value between [0,1], returns eased value using:
	//			Ease in: Accelerate towards goal.
	// \return	new time value.
	// ---------------------------------------------------------------------------
	f32 EaseInQuad(f32 tn)
	{
		return tn * tn;	// Return value eased in
	}

	// @TODO
	//! ---------------------------------------------------------------------------
	// \fn		EaseOutQuad
	// \brief	Given an input value between [0,1], returns eased value using:
	//			Ease Out: Decelerate towards goal.
	// \return	new time value.
	// ---------------------------------------------------------------------------
	f32 EaseOutQuad(f32 tn)
	{
		return tn * (2 - tn);	// Return value eased out
	}

	// @TODO
	//! ---------------------------------------------------------------------------
	// \fn		EaseInOutQuad
	// \brief	Given an input value between [0,1], returns eased value using:
	//			Ease In/Out: EaseIn from tn = [0,0.5], EaseOut from tn [0.5,1]
	// \return	new time value.
	// ---------------------------------------------------------------------------
	f32 EaseInOutQuad(f32 tn)
	{
		// Return value using ease in, for interval [0, 0.5]
		if (2 * tn < 1)
			return EaseInQuad(2 * tn) * 0.5f;

		// Return value using ease out, for interval [0.5, 1]
		return EaseOutQuad(2 * tn - 1) * 0.5f + 0.5f;
	}
}
