/*!
*	\file		Raycast.cpp
*	\brief		Implementation of the functions for the raycasting functionality.
*	\details	Contains the definition of the functions declared in
*				Raycast.h for the part 2 of assignment 4 of CS230.
*
*	\author		Miguel Echeverria - miguel.echeverria@digipen.edu
*	\date		04/04/2019
*	\copyright	Copyright DigiPen Institute Of Technology. All Rights Reserved
*
*/

#include "Raycast.h"

namespace AEX 
{
	Ray::Ray(AEVec2 origin, AEVec2 direction) : mOrigin(origin), mDirection(direction)
	{
	}
	Ray::Ray(f32 originX, f32 originY, f32 dirX, f32 dirY) : mOrigin(AEVec2(originX, originY)), mDirection(AEVec2(dirX, dirY))
	{
	}

	f32 Ray::DistFromOrigin(const AEVec2 & intersection)
	{
		return (intersection - mOrigin).Length();
	}

	f32 Ray::DistFromOriginSquared(const AEVec2 & intersection)
	{
		return (intersection - mOrigin).LengthSq();
	}

	// @TODO
	/*!
	 \brief	Performs a raycast test against a line

	 \param origin	Origin of the ray
	 \param outRes	Direction of the ray (not necessarily normalized).
	 \param line	Line segment to raycast against.
	 \param outPi	if not NULL, compute the point of intersection and store
					in this variable such that outPi = origin + dir * ti;
					where ti is the time of intersection.

	 \details	Do not assume that dir is normalized. You should not
				modify dir, nor normalize it.

	 \return	the time of intersection as a factor of dir (if dir is normalized, then
				this is the same as the distance.
	*/
	f32 RayCastLine(const AEVec2 & origin, const AEVec2 & dir, const LineSegment2D & line, AEVec2 * outPi)
	{
		// Denominator of the equation we use to get the time of intersection
		f32 denominator = line.mN * dir;

		// If said denominator is 0, return a negative number (no intersection)
		if (denominator == 0)
			return -1.0f;

		// Compute the time of intersection
		f32 ti = (line.mNdotP0 - line.mN * origin) / denominator;

		// Compute the point of intersection
		AEVec2 pi = origin + dir * ti;

		// If outPi is valid and the point of intersection is a true positive, set *outPi to it
		if (outPi != NULL && (pi - line.mP0) * (pi - line.mP1) <= 0)
			*outPi = pi;
		// Else, it is a false positive. If ti is positve, make it negative
		else if (ti > 0)
			ti *= -1;

		return ti;	// Return the time of intersection
	}

	// @TODO
	/*!
	 \brief	Performs a raycast test against an obb.

	 \param origin	Origin of the ray
	 \param outRes	Direction of the ray (not necessarily normalized).
	 \param line	Line segment to raycast against.
	 \param outPi	if not NULL, compute the point of intersection and store
					in this variable such that outPi = origin + dir * ti;
					where ti is the time of intersection.

	 \details	-Do not assume that dir is normalized. You should not
				modify dir, nor normalize it.
				- This function should use the RayCastLine to find the
				whether there's an intersection with ray and the obb.
				- choose minimum time of intersection if the ray
				intersects multiple edges of the obb (it will).

	 \return	the time of intersection as a factor of dir (if dir is normalized, then
				this is the same as the distance.
	*/
	f32 RayCastRect(const AEVec2 & origin, const AEVec2 & dir, const Transform & rect, AEVec2 * outPi)
	{
		// Compute the half extents of the OBB and store them in an array
		AEVec2 halfExtents[] = { AEMtx33::RotRad(rect.mOrientation) * AEVec2(rect.mScale.x / 2, 0),
			AEMtx33::RotRad(rect.mOrientation) * AEVec2(0, rect.mScale.y / 2) };

		// Store in an array of LineSegment2Ds, each of the lines in the OBB
		LineSegment2D lines[] = { LineSegment2D(rect.mTranslation + halfExtents[0] + halfExtents[1], rect.mTranslation - halfExtents[0] + halfExtents[1]),
								  LineSegment2D(rect.mTranslation - halfExtents[0] + halfExtents[1], rect.mTranslation - halfExtents[0] - halfExtents[1]),
								  LineSegment2D(rect.mTranslation - halfExtents[0] - halfExtents[1], rect.mTranslation + halfExtents[0] - halfExtents[1]),
								  LineSegment2D(rect.mTranslation + halfExtents[0] - halfExtents[1], rect.mTranslation + halfExtents[0] + halfExtents[1]) };

		f32 ti[4] = { 0.0f };	// Array of time of intersections, one for each line
		AEVec2 pi[4] = { };	// Array of points of intersections, one for each line
		f32 minTi;				// Variable to store the minimum time of intersection

		// Loop to go through the four lines of the OBB
		for (int i = 0; i < 4; i++)
		{
			// Fill the corresponding element of the ti array with its corresponding time of intersection
			// Also, by calling RayCastLine, we are filling up the array of points of intersections
			ti[i] = RayCastLine(origin, dir, lines[i], &pi[i]);

			// If it is the first iteration
			if (i == 0)
			{
				minTi = ti[0];		// Set minTi to the first time of intersection

				// If outPi is not NULL, set *outPi to the first point of intersection
				if (outPi != NULL)
					*outPi = pi[0];
			}

			// If the current time of intersection is positive or zero
			if (ti[i] >= 0)
			{
				// If minTi was negative
				if (minTi < 0)
				{
					// Set minTi to the current time of intersection
					minTi = ti[i];

					// If outPi is not NULL, set *outPi to the current point of intersection
					if (outPi != NULL)
						*outPi = pi[i];
				}
				// Else (minTi is positive or zero) if the current time of intersection is smaller than minTi
				else if (ti[i] < minTi)
				{
					// Set minTi to the current time of intersection
					minTi = ti[i];

					// If outPi is not NULL, set *outPi to the current point of intersection
					if (outPi != NULL)
						*outPi = pi[i];
				}
			}
		}

		return minTi;	// Return the minimum time of intersection
	}

	// @TODO
	/*!
	 \fn		RayCastCircle
	 \brief	Performs a raycast test against a circle.

	 \param origin	Origin of the ray
	 \param outRes	Direction of the ray (not necessarily normalized).
	 \param circle	Circle center
	 \param radius	Circle radius.
	 \param outPi	if not NULL, compute the point of intersection and store
					in this variable such that outPi = origin + dir * ti;
					where ti is the time of intersection.

	 \details	Do not assume that dir is normalized. You should not
				modify dir, nor normalize it.

	 \return	the time of intersection as a factor of dir (if dir is normalized, then
				this is the same as the distance.
	*/
	f32 RayCastCircle(const AEVec2 & origin, const AEVec2 & dir, const AEVec2 & circle, f32 radius, AEVec2 * outPi)
	{
		// Compute the a part of the quadratic equation
		f32 a = dir * dir;

		// Compute the b part of the quadratic equation
		f32 b = dir * (circle - origin) * (-2);

		// Compute the c part of the quadratic equation
		f32 c = ((circle - origin) * (circle - origin)) - (radius * radius);

		// Compute delta (what goes inside of the square root in the quadratic equation)
		f32 delta = b * b - 4 * a * c;

		f32 ti1;	// Variable to store the first possible solution of the equation
		f32 ti2;	// Variable to store the second possible solution of the equation

		// If delta is 0, there will only be one solution
		if (delta == 0)
		{
			// So, set ti1, to the solution of the equation
			ti1 = -b / (2 * a);

			// If outPi is not NULL, compute the point of intersection and set *outPi to it
			if (outPi != NULL)
				*outPi = origin + dir * ti1;

			return ti1;		// Return the time of intersection
		}
		// Else if delta is positive, there will be two solutions
		else if (delta > 0)
		{
			// Compute both solutions and store them into two different variables
			ti1 = (-b + sqrt(delta)) / (2 * a);
			ti2 = (-b - sqrt(delta)) / (2 * a);

			// If both times of intersection are negative, both points are false positives
			if (ti1 < 0 && ti2 < 0)
			{
				return -1.0f;	// Return a negative number (no intersection)
			}
			// Else if both times of intersection are positive or zero, both of
			// them are true positives, but we will only consider the smallest ti
			else if (ti1 >= 0 && ti2 >= 0)
			{
				f32 minTi = std::min(ti1, ti2);	// Get the smallest ti

				// If outPi is not NULL, compute the point of intersection and set *outPi to it
				if (outPi != NULL)
					*outPi = origin + dir * minTi;

				return minTi;	// Return the minimum time of intersection
			}
			// Else if ti1 is positive or zero, and ti2 is negative, ti2 will be a false positive
			else if (ti1 >= 0 && ti2 < 0)
			{
				// If outPi is not NULL, compute the point of intersection and set *outPi to it
				if (outPi != NULL)
					*outPi = origin + dir * ti1;

				return ti1;		// Return the time of intersection
			}
			// Else if ti1 is negative, and ti2 is positive or zero, ti1 will be a false positive
			else if (ti1 < 0 && ti2 >= 0)
			{
				// If outPi is not NULL, compute the point of intersection and set *outPi to it
				if (outPi != NULL)
					*outPi = origin + dir * ti2;

				return ti2;		// Return the time of intersection
			}
		}

		// If none of the above cases returned, that means delta is negative,
		// so there is no intersection, therefore return a negative ti
		return -1.0f;
	}


	// @PROVIDED
	/*!
	 \brief	Performs a raycast test against a circle. Simple wrapper around
			the function above
	*/
	f32 RayCastCircle(const Ray & ray, const AEVec2 & circle, f32 radius, AEVec2 * outPi)
	{
		return RayCastCircle(ray.mOrigin, ray.mDirection, circle, radius, outPi);
	}

	// @PROVIDED
	/*!
	 \brief	Performs a raycast test against an obb. Simple wrapper around
			the function above
	*/
	f32 RayCastRect(const Ray & ray, const Transform &rect, AEVec2 * outPi)
	{
		return RayCastRect(ray.mOrigin, ray.mDirection, rect, outPi);
	}

	// @PROVIDED
	/*!
	 \brief	Performs a raycast test against a line. Simple wrapper around
			the function above
	*/
	f32 RayCastLine(const Ray & ray, const LineSegment2D & line, AEVec2 * outPi)
	{
		return RayCastLine(ray.mOrigin, ray.mDirection, line, outPi);
	}
}