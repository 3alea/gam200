#pragma once

#include <aexmath\aexmath.h>
#include <iostream>
#include "Core/AEXSerialization.h"
#include "Composition/AEXComponent.h"
#include "Components\Logic.h"
#include "Composition\EditorUpdateable.h"
#include "AI/Easing.h"

//#define PI 3.14159265359


namespace AEX 
{
	const f32 PATH_CIRCLE_RADIUS = 1;


// ----------------------------------------------------------------------------
//!	\class	PathPoint2D
//!	\brief	Describes a point for a path, associated to a time mTime.
struct PathPoint2D : public ISerializable
{
	// ------------------------------------------------------------------------
	// DATA
	f32		mTime;
	AEVec2 mPoint;
	bool mIsSelected;

	void FromJson(json & value);
	void ToJson(json & value);

	// ------------------------------------------------------------------------
	// METHODS

	// Constructors

	// @TODO
	//! \brief	Sets time and point to zero.
	PathPoint2D();

	// @TODO
	//! \brief	Sets time to t and point to zero.
	PathPoint2D(f32 t);

	// @TODO
	//! \brief	Sets time to t and point to pt.
	PathPoint2D(f32 t, const AEVec2 & pt);

	// @PROVIDED
	// Debug and save to file
	void Print() const;
	void ToStream(std::ostream & str) const;
	void FromStream(std::istream &str);
};


// ----------------------------------------------------------------------------
/*!
	\class	Path2D
	\brief	Class that holds several path points and provides methods for
			moving along a path.
*/
	struct Path2D : public ISerializable
	{
	public:

		Path2D();

		// ------------------------------------------------------------------------
		// DATA
		AEVec2 mPathOrigin = AEVec2(0, 0);
		std::vector<PathPoint2D> mPoints;	//!	Points on the path associated with a time
		std::vector<f32> mSegmentLengths;	//! Lengths of each segment.
		unsigned mPointsSelected;
		EaseFunc mEaseFunction = EaseLinear;
		unsigned int mEaseFuncID = 0;
		std::string mEaseFuncName = "EaseLinear";
		AEVec2 initialPathOrigin = mPathOrigin;
		

		void FromJson(json & value);
		void ToJson(json & value);

		void Draw(f32 pointCircleRaius = 1.0f, AEVec4 color = AEVec4(0, 0, 0, 1), GameObject * owner = nullptr);

		// ------------------------------------------------------------------------
		// METHODS

		//std::vector<PathPoint2D>::iterator & FindPathPoint(unsigned index);

		// Unselects all the points except the one passed as parameter.
		// Returns the number of points that were selected but are now unselected.
		unsigned UnselectAllExcept(const PathPoint2D & point);

		// Returns the total number of points in the path that are currently selected.
		unsigned NumberOfPointsSelected() const;

		// @TODO
		//! Gets point from time. 
		AEVec2 SampleTime(f32 t) const;

		// @TODO
		//! Gets a point from normalized distance parameter
		AEVec2 SampleParameter(f32 u)const;

		// @TODO
		//! Gets a point from a distance from the start. 
		AEVec2 SampleDistance(f32 dist) const;

		// @TODO
		//! Adds a new point on the path and computes the length
		// of the corresponding segment (this can only happen if you have 2+ points). 
		PathPoint2D * Push(f32 t, const AEVec2 &val);

		// @TODO
		//! Removes all points and segments (hint: use the clear function).
		void Clear();

		// @TODO
		//! Returns the size of the 
		u32 Size() const;

		// @TODO
		//! Returns the duration of the whole path
		//	In other words, the time associated with the last point.
		f32 Duration() const;

		AEVec2 LastPoint() const;

		// @TODO
		//! Returns the total length of the path
		f32 GetTotalLength() const;

		// @TODO
		//! Returns the length from the start to the point represented
		//	by the normalized parameter u. (Hint: Use this function in 
		//	SampleParameter() function above).
		f32 GetLengthFromParam(f32 u)const;

		// @PROVIDED
		//!	Writes the path to a stream (file, string,etc...).
		void ToStream(std::ostream & str) const;

		// @PROVIDED
		//!	Reads the path from a stream.
		void FromStream(std::istream &str);

		bool changey = false;
	};


	// Separate the path class from the path component, in case we want
	// to add a path as a component, or simply as a path that is not a
	// components.
	class PathComponent : public EditorUpdateableComp
	{
	public:

		PathComponent();

		void Update();

		bool doneOnce = false;

		void FromJson(json & value);
		void ToJson(json & value);

		bool OnGui();

		bool mRenderAlways;
		bool mOnGuiOpened;
		bool mHidePath;

		// You should only change this one
		bool mRespectToObject;

		f32 mPathPointRadius = 1.0f;

		// ------------------------------------------------------------------------
		// METHODS

		Path2D & GetPath() ;

		void SetPathOrigin(f32 x = 0.0f, f32 y = 0.0f);

		// Unselects all the points except the one passed as parameter.
		// Returns the number of points that were selected but are now unselected.
		unsigned UnselectAllExcept(const PathPoint2D & point);

		// @TODO
		//! Gets point from time. 
		AEVec2 SampleTime(f32 t) const;

		// @TODO
		//! Gets a point from normalized distance parameter
		AEVec2 SampleParameter(f32 u)const;

		// @TODO
		//! Gets a point from a distance from the start. 
		AEVec2 SampleDistance(f32 dist) const;

		// @TODO
		//! Adds a new point on the path and computes the length
		// of the corresponding segment (this can only happen if you have 2+ points). 
		PathPoint2D * Push(f32 t, const AEVec2 &val);

		// @TODO
		//! Removes all points and segments (hint: use the clear function).
		void Clear();

		// @TODO
		//! Returns the number of points in the path
		u32 Size() const;

		// @TODO
		//! Returns the duration of the whole path
		//	In other words, the time associated with the last point.
		f32 Duration() const;

		AEVec2 LastPoint() const;

		// @TODO
		//! Returns the total length of the path
		f32 GetTotalLength() const;

		// @TODO
		//! Returns the length from the start to the point represented
		//	by the normalized parameter u. (Hint: Use this function in 
		//	SampleParameter() function above).
		f32 GetLengthFromParam(f32 u)const;

		EaseFunc GetEaseFunction() const;

		bool changey = false;
		bool changex = false;
	private:
		Path2D mPath;
		bool respect_to_object;
		//bool undoneOnce = false;
	};

	// ----------------------------------------------------------------------------


	class PathFollower : public LogicComp
	{
		AEX_RTTI_DECL(PathFollower, LogicComp);

	public:

		enum Policy {
			FollowAndStop,
			Loop,
			PingPong
		};

		PathFollower();

		void Update();

		f32 ChangeY(f32 time);

		bool OnGui();

		void SetFollowPolicy(Policy policy);
		Policy GetPolicy() { return mFollowPolicy; }

		void ContinueFollowing();
		void StopFollowing();
		void ResetPosition();
		bool HasFinishedPath();

		virtual ~PathFollower();

		f32 GetTime() { return timer; }
		void SetTime(f32 time) { timer = time; }

		void FromJson(json & value);
		void ToJson(json & value);
		
		// Need to access this from another class.
		// That is why I made it public
		bool mShouldStop;
		f32 timer = 0.0f;

	private:
		bool mPathFinished;
		f32 timerDir = 1.0f;
		Policy mFollowPolicy;
		std::string mPreviewName = "Loop";
	};
}