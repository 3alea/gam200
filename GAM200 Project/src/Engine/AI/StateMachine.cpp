/*!
*	\file		StateMachine.cpp
*	\brief		Implementation StateMachine.h functions.
*	\details	Contains the implementation for all the functions
*				declared in StateMachine.h.
*	\author		Gentzane Pastor - gentzane.pastor@digipen.edu
*	\date		03/28/2019
*	\copyright	Copyright DigiPen Institute Of Technology. All Rights Reserved
*
*/

#include "src/Engine/Components/AEXComponents.h"
#include "StateMachine.h"

namespace AEX
{


//Iterator for the States vector
	std::vector<State*>::iterator it;

	// ----------------------------------------------------------------------------
	// STATE MACHINE
	// ----------------------------------------------------------------------------

	/*!
	*	\fn StateMachine
	*	\brief	Default contructor of StateMachine
	*/
	StateMachine::StateMachine()
	{
		//Goes through all the states
		for (it = mStates.begin(); it != mStates.end(); it++)
		{
			//Sets the states to nullptr
			*it = nullptr;
		}

		//Sets the actor to nullptr
		mActor = nullptr;

		//Sets the mCurrentState to nullptr
		mCurrentState = nullptr;

		//Sets the mNextState to nullptr
		mNextState = nullptr;

		//Sets the mInitialState to nullptr
		mInitialState = nullptr;
	}

	/*!
	*	\fn StateMachine
	*	\brief Non default contructor of StateMachine
	*	\param actor - Pointer to an actor
	*/
	StateMachine::StateMachine(Actor * actor)
	{
		//Checks if the given pointer is nullptr
		if (actor == nullptr)
			return;

		//Sets the mActor to the given actor
		mActor = actor;

		//Sets the mCurrentState to nullptr
		mCurrentState = nullptr;

		//Sets the mNextState to nullptr
		mNextState = nullptr;

		//Sets the mInitialState to nullptr
		mInitialState = nullptr;
	}

	/*!
	*	\fn ~StateMachine
	*	\brief Destructor of StateMachine
	*/
	StateMachine::~StateMachine()
	{
		//Clears all the states
		Clear();
	}

	/*!
	*	\fn AddState
	*	\brief Adds a new state to the state machine
	*	\param state - Pointer to a state
	*/
	void StateMachine::AddState(State * state)
	{
		//Checks if the state is nullptr
		if (state == nullptr)
			return;

		//Goes through all the states
		for (it = mStates.begin(); it != mStates.end(); it++)
		{
			//Checks if the name is duplicated
			if (state->mName == (*it)->mName)
				return;
		}

		//Sets the mActor to the actor of the state
		state->mActor = mActor;

		//Sets the mOwnerStateMachine to this state
		state->mOwnerStateMachine = this;

		//Adds the state tu the states vector
		mStates.push_back(state);
	}

	/*!
	*	\fn RemoveState
	*	\brief RemoveState a state from the state machine
	*	\param state - Pointer to a state
	*/
	void StateMachine::RemoveState(State * state)
	{
		//Checks if the state is nullptr
		if (state == nullptr)
			return;

		//Goes through the states
		for (it = mStates.begin(); it != mStates.end(); it++)
		{
			//Check where is the state
			if (state == *it)
			{
				//Erases the state
				mStates.erase(it);

				//Returns
				return;
			}
		}
	}

	/*!
	*	\fn Clear
	*	\brief Removes all the states from the state machine
	*/
	void StateMachine::Clear()
	{

		//Goes through the states
		for (it = mStates.begin(); it != mStates.end();)
		{
			//Creates a copy of the iterator
			State* temp = *it;

			//Erases the state
			it = mStates.erase(it);

			//Deletes the saved iterator
			delete temp;

			//Checks if the iterator is on the end
			if (it == mStates.end())
				return;

		}

		//Sets the mCurrentState to nullptr
		mCurrentState = nullptr;

		//Sets the mNextState to nullptr
		mNextState = nullptr;

		//Sets the mInitialState to nullptr
		mInitialState = nullptr;
	}

	/*!
	*	\fn GetState
	*	\brief Finds a state by name
	*	\param stateName - Pointer to a state name
	*	\return Pointer to a state
	*/
	State* StateMachine::GetState(const char * stateName)
	{
		//Checks if the name is valid
		if (stateName == nullptr)
			return nullptr;

		//Goes through the list of states
		for (it = mStates.begin(); it != mStates.end(); it++)
		{
			//Checks if the current name is equal to the given name
			if (stateName == (*it)->mName)
			{
				//Return the state
				return *it;
			}
		}

		//Return nullptr if the state is not found
		return nullptr;
	}

	/*!
	*	\fn SetInitState
	*	\brief Sets the initial state to the given pointer
	*	\param state - Pointer to a state
	*/
	void StateMachine::SetInitState(State * state)
	{
		//Checks if the state is nullptr
		if (state == nullptr)
			return;

		//Goes through the states
		for (it = mStates.begin(); it != mStates.end(); it++)
		{
			//Checks if the state already exist
			if (state == *it)
			{
				//Sets the mInitialState to the given state
				mInitialState = state;

				//Returns
				return;
			}
		}
	}

	/*!
	*	\fn SetInitState
	*	\brief Sets the initial state to one state whose name matches the specified one
	*	\param stateName - Pointer to a state name
	*/
	void StateMachine::SetInitState(const char * stateName)
	{
		//Checks if the name is valid
		if (stateName == nullptr)
			return;

		//Sets the initial state
		SetInitState(GetState(stateName));
	}

	/*!
	*	\fn ChangeState
	*	\brief Changes state to the specified state.
	*	\param state - Pointer to a state
	*/
	void StateMachine::ChangeState(State * state)
	{
		//Checks if the state is nullptr
		if (state == nullptr)
			return;

		//Goes through the states
		for (it = mStates.begin(); it != mStates.end(); it++)
		{
			//Checks if the state already exist
			if (state == (*it))
			{
				//Sets the mNextState to the given state
				mNextState = state;

				//Returns
				return;
			}
		}
	}

	/*!
	*	\fn ChangeState
	*	\brief Sets the state to one state whose name matches the specified one
	*	\param stateName - Pointer to a state name
	*/
	void StateMachine::ChangeState(const char * stateName)
	{
		//Checks if the name is valid
		if (stateName == nullptr)
			return;

		//Sets the next state
		mNextState = GetState(stateName);
	}

	/*!
	*	\fn Update
	*	\brief Update of the state machine
	*/
	void StateMachine::Update()
	{
		//Checks if the current state is nullptr
		if (mCurrentState == nullptr)
			return;

		//Checks if the mCurrentState is different mNextState
		if (mCurrentState != mNextState && mNextState)
		{
			//Calls mCurrentState's InternalExit()
			mCurrentState->InternalExit();

			//Sets the next state to the current state
			mCurrentState = mNextState;

			//Calls mNextState's InternalEnter()
			mCurrentState->InternalEnter();
		}

		//Calls mCurrentState's InternalUpdate()
		mCurrentState->InternalUpdate();
	}

	/*!
	*	\fn Reset
	*	\brief Resets the state machine.
	*/
	void StateMachine::Reset()
	{
		//Checks if the mCurrentState is valid 
		if (mCurrentState != nullptr)
		{
			//Calls exit of the current state
			mCurrentState->InternalExit();
		}

		//Changes the current state
		mCurrentState = mInitialState;

		//Changes the next state
		mNextState = mInitialState;

		//Calls InternalEnter of the mCurrentState
		mCurrentState->InternalEnter();
	}

	// ----------------------------------------------------------------------------
	// STATE
	// ----------------------------------------------------------------------------

	/*!
	*	\fn State
	*	\brief Non default constructor State
	*	\param name - Pointer to a name of the state
	*/
	State::State(const char * name)
	{
		//Checks if the given name is valid
		if (name != nullptr)
		{
			//Sets the mName to the given name
			mName = name;
		}

		//If the given name is invalid
		else
		{
			//Sets it as unnamed
			mName = "unnamed";
		}

		//Sets mOwnerStateMachine to nullptr
		mOwnerStateMachine = nullptr;

		//Sets mActor to nullptr
		mActor = nullptr;

		//Sets the time to 0
		mTimeInState = 0.0f;
	}

	/*!
	*	\fn InternalEnter
	*	\brief Called by the state machine when switchin to this state
	*/
	void State::InternalEnter()
	{
		//Sets mTimeInState to 0
		mTimeInState = 0.0f;

		//Calls LogicEnter()
		LogicEnter();
	}

	/*!
	*	\fn InternalExit
	*	\brief Called by the state machine when switchin out of this state.
	*/
	void State::InternalExit()
	{
		//Calls LogicExit()
		LogicExit();
	}

	/*!
	*	\fn InternalUpdate
	*	\brief Called by the state machine while it's in this state
	*/
	void State::InternalUpdate()
	{
		//Increments mTimeInState by gAEFrameTime
		mTimeInState += (f32)aexTime->GetFrameTime();

		//Calls LogicUpdate()
		LogicUpdate();
	}

	// ----------------------------------------------------------------------------
	// SUPER STATE (STATE AS A STATE MACHINE)
	// ----------------------------------------------------------------------------

	/*!
	*	\fn SuperState
	*	\brief Non default constructor SuperState
	*	\param name - Pointer to a state name
	*	\param actor - Pointer to an actor
	*/
	SuperState::SuperState(const char * name, Actor * actor) : State(name)
	{
		//Checks if the given pointer is nullptr
		if (actor == nullptr)
			return;

		//Sets mInternalStateMachine mActor with the given actor pointer
		mInternalStateMachine.mActor = actor;

		//Sets mActor with the given actor pointer
		mActor = actor;
	}

	/*!
	*	\fn InternalEnter
	*	\brief Resets the internal state machine and calls the base InternalEnter
	*/
	void SuperState::InternalEnter()
	{
		//Goes through the internal machine states
		for (it = mInternalStateMachine.mStates.begin(); it != mInternalStateMachine.mStates.end(); it++)
		{
			//Sets the mActor to the current state actor
			(*it)->mActor = mActor;
		}

		//Resets the internal machine state
		mInternalStateMachine.Reset();

		//Calls the state InternalEnter()
		State::InternalEnter();
	}

	/*!
	*	\fn InternalUpdate
	*	\brief Updates the internal state machine,  then calls State::InternalUpdate()
	*/
	void SuperState::InternalUpdate()
	{
		//Calls the update of the Internal state machine
		mInternalStateMachine.Update();

		//Calls state InternalUpdate
		State::InternalUpdate();
	}
}