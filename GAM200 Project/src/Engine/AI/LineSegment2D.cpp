/*!
*	\file		LineSegment2D.cpp
*	\brief		Implementation of the functions for the LineSegment2D structure.
*	\details	Contains the definition of the functions declared in
*				LineSegment2D.h for the part 2 of assignment 4 of CS230.
*
*	\author		Miguel Echeverria - miguel.echeverria@digipen.edu
*	\date		04/04/2019
*	\copyright	Copyright DigiPen Institute Of Technology. All Rights Reserved
*
*/


#include "LineSegment2D.h"

namespace AEX
{

	// @TODO
	/*!
	\brief		Constructs a dummy line segment.
	\details	Sets all values to zero
	*/
	LineSegment2D::LineSegment2D()
	{
		// Default constructor of LineSegment2D, so sets
		// all the data to the default values

		mP0 = AEVec2(0, 0);	// Sets point 0 to (0, 0)
		mP1 = AEVec2(0, 0);	// Sets point 1 to (1, 1)
		mN = AEVec2(0, 0);		// Sets the outward normal to (0, 0)
		mNdotP0 = 0.0f;			// Sets the dot product between the normal and P0 to 0
	}

	// @TODO
	/*! ---------------------------------------------------------------------------

	\brief	Constructs a 2D line segment's data using 2 points

	\details:
	- Edge is defined as E = P1-P0
	- mN is the outward normal and is defined as mN = -E.Perp().Normalize();
	- mNdotP0 = the dot product of the normal with p0.

	---------------------------------------------------------------------------*/
	LineSegment2D::LineSegment2D(const AEVec2 & p0, const AEVec2 &p1)
	{
		// Non-default constructor of the LineSegment2D

		mP0 = p0;						// Sets point 0 to p0
		mP1 = p1;						// Sets point 1 to p1
		AEVec2 edge = p1 - p0;			// Vector that goes from point 0 to point 1
		mN = -edge.Perp().Normalize();	// Sets the outward normal to the corresponding vector
		mNdotP0 = mN * p0;				// Sets the dot product between the normal and point 0
	}


	// @TODO
	/*!
	 \brief	This function determines the distance separating a point from a line

	 \return	The distance. Note that the returned value should be:
				- Negative if the point is in the line's inside half plane
				- Positive if the point is in the line's outside half plane
				- Zero if the point is on the line
	*/
	float StaticPointToStaticLineSegment(AEVec2 *P, LineSegment2D *LS)
	{
		AEVec2 vec = *P - LS->mP0;			// Vector that goes from point 0 of the line, to P
		float projLength = vec * LS->mN;	// Length of the projection of vec onto the normal of the line

		return projLength;					// Return the length of the projection (the distance)
	}

	// @TODO
	/*!
	 \brief	Given a point P and an array of LineSegment2D, determines if a point
				is contained by all line segments (ie. inside half plane of all segments).

	 \return	true if the point is inside all line segments, false otherwise.
	*/
	bool PointInLineSegments(AEVec2 *P, LineSegment2D *LS, u32 count)
	{
		// Loop that goes through the array of line segments
		for (u32 i = 0; i < count; i++)
		{
			// If the point is in the current line's outside half plane, return false
			if (StaticPointToStaticLineSegment(P, LS + i) > 0)
				return false;
		}

		return true;	// Else, return true
	}
}