#pragma once

#include "LineSegment2D.h"
#include <aexmath\aexmath.h>
#include <algorithm>


namespace AEX 
{
	/*!
	 \struct Ray
	 \brief	 Represents a 2-dimensional ray with an origina and direction.
	 \remark Direction is not assumed to be normalized.
	*/
	struct Ray
	{
		Ray(AEVec2 origin = AEVec2(0.0f, 0.0f), AEVec2 direction = AEVec2(1.0f, 0.0f));
		Ray(f32 originX = 0.0f, f32 originY = 0.0f, f32 dirX = 1.0f, f32 dirY = 0.0f);

		// Computes the distance (not squared) from the origin of the ray to a point specified.
		// Usually this will be used to compute the distance from the origin to the
		// point where the ray intersected with a certain object.
		f32 DistFromOrigin(const AEVec2 & intersection);

		// Computes the distance squared from the origin of the ray to a point specified.
		// Usually this will be used to compute the distance from the origin to the
		// point where the ray intersected with a certain object.
		f32 DistFromOriginSquared(const AEVec2 & intersection);

		AEVec2 mOrigin;
		AEVec2 mDirection;
	};

	// @TODO
	//! Performs a raycast test against a line.
	f32 RayCastLine(const AEVec2 & origin, const AEVec2 & dir, const LineSegment2D & line, AEVec2 * outPi);
	// @TODO
	//! Performs a raycast test against an obb.
	f32 RayCastRect(const AEVec2 & origin, const AEVec2 & dir, const Transform & rect, AEVec2 * outPi);
	// @TODO
	//! Performs a raycast test against a circle.
	f32 RayCastCircle(const AEVec2 & origin, const AEVec2 & dir, const AEVec2 & circle, f32 radius, AEVec2 * outPi);

	// @PROVIDED
	//! Performs a raycast test against a line.
	f32 RayCastLine(const Ray & ray, const LineSegment2D & line, AEVec2 * outPi);
	// @PROVIDED
	//! Performs a raycast test against an obb.
	f32 RayCastRect(const Ray & ray, const Transform &rect, AEVec2 * outPi);
	// @PROVIDED
	//! Performs a raycast test against a circle.
	f32 RayCastCircle(const Ray & ray, const AEVec2 & circle, f32 radius, AEVec2 * outPi);
}