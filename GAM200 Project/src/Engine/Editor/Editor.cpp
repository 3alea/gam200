#include "src\Engine\AEX.h"
#include <experimental/filesystem>
#include "src\Engine\Imgui\imgui.h"
#include "src\Engine\Components\AEXComponents.h"
#include "src\Engine\Composition\AEXScene.h"
#include "src\Engine\Graphics\WindowMgr.h"
#include "src\Engine\Composition\AEXFactory.h"
#include "src\Engine\Core\AEXGlobalVariables.h"
#include "Editor.h"
#include "src\Engine\Core\AEXSystem.h"
#include "src/Engine/Composition/AEXGameObject.h"
#include "Physics/CollisionGroups.h"
#include "AI\Path2D.h"
#include "Enemy/EnemyBugger.h"

using namespace AEX;
namespace fs = std::experimental::filesystem;

// HELPER FUNCTIONS DECLARATIONS ---------------------------------------------

static bool			ImGui_ShowcaseHelpWindow = false;
static bool			renderDebugLines = true;
static int counter;

static int miguelDebugCounter = 0;

f32 LoadProgressTimer = 0.0f;

// ---------------------------------------------------------------------------------------------------------

// EDITOR STATE -------------------------------------------------------------------------------------------
bool EditorState::Initialize()
{
	mIsPlaying = false;
	mEditorCameraPos = AEVec2();
	mEditorCameraViewRectSize = 5.0f;
	return true; // 1
}
 // -------------------------------------------------------------------------------------------------------

// MAIN EDITOR -----------------------------------------------------------------------------------------------
void MainEditor::MainLogic()
{
	///////////////Debug for collision groups/////////////////////////////
	if (aexInput->KeyTriggered(GLFW_KEY_DOWN))
	{
		collision_table->printCollisionGroups();
		aexCollisionSystem->PrintColliders();
	}
	if (aexInput->KeyTriggered(GLFW_KEY_UP))
		collision_table->printCollisionTable();
	///////////////Debug for collision groups/////////////////////////////
	if (aexInput->KeyPressed(GLFW_KEY_LEFT_CONTROL) && aexInput->KeyTriggered(GLFW_KEY_F)) //set to fullscreen
	{
		WindowMgr->GetCurrentWindow()->SetFullScreen(!WindowMgr->GetCurrentWindow()->GetFullScreen());
		if (!WindowMgr->GetCurrentWindow()->GetFullScreen())
			WindowMgr->GetCurrentWindow()->SetResolution(Resolution::eRes_1280x720);
	}

	mgizmos->LeftMouseReleased = false;

	if (aexInput->MousePressed(Input::eMouseLeft))
	{
		mgizmos->Leftmousepressed = true;

	}
	else
	{
		if (mgizmos->Leftmousepressed) // if released
		{
			mgizmos->LeftMouseReleased = true;
			mgizmos->onTransform = false;
		}

		mgizmos->Leftmousepressed = false;
	}

	mgizmos->multipleselection->MultipleSelectionLogic();
	

	if (aexInput->MousePressed(Input::eMouseRight))
	{
		mgizmos->Rightmousepressed = true;

	}
	else
	{
		if (mgizmos->Rightmousepressed) // if released
		{
			mgizmos->RightMouseReleased = true;
		}

		mgizmos->Rightmousepressed = false;
	}


	ReadInput();

	ShowTopMenu();
	ShowObjectList();
	ShowInspector();
	
	ShowUtilities();

	ShowPlentyObjects();

	if (Editor->imGui_selectedGO)
		ShowObjectInfo();

	mgizmos->CheckObjects();

	if(Editor->imGui_selectedGO)
		mgizmos->ComputeGizmos();

	mgizmos->CheckInputs();
	
	if (Editor->imGui_selectedGO)
	{
		if (Editor->imGui_selectedGO->GetComp<Enemy>())
		{
			Editor->imGui_selectedGO->GetComp<Enemy>()->EditorUpdate();
		}
	}
}


// Function to read input for editor shortcuts----------------------------------------------------------------
void MainEditor::ReadInput()
{
	// Ctrl + S (Save)
	if (aexInput->KeyPressed(Input::Keys::eControl) && aexInput->KeyTriggered(GLFW_KEY_S)
		&& aexInput->KeyPressed(Input::Keys::eAlt) == false)
		Ctrl_S_Activated = true;
	
	// Ctrl + Alt + S (Save as)
	if (aexInput->KeyPressed(Input::Keys::eControl) && aexInput->KeyPressed(Input::Keys::eAlt)
		&& aexInput->KeyTriggered(GLFW_KEY_S))
		Ctrl_Alt_S_Activated = true;

	// Ctrl pressed (for multiple selection of things)
	if (aexInput->KeyPressed(Input::Keys::eControl))
		Ctrl_Pressed = true;
	else
		Ctrl_Pressed = false;

	// Hold 4 (Transform vertex snapping mode)
	if (aexInput->KeyPressed(GLFW_KEY_4))
	{
		SnappingEnabled = true;
		mgizmos->showgizmos = false;
	}
	else
		SnappingEnabled = false;
}


/* SHOW FUNCTIONS IMPLEMENTATION ----------------------------------------------------------------------------*/
void MainEditor::ShowTopMenu()
{
	if (ImGui_ShowcaseHelpWindow)
		ImGui::ShowDemoWindow();

	if (ImGui::BeginMainMenuBar()) // MainMenu (top-horizontal bar)
	{
		if (ImGui::BeginMenu("File")) // MainMenu block
		{
			if (ImGui::BeginMenu("New", "Ctrl + N")) // MainMenu block Item
			{
				if (ImGui::BeginMenu("GameObject")) // display spaces list
				{
					auto it = aexScene->mSpaces.begin();
					while (it != aexScene->mSpaces.end())
					{
						if (ImGui::MenuItem((*it)->GetBaseName())) // get spaces names
						{
							the_space = *it;
							addGObyname = true;
						}
						it++;
					}

					ImGui::EndMenu();
				}
				if (ImGui::MenuItem("Space"))
				{
					addspacebyname = true;
				}
				if (ImGui::MenuItem("Scene"))
				{
					mAddNewScene = true;
				}
				if (ImGui::MenuItem("Collision Group"))
				{
					mAddNewCG = true;
				}


				ImGui::EndMenu();
			}



			if (ImGui::BeginMenu("Open Level"))
			{
				// Miguel
				try {
					fs::path path("data/Levels");

					for (fs::recursive_directory_iterator dirIt(path);
						dirIt != fs::recursive_directory_iterator();
						dirIt++)
					{
						if (fs::is_directory(dirIt->path()))
							continue;

						if (ImGui::MenuItem(dirIt->path().filename().string().c_str()))
						{
							relPathToLoad = dirIt->path().relative_path().string();
							loadFromJson = true;
						}
					}

					ImGui::EndMenu();
				} CATCH_FS_ERROR
			}

			if (ImGui::MenuItem("Save", "Ctrl + S"))
			{
				SaveExistingLevel();
			}

			if (ImGui::MenuItem("Save as...", "Ctrl + Alt + S"))
			{
				saveToJson = true;
			}

			ImGui::EndMenu();
		}
		if (ImGui::BeginMenu("Search"))
		{
			if (ImGui::MenuItem("Search by name"))
			{
				search_by_name = true;
			}
			if (ImGui::MenuItem("Search by tag"))
			{
				search_by_tag = true;
			}


			ImGui::EndMenu();
		}


		if (ImGui::BeginMenu("Edit")) ImGui::EndMenu();
		if (ImGui::BeginMenu("View")) ImGui::EndMenu();

		if (ImGui::BeginMenu("Options"))
		{
			if (ImGui::BeginMenu("Resolution")) // MainMenu block Item
			{
				int selected = WindowMgr->GetCurrentWindow()->GetResolution();
				for (int n = 0; n < 3; n++)
				{
					char buf[32];

					switch (n)
					{
					case eRes_1920x1080:
						sprintf_s(buf, "1920 x 1080");
						break;
					case eRes_1600x900:
						sprintf_s(buf, "1600 x 900");
						break;
					case eRes_1280x720:
						sprintf_s(buf, "1280 x 720");
						break;
					}

					if (ImGui::Selectable(buf, selected == n))
						WindowMgr->GetCurrentWindow()->SetResolution(static_cast<Resolution>(n));

				}
				ImGui::EndMenu();
			}

			bool isFullscreen = WindowMgr->GetCurrentWindow()->GetFullScreen();
			ImGui::Checkbox("Fullscreen", &isFullscreen);
			if (isFullscreen != WindowMgr->GetCurrentWindow()->GetFullScreen())
				WindowMgr->GetCurrentWindow()->SetFullScreen(isFullscreen);
			
			ImGui::Checkbox("Render debug lines", &renderDebugLines);

			ImGui::Checkbox("Wireframe mode", &GfxMgr->mbWireframeMode);

			ImGui::SliderFloat("Gizmo size", &Editorstate->mGizmoSize, 0.1f, 5.0f);

			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("Help"))
		{
			ImGui_ShowcaseHelpWindow = !ImGui_ShowcaseHelpWindow;
			ImGui::EndMenu();
		}

		if (ImGui::Button("Play", ImVec2(120, 0)))
		{
			//SavetoTemp();
			Editorstate->SetPlayingState(true);
		}

		ImGui::SameLine(700.0f);
		ImGui::Text("Scene: %s", aexScene->GetBaseName());
	}
	ImGui::EndMainMenuBar();

	
	// Miguel
	static bool showResourcesLoaded = false;
	ImGui::Checkbox("Show loaded resources", &showResourcesLoaded);
	if (showResourcesLoaded)
		aexResourceMgr->OnGui();


	if (mAddNewScene)
	{
		NewSceneDialog();
	}

	if (mAddNewCG)
	{
		NewCGDialog();
	}

	if (Ctrl_S_Activated)
	{
		SaveExistingLevel();
	}

	if (Ctrl_Alt_S_Activated)
	{
		saveToJson = true;
	}

	if (search_by_name) // logic for adding a gameobject setting the name
	{
		Search_by_Name();
	}

	if (search_by_tag) // logic for adding a gameobject setting the tag
	{
		Search_by_Tag();
	}

	if (addGObyname) // logic for adding a gameobject setting the name
	{
		AddGameObjectbyName();
	}

	if (addspacebyname) // logic for adding a space setting the name
	{
		AddSpacebyName();
	}

	if (loadFromJson)
	{
		LoadfromJson();
	}

	if (saveToJson)
	{
		SavetoJson();
	}

	if (loadFromTemp)
	{
		// std::cout << "loading..." << std::endl;
		//LoadfromTemp();
	}

	if (mbShowProgressBar)
	{
		ProgressBar(mParamInfo.message, mParamInfo.speed_factor, mParamInfo.use_x_button, mParamInfo.bar_limit);
	}

	if (mbAddSceneAfterSaving)
	{
		Scene::ReleaseInstance();
		imGui_selectedGO = NULL;
		json value;
		LoadFromFile(value, "data\\Levels\\defaultlevel.json");
		aexScene->FromJson(value);
		mbAddSceneAfterSaving = false;
	}
}

void MainEditor::ShowObjectList()
{
	static int selected = -1;
	int n = 0;
	if (ImGui::Begin("Space List"))
	{
		FOR_EACH(it, aexScene->mSpaces) {
			auto& space = *it;
			if (ImGui::CollapsingHeader(space->GetBaseName()))
			{
				for (auto mapIt = space->mallObjects.begin(); mapIt != space->mallObjects.end(); mapIt++)
				{
					for (u32 i = 0, count = 0; i < mapIt->second.size(); i++, count++)
					{
						if (mapIt->second[i] == nullptr)
							continue;

						char buf[64];
						sprintf_s(buf, mapIt->second[i]->GetBaseName(), n);
						std::string name = buf;
						if (ImGui::Selectable((name + "##" + std::to_string(count)).c_str(), selected == n))
						{
							imGui_selectedGO = mapIt->second[i];
							selected = n;
						}
					}
				}

			}
		}
	}
	ImGui::End();
}

void MainEditor::ShowInspector()
{
	//if (ImGui::Begin("Object Inspector"))
	//{
		//if (imGui_selectedGO)
		//{
		//	bool changed = ShowObjectInfo();
		//	//bool changed = imGui_selectedGO->OnGui();
		//	if (changed) //&& selected_object_has_changed == false) {
		//	{//selected_object_has_changed = true;
		//		AddJsonToUndoStack();
		//		changesmade = true;
		//	}
		//}
	//}
	//
	//ImGui::End();
}

void MainEditor::ShowArchetypeList()
{
	if (ImGui::CollapsingHeader("Archetypes"))
	{
		ImGuiIO & io = ImGui::GetIO();
		static std::string fileName;

		if (ImGui::TreeNode("Select space to add next archetypes to"))
		{
			static int selected = -1;
			u32 n = 0;
			static bool setDefault = true;

			if (setDefault)
			{
				ImGui::Selectable("Main", true);
				//archetypes_selectedSpace = aexScene->FindSpace("Main");
			}

			FOR_EACH(spaceIt, aexScene->mSpaces)
			{
				if (n >= aexScene->mSpaces.size())
					break;

				if (n == 0 && setDefault)
				{
					n++;
					continue;
				}

				if (ImGui::Selectable((*spaceIt)->GetBaseName(), selected == n))
				{
					archetypes_selectedSpace = aexScene->FindSpace((*spaceIt)->GetBaseName());
					selected = n;
					setDefault = false;
				}

				n++;
			}
			ImGui::TreePop();
		}

		ImGui::Separator();

		try {
			fs::path archFolder("data\\Archetypes");
			fs::recursive_directory_iterator endDirIt;

			for (fs::recursive_directory_iterator dirIt(archFolder); dirIt != endDirIt; dirIt++)
			{
				fileName = aexResourceMgr->GetNameNoExtension(dirIt->path().string());
				ImGui::Button(fileName.c_str());

				if (ImGui::IsItemActive())
					ImGui::GetForegroundDrawList()->AddLine(io.MouseClickedPos[0], io.MousePos, ImGui::GetColorU32(ImGuiCol_Button), 2.0f);

				if (ImGui::IsItemDeactivated())
				{
					AEVec2 mousePos = aexInput->GetGLFWMousePos();
					mousePos = Editorstate->mEditorCamera->PointToWorld(mousePos);
					json value;
					LoadFromFile(value, dirIt->path().string());

					//GameObject::objectID++;
					if (GameObject * obj = dynamic_cast<GameObject *>(aexFactory->Create(value["RTTI Type"].get<std::string>().c_str())))
					{
						//__debugbreak();
						if (archetypes_selectedSpace != nullptr)
						{
							obj->FromJson(value);
							archetypes_selectedSpace->AddObject(obj);
							TransformComp * tr = obj->GetComp<TransformComp>();
							tr->SetPosition(mousePos);
							if (PathComponent * path = obj->GetComp<PathComponent>())
							{
								if (path->mRespectToObject)
								{
									tr->translationChanged = true;
									tr->transDisplacement = AEVec2(tr->mLocal.mTranslationZ.x, tr->mLocal.mTranslationZ.y);
								}
							}
							imGui_selectedGO = obj;
							//std::cout << "Archetype instantiated" << std::endl;
						}
						else
						{
							// TODO: Avoid memory leak
						}
					}
					else ASSERT(0, NULL); // HARD CRASH
				}
			}
		} CATCH_FS_ERROR
	}
}

void MainEditor::ShowCollisionTable()
{
	collision_table->printCollisionTable();
}

void MainEditor::ShowUtilities()
{
	if (ImGui::Begin("Utilities"))
	{
		ShowArchetypeList();
		ShowCollisionTable();
	}
	ImGui::End();
}

void MainEditor::ProgressBar(const char * message, f32 speedFactor, bool useXButton, f32 barLimit, bool shouldResetTimer)
{
	static float timer = 0.0f;

	if (useXButton == false)
	{
		ImGui::Begin(message);
		ImGui::ProgressBar(timer);
		timer += (f32)aexTime->GetFrameTime() * speedFactor;
		ImGui::End();
	}
	else
	{
		
		static bool open = true;
		if (open)
		{
			ImGui::Begin(message, &open);
			ImGui::ProgressBar(timer);
			timer += (f32)aexTime->GetFrameTime() * speedFactor;
			ImGui::End();
		}
	}

	if (timer > barLimit)
	{
		mbShowProgressBar = false;

		if (shouldResetTimer)
			timer = 0.0f;
	}
}

void MainEditor::ProgressBarLocked(const char * message, f32 speedFactor, bool useXButton, f32 barLimit, bool shouldResetTimer)
{
	static float timer = 0.0f;

	while (timer <= 15.0f)
	{
		if (useXButton == false)
		{
			ImGui::Begin(message);
			ImGui::ProgressBar(timer);
			timer += (f32)aexTime->GetFrameTime() * speedFactor;
			ImGui::End();
		}
		else
		{

			static bool open = true;
			if (open)
			{
				ImGui::Begin(message, &open);
				ImGui::ProgressBar(timer);
				timer += (f32)aexTime->GetFrameTime() * speedFactor;
				ImGui::End();
			}
		}

		//std::cout << miguelDebugCounter << std::endl;
		miguelDebugCounter++;

		if (timer > barLimit)
		{
			//if (shouldResetTimer)
			//	timer = 0.0f;
			//mbShowProgressBar = false;
			//break;
		}
	}

	if (shouldResetTimer)
		timer = 0.0f;
	mbShowProgressBar = false;
}





void MainEditor::ShowPlentyObjects()
{
	bool ManyObjects = false;

	FOR_EACH(it, ListimGui_selectedGO)
	{
		if (!imGui_selectedGO)
			continue;
		if ((*it)->GetBaseID() != imGui_selectedGO->GetBaseID()) // check if the slected game object is no longer in the collapsing list
			ManyObjects = true;
		
		TransformComp* tran1 = (*it)->GetComp<TransformComp>();
		
		AEVec2 pos1 = AEVec2(tran1->GetPosition3D().x, tran1->GetPosition3D().y);

		if (!StaticPointToOrientedRect(&mgizmos->mouseposRB, &pos1, tran1->GetScale().x, tran1->GetScale().y, tran1->GetRotationAngle()))
		{
			ListimGui_selectedGO.erase(it);
			break;
		}
	}

	if (ListimGui_selectedGO.size() == 1)
	{
		ListimGui_selectedGO.clear();
		return;
	}

	if (!ManyObjects)
	{
		ListimGui_selectedGO.clear();
		return;
	}

	static int selected = -1;
	int n = 0;
	if (ImGui::Begin("Selected objects list"))
	{
		FOR_EACH(it, ListimGui_selectedGO)
		{
			char buf[128];
			sprintf_s(buf, (*it)->GetBaseName(), n);
			if (ImGui::Selectable(buf, selected == n))
			{
				imGui_selectedGO = *it;
				selected = n;
				break;
			}
			
		}
	}
	
	ImGui::End();
	
}

/* Show function implementation ENDS ----------------------------------------------------------------------------*/

/* SELECTED GAME OBJECT FUNCTIONS -----------------------------------------------------------------------------*/
bool MainEditor::ShowObjectInfo()
{
	bool changed = false;

	if (imGui_selectedGO == NULL) //sanitty check
		return changed;
	imGui_selectedGO->SelectableLogic();
	if (ImGui::Begin("Object Info"))
	{
		if (ImGui::TreeNode(imGui_selectedGO->GetBaseName()))
		{
			if (ImGui::CollapsingHeader("Components: "))
			{
				changed = imGui_selectedGO->OnGui();
			}
			
			ImGui::Separator();
			if (ImGui::CollapsingHeader("Childrens: "))
			{
				ShowChildren();
			}
			ImGui::Separator();
			if (ImGui::CollapsingHeader("Tags: "))
			{
				ShowTags();
				if (ImGui::Button("Add tag"))
					settag = true;
			}
			ImGui::Separator();
			ImGui::Separator();
			if (ImGui::Button("Create children"))
			{
				createchildren = true;
			}
			ImGui::Separator();
			if (ImGui::Button("Change Object Name"))
			{
				changename = true;
			}
			ImGui::Separator();
			if (ImGui::Button("CopyObject"))
			{
				CopyObject();
			}
			ImGui::Separator();

			// Miguel
			static int mouseInput = 0;
			//static int count = 0;
			static std::string path;
			static std::string saveorupdate;

			ImGui::Button("Save To Archetype");

			if (imGui_selectedGO->mSavedToArchetype)
			{
				//mouseInput = 1;
				saveorupdate = "Update";
			}
			else
			{
				//mouseInput = 0;
				saveorupdate = "Save";
			}

			if (ImGui::BeginPopupContextItem("type archetype name", mouseInput))
			{
				ImGui::Text("Name of archetype:");

				ImGui::InputText("##edit", imGui_selectedGO->mEditorArchetypeName, IM_ARRAYSIZE(imGui_selectedGO->mEditorArchetypeName));

				if (ImGui::Button(saveorupdate.c_str()))
				{
					path = imGui_selectedGO->mEditorArchetypeName;
					path += ".json";
					imGui_selectedGO->SaveToArchetype(path.c_str());
				}

				ImGui::SameLine();

				if (ImGui::Button("Close"))
					ImGui::CloseCurrentPopup();

				ImGui::EndPopup();
			}
			ImGui::Separator();


			if (ImGui::Button("Delete Game Object"))
			{
				DeleteGameObject();
				ImGui::TreePop();
				ImGui::End();
				return true;
			}
			ImGui::Separator();
			ImGui::Checkbox("Selectable", &imGui_selectedGO->selectable);
		  ImGui::TreePop();
		}
	}

	ImGui::End();

	if (changename)
	{
		if (!changed)
			changed = ChangeObjectName();
		else
			ChangeObjectName();

	}
	if (settag)
	{
		ImGui::OpenPopup("Enter the tag");

		if (ImGui::BeginPopupModal("Enter the tag", NULL, ImGuiWindowFlags_AlwaysAutoResize))
		{
			static char strName[128] = "";
			ImGui::InputTextWithHint("Enter tag", "enter the tag", strName, IM_ARRAYSIZE(strName));

			ImGui::Separator();
			if (ImGui::Button("OK", ImVec2(120, 0)))
			{
				imGui_selectedGO->SetTag(strName);
				settag = false;
			}

			ImGui::SetItemDefaultFocus();
			ImGui::SameLine();
			if (ImGui::Button("Cancel", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); settag = false; }
			ImGui::EndPopup();
		}
	}
	if (createchildren)
	{
		if (!changed)
			changed = CreateChildren();
		else
			CreateChildren();
	}

	return changed;
}

void MainEditor::ShowChildren()
{
	static int selected = -1;
	int n = 0;

	for (auto it = imGui_selectedGO->GetAllChildren().begin(); it != imGui_selectedGO->GetAllChildren().end(); it++)
	{
		for (unsigned int i = 0; i < imGui_selectedGO->GetAllChildren().size(); i++)
		{
			char buf[32];
			sprintf_s(buf, imGui_selectedGO->GetAllChildren()[i]->GetBaseName(), n);
			if (ImGui::Selectable(buf, selected == n))
			{
				imGui_selectedGO = imGui_selectedGO->GetAllChildren()[i];
				selected = n;
				return;
			}
		}
	}
}

void MainEditor::ShowTags()
{
	std::vector<std::string> mtags = imGui_selectedGO->GetTags();

	FOR_EACH(it, mtags)
	{
		char charp[20]; // un poco txapuza
		strcpy_s(charp, (*it).c_str());
		if (ImGui::TreeNode(charp))
		{
			static int selected = -1;
			int n = 0;
			std::vector<GameObject*> go;
			imGui_selectedGO->GetParentSpace()->FindAllObjectsbyTag((*it), go);
			FOR_EACH(obit, go)
			{
				if (ImGui::MenuItem((*obit)->GetBaseName()))
				{
					imGui_selectedGO = *obit;
					selected = n;
				}
			}

			ImGui::TreePop();
		}
		//if(rightclick)    TO BE IMPLEMENTYED
		//	RemoveTag((*it));
		//	break;
	}
}


// ->  Modify Selected GameObject
bool MainEditor::CreateChildren()
{
	bool changed = false;
	ImGui::OpenPopup("Enter the name");

	if (ImGui::BeginPopupModal("Enter the name", NULL, ImGuiWindowFlags_AlwaysAutoResize))
	{
		static char strName[128] = "";
		ImGui::InputTextWithHint("Enter name", "enter game object name here", strName, IM_ARRAYSIZE(strName));

		ImGui::Separator();
		if (ImGui::Button("OK", ImVec2(120, 0)))
		{
			imGui_selectedGO->AddChild(strName);
			changed = true;
			createchildren = false;
		}

		ImGui::SetItemDefaultFocus();
		ImGui::SameLine();
		if (ImGui::Button("Cancel", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); createchildren = false; }
		ImGui::EndPopup();
	}
	return changed;
}

void MainEditor::DeleteGameObject()
{
	imGui_selectedGO->GetParentSpace()->DestroyObject(imGui_selectedGO);
	imGui_selectedGO = NULL;
}

void MainEditor::CopyObject()
{
	if(imGui_selectedGO)
		imGui_selectedGO->clone();
}

bool MainEditor::ChangeObjectName()
{
	bool changed = false;
	ImGui::OpenPopup("Enter the new name");

	if (ImGui::BeginPopupModal("Enter the new name", NULL, ImGuiWindowFlags_AlwaysAutoResize))
	{
		static char strName[128] = "";
		ImGui::InputTextWithHint("Enter name", "enter game object new  name here", strName, IM_ARRAYSIZE(strName));

		ImGui::Separator();
		if (ImGui::Button("OK", ImVec2(120, 0)))
		{
			imGui_selectedGO->SetBaseName(strName);
			changed = true;
			changename = false;
		}

		ImGui::SetItemDefaultFocus();
		ImGui::SameLine();
		if (ImGui::Button("Cancel", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); changename = false; }
		ImGui::EndPopup();
	}
	
	return changed;
}
/* Selected Game Object functions ENDS --------------------------------------------------------------------------*/

/* SEARCH FUNCTIONS IMPLEMENTATION ----------------------------------------------------------------------------*/
void MainEditor::Search_by_Name()
{
	ImGui::OpenPopup("Enter the name");

	if (ImGui::BeginPopupModal("Enter the name", NULL, ImGuiWindowFlags_AlwaysAutoResize))
	{
		static char strName[128] = "";
		ImGui::InputTextWithHint("Enter name", "enter game object name here", strName, IM_ARRAYSIZE(strName));

		ImGui::Separator();
		if (ImGui::Button("OK", ImVec2(120, 0)))
		{
			imGui_selectedGO = aexScene->FindObjectInANYSpace(strName); // should create another menu because more than one object may have the same name
			search_by_name = false;
		}

		ImGui::SetItemDefaultFocus();
		ImGui::SameLine();
		if (ImGui::Button("Cancel", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); search_by_name = false; }
		ImGui::EndPopup();
	}
}

void MainEditor::Search_by_Tag()
{
	ImGui::OpenPopup("Enter the tag");

	if (ImGui::BeginPopupModal("Enter the tag", NULL, ImGuiWindowFlags_AlwaysAutoResize))
	{
		static char strName[128] = "";
		ImGui::InputTextWithHint("Enter tag", "enter game object name here", strName, IM_ARRAYSIZE(strName));

		ImGui::Separator();
		if (ImGui::Button("OK", ImVec2(120, 0)))
		{
			imGui_selectedGO = aexScene->FindObjectInANYSpace(strName); // WRONG, NOT IMPLEMENTED
			search_by_tag = false;
		}

		ImGui::SetItemDefaultFocus();
		ImGui::SameLine();
		if (ImGui::Button("Cancel", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); search_by_tag = false; }
		ImGui::EndPopup();
	}
}
/* Search function implementation ENDS ----------------------------------------------------------------------------*/

/* ADD FUNCTIONS IMPLEMENTATION ----------------------------------------------------------------------------*/
void MainEditor::AddGameObjectbyName()
{
	ImGui::OpenPopup("Enter the name");

	if (ImGui::BeginPopupModal("Enter the name", NULL, ImGuiWindowFlags_AlwaysAutoResize))
	{
		static char strName[128] = "";
		ImGui::InputTextWithHint("Enter name", "enter game object name here", strName, IM_ARRAYSIZE(strName));

		ImGui::Separator();
		if (ImGui::Button("OK", ImVec2(120, 0)))
		{
			the_space->AddObject(strName);
			addGObyname = false;
		}

		ImGui::SetItemDefaultFocus();
		ImGui::SameLine();
		if (ImGui::Button("Cancel", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); addGObyname = false; }
		ImGui::EndPopup();
	}

}

void MainEditor::AddSpacebyName()
{
	ImGui::OpenPopup("Enter the name");

	if (ImGui::BeginPopupModal("Enter the name", NULL, ImGuiWindowFlags_AlwaysAutoResize))
	{
		static char strName[128] = "";
		ImGui::InputTextWithHint("Enter name", "enter space name here", strName, IM_ARRAYSIZE(strName));

		ImGui::Separator();
		if (ImGui::Button("OK", ImVec2(120, 0)))
		{
			aexScene->AddSpace(strName);
			addspacebyname = false;
		}

		ImGui::SetItemDefaultFocus();
		ImGui::SameLine();
		if (ImGui::Button("Cancel", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); addspacebyname = false; }
		ImGui::EndPopup();
	}
}

void MainEditor::NewSceneDialog()
{
	ImGui::OpenPopup("New Scene");

	if (ImGui::BeginPopupModal("New Scene", NULL, ImGuiWindowFlags_AlwaysAutoResize))
	{
		ImGui::Text("You are about to create a new scene.\nDo you want to save the current one?\nIf you don't, all unsaved changes will be lost.\n\n");

		//ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.8f, 0.835f, 0.45f, 0.4f));
		ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(0.0f, 0.6f, 0.6f));
		ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(0.0f, 0.8f, 0.8f));
		if (ImGui::Button("Save", ImVec2(100, 0)))
		{
			Ctrl_S_Activated = true;
			mbAddSceneAfterSaving = true;
			mAddNewScene = false;
			ImGui::CloseCurrentPopup();
		}
		ImGui::PopStyleColor(2);
		ImGui::SameLine(140.0f);
		if (ImGui::Button("Don't save"))
		{
			Scene::ReleaseInstance();
			imGui_selectedGO = NULL;
			json value;
			LoadFromFile(value, "data\\Levels\\defaultlevel.json");
			aexScene->FromJson(value);
			mAddNewScene = false;
			ImGui::CloseCurrentPopup();
		}
		ImGui::SameLine(260.0f);
		if (ImGui::Button("Cancel", ImVec2(100.0f, 0)))
		{
			mAddNewScene = false;
			ImGui::CloseCurrentPopup();
			//ImGui::EndPopup();
		}

		ImGui::EndPopup();
	}
}


void MainEditor::NewCGDialog()
{
	ImGui::OpenPopup("Enter the name");

	if (ImGui::BeginPopupModal("Enter the name", NULL, ImGuiWindowFlags_AlwaysAutoResize))
	{
		static char strName[128] = "";
		ImGui::InputTextWithHint("Enter name", "enter collision group name here", strName, IM_ARRAYSIZE(strName));

		ImGui::Separator();
		if (ImGui::Button("OK", ImVec2(120, 0)))
		{
			collision_table->AddCollisionGroup(strName);
			mAddNewCG = false;
		}

		ImGui::SetItemDefaultFocus();
		ImGui::SameLine();
		if (ImGui::Button("Cancel", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); mAddNewCG = false; }
		ImGui::EndPopup();
	}
}

/* SAVE/LOAD TO/FROM JSON FUNCTIONS IMPLEMENTATION --------------------------------------------------------*/
void MainEditor::SaveExistingLevel()
{
	if (aexScene->mbAlreadySaved == false)
		saveToJson = true;
	else
	{
		std::string extension = ".json";
		std::string filename = "data/Levels/";
		filename += aexScene->GetBaseName() + extension;
		json value;
		aexScene->mbAlreadySaved = true;
		aexScene->ToJson(value);
		SaveToFile(value, filename.c_str());
		mParamInfo.message = "Saving...";
		mParamInfo.speed_factor = 3.0f;
		mbShowProgressBar = true;
		Ctrl_S_Activated = false;
	}
}

void MainEditor::SavetoJson()
{
	ImGui::OpenPopup("Save to Json");

	if (ImGui::BeginPopupModal("Save to Json", NULL, ImGuiWindowFlags_AlwaysAutoResize))
	{
		ImGui::Text("Saving level to a json file.\nPlease enter the name of the file:");
		ImGui::Separator();
		static char strSave[128] = "";
		ImGui::InputTextWithHint("Set file name", "enter filename here", strSave, IM_ARRAYSIZE(strSave));

		ImGui::Separator();
		if (ImGui::Button("OK", ImVec2(120, 0)))
		{
			std::string extension = ".json";
			std::string filename = "data/Levels/";
			filename += strSave + extension;
			json value;
			aexScene->SetBaseName(strSave);
			aexScene->mbAlreadySaved = true;
			aexScene->ToJson(value);
			SaveToFile(value, filename.c_str());
			saveToJson = false;
			Ctrl_S_Activated = false;
			Ctrl_Alt_S_Activated = false;
			mParamInfo.message = "Saving as...";
			mParamInfo.speed_factor = 1.5f;
			mbShowProgressBar = true;
			ImGui::EndPopup();
			return;
		}

		ImGui::SetItemDefaultFocus();
		ImGui::SameLine();
		if (ImGui::Button("Cancel", ImVec2(120, 0)))
		{
			saveToJson = false;
			Ctrl_S_Activated = false;
			Ctrl_Alt_S_Activated = false;
			ImGui::CloseCurrentPopup();
		}
		ImGui::EndPopup();
	}
}

//void MainEditor::SavetoTemp()
//{
//	std::string file = "temp/temp.json";
//	json value;
//	aexScene->ToJson(value);
//	SaveToFile(value, file.c_str());
//}

void MainEditor::SaveToLevel(const float newX, const float newY)
{
	// Save the new player's spawn position in the editor
	playerNewPosition.x = newX;
	playerNewPosition.y = newY;
}

void MainEditor::LoadfromJson()
{
	// Doesn't work
	//Space * space = aexScene->FindSpace("Main");
	//GameObject * obj = nullptr;
	//if (space != nullptr)
	//	obj = space->AddObject("Loading");
	//if (obj != nullptr)
	//{
	//	obj->AddComp(new TransformComp);
	//	obj->AddComp(new SpriteComp);
	//	SpriteComp * sprite = obj->GetComp<SpriteComp>();
	//	if (sprite != nullptr)
	//		sprite->mTex = aexResourceMgr->getResource<Texture>("data\\Textures\\loadcircle.png");
	//}
	Scene::ReleaseInstance();
	imGui_selectedGO = NULL;
	//while (!undo_stack.empty())
	//	undo_stack.pop();
	//while (!redo_stack.empty())
	//	redo_stack.pop();
	json value;
	LoadFromFile(value, relPathToLoad.c_str());
	aexScene->FromJson(value);
	aexScene->SetBaseName(aexResourceMgr->GetNameNoExtension(relPathToLoad).c_str());
	loadFromJson = false;

	//aexLogic->Initialize();
	//aexPhysicsMgr->Initialize();
	//aexCollisionSystem->Initialize();
	//loadFromJson = false;



	// Previous implementation--------------------------------------------------------------------------
	//ImGui::OpenPopup("Load from Json");
	//
	//if (ImGui::BeginPopupModal("Load from Json", NULL, ImGuiWindowFlags_AlwaysAutoResize))
	//{
	//	ImGui::Text("Loading level from json file.\nPlease select the name of the file to load:");
	//	ImGui::Separator();
	//	static char strLoad[128] = "";
	//	ImGui::InputTextWithHint("Load file", "enter file name here", strLoad, IM_ARRAYSIZE(strLoad));
	//
	//	ImGui::Separator();
	//	if (ImGui::Button("OK", ImVec2(120, 0)))
	//	{
	//		//aexScene->DeleteSpace("Main");
	//		Scene::ReleaseInstance();
	//		std::string extension = ".json";
	//		std::string filename = strLoad + extension;
	//		json value;
	//		LoadFromFile(value, filename.c_str());
	//		aexScene->FromJson(value);
	//		loadFromJson = false;
	//	}
	//
	//	ImGui::SetItemDefaultFocus();
	//	ImGui::SameLine();
	//	if (ImGui::Button("Cancel", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); loadFromJson = false; }
	//	ImGui::EndPopup();
	//}
}

//void MainEditor::LoadfromTemp()
//{
//	Scene::ReleaseInstance();
//
//	imGui_selectedGO = NULL;
//
//	std::string file = "temp/temp.json";
//	json value;
//	LoadFromFile(value, file.c_str());
//	aexScene->mSpaces.clear();
//	aexScene->FromJson(value);
//	loadFromTemp = false;
//}

void MainEditor::LoadFromLevel()
{
	// Reload the level
	ReloadLevel();

	// Find the player and change its position to the one saved
	aexScene->FindObjectInANYSpace("player")->GetComp<TransformComp>()->mLocal.mTranslationZ.x = playerNewPosition.x;
	aexScene->FindObjectInANYSpace("player")->GetComp<TransformComp>()->mLocal.mTranslationZ.y = playerNewPosition.y;

	aexScene->FindObjectInANYSpace("player")->GetComp<Rigidbody>()->mIgnoreGravity = false;
	
	// Do the same for everything that follows it
	aexScene->FindObjectInANYSpace("foot")->GetComp<TransformComp>()->mLocal.mTranslationZ = playerNewPosition;
	aexScene->FindObjectInANYSpace("aim")->GetComp<TransformComp>()->mLocal.mTranslationZ = playerNewPosition;
	aexScene->FindObjectInANYSpace("camera")->GetComp<TransformComp>()->mLocal.mTranslationZ.x = playerNewPosition.x;

	if (aexScene->GetBaseNameString() == "Stomach")
		aexScene->FindObjectInANYSpace("camera")->GetComp<TransformComp>()->mLocal.mTranslationZ.y = playerNewPosition.y;

	aexScene->FindObjectInANYSpace("fadeout")->GetComp<TransformComp>()->mLocal.mTranslationZ.x = playerNewPosition.x;
	aexScene->FindObjectInANYSpace("fadeout")->GetComp<TransformComp>()->mLocal.mTranslationZ.y = playerNewPosition.y;
}

void MainEditor::LoadFromCertainLevel(const std::string& levelName)
{
	// Delete previous state
	Scene::ReleaseInstance();

	// Debug for multiple selection
	imGui_selectedGO = NULL;

	// Start off file path with...
	std::string path = "data/Levels/";
	// .json string
	std::string JASON = ".json";

	// Add the three strings to get the full path
	std::string file = (levelName.rfind(path, 0) == 0) ? levelName + JASON : path + levelName + JASON;

	json value;
	LoadFromFile(value, file.c_str());

	aexScene->Shutdown();
	aexCollisionSystem->Shutdown();
	aexScene->FromJson(value);

	// Clear the name stored
	levelToLoadName.clear();

	// Set loading to false
	loadFromCertainLevel = false;
}

void MainEditor::ReloadLevel()
{
	std::string name = aexScene->GetBaseNameString();
	LoadFromCertainLevel(name);
	//audiomgr->StopAll();
	//aexScene->LevelMusic = audiomgr->Loop(aexResourceMgr->getResource<Sound>("data\\Audio\\PlaytestingAudios\\ENTAILS.wav"), Voice::Music);
	//aexScene->LevelMusic->SetVolume(0.5f);
}

void MainEditor::LoadCheckpoint()
{
	// Simply load the saved level
	LoadFromLevel();
}

/* SAVE/LOAD to/from implementaion ENDS ------------------------------------------------------------------*/

/* EDITOR TOOLS FUNCTIONS IMPLEMENTATION -----------------------------------------------------------------*/
void MainEditor::AutoSave()
{
	json autosave;
	f32 current_time = timer->GetTimeSinceStart();
	if (current_time > 60) // 1 minute
	{
		//if (changesmade)
		//{
			aexScene->ToJson(autosave);
			SaveToFile(autosave, "data/History/Autosave.json"); // change second parameter!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			changesmade = false;
		//}

		timer->Reset();
	}
}

void MainEditor::AddJsonToUndoStack()
{
	if (imGui_selectedGO == NULL)
		return;
	Helper helper;
	helper.id = imGui_selectedGO->GetBaseID();
	helper.name = imGui_selectedGO->GetBaseName();

	imGui_selectedGO->ToJson(helper.jsonfile);

	undo_stack.push(helper);
}
void MainEditor::AddJsonToRedoStack()
{
	if (imGui_selectedGO == NULL)
		return;
	Helper helper;
	helper.id = imGui_selectedGO->GetBaseID();
	helper.name = imGui_selectedGO->GetBaseName();

	imGui_selectedGO->ToJson(helper.jsonfile);

	redo_stack.push(helper);
}

void MainEditor::Undo()
{
	if (undo_stack.size() == 0)
		return;
	int flag = 0;

	AddJsonToRedoStack();

	Helper helper  = undo_stack.top();

	std::vector<GameObject*> allgos;
	aexScene->FindAllObjectsInANYSpace(helper.name, allgos);
	FOR_EACH(it, allgos)
	{
		if ((*it)->GetBaseID() == helper.id)
		{
			imGui_selectedGO = *it;
			flag = 1;
		}
	}

	if(flag)
		imGui_selectedGO->FromJson(helper.jsonfile);

	undo_stack.pop();
}

void MainEditor::Redo()
{
	if (imGui_selectedGO == NULL)
		return;
	if (redo_stack.size() == 0)
		return;

	AddJsonToUndoStack();
	int flag = 0;
	Helper helper = redo_stack.top();

	std::vector<GameObject*> allgos;
	aexScene->FindAllObjectsInANYSpace(helper.name, allgos);
	FOR_EACH(it, allgos)
	{
		if ((*it)->GetBaseID() == helper.id)
		{
			imGui_selectedGO = *it;
			flag = 1;
		}
	}

	if (flag)
		imGui_selectedGO->FromJson(helper.jsonfile);

	redo_stack.pop();
}
/* EDITOR tools implementation ENDS -----------------------------------------------------------------*/

void Gizmos::CheckObjects()
{
	if (aexInput->KeyPressed(GLFW_KEY_LEFT_CONTROL))
		return;
	if (!ImGui::GetIO().WantCaptureMouse)
	{
		if (Rightmousepressed)
		{
			mouseposRB = Editorstate->mEditorCamera->PointToWorld(aexInput->GetGLFWMousePos());
			FOR_EACH(it, aexScene->mSpaces) // go onto all spaces
			{
				FOR_EACH(Sit, (*it)->mallObjects) // go onto the map
				{
					FOR_EACH(tit, Sit->second) // go onto the std vectos of go
					{
						TransformComp* objtransform = (*tit)->GetComp<TransformComp>();
						SpriteComp* objSprite = (*tit)->GetComp<SpriteComp>();
						AEVec2 texSize = objSprite ? AEVec2(static_cast<f32>(objSprite->mTex->get()->GetWidth()) / 100.0f, static_cast<f32>(objSprite->mTex->get()->GetHeight() / 100.0f)) : AEVec2(1.0f, 1.0f);

						if (objtransform == NULL) // sanity check, doesnt have a transform
							continue;

						AEVec3 vec3 = objtransform->GetPosition3D();

						if (StaticPointToOrientedRect(&mouseposRB, &AEVec2(vec3.x, vec3.y), objtransform->GetScale().x * texSize.x,
							objtransform->GetScale().y * texSize.y, objtransform->GetRotationAngle()) && (*tit)->selectable)
						{
							Editor->imGui_selectedGO = *tit;
							// if it is not in the std::vector yet
							if (std::find(Editor->ListimGui_selectedGO.begin(), Editor->ListimGui_selectedGO.end(), *tit) == Editor->ListimGui_selectedGO.end())
								Editor->ListimGui_selectedGO.push_back(*tit);
						}
					}
				}
			}
		}
		if (Leftmousepressed)
		{
			mouseposLB = Editorstate->mEditorCamera->PointToWorld(aexInput->GetGLFWMousePos());
		}
	}
}

void Gizmos::ComputeGizmos()
{
	const f32 viewRectangleSize = Editorstate->mEditorCamera->mViewRectangleSize;
	TransformComp* objtransform = Editor->imGui_selectedGO->GetComp<TransformComp>();

	SpriteComp* objSprite = Editor->imGui_selectedGO->GetComp<SpriteComp>();

	AEVec2 texSize = objSprite ? AEVec2(static_cast<f32>(objSprite->mTex->get()->GetWidth()) / 100.0f, static_cast<f32>(objSprite->mTex->get()->GetHeight() / 100.0f)) : AEVec2(1.0f, 1.0f);

	if (objtransform == NULL) // sanity check
		return;

	xAngle.FromAngle(objtransform->mWorld.mOrientation);
	xHalfExtent = AEVec2(objtransform->mWorld.mTranslationZ.x, objtransform->mWorld.mTranslationZ.y) + xAngle * objtransform->mWorld.mScale.x / 2.0f;
	//xHalfExtent = AEVec2(xHalfExtent.x * texSize.x, xHalfExtent.y * texSize.y);

	yAngle.FromAngle(objtransform->mWorld.mOrientation + DegToRad(90.0f));
	yHalfExtent = AEVec2(objtransform->mWorld.mTranslationZ.x, objtransform->mWorld.mTranslationZ.y) + yAngle * objtransform->mWorld.mScale.y / 2.0f;
	//yHalfExtent = AEVec2(yHalfExtent.x * texSize.x, yHalfExtent.y * texSize.y);

	AEMtx33 rotationmatrix, translationmatrix;
	AEVec2 vec2;
	rotationmatrix = AEMtx33::RotRad(objtransform->GetRotationAngle());
	translationmatrix = AEMtx33::Translate(objtransform->GetPosition3D().x, objtransform->GetPosition3D().y);
	rotationmatrix = translationmatrix * rotationmatrix;
	//COMPUTE OUT BOX
	OutBox.mScale = AEVec2(objtransform->GetScale().x * texSize.x, objtransform->GetScale().y * texSize.y);
	OutBox.mTranslationZ = objtransform->GetPosition3D();
	OutBox.mOrientation = objtransform->GetRotationAngle();
	// COMPUTE IN BOX
	InBox = OutBox;
	InBox.mScale = objtransform->GetScale() * texSize.x * viewRectangleSize / 12.0f;


	//TOPES
	if (InBox.mScale.x > OutBox.mScale.x)
		InBox.mScale.x = OutBox.mScale.x;
	if (InBox.mScale.y > OutBox.mScale.y)
		InBox.mScale.y = OutBox.mScale.y;

	if (InBox.mScale.x > 1.0f)
		InBox.mScale.x = 1.0f;
	if (InBox.mScale.y > 1.0f)
		InBox.mScale.y = 1.0f;

	InBox.mOrientation = OutBox.mOrientation;
	// COMPUTE RIGHT BOX
	if (operation == OPERATION::SCALE_)
	{
		RightBox.mScale = InBox.mScale;
		RightBox.mTranslationZ = AEVec3(objtransform->mWorld.mTranslationZ.x + Editorstate->mGizmoSize * objtransform->GetScale().x * viewRectangleSize / 3.5f, objtransform->mWorld.mTranslationZ.y, objtransform->GetPosition3D().z);
	}

	if (operation == OPERATION::TRANSLATE_)
	{
		InBox.mOrientation = 0.0f;
		RightBox.mScale.x = InBox.mScale.x * 1.5f;
		RightBox.mScale.y = InBox.mScale.y * 0.6f;
		RightBox.mOrientation = 0.0f;
		RightBox.mTranslationZ = AEVec3(objtransform->mWorld.mTranslationZ.x + Editorstate->mGizmoSize * objtransform->GetScale().x * viewRectangleSize / 3.5f, objtransform->mWorld.mTranslationZ.y, objtransform->GetPosition3D().z);

	}
	
	vec2 = AEVec2(objtransform->GetPosition3D().x + objtransform->GetScale().x / 2, objtransform->GetPosition3D().y);
	vec2 = rotationmatrix.MultVec(vec2);

	// COMPUTE TOP BOX  
	if (operation == OPERATION::SCALE_)
	{
		TopBox.mScale = InBox.mScale;
		TopBox.mTranslationZ = AEVec3(objtransform->mWorld.mTranslationZ.x, objtransform->mWorld.mTranslationZ.y + Editorstate->mGizmoSize * objtransform->GetScale().y * viewRectangleSize / 3.5f, objtransform->GetPosition3D().z);
	}

	if (operation == OPERATION::TRANSLATE_)
	{
		TopBox.mScale.x = InBox.mScale.x * 0.6f;
		TopBox.mScale.y = InBox.mScale.y * 1.5f;
		TopBox.mTranslationZ = AEVec3(objtransform->mWorld.mTranslationZ.x, objtransform->mWorld.mTranslationZ.y + Editorstate->mGizmoSize * objtransform->GetScale().y * viewRectangleSize / 3.5f, objtransform->GetPosition3D().z);
		TopBox.mOrientation = 0.0f;
	}

	vec2 = AEVec2(objtransform->GetPosition3D().x, objtransform->GetPosition3D().y + objtransform->GetScale().y / 2.0f);
	vec2 = rotationmatrix.MultVecDir(vec2);
	// COMPUTE CORNER BOX 
	CornerBox.mScale = InBox.mScale;
	vec2 = AEVec2(objtransform->GetPosition3D().x + OutBox.mScale.x / 2 , objtransform->GetPosition3D().y + OutBox.mScale.y / 2.0f);
	vec2 = rotationmatrix.MultVecDir(vec2);
	AEVec2 extentSum = xHalfExtent + yHalfExtent;
	CornerBox.mTranslationZ = AEVec3(objtransform->GetPosition3D().x + objtransform->GetScale().x * viewRectangleSize / 3.5f * texSize.x, objtransform->GetPosition3D().y + objtransform->GetScale().y * viewRectangleSize / 3.5f * texSize.y, objtransform->GetPosition3D().z);
	//COMPUTE CIRCLE
	circle.pos = AEVec2(OutBox.mTranslationZ.x, OutBox.mTranslationZ.y);
	if(OutBox.mScale.x < OutBox.mScale.y)
		circle.radius = OutBox.mScale.x * viewRectangleSize / 7.0f;
	else
		circle.radius = OutBox.mScale.y * viewRectangleSize / 7.0f;
	circle.numVertices = 48;
	circle.color = AEVec4(255, 0, 0, 1.0f);
	// COMPUTE ROTATION BOX
	rotationbox.mScale = OutBox.mScale * viewRectangleSize / 20.0f;
	vec2 = AEVec2(objtransform->GetPosition3D().x, objtransform->GetPosition3D().y + circle.radius/1.8f);
	vec2 = rotationmatrix.MultVecDir(vec2);
	vec2 = AEVec2(objtransform->mWorld.mTranslationZ.x, objtransform->mWorld.mTranslationZ.y + (rotationbox.mScale.y + circle.radius)/2.0f);
	rotationbox.mTranslationZ = AEVec3(vec2.x, vec2.y, objtransform->GetPosition3D().z);


	ShowGizmos();
}
void Gizmos::ShowGizmos()
{
	const f32 viewRectangleSize = Editorstate->mEditorCamera->mViewRectangleSize;

	TransformComp* objtransform = Editor->imGui_selectedGO->GetComp<TransformComp>();

	// SHOW OUTSIDE BOX 
	GfxMgr->DrawRectangle(OutBox, AEVec4(255, 255, 255, 1.0f));
	if (!showgizmos)
		return;
	if (operation == OPERATION::NONE_)
		return;

	// SHOW SMALL BOX
	if (operation == OPERATION::TRANSLATE_)
	{
		GfxMgr->DrawRectangle(RightBox, AEVec4(1.0f, 0.0f, 0.0f, 1.0f));
		GfxMgr->DrawRectangle(TopBox, AEVec4(0.0f, 1.0f, 0.0f, 1.0f));
		GfxMgr->DrawRectangle(InBox, AEVec4(255, 255, 255, 1.0f));

		DebugLine xLine;
		xLine.start = AEVec2(objtransform->mWorld.mTranslationZ.x, objtransform->mWorld.mTranslationZ.y);
		xLine.end = AEVec2(objtransform->mWorld.mTranslationZ.x + Editorstate->mGizmoSize * objtransform->GetScale().x * viewRectangleSize / 3.5f, objtransform->mWorld.mTranslationZ.y);
		xLine.color = AEVec4(1.0f, 0.0f, 0.0f, 1.0f);
		GfxMgr->DrawLine(xLine);

		DebugLine yLine;
		yLine.start = AEVec2(objtransform->mWorld.mTranslationZ.x, objtransform->mWorld.mTranslationZ.y);
		yLine.end = AEVec2(objtransform->mWorld.mTranslationZ.x, objtransform->mWorld.mTranslationZ.y + Editorstate->mGizmoSize * objtransform->GetScale().y * viewRectangleSize / 3.5f);
		yLine.color = AEVec4(0.0f, 1.0f, 0.0f, 1.0f);
		GfxMgr->DrawLine(yLine);
	}
	// SHOW LEFT BOX
	if (operation == OPERATION::SCALE_)
	{
		GfxMgr->DrawRectangle(RightBox, AEVec4(0, 255, 0, 1.0f));
		GfxMgr->DrawRectangle(CornerBox, AEVec4(255, 255, 0, 1.0f));
		GfxMgr->DrawRectangle(TopBox, AEVec4(255, 255, 255, 1.0f));

		DebugLine xLine;
		xLine.start = AEVec2(objtransform->mWorld.mTranslationZ.x, objtransform->mWorld.mTranslationZ.y);
		xLine.end = AEVec2(objtransform->mWorld.mTranslationZ.x + Editorstate->mGizmoSize * objtransform->GetScale().x * viewRectangleSize / 3.5f, objtransform->mWorld.mTranslationZ.y);
		xLine.color = AEVec4(0.2f, 0.53f, 0.92f, 1.0f);
		GfxMgr->DrawLine(xLine);

		DebugLine yLine;
		yLine.start = AEVec2(objtransform->mWorld.mTranslationZ.x, objtransform->mWorld.mTranslationZ.y);
		yLine.end = AEVec2(objtransform->mWorld.mTranslationZ.x, objtransform->mWorld.mTranslationZ.y + Editorstate->mGizmoSize * objtransform->GetScale().y * viewRectangleSize / 3.5f);
		yLine.color = AEVec4(0.96f, 0.57f, 0.06f, 1.0f);
		GfxMgr->DrawLine(yLine);
	}

	if (operation == OPERATION::ROTATE_)
	{
		GfxMgr->DrawCircle(circle);
		GfxMgr->DrawRectangle(rotationbox, AEVec4(0,100,100,1.0f));
	}

	CheckGizmosCollisions();


	// first stament of the if should be removed if only vertical and horizontal move
	// is implemented, which is commented in the ChangeTransform function
	if (operation == OPERATION::TRANSLATE_)
	{
		if (Leftmousepressed)
		{
			if (inboxisselected || topboxselected || rightboxselected)
			{
				ObjectModified = ChangeTransform();
			}
		}
	}

	if (operation == OPERATION::SCALE_)
	{
		if (Leftmousepressed)
		{
			if (rightboxselected || topboxselected || cornerboxselected)
				ComputeScale();
		}

	}
	if (operation == OPERATION::ROTATE_)
	{
		if (Leftmousepressed)
		{
			if (rotationboxselected)
				toRotate = ComputeRotation(); // actaully computing the angle
		}
		if(LeftMouseReleased)
		{
			if (rotchanged) // rotate
			{
				TransformComp* trans = Editor->imGui_selectedGO->GetComp<TransformComp>();

				if (trans == NULL)
					return;

				Editor->AddJsonToUndoStack();
				trans->SetRotationAngle(- toRotate);
				rotchanged = false;
			}
		}
	}


	if (!Leftmousepressed)
	{
		rightboxselected = false;
		cornerboxselected = false;
		inboxisselected = false;
		topboxselected = false;
		rotationboxselected = false;
	}
}

void Gizmos::CheckInputs()
{
	ImGuiIO& io = ImGui::GetIO();

	if (io.WantTextInput)
		return;

	if (aexInput->KeyPressed(GLFW_KEY_1))
		operation = OPERATION::TRANSLATE_;
	if (aexInput->KeyPressed(GLFW_KEY_2))
		operation = OPERATION::SCALE_;
	if (aexInput->KeyPressed(GLFW_KEY_3))
		operation = OPERATION::ROTATE_;
	if (aexInput->KeyPressed(GLFW_KEY_0))
		operation = OPERATION::NONE_;


	if (aexInput->KeyPressed(GLFW_KEY_DELETE)) // delete 
	{
		if (showgizmos)
		{
			if (Editor->imGui_selectedGO != NULL)
			{
				Editor->imGui_selectedGO->GetParentSpace()->DestroyObject(Editor->imGui_selectedGO);
				Editor->imGui_selectedGO = NULL;
			}
		}
		else
		{
			for (auto it = multipleselection->Selected_GOs.begin(); it != multipleselection->Selected_GOs.end();)
			{
				if (*it == Editor->imGui_selectedGO)
					Editor->imGui_selectedGO = NULL;

				(*it)->GetParentSpace()->DestroyObject(*it);
				it = multipleselection->Selected_GOs.erase(it);
			}
		}
	}

	if (aexInput->KeyPressed(Input::Keys::eControl) && aexInput->KeyTriggered(GLFW_KEY_C)) // copy
	{
		size_t size = multipleselection->Selected_GOs.size();
		for (unsigned i = 0; i < size; i++)
		{
			multipleselection->Selected_GOs.at(i)->CopyObject();
		}
	
	}

	if (aexInput->KeyPressed(Input::Keys::eControl) && aexInput->KeyTriggered(GLFW_KEY_V)) // paste
	{
		size_t size = multipleselection->Selected_GOs.size();
		for (unsigned i = 0; i < size; i++)
		{
			multipleselection->Selected_GOs.at(i)->PasteObject();
		}
		
	}

	if (aexInput->KeyPressed(Input::Keys::eControl) && aexInput->KeyTriggered(GLFW_KEY_Z)) // UNDO
	{
		if (!showgizmos)
			multipleselection->MultipleUndo();
		else
			Editor->Undo();
	}
	if (aexInput->KeyPressed(Input::Keys::eControl) && aexInput->KeyTriggered(GLFW_KEY_Y)) // REDO
	{
		if (!showgizmos)
			multipleselection->MultipleRedo();
		else
			Editor->Redo();
	}
}

void Gizmos::CheckGizmosCollisions()
{
	if (!Leftmousepressed)
		return;
	mouseposLB = Editorstate->mEditorCamera->PointToWorld(aexInput->GetGLFWMousePos());
	if (operation == OPERATION::TRANSLATE_)
	{
		if (StaticPointToOrientedRect(&mouseposLB, &AEVec2(InBox.mTranslationZ.x, InBox.mTranslationZ.y), InBox.mScale.x, InBox.mScale.y, InBox.mOrientation))
		{
			if (!inboxisselected)
				Editor->AddJsonToUndoStack();
			inboxisselected = true;
			prevmousepos = mouseposLB;
		}
		else if (StaticPointToOrientedRect(&mouseposLB, &AEVec2(RightBox.mTranslationZ.x, RightBox.mTranslationZ.y), RightBox.mScale.x, RightBox.mScale.y, RightBox.mOrientation))
		{
			if (!rightboxselected)
				Editor->AddJsonToUndoStack();
			rightboxselected = true;
		}
		else if (StaticPointToOrientedRect(&mouseposLB, &AEVec2(TopBox.mTranslationZ.x, TopBox.mTranslationZ.y), TopBox.mScale.x, TopBox.mScale.y, TopBox.mOrientation))
		{
			if (!topboxselected)
				Editor->AddJsonToUndoStack();
			topboxselected = true;
		}
	}

	if (operation == OPERATION::SCALE_)
	{
		// Check rightbox 
		if (StaticPointToOrientedRect(&mouseposLB, &AEVec2(RightBox.mTranslationZ.x, RightBox.mTranslationZ.y), RightBox.mScale.x, RightBox.mScale.y, RightBox.mOrientation))
		{
			if (!rightboxselected)
			{
				Editor->AddJsonToUndoStack();
				prevScale = Editor->imGui_selectedGO->GetComp<TransformComp>()->GetScale();
				prevmousepos = mouseposLB;
			}
			rightboxselected = true;
		}
		else if (StaticPointToOrientedRect(&mouseposLB, &AEVec2(CornerBox.mTranslationZ.x, CornerBox.mTranslationZ.y), CornerBox.mScale.x, CornerBox.mScale.y, CornerBox.mOrientation))
		{
			if (!cornerboxselected)
			{
				Editor->AddJsonToUndoStack();
				prevScale = Editor->imGui_selectedGO->GetComp<TransformComp>()->GetScale();
				prevmousepos = mouseposLB;
			}
			cornerboxselected = true;
		}
		else if (StaticPointToOrientedRect(&mouseposLB, &AEVec2(TopBox.mTranslationZ.x, TopBox.mTranslationZ.y), TopBox.mScale.x, TopBox.mScale.y, TopBox.mOrientation))
		{
			if (!topboxselected)
			{
				Editor->AddJsonToUndoStack();
				prevScale = Editor->imGui_selectedGO->GetComp<TransformComp>()->GetScale();
				prevmousepos = mouseposLB;
			}
			topboxselected = true;
		}
	}

	if (operation == OPERATION::ROTATE_)
	{
		if (StaticPointToOrientedRect(&mouseposLB, &AEVec2(rotationbox.mTranslationZ.x, rotationbox.mTranslationZ.y), rotationbox.mScale.x, rotationbox.mScale.y, rotationbox.mOrientation))
		{
			//compute line
			initialrotpos = AEVec2(rotationbox.mTranslationZ.x, rotationbox.mTranslationZ.y);
			rotationboxselected = true;
		}

	}
}

void Gizmos::ComputeScale()
{
	bool flag = false;
	onScale = true;
	TransformComp* trans = Editor->imGui_selectedGO->GetComp<TransformComp>();

	if (trans == NULL)
		return;


	AEVec2 dist = AEVec2(mouseposLB.x - prevmousepos.x, mouseposLB.y - prevmousepos.y);

	if (rightboxselected)
	{
		if (dist.x > 0)
		{
			AEVec2 check = prevScale / 2 + dist;
			if (check.x <= OutBox.mScale.x / 2)
				return;
		}
		else
		{
			AEVec2 check = prevScale / 2 + dist;
			if (check.x >= OutBox.mScale.x / 2)
				return;
		}
		trans->SetScale(AEVec2(prevScale.x + dist.x, prevScale.y));
	}
	if (topboxselected)
	{
		if (dist.y > 0)
		{
			AEVec2 check = prevScale / 2 + dist;
			if (check.y <= OutBox.mScale.y / 2)
				return;
		}
		else
		{
			AEVec2 check = prevScale / 2 + dist;
			if (check.y >= OutBox.mScale.y / 2)
				return;
		}
		trans->SetScale(AEVec2(prevScale.x , prevScale.y + dist.y));
	}

	if (cornerboxselected)
	{
		if (dist.x > 0)
		{
			AEVec2 check = prevScale / 2 + dist;
			if (check.x <= OutBox.mScale.x / 2)
				return;
		}
		else
		{
			AEVec2 check = prevScale / 2 + dist;
			if (check.x >= OutBox.mScale.x / 2)
				return;
		}

		if (dist.y > 0)
		{
			AEVec2 check = prevScale / 2 + dist;
			if (check.y <= OutBox.mScale.y / 2)
				return;
		}
		else
		{
			AEVec2 check = prevScale / 2 + dist;
			if (check.y >= OutBox.mScale.y / 2)
				return;
		}
		trans->SetScale(AEVec2(prevScale.x + dist.x, prevScale.y + dist.x)); // add the x to make it proportional always
	}
}

bool Gizmos::ChangeTransform()
{
	if (Editor->imGui_selectedGO == NULL)
		return false;
	const f32 viewRectangleSize = Editorstate->mEditorCamera->mViewRectangleSize;
	TransformComp* trans = Editor->imGui_selectedGO->GetComp<TransformComp>();

	if (trans == NULL)
		return false;

	SpriteComp* objSprite = Editor->imGui_selectedGO->GetComp<SpriteComp>();
	AEVec2 texSize = objSprite ? AEVec2(static_cast<f32>(objSprite->mTex->get()->GetWidth()) / 100.0f, static_cast<f32>(objSprite->mTex->get()->GetHeight() / 100.0f)) : AEVec2(1.0f, 1.0f);

	AEVec3 previoustrnasform = /*trans->mLocal.mTranslationZ;*/trans->GetPosition3D();

	if(inboxisselected)
		trans->SetPosition3D(AEVec3(mouseposLB.x, mouseposLB.y, previoustrnasform.z));
	// vertical movement only
	else if (topboxselected)
		trans->SetPosition3D(AEVec3(previoustrnasform.x , mouseposLB.y - Editorstate->mGizmoSize * trans->GetScale().y *  viewRectangleSize / 3.5f, previoustrnasform.z));
	// horizontal movement only
	else if (rightboxselected)
		trans->SetPosition3D(AEVec3(mouseposLB.x - Editorstate->mGizmoSize * trans->GetScale().x * viewRectangleSize / 3.5f, previoustrnasform.y, previoustrnasform.z));


	if (previoustrnasform.x != trans->mLocal.mTranslationZ.x ||//GetPosition3D().x ||/*&&*/ // if position has changed
		previoustrnasform.y != trans->mLocal.mTranslationZ.y)//GetPosition3D().y)
	{
		trans->translationChanged = true;
		AEVec3 displacement3D = trans->mLocal.mTranslationZ - previoustrnasform;
		trans->transDisplacement = AEVec2(displacement3D.x, displacement3D.y);
		return true;
	}
	else
		trans->translationChanged = false;

	return false;
}


f32 Gizmos::ComputeRotation()
{
	TransformComp* trans = Editor->imGui_selectedGO->GetComp<TransformComp>();

	if (trans == NULL)
		return 0;

	DebugLine mouseline;
	mouseline.start = AEVec2(InBox.mTranslationZ.x, InBox.mTranslationZ.y);
	mouseline.end = mouseposLB;
	mouseline.color = AEVec4(255, 0, 255, 1.0f);
	AEVec2 mousevec = mouseline.end - mouseline.start;
	GfxMgr->DrawLine(mouseline);

	DebugLine boxline;
	boxline.start = AEVec2(InBox.mTranslationZ.x, InBox.mTranslationZ.y);
	boxline.end = AEVec2(rotationbox.mTranslationZ.x, rotationbox.mTranslationZ.y);
	boxline.color = AEVec4(0, 0, 255, 1.0f);
	AEVec2 boxvec = boxline.end - boxline.start;
	GfxMgr->DrawLine(boxline);

	f32 dot = mousevec.x * boxvec.x + mousevec.y * boxvec.y;
	f32 det = mousevec.x * boxvec.y - mousevec.y * boxvec.x;
	f32 toRotate = atan2(det, dot);

	rotchanged = true;

	return  toRotate;
}

void MultipleSelection::MultipleSelectionLogic()
{
	/*if (update == false)
	{
		return;
	}*/

	if (aexInput->KeyPressed(GLFW_KEY_LEFT_CONTROL))
	{
		ControlLogic();
	}
	LeftMouseReleased = false;
	if (box.mScale.x > 0.2f || box.mScale.y > 0.2f)
		Editor->mgizmos->showgizmos = false;
	else
		Editor->mgizmos->showgizmos = true;

	if (aexInput->MousePressed(Input::eMouseRight))
	{
		if (!Editor->mgizmos->Rightmousepressed)
			prevmousepos = Editorstate->mEditorCamera->PointToWorld(aexInput->GetGLFWMousePos());

		SquareCreation();
	}


	mousepos = Editorstate->mEditorCamera->PointToWorld(aexInput->GetGLFWMousePos());

	if (aexInput->MousePressed(Input::eMouseLeft)) // if clicked outside the box
	{
		if (!StaticPointToStaticRect(&Editorstate->mEditorCamera->PointToWorld(aexInput->GetGLFWMousePos()), &AEVec2(box.mTranslationZ.x, box.mTranslationZ.y), box.mScale.x, box.mScale.y))
		{
			if (!onRotation && !onScale && !onTransform)
			{
				Selected_GOs.resize(0);
				box.mScale = AEVec2(0, 0);
			}
		}
	}

	if (aexInput->MousePressed(Input::eMouseLeft))
	{
		LeftMousePressed = true;

	}
	else
	{
		if (LeftMousePressed) // if released
		{
			LeftMouseReleased = true;
			onTransform = false;
			onRotation = false;
			onScale = false;
			inboxisselected = false;
			rightboxselected = false;
			topboxselected = false;
		}

		LeftMousePressed = false;
	}



	ComputeMultipleGizmos();
	CheckGizmosCollision();

	ShowMultipleGizmos();

	if (Editor->mgizmos->operation == Gizmos::OPERATION::TRANSLATE_)
	{
		if (onTransform && Selected_GOs.size() >= 1)
			MultipleTransform();
	}
	
	if (Editor->mgizmos->operation == Gizmos::OPERATION::SCALE_)
	{
		if (onScale && Selected_GOs.size() >= 1)
			MultipleScale();
	}

	if (Editor->mgizmos->operation == Gizmos::OPERATION::ROTATE_)
	{
		if (onRotation)
			ComputeMultipleRotation();
		if (LeftMouseReleased)
			ApplyMultipleRotation();
	}
}

void MultipleSelection::SquareCreation()
{
	AEVec2 currentmousepos = Editorstate->mEditorCamera->PointToWorld(aexInput->GetGLFWMousePos());
	
	AEVec2 midpoint = AEVec2((currentmousepos.x + prevmousepos.x) / 2, (currentmousepos.y + prevmousepos.y) / 2);
	f32 xdist = abs(currentmousepos.x - prevmousepos.x);
	f32 ydist = abs(currentmousepos.y - prevmousepos.y);

	box.mTranslationZ = AEVec3(midpoint.x, midpoint.y, 0);
	box.mScale.x = xdist;
	box.mScale.y = ydist;

	if (box.mScale.x > 0.4|| box.mScale.y > 0.4)
		Editor->imGui_selectedGO = NULL;
	if(!onTransform)
		CheckCollision();

	ReleaseVector();
	
}

void MultipleSelection::CheckCollision()
{
	FOR_EACH(spaceit, aexScene->mSpaces)
	{
		FOR_EACH(mapit, (*spaceit)->mallObjects)
		{
			FOR_EACH(goit, mapit->second)
			{
				TransformComp* transform = (*goit)->GetComp<TransformComp>();
				if (transform == NULL)
					continue;
				AEVec2 objectpos = AEVec2(transform->GetPosition3D().x, transform->GetPosition3D().y);
				if (StaticPointToStaticRect(&objectpos, &AEVec2(box.mTranslationZ.x, box.mTranslationZ.y), box.mScale.x, box.mScale.y))
				{
					if (aexInput->KeyPressed(GLFW_KEY_LEFT_SHIFT))
					{
						if ((*goit)->selectable && transform->GetPosition3D().z == 0.0f)
							if (std::find(Selected_GOs.begin(), Selected_GOs.end(), *goit) == Selected_GOs.end())
								Selected_GOs.push_back(*goit);
					}
					else
					{
						if ((*goit)->selectable)
						{
							if(std::find(Selected_GOs.begin(), Selected_GOs.end(), *goit) == Selected_GOs.end())
								Selected_GOs.push_back(*goit);
								
						}
					}
				}
			}
		}
	}
}

void MultipleSelection::ComputeMultipleGizmos()
{
	/*if (update == false)
	{
		return;
	}*/
	// COMPUTE INBOX
	inbox.mTranslationZ = box.mTranslationZ;
	inbox.mScale = box.mScale / 8;

	rotationbox.mScale = inbox.mScale;
	rotationbox.mTranslationZ = AEVec3(inbox.mTranslationZ.x, inbox.mTranslationZ.y + box.mScale.y/2.3f, 0.0f);

	if (Editor->mgizmos->operation == Gizmos::OPERATION::SCALE_)
	{
		rightbox.mScale = inbox.mScale;
		rightbox.mTranslationZ = AEVec3(inbox.mTranslationZ.x + box.mScale.x / 2.3f, inbox.mTranslationZ.y, 0.0f);

		topbox.mScale = inbox.mScale;
		topbox.mTranslationZ = AEVec3(inbox.mTranslationZ.x, inbox.mTranslationZ.y + box.mScale.y / 2.3f, 0.0f);

		cornerbox.mScale = inbox.mScale;
		cornerbox.mTranslationZ = AEVec3(inbox.mTranslationZ.x + box.mScale.x / 2.3f, inbox.mTranslationZ.y + box.mScale.y / 2.3f, 0.0f);
	}
	if (Editor->mgizmos->operation == Gizmos::OPERATION::TRANSLATE_)
	{
		rightbox.mScale = inbox.mScale;
		rightbox.mScale.y /= 2;
		rightbox.mTranslationZ = AEVec3(inbox.mTranslationZ.x + box.mScale.x / 2.2f, inbox.mTranslationZ.y, 0.0f);

		topbox.mScale = inbox.mScale;
		topbox.mScale.x /= 2;
		topbox.mTranslationZ = AEVec3(inbox.mTranslationZ.x, inbox.mTranslationZ.y +  box.mScale.y / 2.2f, 0.0f);
	}

	if (Editor->mgizmos->operation == Gizmos::OPERATION::ROTATE_)
	{
		//COMPUTE CIRCLE
		circle.pos = AEVec2(box.mTranslationZ.x, box.mTranslationZ.y);
		if (box.mScale.x < box.mScale.y)
			circle.radius = box.mScale.x;
		else
			circle.radius = box.mScale.y;
		circle.numVertices = 78;
		circle.color = AEVec4(255, 0, 0, 1.0f);
	}
}

void MultipleSelection::ShowMultipleGizmos()
{
	GfxMgr->DrawRectangle(box, AEVec4(0, 100, 255, 1.0f));

	if (Editor->mgizmos->operation == Gizmos::OPERATION::TRANSLATE_)
	{
		GfxMgr->DrawRectangle(inbox, AEVec4(0, 100, 100, 1.0f));
		GfxMgr->DrawRectangle(rightbox, AEVec4(0, 190, 88, 1.0f));
		GfxMgr->DrawRectangle(topbox, AEVec4(0, 104, 31, 1.0f));
		DebugLine xLine;
		xLine.start = AEVec2(box.mTranslationZ.x, box.mTranslationZ.y);
		xLine.end = AEVec2(box.mTranslationZ.x + box.mScale.x / 2.2f, box.mTranslationZ.y);
		xLine.color = AEVec4(1.0f, 0.0f, 0.0f, 1.0f);
		GfxMgr->DrawLine(xLine);

		DebugLine yLine;
		yLine.start = AEVec2(box.mTranslationZ.x, box.mTranslationZ.y);
		yLine.end = AEVec2(box.mTranslationZ.x, box.mTranslationZ.y + box.mScale.y / 2.2f);
		yLine.color = AEVec4(0.0f, 1.0f, 0.0f, 1.0f);
		GfxMgr->DrawLine(yLine);
	}

	if (Editor->mgizmos->operation == Gizmos::OPERATION::SCALE_)
	{
		GfxMgr->DrawRectangle(rightbox, AEVec4(200, 190, 88, 1.0f));
		GfxMgr->DrawRectangle(topbox, AEVec4(255, 104, 31, 1.0f));
		GfxMgr->DrawRectangle(cornerbox, AEVec4(255, 104, 31, 1.0f));
	}
	if (Editor->mgizmos->operation == Gizmos::OPERATION::ROTATE_)
		GfxMgr->DrawRectangle(rotationbox, AEVec4(0, 255, 0, 1.0f));

	if (Editor->mgizmos->operation == Gizmos::OPERATION::ROTATE_)
	{
		GfxMgr->DrawCircle(circle);
	}
	FOR_EACH(it, Selected_GOs)
	{
		TransformComp* transformcomp = (*it)->GetComp<TransformComp>();
		if (transformcomp == NULL)
			return;
		Transform transform;
		transform.mTranslationZ = AEVec3(transformcomp->GetPosition3D().x, transformcomp->GetPosition3D().y, 0);
		transform.mScale = transformcomp->GetScale();
		transform.mOrientation = transformcomp->GetRotationAngle();

		GfxMgr->DrawRectangle(transform, AEVec4(255, 255, 255, 1.0f));
	}
}

void MultipleSelection::CheckGizmosCollision()
{
	if (Editor->mgizmos->operation == Gizmos::OPERATION::TRANSLATE_)
	{
		if(aexInput->MousePressed(Input::eMouseLeft))
		{
			if (StaticPointToStaticRect(&mousepos, &AEVec2(inbox.mTranslationZ.x, inbox.mTranslationZ.y), inbox.mScale.x, inbox.mScale.y))
			{
				if (!onTransform)
				{
					MultipleAddJsonToUndoStack();
					prevbox = box;
				}
				ComputeDiff();
				onTransform = true;
				inboxisselected = true;
			}
			if (StaticPointToStaticRect(&mousepos, &AEVec2(rightbox.mTranslationZ.x, rightbox.mTranslationZ.y), rightbox.mScale.x, rightbox.mScale.y))
			{
				if (!onTransform)
				{
					MultipleAddJsonToUndoStack();
					prevbox = box;
				}
				ComputeDiff();
				onTransform = true;
				rightboxselected = true;
			}
			if (StaticPointToStaticRect(&mousepos, &AEVec2(topbox.mTranslationZ.x, topbox.mTranslationZ.y), topbox.mScale.x, topbox.mScale.y))
			{
				if (!onTransform)
				{
					MultipleAddJsonToUndoStack();
					prevbox = box;
				}
				ComputeDiff();
				onTransform = true;
				topboxselected = true;
			}
		}
	}

	if (Editor->mgizmos->operation == Gizmos::OPERATION::SCALE_)
	{
		if (aexInput->MousePressed(Input::eMouseLeft))
		{
			if (StaticPointToStaticRect(&mousepos, &AEVec2(rightbox.mTranslationZ.x, rightbox.mTranslationZ.y), rightbox.mScale.x, rightbox.mScale.y))
			{
				if (!onScale)
				{
					MultipleAddJsonToUndoStack();
					prevbox = box;
					prevmousepos = Editorstate->mEditorCamera->PointToWorld(aexInput->GetGLFWMousePos());
					prevscale = box.mScale;
				}
				onScale = true;
				rightboxselected = true;
			}

			if (StaticPointToStaticRect(&mousepos, &AEVec2(topbox.mTranslationZ.x, topbox.mTranslationZ.y), topbox.mScale.x, topbox.mScale.y))
			{
				if (!onScale)
				{
					MultipleAddJsonToUndoStack();
					prevbox = box;
					prevmousepos = Editorstate->mEditorCamera->PointToWorld(aexInput->GetGLFWMousePos());
					prevscale = box.mScale;
				}
				onScale = true;
				topboxselected = true;
			}
			
			if (StaticPointToStaticRect(&mousepos, &AEVec2(cornerbox.mTranslationZ.x, cornerbox.mTranslationZ.y), cornerbox.mScale.x, cornerbox.mScale.y))
			{
				if (!onScale)
				{
					MultipleAddJsonToUndoStack();
					prevbox = box;
					prevmousepos = Editorstate->mEditorCamera->PointToWorld(aexInput->GetGLFWMousePos());
					prevscale = box.mScale;
				}
				onScale = true;
				cornerboxselected = true;
			}
		}
	}

	if (Editor->mgizmos->operation == Gizmos::OPERATION::ROTATE_)
	{
		if (aexInput->MousePressed(Input::eMouseLeft))
		{
			if (StaticPointToStaticRect(&mousepos, &AEVec2(rotationbox.mTranslationZ.x, rotationbox.mTranslationZ.y), rotationbox.mScale.x, rotationbox.mScale.y))
			{
				if (!onRotation)
				{
					MultipleAddJsonToUndoStack();
					prevbox = box;
				}
				onRotation = true;
			}
		}
	}
}

void  MultipleSelection::ControlLogic()
{
	if (Editor->imGui_selectedGO)
	{
		if (std::find(Selected_GOs.begin(), Selected_GOs.end(), Editor->imGui_selectedGO) == Selected_GOs.end())
			Selected_GOs.push_back(Editor->imGui_selectedGO);
	}

	FOR_EACH(spaceit, aexScene->mSpaces)
	{
		FOR_EACH(mapit, (*spaceit)->mallObjects)
		{
			FOR_EACH(goit, mapit->second)
			{
				TransformComp* transform = (*goit)->GetComp<TransformComp>();
				if (transform == NULL)
					continue;
				AEVec2 objectpos = AEVec2(transform->GetPosition3D().x, transform->GetPosition3D().y);
				if (StaticPointToStaticRect(&mousepos, &objectpos, transform->GetScale().x, transform->GetScale().y))
				{
					if (aexInput->MousePressed(Input::eMouseRight))
					{
						if (std::find(Selected_GOs.begin(), Selected_GOs.end(), *goit) == Selected_GOs.end())
						{
							if((*goit)->selectable)
								if (std::find(Selected_GOs.begin(), Selected_GOs.end(), *goit) == Selected_GOs.end())
									Selected_GOs.push_back(*goit);
						}
					}
				}
			}
		}
	}

	if (Selected_GOs.size() > 1)
		ComputeControlBox();
}


void MultipleSelection::ComputeControlBox()
{
	f32 leftmost = 1000;
	f32 rightmost = -1000;
	f32 topmost = -1000;
	f32 botmost = 1000;
	FOR_EACH(goit, Selected_GOs)
	{
		TransformComp* transform = (*goit)->GetComp<TransformComp>();
		if (transform == NULL)
			continue;

		AEVec2 pos = AEVec2(transform->GetPosition().x, transform->GetPosition().y);

		if ((pos.x - transform->GetScale().x/2) < leftmost)
			leftmost = pos.x - transform->GetScale().x / 2;

		if ((pos.x + transform->GetScale().x / 2)  > rightmost)
			rightmost = pos.x + transform->GetScale().x / 2;

		if (pos.y - transform->GetScale().y / 2 < botmost)
			botmost = pos.y - transform->GetScale().y / 2;

		if (pos.y + transform->GetScale().y / 2 > topmost)
			topmost = pos.y + transform->GetScale().y / 2;
	}

	f32 xmidpoint = (leftmost + rightmost) / 2;
	f32 ymidpoint = (topmost + botmost) / 2;
	box.mTranslationZ = AEVec3(xmidpoint, ymidpoint, 0);
	box.mScale.x = rightmost - leftmost;
	box.mScale.y = topmost - botmost;
}


void MultipleSelection::ComputeDiff()
{
	FOR_EACH(it, Selected_GOs)
	{
		TransformComp* transform = (*it)->GetComp<TransformComp>();
		AEVec2 pos = AEVec2(transform->GetPosition3D().x, transform->GetPosition3D().y);

		(*it)->diffRespecttoBox = AEVec2(pos.x - box.mTranslationZ.x, pos.y - box.mTranslationZ.y);
	}
}

bool MultipleSelection::MultipleTransform()
{
	bool flag = false;
	Editor->mgizmos->inboxisselected = false;
	Editor->mgizmos->topboxselected = false;
	Editor->mgizmos->rightboxselected = false;


	FOR_EACH(it, Selected_GOs)
	{
		TransformComp* trans = (*it)->GetComp<TransformComp>();

		if (trans == NULL)
			continue;

		AEVec3 previoustrnasform = trans->GetPosition3D();
		AEVec3 preboxtransfor = box.mTranslationZ;

		if (inboxisselected)
		{
			trans->SetPosition3D(AEVec3(mousepos.x + (*it)->diffRespecttoBox.x, mousepos.y + (*it)->diffRespecttoBox.y, previoustrnasform.z));
			box.mTranslationZ = AEVec3(mousepos.x, mousepos.y, 0);
		}
		if (rightboxselected)
		{
			trans->SetPosition3D(AEVec3(mousepos.x - box.mScale.x / 2.2f + (*it)->diffRespecttoBox.x, previoustrnasform.y, previoustrnasform.z));
			box.mTranslationZ = AEVec3(mousepos.x - box.mScale.x / 2.2f, preboxtransfor.y, 0.0f);
		}
		if (topboxselected)
		{
			trans->SetPosition3D(AEVec3(previoustrnasform.x, mousepos.y - box.mScale.y / 2.2f + (*it)->diffRespecttoBox.y, previoustrnasform.z));
			box.mTranslationZ = AEVec3(preboxtransfor.x, mousepos.y - box.mScale.y / 2.2f, 0.0f);
		}

		if (previoustrnasform.x != trans->GetPosition3D().x && // if position has changed
			previoustrnasform.y != trans->GetPosition3D().y)
			flag =  true;
	}

	return flag;
}

bool MultipleSelection::MultipleScale()
{
	Editor->mgizmos->rightboxselected = false;
	Editor->mgizmos->topboxselected = false;
	Editor->mgizmos->cornerboxselected = false;

	bool flag = false;

	if (rightboxselected)
	{
		//only for x

		AEVec2 dist = AEVec2(mousepos.x - prevmousepos.x, mousepos.y - prevmousepos.y);

		if (dist.x > 0)
		{
			AEVec2 check = prevscale / 2 + dist;
			if (check.x <= box.mScale.x / 2)
				return false;
		}
		else
		{
			AEVec2 check = prevscale / 2 + dist;
			if (check.x >= box.mScale.x / 2)
				return false;
		}
		box.mScale.x += dist.x;

		FOR_EACH(it, Selected_GOs)
		{
			TransformComp* trans = (*it)->GetComp<TransformComp>();

			if (trans == NULL)
				continue;

			AEVec2 previousScale = trans->GetScale();

			trans->SetScale(AEVec2(previousScale.x + dist.x, previousScale.y));

			if (previousScale.x != trans->GetScale().x && // if position has changed
				previousScale.y != trans->GetScale().y)
				flag = true;
		}
	}
	if (topboxselected)
	{
		//only for y

		AEVec2 dist = AEVec2(mousepos.x - prevmousepos.x, mousepos.y - prevmousepos.y);

		if (dist.y > 0)
		{
			AEVec2 check = prevscale / 2 + dist;
			if (check.y <= box.mScale.y / 2)
				return false;
		}
		else
		{
			AEVec2 check = prevscale / 2 + dist;
			if (check.y >= box.mScale.y / 2)
				return false;
		}
		box.mScale.y += dist.y;

		FOR_EACH(it, Selected_GOs)
		{
			TransformComp* trans = (*it)->GetComp<TransformComp>();

			if (trans == NULL)
				continue;

			AEVec2 previousScale = trans->GetScale();

			trans->SetScale(AEVec2(previousScale.x, previousScale.y + dist.y));

			if (previousScale.x != trans->GetScale().x && // if position has changed
				previousScale.y != trans->GetScale().y)
				flag = true;
		}
	}
	if (cornerboxselected)
	{
		// for x and y

		AEVec2 dist = AEVec2(mousepos.x - prevmousepos.x, mousepos.y - prevmousepos.y);

		if (dist.x > 0)
		{
			AEVec2 check = prevscale / 2 + dist;
			if (check.x <= box.mScale.x / 2)
				return false;
		}
		else
		{
			AEVec2 check = prevscale / 2 + dist;
			if (check.x >= box.mScale.x / 2)
				return false;
		}
		box.mScale.x += dist.x;
		box.mScale.y += dist.x;

		FOR_EACH(it, Selected_GOs)
		{
			TransformComp* trans = (*it)->GetComp<TransformComp>();

			if (trans == NULL)
				continue;

			AEVec2 previousScale = trans->GetScale();

			trans->SetScale(AEVec2(previousScale.x + dist.x, previousScale.y + dist.x));

			if (previousScale.x != trans->GetScale().x && // if position has changed
				previousScale.y != trans->GetScale().y)
				flag = true;
		}
	}
	return flag;
}

bool MultipleSelection::ComputeMultipleRotation()
{
	Editor->mgizmos->rotationboxselected = false;

	DebugLine mouseline;
	mouseline.start = AEVec2(box.mTranslationZ.x, box.mTranslationZ.y);
	mouseline.end = mousepos;
	mouseline.color = AEVec4(255, 0, 255, 1.0f);
	AEVec2 mousevec = mouseline.end - mouseline.start;
	GfxMgr->DrawLine(mouseline);

	DebugLine boxline;
	boxline.start = AEVec2(box.mTranslationZ.x, box.mTranslationZ.y);
	boxline.end = AEVec2(rotationbox.mTranslationZ.x, rotationbox.mTranslationZ.y);
	boxline.color = AEVec4(0, 0, 255, 1.0f);
	AEVec2 boxvec = boxline.end - boxline.start;
	GfxMgr->DrawLine(boxline);

	f32 dot = mousevec.x * boxvec.x + mousevec.y * boxvec.y;
	f32 det = mousevec.x * boxvec.y - mousevec.y * boxvec.x;
	toRotate = atan2(det, dot);

	//rotchanged = true; // undo/redo
	//std::cout << toRotate << std::endl;
	return  false;
}

void MultipleSelection::MultipleUndo()
{
	if (undo_stack.size() == 0)
		return;

	MultipleAddJsonToRedoStack();

	std::vector<Helper> helper = undo_stack.top();

	FOR_EACH(bigit, Selected_GOs)
	{
		FOR_EACH(smallit, helper)
		{
			if(smallit->id == (*bigit)->GetBaseID())
				(*bigit)->FromJson(smallit->jsonfile);
		}
	}

	undo_stack.pop();
	Transform temp = box;
	box = prevbox;
	prevbox = temp;
}

void MultipleSelection::MultipleRedo()
{
	if (redo_stack.size() == 0)
		return;

	MultipleAddJsonToUndoStack();

	std::vector<Helper> helper = redo_stack.top();

	FOR_EACH(bigit, Selected_GOs)
	{
		FOR_EACH(smallit, helper)
		{
			if (smallit->id == (*bigit)->GetBaseID())
				(*bigit)->FromJson(smallit->jsonfile);
		}
	}

	redo_stack.pop();
	box = prevbox;
}
void MultipleSelection::MultipleAddJsonToRedoStack()
{
	std::vector<Helper> vector;
	FOR_EACH(it, Selected_GOs)
	{
		Helper helper;
		helper.id = (*it)->GetBaseID();
		helper.name = (*it)->GetBaseName();

		(*it)->ToJson(helper.jsonfile);

		vector.push_back(helper);
	}

	redo_stack.push(vector);
}

void MultipleSelection::MultipleAddJsonToUndoStack()
{
	std::vector<Helper> vector;
	FOR_EACH(it, Selected_GOs)
	{
		Helper helper;
		helper.id = (*it)->GetBaseID();
		helper.name = (*it)->GetBaseName();

		(*it)->ToJson(helper.jsonfile);

		vector.push_back(helper);
	}

	undo_stack.push(vector);
}



void MultipleSelection::ApplyMultipleRotation()
{
	//box.mOrientation = toRotate;
	FOR_EACH(it, Selected_GOs)
	{
		TransformComp* trans = (*it)->GetComp<TransformComp>();

		if (trans == NULL)
			return;

		//Editor->AddJsonToUndoStack(); // undo.redo
		trans->SetRotationAngle(-toRotate);
	}
}

void MultipleSelection::ReleaseVector()
{
	if (aexInput->KeyPressed(GLFW_KEY_LEFT_CONTROL))
		return;

	for (auto it = Selected_GOs.begin(); it != Selected_GOs.end();)
	{
		//std::cout << (*it)->GetBaseName();
		TransformComp* transform = (*it)->GetComp<TransformComp>();
		if (transform == NULL)
			continue;
		AEVec2 objectpos = AEVec2(transform->GetPosition3D().x, transform->GetPosition3D().y);
		if (!StaticPointToStaticRect(&objectpos, &AEVec2(box.mTranslationZ.x, box.mTranslationZ.y), box.mScale.x, box.mScale.y))
		{
			it = Selected_GOs.erase(it);
			if (it == Selected_GOs.end())
				break;
		}
		else
			it++;
	}
}