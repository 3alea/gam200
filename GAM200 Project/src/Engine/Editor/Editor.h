#pragma once
#include "Platform/AEXTime.h"
#include "Platform/AEXInput.h"
#include "src/Engine/Imgui/imgui.h"
#include "src/Engine/Math/Collisions.h"
#include "src/Engine/Components/Rigidbody.h"
#include "src/Engine/Graphics/GfxMgr.h"
#include "src/Engine/Composition/AEXScene.h"
#include "src/Engine/Math/ContactCollisions.h"
#include <map>
#include <stack>

using namespace AEX;

class Camera;

extern f32 LoadProgressTimer;

class EditorState : public ISystem
{
	AEX_RTTI_DECL(EditorState, ISystem);
	AEX_SINGLETON(EditorState);

	friend class MainEditor;

public:
	bool Initialize();
	bool IsPlaying() { return mIsPlaying; }
	void SetPlayingState(bool playingState) { mIsPlaying = playingState; }

	Camera * mEditorCamera;
	AEVec2 mEditorCameraPos;
	f32 mEditorCameraViewRectSize;
	f32 mGizmoSize = 1.0f;

private:
	bool mIsPlaying;
};

#define Editorstate (EditorState::Instance())

struct Helper
{
	u32 id;
	const char* name;
	json jsonfile;
};

struct MultipleSelection
{
	MultipleSelection() { box.mScale = AEVec2(0, 0); }
	void MultipleSelectionLogic();
	void SquareCreation();
	void CheckCollision();
	void ComputeMultipleGizmos();
	void ShowMultipleGizmos();
	void CheckGizmosCollision();
	void ControlLogic();
	void ComputeControlBox();
	void ReleaseVector();
	bool MultipleTransform();
	bool MultipleScale();
	AEVec2 prevscale;
	bool ComputeMultipleRotation();
	f32 toRotate = 0.0f;
	void ApplyMultipleRotation();
	void ComputeDiff();

	void MultipleUndo();
	void MultipleRedo();
	void MultipleAddJsonToUndoStack();
	void MultipleAddJsonToRedoStack();
	std::stack<std::vector<Helper>> undo_stack;
	std::stack<std::vector<Helper>> redo_stack;

	AEVec2 prevmousepos;
	AEVec2 mousepos;
	bool LeftMousePressed = false;
	bool LeftMouseReleased = false;

	std::vector<GameObject*> Selected_GOs;

	DebugCircle circle;
	Transform box;
	Transform prevbox;
	Transform inbox;
	Transform rightbox;
	Transform topbox;
	Transform cornerbox;
	Transform rotationbox;
	bool onTransform = false;
	bool onScale = false;
	bool onRotation = false;
	bool inboxisselected = false;
	bool rightboxselected = false;
	bool topboxselected = false;
	bool cornerboxselected = false;
};


struct Gizmos
{
	Gizmos(){
		multipleselection = new MultipleSelection;
	}
	~Gizmos() { delete multipleselection; }
	void CheckObjects();
	void CheckInputs();

	void ComputeGizmos();
	void ShowGizmos();
	void CheckGizmosCollisions();

	bool ChangeTransform();
	bool onTransform = false;
	void ComputeScale();
	bool onScale = false;
	f32 ComputeRotation();

	AEVec2 xAngle;
	AEVec2 yAngle;
	AEVec2 xHalfExtent;
	AEVec2 yHalfExtent;

	Transform OutBox;
	Transform InBox;
	bool inboxisselected = false;
	Transform TopBox;
	bool topboxselected = false;
	Transform RightBox;
	bool rightboxselected = false;
	Transform CornerBox;
	bool cornerboxselected = false;
	DebugCircle circle;
	Transform rotationbox;
	bool rotationboxselected = false;
	bool rotchanged = false;
	f32 toRotate = 0;
	AEVec2 toScale;
	AEVec2 prevScale;
	AEVec2 initialrotpos;
	AEVec2 currentrotpos;

	bool showgizmos = true;
	AEVec2 mouseposRB;
	AEVec2 mouseposLB;
	AEVec2 prevmousepos;
	bool Leftmousepressed = false;
	bool Rightmousepressed = false;
	bool LeftMouseReleased = false;
	bool RightMouseReleased = false;
	bool ObjectModified = false;
	enum OPERATION
	{
		NONE_,
		TRANSLATE_,
		SCALE_,
		ROTATE_
	};

	OPERATION operation = NONE_;

	MultipleSelection* multipleselection;
};


// Added by Miguel for progress bar functions
struct ProgressBarParamInfo
{
	const char * message = "Progress";
	float speed_factor = 1.0f;
	bool use_x_button = false;
	float bar_limit = 1.05f;
	bool should_reset_timer = true;
};

class MainEditor
{
	AEX_SINGLETON(MainEditor);

public:
	void Initialize()
	{
		timer = new AEXTimer; timer->Start(); 
		mgizmos = new Gizmos;
		archetypes_selectedSpace = aexScene->FindSpace("Main");
		Editorstate->mIsPlaying = true;
		WindowMgr->GetCurrentWindow()->SetFullScreen(false);
		LoadFromCertainLevel("LogosScreen");
		//if (Editorstate->mIsPlaying)
		//	SavetoTemp();
	}

	void MainLogic();
	// Show functions ----------
	void ShowTopMenu();
	void ShowObjectList();
	void ShowInspector();
	void ShowPlentyObjects();

	// Function and booleans to check for input for editor keyboard shortcuts -----------------------
	void ReadInput();
	bool Ctrl_S_Activated = false;
	bool Ctrl_Alt_S_Activated = false;

	// Added by Miguel
	void ShowArchetypeList();

	void ShowCollisionTable();

	void ShowUtilities();

	void ProgressBar(const char * message = "Progress", f32 speedFactor = 1.0f, bool useXButton = false, f32 barLimit = 1.05f, bool shouldResetTimer = true);
	ProgressBarParamInfo mParamInfo;
	bool mbShowProgressBar = false;
	// Don't use this one
	void ProgressBarLocked(const char * message = "Progress", f32 speedFactor = 1.0f, bool useXButton = false, f32 barLimit = 1.05f, bool shouldResetTimer = true);
	bool SnappingEnabled = false;

	bool Ctrl_Pressed = false;


	// Selected GameObjects functions
	//	-> Show functions
	bool ShowObjectInfo();
	bool settag = false;
	void ShowChildren();
	void ShowTags();
	//	-> Modify GameObject
	bool CreateChildren();
	bool createchildren = false;
	void DeleteGameObject();
	void CopyObject();
	bool ChangeObjectName();
	bool changename = false;
	// Search functionality ----
	void Search_by_Name();
	bool search_by_name = false;
	void Search_by_Tag();
	bool search_by_tag = false;
	// Add go, space by name
	void AddGameObjectbyName();
	bool addGObyname = false;
	Space* the_space;
	void AddSpacebyName();
	bool addspacebyname = false;
	// Add new scene
	void NewSceneDialog();
	bool mAddNewScene = false;
	bool mbAddSceneAfterSaving = false;
	// Add new collision group
	bool mAddNewCG = false;
	void NewCGDialog();
	// Load, save to/from json ---
	void SaveExistingLevel();
	void SavetoJson();
	bool saveToJson = false;
	void LoadfromJson();
	bool loadFromJson = false;
	std::string relPathToLoad;

	//Load, save to/from temp.json 
	//void SavetoTemp();
	//void LoadfromTemp();
	bool loadFromTemp = false;

	//Load, save to/from level.json
	// These are for the checkpoints
	void SaveToLevel(const float newX, const float newY);
	AEVec2 playerNewPosition;
	void LoadFromLevel();

	// Loading certain levels
	void LoadFromCertainLevel(const std::string& levelName);
	std::string levelToLoadName;
	bool loadFromCertainLevel = false;

	void ReloadLevel();
	// Loading checkpoints
	void LoadCheckpoint();

	// AutoSave logic -------------
	void AutoSave();
	bool changesmade = false;
	AEXTimer* timer;
	// Undo/redo ------------------
	void AddJsonToUndoStack();
	void AddJsonToRedoStack();
	void Undo();
	void Redo();
	std::stack<Helper> undo_stack;
	std::stack<Helper> redo_stack;
	// ---------------------

	GameObject * imGui_selectedGO = NULL;
	GameObject * PREVimGui_selectedGO = NULL;
	Space * archetypes_selectedSpace = NULL;//aexScene->FindSpace("Main");
	std::vector<GameObject*> ListimGui_selectedGO;
	Gizmos* mgizmos;

	// Determines if it should load a level or not
	bool loadCheckpoint = false;
};

#define Editor (MainEditor::Instance())

