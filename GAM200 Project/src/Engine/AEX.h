#ifndef AEX_H_
#define AEX_H_

#include "Core\AEXCore.h"
#include "Composition\AEXComposition.h"
#include "GameStates/AEXGameState.h"
#include "Platform\AEXInput.h"
#include "Platform\AEXTime.h"
#include "Utilities/AEXContainers.h"
#include "Graphics\GfxMgr.h"
#include "Graphics\WindowMgr.h"
#include "src\Engine\Physics\PhysicsManager.h"
#include "src\Engine\Physics\CollisionSystem.h"
#include "Audio/AudioManager.h"
#include "Resources/AEXResourceManager.h"
#include "src\Engine\Components\Logic.h"

// Easy access to singleton
#pragma warning (disable:4251) // dll and STL
namespace AEX {
	class AEXEngine : public ISystem
	{
		AEX_RTTI_DECL(AEXEngine, ISystem);
		AEX_SINGLETON(AEXEngine);
	public:
		virtual ~AEXEngine();
		virtual bool Initialize();
		void Run(IGameState*gameState = nullptr);
	};
}
#pragma warning (default:4251) // dll and STL
#define aexEngine (AEX::AEXEngine::Instance())

#endif