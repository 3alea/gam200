#include "PausedLogic.h"
#include "Composition/AEXComponent.h"


namespace AEX
{
	PausedLogicComp::PausedLogicComp()
	{
		PausedLogicComp::Initialize();
	}
	PausedLogicComp::~PausedLogicComp()
	{
		PausedLogicComp::Shutdown();
	}


	void PausedLogicComp::FromJson(json & value)
	{
		SetBaseName(value["Name"].get<std::string>().c_str());
		SetEnabled(value["Enabled"].get<bool>());
		mbRemovable = value["Removable"].get<bool>();
		mbGlobalComp = value["GlobalComponent"].get<bool>();
	}
	void PausedLogicComp::ToJson(json & value)
	{
		value["Name"] = GetBaseName();
		value["RTTI Type"] = GetType().GetName();
		value["Enabled"] = mbEnabled;
		value["Removable"] = mbRemovable;
		value["GlobalComponent"] = mbGlobalComp;
	}


	void PausedLogicComp::Initialize()
	{
		aexPausedLogic->AddComp(this);
	}
	void PausedLogicComp::Update()
	{
	}
	void PausedLogicComp::Shutdown()
	{
		aexPausedLogic->RemoveComp(this);
	}



	void PausedLogic::Update()
	{
		for (auto comp = mComps.begin(); comp != mComps.end(); comp++)
		{
			if ((*comp)->IsEnabled())
				(*comp)->Update();
		}
	}
	void PausedLogic::AddComp(IComp * logicComp)
	{
		mComps.remove(logicComp); // no duplicates
		mComps.push_back(logicComp);
	}
	void PausedLogic::RemoveComp(IComp * logicComp)
	{
		mComps.remove(logicComp);
	}
	void PausedLogic::ClearComps()
	{
		mComps.clear();
	}
}