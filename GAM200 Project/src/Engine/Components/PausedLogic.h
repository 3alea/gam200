#pragma once

#include "Core/AEXSystem.h"
#include "Composition/AEXComponent.h"
#include "Core/AEXSerialization.h"


namespace AEX
{
	class PausedLogicComp : public IComp
	{
		AEX_RTTI_DECL(PausedLogicComp, IComp);

	public:

		PausedLogicComp();
		~PausedLogicComp();

		void FromJson(json & value);
		void ToJson(json & value);

		virtual void Initialize();
		virtual void Update();
		virtual void Shutdown();
	};


	class PausedLogic : public ISystem
	{
		AEX_RTTI_DECL_BASE(PausedLogic);
		AEX_SINGLETON(PausedLogic);

		public:
			virtual void Update();

			// component management
			void AddComp(IComp * logicComp);
			void RemoveComp(IComp * logicComp);
			void ClearComps();
		private:
			std::list<IComp *> mComps;
	};

	#define aexPausedLogic (PausedLogic::Instance())
}