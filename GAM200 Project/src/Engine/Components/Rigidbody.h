#pragma once

#include <aexmath\aexmath.h>
#include "src\Engine\Composition\AEXComponent.h"
#include "src\Engine\Core\AEXRtti.h"


namespace AEX
{
	class ICollider; // Forward declaration

	class Rigidbody : public IComp
	{
		AEX_RTTI_DECL(Rigidbody, IComp)

	public:

		enum DynamicState {
			STATIC,
			DYNAMIC,
			KYNEMATIC
		};

		Rigidbody();
		~Rigidbody();

		AEVec3 * GetOwnerPosition();
		void ChangeState(DynamicState state);

		void Integrate(float timeStep);
		void AddForce(const AEVec2 & force);
		void Initialize();
		void Update();
		void Shutdown();
		bool OnGui();
		void SetGravity(AEVec2* newGravity);

		void FromJson(json & value);
		void ToJson(json & vlaue);


		AEVec3 * mPosition;
		AEVec3	mPrevPosition;
		AEVec2	mVelocity;
		AEVec2	mAcceleration;
		AEVec2*	mGravity;
		f32		mInvMass;
		f32		mDrag;
		DynamicState mDynamicState;
		ICollider * mCollider;

		bool mIgnoreGravity;
		bool mBounce;
	};
}
