#pragma once
#include "Graphics\Font.h"
#include "Resources\AEXResourceManager.h"
#include "Renderable.h"

struct SpriteText : public Renderable
{
	AEX_RTTI_DECL(SpriteText, Renderable)

	SpriteText(const char* fontPath = "data\\Fonts\\Algerian Regular.ttf", const char* text = "");
	~SpriteText();

	virtual void Initialize() override;
	virtual void Update() override;
	virtual void Render(Camera* cam) override;
	virtual void Shutdown() override;

	void SetText(const char* text) { strcpy_s(mText, text); }

	bool OnGui();

	void FromJson(json & value);
	void ToJson(json & value);

	char mText[1024];

protected:
	AEX::TResource<Font>* mFont;

private:
	GLuint VAO, VBO;

	AEVec2 mOffset;
	f32 mSize = 1.0f;
};