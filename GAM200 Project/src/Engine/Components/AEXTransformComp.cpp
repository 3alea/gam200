#include <iostream>
#include "../Imgui/imgui.h"
#include "Editor/Editor.h"
#include "Components\Collider.h"
#include "Components\SpriteComps.h"
#include "AEXTransformComp.h"
#include "Graphics/GfxMgr.h"
#include "AI\Path2D.h"


namespace AEX
{
	// --------------------------------------------------------------------
#pragma region// TRANSFORM2D

// --------------------------------------------------------------------
	TransformComp::TransformComp()
	{

	}
	// --------------------------------------------------------------------
	TransformComp::~TransformComp()
	{

	}
	void TransformComp::Initialize()
	{
		if (ICollider* col = mOwner->GetComp<ICollider>())
		{
			col->mOwnerPos = &this->mWorld.mTranslationZ;
		}
		if (Rigidbody* rb = mOwner->GetComp<Rigidbody>())
		{
			rb->mPosition = &this->mWorld.mTranslationZ;
		}
	}

	void TransformComp::Update()
	{
		mWorld = mLocal;
		GameObject* parent = GetOwner()->GetParent();

		/*if (Editor->imGui_selectedGO && std::string(Editor->imGui_selectedGO->GetBaseName()) == "bottom"
			&& std::string(mOwner->GetBaseName()) == "right")
		{
			if (Editorstate->IsPlaying())
				__debugbreak();
			//if (!Editor->SnappingEnabled)
			//	__debugbreak();
			if (!mOwner)
				__debugbreak();
			if (Editor->imGui_selectedGO == nullptr)
				__debugbreak();
			if (Editor->imGui_selectedGO->GetParentSpace() != mOwner->GetParentSpace())
				__debugbreak();
		}*/

		// If in the editor, if snapping mode is enabled, if a go is selected,
		// and the current go to update is in the same space as the selected go
		if (!Editorstate->IsPlaying() && Editor->SnappingEnabled && mOwner && Editor->imGui_selectedGO != nullptr
			&& Editor->imGui_selectedGO->GetParentSpace() == mOwner->GetParentSpace())
		{
			//if (std::string(Editor->imGui_selectedGO->GetBaseName()) == "bottom"
			//	&& std::string(mOwner->GetBaseName()) == "right")
			//{
				//__debugbreak();
			//}

			// Draw a circle on each of their transform's corner vertices
			if (TransformComp * tr = Editor->imGui_selectedGO->GetComp<TransformComp>())
				tr->DrawCornerCircles();

			if (Editor->imGui_selectedGO != mOwner)
			{
				//std::cout <<"SNAP DEBUG : "<< mOwner->GetBaseName() << std::endl;
				Editor->imGui_selectedGO->CheckSnapping(this);
			}
		}

		while (parent && parent->GetComp<TransformComp>())
		{
			Transform& parentTrans = parent->GetComp<TransformComp>()->mLocal;
			mWorld.mScale.x *= parentTrans.mScale.x;
			mWorld.mScale.y *= parentTrans.mScale.y;
			mWorld.mTranslation += parentTrans.mTranslation;
			mWorld.mTranslationZ += parentTrans.mTranslationZ;
			mWorld.mOrientation += parentTrans.mOrientation;

			parent = parent->GetParent();
		}

		/*if (PathComponent * path = mOwner->GetComp<PathComponent>())
		{
			AEMtx33 matrix;

			if (path->mRespectToObject)
				matrix = GetModelToWorld();
			else
				matrix = GetWorldToModel();

			FOR_EACH(it, path->GetPath().mPoints)
			{
				it->mPoint = matrix * it->mPoint;
			}
		}*/
	}

	// --------------------------------------------------------------------
	f32 TransformComp::GetRotationAngle()
	{
		return mWorld.mOrientation;
	}
	// --------------------------------------------------------------------
	AEVec2 TransformComp::GetDirection()
	{
		AEVec2 dir;
		dir.FromAngle(DegToRad(mWorld.mOrientation));
		return dir;
	}
	// --------------------------------------------------------------------
	AEVec2 TransformComp::GetPosition()
	{
		return mWorld.mTranslation;
	}
	// --------------------------------------------------------------------
	AEVec3 TransformComp::GetPosition3D()
	{
		return mWorld.mTranslationZ;
	}
	// --------------------------------------------------------------------
	AEVec2 TransformComp::GetScale()
	{
		return mWorld.mScale;

	}
	// --------------------------------------------------------------------
	void TransformComp::SetDirection(AEVec2 dir)
	{
		mLocal.mOrientation = RadToDeg(dir.GetAngle());
	}
	// --------------------------------------------------------------------
	void TransformComp::SetRotationAngle(f32 angle)
	{
		mLocal.mOrientation = angle;
	}
	// --------------------------------------------------------------------
	void TransformComp::SetPosition(const AEVec2 & pos)
	{
		mLocal.mTranslation = pos;
		mLocal.mTranslationZ.x = pos.x;
		mLocal.mTranslationZ.y = pos.y;
	}
	// --------------------------------------------------------------------
	void TransformComp::SetPosition3D(const AEVec3 & pos)
	{
		mLocal.mTranslationZ = pos;
		mLocal.mTranslation.x = pos.x;
		mLocal.mTranslation.y = pos.y;
	}
	// --------------------------------------------------------------------
	void TransformComp::SetScale(const AEVec2 & scale)
	{
		mLocal.mScale = scale;
	}

	/*void TransformComp::ToJson(json & val) 
	{
		ToJsonVec2(val["position"], mLocal.mTranslation);
		ToJsonVec2(val["scale"], mLocal.mScale);
		val["rotation"] = mLocal.mOrientation;
	}

	void TransformComp::FromJson(json & val)
	{
		// sanity
		if (auto it = val.find("position") != val.end())
			FromJsonVec2(val["position"], mLocal.mTranslation);
		if (auto it = val.find("scale") != val.end())
			FromJsonVec2(val["scale"], mLocal.mScale);
		if (auto it = val.find("rotation") != val.end())
			mLocal.mOrientation = val["rotation"];
	}*/

	// --------------------------------------------------------------------
	AEMtx33 TransformComp::GetModelToWorld()
	{
		return mLocal.GetMatrix();
	}
	// --------------------------------------------------------------------
	AEMtx33 TransformComp::GetWorldToModel()
	{
		return mLocal.GetInvMatrix();
	}
	// --------------------------------------------------------------------
	glm::mat4 TransformComp::GetModelToWorld4x4()
	{
		glm::mat4 transform = glm::mat4(1.0f); // make sure to initialize matrix to identity matrix first
		transform = glm::translate(transform, glm::vec3(mWorld.mTranslationZ.x, mWorld.mTranslationZ.y, mWorld.mTranslationZ.z));
		transform = glm::rotate(transform, mWorld.mOrientation, glm::vec3(0.0f, 0.0f, 1.0f));
		transform = glm::scale(transform, glm::vec3(mWorld.mScale.x, mWorld.mScale.y, 0.0f));
		return transform;
	}
	// --------------------------------------------------------------------
	AEMtx44 TransformComp::GetWorldToModel4x4()
	{
		AEMtx44 scale_mtx = AEMtx44::Scale(1.0f/mLocal.mScale.x, 1.0f/mLocal.mScale.y, 1.0f);
		AEMtx44 rot_mtx = AEMtx44::RotateXYZ(0, 0, -mLocal.mOrientation);
		AEMtx44 trans_mtx = AEMtx44::Translate(-mLocal.mTranslationZ.x, -mLocal.mTranslationZ.y, -mLocal.mTranslationZ.z);
		return(scale_mtx*rot_mtx*trans_mtx);
	}


	#pragma endregion

	// --------------------------------------------------------------------
	#pragma region// TRANSFORM3D

	// --------------------------------------------------------------------
	TransformComp3D::TransformComp3D()
	{}
	// --------------------------------------------------------------------
	TransformComp3D::~TransformComp3D()
	{}
	// --------------------------------------------------------------------
	AEVec3	TransformComp3D::GetRotationXYZ()
	{
		return mLocal.rot.ToEulerXYZ();
	}
	// --------------------------------------------------------------------
	AEMtx33 TransformComp3D::GetRotationMtx33()
	{
		// get a rotation matrix
		AEMtx44 mtx44 = GetRotationMtx44();
		AEMtx33 mtx33;
		// copy data
		for (u32 i = 0; i < 3; ++i)
			for (u32 j = 0; j < 3; ++j)
				mtx33.m[i][j] = mtx44.v[i*4+j];
		// return
		return mtx33;
	}
	// --------------------------------------------------------------------
	AEMtx44 TransformComp3D::GetRotationMtx44()
	{
		return mLocal.rot.ToMtx44();
	}
	// --------------------------------------------------------------------
	AEVec3	TransformComp3D::GetPosition()
	{
		return mLocal.position;
	}
	// --------------------------------------------------------------------
	AEVec3	TransformComp3D::GetScale()
	{
		return mLocal.scale;
	}
	// --------------------------------------------------------------------
	AEMtx44 TransformComp3D::GetModelToWorld()
	{
		return mLocal.GetMatrix();
	}
	// --------------------------------------------------------------------
	AEMtx44 TransformComp3D::GetWorldToModel()
	{
		return mLocal.GetInverseMatrix();
	}
	// --------------------------------------------------------------------
	void TransformComp3D::SetRotationXYZRad(f32 xRad, f32 yRad, f32 zRad)
	{
		mLocal.rot.FromEulerXYZ(xRad, yRad, zRad);
	}
	// --------------------------------------------------------------------
	void TransformComp3D::SetRotationXYZDeg(f32 xDeg, f32 yDeg, f32 zDeg)
	{
		SetRotationXYZRad(DegToRad(xDeg), DegToRad(yDeg), DegToRad(zDeg));
	}
	// --------------------------------------------------------------------
	void TransformComp3D::SetPosition(const AEVec3 & pos)
	{
		mLocal.position = pos;
	}
	// --------------------------------------------------------------------
	void TransformComp3D::SetScale(const AEVec3 & scale)
	{
		mLocal.scale = scale;
	}
	// --------------------------------------------------------------------
	void TransformComp3D::SetScale(f32 sx, f32 sy, f32 sz)
	{
		mLocal.scale = AEVec3(sx, sy, sz);
	}
	// --------------------------------------------------------------------
	void TransformComp3D::SetScale(f32 sc)
	{
		mLocal.scale = AEVec3(sc, sc, sc);
	}

	#pragma endregion

	/*json & TransformComp::operator<<(json & j) const
	{
		j["local"] << mLocal;
		return j;
	}
	void TransformComp::operator >> (json & j)
	{
		if (j.find("local") != j.end())
			j["local"] >> mLocal;
	}
	std::ostream & TransformComp::operator<<(std::ostream & o) const
	{
		json j;
		this->operator<<(j);
		return o << j;
	}*/

	bool TransformComp::OnGui()
	{
		bool changed = false;
	
		float transTemp[3]{ mLocal.mTranslationZ.x, mLocal.mTranslationZ.y, mLocal.mTranslationZ.z };
		ImGui::Text("Translation"); 
		if (ImGui::InputFloat3("Translation", transTemp))
		{
			changed = true;
			translationChanged = true;
			//Editor->mgizmos->ObjectModified = true;
		}
		else
			translationChanged = false;
			//Editor->mgizmos->ObjectModified = false;

		float scaleTemp[2]{ mLocal.mScale.x, mLocal.mScale.y };
		ImGui::Text("Scale");
		if(ImGui::InputFloat2("Scale", scaleTemp)) changed = true;

		f32 orientation[1]{ 180.0f / PI * mLocal.mOrientation };
		ImGui::Text("Rotation");
		if (ImGui::InputFloat("Rotation", orientation))
		{
			//if (*orientation == )
			mLocal.mOrientation = *orientation * PI / 180.0f;
			changed = true;
		}

		

		// This was from previous implementation of snapping
		//ImGui::NewLine();
		//if (ImGui::Checkbox("Enable Snapping", &mEnableSnapping))
		//	changed = true;

		if (changed)
			Editor->AddJsonToUndoStack();


		mLocal.mTranslationZ.x = transTemp[0];
		mLocal.mTranslationZ.y = transTemp[1];
		mLocal.mTranslationZ.z = transTemp[2];
		mLocal.mScale.x = scaleTemp[0];
		mLocal.mScale.y = scaleTemp[1];
		//mLocal.mOrientation = orientation[0];
		//ImGui::Text("Tag");
		//if (ImGui::InputText("Tag", onGuiTag, IM_ARRAYSIZE(onGuiTag))) changed = true;
		//tag = onGuiTag;
		
		return changed;
	}

	void TransformComp::FromJson(json & value)
	{
		this->IComp::FromJson(value);

		value["Translation"] >> mLocal.mTranslation;
		value["TranslationZ"] >> mLocal.mTranslationZ;
		value["Scale"] >> mLocal.mScale;
		value["Orientation"] >> mLocal.mOrientation;
		//tag = value["Tag"].get<std::string>().c_str(); // BLOWS UP
		//value["Tag"] >> tag;
	}

	void TransformComp::ToJson(json & value)
	{
		this->IComp::ToJson(value);

		value["Translation"] << mLocal.mTranslation;
		value["TranslationZ"] << mLocal.mTranslationZ;
		value["Scale"] << mLocal.mScale;
		value["Orientation"] << mLocal.mOrientation;
		//value["Tag"] << tag;
	}

	void TransformComp::DrawCornerCircles()
	{
		const AEVec3 & pos = mWorld.mTranslationZ;
		const AEVec2 & scale = mWorld.mScale;
		const f32 & rot = mWorld.mOrientation;

		SpriteComp* objSprite = GetOwner()->GetComp<SpriteComp>();
		AEVec2 texSize = objSprite ? AEVec2(static_cast<f32>(objSprite->mTex->get()->GetWidth()) / 100.0f, static_cast<f32>(objSprite->mTex->get()->GetHeight() / 100.0f)) : AEVec2(1.0f, 1.0f);
		AEVec2 mObjScale(mWorld.mScale.x * texSize.x, mWorld.mScale.y * texSize.y);

		AEMtx33 trMtx = AEMtx33::Translate(pos.x, pos.y);
		AEMtx33 rotMtx = AEMtx33::RotRad(rot);
		AEMtx33 scaleMtx = AEMtx33::Scale(mObjScale.x / 2.0f, mObjScale.y / 2.0f);

		f32 smallerScale = mObjScale.x > mObjScale.y ? mObjScale.y : mObjScale.x;
		f32 radius = smallerScale / SNAP_RADIUS_DIVISOR;

		//AEVec2 aspectRatio(mTex->get()->GetWidth() / 100.0f, mTex->get()->GetHeight() / 100.0f);


		DebugCircle mTopLeftCircle = { trMtx * rotMtx * scaleMtx * AEVec2(-1,1), radius, 15, {0,1,0,1} };
		DebugCircle mTopRightCircle = { trMtx * rotMtx * scaleMtx * AEVec2(1,1), radius, 15, {0,1,0,1} };
		DebugCircle mBottomLeftCircle = { trMtx * rotMtx * scaleMtx * AEVec2(-1,-1), radius, 15, {0,1,0,1} };
		DebugCircle mBottomRightCircle = { trMtx * rotMtx * scaleMtx * AEVec2(1,-1), radius, 15, {0,1,0,1} };

		GfxMgr->DrawCircle(mTopLeftCircle);
		GfxMgr->DrawCircle(mTopRightCircle);
		GfxMgr->DrawCircle(mBottomLeftCircle);
		GfxMgr->DrawCircle(mBottomRightCircle);
	}
}