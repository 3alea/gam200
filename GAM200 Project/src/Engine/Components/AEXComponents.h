#pragma once

// Includes
#include "AEXTransformComp.h"
#include "SpriteComps.h"
#include "ParticleEmitter.h"
#include "SpriteText.h"
#include "Camera.h"
#include "src/Engine/Audio/AudioManager.h"

// Find De-Finds (sorry for the bad pun)
#define GetTransformComp		 dynamic_cast<TransformComp*>(GetComp("AEX::TransformComp"))

#define GetTransformCompFromOwner dynamic_cast<TransformComp*>(GetOwner()->GetComp("AEX::TransformComp"))
//#define GetRigidBodyFromOwner	  dynamic_cast<RigidBody*>(GetOwner()->GetComp("RigidBody"))	
//#define GetColliderFromOwner	  dynamic_cast<Collider*>(GetOwner()->GetComp("Collider"))
//#define GetRenderableFromOwner	  dynamic_cast<Renderable*>(GetOwner()->GetComp("Renderable"))

#define GetTransByComp(comp)	 dynamic_cast<TransformComp*>(comp->GetOwner()->GetComp("AEX::TransformComp"))
//#define GetRigidBodyByComp(comp) dynamic_cast<RigidBody*>(comp->GetOwner()->GetComp("RigidBody"))
//#define GetColliderByComp(comp)  dynamic_cast<Collider*>(comp->GetOwner()->GetComp("Collider"))
//#define GetRenderableByComp(comp)  dynamic_cast<Renderable*>(comp->GetOwner()->GetComp("Renderable"))

#define GetTransLocalByComp(comp) GetTransByComp(comp)->mLocal