#pragma once
#include "Composition/AEXComponent.h"


using namespace AEX;

struct FrameBuffer;

namespace AEX
{
	class TransformComp;
}

struct Viewport
{
	Viewport(float _left = 0.0f, float _right = 0.0f, float _bot = 0.0f, float _top = 0.0f);

	Viewport& operator+=(float& rhs);
	Viewport& operator-=(float & rhs);

public:
	float left;
	float right;
	float bot;
	float top;
};

typedef bool(*ObjectPointContainFn) (const AEPoint2 &, GameObject *);

class Camera : public IComp
{
	AEX_RTTI_DECL_BASE(Camera);


public:
	Camera(FrameBuffer* pFB = nullptr) : mpFrameBuffer(pFB) {
		mOffset = AEVec2(0.0f, 0.0f);
		mRotation = 0.0f;
		mViewRectangleSize = 2.5f;
		mNearPlane = 0.1f;
		mFarPlane = 1000.0f;
		mbHasFB = pFB ? true : false;
	}
	~Camera();

	void Initialize();
	void Update();

	AEVec2 GetWorldPos() { return mWorldPos; }

	bool OnGui();

	AEMtx33 GetWorldToCam();
	AEMtx33 GetCamToWorld();

	bool PickGameObject(const AEPoint2 & pointWin, GameObject * pObj, ObjectPointContainFn selector);

	AEVec2 PointToWindow(const AEVec2 & pos);
	AEVec2 PointToWorld(const AEVec2& pos);

	// Miguel
	void FromJson(json & value);
	void ToJson(json & value);

public:
	bool mbRenderCam;
	bool mbAdjustToWindow;
	AEVec2 mOffset;
	f32 mRotation;

	f32 mNearPlane;
	f32 mFarPlane;
	f32 mViewRectangleSize;
	Viewport mCamViewport;
	s32 mRenderOrder = 0;
	FrameBuffer* mpFrameBuffer;

private:
	AEVec2 mWorldPos;
	bool mbHasFB;
};