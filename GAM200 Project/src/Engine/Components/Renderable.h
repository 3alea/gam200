#pragma once
#include "Resources/AEXResourceManager.h"
#include "Components/Camera.h"
#include "Graphics/Shader.h"
#include "Graphics/Blending.h"

using namespace AEX;

//template<typename T>
//class AEX::TResource;

struct Renderable : public IComp
{
	AEX_RTTI_DECL(Renderable, IComp);
	
	Renderable();
	virtual ~Renderable() {}

	virtual void Initialize();
	virtual void Update();
	virtual void Render(Camera* cam);
	virtual void Shutdown();

	virtual bool OnGui();

	// Commented out by Miguel because now it will use the Resource Manager
	//Shader* CreateShader(const char*, const char*);

	bool			mbVisible;		// Specifies whether the object should be rendered or not.

	f32 mColor[4];

	//Shader*	 mShader;
	AEX::TResource<Shader> * mShader;
	BlendingController* mBlending;
	bool flipX, flipY;
	AEVec2 texSize;
	AEVec2 mTexOffset;

	// Miguel
	virtual void FromJson(json & value);
	virtual void ToJson(json & value);

private:
	//AEX::TResource<Model> *mModel;

	// Warning: do not serialize
	unsigned int VAO;
	unsigned int VBO;
	unsigned int EBO;
};