#pragma once
#include "Logic.h"

using namespace AEX;

/*********************************************************/
/* WARNING! WARNING! WARNING! WARNING! WARNING! WARNING! */
/* Use this ONLY on static object, such as backgrounds!  */
/*********************************************************/
class ParallaxComp : public LogicComp
{
	AEX_RTTI_DECL(ParallaxComp, LogicComp);

public:
	ParallaxComp();

	void Initialize();
	void Update();

	bool OnGui();
	void FromJson(json & value);
	void ToJson(json & value);

public:
	AEVec2 mOffset;
	AEVec2 mMinLimits;
	AEVec2 mMaxLimits;
	bool mbLimitsOnX;
	bool mbLimitsOnY;
	AEVec2 mCenter;
	char mTargetName[256];

private:
	AEVec3 mOriginalPos;
	AEVec3 mTargetOriginalPos;

	bool mbFirstIteration;
};