#pragma once
#include "src/Engine/Composition/AEXComponent.h"

namespace AEX
{
	class IgnoreGravityEffect : public IComp
	{
		AEX_RTTI_DECL(IgnoreGravityEffect, IComp);

		public:
			virtual void Initialize();
			virtual void Shutdown();
			bool OnGui();
	};
}
