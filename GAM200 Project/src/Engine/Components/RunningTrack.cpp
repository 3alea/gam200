#include <spine\spine.h>
#include "Composition/AEXScene.h"
#include "AEXTransformComp.h"
#include <iostream>
#include "Components/Collider.h"
#include "Follow/Follower.h"
#include "RunningTrack.h"

using namespace AEX;
using namespace ImGui;

void RunningTrack::Initialize()
{
	spine = mOwner->GetComp<SpineAnimationComp>();
	if(spine != nullptr)
		myOrientation = spine->GetSkeleton()->getScaleX();
}

void RunningTrack::Update()
{
	if (!spine)
		Initialize();

	if (spine)
	{
		float tempSpeed = speed > 0 ? speed : -speed;
		spine->ChangeAnimation(0, "Move Left", true, tempSpeed);
		spine->ChangeAnimation(1, "Gear", true, tempSpeed);
		spine->GetSkeleton()->setScaleX(myOrientation);

		switch (runningAnimation)
		{
		case 1:
			spine->ChangeAnimation(2, "Runningtrack_001", true, tempSpeed);
			break;
		case 2:
			spine->ChangeAnimation(2, "Runningtrack_002", true, tempSpeed);
			break;
		case 3:
			spine->ChangeAnimation(2, "Runningtracks_003", true, tempSpeed);
			break;
		default:
			spine->ChangeAnimation(2, "Runningtracks_004", true, tempSpeed);
			break;
		}
	}
}

void RunningTrack::OnCollisionEvent(const CollisionEvent & collision)
{
	TransformComp* trans = NULL;
	if (trans = collision.otherObject->GetComp<TransformComp>())
	{
		if (collision.otherObject->GetBaseNameString() != "foot")
		{
			trans->mLocal.mTranslationZ.x += speed;

		}
		else
		{
			collision.otherObject->GetComp<Follower>()->player->GetComp<TransformComp>()->mLocal.mTranslationZ.x += speed;
		}
	}
}

bool RunningTrack::OnGui()
{
	bool isChanged = false;

	// Text for the speed
	ImGui::Text("Speed");
	// Allow user to input speed from the editor
	if (ImGui::InputFloat("Speed", &speed))
	{
		isChanged = true;
	}
	// Allow user to change between animations
	if (ImGui::InputInt("Running track animation number", &runningAnimation))
	{
		isChanged = true;
	}

	return isChanged;
}

void RunningTrack::FromJson(json & value)
{
	this->LogicComp::FromJson(value);

	value["speed"] >> speed;
	value["runningAnimation"] >> runningAnimation;
}

void RunningTrack::ToJson(json & value)
{
	this->LogicComp::ToJson(value);

	value["speed"] << speed;
	value["runningAnimation"] << runningAnimation;
}
