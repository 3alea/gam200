#pragma once

// ----------------------------------------------------------------------------
// File: ParticleSystem.h
// Project: CS230 Assignment 5
// Purpose: Declares the necessary structures for creating particle effects.
// Author: Thomas Komair
// Copyright DigiPen Institute of Technology. All rights reserved
// ----------------------------------------------------------------------------
#ifndef PARTICLE_SYSTEM_H_
#define PARTICLE_SYSTEM_H_
// ----------------------------------------------------------------------------
#include "Composition\AEXComponent.h"
#include "Graphics\GfxMgr.h"
#include "Graphics/Texture.h"


// Helpful Defines
#define MIN_VAL 0
#define MAX_VAL 1
#define START_VAL 0
#define END_VAL 1
#define MAX_PARTICLE_COUNT 5000

using namespace AEX;
// ----------------------------------------------------------------------------
// Helper functions of helper functions
AEVec2 RandomPosRect(AEVec2 rectOffset, AEVec2 rectSize, bool filled = false);
AEVec2 RandomPosCircle(AEVec2 cicrlePos, AEVec2 circleSize, f32 minAngle, f32 maxAngle, bool filled = false);
AEVec2 RandomPosLine(AEVec2 start, AEVec2 end);

// Forward declaration of the Particle structure.
struct Particle;

// ----------------------------------------------------------------------------
// Emission Properties
struct EmitterProperties
{
	EmitterProperties();
	bool OnGui();

	// Enabled bool
	bool mbParticlesEnabled;

	// max emit count, -1 means no limit. 
	int mMaxActiveCount;

	// Emit rate. emission per udpdate, -1 means it will emit until there are no longer free particles or it reaches the limit
	int mEmitRate;

	// Emitter States
	bool mbUsePhysics;				// Updates position and rotation using velocity and angular velocity
	bool mbAnimateProperties;		// Animate properties over time.
	bool mbAnimatePhxProperties;	// Animate physics properties over time. 
	bool mbLockedRotation;			// Lock rotation (should prevent any kind of orientation change (physics or animation)

	bool mFront = false;
	AEVec2 mOffset;
									// Lifetime range
									// Used to determine the lifetime of a particle randomly.
	f32		mLifeTimeRange[2];

	// Color Range
	// Second dimension is for animating the properites.
	// Used when mbAnimateProperties is true.
	f32		mColorRange[2][2][4];

	// Physics Properties. Used when mbUsePhysics = true
	// The second dimension is for animating over the lifetime of the properties. 
	// Used when both mbAnimateProperties and mbAnimatePhxProperties are true.
	AEVec2	mVelRange[2][2];		// Velocity range.
	f32		mAngVelRange[2][2];		// Angular verlocity range.
	f32		mDragRange[2][2];		// Drag range.
	f32		mAngDragRange[2][2];	// Angular drag range.

									// Transform Data.
									// Second dimension is for animating over the lifetime of the properties
									// Used when mbAnimateProperties is true.
	AEVec2 mScaleRange[2][2];
	f32	   mRotRange[2][2];

	// Emitter Type
	enum EEmitterShape { ES_POINT, ES_RECT, ES_CIRCLE, ES_LINE, ES_COUNT };
	EEmitterShape mEmitShapeType;

	// Should the shape be filled
	// Only applies when mEmitShapeType is ES_RECT or ES_CIRCLE
	bool mbEmitShapeFill;

	// rect/fill-rect data
	AEVec2 mEmitShapeOffset;
	AEVec2 mEmitShapeSize;

	// circle data: Emit Angle limits
	f32 mEmitShapeAngle[2];

	// line data
	AEVec2 mEmitShapeLineStart;	// Start position of the line emitter (with respect to the emitter transform).
	AEVec2 mEmitShapeLineEnd;	// End position of the line emitter (with respect to the emitter transform).

	void FromJson(json & value);
	void ToJson(json & value);

private:
	const char* selectedItem;
};

// ----------------------------------------------------------------------------
struct Particle
{
	// current life
	f32 mLifeTime;
	f32 mCurrentLife;

	// color
	f32		mParticleColor[4];

	// animation properties
	AEVec2  mVelRange[2];
	AEVec2	mScaleRange[2];
	f32		mDragRange[2];
	f32		mAngVelocityRange[2];
	f32		mAngDragRange[2];
	f32		mRotationRange[2];
	f32		mColorRange[2][4];

	// velocity
	AEVec2  mVelocity;
	f32		mDrag;
	f32		mAngDrag;
	f32		mAngVelocity;

	// Transform
	Transform mTransform;

	void Initialize(EmitterProperties * emitProp);
	bool Update(f32 dt, EmitterProperties * emitProp);
};

class ParticleEmitter : public Renderable
{
public:
	AEX_RTTI_DECL(ParticleEmitter, Renderable);

	~ParticleEmitter();

	// dynamic array of particles
	std::vector <Particle> mParticles;
	std::list<Particle*> mFreeParticles;
	std::list<Particle*> mActiveParticles;

	// Particle Count
	u32 mParticleCount;

	AEX::TResource<Texture> * mTex;

	// Emission properties
	EmitterProperties * mEmitProperties;

	// ------------------------------------------------------------------------
	// Renderable inherited.
	//
	virtual void Initialize();
	virtual void Update();
	virtual void Render(Camera * pCam) override;

	void FromJson(json & value);
	void ToJson(json & value);

	bool OnGui();

	// ------------------------------------------------------------------------
	// Particle System specific
	//
	ParticleEmitter(u32 particleCount = MAX_PARTICLE_COUNT);

private:
	bool CheckCameraCulling();
	void SetParticleCount(u32 particleCount);
	void Reset();
	void Step(f32 dt);
	s32  EmitParticles(s32 emit_count = 1);
};


// ----------------------------------------------------------------------------
#endif