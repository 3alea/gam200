#ifndef AEX_COLLIDER_H_
#define AEX_COLLIDER_H_

#include "Components\Rigidbody.h"
#include "src\Engine\Composition\AEXComponent.h"

namespace AEX
{
	class Rigidbody;

	class ICollider : public IComp
	{
		AEX_RTTI_DECL(ICollider, IComp);

	public:
		ICollider();

		virtual ~ICollider();

		virtual AEVec3 * GetOwnerPosition();
		void ChangeColliderState(Rigidbody::DynamicState _isDynamic);

		void Initialize();
		void Update();
		void Shutdown();

		virtual bool OnGui();

		void FromJson(json & value);
		void ToJson(json & value);

		void printCollisionGroup();

		AEVec2 mScale;
		AEVec2 mRealScale;
		f32 mOrientation;
		AEVec2 mOffset;
		AEVec3 mColPos;
		bool mIsGhost;
		Rigidbody * mRigid;
		AEVec3* mOwnerPos;
		std::string collisionGroup;
	};


	class CircleCollider : public ICollider
	{
		AEX_RTTI_DECL(CircleCollider, ICollider);

	public:
		~CircleCollider();

		bool OnGui();
	};

	class BoxCollider : public ICollider
	{
		AEX_RTTI_DECL(BoxCollider, ICollider);


	public:
		BoxCollider();
		~BoxCollider();

		bool OnGui();

		void FromJson(json & value);
		void ToJson(json & value);

		bool mIsAABB;
	};
}


#endif