#include "Graphics/FrameBuffer.h"
#include "AEXTransformComp.h"
#include "Camera.h"
#include "Graphics/GfxMgr.h"
#include "Graphics/WindowMgr.h"
#include "Imgui/imgui.h"
#include "Platform/AEXTime.h"
#include "Utilities/RNG.h"
#include "Composition/AEXScene.h"
#include "Editor//Editor.h"

#include <iostream>

using namespace AEX;


Camera::~Camera()
{
	if (mpFrameBuffer)
	{
		delete mpFrameBuffer;
		mpFrameBuffer = nullptr;
	}

	GfxMgr->mCameraList.remove(this);
}

void Camera::Initialize()
{
	mbRenderCam = true;
	mCamViewport = Viewport(0.0f, f32(WindowMgr->GetCurrentWindow()->GetWidth()), 0.0f, f32(WindowMgr->GetCurrentWindow()->GetHeight()));

	GfxMgr->mCameraList.push_back(this);
}

void Camera::Update()
{
	if (GameObject* go = GetOwner())
	{
		if (TransformComp* trans = GetOwner()->GetComp<TransformComp>())
			mWorldPos = mOffset - AEVec2(trans->mLocal.mTranslationZ.x, trans->mLocal.mTranslationZ.y);
	}
	else
		mWorldPos = mOffset;
}

bool Camera::OnGui()
{
	bool changed = true;
	if (ImGui::CollapsingHeader("Camera"))
	{
		//DeleteCompOnGui(TYPE().GetName());

		if(ImGui::Checkbox("Render Camera", &mbRenderCam)) changed = true; 

		if (ImGui::InputInt("RenderOrder", &mRenderOrder)) changed = true;

		ImGui::Text("Camera Offset");
		ImGui::InputFloat2("Camera Offset", mOffset.v);

		ImGui::Text("Camera Rotation");
		if(ImGui::InputFloat("Camera Rotation", &mRotation)) changed = true;

		ImGui::Text("Near Plane");
		if(ImGui::InputFloat("Near Plane", &mNearPlane)) changed = true;

		ImGui::Text("Far Plane");
		if(ImGui::InputFloat("Far Plane", &mFarPlane)) changed = true;

		ImGui::Text("View Rectangle size");
		if(ImGui::InputFloat("View Rectangle size", &mViewRectangleSize)) changed = true;

		ImGui::Text("Viewport");
		float camViewport[4]{ mCamViewport.left, mCamViewport.bot, mCamViewport.right, mCamViewport.top };
		if(ImGui::InputFloat4("Viewport", camViewport)) changed =  true;
		mCamViewport = Viewport(camViewport[0], camViewport[2], camViewport[1], camViewport[3]);


		if (ImGui::Checkbox("Create Framebuffer", &mbHasFB))
		{
			if (mbHasFB)
				mpFrameBuffer = new FrameBuffer();
			else
			{
				delete mpFrameBuffer;
				mpFrameBuffer = nullptr;
			}
		}

		if (mbHasFB) {
			mpFrameBuffer->OnGui();

			// show the frame buffer image
			if (mpFrameBuffer->mColorTex->mTex && mpFrameBuffer->mColorTex->mTex->get()) {
				auto tex = mpFrameBuffer->mColorTex->mTex->get();
				ImGui::Image((ImTextureID)tex->GetID(), ImVec2(256, 256), ImVec2(0, 0), ImVec2(0, -1));
			}
		}
	}

	return changed;
}

AEMtx33 Camera::GetCamToWorld()
{
	Transform cameraTrans;
	cameraTrans.mTranslation = mOffset;
	cameraTrans.mTranslationZ = AEVec3(mOffset.x, mOffset.y, 20.0f);
	//cameraTrans.mScale = a;
	cameraTrans.mOrientation = mRotation;
	AEMtx33 lol = cameraTrans.GetMatrix();
	// Return the matrix of the transform
	return lol;
}

AEMtx33 Camera::GetWorldToCam()
{
	AEMtx33 transform, scale;
	Transform cameraTrans;
	cameraTrans.mTranslation = mOffset;
	cameraTrans.mTranslationZ = AEVec3(mOffset.x, mOffset.y, 20.0f);
	//cameraTrans.mScale = a;
	cameraTrans.mOrientation = mRotation;
	// Return the inverse matrix of the transform
	return cameraTrans.GetInvMatrix();
}

bool Camera::PickGameObject(const AEPoint2 & pointWin, GameObject * pObj, ObjectPointContainFn selector)
{
	// Check if pObj exists
	if (pObj)
	{
		// If that's so, return the result of the selector
		return selector(pointWin, pObj);
	}
	else  // If not, return false
		return false;
}

AEVec2 Camera::PointToWindow(const AEVec2 & worldPoint)
{
	// Compute the size of the viewport
	AEVec2 viewportSize(mCamViewport.right - mCamViewport.left, mCamViewport.top - mCamViewport.bot);
	// Compute the window size 
	Window& wnd = *WindowMgr->GetCurrentWindow();
	int xpos, ypos;
	glfwGetWindowPos(wnd, &xpos, &ypos);
	AEVec2 windowSize(f32(wnd.GetWidth() - xpos), f32(wnd.GetHeight() - ypos));

	// Compute the center of the viewport 
	f32 viewCenterX = mCamViewport.left - windowSize.x / 2 + viewportSize.x / 2;
	f32 viewCenterY = mCamViewport.bot - windowSize.y / 2 + viewportSize.y / 2;
	AEVec2 viewCenter(viewCenterX, viewCenterY);

	// Convert it to the camera space
	AEPoint2 toCameraSpace = /*this->GetWorldToCam()*/ worldPoint;
	// Get it to Normalized Device Coordinates (NDC)
	AEPoint2 toNDC(toCameraSpace.x / windowSize.x, toCameraSpace.y / windowSize.y);
	// Convert the point into the window screen
	AEPoint2 toWindow(toNDC.x * viewportSize.x + viewCenter.x, toNDC.y * viewportSize.y + viewCenter.y);

	// Return the conversion
	return toWindow;
}

AEVec2 Camera::PointToWorld(const AEVec2 & windowPoint)
{
	// Compute the size of the viewport
	AEVec2 viewportSize(mCamViewport.right - mCamViewport.left, mCamViewport.top - mCamViewport.bot);
	// Compute the window size 
	Window& wnd = *WindowMgr->GetCurrentWindow();
	int xpos, ypos;
	glfwGetWindowPos(wnd, &xpos, &ypos);
	AEVec2 windowSize(f32(wnd.GetWidth() - xpos), f32(wnd.GetHeight() - ypos));

	AEVec2 viewportCoords(windowPoint.x - mCamViewport.left, windowPoint.y - mCamViewport.bot);
	AEVec2 ndcCoords(viewportCoords.x / viewportSize.x, viewportCoords.y / viewportSize.y);
	f32 aspectRatio = windowSize.x / windowSize.y;
	AEVec2 viewRectangleCoords(2.0f * ndcCoords.x * mViewRectangleSize * aspectRatio, 2.0f * ndcCoords.y * mViewRectangleSize);

	/*std::cout << "windowPoint: (" << windowPoint.x << ", " << windowPoint.y << std::endl;
	std::cout << "mCamViewport: (" << mCamViewport.left << ", " << mCamViewport.bot << std::endl;
	std::cout << "viewportCoords: (" << viewportCoords.x << ", " << viewportCoords.y << std::endl;
	std::cout << "viewportSize: (" << viewportSize.x << ", " << viewportSize.y << std::endl;
	std::cout << "ndcCoords: (" << ndcCoords.x << ", " << ndcCoords.y << std::endl << std::endl;*/

	// Multiply the result with the camera matrix
	AEMtx33 mtx = this->GetWorldToCam();

	// Return the result of the conversion
	return mtx * viewRectangleCoords;
}

Viewport::Viewport(float _left, float _right, float _bot, float _top)
{
	left = _left;
	right = _right;
	bot = _bot;
	top = _top;
}

Viewport & Viewport::operator+=(float & rhs)
{
	left += rhs;
	right -= rhs;
	bot += rhs;
	top -= rhs;

	return *this;
}

Viewport & Viewport::operator-=(float & rhs)
{
	left -= rhs;
	right += rhs;
	bot -= rhs;
	top += rhs;

	return *this;
}



void Camera::FromJson(json & value)
{
	this->IComp::FromJson(value);
	
	value["RenderCamera"] >> mbRenderCam;
	value["Adjust To Window"] >> mbAdjustToWindow;
	value["Position"] >> mOffset;
	value["Rotation"] >> mRotation;
	value["NearPlane"] >> mNearPlane;
	value["FarPlane"] >> mFarPlane;
	value["ViewRectangleSize"] >> mViewRectangleSize;
	value["RenderOrder"] >> mRenderOrder;
	
	value["HasFB"] >> mbHasFB;
	if (mbHasFB)
	{
		if (mpFrameBuffer) delete mpFrameBuffer;
		mpFrameBuffer = new FrameBuffer();
	}
	else
		mpFrameBuffer = nullptr;
	
	json & viewportVal = value["Viewport"];
	viewportVal["Left"] >> mCamViewport.left;
	viewportVal["Right"] >> mCamViewport.right;
	viewportVal["Bottom"] >> mCamViewport.bot;
	viewportVal["Top"] >> mCamViewport.top;
	
}

void Camera::ToJson(json & value)
{
	this->IComp::ToJson(value);

	value["RenderCamera"] = mbRenderCam;
	value["UseFramebuffer"] << mbHasFB;
	value["Adjust To Window"] << mbAdjustToWindow;
	value["Position"] << mOffset;
	value["Rotation"] = mRotation;
	value["NearPlane"] = mNearPlane;
	value["FarPlane"] = mFarPlane;
	value["ViewRectangleSize"] = mViewRectangleSize;
	value["HasFB"] = mbHasFB;
	value["RenderOrder"] << mRenderOrder;

	json viewportVal;
	viewportVal["Left"] = mCamViewport.left;
	viewportVal["Right"] = mCamViewport.right;
	viewportVal["Bottom"] = mCamViewport.bot;
	viewportVal["Top"] = mCamViewport.top;
	value["Viewport"] = viewportVal;
}

/*try {
			this->Renderable::FromJson(value);

			std::string metaFile;
			value["Meta File"] >> metaFile;

			fs::path metaPath(metaFile);
			json val;

			if (!fs::exists(metaPath))
			{
				metaPath.replace_extension(".png");

				if (!fs::exists(metaPath))
					metaPath.replace_extension(".jpg");

				mTex = aexResourceMgr->getResource<Texture>(metaPath.string().c_str());
				mTex->ToJson(val);
				return;
			}

			LoadFromFile(val, metaFile);

			std::string path;
			val["Relative Path"] >> path;

			mTex = aexResourceMgr->getResource<Texture>(path.c_str());
			mTex->get()->FromJson(value);

			} CATCH_FS_ERROR*/