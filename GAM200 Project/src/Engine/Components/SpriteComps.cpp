#include <iostream>
#include <experimental/filesystem>
#include "Imgui/imgui.h"
#include "AEXComponents.h"
#include "Graphics/SpriteAnimation.h"
#include "Graphics/Texture.h"
#include "Graphics/GfxMgr.h"
#include "Resources/AEXResourceManager.h"
#include "SpriteComps.h"


namespace AEX
{
	namespace fs = std::experimental::filesystem;

	struct SpriteAnim;
	
	/*const char* GetWrapType(TexWrappingMethod method)
	{
		switch (method)
		{
		case eRepeat:
			return "Repeat"

		case eRepeat_Mirror:
			return "Mirror Repeat";

		case eClamp:
			return "Clamp"; 

		case eBorderCol:
			return "Border Color";
		}
	}*/

	void TexWrapOnGui(const char* imguiName, const char* currentItem, const char** borderModes_items, Texture* pTex, u32 val)
	{
		static ImGuiComboFlags borderModeFlags = 0;

		if (ImGui::BeginCombo(imguiName, currentItem, borderModeFlags)) // The second parameter is the label previewed before opening the combo.
		{

			int n;
			for (n = 0; n < IM_ARRAYSIZE(borderModes_items); n++)
			{
				bool is_selected = (currentItem == borderModes_items[n]);
				if (ImGui::Selectable(borderModes_items[n], is_selected))
				{
					currentItem = borderModes_items[n];

					switch (n)
					{
					case 0:
						pTex->SetWrapMethod(val, eRepeat);
						break;
					case 1:
						pTex->SetWrapMethod(val, eRepeat_Mirror);
						break;
					case 2:
						pTex->SetWrapMethod(val, eClamp);
						break;
					case 3:
						pTex->SetWrapMethod(val, eBorderCol);
						break;
					}
				}
				if (is_selected)
					ImGui::SetItemDefaultFocus();   // Set the initial focus when opening the combo (scrolling + for keyboard navigation support in the upcoming navigation branch)
			}

			ImGui::EndCombo();
		}
	}

	SpriteComp::SpriteComp(bool shouldPush) : mbShouldPush(shouldPush), mTex(nullptr)
	{
	}

	SpriteComp::~SpriteComp()
	{
		Shutdown();
	}

	//-----------------------------------------------------------------------------------
	// Sprite
	//-----------------------------------------------------------------------------------
	void SpriteComp::Initialize()
	{
		//std::cout << "Initializer of spritecomp" << std::endl;

		this->Renderable::Initialize();
		if (mTex == nullptr)
		{
			//std::cout << "mTex is nullptr" << std::endl;
			mTex = aexResourceMgr->getResource<Texture>("data\\Textures\\Default.png");
		}
		//std::cout << "mTex = " << mTex << std::endl;
		
		if (mbShouldPush)
			GfxMgr->mRenderableList.push_back(this);
	}
	
	void SpriteComp::Update()
	{
		mTex->get()->SetWrappable(0, false);
	}
	
	void SpriteComp::Render(Camera * cam)
	{
		if (mTex->get() == NULL)
		{
			//std::cout << "The actual texture is NULL" << std::endl;
			return;
		}
		if (mBlending)
			mBlending->SetBlendAttributes();
		glBindTexture(GL_TEXTURE_2D, mTex->get()->GetID());
	
		mTex->get()->CheckTextureMethods();

		// create transformations
		glm::mat4 transform = mbShouldPush ? GetTransformCompFromOwner->GetModelToWorld4x4() : glm::mat4(1.0f);
	
		// get matrix's uniform location and set matrix
		Shader * actualShader = mShader->get();

		if (actualShader != nullptr)
		{
			AEVec2 aspectRatio = mbShouldPush ? AEVec2(mTex->get()->GetWidth() / 100.0f, mTex->get()->GetHeight() / 100.0f) : AEVec2(1.0f, 1.0f);
			//f32 aspectRatio = (f32)mTex->get()->GetHeight() / (f32)mTex->get()->GetWidth();
			actualShader->use();
			actualShader->setVec4("color", mColor[0], mColor[1], mColor[2], mColor[3]);
			actualShader->setMat3("texTransform", glm::mat3(1.0f));
			actualShader->setMat4(1, glm::value_ptr(transform));
			actualShader->setVec2("aspectRatio", aspectRatio);
			actualShader->setVec2("texOffset", mTexOffset);

		}
	
		Renderable::Render(cam);
	}
	
	void SpriteComp::Shutdown()
	{
		Renderable::Shutdown();
		//delete mSprite;
	}
	
	bool SpriteComp::OnGui()
	{

		bool changed = false;
	
		// Miguel
		static std::string actualName = "Default.png";
		static bool isPathShown = false;
		std::string previewName;

		if (ImGui::Checkbox("Show Paths", &isPathShown)) changed = true;

		// To solve bug where currently selected texture wouldn't change
		// to only the name as soon as you unclicked the checkbox
		if (!isPathShown)
			previewName = aexResourceMgr->GetNameWithExtension(actualName);
		else
			previewName = actualName;

		if (ImGui::BeginCombo("Texture", previewName.c_str()))
		{
			try {
				fs::path textureDir = fs::path("data\\Textures");

				int count = 0;
				for (auto dirIt = fs::recursive_directory_iterator(textureDir);
					dirIt != fs::recursive_directory_iterator();
					dirIt++, ++count)
				{
					//if (dirIt->is_directory())
					//continue;
					if (fs::is_directory(dirIt->path()))
						continue;

					if (dirIt->path().extension().string() == ".meta")
						continue;

					std::string currentFileName;

					if (isPathShown)
						currentFileName = dirIt->path().relative_path().string();
					else
						currentFileName = dirIt->path().filename().string();

					if (ImGui::Selectable((currentFileName + "##" + std::to_string(count)).c_str()))
					{
						actualName = dirIt->path().relative_path().string().c_str();
						mTex = aexResourceMgr->getResource<Texture>(actualName.c_str());
					}
				}
			} CATCH_FS_ERROR

				ImGui::EndCombo();
		}

		ImGui::Text("Flip X");
		ImGui::Checkbox("Flip X", &flipX);

		ImGui::Text("Flip Y");
		ImGui::Checkbox("Flip Y", &flipY);

		ImGui::Text("VertexColor");
		float col[4] = { mColor[0], mColor[1], mColor[2], mColor[3] };
		if (ImGui::ColorEdit4("VertexColor", col)) changed = true;
		for (int i = 0; i < 4; i++)
			mColor[i] = col[i];

		ImGui::Text("Texture Size");
		float texz[2] = { texSize.x, texSize.y };
		ImGui::InputFloat2("Texture Size", texz);
		texSize.x = texz[0];
		texSize.y = texz[1];

		//std::cout << "x : " << texSize.x << " y : " << texSize.y;

#pragma region BORDER MODES
		Texture* tex = mTex->get();
		if (tex)
		{
			/*eRepeat,		// Repeats the texture image
			eRepeat_Mirror, // Same as 'eRepeat' but mirrors the image with each repeat
			eClamp,			// Clamps the coordinates between 0 and 1. The result is that higher coordinates become clamped to the edge, resulting in a straight edge pattern
			eBorderCol		// Coordinates outside the range are now given a user-specified border color.*/
			static const char* borderModes_items_x[] = { "Repeat X" };
			static const char* borderModes_items_y[] = { "Repeat Y", "Mirror Repeat Y", "Clamp Y", "Border Color Y" };

			static const u32 x_param = 0u;
			static const u32 y_param = 1u;

			bool* wrappable = tex->GetWrappable();
			TexWrappingMethod* wrapMethods = tex->GetWrapMethod();

			static const char* borderMode_item_current_x = borderModes_items_x[wrapMethods[x_param]];
			static const char* borderMode_item_current_y = borderModes_items_y[wrapMethods[y_param]];

			ImGui::Text("Border Modes");


			if (ImGui::Checkbox("Border on x", &wrappable[x_param]))
				tex->SetWrappable(x_param, wrappable[x_param]);

			if (wrappable[x_param])
				TexWrapOnGui("Border Method x", borderMode_item_current_x, borderModes_items_x, tex, x_param);

			if (ImGui::Checkbox("Border on y", &wrappable[y_param]))
				tex->SetWrappable(y_param, wrappable[y_param]);

			if (wrappable[y_param])
				TexWrapOnGui("Border Method y", borderMode_item_current_y, borderModes_items_y, tex, y_param);
		}
#pragma endregion

		static const char* SrcFactor_item_current = "One"; // Here our selections are a single pointer stored outside the object.
		static const char* DestFactor_item_current = "One";          
		static const char* BlendEq_item_current = "Add";           
		changed = mBlending->OnGui(SrcFactor_item_current, DestFactor_item_current, BlendEq_item_current);

		return changed;
	}


	void SpriteComp::FromJson(json & value)
	{
		try {
			this->Renderable::FromJson(value);

			std::string metaFile;
			value["Meta File"] >> metaFile;

			fs::path metaPath(metaFile);
			json val;

			if (!fs::exists(metaPath))
			{
				metaPath.replace_extension(".png");

				if (!fs::exists(metaPath))
					metaPath.replace_extension(".jpg");

				mTex = aexResourceMgr->getResource<Texture>(metaPath.string().c_str());
				mTex->ToJson(val);
				return;
			}

			LoadFromFile(val, metaFile);

			std::string path;
			val["Relative Path"] >> path;

			mTex = aexResourceMgr->getResource<Texture>(path.c_str());
			mTex->get()->FromJson(value);

		} CATCH_FS_ERROR
	}
	void SpriteComp::ToJson(json & value)
	{
		//std::cout << "ToJson of spritecomp" << std::endl;
		this->Renderable::ToJson(value);
		mTex->ToJson(value); 
		mTex->get()->ToJson(value);


	}

	
	//-----------------------------------------------------------------------------------
	// SpriteAnimation
	//-----------------------------------------------------------------------------------
	SpriteAnimationComp::SpriteAnimationComp() : mSprite(nullptr) {}

	SpriteAnimationComp::~SpriteAnimationComp()
	{
		Shutdown();
	}

	void SpriteAnimationComp::Initialize()
	{
		Renderable::Initialize();
	
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// Create a default texture
		if (mSprite == nullptr)
			mSprite = aexResourceMgr->getResource<SpriteAnim>("data\\Animations\\dotto.anim");

		if (mSprite->get() != NULL)
			mSprite->get()->PlayAnimation();

		GfxMgr->mRenderableList.push_back(this);
	}
	
	void SpriteAnimationComp::Update()
	{
	}
	
	void SpriteAnimationComp::Render(Camera * cam)
	{
		if (mSprite->get() == nullptr)
			return;

		//mBlending->SetBlendAttributes();
	
		mSprite->get()->UpdateAnimation();
		glBindTexture(GL_TEXTURE_2D, mSprite->get()->mpSheet->mTexture->GetID());
	
		// create transformations
		glm::mat4 transform = GetTransformCompFromOwner->GetModelToWorld4x4();
	
		Shader * actualShader = mShader->get();
		if (actualShader != nullptr)
		{
			// get matrix's uniform location and set matrix
			actualShader->use();
			actualShader->setVec4("color", mColor[0], mColor[1], mColor[2], mColor[3]);
			actualShader->setMat3("texTransform", mSprite->get()->GetFrame());
			actualShader->setMat4(1, glm::value_ptr(transform));
		}
	
		Renderable::Render(cam);
	}
	
	void SpriteAnimationComp::Shutdown()
	{
		Renderable::Shutdown();
		//delete mSprite;

	}


	void SpriteAnimationComp::FromJson(json & value)
	{
		try {
			this->Renderable::FromJson(value);

			std::string metaFile;
			value["Meta File"] >> metaFile;

			fs::path metaPath(metaFile);
			json val;

			if (!fs::exists(metaPath))
			{
				metaPath.replace_extension(".anim");
				mSprite = aexResourceMgr->getResource<SpriteAnim>(metaPath.string().c_str());
				mSprite->ToJson(val);
				return;
			}
				
			LoadFromFile(val, metaFile);

			std::string path;
			val["Relative Path"] >> path;

			mSprite = aexResourceMgr->getResource<SpriteAnim>(path.c_str());

		} CATCH_FS_ERROR
	}

	void SpriteAnimationComp::ToJson(json & value)
	{
		this->Renderable::ToJson(value);
		mSprite->ToJson(value);
		value["RTTI Type"] = GetType().GetName();
	}

	
	bool SpriteAnimationComp::OnGui()
	{
		bool changed = false;
		

		if (mSprite->get() == NULL)
		{
			return changed;
		}

			ImGui::Text("Is Visible");
			ImGui::Checkbox("Is Visible", &mbVisible);

			bool isPlaying = mSprite->get()->mbAnimPlaying;
			ImGui::Text("Playing");
			ImGui::Checkbox("Playing", &isPlaying);
	
			if (isPlaying)
				mSprite->get()->PlayAnimation();
			else
				mSprite->get()->PauseAnimation();
	
			ImGui::Text("Looping");
			ImGui::Checkbox("Looping", &mSprite->get()->mbAnimLoop);
	
			ImGui::Text("Ping-Pong");
			ImGui::Checkbox("Ping-Pong", &mSprite->get()->mbAnimPingPong);

			ImGui::Text("Flip X");
			ImGui::Checkbox("Flip X", &flipX);

			ImGui::Text("Flip Y");
			ImGui::Checkbox("Flip Y", &flipY);

			ImGui::Text("Texture Size");
			ImGui::InputFloat2("Texture Size", texSize.v);
	
			ImGui::Text("VertexColor");
			float col[4] = { mColor[0], mColor[1], mColor[2], mColor[3] };
			ImGui::ColorEdit4("VertexColor", col);
			for (int i = 0; i < 4; i++)
				mColor[i] = col[i];
	
			ImGui::Text("Anim Speed");
			ImGui::InputFloat("Anim Speed", &mSprite->get()->mAnimSpeed);
	
			ImGui::Text("Current frame");
			ImGui::InputInt("Current frame", &mSprite->get()->mAnimCurrentFrame);
			if (mSprite->get()->mAnimCurrentFrame < 0)
				mSprite->get()->mAnimCurrentFrame = mSprite->get()->mpSheet->mFrameCount - 1;
			else if (u32(mSprite->get()->mAnimCurrentFrame) > mSprite->get()->mpSheet->mFrameCount - 1)
				mSprite->get()->mAnimCurrentFrame = 0;
	
			static const char* SrcFactor_item_current = "One";            // Here our selection is a single pointer stored outside the object.
			static const char* DestFactor_item_current = "One";            // Here our selection is a single pointer stored outside the object.
			static const char* BlendEq_item_current = "Add";            // Here our selection is a single pointer stored outside the object.
			mBlending->OnGui(SrcFactor_item_current, DestFactor_item_current, BlendEq_item_current);

		// Miguel
		static std::string actualName = "dotto.anim";
		static bool isPathShown = false;
		std::string previewName;

		if(ImGui::Checkbox("Show Paths", &isPathShown))  changed = true;

		// To solve bug where currently selected texture wouldn't change
		// to only the name as soon as you unclicked the checkbox
		if (!isPathShown)
			previewName = aexResourceMgr->GetNameWithExtension(actualName);
		else
			previewName = actualName;

		if (ImGui::BeginCombo("SpriteAnim", previewName.c_str()))
		{
			//namespace fs = std::filesystem;
			namespace fs = std::experimental::filesystem;

			fs::path spriteDir = fs::path("data\\Animations");

			int count = 0;
			for (auto dirIt = fs::recursive_directory_iterator(spriteDir);
				dirIt != fs::recursive_directory_iterator();
				dirIt++, ++count)
			{
				//if (dirIt->is_directory())
				//continue;
				if (fs::is_directory(dirIt->path()))
					continue;

				if (dirIt->path().extension().string() == ".meta")
					continue;

				std::string currentFileName;

				if (isPathShown)
					currentFileName = dirIt->path().relative_path().string();
				else
					currentFileName = dirIt->path().filename().string();

				if (ImGui::Selectable((currentFileName + "##" + std::to_string(count)).c_str()))
				{
					actualName = dirIt->path().relative_path().string().c_str();
					mSprite = aexResourceMgr->getResource<SpriteAnim>(actualName.c_str());
				}
			}

			ImGui::EndCombo();
		}
		
	return changed;
	}

}