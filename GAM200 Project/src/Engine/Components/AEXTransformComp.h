#ifndef AEX_TRANSFORM_COMP
#define AEX_TRANSFORM_COMP

#include <aexmath\AEXMath.h>
#include "Composition\AEXComponent.h"
#include "Core/AEXSerialization.h"

#include <./extern/glm/glm.hpp>
#include <./extern/glm/gtc/matrix_transform.hpp>
#include <./extern/glm/gtc/type_ptr.hpp>


namespace AEX
{
	const f32 SNAP_RADIUS_DIVISOR = 8.0f;

	class TransformComp : public IComp
	{
		AEX_RTTI_DECL(TransformComp, IComp);

	public:
		TransformComp();
		virtual ~TransformComp() override;

		void Initialize() override;
		void Update() override;

		f32 GetRotationAngle();
		AEVec2 GetDirection();
		AEVec2 GetPosition();
		AEVec3 GetPosition3D();
		AEVec2 GetScale();
		AEMtx33 GetModelToWorld();
		AEMtx33 GetWorldToModel();
		glm::mat4 GetModelToWorld4x4();
		AEMtx44 GetWorldToModel4x4();

		void SetDirection(AEVec2 dir);
		void SetRotationAngle(f32 angle);
		void SetPosition(const AEVec2 & pos);
		void SetPosition3D(const AEVec3 & posZorder);
		void SetScale(const AEVec2 & scale);

		//json& operator<<(json&j)  const;
		//void operator>>(json&j);
		//std::ostream& operator<<(std::ostream & o) const;

		// Miguel
		virtual void FromJson(json & value);
		virtual void ToJson(json & value);

		//const char* tag = "None";

		//char onGuiTag[256] = "None";

		void DrawCornerCircles();

		// It doesn't compileee, but whyyyy???
		//AEX::DebugCircle mTopLeftCircle;
		//DebugCircle mTopRightCircle;
		//DebugCircle mBottomLeftCircle;
		//DebugCircle mBottomRightCircle;
		//bool mEnableSnapping = false;

		bool OnGui();

		// bool added by Miguel for path editing
		bool translationChanged = false;
		AEVec2 transDisplacement = AEVec2(0,0);
		
		// Data
	public:
		Transform mWorld;
		Transform mLocal;
	};


	class TransformComp3D : public IComp
	{
		AEX_RTTI_DECL(TransformComp3D, IComp);

	public:
		TransformComp3D();
		virtual ~TransformComp3D();

		AEVec3	GetRotationXYZ();
		AEMtx33 GetRotationMtx33();
		AEMtx44 GetRotationMtx44();
		AEVec3	GetPosition();
		AEVec3	GetScale();
		AEMtx44 GetModelToWorld();
		AEMtx44 GetWorldToModel();

		void SetRotationXYZRad(f32 xRad, f32 yRad, f32 zRad);
		void SetRotationXYZDeg(f32 xDeg, f32 yDeg, f32 zDeg);
		void SetPosition(const AEVec3 & pos);
		void SetScale(const AEVec3 & scale);
		void SetScale(f32 sx, f32 sy, f32 sz);
		void SetScale(f32 sc);
		
		// Data
	public:
		Transform3D mLocal;
	};
}

#endif