#pragma once
#include "../Composition/AEXGameObject.h"
#include "AEXComponents.h"
#include "Renderable.h"
#include "Graphics\Vertex.h"
#include "Graphics\Model.h"
#include "Imgui\imgui.h"
#include "Resources\AEXResourceManager.h"
#include "Graphics\SpineTexture.h"

namespace spine {
	class Skeleton;
	class AnimationState;
	class Bone;
	enum EventType;
	class TrackEntry;
	class Event;
	class SkeletonClipping;
}

class SpineAnimationComp : public Renderable
{
public:
	AEX_RTTI_DECL(SpineAnimationComp, Renderable);

	SpineAnimationComp() :
		Renderable(),
		mSkeleton(0),
		mAnimationState(0),
		mSpineData(0),
		mTransform(0),
		mClipper()
	{
		for (unsigned i = 0u; i < 4u; i++)
			mColor[i] = 1.0f;
	}

	~SpineAnimationComp();

	AEX::TResource<SpineData> * mSpineData;

	//SpineData* mSpineData;
	AEVec2 Size = AEVec2(0.01f, 0.01f);

	// Reloads SpineData, refreshing all changes made to the data
	void ReloadSpineData();

	// Sets all data necessary for the SpineComp
	virtual void Initialize();

	// Updates the animation and computes world transforms
	virtual void Update();

	// Debug render drawing the skeleton bones and lines connecting them
	void DebugRender();

	// Render function rendering the whole Spine animation. Not implemented yet due to: missing Mesh
	void Render(Camera* cam);

	void Shutdown();

	// Shutdown function will be the same as derived Shutdown function (for now)
	// void Shutdown();

	void ShowBoneData(spine::Bone* bone);

	// OnGui function will do nothing (for now)
	bool OnGui();

	spine::Skeleton * GetSkeleton() { return mSkeleton; }
	spine::AnimationState * GetAnimState(){ return mAnimationState; }
	spine::Bone * GetBone(const char * boneName);

	// Checks if the current animation is the one passed
	bool CheckCurrentAnimationAtPostion(const char * animationName, const int pos);

	// Change animation function changing animation if it exists
	void ChangeAnimation(const unsigned int position, const spine::String& name, bool shouldLoop, float speed = 0.f, float mergeSpeed = 0.f);

	// Add an empty animation to a track to end it
	void EmptyAnimationAtPosition(const unsigned int position);

	// Clear animation at position to remove it from the animation
	void ClearAnimationAtPosition(const unsigned int position);

	// Clear all animations currently set
	void ClearAllAnimations();

	// Pause the animation if true is passed. Play it if false is passed.
	void SetPauseState(bool isPaused);

	// Set the new dt to use for the animation (0 will reset the animation to the beginning)
	void SetDt(float newDt);

	// Offset to be added to the transform
	AEVec2 offset;

	// If offset is changed
	bool offsetChanged;

	// FromJson/ToJson will be implemented in the future. For now, the json loading/saving will be done in the Spine demo
	virtual void FromJson(json & value);
	virtual void ToJson(json & value);

	// Speed of animation when going backwards
	float goBackSpeed;

	// Boolean determining if an animation should play backwards
	bool playBackwards = false;

	bool isBackwardsComplete = false;

	// Rotation for the car
	float carRotation = 0.f;

	// Starting time of the spine animation
	float startTime = 0.f;

	float speed = 1.f;

	void SetSkeletonSize(const AEVec2& size);

private:
	spine::Skeleton* mSkeleton = 0;
	spine::AnimationState* mAnimationState = 0;

	// The animation's transform
	TransformComp* mTransform = 0;

	// Model is still not included until now. We will needed in order to manipulate each spine part.
	Model* mSpineModel = 0;

	// Vertices of the meshes
	spine::Vector<Vertex> mVertices;

	// Indeces defining the two triangles composing every mesh. Correspond to the vertices of these meshes.
	unsigned short quadIndices[6] = {0, 1, 2, 2, 3, 0};

	// Vertex positions, going index by index
	float* mVertexPositions;

	// Skeleton Clipper of this spine animation
	spine::SkeletonClipping* mClipper;

	// Backwards time, moves time backwards
	float backwardsTime = 0.f;

	// Added by Miguel for pause animation
	bool mPaused = false;
	float dt = 0.0f;
	bool mChangedDt = false;

	// Just to make sure
	bool superInitialized = false;
};

// Function used for custom behaviour when certain events happen
void callback(spine::AnimationState* state, spine::EventType type, spine::TrackEntry* entry, spine::Event* event);