#pragma once

#include "Composition\AEXComponent.h"
#include "src/Engine/Events/event_dispatcher.hh"

namespace AEX
{
	class LogicComp; // Forward declaration

	struct LogicCompEventHandler : public EventHandler
	{
		LogicCompEventHandler(LogicComp* LComp);

		LogicComp* object;

	};

	struct CollisionEvent : public Event
	{
		CollisionEvent(GameObject* obj1 = NULL, GameObject* obj2 = NULL);

		GameObject* thisObject;
		GameObject* otherObject;
	};

	struct CollisionStarted : public Event
	{
		CollisionStarted(GameObject* obj1 = NULL, GameObject* obj2 = NULL);

		GameObject* thisObject;
		GameObject* otherObject;
	};

	struct CollisionEnded : public Event
	{
		CollisionEnded(GameObject* obj1 = NULL, GameObject* obj2 = NULL);

		GameObject* thisObject;
		GameObject* otherObject;
	};

	class LogicComp : public virtual IComp, public Listener
	{
		AEX_RTTI_DECL(LogicComp, IComp);
	public:
		LogicComp();

		void FromJson(json & value);
		void ToJson(json & value);

		virtual void Initialize();
		virtual void Update() {}
		virtual void Shutdown();

		// Checks if the collision is with this object and calls private method
		void OnCollision(const CollisionEvent& collision);
		void collisionStarted(const CollisionStarted& collision);
		void collisionEnded(const CollisionEnded& collision);

		virtual void handle_event(const Event& event);

		virtual ~LogicComp();
		
		private:
			// Overridden by childs
			virtual void OnCollisionEvent(const CollisionEvent& collision) {}
			virtual void OnCollisionStarted(const CollisionStarted& collision) {}
			virtual void OnCollisionEnded(const CollisionEnded& collision) {}


			LogicCompEventHandler event_handler;
	};

	class Logic :public ISystem
	{
		AEX_RTTI_DECL(Logic, ISystem);
		AEX_SINGLETON(Logic);

	public:
		virtual void Update();

		// component management
		void AddComp(IComp * logicComp);
		void RemoveComp(IComp * logicComp);
		void ClearComps();
	private:
		std::list<IComp *> mComps;
	};

	#define aexLogic (Logic::Instance())
}