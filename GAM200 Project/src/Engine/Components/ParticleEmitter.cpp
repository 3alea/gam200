// ----------------------------------------------------------------------------
//	Copyright (C)DigiPen Institute of Technology.
//	Reproduction or disclosure of this file or its contents without the prior 
//	written consent of DigiPen Institute of Technology is prohibited.
//	
//	File Name:		ParticleSystem.cpp
//	Purpose:		This C++ file contains all the functions for the Particle
//					System.
//	Project:		CS230_alejandro.balea_5
//	Author:			Alejandro Balea Moreno - alejandro.balea - 540002118
// ----------------------------------------------------------------------------
#include <experimental/filesystem>
#include "Graphics/WindowMgr.h"
#include "ParticleEmitter.h"
#include "AEXComponents.h"
#include "Platform/AEXTime.h"
#include "Imgui/imgui.h"
#include "Math/Collisions.h"
#include "Editor/Editor.h"

// ----------------------------------------------------------------------------
// LERP
#define LERP(min, max, tn) ((min) + ((max)-(min)) * (tn));
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// HELPER FUNCTION PROTOTYPES
bool RandBool();
// ----------------------------------------------------------------------------

void DoubleArrayOnGui(f32* floatArray[2][2], const char* labelName, f32 min = -10000.f, f32 max = 10000.f)
{
	f32 orderedArray[]{ *floatArray[0][0], *floatArray[0][1], *floatArray[1][0], *floatArray[1][1] };
	ImGui::DragFloat4(labelName, orderedArray, 1.0f, -10000.f, 10000.f);

	for (int i = 0; i < 4; i++)
		*floatArray[i / 2][i % 2] = orderedArray[i];
}

#pragma region PARTICLE
/**************************************************************************/
/*!
\fn
Initialize

\brief
Initializes a particle's update data using the provided emitter properties.

\param ep
The emitter properties to initialize the particle with.
*/
/**************************************************************************/
void Particle::Initialize(EmitterProperties * ep)
{
	// Compute the lifetime as a random float between the LifeTime range from ep
	mLifeTime = AEX::RandFloat(ep->mLifeTimeRange[MIN_VAL], ep->mLifeTimeRange[MAX_VAL]);

	// Compute both scale ranges as a random Vector between the scale ranges from ep
	mScaleRange[START_VAL] = AEVec2::Random(AEVec2(ep->mScaleRange[START_VAL][MIN_VAL].x, ep->mScaleRange[START_VAL][MIN_VAL].y), AEVec2(ep->mScaleRange[START_VAL][MAX_VAL].x, ep->mScaleRange[START_VAL][MAX_VAL].y));
	mScaleRange[END_VAL] = AEVec2::Random(AEVec2(ep->mScaleRange[END_VAL][MIN_VAL].x, ep->mScaleRange[END_VAL][MIN_VAL].y), AEVec2(ep->mScaleRange[END_VAL][MAX_VAL].x, ep->mScaleRange[END_VAL][MAX_VAL].y));

	// Compute both rotation ranges as a random float between the the rotation ranges from ep
	mRotationRange[START_VAL] = AEX::RandFloat(ep->mRotRange[START_VAL][MIN_VAL], ep->mRotRange[START_VAL][MAX_VAL]);
	mRotationRange[END_VAL] = AEX::RandFloat(ep->mRotRange[END_VAL][MIN_VAL], ep->mRotRange[END_VAL][MAX_VAL]);

	// Create two random values between the intervals 0 and 1 to use them in AEGfxColInterp
	f32 normIntervalStart = AEX::RandFloat(0.0f, 1.0f);
	f32 normIntervalEnd = AEX::RandFloat(0.0f, 1.0f);
	

	for (unsigned i = 0u; i < 4; i++)
	{
		//mColorRange[START_VAL] = Lerp(ep->mColorRange[START_VAL][MIN_VAL], ep->mColorRange[START_VAL][MAX_VAL], normIntervalStart);
		//mColorRange[END_VAL] = Lerp(ep->mColorRange[END_VAL][MIN_VAL], ep->mColorRange[END_VAL][MAX_VAL], normIntervalEnd);

		mColorRange[START_VAL][i] = AEX::RandFloat(ep->mColorRange[START_VAL][MIN_VAL][i], ep->mColorRange[START_VAL][MAX_VAL][i]);
		mColorRange[END_VAL][i] = AEX::RandFloat(ep->mColorRange[END_VAL][MIN_VAL][i], ep->mColorRange[END_VAL][MAX_VAL][i]);
	}


	// Compute both velocity ranges as a random Vector between the velocity ranges from ep
	mVelRange[START_VAL] = AEVec2::Random(AEVec2(ep->mVelRange[START_VAL][MIN_VAL].x, ep->mVelRange[START_VAL][MIN_VAL].y), AEVec2(ep->mVelRange[START_VAL][MAX_VAL].x, ep->mVelRange[START_VAL][MAX_VAL].y));
	mVelRange[END_VAL] = AEVec2::Random(AEVec2(ep->mVelRange[END_VAL][MIN_VAL].x, ep->mVelRange[END_VAL][MIN_VAL].y), AEVec2(ep->mVelRange[END_VAL][MAX_VAL].x, ep->mVelRange[END_VAL][MAX_VAL].y));

	// Compute the angular velocity range as a random float between the angular velocity ranges from ep
	mAngVelocityRange[START_VAL] = AEX::RandFloat(ep->mAngVelRange[START_VAL][MIN_VAL], ep->mAngVelRange[START_VAL][MAX_VAL]);
	mAngVelocityRange[END_VAL] = AEX::RandFloat(ep->mAngVelRange[END_VAL][MIN_VAL], ep->mAngVelRange[END_VAL][MAX_VAL]);

	// Compute the drag range as a random float between the drag ranges from ep
	mDragRange[START_VAL] = AEX::RandFloat(ep->mDragRange[START_VAL][MIN_VAL], ep->mDragRange[START_VAL][MAX_VAL]);
	mDragRange[END_VAL] = AEX::RandFloat(ep->mDragRange[END_VAL][MIN_VAL], ep->mDragRange[END_VAL][MAX_VAL]);

	// Compute the angular drag range as a random float between the angular velocity drag from ep
	mAngDragRange[START_VAL] = AEX::RandFloat(ep->mAngDragRange[START_VAL][MIN_VAL], ep->mAngDragRange[START_VAL][MAX_VAL]);
	mAngDragRange[END_VAL] = AEX::RandFloat(ep->mAngDragRange[END_VAL][MAX_VAL], ep->mAngDragRange[END_VAL][MAX_VAL]);

	// Initialize the remaining values to its starting values
	mCurrentLife = 0.0f;
	mTransform.mScale = mScaleRange[START_VAL];
	mTransform.mOrientation = mRotationRange[START_VAL];
	mVelocity = mVelRange[START_VAL];
	mDrag = mDragRange[START_VAL];
	mAngVelocity = mAngVelocityRange[START_VAL];
	mAngDrag = mAngDragRange[START_VAL];
	
	for (unsigned i = 0u; i < 4; i++)
		mParticleColor[i] = mColorRange[START_VAL][i];
}

/**************************************************************************/
/*!
\fn
Update

\brief
This function updates the particle data (life, color, size, etc...)
given some emitter properties.

\param dt
The time step to update the particle life by.

\param emitProp
The emitter properties to use when updating.

\return
The checkup if the particle is dead or not.
*/
/**************************************************************************/
bool Particle::Update(f32 dt, EmitterProperties * emitProp)
{
	// get the normalized value from the LifeTime
	f32 normLife = mCurrentLife / mLifeTime;

	// Check if the inputted EmitterProperties uses physics
	if (emitProp->mbUsePhysics)
	{
		// Check if the inputted EmitterProperties has AnimatePhxProperties activated
		if (emitProp->mbAnimatePhxProperties)
		{
			// Compute the velocity, drag, angular velocity and angular drag with a lerp
			mVelocity = LERP(mVelRange[START_VAL], mVelRange[END_VAL], normLife);
			mAngVelocity = LERP(mAngVelocityRange[START_VAL], mAngVelocityRange[END_VAL], normLife);
			mAngDrag = LERP(mAngDragRange[START_VAL], mAngDragRange[END_VAL], normLife);
			mDrag = LERP(mDragRange[START_VAL], mDragRange[END_VAL], normLife);
		}

		// Update the linear movement
		mTransform.mTranslationZ += mVelocity * dt;
		mVelocity *= mDrag;

		// Check if the rotation is not locked
		if (!emitProp->mbLockedRotation)
		{
			// Update the angular movement
			mTransform.mOrientation += mAngVelocity * dt;
			mAngVelocity *= mAngDrag;
		}
	}

	// Check if the inputted EmitterProperties has AnimateProperties activated
	if (emitProp->mbAnimateProperties)
	{
		// If the inputted EmitterProperties has mbLockedRotation deactivated, update the orientation of the particle
		if (!emitProp->mbLockedRotation)
			mTransform.mOrientation = LERP(mRotationRange[0], mRotationRange[1], normLife);

		// Update the scale and color of the particle using LERP and AEGfxColInterp
		mTransform.mScale = LERP(mScaleRange[0], mScaleRange[1], normLife);
		
		for (unsigned i = 0u; i < 4; i++)
			mParticleColor[i] = Lerp(mColorRange[START_VAL][i], mColorRange[END_VAL][i], normLife);
	}

	// Increment the current LifeTime by dt
	mCurrentLife += dt;

	// If the current LifeTime is higher than the LifeTime of the particle, then the particle need to be freed: return true
	if (mCurrentLife > mLifeTime)
		return true;

	// If not, continue: return false
	return false;
}
#pragma endregion

EmitterProperties::EmitterProperties()
{
	mMaxActiveCount = -1;
	mEmitRate = 25;

	mbUsePhysics = false;
	mbAnimateProperties = true;
	mbAnimatePhxProperties = true;
	mbLockedRotation = false;

	mLifeTimeRange[0] = 0.25f;
	mLifeTimeRange[1] = 1.25f;

	mVelRange[0][0] = AEVec2(-10.f, -10.f);
	mVelRange[0][1] = AEVec2(10.f, 10.f);
	mVelRange[1][0] = AEVec2(0.f, 10.f);
	mVelRange[1][1] = AEVec2(0.f, 100.f);

	mAngVelRange[0][0] = 0.0f;
	mAngVelRange[0][1] = 0.0f;
	mAngVelRange[1][0] = 0.0f;
	mAngVelRange[1][1] = 0.0f;

	mDragRange[0][0] = 0.98f;
	mDragRange[0][1] = 0.98f;
	mDragRange[1][0] = 0.0f;
	mDragRange[1][1] = 0.0f;

	mAngDragRange[0][0] = 0.98f;
	mAngDragRange[0][1] = 0.98f;
	mAngDragRange[1][0] = 0.98f;
	mAngDragRange[1][1] = 0.98f;

	mScaleRange[0][0] = AEVec2(1.0f, 1.0f);
	mScaleRange[0][1] = AEVec2(1.0f, 1.0f);
	mScaleRange[1][0] = AEVec2(0.f, 0.f);
	mScaleRange[1][1] = AEVec2(0.f, 0.f);

	mRotRange[0][0] = 0.0f;
	mRotRange[0][1] = 45.0f;
	mRotRange[1][0] = 135.0f;
	mRotRange[1][1] = 225.0f;

	for (unsigned i = 0u; i < 4; i++)
	{
		mColorRange[START_VAL][MIN_VAL][i] = 1.0f;
		mColorRange[START_VAL][MAX_VAL][i] = 1.0f;
		mColorRange[END_VAL][MIN_VAL][i] = 1.0f;
		mColorRange[END_VAL][MAX_VAL][i] = 1.0f;
	}

	mEmitShapeType = ES_CIRCLE;
	mbEmitShapeFill = false;
	mEmitShapeOffset = AEVec2(0.0f, 0.0f);
	mEmitShapeSize = AEVec2(2.0f, 2.0f);

	mEmitShapeAngle[0] = 45.0f;
	mEmitShapeAngle[1] = 135.0f;

	mEmitShapeLineStart = AEVec2(0.0f, 0.0f);
	mEmitShapeLineEnd = AEVec2(0.0f, 0.0f);

	selectedItem = "Circle";
}

bool EmitterProperties::OnGui()
{
	bool changed = false;

	// Particle enabler
	if (ImGui::Checkbox("Enable Particles", &mbParticlesEnabled)) changed = true;
	//// max emit count, -1 means no limit. 
	//int mMaxActiveCount;
	if (ImGui::DragInt("Max Emit Count", &mMaxActiveCount)) changed = true;
	if (mMaxActiveCount < -1) mMaxActiveCount = -1;
	//// Emit rate. emission per udpdate, -1 means it will emit until there are no longer free particles or it reaches the limit
	//int mEmitRate;
	if (ImGui::DragInt("Emit Rate", &mEmitRate)) changed = true;
	//// Emitter States
	//bool mbUsePhysics;				// Updates position and rotation using velocity and angular velocity
	if (ImGui::Checkbox("Use Physics", &mbUsePhysics)) changed = true;
	//bool mbAnimateProperties;		// Animate properties over time.
	if (ImGui::Checkbox("Animate Properties", &mbAnimateProperties)) changed = true;
	//bool mbAnimatePhxProperties;	// Animate physics properties over time. 
	if (ImGui::Checkbox("Animate Phx Properties", &mbAnimatePhxProperties)) changed = true;

	//bool mbLockedRotation;			// Lock rotation (should prevent any kind of orientation change (physics or animation)
	if (ImGui::Checkbox("Lock Particle Rotation", &mbLockedRotation)) changed = true;

	// Lifetime range
	// Used to determine the lifetime of a particle randomly.
	//f32		mLifeTimeRange[2];
	if (ImGui::DragFloat2("Lifetime Range", mLifeTimeRange, 1.0f, 0.0f, 10000.0f)) changed = true;

	ImGui::Checkbox("Render at front", &mFront);

	f32 offsetArray[2]{ mOffset.x, mOffset.y };
	if (ImGui::DragFloat2("Particle Offset", offsetArray, 1.0f, 0.0f, 10000.0f))
	{
		mOffset.x = offsetArray[0];
		mOffset.y = offsetArray[1];
		changed = true;
	}

	//
	//// Color Range
	//// Second dimension is for animating the properites.
	//// Used when mbAnimateProperties is true.
	//u32		mColorRange[2][2];
	ImGui::Text("Color Range");
	ImGui::ColorEdit4("Start min", mColorRange[START_VAL][MIN_VAL]);
	ImGui::ColorEdit4("Start max", mColorRange[START_VAL][MAX_VAL]);
	ImGui::ColorEdit4("End min", mColorRange[END_VAL][MIN_VAL]);
	ImGui::ColorEdit4("End max", mColorRange[END_VAL][MAX_VAL]);


	//
	//// Physics Properties. Used when mbUsePhysics = true
	//// The second dimension is for animating over the lifetime of the properties. 
	//// Used when both mbAnimateProperties and mbAnimatePhxProperties are true.
	//AEVec2	mVelRange[2][2];		// Velocity range.
	for (int i = 0; i < 4; i++)
	{
		AEVec2& currentVec = mVelRange[i / 2][i % 2];
		f32 velVecArray[2]{ currentVec.x, currentVec.y };
		std::string name = "Vel Range " + std::to_string(i);
		if (ImGui::DragFloat2(name.c_str(), velVecArray, 1.0f, -10000.f, 10000.f)) changed = true;

		currentVec = AEVec2(velVecArray[0], velVecArray[1]);
	}

	//f32		mAngVelRange[2][2];		// Angular verlocity range.
	f32 angVecRange[]{ mAngVelRange[0][0], mAngVelRange[0][1], mAngVelRange[1][0], mAngVelRange[1][1] };
	if (ImGui::DragFloat4("Angular Velocity Range", angVecRange, 1.0f, -10000.f, 10000.f)) changed = true;

	for (int i = 0; i < 4; i++)
		mAngVelRange[i / 2][i % 2] = angVecRange[i];

	//f32		mDragRange[2][2];		// Drag range.
	f32 dragRange[]{ mDragRange[0][0], mDragRange[0][1], mDragRange[1][0], mDragRange[1][1] };
	if (ImGui::DragFloat4("Drag Range", dragRange, 1.0f, -10000.f, 10000.f)) changed = true;

	for (int i = 0; i < 4; i++)
		mDragRange[i / 2][i % 2] = dragRange[i];

	//f32		mAngDragRange[2][2];	// Angular drag range.
	f32 angDragRange[]{ mAngDragRange[0][0], mAngDragRange[0][1], mAngDragRange[1][0], mAngDragRange[1][1] };
	if (ImGui::DragFloat4("Ang Drag Range", angDragRange, 1.0f, -10000.f, 10000.f)) changed = true;

	for (int i = 0; i < 4; i++)
		mAngDragRange[i / 2][i % 2] = angDragRange[i];

	//								// Transform Data.
	//								// Second dimension is for animating over the lifetime of the properties
	//								// Used when mbAnimateProperties is true.
	//AEVec2 mScaleRange[2][2];
	for (int i = 0; i < 4; i++)
	{
		AEVec2& currentVec = mScaleRange[i / 2][i % 2];
		f32 velVecArray[2]{ currentVec.x, currentVec.y };
		std::string name = "Scale Range " + std::to_string(i);
		if (ImGui::DragFloat2(name.c_str(), velVecArray, 1.0f, -10000.f, 10000.f)) changed = true;

		currentVec = AEVec2(velVecArray[0], velVecArray[1]);
	}

	//f32	   mRotRange[2][2];
	f32 rotationRange[]{ mRotRange[0][0], mRotRange[0][1], mRotRange[1][0], mRotRange[1][1] };
	if (ImGui::DragFloat4("Rot Range", rotationRange, 1.0f, -10000.f, 10000.f)) changed = true;

	for (int i = 0; i < 4; i++)
		mRotRange[i / 2][i % 2] = rotationRange[i];

	//
	//// Emitter Type
	//enum EEmitterShape { ES_POINT, ES_RECT, ES_CIRCLE, ES_LINE, ES_COUNT };
	//EEmitterShape mEmitShapeType;
	static ImGuiComboFlags emitterShapeFlags = 0;
	const char* emitShape_items[] = { "Point", "Rectangle", "Circle", "Line" };

	ImGui::Text("Emmiter type");
	if (ImGui::BeginCombo("Emmiter type", selectedItem, emitterShapeFlags)) // The second parameter is the label previewed before opening the combo.
	{
		int n;
		for (n = 0; n < IM_ARRAYSIZE(emitShape_items); n++)
		{
			bool is_selected = (selectedItem == emitShape_items[n]);
			if (ImGui::Selectable(emitShape_items[n], is_selected))
			{
				selectedItem = emitShape_items[n];

				switch (n)
				{
				case 0:
					mEmitShapeType = ES_POINT;
					break;
				case 1:
					mEmitShapeType = ES_RECT;
					break;
				case 2:
					mEmitShapeType = ES_CIRCLE;
					break;
				case 3:
					mEmitShapeType = ES_LINE;
					break;
				}
			}
			if (is_selected)
				ImGui::SetItemDefaultFocus();   // Set the initial focus when opening the combo (scrolling + for keyboard navigation support in the upcoming navigation branch)
		}

		ImGui::EndCombo();
	}

	//// Should the shape be filled
	//// Only applies when mEmitShapeType is ES_RECT or ES_CIRCLE
	if (mEmitShapeType == ES_RECT || mEmitShapeType == ES_CIRCLE)
	{
		//bool mbEmitShapeFill;
		ImGui::Text("Emmiter Shape Fill");
		if (ImGui::Checkbox("Emmiter Shape Fill", &mbEmitShapeFill)) changed = true;

		if (mEmitShapeType == ES_RECT)
		{
			//// rect/fill-rect data
			//AEVec2 mEmitShapeOffset;
			//AEVec2 mEmitShapeSize;
			if (ImGui::DragFloat2("Emit Shape Offset", mEmitShapeOffset.v)) changed = true;
			if (ImGui::DragFloat2("Emit Shape Size", mEmitShapeSize.v)) changed = true;
		}
		//// circle data: Emit Angle limits
		//f32 mEmitShapeAngle[2];
		else if (ImGui::DragFloat2("Emit Shape Angle", mEmitShapeAngle)) changed = true;
	}
	else if (mEmitShapeType == ES_LINE)
	{
		//// line data
		//AEVec2 mEmitShapeLineStart;	// Start position of the line emitter (with respect to the emitter transform).
		//AEVec2 mEmitShapeLineEnd;	// End position of the line emitter (with respect to the emitter transform).
		if (ImGui::DragFloat2("Emit Line Start", mEmitShapeLineStart.v)) changed = true;
		if (ImGui::DragFloat2("Emit Line End", mEmitShapeLineEnd.v)) changed = true;
	}

	return changed;
}


#pragma region PARTICLE EMITTER
// ----------------------------------------------------------------------------
#pragma region// RENDERABLE INHERITED.


ParticleEmitter::~ParticleEmitter()
{
	delete mEmitProperties;
	GfxMgr->mRenderableList.remove(this);
}

void ParticleEmitter::Initialize()
{
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Create a default texture

	Renderable::Initialize();

	// Set the gfx render states
	mBlending->SetSource(BlendingController::BlendFactor::eSrcAlpha);
	mBlending->SetDestination(BlendingController::BlendFactor::eOne);
	mBlending->SetEquation(BlendingController::BlendEquation::eFuncAdd);

	GfxMgr->mRenderableList.push_back(this);
}

/**************************************************************************/
/*!
\fn
Update

\brief
Updates the emitter for one frame.
*/
/**************************************************************************/
void ParticleEmitter::Update()
{
	s32 emitCount;

	// Determine if it should emit any particles using the EmitParticles function:
	//	  - If the emit rate is not equal to -1 (max number of particles that
	//		can be emitted), set emitCount as the emit rate
	if (mEmitProperties->mEmitRate != -1)
		emitCount = mEmitProperties->mEmitRate;
	else
	{
		//	  - If the emit rate == -1 and the maxActiveCount == -1,
		//		set emitCount as the particle count - the size of mActiveParticles
		if (mEmitProperties->mMaxActiveCount == -1)
			emitCount = mParticleCount - mActiveParticles.size();

		//	  - If the emit rate == -1 and the maxActiveCount != -1,
		//		set emitCount as the max active count - the size of mActiveParticles
		else
			emitCount = mEmitProperties->mMaxActiveCount - mActiveParticles.size();
	}

	// Call EmitParticles to emit the new particles depending on emitCount if the particles are enabled
	if (mEmitProperties->mbParticlesEnabled)
		EmitParticles(emitCount);

	// Call Step() with the framerate as a parameter
	Step((f32)aexTime->GetFrameTime());
}

// @PROVIDED
// ----------------------------------------------------------------------------
// Renders all the alive particles using additive alpha blending mode. 
void ParticleEmitter::Render(Camera * pCam)
{
	if (!CheckCameraCulling() && Editorstate->IsPlaying())
		return;
	/*GameObject* camOwner = pCam->mOwner;
	if (camOwner && camOwner->GetComp<TransformComp>()->mLocal.m)*/
	// bind Texture
	glBindTexture(GL_TEXTURE_2D, mTex->get()->GetID());

	//std::cout << "Num of particles:" << mActiveParticles.size() << std::endl;
	//std::cout << "Num of cameras:" << GfxMgr->mCameraList.size() << std::endl;

	// draw each particle
	FOR_EACH(particleIt, mActiveParticles)
	{
		AEVec2& offset = mEmitProperties->mOffset;
		Particle & p = **particleIt;
		glm::mat4 transform = glm::mat4(1.0f); // make sure to initialize matrix to identity matrix first
		transform = glm::translate(transform, glm::vec3(p.mTransform.mTranslationZ.x + offset.x, p.mTransform.mTranslationZ.y + offset.y, p.mTransform.mTranslationZ.z));
		transform = glm::rotate(transform, p.mTransform.mOrientation, glm::vec3(0.0f, 0.0f, 1.0f));
		transform = glm::scale(transform, glm::vec3(p.mTransform.mScale.x, p.mTransform.mScale.y, 0.0f));

		Shader * actualShader = mShader->get();
		if (actualShader != nullptr)
		{
			actualShader->use();
			// pass transformation matrices to the shader
			actualShader->setVec4("color", p.mParticleColor[0], p.mParticleColor[1], p.mParticleColor[2], p.mParticleColor[3]);
			actualShader->setVec2("aspectRatio", AEVec2(1.0f, 1.0f));
			actualShader->setMat3("texTransform", glm::mat3(1.0f));
			actualShader->setMat4(1, glm::value_ptr(transform));

			Renderable::Render(pCam);
		}
	}

	glDisable(GL_BLEND);
}

namespace fs = std::experimental::filesystem;

bool ParticleEmitter::OnGui()
{
	bool changed = false;
	static bool isPathShown = false;
	static std::string actualName = "particle.png";
	std::string previewName;

	if (ImGui::Checkbox("Show Paths", &isPathShown)) changed = true;

	// To solve bug where currently selected texture wouldn't change
	// to only the name as soon as you unclicked the checkbox
	if (!isPathShown)
		previewName = aexResourceMgr->GetNameWithExtension(actualName);
	else
		previewName = actualName;

	if (ImGui::BeginCombo("Texture", previewName.c_str()))
	{
		try {
			fs::path textureDir = fs::path("data\\Textures");

			int count = 0;
			for (auto dirIt = fs::recursive_directory_iterator(textureDir);
				dirIt != fs::recursive_directory_iterator();
				dirIt++, ++count)
			{
				//if (dirIt->is_directory())
				//continue;
				if (fs::is_directory(dirIt->path()))
					continue;

				if (dirIt->path().extension().string() == ".meta")
					continue;

				std::string currentFileName;

				if (isPathShown)
					currentFileName = dirIt->path().relative_path().string();
				else
					currentFileName = dirIt->path().filename().string();

				if (ImGui::Selectable((currentFileName + "##" + std::to_string(count)).c_str()))
				{
					actualName = dirIt->path().relative_path().string().c_str();
					mTex = aexResourceMgr->getResource<Texture>(actualName.c_str());
				}
			}
		} CATCH_FS_ERROR

			ImGui::EndCombo();
	}

	if (mEmitProperties->OnGui()) changed = true;

	return false;
}

#pragma endregion

// ----------------------------------------------------------------------------
#pragma region// PARTICLE SYSTEM SPECIFIC

/**************************************************************************/
/*!
\fn
ParticleEmitter (Non-Default constructor)

\brief
Constructs a particle emitter object.

\param particleCount
Number of particles in the system.
*/
/**************************************************************************/
ParticleEmitter::ParticleEmitter(u32 particleCount)
{
	mEmitProperties = new EmitterProperties();
	mTex = aexResourceMgr->getResource<Texture>("data\\Textures\\particle.png");

	SetParticleCount(particleCount);
}

/**************************************************************************/
/*!
\fn
SetParticleCount

\brief
Sets mParticleCount member variable and resize the particle vector
mParticles.

\param particleCount
The new size for mParticles.

\return
A pointer to the ocean we just created.
*/
/**************************************************************************/
void ParticleEmitter::SetParticleCount(u32 particleCount)
{
	// Set particleCount as the new mParticleCount
	mParticleCount = particleCount;
	// Resize mParticles with the inputted parameter
	mParticles.resize(mParticleCount);
	// Call Reset() to reset both active and free lists
	Reset();
}

/**************************************************************************/
/*!
\fn
Reset

\brief
Clears the active list and add all the particles to the freelist.
*/
/**************************************************************************/
void ParticleEmitter::Reset()
{
	mActiveParticles.clear(); // remove all active particles
	mFreeParticles.clear();	// remove all free particles

							// Push all the particles from mParticles to the free list
	for (u32 i = 0; i < mParticles.size(); ++i)
		mFreeParticles.push_back(&mParticles[i]);
}

/**************************************************************************/
/*!
\fn
Step

\brief
For each particle in the active list, call update. If the particle
is dead, add it to the free list and remove it from the active list.

\param dt
Delta time to update the particles by.
*/
/**************************************************************************/
void ParticleEmitter::Step(f32 dt)
{
	// Create an iterator to go through the mActiveParticles list
	std::list<Particle*>::iterator it;
	for (it = mActiveParticles.begin(); it != mActiveParticles.end(); it++)
	{
		// Update each active particle. Also check if it returns true
		if ((*it)->Update(dt, mEmitProperties))
		{
			// If that's so, push the particle to mFreeParticles and erase it from mActiveParticles
			mFreeParticles.push_back(*it);
			it = mActiveParticles.erase(it);

			// SANITY CHECK: if that particle was the last one to be updated, break the loop
			if (it == mActiveParticles.end())
				break;
		}
	}
}

/**************************************************************************/
/*!
\fn
EmitParticles

\brief
Activates emit_count particles from the freelist.

\param emit_count
The number of particles to emit.

\return
The number of particles actually emitted.
*/
/**************************************************************************/
s32  ParticleEmitter::EmitParticles(s32 emit_count)
{
	// Define numOfParticlesSpawned and initialize it to 0
	s32 numOfParticlesSpawned = 0;

	for (numOfParticlesSpawned = 0; numOfParticlesSpawned < emit_count; numOfParticlesSpawned++)
	{
		// If there is no particle from mFreeParticles left, return the total num of particles spawned
		if (mFreeParticles.empty())
			return numOfParticlesSpawned;

		// Get the particle from the freelist and adding it to the active list
		mActiveParticles.push_back(mFreeParticles.front());
		// Call initialize on the particle using mEmitProperties
		mFreeParticles.front()->Initialize(mEmitProperties);

		// Set the position of the particle depending on the position of the particle Emitter
		mFreeParticles.front()->mTransform.mTranslationZ = GetTransformCompFromOwner->mLocal.mTranslationZ;

		// Determine the position of the particle with respect to mEmitProperties->mEmitShapType
		switch (mEmitProperties->mEmitShapeType)
		{
			// case ES_POINT: particle position = emitter position. Already done. Break�.
		case EmitterProperties::ES_POINT:
			break;

			// case ES_RECT: particle position = emitter position + the value returned by RandomPosRect()
		case EmitterProperties::ES_RECT:
			mFreeParticles.front()->mTransform.mTranslationZ += RandomPosRect(mEmitProperties->mEmitShapeOffset,
				mEmitProperties->mEmitShapeSize,
				mEmitProperties->mbEmitShapeFill);
			break;

			// case ES_CIRCLE: particle position = emitter position + the value returned by RandomPosCircle()
		case EmitterProperties::ES_CIRCLE:
			mFreeParticles.front()->mTransform.mTranslationZ += RandomPosCircle(mEmitProperties->mEmitShapeOffset,
				mEmitProperties->mEmitShapeSize,
				mEmitProperties->mEmitShapeAngle[0],
				mEmitProperties->mEmitShapeAngle[1],
				mEmitProperties->mbEmitShapeFill);
			break;

			// case ES_LINE: particle position equal to emitter position + the value returned by RandomPosLine()
		case EmitterProperties::ES_LINE:
			mFreeParticles.front()->mTransform.mTranslationZ += RandomPosLine(mEmitProperties->mEmitShapeLineStart,
				mEmitProperties->mEmitShapeLineEnd);
			break;
		}

		// Remove the particle from the free list, since it is already on the active list
		mFreeParticles.pop_front();
	}

	// Return the total num of particles spawned
	return numOfParticlesSpawned;
}


void EmitterProperties::FromJson(json & value)
{
	//value["EmitterProperties"] = emitPropVal;
	json & emitPropsVal = value["EmitterProperties"];


	emitPropsVal["Enabled"] >> mbParticlesEnabled;

	//// max emit count, -1 means no limit. 
	//int mMaxActiveCount;
	emitPropsVal["MaxActiveCount"] >> mMaxActiveCount;
	//// Emit rate. emission per udpdate, -1 means it will emit until there are no longer free particles or it reaches the limit
	//int mEmitRate;
	emitPropsVal["EmitRate"] >> mEmitRate;

	//// Emitter States
	//bool mbUsePhysics;				// Updates position and rotation using velocity and angular velocity
	//bool mbAnimateProperties;			// Animate properties over time.
	//bool mbAnimatePhxProperties;		// Animate physics properties over time. 
	//bool mbLockedRotation;			// Lock rotation (should prevent any kind of orientation change (physics or animation)
	json & emitterStatesVal = emitPropsVal["EmitterStates"];
	emitterStatesVal["UsePhysics"] >> mbUsePhysics;
	emitterStatesVal["AnimateProperties"] >> mbAnimateProperties;
	emitterStatesVal["AnimatePhxProperties"] >> mbAnimatePhxProperties;
	emitterStatesVal["LockedRotation"] >> mbLockedRotation;

	emitPropsVal["RenderFront"] >> mFront;

	emitPropsVal["ParticleOffset"] >> mOffset;

	// Lifetime range
	// Used to determine the lifetime of a particle randomly.
	//f32		mLifeTimeRange[2];
	json & lifetimeVal = emitPropsVal["LifeTimeRange"];
	lifetimeVal["min"] >> mLifeTimeRange[MIN_VAL];
	lifetimeVal["max"] >> mLifeTimeRange[MAX_VAL];


	//json & PARAM_Val = emitPropsVal["PARAMETER_NAME"];
	//PARAM_Val["min"] >> mEmitProperties->PARAMETER[MIN_VAL];
	//PARAM_Val["max"] >> mEmitProperties->PARAMETER[MAX_VAL];



	//json & PARAM_Val = emitPropsVal["PARAMETER_NAME"];
	//json & PARAM_StartVal = PARAM_Val["start"];
	//PARAM_StartVal["min"] >> mEmitProperties->PARAMETER[START_VAL][MIN_VAL];
	//PARAM_StartVal["max"] >> mEmitProperties->PARAMETER[START_VAL][MAX_VAL];
	//json & PARAM_EndVal = PARAM_Val["end"];
	//PARAM_EndVal["min"] >> mEmitProperties->PARAMETER[END_VAL][MIN_VAL];
	//PARAM_EndVal["max"] >> mEmitProperties->PARAMETER[END_VAL][MAX_VAL];


	//// Color Range
	//// Second dimension is for animating the properites.
	//// Used when mbAnimateProperties is true.
	//u32		mColorRange[2][2];
	json& val = value["Color"];

	json & ColorRangeVal = emitPropsVal["ColorRange"];
	json & ColorRangeStart = ColorRangeVal["start"];
	json & ColorRangeStartMinVal = ColorRangeStart["min"];
	ColorRangeStartMinVal["r"] >> mColorRange[START_VAL][MIN_VAL][0];
	ColorRangeStartMinVal["g"] >> mColorRange[START_VAL][MIN_VAL][1];
	ColorRangeStartMinVal["b"] >> mColorRange[START_VAL][MIN_VAL][2];
	ColorRangeStartMinVal["a"] >> mColorRange[START_VAL][MIN_VAL][3];
	json & ColorRangeStartMaxVal = ColorRangeStart["max"];
	ColorRangeStartMaxVal["r"] >> mColorRange[START_VAL][MAX_VAL][0];
	ColorRangeStartMaxVal["g"] >> mColorRange[START_VAL][MAX_VAL][1];
	ColorRangeStartMaxVal["b"] >> mColorRange[START_VAL][MAX_VAL][2];
	ColorRangeStartMaxVal["a"] >> mColorRange[START_VAL][MAX_VAL][3];
	json & ColorRangeEnd = ColorRangeVal["end"];
	json & ColorRangeEndMinVal = ColorRangeEnd["min"];
	ColorRangeEndMinVal["r"] >> mColorRange[END_VAL][MIN_VAL][0];
	ColorRangeEndMinVal["g"] >> mColorRange[END_VAL][MIN_VAL][1];
	ColorRangeEndMinVal["b"] >> mColorRange[END_VAL][MIN_VAL][2];
	ColorRangeEndMinVal["a"] >> mColorRange[END_VAL][MIN_VAL][3];
	json & ColorRangeEndMaxVal = ColorRangeEnd["max"];
	ColorRangeStartMaxVal["r"] >> mColorRange[END_VAL][MAX_VAL][0];
	ColorRangeStartMaxVal["g"] >> mColorRange[END_VAL][MAX_VAL][1];
	ColorRangeStartMaxVal["b"] >> mColorRange[END_VAL][MAX_VAL][2];
	ColorRangeStartMaxVal["a"] >> mColorRange[END_VAL][MAX_VAL][3];

	//// Physics Properties. Used when mbUsePhysics = true
	//// The second dimension is for animating over the lifetime of the properties. 
	//// Used when both mbAnimateProperties and mbAnimatePhxProperties are true.
	//AEVec2	mVelRange[2][2];		// Velocity range.
	json & VelRangeVal = emitPropsVal["VelRange"];
	json & VelRangeStart = VelRangeVal["start"];
	VelRangeStart["min"] >> mVelRange[START_VAL][MIN_VAL];
	VelRangeStart["max"] >> mVelRange[START_VAL][MAX_VAL];
	json & VelRangeEnd = VelRangeVal["end"];
	VelRangeEnd["min"] >> mVelRange[END_VAL][MIN_VAL];
	VelRangeEnd["max"] >> mVelRange[END_VAL][MAX_VAL];

	//f32		mAngVelRange[2][2];		// Angular verlocity range.

	//json & AngVelRangeVal = emitPropsVal["PARAMETER_NAME"];
	//json & AngVelRangeValStart = AngVelRangeVal["start"];
	json & AngVelRangeVal = emitPropsVal["AngVelRange"];
	json & AngVelRangeValStart = /*AngVelRangeVal*/AngVelRangeVal["start"];
	AngVelRangeValStart["min"] >> mAngVelRange[START_VAL][MIN_VAL];
	AngVelRangeValStart["max"] >> mAngVelRange[START_VAL][MAX_VAL];
	json & AngVelRangeValEnd = /*AngVelRangeVal*/AngVelRangeVal["end"];
	AngVelRangeValEnd["min"] >> mAngVelRange[END_VAL][MIN_VAL];
	AngVelRangeValEnd["max"] >> mAngVelRange[END_VAL][MAX_VAL];

	//f32		mDragRange[2][2];		// Drag range.
	json & DragRangeVal = emitPropsVal["DragRange"];
	json & DragRangeValStart = DragRangeVal["start"];
	DragRangeValStart["min"] >> mDragRange[START_VAL][MIN_VAL];
	DragRangeValStart["max"] >> mDragRange[START_VAL][MAX_VAL];
	json & DragRangeValEnd = DragRangeVal["end"];
	DragRangeValEnd["min"] >> mDragRange[END_VAL][MIN_VAL];
	DragRangeValEnd["max"] >> mDragRange[END_VAL][MAX_VAL];

	//f32		mAngDragRange[2][2];	// Angular drag range.
	json & AngDragRangeVal = emitPropsVal["AngDragRange"];
	json & AngDragRangeValStart = AngDragRangeVal["start"];
	AngDragRangeValStart["min"] >> mAngDragRange[START_VAL][MIN_VAL];
	AngDragRangeValStart["max"] >> mAngDragRange[START_VAL][MAX_VAL];
	json & AngDragRangeValEnd = AngDragRangeVal["end"];
	AngDragRangeValEnd["min"] >> mAngDragRange[END_VAL][MIN_VAL];
	AngDragRangeValEnd["max"] >> mAngDragRange[END_VAL][MAX_VAL];

	// Transform Data.
	// Second dimension is for animating over the lifetime of the properties
	// Used when mbAnimateProperties is true.
	//AEVec2 mScaleRange[2][2];
	json & ScaleRangeVal = emitPropsVal["ScaleRange"];
	json & ScaleRangeStartVal = ScaleRangeVal["start"];
	ScaleRangeStartVal["min"] >> mScaleRange[START_VAL][MIN_VAL];
	ScaleRangeStartVal["max"] >> mScaleRange[START_VAL][MAX_VAL];
	json & ScaleRangeEndVal = ScaleRangeVal["end"];
	ScaleRangeEndVal["min"] >> mScaleRange[END_VAL][MIN_VAL];
	ScaleRangeEndVal["max"] >> mScaleRange[END_VAL][MAX_VAL];

	//f32	   mRotRange[2][2];
	json & RotRangeVal = emitPropsVal["RotRange"];
	json & RotRangeStartVal = RotRangeVal["start"];
	RotRangeStartVal["min"] >> mRotRange[START_VAL][MIN_VAL];
	RotRangeStartVal["max"] >> mRotRange[START_VAL][MAX_VAL];
	json & RotRangeEndVal = RotRangeVal["end"];
	RotRangeEndVal["min"] >> mRotRange[END_VAL][MIN_VAL];
	RotRangeEndVal["max"] >> mRotRange[END_VAL][MAX_VAL];

	//// Emitter Type
	//enum EEmitterShape { ES_POINT, ES_RECT, ES_CIRCLE, ES_LINE, ES_COUNT };
	//EEmitterShape mEmitShapeType;
	int emitShape = emitPropsVal["EmitShapeType"];
	switch (emitShape)
	{
	case 0:
		mEmitShapeType = ES_POINT;
		break;
	case 1:
		mEmitShapeType = ES_RECT;
		break;
	case 2:
		mEmitShapeType = ES_CIRCLE;
		break;
	case 3:
		mEmitShapeType = ES_LINE;
		break;
	}

	//// Should the shape be filled
	//// Only applies when mEmitShapeType is ES_RECT or ES_CIRCLE
	//bool mbEmitShapeFill;
	emitPropsVal["EmitShapeFill"] >> mbEmitShapeFill;

	//// rect/fill-rect data
	//AEVec2 mEmitShapeOffset;
	emitPropsVal["EmitShapeOffset"] >> mEmitShapeOffset;

	//AEVec2 mEmitShapeSize;
	emitPropsVal["EmitShapeSize"] >> mEmitShapeSize;

	//// circle data: Emit Angle limits
	//f32 mEmitShapeAngle[2];
	json & emitShapeAngleVal = emitPropsVal["EmitShapeAngle"];
	emitShapeAngleVal["min"] >> mEmitShapeAngle[MIN_VAL];
	emitShapeAngleVal["max"] >> mEmitShapeAngle[MAX_VAL];

	//// line data
	//AEVec2 mEmitShapeLineStart;	// Start position of the line emitter (with respect to the emitter transform).
	emitPropsVal["EmitShapeLineStart"] >> mEmitShapeLineStart;

	//AEVec2 mEmitShapeLineEnd;	// End position of the line emitter (with respect to the emitter transform).
	emitPropsVal["EmitShapeLineEnd"] >> mEmitShapeLineEnd;

	switch (mEmitShapeType)
	{
	case EmitterProperties::ES_POINT:
		selectedItem = "Point";
		break;

	case EmitterProperties::ES_RECT:
		selectedItem = "Rectangle";
		break;

	case EmitterProperties::ES_CIRCLE:
		selectedItem = "Circle";
		break;

	case EmitterProperties::ES_LINE:
		selectedItem = "Line";
		break;
	}
}

void EmitterProperties::ToJson(json & value)
{
	json emitPropVal;

	
	emitPropVal["Enabled"] << mbParticlesEnabled;

	//// max emit count, -1 means no limit. 
	//int mMaxActiveCount;
	emitPropVal["MaxActiveCount"] << mMaxActiveCount;
	//// Emit rate. emission per udpdate, -1 means it will emit until there are no longer free particles or it reaches the limit
	//int mEmitRate;
	emitPropVal["EmitRate"] << mEmitRate;

	//// Emitter States
	//bool mbUsePhysics;				// Updates position and rotation using velocity and angular velocity
	//bool mbAnimateProperties;			// Animate properties over time.
	//bool mbAnimatePhxProperties;		// Animate physics properties over time. 
	//bool mbLockedRotation;			// Lock rotation (should prevent any kind of orientation change (physics or animation)
	json emitterStates;
	emitterStates["UsePhysics"] << mbUsePhysics;
	emitterStates["AnimateProperties"] << mbAnimateProperties;
	emitterStates["AnimatePhxProperties"] << mbAnimatePhxProperties;
	emitterStates["LockedRotation"] << mbLockedRotation;
	emitPropVal["EmitterStates"] = emitterStates;

	emitPropVal["RenderFront"] << mFront;
	emitPropVal["ParticleOffset"] << mOffset;

	// Lifetime range
	// Used to determine the lifetime of a particle randomly.
	//f32		mLifeTimeRange[2];
	json lifetimeVal;
	lifetimeVal["min"] << mLifeTimeRange[MIN_VAL];
	lifetimeVal["max"] << mLifeTimeRange[MAX_VAL];
	emitPropVal["LifeTimeRange"] = lifetimeVal;

	//// Color Range
	//// Second dimension is for animating the properites.
	//// Used when mbAnimateProperties is true.
	//u32		mColorRange[2][2];
	json ColorRangeVal;
	json ColorRangeStart;
	json ColorRangeEnd;
	json ColorRangeStartMinVal;
	ColorRangeStartMinVal["r"] << mColorRange[START_VAL][MIN_VAL][0];
	ColorRangeStartMinVal["g"] << mColorRange[START_VAL][MIN_VAL][1];
	ColorRangeStartMinVal["b"] << mColorRange[START_VAL][MIN_VAL][2];
	ColorRangeStartMinVal["a"] << mColorRange[START_VAL][MIN_VAL][3];
	ColorRangeStart["min"] = ColorRangeStartMinVal;

	json ColorRangeStartMaxVal;
	ColorRangeStartMaxVal["r"] << mColorRange[START_VAL][MAX_VAL][0];
	ColorRangeStartMaxVal["g"] << mColorRange[START_VAL][MAX_VAL][1];
	ColorRangeStartMaxVal["b"] << mColorRange[START_VAL][MAX_VAL][2];
	ColorRangeStartMaxVal["a"] << mColorRange[START_VAL][MAX_VAL][3];
	ColorRangeStart["max"] = ColorRangeStartMaxVal;

	json ColorRangeEndMinVal;
	ColorRangeEndMinVal["r"] << mColorRange[END_VAL][MIN_VAL][0];
	ColorRangeEndMinVal["g"] << mColorRange[END_VAL][MIN_VAL][1];
	ColorRangeEndMinVal["b"] << mColorRange[END_VAL][MIN_VAL][2];
	ColorRangeEndMinVal["a"] << mColorRange[END_VAL][MIN_VAL][3];
	ColorRangeEnd["min"] = ColorRangeEndMinVal;

	json ColorRangeEndMaxVal;
	ColorRangeEndMaxVal["r"] << mColorRange[END_VAL][MAX_VAL][0];
	ColorRangeEndMaxVal["g"] << mColorRange[END_VAL][MAX_VAL][1];
	ColorRangeEndMaxVal["b"] << mColorRange[END_VAL][MAX_VAL][2];
	ColorRangeEndMaxVal["a"] << mColorRange[END_VAL][MAX_VAL][3];
	ColorRangeEnd["max"] = ColorRangeEndMaxVal;
	ColorRangeVal["start"] = ColorRangeStart;
	ColorRangeVal["end"] = ColorRangeEnd;
	emitPropVal["ColorRange"] = ColorRangeVal;

	//// Physics Properties. Used when mbUsePhysics = true
	//// The second dimension is for animating over the lifetime of the properties. 
	//// Used when both mbAnimateProperties and mbAnimatePhxProperties are true.
	//AEVec2	mVelRange[2][2];		// Velocity range.
	json VelRangeVal;
	json VelRangeStart;
	json VelRangeEnd;
	VelRangeStart["min"] << mVelRange[START_VAL][MIN_VAL];
	VelRangeStart["max"] << mVelRange[START_VAL][MAX_VAL];
	VelRangeVal["start"] = VelRangeStart;
	VelRangeEnd["min"] << mVelRange[END_VAL][MIN_VAL];
	VelRangeEnd["max"] << mVelRange[END_VAL][MAX_VAL];
	VelRangeVal["end"] = VelRangeEnd;
	emitPropVal["VelRange"] = VelRangeVal;

	//f32		mAngVelRange[2][2];		// Angular verlocity range.
	json AngVelRangeVal;
	json AngVelRangeValStart;
	json AngVelRangeValEnd;
	AngVelRangeValStart["min"] << mAngVelRange[START_VAL][MIN_VAL];
	AngVelRangeValStart["max"] << mAngVelRange[START_VAL][MAX_VAL];
	AngVelRangeVal["start"] = AngVelRangeValStart;
	AngVelRangeValEnd["min"] << mAngVelRange[END_VAL][MIN_VAL];
	AngVelRangeValEnd["max"] << mAngVelRange[END_VAL][MAX_VAL];
	AngVelRangeVal["end"] = AngVelRangeValEnd;
	emitPropVal["AngVelRange"] = AngVelRangeVal;

	//f32		mDragRange[2][2];		// Drag range.
	json DragRangeVal;
	json DragRangeValStart;
	json DragRangeValEnd;
	DragRangeValStart["min"] << mDragRange[START_VAL][MIN_VAL];
	DragRangeValStart["max"] << mDragRange[START_VAL][MAX_VAL];
	DragRangeVal["start"] = DragRangeValStart;
	DragRangeValEnd["min"] << mDragRange[END_VAL][MIN_VAL];
	DragRangeValEnd["max"] << mDragRange[END_VAL][MAX_VAL];
	DragRangeVal["end"] = DragRangeValEnd;
	emitPropVal["DragRange"] = DragRangeVal;

	//f32		mAngDragRange[2][2];	// Angular drag range.
	json AngDragRangeVal;
	json AngDragRangeValStart;
	json AngDragRangeValEnd;
	AngDragRangeValStart["min"] << mAngDragRange[START_VAL][MIN_VAL];
	AngDragRangeValStart["max"] << mAngDragRange[START_VAL][MAX_VAL];
	AngDragRangeVal["start"] = AngDragRangeValStart;
	AngDragRangeValEnd["min"] << mAngDragRange[END_VAL][MIN_VAL];
	AngDragRangeValEnd["max"] << mAngDragRange[END_VAL][MAX_VAL];
	AngDragRangeVal["end"] = AngDragRangeValEnd;
	emitPropVal["AngDragRange"] = AngDragRangeVal;

	// Transform Data.
	// Second dimension is for animating over the lifetime of the properties
	// Used when mbAnimateProperties is true.
	//AEVec2 mScaleRange[2][2];
	json ScaleRangeVal;
	json ScaleRangeStartVal;
	json ScaleRangeEndVal;
	ScaleRangeStartVal["min"] << mScaleRange[START_VAL][MIN_VAL];
	ScaleRangeStartVal["max"] << mScaleRange[START_VAL][MAX_VAL];
	ScaleRangeVal["start"] = ScaleRangeStartVal;
	ScaleRangeEndVal["min"] << mScaleRange[END_VAL][MIN_VAL];
	ScaleRangeEndVal["max"] << mScaleRange[END_VAL][MAX_VAL];
	ScaleRangeVal["end"] = ScaleRangeEndVal;
	emitPropVal["ScaleRange"] = ScaleRangeVal;

	//f32	   mRotRange[2][2];
	json RotRangeVal;
	json RotRangeStartVal;
	json RotRangeEndVal;
	RotRangeStartVal["min"] << mRotRange[START_VAL][MIN_VAL];
	RotRangeStartVal["max"] << mRotRange[START_VAL][MAX_VAL];
	RotRangeVal["start"] = RotRangeStartVal;
	RotRangeEndVal["min"] << mRotRange[END_VAL][MIN_VAL];
	RotRangeEndVal["max"] << mRotRange[END_VAL][MAX_VAL];
	RotRangeVal["end"] = RotRangeEndVal;
	emitPropVal["RotRange"] = RotRangeVal;

	//// Emitter Type
	//enum EEmitterShape { ES_POINT, ES_RECT, ES_CIRCLE, ES_LINE, ES_COUNT };
	//EEmitterShape mEmitShapeType;
	emitPropVal["EmitShapeType"] << mEmitShapeType;

	//// Should the shape be filled
	//// Only applies when mEmitShapeType is ES_RECT or ES_CIRCLE
	//bool mbEmitShapeFill;
	emitPropVal["EmitShapeFill"] << mbEmitShapeFill;

	//// rect/fill-rect data
	//AEVec2 mEmitShapeOffset;
	emitPropVal["EmitShapeOffset"] << mEmitShapeOffset;

	//AEVec2 mEmitShapeSize;
	emitPropVal["EmitShapeSize"] << mEmitShapeSize;

	//// circle data: Emit Angle limits
	//f32 mEmitShapeAngle[2];
	json emitShapeAngleVal;
	/*lifetimeVal*/emitShapeAngleVal["min"] << mEmitShapeAngle[MIN_VAL];
	/*lifetimeVal*/emitShapeAngleVal["max"] << mEmitShapeAngle[MAX_VAL];
	emitPropVal["EmitShapeAngle"] = emitShapeAngleVal;

	//// line data
	//AEVec2 mEmitShapeLineStart;	// Start position of the line emitter (with respect to the emitter transform).
	emitPropVal["EmitShapeLineStart"] << mEmitShapeLineStart;

	//AEVec2 mEmitShapeLineEnd;	// End position of the line emitter (with respect to the emitter transform).
	emitPropVal["EmitShapeLineEnd"] << mEmitShapeLineEnd;

	value["EmitterProperties"] = emitPropVal;
}


void ParticleEmitter::FromJson(json & value)
{
	try {
		this->Renderable::FromJson(value);

		if (mEmitProperties != nullptr)
			mEmitProperties->FromJson(value);
		else
			ASSERT(0, "Emitter properties has not been created yet.");

		std::string metaFile;
		value["Meta File"] >> metaFile;

		fs::path metaPath(metaFile);
		json val;

		if (!fs::exists(metaPath))
		{
			metaPath.replace_extension(".png");

			if (!fs::exists(metaPath))
				metaPath.replace_extension(".jpg");

			mTex = aexResourceMgr->getResource<Texture>(metaPath.string().c_str());
			mTex->ToJson(val);
			return;
		}

		LoadFromFile(val, metaFile);

		std::string path;
		val["Relative Path"] >> path;

		mTex = aexResourceMgr->getResource<Texture>(path.c_str());

	} CATCH_FS_ERROR
}

void ParticleEmitter::ToJson(json & value)
{
	this->Renderable::ToJson(value);

	if (mEmitProperties != nullptr)
		mEmitProperties->ToJson(value);

	mTex->ToJson(value);
}

bool ParticleEmitter::CheckCameraCulling()
{
	f32 aspect = WindowMgr->GetAspectRatio();
	AEVec2 particleSize = AEVec2(mEmitProperties->mScaleRange[START_VAL][MAX_VAL]) + mOwner->GetComp<TransformComp>()->mLocal.mScale;
	AEVec2 particlePos(mOwner->GetComp<TransformComp>()->mLocal.mTranslationZ.x, mOwner->GetComp<TransformComp>()->mLocal.mTranslationZ.y);
	for (auto it = GfxMgr->mCameraList.begin(); it != GfxMgr->mCameraList.end(); it++)
	{
		if (!(*it)->mOwner)
			continue;

		AEVec2 cameraPos = AEVec2((*it)->mOwner->GetComp<TransformComp>()->mLocal.mTranslationZ.x, (*it)->mOwner->GetComp<TransformComp>()->mLocal.mTranslationZ.y);

		if (StaticRectToStaticRect(&cameraPos, 2.f * aspect * (*it)->mViewRectangleSize, 2.f * (*it)->mViewRectangleSize, &particlePos, particleSize.x, particleSize.y))
			return true;
	}

	return false;
}

/**************************************************************************/
/*!
\fn
RandomPosRect

\brief
Computes a random position from a rectangle.

\param rectOffset
The position of the rectangle.

\param rectSize
The size of the rectangle.

\param filled
The checkup if the position should be on the circle's perimeter or inside of it.

\return
A random position on a rectangle.
*/
/**************************************************************************/
AEVec2 RandomPosRect(AEVec2 rectOffset, AEVec2 rectSize, bool filled)
{
	// Compute the resultant position of the rect
	AEVec2 randRect;
	randRect.x = AEX::RandFloat(-rectSize.x / 2.0f, rectSize.x / 2.0f);
	randRect.y = AEX:: RandFloat(-rectSize.y / 2.0f, rectSize.y / 2.0f);

	// If the rectangle is filled, just return the computed vector
	if (filled)
		return randRect;

	// Use a random Boolean to determine the resultant position of the particle
	bool XorY = RandBool();
	if (XorY) {
		// Compute the resultant y coordinate of the particle
		f32 yCorner = (randRect.y < 0.0f) ? -rectSize.y / 2.0f : rectSize.y / 2.0f;
		AEVec2 result(randRect.x, yCorner);
		// Increment it by the offset of the rectangle
		result += rectOffset;
		// Return the resultant position
		return result;
	}
	else {
		// Compute the resultant x coordinate of the particle
		f32 xCorner = (randRect.x < 0.0f) ? -rectSize.x / 2.0f : rectSize.x / 2.0f;
		AEVec2 result(xCorner, randRect.y);
		// Increment it by the offset of the rectangle
		result += rectOffset;
		// Return the resultant position
		return result;
	}
}

/**************************************************************************/
/*!
\fn
RandomPosCircle

\brief
Computes a random position from a circle.

\param circlePos
The position of the center of the circle.

\param circleSize
The size of the circle.

\param minAngle
The minimum angle of the circle.

\param maxAngle
The maximum angle of the circle.

\param filled
The checkup if the position should be on the circle's perimeter or inside of it.

\return
A random position on a circle / oval.
*/
/**************************************************************************/
AEVec2 RandomPosCircle(AEVec2 circlePos, AEVec2 circleSize, f32 minAngle, f32 maxAngle, bool filled)
{
	// Compute a random angle between the inputted range
	f32 resultantAngle = AEX::RandFloat(minAngle, maxAngle);
	// Compute the resultant position of the circle (normalized)
	f32 fromCircleDist = filled ? AEX::RandFloat(0.0f, 1.0f) : 1.0f;

	// Compute the resultant position of the particle
	AEVec2 result;
	result.x = circleSize.x * cos(resultantAngle);
	result.y = circleSize.y * sin(resultantAngle);
	result *= fromCircleDist;
	result += circlePos;
	return result;
}

/**************************************************************************/
/*!
\fn
RandomPosLine

\brief
Computes a random position from a line segment.

\param start
Starting point of the line segment.

\param end
End point of the line segment.

\return
A random position on a line segment defined by start and end.
*/
/**************************************************************************/
AEVec2 RandomPosLine(AEVec2 start, AEVec2 end)
{
	// Compute a random float
	float randomNum = (float)rand() / (float)RAND_MAX;
	// Compute the differential between these values
	AEVec2 diff = end - start;
	// Compute the resultant random position
	AEVec2 result = start + diff * randomNum;
	// Return the result of the computation
	return result;
}
// ----------------------------------------------------------------------------

/**************************************************************************/
/*!
\fn
RandBool

\brief
Computes a random boolean (just true or false).

\return
Just a random boolean.
*/
/**************************************************************************/
bool RandBool()
{
	// Compute the resultant value of the boolean
	unsigned result = rand() % 2;

	// If result is 1, return true
	if (result)
		return true;

	//If not, return false
	return false;
}

#pragma endregion
#pragma endregion