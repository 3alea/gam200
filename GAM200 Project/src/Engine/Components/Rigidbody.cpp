#include "Composition\AEXGameObject.h"
#include "Components\AEXTransformComp.h"
#include "Physics\PhysicsManager.h"
#include "Platform\AEXTime.h"
#include "Composition\AEXFactory.h"
#include "Physics\CollisionSystem.h"
#include "Imgui\imgui.h"
#include "Rigidbody.h"
#include "src/Engine/Components/Collider.h"
#include "src/Engine/Composition/AEXSpace.h"
#include "src/Engine/Components/IgnoreGravityEffect.h"

namespace AEX
{
	void Rigidbody::Initialize()
	{
		mPosition = GetOwnerPosition();

		mGravity = mOwner->GetParentSpace()->GetGravity();

		IgnoreGravityEffect* ignoreGravity = mOwner->GetComp<IgnoreGravityEffect>();

		if (ignoreGravity)
			mIgnoreGravity = true;

		ICollider* collider = mOwner->GetComp<ICollider>();

		if (collider != NULL)
		{
			mCollider = collider;
			collider->mRigid = this;
			collider->ChangeColliderState(mDynamicState);
		}
	}

	Rigidbody::Rigidbody() : IComp(), mInvMass(1.0f), mDrag(0.990f),
							mPrevPosition(0, 0, 0), mVelocity(0,0),
							mAcceleration(0,0), mGravity(NULL),// mPosition(GetOwnerPosition()),
							mDynamicState(DYNAMIC), mCollider(NULL),
							mIgnoreGravity(false), mBounce(true)
	{
		// Add the rigid body to the all rigid bodys vector
		aexPhysicsMgr->AddComponent(this);
	}

	Rigidbody::~Rigidbody()
	{
		// Remove the rigid body from the physics manager vector
		aexPhysicsMgr->RemoveComponent(this);

		// Check if a collider is pointing to this rigid body
		if (mCollider)
		{
			// Pass the collider to the static vector
			mCollider->ChangeColliderState(STATIC);
		
			// Stop pointing to this
			mCollider->mRigid = NULL;
		}
	}

	AEVec3 * Rigidbody::GetOwnerPosition()
	{
		if (mOwner)
		{
			TransformComp * trComp = mOwner->GetComp<TransformComp>();
			return &(trComp->mLocal.mTranslationZ);	// Before, this was mWorld, but made prototype behave weird
		}
		else return NULL;
	}

	void Rigidbody::ChangeState(DynamicState state)
	{
		mDynamicState = state;
	}

	void Rigidbody::Integrate(float timeStep)
	{
		// Check if it is a static object
		if (mInvMass == 0.0f)
		{
			return; // Done
		}

		// Save the position
		mPrevPosition = *mPosition;

		// Calculate the acceleration
		AEVec2 acceleration = mAcceleration * mInvMass;

		// Calculate the velocity and add it
		mVelocity += acceleration * timeStep;

		// Take into account the drag effect
		mVelocity *= mDrag;

		// Update the position
		mPosition->x += mVelocity.x * timeStep;
		mPosition->y += mVelocity.y * timeStep;

		// Clear the forces
		mAcceleration = { 0.0f, 0.0f };
	}

	void Rigidbody::AddForce(const AEVec2 & force)
	{
		mAcceleration += force;
	}

	void Rigidbody::Update()
	{
		// If the rigid body is static or kynematic gravity should not affect the object
		if (mDynamicState == STATIC || mDynamicState == KYNEMATIC)
			return;

		if (mInvMass && !mIgnoreGravity)
			AddForce(*mGravity / mInvMass);

		// Integrate physics
		// HOT FIX: CAREFULL - FRAME RATE WILL NOW ALWAYS BE 60FPS!!!

		float dt = (float)aexTime->GetFrameTime();

		if (dt > (1.0f / 24.0f))
			dt = 1.0f / 24.0f;

		Integrate(dt);//1.f/60.f);
	}

	void Rigidbody::SetGravity(AEVec2* newGravity)
	{
		mGravity = newGravity;
	}

	void Rigidbody::Shutdown()
	{

	}

	bool Rigidbody::OnGui()
	{
		bool changed = false;


		float velocity[2]{ mVelocity.x, mVelocity.y };
		ImGui::Text("Linear velocity");
		if(ImGui::InputFloat2("Linear velocity", velocity)) changed = true;
		mVelocity.x = velocity[0];
		mVelocity.y = velocity[1];

		//float gravity[2]{ mGravity.x, mGravity.y };
		//ImGui::Text("Gravity");
		//if(ImGui::InputFloat2("Gravity", gravity)) changed = true;
		//mGravity.x = gravity[0];
		//mGravity.y = gravity[1];

		ImGui::Text("Mass");
		if(ImGui::InputFloat("Mass", &mInvMass)) changed = true;

		ImGui::Text("Linear Drag");
		if(ImGui::InputFloat("Linear Drag", &mDrag)) changed = true;

		int dynamicState = mDynamicState;
		if(ImGui::RadioButton("Static", &dynamicState, STATIC)) changed = true; ImGui::SameLine();
		if(ImGui::RadioButton("Dynamic", &dynamicState, DYNAMIC)) changed = true; ImGui::SameLine();
		//if(ImGui::RadioButton("Kinematic", &dynamicState, KYNEMATIC)) changed = true;

		if (dynamicState != mDynamicState)
		{
			switch (dynamicState)
			{
			case STATIC:
			{
				mDynamicState = STATIC;
				break;
			}

			case DYNAMIC:
			{
				mDynamicState = DYNAMIC;
				break;
			}

			//case KYNEMATIC:
			//	mDynamicState = KYNEMATIC;
			//	break;
			}
		}

		if (mCollider)
			mCollider->ChangeColliderState(mDynamicState);

		ImGui::Text("Bounce");
		if (ImGui::Checkbox("Bounce", &mBounce)) changed = true;

		return changed;
	}
	
	void Rigidbody::FromJson(json & value)
	{
		this->IComp::FromJson(value);

		value["PrevPosition"] >> mPrevPosition;
		value["Velocity"] >> mVelocity;
		value["Acceleration"] >> mAcceleration;
		//value["Gravity"] >> mGravity;
		value["InvMass"] >> mInvMass;
		value["Drag"] >> mDrag;
		int temp;
		value["Dynamic State"] >> temp;
		mDynamicState = (Rigidbody::DynamicState)temp;
		value["mIgnoreGravity"] >> mIgnoreGravity;
		value["Bounce"] >> mBounce;
	}

	void Rigidbody::ToJson(json & value)
	{
		this->IComp::ToJson(value);

		value["PrevPosition"] << mPrevPosition;
		value["Velocity"] << mVelocity;
		value["Acceleration"] << mAcceleration;
		//value["Gravity"] << mGravity;
		value["InvMass"] << mInvMass;
		value["Drag"] << mDrag;
		value["Dynamic State"] << int(mDynamicState);
		value["mIgnoreGravity"] << mIgnoreGravity;
		value["Bounce"] << mBounce;
	}
}