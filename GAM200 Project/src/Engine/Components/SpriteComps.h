#pragma once
#include "Graphics/Texture.h"
#include "Graphics/Sprite.h"
#include "Renderable.h"



namespace AEX
{
	struct SpriteAnim;
	template <typename T>class TResource;
	

	class SpriteComp : public Renderable
	{
	public:
		AEX_RTTI_DECL(SpriteComp, Renderable);
	
		SpriteComp(bool shouldPush = true);
		~SpriteComp();

		void Initialize() override;
		void Update() override;
		void Render(Camera* cam) override;
		void Shutdown() override;
	
		bool OnGui() override;

		void FromJson(json & value);
		void ToJson(json & value);
	
		// Texture * mTex;
		bool mbShouldPush;
		TResource<Texture> * mTex;
	};
	

	struct SpriteAnimationComp : public Renderable
	{
		AEX_RTTI_DECL(SpriteAnimationComp, Renderable);
	
		SpriteAnimationComp();
		~SpriteAnimationComp();
		void Initialize() override;
		void Update() override;
		void Render(Camera* cam) override;
		void Shutdown() override;
	
		bool OnGui() override;
	
		void FromJson(json & value);
		void ToJson(json & value);

		//SpriteAnim* mSprite;

		TResource<SpriteAnim> * mSprite;
	};
}