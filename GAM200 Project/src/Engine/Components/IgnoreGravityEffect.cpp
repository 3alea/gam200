#include "IgnoreGravityEffect.h"
#include "src/Engine/Components/Rigidbody.h"
#include "src/Engine/Composition/AEXGameObject.h"

namespace AEX
{
	void IgnoreGravityEffect::Initialize()
	{
		Rigidbody* rb = mOwner->GetComp<Rigidbody>();

		if (rb)
			rb->mIgnoreGravity = true;
	}

	void IgnoreGravityEffect::Shutdown()
	{
		Rigidbody* rb = mOwner->GetComp<Rigidbody>();

		if (rb)
			rb->mIgnoreGravity = false;
	}

	bool IgnoreGravityEffect::OnGui()
	{
		return false;
	}
}
