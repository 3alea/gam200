#include "AEXComponents.h"
#include "Imgui\imgui.h"
#include <experimental/filesystem>


#include <iostream>

namespace fs = std::experimental::filesystem;

SpriteText::SpriteText(const char * fontPath, const char * text)
{
	strcpy_s(mText, text);
	mFont = aexResourceMgr->getResource<Font>(fontPath);
	mShader = aexResourceMgr->getResource<Shader>("data\\Shaders\\Text.shader");
}

SpriteText::~SpriteText()
{
	Shutdown();
}

void SpriteText::Initialize()
{
	glEnable(GL_CULL_FACE);

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(f32) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(f32), 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	GfxMgr->mRenderableList.push_back(this);
}

void SpriteText::Update()
{
	Renderable::Update();
}

void SpriteText::Render(Camera * cam)
{
	glEnable(GL_CULL_FACE);
	// Activate corresponding render state
	Shader * actualShader = mShader->get();
	glm::mat4 transform = GetTransformCompFromOwner->GetModelToWorld4x4();
	actualShader->setMat4(1, glm::value_ptr(transform));

	actualShader->setVec4("myColor", mColor[0], mColor[1], mColor[2], mColor[3]);

	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(VAO);

	std::string text(mText);

	AEVec2 totalOffset(0.0f, 0.0f);

	const f32 defaultSize = 100.0f;

	Font * actualFont = mFont->get();

	ASSERT(actualFont, "The actual font is null");
	check_gl_error();

	// Iterate through all characters
	std::string::iterator c;
	for (c = text.begin(); c != text.end(); c++)
	{
		Character ch = actualFont->GetCharacter(*c);

		f32 xpos = totalOffset.x + ch.Bearing.x * mSize / defaultSize;
		f32 ypos = totalOffset.y - (ch.Size.y - ch.Bearing.y) * mSize / defaultSize;

		f32 w = ch.Size.x * mSize / defaultSize;
		f32 h = ch.Size.y * mSize / defaultSize;
		// Update VBO for each character
		f32 vertices[6][4] = {
			{ xpos,     ypos + h,   0.0, 0.0 },
			{ xpos,     ypos,       0.0, 1.0 },
			{ xpos + w, ypos,       1.0, 1.0 },

			{ xpos,     ypos + h,   0.0, 0.0 },
			{ xpos + w, ypos,       1.0, 1.0 },
			{ xpos + w, ypos + h,   1.0, 0.0 }
		};
		// Render glyph texture over quad
		glBindTexture(GL_TEXTURE_2D, ch.TextureID);
		// Update content of VBO memory
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		check_gl_error();
		// Render quad
		glDrawArrays(GL_TRIANGLES, 0, 6);
		check_gl_error();
		// Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
		totalOffset.x += (ch.Advance >> 6) * mSize / defaultSize; // Bitshift by 6 to get value in pixels (2^6 = 64)
	}

	glDisable(GL_CULL_FACE);
	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void SpriteText::Shutdown()
{
	Renderable::Shutdown();
}

bool SpriteText::OnGui()
{
	bool changed = false;

	if(ImGui::InputText("Input Text", mText, IM_ARRAYSIZE(mText))) changed = true;

	float col[4] = { mColor[0], mColor[1], mColor[2], mColor[3] };
	if (ImGui::ColorEdit4("Text Color", col))
		for (int i = 0; i < 4; i++)
			mColor[i] = col[i];
	

	// Miguel
	static std::string actualName = "Select Here...";
	static bool isPathShown = false;
	std::string previewName;

	ImGui::Checkbox("Show Paths", &isPathShown);

	// To solve bug where currently selected texture wouldn't change
	// to only the name as soon as you unclicked the checkbox
	if (!isPathShown)
		previewName = aexResourceMgr->GetNameWithExtension(actualName);
	else
		previewName = actualName;

	if (ImGui::BeginCombo("Texture", previewName.c_str()))
	{
		try {
			fs::path textureDir = fs::path("data\\Fonts");

			int count = 0;
			for (auto dirIt = fs::recursive_directory_iterator(textureDir);
				dirIt != fs::recursive_directory_iterator();
				dirIt++, ++count)
			{
				//if (dirIt->is_directory())
				//continue;
				if (fs::is_directory(dirIt->path()))
					continue;

				if (dirIt->path().extension().string() == ".meta")
					continue;

				std::string currentFileName;

				if (isPathShown)
					currentFileName = dirIt->path().relative_path().string();
				else
					currentFileName = dirIt->path().filename().string();

				if (ImGui::Selectable((currentFileName + "##" + std::to_string(count)).c_str()))
				{
					actualName = dirIt->path().relative_path().string().c_str();
					mFont = aexResourceMgr->getResource<Font>(actualName.c_str());
				}
			}
		} CATCH_FS_ERROR

			ImGui::EndCombo();
	}
	//ImGui::SameLine(); HelpMarker("USER:\nHold SHIFT or use mouse to select text.\n" "CTRL+Left/Right to word jump.\n" "CTRL+A or double-click to select all.\n" "CTRL+X,CTRL+C,CTRL+V clipboard.\n" "CTRL+Z,CTRL+Y undo/redo.\n" "ESCAPE to revert.\n\nPROGRAMMER:\nYou can use the ImGuiInputTextFlags_CallbackResize facility if you need to wire InputText() to a dynamic string type. See misc/cpp/imgui_stdlib.h for an example (this is not demonstrated in imgui_demo.cpp).");


	return changed;
}

void SpriteText::FromJson(json & value)
{
	try {
		this->Renderable::FromJson(value);

		std::string tText;
		value["Text"] >> tText;
		strcpy(mText, tText.c_str());


		std::string metaFile;
		value["Meta File"] >> metaFile;

		fs::path metaPath(metaFile);
		json val;

		if (!fs::exists(metaPath))
		{
			metaPath.replace_extension(".ttf");

			mFont = aexResourceMgr->getResource<Font>(metaPath.string().c_str());
			mFont->ToJson(val);
			return;
		}

		LoadFromFile(val, metaFile);

		std::string path;
		val["Relative Path"] >> path;

		mFont = aexResourceMgr->getResource<Font>(path.c_str());

	} CATCH_FS_ERROR	
}

void SpriteText::ToJson(json & value)
{
	this->Renderable::ToJson(value);

	std::string tText(mText);
	value["Text"] = tText;
	mFont->ToJson(value);
}
