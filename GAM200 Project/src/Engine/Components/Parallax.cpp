#include "Parallax.h"
#include "Composition/AEXScene.h"
#include "Editor/Editor.h"
#include <cstdio>

ParallaxComp::ParallaxComp()
{

}

void ParallaxComp::Initialize()
{
	mOriginalPos = mOwner->GetComp<TransformComp>()->mLocal.mTranslationZ;
}

void ParallaxComp::Update()
{
	if (GameObject* target = aexScene->FindObjectInANYSpace("camera"))
	{
		if (TransformComp* objTrans = target->GetComp<TransformComp>())
		{
			AEVec3 targetPos = objTrans->mWorld.mTranslationZ;

			AEVec2 finalPos = AEVec2(mOriginalPos.x - mOffset.x * (targetPos.x - mCenter.x), mOriginalPos.y - mOffset.y * (targetPos.y - mCenter.x));

			if (!mbLimitsOnX)
			{
				finalPos.x = finalPos.x < mMinLimits.x ? mMinLimits.x : finalPos.x;
				finalPos.x = finalPos.x > mMaxLimits.x ? mMaxLimits.x : finalPos.x;
			}
			else if (TransformComp* tr = mOwner->GetComp<TransformComp>()) finalPos.x = tr->mLocal.mTranslationZ.x;

			if (!mbLimitsOnY)
			{
				finalPos.y = finalPos.y < mMinLimits.y ? mMinLimits.y : finalPos.y;
				finalPos.y = finalPos.y > mMaxLimits.y ? mMaxLimits.y : finalPos.y;
			}
			else if(TransformComp* tr = mOwner->GetComp<TransformComp>()) finalPos.y = tr->mLocal.mTranslationZ.y;

			mOwner->GetComp<TransformComp>()->mLocal.mTranslationZ = AEVec3(finalPos.x, finalPos.y, mOriginalPos.z);
		}
	}
}

bool ParallaxComp::OnGui()
{
	bool isChanged = false;

	ImGui::Text("Parallax offset");
	if (ImGui::InputFloat2("Parallax offset", mOffset.v)) isChanged = true;

	if (ImGui::Checkbox("Lock on X", &mbLimitsOnX)) isChanged = true;
	if (ImGui::Checkbox("Lock on Y", &mbLimitsOnY)) isChanged = true;

	ImGui::Text("Min Limits");
	if (ImGui::InputFloat2("Min Limits", mMinLimits.v)) isChanged = true;

	ImGui::Text("MaxLimits");
	if (ImGui::InputFloat2("MaxLimits", mMaxLimits.v)) isChanged = true;

	ImGui::Text("Center");
	if (ImGui::InputFloat2("Center", mCenter.v)) isChanged = true;

	//char * string;
	//if (ImGui::InputText("input text", mTargetName, IM_ARRAYSIZE(mTargetName))) isChanged = true;

	return isChanged;
}

// FromJson specialized for this component
void ParallaxComp::FromJson(json & value)
{
	//std::string target(mTargetName);
	this->IComp::FromJson(value);

	value["ParallaxOffset"] >> mOffset;
	//value["TargetName"] >> target;
	value["MinLimits"] >> mMinLimits;
	value["MaxLimits"] >> mMaxLimits;
	value["Center"] >> mCenter;
	value["LimitsOnX"] >> mbLimitsOnX;
	value["LimitsOnY"] >> mbLimitsOnY;

	/*sprintf(buffer, "%.4s", your_string.c_str());
	strcpy(mTargetName, target.c_str());*/
}

// ToJson specialized for this component
void ParallaxComp::ToJson(json & value)
{
	this->IComp::ToJson(value);

	value["ParallaxOffset"] << mOffset;
	//value["TargetName"] << mTargetName;
	value["MinLimits"] << mMinLimits;
	value["MaxLimits"] << mMaxLimits;
	value["Center"] << mCenter;
	value["LimitsOnX"] << mbLimitsOnX;
	value["LimitsOnY"] << mbLimitsOnY;
}