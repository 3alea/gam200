#include "Logic.h"
#include "src/Engine/Events/type_info.hh"
#include <iostream>

namespace AEX
{
	//-------------------------------------------------------------------------
#pragma region // Base Logic Component

	LogicComp::LogicComp() : IComp(), event_handler(this)
	{
		Initialize();
		eventDispatcher.subscribe(*this, type_of<CollisionEvent>());
		eventDispatcher.subscribe(*this, type_of<CollisionStarted>());
		eventDispatcher.subscribe(*this, type_of<CollisionEnded>());

	}
	void LogicComp::FromJson(json & value)
	{
		//this->IComp::FromJson(value);
		SetBaseName(value["Name"].get<std::string>().c_str());
		SetEnabled(value["Enabled"].get<bool>());
		mbRemovable = value["Removable"].get<bool>();
		mbGlobalComp = value["GlobalComponent"].get<bool>();
	}
	void LogicComp::ToJson(json & value)
	{
		//this->IComp::ToJson(value);
		value["Name"] = GetBaseName();
		value["RTTI Type"] = GetType().GetName();
		value["Enabled"] = mbEnabled;
		value["Removable"] = mbRemovable;
		value["GlobalComponent"] = mbGlobalComp;
	}
	void LogicComp::Initialize() {
		aexLogic->AddComp(this);
	}
	void LogicComp::Shutdown() {
		eventDispatcher.unsubscribe(*this, type_of<CollisionStarted>());
		eventDispatcher.unsubscribe(*this, type_of<CollisionEvent>());
		eventDispatcher.unsubscribe(*this, type_of<CollisionEnded>());

		aexLogic->RemoveComp(this);
	}
	void LogicComp::OnCollision(const CollisionEvent & collision)
	{
		if (collision.thisObject == this->mOwner)
			OnCollisionEvent(collision);
	}

	void LogicComp::collisionStarted(const CollisionStarted & collision)
	{
		if (collision.thisObject == this->mOwner)
		{
			//
			//std::cout << "-------------------------------" << std::endl;
			//std::cout << " Collision started triggered" << std::endl;
			//std::cout << "-------------------------------" << std::endl;
			OnCollisionStarted(collision);
		}
	}

	void LogicComp::collisionEnded(const CollisionEnded & collision)
	{
		if (collision.thisObject == this->mOwner)
		{
			//std::cout << "-------------------------------" << std::endl;
			//std::cout << " Collision ended triggered" << std::endl;
			//std::cout << "-------------------------------" << std::endl;
			OnCollisionEnded(collision);
		}
	}

	void LogicComp::handle_event(const Event & event)
	{
		event_handler.handle(event);
	}

	LogicComp::~LogicComp() {
		Shutdown();
	}
#pragma endregion

	//-------------------------------------------------------------------------
#pragma region // Logic System
	//Logic::Logic() {}

	void Logic::Update()
	{
		for (auto comp = mComps.begin();comp != mComps.end(); comp++)
		{
			if ((*comp)->IsEnabled())
				(*comp)->Update();
		}
	}

	// component management
	void Logic::AddComp(IComp * logicComp) {
		mComps.remove(logicComp); // no duplicates
		mComps.push_back(logicComp);
	}
	void Logic::RemoveComp(IComp * logicComp) {
		mComps.remove(logicComp);
	}
	void Logic::ClearComps() {
		mComps.clear();
	}

	LogicCompEventHandler::LogicCompEventHandler(LogicComp * LComp)
		: object(LComp)
	{
		register_handler<LogicComp, CollisionEvent>(*object, &LogicComp::OnCollision);
		register_handler<LogicComp, CollisionStarted>(*object, &LogicComp::collisionStarted);
		register_handler<LogicComp, CollisionEnded>(*object, &LogicComp::collisionEnded);
	}

	CollisionEvent::CollisionEvent(GameObject * obj1, GameObject * obj2)
		: thisObject(obj1), otherObject(obj2)
	{
	}

#pragma endregion
	CollisionStarted::CollisionStarted(GameObject * obj1, GameObject * obj2)
		: thisObject(obj1), otherObject(obj2)
	{
	}

	CollisionEnded::CollisionEnded(GameObject * obj1, GameObject * obj2)
		: thisObject(obj1), otherObject(obj2)
	{
	}
}