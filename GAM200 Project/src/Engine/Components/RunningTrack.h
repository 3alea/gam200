#pragma once
#include "Logic.h"
#include "Imgui\imgui.h"
#include "SpineAnimation.h"

using namespace AEX;

class RunningTrack : public LogicComp
{
	AEX_RTTI_DECL(RunningTrack, LogicComp);
public:
	// Speed of the running track. Direction matters
	float speed = 0;

	// SpineAnimation
	SpineAnimationComp* spine;

	// Tracks starting direction
	float myOrientation = 0;

	// Start is called before the first frame update
	virtual void Initialize();

	// Update is called once per frame
	virtual void Update();

	// OnCollision event
	virtual void OnCollisionEvent(const CollisionEvent & collision);

	// OnGui() function
	virtual bool OnGui();
	
	// FromJson specialized for this component
	virtual void FromJson(json & value);
	// ToJson specialized for this component
	virtual void ToJson(json & value);

	// Choose running track animation type
	int runningAnimation;

private:
	float dashTimer;
	AEVec2 mDirection;
	bool canDash;
};