#include "../Components/AEXComponents.h"
#include "../Graphics/SpriteAnimation.h"
#include "../Imgui/imgui.h"

#include "../Graphics/GfxMgr.h"
#include "../Graphics/WindowMgr.h"

#include <./extern/glm/glm.hpp>
#include <./extern/glm/gtc/matrix_transform.hpp>
#include <./extern/glm/gtc/type_ptr.hpp>

//#include "../Graphics/Model.h"

//#include "Resources/AEXResourceManager.h"

Renderable::Renderable() : texSize(1.0f, 1.0f) 
{
	for (int i = 0; i < 4; i++)
		mColor[i] = 1.0f;

	VAO = VBO = EBO = 0;
}

void Renderable::Initialize()
{
	//mModel = aexResourceMgr->getResource<Model>("Quad.model");

	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Initialize Render data
	float mVertices[] = {
		// positions        
	     0.5f,  0.5f, 0.0f,  // top right
		 0.5f, -0.5f, 0.0f,  // bottom right
		-0.5f, -0.5f, 0.0f,  // bottom left
		-0.5f,  0.5f, 0.0f,  // top left
	};

	unsigned int indices[] = {  // note that we start from 0!
		0, 1, 3,   // first triangle
		1, 2, 3    // second triangle
	};

	

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);

	// ..:: Initialization code :: ..
	// 1. bind Vertex Array Object
	glBindVertexArray(VAO);
	// 2. copy our vertices array in a vertex buffer for OpenGL to use
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(mVertices), mVertices, GL_STATIC_DRAW);
	// 3. copy our index array in a element buffer for OpenGL to use
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
	// 4. then set the vertex attributes pointers
	// position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// remember: do NOT unbind the EBO while a VAO is active as the bound element buffer object IS stored in the VAO; keep the EBO bound.
	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
	// VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
	glBindVertexArray(0);

	mbVisible = true;
	mBlending = new BlendingController();
	//mShader = CreateShader("data/Shaders/TextureColor.vs", "data/Shaders/TextureColor.fs");
	mShader = aexResourceMgr->getResource<Shader>("data\\Shaders\\TextureColor.shader");
	flipX = false;
	flipY = false;
}

void Renderable::Update()
{
}

// Commented out by Miguel
//Shader * Renderable::CreateShader(const char * vtxPath, const char * fragPath)
//{
//	mShader = new Shader(vtxPath, fragPath);
//	return mShader;
//}

void Renderable::Render(Camera* cam)
{
	f32 xFlip = flipX ? -texSize.x : texSize.x;
	f32 yFlip = flipY ? -texSize.y : texSize.y;
	AEVec2 texel(xFlip, yFlip);

	Shader * actualShader = mShader->get();
	if (actualShader == nullptr)
	{
		//std::cout << "The actual shader is NULL" << std::endl;
		return;
	}

	actualShader->use();
	actualShader->setVec2("texSize", texel);
	mBlending->SetBlendAttributes();

	glBindVertexArray(VAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

void Renderable::Shutdown()
{
	GfxMgr->mRenderableList.remove(this);
	if(VAO) glDeleteVertexArrays(1, &VAO);
	if (EBO) glDeleteBuffers(1, &EBO);
	if (VBO) glDeleteBuffers(1, &VBO);
}

bool Renderable::OnGui()
{
	return false;
}

void Renderable::FromJson(json & value)
{
	this->IComp::FromJson(value);

	json val = value["Color"];
	mColor[0] = val["r"].get<f32>();
	mColor[1] = val["g"].get<f32>();
	mColor[2] = val["b"].get<f32>();
	mColor[3] = val["a"].get<f32>();
	
	if (value["Flip X"] != nullptr)
		value["Flip X"] >> flipX;

	if (value["Flip Y"] != nullptr)
		value["Flip Y"] >> flipY;

	value["Texture Size"] >> texSize;

	if (value["Visible"] != nullptr)
		value["Visible"] >> mbVisible;

	if (value["Model"] != nullptr) {
		std::string modelName;
		value["Model"] >> modelName;
		//mModel = aexResourceMgr->getResource<Model>(modelName.c_str()));
	}
}
void Renderable::ToJson(json & value)
{
	this->IComp::ToJson(value);

	json colorVal;
	colorVal["r"] = mColor[0];
	colorVal["g"] = mColor[1];
	colorVal["b"] = mColor[2];
	colorVal["a"] = mColor[3];
	value["Color"] = colorVal;

	value["Flip X"] << flipX;
	value["Flip Y"] << flipY;
	value["Texture Size"] << texSize;
	value["Visible"] << mbVisible;
}
