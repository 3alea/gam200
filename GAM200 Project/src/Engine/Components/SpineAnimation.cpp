#include <spine/spine.h>
#include <spine\Skeleton.h>
#include <spine\AnimationState.h>
#include <experimental/filesystem>
#include "SpineAnimation.h"
#include "Graphics\GfxMgr.h"
#include "Platform\AEXInput.h"
#include "Platform/AEXTime.h"
#include <spine\Bone.h>
#include <iostream>
#include <spine\Slot.h>
#include <spine\Attachment.h>
#include <spine\RegionAttachment.h>
#include <spine\MeshAttachment.h>
#include <spine\ClippingAttachment.h>
#include "Graphics\Color.h"
#include "Player/Movement/PlayerController.h" // Player aiming direction macros
#include "Player/Stats/PlayerStats.h"         // Player jump condition!
#include "Editor/Editor.h"                    // IsPlaying for delay

namespace fs = std::experimental::filesystem;

SpineAnimationComp::~SpineAnimationComp()
{
	if (mClipper)
	{
		mClipper->clipEnd();
		delete mClipper;
	}
		
	Shutdown();
}

void SpineAnimationComp::ReloadSpineData()
{
	if (mSkeleton)
	{
		delete mSkeleton;
	}
	if (mAnimationState)
	{
		delete mAnimationState;
	}

	// create the skeleton
	if (mSpineData->get()->GetSkeleton())
	{
		mSkeleton = new spine::Skeleton(mSpineData->get()->GetSkeleton());

		// set default skin
		if (mSpineData->get()->GetSkeleton()->getSkins().size())
			mSkeleton->setSkin(mSpineData->get()->GetSkeleton()->getSkins()[0]);
	}

	// create the animation state
	if (mSpineData->get()->GetAnimation())
		mAnimationState = new spine::AnimationState(mSpineData->get()->GetAnimation());

	mbVisible = true;
	texSize = AEVec2(1.0f, 1.0f);
	flipX = false;
	flipY = false;
	if (mSkeleton)
	{
		mSkeleton->setScaleX(Size.x);
		mSkeleton->setScaleY(Size.y);
	}
}

void SpineAnimationComp::Initialize()
{
	// Sanity check
	if (!mSpineData || !mSkeleton)
		ReloadSpineData();

	mShader = NULL;

	// get transform data, if any
	if (GetOwner())
		mTransform = dynamic_cast<TransformComp*>(GetOwner()->GetComp("AEX::TransformComp"));
	
	// create custom model for the rendering
	mSpineModel = new Model(Model::eTriangleList);

	GfxMgr->mRenderableList.push_back(this);

	mbVisible = true;
	mBlending = new BlendingController();
	//mShader = CreateShader("data/Shaders/TextureColor.vs", "data/Shaders/TextureColor.fs");
	mShader = aexResourceMgr->getResource<Shader>("data\\Shaders\\Spine.shader");
	texSize = AEVec2(1.0f, 1.0f);
	flipX = false;
	flipY = false;
	mSkeleton->setScaleX(Size.x);
	mSkeleton->setScaleY(Size.y);
	mAnimationState->setListener(callback);

	if (mAnimationState->getCurrent(0))
		backwardsTime = mAnimationState->getCurrent(0)->getAnimationEnd();
	else
		backwardsTime = (f32)aexTime->GetFrameTime();

	mChangedDt = false;
}

void SpineAnimationComp::Update()
{
	/*if (!superInitialized)
	{
		Initialize();
		superInitialized = true;
	}*/

	// If the animation has been paused, and the dt has not been changed in this frame
	if (mPaused)
		return;

	// Get the frame time
	if (mChangedDt == false)
		dt = (f32)aexTime->GetFrameTime();
	else
		mChangedDt = false;
	
	if (startTime > 0.f && Editorstate->IsPlaying())
	{
		startTime -= dt;
		return;
	}

	// Playing backwards
	if (playBackwards && mAnimationState->getCurrent(0))
	{
		// If the animation is done and it is not looping, remove from the track
		if (!mAnimationState->getCurrent(0)->getLoop() && isBackwardsComplete)
		{
			mAnimationState->clearTrack(0);
			if (mAnimationState->getCurrent(1))
				mAnimationState->clearTrack(1);
			return;
		}
		else
			isBackwardsComplete = false;

		// Stop time from moving forward
		mAnimationState->getCurrent(0)->setTimeScale(0.f);
		if(mAnimationState->getCurrent(1))
			mAnimationState->getCurrent(1)->setTimeScale(0.f);

		// Go back in time
		backwardsTime -= ((f32)aexTime->GetFrameTime() * goBackSpeed);

		// Restart animation if it reaches the end
		if (backwardsTime < 0.f)
		{
			// Say that the animation is done this frame
			isBackwardsComplete = true;
			if (mAnimationState->getCurrent(0)->getLoop())
				backwardsTime = mAnimationState->getCurrent(0)->getAnimationEnd();
		}

		// Set time to the time of animation going backwards
		mAnimationState->getCurrent(0)->setTrackTime(backwardsTime);
		if (mAnimationState->getCurrent(1))
			mAnimationState->getCurrent(1)->setTrackTime(backwardsTime);
	}
	else if (mAnimationState->getCurrent(0))
		mAnimationState->getCurrent(0)->setTimeScale(goBackSpeed);

	// Update animation and apply to the skeleton
	if(!playBackwards)
		mAnimationState->update(dt);

	if(mSkeleton)
		mAnimationState->apply(*mSkeleton);

	// Set the position of the skeleton from the transform (if any)
	if (mTransform)
		mSkeleton->setPosition(mTransform->mLocal.mTranslationZ.x + offset.x, mTransform->mLocal.mTranslationZ.y + offset.y);

	spine::Bone * aimBone = mSkeleton->findBone("Aim");

	// Aim update
	if (mOwner->GetBaseNameString() == "player" && aimBone)
	{
		// determine the orientaiton based on input. 
		static float orientation_angle = 45.f;
		if (PLAYER_GOING_RIGHT || PLAYER_GOING_LEFT)
		{
			if (PLAYER_GOING_UP)
				orientation_angle = 45.f;
			else
				orientation_angle = 0.f;
		}
		else if (PLAYER_GOING_UP)
		{
			orientation_angle = 90.f;
		}
		else
		{
			orientation_angle = 0.f;
		}
		
		// If the player is aiming downwards in the air
	    if (PLAYER_GOING_DOWN && !mOwner->GetComp<PlayerStats>()->isGrounded)
			orientation_angle = -90.f;

		// If the player is crouching
		if (PLAYER_GOING_DOWN && mOwner->GetComp<PlayerStats>()->canDuck == false)
		{
			orientation_angle = 10.f;
		}

		aimBone->setRotation(orientation_angle);
	}
	else if (aimBone)
	{
		aimBone->setRotation((mOwner->GetComp<TransformComp>()->mLocal.mOrientation * 180.f / PI) + 180.f);
	}

	// Car rotation bone
	spine::Bone * carBone = mSkeleton->findBone("Body");

	// Rotate the cars
	if (mOwner->GetComp<ICollider>() && mOwner->GetComp<ICollider>()->collisionGroup == "Floor" && carBone)
	{
		// Rotate the bone the amount set
		carBone->setRotation(carRotation);
	}

	// Update the world transform of the whole skeleton (bone hierarchy)
	mSkeleton->updateWorldTransform();
}

// Render function
void SpineAnimationComp::DebugRender()
{
	std::vector<spine::Bone*> boneStack;
	boneStack.push_back(mSkeleton->getRootBone());
	while (boneStack.size())
	{
		// get the current bone
		spine::Bone* bone = boneStack.back();
		boneStack.pop_back();
	
		// draw a circle a the bone's position
		DebugCircle debugCircle(AEVec2(bone->getWorldX(), bone->getWorldY()), 0.5f, 50, AEVec4(1.0f, 1.0f, 1.0f, 1.0f));
	
		GfxMgr->DrawCircle(debugCircle);
	
		// draw line to each children and add to bone stack
		for (u32 i = 0; i < bone->getChildren().size(); ++i)
		{
			// get reference to child bone
			spine::Bone* childBone = bone->getChildren()[i];
	
			// add to bone stack
			boneStack.push_back(childBone);
	
			// Create line
			DebugLine line;

			line.start = AEVec2(bone->getWorldX(), bone->getWorldY());
			line.end = AEVec2(childBone->getWorldX(), childBone->getWorldY());
			line.color = AEVec4(1.0f, 1.0f, 1.0f, 1.0f);

			// draw line
			GfxMgr->DrawLine(line);
		}
	}
}

// Render function temporarily only doing debugrender
void SpineAnimationComp::Render(Camera* cam)
{
	using namespace spine;
	// Debug
	//if (aexInput->KeyPressed(GLFW_KEY_LEFT_CONTROL) && aexInput->KeyPressed('D')) { DebugRender(); }


	// Create values for clipping in case this is necessary
	SkeletonClipping clipping;
	ClippingAttachment* clipAttachment = nullptr;

	// upload to the gpu
	if (mShader && mShader->get()) {
		mShader->get()->use();
		mShader->get()->setMat4(1, glm::value_ptr(glm::mat4(1.0f)));
		mShader->get()->setVec4("modulationColor", mColor[0], mColor[1], mColor[2], mColor[3]);
		mShader->get()->setInt("ourTexture", 0);
	}

	// clear the model
	mSpineModel->Clear();

	// Texture to be set and drawn later
	Texture* texture = NULL;

	// For each slot in the draw order array of the skeleton
	for (int i = 0, n = mSkeleton->getSlots().size(); i < n; ++i)
	{
		Slot* slot = mSkeleton->getDrawOrder()[i];
		if (!slot)
			continue;
		// Fetch the currently active attachment, continue
		// with the next slot in the draw order if no
		// attachment is active on the slot
		Attachment* attachment = slot->getAttachment();

		// If there is no attachment, check if clipping has ended
		if (!attachment)
		{
			clipping.clipEnd(*slot);
			continue;
		}

		// Get the colors saved in the spine animation
		spine::Color skeletonColor = mSkeleton->getColor();
		spine::Color slotColor = slot->getColor();
		::Color tint(skeletonColor.r * slotColor.r, skeletonColor.g * slotColor.g, skeletonColor.b * slotColor.b, skeletonColor.a * slotColor.a);

		// Fill the vertices array, indices, and texture depending on the type of attachment
		if (attachment->getRTTI().isExactly(RegionAttachment::rtti))
		{
			// Cast to an spRegionAttachment so we can get the rendererObject
			// and compute the world vertices
			RegionAttachment* regionAttachment = dynamic_cast<RegionAttachment*>(attachment);

			// Our engine specific Texture is stored in the AtlasRegion which was
			// assigned to the attachment on load. It represents the texture atlas
			// page that contains the image the region attachment is mapped to.
			AtlasRegion * region = (AtlasRegion*)regionAttachment->getRendererObject();
			auto tmpTexture = (Texture*)region->page->getRendererObject();

			// this is a new texture, we need to draw what we have
			if (texture && tmpTexture != texture) {

				if (texture)
					texture->Bind();
				mSpineModel->UploadToGPU();
				mSpineModel->Draw();

				// cleanup 
				mSpineModel->Clear();
			}
			texture = tmpTexture;

			// Ensure there is enough room for vertices
			//mVertices.setSize(4, Vertex());
			
			// Allocate memory for the vertices in use
			float * mVertexPositions = new float[8];

			// Computed the world vertices positions for the 4 vertices that make up
			// the rectangular region attachment. This assumes the world transform of the
			// bone to which the slot (and hence attachment) is attached has been calculated
			// before rendering via Skeleton::updateWorldTransform(). The vertex positions
			// will be written directoy into the vertices array, with a stride of sizeof(Vertex)
			regionAttachment->computeWorldVertices(slot->getBone(), mVertexPositions, 0, 2);

			// We will have to set the values of the vertices manually because we do not have a way to convert a spine::Vertex into our own Vertex.
			// Also, Spine's buffer is not the same as ours, doing a different behaviour.
			Vertex tempVertex;
			// copy color and UVs to the vertices
			tempVertex.mColor = tint;

			// Now we need to check if clipping is being done. In that case, we will need to render differently
			if (clipping.isClipping())
			{
				// We only have 2 triangles that make a quad, formated counter-clockwise
				unsigned short mTriangles[] = { 2,1,0,3,2,0 };
				clipping.clipTriangles(mVertexPositions, mTriangles, 6, regionAttachment->getUVs().buffer(), 2);

				// Get the triangle size to know when to end
				size_t triangleSize = clipping.getClippedTriangles().size();

				// For all of the triangles we need to draw, set the position and texture coordinate to the ones indicated
				for (size_t counter = 0; counter < triangleSize; counter++)
				{
					int index = 2 * clipping.getClippedTriangles()[counter];
					tempVertex.mPosition = AEVec2(clipping.getClippedVertices()[index], clipping.getClippedVertices()[index + 1]);
					tempVertex.mTexCoord = AEVec2(clipping.getClippedUVs()[index], 1 - clipping.getClippedUVs()[index + 1]);
					mSpineModel->AddVertex(tempVertex);
				}
			}
			else
			{
				// Bottom left vertex
				tempVertex.mPosition = AEVec2(mVertexPositions[0], mVertexPositions[1]);
				tempVertex.mTexCoord = AEVec2(regionAttachment->getUVs()[0], 1 - regionAttachment->getUVs()[1]); mSpineModel->AddVertex(tempVertex);

				// Bottom right vertex
				tempVertex.mPosition = AEVec2(mVertexPositions[2], mVertexPositions[3]);
				tempVertex.mTexCoord = AEVec2(regionAttachment->getUVs()[2], 1 - regionAttachment->getUVs()[3]); mSpineModel->AddVertex(tempVertex);

				// Top right vertex
				tempVertex.mPosition = AEVec2(mVertexPositions[4], mVertexPositions[5]);
				tempVertex.mTexCoord = AEVec2(regionAttachment->getUVs()[4], 1 - regionAttachment->getUVs()[5]); mSpineModel->AddVertex(tempVertex);

				// Bottom left vertex
				tempVertex.mPosition = AEVec2(mVertexPositions[0], mVertexPositions[1]);
				tempVertex.mTexCoord = AEVec2(regionAttachment->getUVs()[0], 1 - regionAttachment->getUVs()[1]); mSpineModel->AddVertex(tempVertex);

				// Top right vertex
				tempVertex.mPosition = AEVec2(mVertexPositions[4], mVertexPositions[5]);
				tempVertex.mTexCoord = AEVec2(regionAttachment->getUVs()[4], 1 - regionAttachment->getUVs()[5]); mSpineModel->AddVertex(tempVertex);

				// Top left vertex
				tempVertex.mPosition = AEVec2(mVertexPositions[6], mVertexPositions[7]);
				tempVertex.mTexCoord = AEVec2(regionAttachment->getUVs()[6], 1 - regionAttachment->getUVs()[7]); mSpineModel->AddVertex(tempVertex);
			}
			
			// Clear the vertices
			delete[] mVertexPositions;
		}
		if (attachment->getRTTI().isExactly(MeshAttachment::rtti))
		{
			if (clipping.isClipping())
			{
				return;
			}
			// Cast to an MeshAttachment so we can get the rendererObject
			// and compute the world vertices
			MeshAttachment* mesh = (MeshAttachment*)attachment;

			// Ensure there is enough room for vertices
			//mVertices.setSize(mesh->getWorldVerticesLength() / 2, Vertex());

			// Our engine specific Texture is stored in the AtlasRegion which was
			// assigned to the attachment on load. It represents the texture atlas
			// page that contains the image the region attachment is mapped to.
			AtlasRegion * region = (AtlasRegion*)mesh->getRendererObject();
			auto tmpTexture = (Texture*)region->page->getRendererObject();

			// this is a new texture, we need to draw what we have
			if (texture && tmpTexture != texture) {

				if (texture)
					texture->Bind();
				mSpineModel->UploadToGPU();
				mSpineModel->Draw();

				// cleanup 
				mSpineModel->Clear();
			}
			texture = tmpTexture;
			// Computed the world vertices positions for the vertices that make up
			// the mesh attachment. This assumes the world transform of the
			// bone to which the slot (and hence attachment) is attached has been calculated
			// before rendering via Skeleton::updateWorldTransform(). The vertex positions will
			// be written directly into the vertices array, with a stride of sizeof(Vertex)
			unsigned int numVertices = mesh->getWorldVerticesLength();

			// Allocate memory for the vertices in use
			float * mVertexPositions = new float[numVertices];

			//regionAttachment->computeWorldVertices(slot->getBone(), vertexPositions, 0, 2);
			mesh->computeWorldVertices(*slot, 0, numVertices, mVertexPositions, 0, 2);

			// triangles is an array of indices of the vertices in 'vertex_positions' that 
			// is used to construct the triangles of the mesh attachment
			auto& triangles = mesh->getTriangles();

			Vertex tempVertex;

			// Copy color and UVs to the vertices
			for (unsigned int j = 0; j < triangles.size(); j++)
			{
				unsigned short vertexIndex = triangles[j];
				
				// set the position
				tempVertex.mPosition.x = mVertexPositions[vertexIndex * 2];
				tempVertex.mPosition.y = mVertexPositions[vertexIndex * 2 + 1];

				// set the UVs
				tempVertex.mTexCoord.x = mesh->getUVs()[vertexIndex * 2];
				tempVertex.mTexCoord.y = 1.0f - mesh->getUVs()[vertexIndex * 2 + 1];

				// set the color
				tempVertex.mColor.set(tint);

				// Finally, add the vertex
				mSpineModel->AddVertex(tempVertex);
			}
			// Clear the vertices
			delete [] mVertexPositions;
		}
		else if (attachment->getRTTI().isExactly(spine::ClippingAttachment::rtti))
		{
			// Set up the clipping attachment to be applied to this region
			clipAttachment = (ClippingAttachment*)attachment;
			clipping.clipStart(*slot, clipAttachment);
		}

		// End clipping here
		//clipping.clipEnd(*slot);
	}
	//// set the texture
	if (texture)
		texture->Bind();

	// draw with the demo camera
	mSpineModel->UploadToGPU();
	mSpineModel->Draw();
}

void SpineAnimationComp::Shutdown()
{
	Renderable::Shutdown();
}

void SpineAnimationComp::ShowBoneData(spine::Bone * bone)
{
	// Display a button to show/not show children
	if (ImGui::TreeNode(bone->getData().getName().buffer()))
	{
		// Go through all child bones
		for (unsigned i = 0; i < bone->getChildren().size(); i++)
		{
			// Get the child bone
			spine::Bone* childBone = bone->getChildren()[i];
			// Display the child bone name + any children it may have
			ShowBoneData(childBone);
		}
		// Show the tree info
		ImGui::TreePop();
	}
}

bool SpineAnimationComp::OnGui()
{
	bool changed = false;

	// Miguel
	static std::string actualName = "spineboy.spine";
	static bool isPathShown = false;
	std::string previewName;

	if (ImGui::Checkbox("Show Paths", &isPathShown)) changed = true;

	float offsetTemp[2] = { offset.x, offset.y };
	if (ImGui::InputFloat2("Offset", offsetTemp))
	{
		offsetChanged = true;
		changed = true;
	}

	// Rotation of the car
	if (ImGui::InputFloat("Car rotation", &carRotation))
		changed = true;

	// Start time of the animation
	if (ImGui::InputFloat("Start time", &startTime))
		changed = true;

	// Speed
	if (ImGui::InputFloat("Speed", &speed))
		changed = true;

	if (offsetChanged)
	{
		offset.x = offsetTemp[0];
		offset.y = offsetTemp[1];
		offsetChanged = false;
	}

	// To solve bug where currently selected texture wouldn't change
	// to only the name as soon as you unclicked the checkbox
	if (!isPathShown)
		previewName = aexResourceMgr->GetNameWithExtension(actualName);
	else
		previewName = actualName;

	if (ImGui::BeginCombo("SpineData", previewName.c_str()))
	{
		try {
			fs::path textureDir = fs::path("data\\Spine");

			int count = 0;
			for (auto dirIt = fs::directory_iterator(textureDir);
				dirIt != fs::directory_iterator();
				dirIt++, ++count)
			{
				if (fs::is_directory(dirIt->path()) || dirIt->path().extension() == ".meta")
					continue;

				std::string currentFileName;

				if (isPathShown)
					currentFileName = dirIt->path().relative_path().string();
				else
					currentFileName = dirIt->path().filename().string();

				if (ImGui::Selectable((currentFileName + "##" + std::to_string(count)).c_str()))
				{
					actualName = dirIt->path().relative_path().string().c_str();
					mSpineData = aexResourceMgr->getResource<SpineData>(actualName.c_str());
					ReloadSpineData();
				}
			}
		} CATCH_FS_ERROR

		ImGui::EndCombo();
	}

	spine::Skin * currentSkin = mSkeleton->getSkin();
	auto skelData = mSkeleton->getData();
	auto animations = skelData->getAnimations();
	auto skins = skelData->getSkins();
	spine::Bone* rootBone = mSkeleton->getRootBone();

	if (ImGui::BeginCombo("Animations", "Animation")) 
	{
		for (u32 a = 0; a < animations.size(); ++a) {
			auto anim = animations[a];
			if (ImGui::Selectable(anim->getName().buffer()))
			{
				mAnimationState->setAnimation(0, anim, true);
			}
		}
		ImGui::EndCombo();
	}
	if (ImGui::BeginCombo("Skins", currentSkin->getName().buffer()))
	{
		for (u32 a = 0; a < skins.size(); ++a) {
			auto skin = skins[a];
			if (ImGui::Selectable(skin->getName().buffer()))
			{
				mSkeleton->setSkin(skin);
			}
		}
		ImGui::EndCombo();
	}
	// Show all of the bones
	if (ImGui::CollapsingHeader("Bones"))
	{
		// Rootbone is the very first bone where the rest derive from
		ShowBoneData(rootBone);
	}

	if (ImGui::InputFloat2("Scale", Size.v, 3))
	{
		changed = true;
	}

	if (ImGui::Checkbox("Backwards", &playBackwards))
	{
		changed = true;
	}

	return changed;
}

spine::Bone * SpineAnimationComp::GetBone(const char * boneName)
{
	//auto bone = mSkeleton->findBone(boneName);
	return nullptr;
}

bool SpineAnimationComp::CheckCurrentAnimationAtPostion(const char * animationName, const int pos)
{
	if (mAnimationState->getCurrent(pos) == nullptr)
		return false;
	return (mSkeleton->getData()->findAnimation(animationName) == mAnimationState->getCurrent(pos)->getAnimation());
}

void SpineAnimationComp::ChangeAnimation(const unsigned int position, const spine::String & name, bool shouldLoop, float speed, float mergeSpeed)
{
	// If an invalid name for an animation is sent
	if (name.length() <= 0)
		return;
	// Get a replica of the animation we want to change
	spine::Animation *animation = mSkeleton->getData()->findAnimation(name);
	// If the spot exists and the animation is not already set to the one given...
	if (mAnimationState->getCurrent(position) == nullptr || mAnimationState->getCurrent(position)->getAnimation() != animation)
	{
		spine::TrackEntry* track = mAnimationState->setAnimation(position, animation, shouldLoop);

		track->setMixDuration(mergeSpeed);

		// Change the speed if asked for
		if (speed != 0.f)
			track->setTimeScale(speed);
		else
			track->setTimeScale(1.f);

		if (position == 0u)
		{
			backwardsTime = mAnimationState->getCurrent(position)->getAnimationEnd();
			goBackSpeed = mAnimationState->getCurrent(position)->getTimeScale();
		}
	}
}

void SpineAnimationComp::EmptyAnimationAtPosition(const unsigned int position)
{
	// Add an empty animation to the track
	mAnimationState->addEmptyAnimation(position, 0.2f, 0.f);
}

void SpineAnimationComp::ClearAnimationAtPosition(const unsigned int position)
{
	// Add an empty animation to the track
	mAnimationState->setEmptyAnimation(position, 0.2f);
	// ~Simply a wrapper around this function!~
	//mAnimationState->clearTrack(position);
}

void SpineAnimationComp::ClearAllAnimations()
{
	// Simply a wrapper around this function!
	mAnimationState->clearTracks();
}

void SpineAnimationComp::FromJson(json & value)
{
	this->Renderable::FromJson(value);

	try {
		std::string metaFile;
		value["Meta File"] >> metaFile;

		fs::path metaPath(metaFile);
		json val;

		if (!fs::exists(metaPath))
		{
			metaPath.replace_extension(".spine");

			mSpineData = aexResourceMgr->getResource<SpineData>(metaPath.string().c_str());
			mSpineData->ToJson(val);
			return;
		}

		LoadFromFile(val, metaFile);

		std::string path;
		val["Relative Path"] >> path;

		mSpineData = aexResourceMgr->getResource<SpineData>(path.c_str());

		// create the skeleton
		mSkeleton = new spine::Skeleton(mSpineData->get()->GetSkeleton());

		// create the animation state
		mAnimationState = new spine::AnimationState(mSpineData->get()->GetAnimation());

		// set default skin
		if (mSpineData->get()->GetSkeleton()->getSkins().size())
			mSkeleton->setSkin(mSpineData->get()->GetSkeleton()->getSkins()[0]);
	} CATCH_FS_ERROR

	value["Scale"] >> Size;
	value["xOffset"] >> offset.x;
	value["yOffset"] >> offset.y;
	value["Backwards"] >> playBackwards;
	value["CarRotation"] >> carRotation;
	value["StartTime"] >> startTime;
	value["Speed"] >> speed;

	std::string tempAnimationName;
	value["Animation"] >> tempAnimationName;

	const char* temp = tempAnimationName.c_str();

	spine::String tempSpineName = temp;
	ChangeAnimation(0, tempSpineName, true, speed, 1.f);
}

void SpineAnimationComp::ToJson(json & value)
{
	this->Renderable::ToJson(value);
	mSpineData->ToJson(value);

	value["Scale"] << Size;
	value["xOffset"] << offset.x;
	value["yOffset"] << offset.y;
	value["Backwards"] << playBackwards;
	value["CarRotation"] << carRotation;
	value["StartTime"] << startTime;
	value["Speed"] << speed;

	if (mAnimationState->getCurrent(0))
	{
		const char* temp = mAnimationState->getCurrent(0)->getAnimation()->getName().buffer();
		std::string tempAnimationName = temp;
		value["Animation"] << tempAnimationName;
	}
}

/// Set skeleton size
void SpineAnimationComp::SetSkeletonSize(const AEVec2 & size)
{
	mSkeleton->setScaleX(size.x);
	mSkeleton->setScaleY(size.y);
}

void callback(spine::AnimationState* state, spine::EventType type, spine::TrackEntry* entry, spine::Event* event)
{
	// If the event is a custom event...
	if (type == spine::EventType_Event)
	{
		// Write your custom events here!
		// Examples:
		//if(event->getStringValue().buffer() == "step") {}
		//if(event->getData().getName().buffer() == "step") {}
	}
}

// Pause the animation if true is passed. Play it if false is passed.
void SpineAnimationComp::SetPauseState(bool isPaused)
{
	mPaused = isPaused;
}

// Set the new dt to use for the animation (0 will reset the animation to the beginning)
void SpineAnimationComp::SetDt(float newDt)
{
	dt = newDt;
	mChangedDt = true;
}