#include <iostream>
#include "Physics/CollisionSystem.h"
#include "Composition/AEXGameObject.h"
#include "Components/AEXTransformComp.h"
#include "Components/SpriteComps.h"
#include "Graphics/GfxMgr.h"
#include "Imgui/imgui.h"
#include "Collider.h"
#include "src/Engine/Composition/AEXSpace.h"
#include "Physics/CollisionGroups.h"
#include "Editor/Editor.h"

namespace AEX
{
	ICollider::ICollider() 
		: mOwnerPos(NULL)
		, mScale(1.0f, 1.0f)
		, mIsGhost(false)
		, mRigid(NULL)
		, mOffset(0,0)
		, collisionGroup("none")
	{
		// Add the collider to the vector of all colliders
		aexCollisionSystem->mAllColliders.push_back(this);
	}

	ICollider::~ICollider()
	{
		// Remove the collider from the static or dynamic list 
		aexCollisionSystem->RemoveCollider(this);
		aexCollisionSystem->RemoveFromPrevContacts(this);

		// Find this collider
		FOR_EACH(it, aexCollisionSystem->mAllColliders)
		{
			if (*it == this)
			{
				// Remove the collider pointer from the vector
				aexCollisionSystem->mAllColliders.erase(it);
				break;
			}
		}

		// Check if there is a rigid body pointing to this
		if (mRigid)
		{
			// Stop pointing
			mRigid->mCollider = NULL;
		}
	}

	AEVec3 * ICollider::GetOwnerPosition()
	{
		if (mOwner)
		{
			TransformComp * trComp = mOwner->GetComp<TransformComp>();

			if (!trComp)
				return NULL;

			return &(trComp->mLocal.mTranslationZ);	// Before, this was mWorld, but made prototype behave weird
		}
		else
		{
			//std::cout << "ERROR HERE" << std::endl;
			return NULL;

		}
	}

	void ICollider::Initialize()
	{
		// Pointer to the transform of the owner
		mOwnerPos = GetOwnerPosition();
		
		Rigidbody * rigid = mOwner->GetComp<Rigidbody>();

		// Check if the owner does not have a rigidbody
		if (rigid == NULL)
		{
			// Add the collider to the static collider vector by default
			aexCollisionSystem->AddCollider(this, Rigidbody::STATIC);
			mRigid = NULL;
			return;
		}

		// Add the collider to the static or dynamic vector
		aexCollisionSystem->AddCollider(this, rigid->mDynamicState);

		// Point to the rigid body of the owner
		mRigid = rigid;

		rigid->mCollider = this;
	}

	void ICollider::ChangeColliderState(Rigidbody::DynamicState state)
	{
		aexCollisionSystem->RemoveCollider(this);
		aexCollisionSystem->AddCollider(this, state);
	}

	void ICollider::Update()
	{
		if (!mOwner)
			return;

		if (mOwner->GetComp<TransformComp>())
			mOrientation = mOwner->GetComp<TransformComp>()->mLocal.mOrientation;
		
		if (SpriteComp* mSprite = GetOwner()->GetComp<SpriteComp>())
			mRealScale = AEVec2(mScale.x, mScale.y * (f32)mSprite->mTex->get()->GetHeight() / (f32)mSprite->mTex->get()->GetWidth());
		else
			mRealScale = mScale;
 		
	}

	void ICollider::Shutdown()
	{

	}

	BoxCollider::~BoxCollider() 
	{
		
	}

	BoxCollider::BoxCollider() : mIsAABB(true)
	{
	}

	bool ICollider::OnGui()
	{
		// Pointer to the transform of the owner
		mOwnerPos = GetOwnerPosition();

		bool changed = false;
		float scale[2]{ mScale.x, mScale.y };
		ImGui::Text("Collider size");
		if (ImGui::InputFloat2("Collider size", scale)) changed = true;
		mScale.x = scale[0];
		mScale.y = scale[1];

		if (SpriteComp* mSprite = GetOwner()->GetComp<SpriteComp>())
			mRealScale = AEVec2(mScale.x, mScale.y * ((f32)mSprite->mTex->get()->GetHeight() / (f32)mSprite->mTex->get()->GetWidth()));
		else
			mRealScale = mScale;
		//else if (SpriteAnimationComp* mAnim = GetOwner()->GetComp<SpriteAnimationComp>())
		//else if (SpineAnimationComp* mAnim = GetOwner()->GetComp<SpineAnimationComp>())
		
		if (ImGui::InputFloat2("Collider offset", mOffset.v)) changed = true;
		
		if (ImGui::BeginCombo("Collision Group", collisionGroup.c_str()))
		{
			static std::vector<std::string>& groups = collision_table->getCollisionGroups();

			auto end = groups.end();

			bool dummy = false;

			if (ImGui::Selectable("none", &dummy))
			{
				collisionGroup = "none";
				printCollisionGroup();
				changed = true;
			}

			for (auto it = groups.begin(); it != end; ++it)
			{
				if (ImGui::Selectable(it->c_str(), &dummy))
				{
					collisionGroup = *it;
					printCollisionGroup();
					changed = true;
					break;
				}
			}

			ImGui::EndCombo();
		}

		if (ImGui::Button("New Collision Group"))
		{
			Editor->mAddNewCG = true;
		}

		ImGui::Text("Ghost");
		if(ImGui::Checkbox("Ghost", &mIsGhost)) changed = true;

		//std::cout << "It is working" << std::endl;

		mOrientation = mOwner->GetComp<TransformComp>()->mLocal.mOrientation;
		Transform debugRectangle(AEVec2(mOwnerPos->x, mOwnerPos->y) + mOffset, mRealScale, mOrientation);
		GfxMgr->DrawRectangle(debugRectangle, AEVec4(239.0f / 255.0f, 130.0f / 255.0f, 1.0f, 1.0f));

		return changed;
	}
	

	bool CircleCollider::OnGui()
	{
		return this->ICollider::OnGui();
	}

	CircleCollider::~CircleCollider()
	{
	}

	bool BoxCollider::OnGui()
	{
		ImGui::Checkbox("AABB", &mIsAABB);
		return this->ICollider::OnGui();
	}


	void ICollider::FromJson(json & value)
	{
		this->IComp::FromJson(value);

		value["Scale"] >> mScale;
		value["Orientation"] >> mOrientation;
		value["ColPos"] >> mColPos;
		value["Ghost"] >> mIsGhost;
		value["Offset"] >> mOffset;
 		value["collision group"] >> collisionGroup;
	}

	void ICollider::ToJson(json & value)
	{
		this->IComp::ToJson(value);

		value["Scale"] << mScale;
		value["Orientation"] << mOrientation;
		value["ColPos"] << mColPos;
		value["Ghost"] << mIsGhost;
		value["Offset"] << mOffset;
		value["collision group"] << collisionGroup;
	}

	void ICollider::printCollisionGroup()
	{
		//std::cout << "---------------" << std::endl;
		//std::cout << "The Collider of " << mOwner->GetBaseName() << " is in the collision group of " << collisionGroup << std::endl;
		//std::cout << "---------------" << std::endl;
	}


	void BoxCollider::FromJson(json & value)
	{
		this->ICollider::FromJson(value);

		if (value["Is AABB"] != nullptr)
			mIsAABB = value["Is AABB"].get<bool>();
	}

	void BoxCollider::ToJson(json & value)
	{
		this->ICollider::ToJson(value);
		value["Is AABB"] = mIsAABB;
	}
}