#include "Engine/AEX.h"
//#include "src\Engine\Editor\Editor.h"
//#include "Demos/JsonTests/JsonTests.h"
//#include "Demos/OpenGLDemo/OpenGLDemo.h"
#include "Demos/ImGuiDemo/ImGuiDemo.h"
#include "Utilities/AEXUtils.h"
//#include "Demos/ResourceManagerTests/ResourceManagerTests.h"
//#include "Demos/SpineDemo/SpineDemo.h"

void PreventMultipleInstances()
{
	const std::string & myDocs = AEX::GetMyDocuments();
	
	//HANDLE fileHandle = CreateFile(myDocs.c_str(), GENERIC_READ, FILE_SHARE_READ, );
}

int main(void)
{
	//PreventMultipleInstances();

	HANDLE mutexHandle = CreateMutex(nullptr, true, "Entails-Duplicates-Mutex");

	if (GetLastError() == ERROR_ALREADY_EXISTS)
		std::exit(0);

	if (mutexHandle == nullptr)
		std::cout << "The mutex handle is NULL: " << GetLastError() << std::endl;

	aexEngine->Initialize();
	//Editorstate->Initialize();
	//Editor->Initialize();
	aexEngine->Run(new ImGuiDemo);
	AEX::AEXEngine::ReleaseInstance();

	if (!ReleaseMutex(mutexHandle))
		std::cout << "Release Mutex failed: " << GetLastError() << std::endl;

	if (!CloseHandle(mutexHandle))
		std::cout << "Mutex handle closure failed: " << GetLastError() << std::endl;

	return 0;
}