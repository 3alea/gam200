#include "Composition/AEXScene.h"
#include "Platform/AEXTime.h"
#include "Platform/AEXInput.h"
#include "Grenade.h"
#include "src/Gameplay/Enemy/EnemyFlying.h"
#include "src/Gameplay/Enemy/EnemyGrounded.h"
#include "src/Gameplay/Enemy/EnemySpawner.h"
#include "src/Gameplay/Enemy/EnemyKamikaze.h"
#include "Components\Collider.h"
#include "Resources/AEXResourceManager.h"
#include "Enemy/EnemyMitosis.h"

using namespace AEX;
using namespace ImGui;

void GrenadeLogic::Initialize()
{
	t = GetOwner()->GetComp<TransformComp>();
	rb = GetOwner()->GetComp<Rigidbody>();

	// We want the grenade to move after being updated only, so we set init to true
	init = true;
}

void GrenadeLogic::createExplosion()
{
	GameObject * explosion = mOwner->GetParentSpace()->InstantiateArchetype("explosion", t->mLocal.mTranslationZ);
}

void GrenadeLogic::Update()
{
	if (init)
	{
		rb->mVelocity = Velocity;

		//t->mLocal.mTranslationZ.x = p0.x;
		//t->mLocal.mTranslationZ.y = p0.y;

		// After force is added, turn this off
		init = false;
	}

	timer += (f32)aexTime->GetFrameTime();
	if (timer >= maxTime)
	{
		createExplosion();
		mOwner->GetParentSpace()->DestroyObject(mOwner);
		timer = 0.0f;
	}
}

bool GrenadeLogic::OnGui()
{
	return false;
}

void GrenadeLogic::OnCollisionEvent(const CollisionEvent & collision)
{
	// Should explode if grenade crashes with any of the enemies
	if (collision.otherObject->GetComp<EnemyFlying>()
		|| collision.otherObject->GetComp<EnemyGrounded>()
		|| collision.otherObject->GetComp<EnemyKamikaze>()
		|| collision.otherObject->GetComp<EnemySpawner>()
		|| collision.otherObject->GetComp<EnemyMitosis>())
	{
		createExplosion();
		mOwner->GetParentSpace()->DestroyObject(mOwner);
	}
	// If it touches anything else, it should slow down on the x
	if(!collision.otherObject->GetComp<BoxCollider>()->mIsGhost)
		rb->mVelocity.x /= 1.05f;
}

void GrenadeLogic::OnCollisionStarted(const CollisionStarted & collision)
{
	// Should explode if grenade crashes with any of the enemies
	if (collision.otherObject->GetComp<EnemyFlying>()
		|| collision.otherObject->GetComp<EnemyGrounded>()
		|| collision.otherObject->GetComp<EnemyKamikaze>()
		|| collision.otherObject->GetComp<EnemySpawner>()
		|| collision.otherObject->GetComp<EnemyMitosis>())
	{
		createExplosion();
		mOwner->GetParentSpace()->DestroyObject(mOwner);
	}
}

// FromJson specialized for this component
void GrenadeLogic::FromJson(json & value)
{
	this->IComp::FromJson(value);
}

// ToJson specialized for this component
void GrenadeLogic::ToJson(json & value)
{
	this->IComp::ToJson(value);
}
