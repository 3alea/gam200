#include "Components/SpineAnimation.h"
#include "Composition/AEXScene.h"
#include "Platform/AEXTime.h"
#include "BulletLogic.h"
#include "Activator/EnemyActivator.h"
#include "src/Gameplay/Enemy/EnemyFlying.h"
#include "src/Gameplay/Enemy/EnemyGrounded.h"
#include "src/Gameplay/Enemy/EnemySpawner.h"
#include "src/Gameplay/Enemy/EnemyKamikaze.h"
#include "src/Gameplay/Enemy/EnemyMitosis.h"
#include "src/Gameplay/Enemy/EnemyTurret.h"
#include "Environment/Checkpoint.h"
#include "CameraLogic/CameraChanger.h"
#include "Resources/AEXResourceManager.h"
#include "AI\Easing.h"
#include "Follow\ChaseObject.h"
#include "Environment\Background\StationZoomOut.h"
#include "Enemy/WaveSystem/Spawner.h"

using namespace AEX;
using namespace ImGui;

int BulletLogic::mStationEnemiesKilled = 0;

void BulletLogic::Initialize()
{
	t = GetOwner()->GetComp<TransformComp>();
	//mySprite = GetOwner()->GetComp<SpriteComp>();
	stats = GetOwner()->GetParentSpace()->FindObject("player")->GetComp<PlayerStats>();
	//bulletSprite = aexResourceMgr->getResource<Texture>("data\\Textures\\bullet.png");
	//mySprite->mTex = bulletSprite;
	maxTime = 4.f;
	speed = 0.25f;
	timer = 0.0f;
}

void BulletLogic::Update()
{
	//////////////Scale////////////////
	if (timer < timeToScale)
	{
		timer += (f32)aexTime->GetFrameTime();
		
		float tn = EaseInOutQuad(timer / timeToScale);
		
		// Lerp the scale of the bullet
		t->mLocal.mScale = initScale + (maxScale - initScale) * tn;
	}

	//////////////Scale////////////////
	myOrientation = t->mLocal.mOrientation;
	timer += (f32)aexTime->GetFrameTime();
	if (timer >= maxTime)
	{
		mOwner->GetParentSpace()->DestroyObject(mOwner);
	}

	t->mLocal.mTranslationZ.x += speed * Cos(myOrientation);
	t->mLocal.mTranslationZ.y += speed * Sin(myOrientation);

	ParticleEmitter* bulletParticles = mOwner->GetComp<ParticleEmitter>();
	if (bulletParticles)
	{
		AEVec2 particleAngle;
		particleAngle.FromAngle(myOrientation + PI);
		bulletParticles->mEmitProperties->mVelRange[START_VAL][MIN_VAL] = AEVec2(particleAngle.x, particleAngle.y);
		bulletParticles->mEmitProperties->mVelRange[START_VAL][MAX_VAL] = AEVec2(particleAngle.x, particleAngle.y);
	}
}

bool BulletLogic::OnGui()
{
	// isChanged boolean to return if something has changed
	bool isChanged = false;

	// Collapsing header - when clicked, opens up to show the rest of edited variables
	if (ImGui::CollapsingHeader("BulletLogic"))
	{
		// Is Supershot
		ImGui::Text("Initial Scale");

		if (ImGui::InputFloat2("initial scale", initScale.v))
		{
			isChanged = true;
		}

		ImGui::Text("Max Scale");
		if (ImGui::InputFloat2("max scale", maxScale.v))
		{
			isChanged = true;
		}

		ImGui::Text("Time to scale");
		if (ImGui::InputFloat("time", &timeToScale))
		{
			isChanged = true;
		}

		// Is Supershot
		ImGui::Text("Is supershot");
		// Check this if it is a supershot (and not a normal shot)
		if (ImGui::Checkbox("Is supershot", &isSuperShot))
		{
			isChanged = true;
		}
	}
	return isChanged;
}



void BulletLogic::OnCollisionEvent(const CollisionEvent & collision)
{
	if (collision.otherObject->GetComp<EnemyFlying>())
	{
		if (!collision.otherObject->GetComp<EnemyFlying>()->InvencibleEnemy)
		{
			if (collision.otherObject->GetComp<EnemyFlying>()->Shoot_activator)
			{
				collision.otherObject->GetComp<EnemyFlying>()->mLives--;
				

				collision.otherObject->GetComp<Enemy>()->ActivateDamage = true;
				if (!isSuperShot)
				{audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\Enemies\\EnemyFlying\\FlyingDie.wav"), Voice::SFX);
					stats->combo += 2;
					stats->comboTimer = 0.0f;
					stats->chargeMeter += stats->combo;
					if (stats->chargeMeter > stats->chargeMeterCap)
					{
						stats->chargeMeter = stats->chargeMeterCap;
					}
				}
			}
		}
	}

	// For Station enemy
	if (ChaseObject * chaser = collision.otherObject->GetComp<ChaseObject>())
	{
		if (chaser->mLives == 0)
			chaser->mLives--;

		else if (chaser->mLives > 0)
		{
			chaser->mLives--;

			collision.otherObject->GetComp<Enemy>()->ActivateDamage = true;

			if (!isSuperShot)
			{
				stats->combo += 2;
				stats->comboTimer = 0.0f;
				stats->chargeMeter += stats->combo;
				if (stats->chargeMeter > stats->chargeMeterCap)
				{
					stats->chargeMeter = stats->chargeMeterCap;
				}
			}
			
			if (chaser->mLives == 0)
			{
				//chaser->mOwner->GetParentSpace()->DestroyObject(chaser->mOwner);
				chaser->mDead = true;
				chaser->mOwner->GetComp<SpineAnimationComp>()->mColor[3] = 0.0f;
				mStationEnemiesKilled++;
				audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/Station/SPLAT Crush 01.wav"), Voice::SFX);

				if (collision.otherObject->mSpawnerOwner)
					collision.otherObject->mSpawnerOwner->EnemyKilled();

				// Make the particles inactive
				//std::string particlesName = "Particles";
				//particlesName += std::to_string(chaser->mParticleId);
				//GameObject * emitterObj = aexScene->FindObjectInANYSpace(particlesName.c_str());

				ParticleEmitter * emitter = chaser->mOwner->GetComp<ParticleEmitter>();

				if (emitter != nullptr)
					emitter->mEmitProperties->mbParticlesEnabled = false;
			}

			if (mStationEnemiesKilled >= 25)//7)
			{
				// Load new level
				StationZoomOut::mStartFadeToBlack = true;
				//StationZoomOut::mStartSecondShake = true;

				//aexScene->ChangeLevel("data/Levels/texture-repeat-test.json");
				//aexScene->bFadeTransition = true;
			}
		}
	}

	if (collision.otherObject->GetComp<EnemyGrounded>())
	{
		if (!collision.otherObject->GetComp<EnemyGrounded>()->InvencibleEnemy)
		{
			if (collision.otherObject->GetComp<EnemyGrounded>()->Shoot_activator)
			{
				//audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\Enemies\\EnemyGrounded\\GroundedHit.mp3"), Voice::SFX);
				collision.otherObject->GetComp<Enemy>()->ActivateDamage = true;
				collision.otherObject->GetComp<EnemyGrounded>()->mLives--;
				if (!isSuperShot)
				{
					stats->combo += 2;
					stats->comboTimer = 0.0f;
					stats->chargeMeter += stats->combo;
					if (stats->chargeMeter > stats->chargeMeterCap)
					{
						stats->chargeMeter = stats->chargeMeterCap;
					}
				}
			}
		}
	}
	if (collision.otherObject->GetComp<EnemyTurret>())
	{
		if (!collision.otherObject->GetComp<EnemyTurret>()->InvencibleEnemy)
		{
			if (collision.otherObject->GetComp<EnemyTurret>()->Shoot_activator)
			{
				collision.otherObject->GetComp<Enemy>()->ActivateDamage = true;
				collision.otherObject->GetComp<EnemyTurret>()->mLives--;
				//audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\Enemies\\EnemyGrounded\\GroundedHit.mp3"), Voice::SFX);
				if (!isSuperShot)
				{
					stats->combo += 2;
					stats->comboTimer = 0.0f;
					stats->chargeMeter += stats->combo;
					if (stats->chargeMeter > stats->chargeMeterCap)
					{
						stats->chargeMeter = stats->chargeMeterCap;
					}
				}
			}
		}
	}

	if (collision.otherObject->GetComp<EnemyKamikaze>())
	{
		if (!collision.otherObject->GetComp<EnemyKamikaze>()->InvencibleEnemy)
		{
			if (collision.otherObject->GetComp<EnemyKamikaze>()->Shoot_activator)
			{
				collision.otherObject->GetComp<Enemy>()->ActivateDamage = true;
				collision.otherObject->GetComp<EnemyKamikaze>()->mLives--;
				if (!isSuperShot)
				{
					stats->combo += 2;
					stats->comboTimer = 0.0f;
					stats->chargeMeter += stats->combo;
					if (stats->chargeMeter > stats->chargeMeterCap)
					{
						stats->chargeMeter = stats->chargeMeterCap;
					}
				}
			}
		}
	}

	if (collision.otherObject->GetComp<EnemySpawner>())
	{
		if (!collision.otherObject->GetComp<EnemySpawner>()->InvencibleEnemy)
		{
			if (collision.otherObject->GetComp<EnemySpawner>()->Shoot_activator)
			{
				
				collision.otherObject->GetComp<Enemy>()->ActivateDamage = true;
				collision.otherObject->GetComp<EnemySpawner>()->mLives--;
				if (!isSuperShot)
				{audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\Enemies\\EnemySpawner\\SpawnerHit.wav"), Voice::SFX);
					stats->combo += 2;
					stats->comboTimer = 0.0f;
					stats->chargeMeter += stats->combo;
					if (stats->chargeMeter > stats->chargeMeterCap)
					{
						stats->chargeMeter = stats->chargeMeterCap;
					}
				}
			}
		}
	}

	if (collision.otherObject->GetComp<EnemyMitosis>())
	{
		if (!collision.otherObject->GetComp<EnemyMitosis>()->InvencibleEnemy)
		{
			if (collision.otherObject->GetComp<EnemyMitosis>()->Shoot_activator)
			{
				
				
				collision.otherObject->GetComp<Enemy>()->ActivateDamage = true;
				collision.otherObject->GetComp<EnemyMitosis>()->mLives--;
				if (!isSuperShot)
				{audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\Enemies\\EnemyMitosis\\MitosisDivide.wav"), Voice::SFX);
					stats->combo += 2;
					stats->comboTimer = 0.0f;
					stats->chargeMeter += stats->combo;
					if (stats->chargeMeter > stats->chargeMeterCap)
					{
						stats->chargeMeter = stats->chargeMeterCap;
					}
				}
				else
					collision.otherObject->GetComp<EnemyMitosis>()->shouldSplit = false;
			}
		}
	}

	if (collision.otherObject->GetComp<ICollider>()->collisionGroup == "Activator")
	{
		return;
	}

	if (collision.otherObject->GetBaseNameString() == "steak" ||
		collision.otherObject->GetBaseNameString() == "finger" ||
		collision.otherObject->GetBaseNameString() == "carrot" ||
		collision.otherObject->GetBaseNameString() == "pizza" ||
		collision.otherObject->GetBaseNameString() == "skeleton fish" ||
		collision.otherObject->GetBaseNameString() == "burger" ||
		collision.otherObject->GetBaseNameString() == "polo")
	{
		return;
	}

	if (!isSuperShot)
		if (collision.otherObject->GetBaseNameString() != "player"
			&& collision.otherObject->GetBaseNameString() != "foot"
			&& !collision.otherObject->GetComp<BulletLogic>()
			&& !collision.otherObject->GetComp<EnemyActivator>()
			&& !collision.otherObject->GetComp<Checkpoint>()
			&& !collision.otherObject->GetComp<CameraChanger>()
			&& collision.otherObject->GetComp<ICollider>()->collisionGroup != "EnemyBullet"
			&& collision.otherObject->GetComp<ICollider>()->collisionGroup != "Activator"
			&& (!collision.otherObject->GetComp<ChaseObject>() || (collision.otherObject->GetComp<ChaseObject>() && collision.otherObject->GetComp<ChaseObject>()->mLives > 0)))
		{
			mOwner->GetParentSpace()->DestroyObject(mOwner);
		}
}

// FromJson specialized for this component
void BulletLogic::FromJson(json & value)
{
	this->IComp::FromJson(value);

	value["maxScale"] >> maxScale;
	value["Is supershot"] >> isSuperShot;
	value["initScale"] >> initScale;
	value["timetoscale"] >> timeToScale;
}

// ToJson specialized for this component
void BulletLogic::ToJson(json & value)
{
	this->IComp::ToJson(value);

	value["Is supershot"] << isSuperShot;
	value["maxScale"] << maxScale;
	value["initScale"] << initScale;
	value["timetoscale"] << timeToScale;
}