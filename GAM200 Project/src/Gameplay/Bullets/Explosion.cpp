#include "Platform/AEXTime.h"
#include "Explosion.h"
#include "Enemy/EnemyFlying.h"
#include "Enemy/EnemyGrounded.h"
#include "Enemy/EnemySpawner.h"
#include "Enemy/EnemyKamikaze.h"
#include "Enemy/EnemyMitosis.h"
#include "Resources/AEXResourceManager.h"


using namespace AEX;
using namespace ImGui;

void ExplosionLogic::Initialize()
{
	// Get the stats from the player
	stats = mOwner->GetParentSpace()->FindObject("player")->GetComp<PlayerStats>();
}

void ExplosionLogic::Update()
{
	timer += (f32)aexTime->GetFrameTime();
	if (timer >= maxTime)
	{
		mOwner->GetParentSpace()->DestroyObject(mOwner);
	}
}

bool ExplosionLogic::OnGui()
{
	// Collapsing header - when clicked, opens up to show the rest of edited variables
	if (ImGui::CollapsingHeader("Explosion"))
	{
		// isChanged boolean to return if something has changed
		bool isChanged = false;

		// Max time
		ImGui::Text("Explosion max time");
		if (ImGui::InputFloat("Explosion max time", &maxTime))
		{
			isChanged = true;
		}
	}

	return false;
}



void ExplosionLogic::collisionStarted(const CollisionEvent & collision)
{
	// Should explode if grenade crashes with any of the enemies
	// Also add to the combo if it is an enemy
	if (collision.otherObject->GetComp<EnemyFlying>())
	{
		collision.otherObject->GetComp<EnemyFlying>()->mLives -= 5;
		stats->combo++;
		stats->comboTimer = 0.f;
		stats->chargeMeter += stats->combo;
	}

	if (collision.otherObject->GetComp<EnemyGrounded>())
	{
		collision.otherObject->GetComp<EnemyGrounded>()->mLives -= 5;
		stats->combo++;
		stats->comboTimer = 0.f;
		stats->chargeMeter += stats->combo;
	}

	if (collision.otherObject->GetComp<EnemyKamikaze>())
	{
		collision.otherObject->GetComp<EnemyKamikaze>()->mLives -= 5;
		stats->combo++;
		stats->comboTimer = 0.f;
		stats->chargeMeter += stats->combo;
	}

	if (collision.otherObject->GetComp<EnemySpawner>())
	{
		collision.otherObject->GetComp<EnemySpawner>()->mLives -= 5;
		stats->combo++;
		stats->comboTimer = 0.f;
		stats->chargeMeter += stats->combo;
	}

	if (collision.otherObject->GetComp<EnemyMitosis>())
	{
		collision.otherObject->GetComp<EnemyMitosis>()->mLives -= 5;
		stats->combo++;
		stats->comboTimer = 0.f;
		stats->chargeMeter += stats->combo;
	}
	//////////////////////////////////////////////////////////////

}

// FromJson specialized for this component
void ExplosionLogic::FromJson(json & value)
{
	this->IComp::FromJson(value);

	value["Explosion Time Alive"] >> maxTime;
}

// ToJson specialized for this component
void ExplosionLogic::ToJson(json & value)
{
	this->IComp::ToJson(value);

	value["Explosion Time Alive"] << maxTime;
}
