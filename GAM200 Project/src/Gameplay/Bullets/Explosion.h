#pragma once

#include "Composition\AEXGameObject.h"
#include "Components\AEXTransformComp.h"
#include "Components\Logic.h"
#include "Player\Stats\PlayerStats.h"
#include "Imgui\imgui.h"

using namespace AEX;

class ExplosionLogic : public LogicComp
{
	AEX_RTTI_DECL(ExplosionLogic, LogicComp);
public:
	// Player stats to add to the combo
	PlayerStats* stats;

	// In order to be able to destroy the grenade over time...
	float timer = 0.f;

	// Maximum time grenade is alive...
	float maxTime;

	// Start is called before the first frame update
	virtual void Initialize();

	// Update is called once per frame
	virtual void Update();

	// OnGui() function
	virtual bool OnGui();

	// OnBulletCollision
	virtual void collisionStarted(const CollisionEvent & collision);
	
	// FromJson specialized for this component
	virtual void FromJson(json & value);
	// ToJson specialized for this component
	virtual void ToJson(json & value);
};