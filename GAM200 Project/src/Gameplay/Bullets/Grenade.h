#pragma once

#include "Composition\AEXGameObject.h"
#include "Components\AEXTransformComp.h"
#include "Components\Logic.h"
#include "Components\Rigidbody.h"
#include "Imgui\imgui.h"
#include "Player\Stats\PlayerStats.h"

using namespace AEX;

class GrenadeLogic : public LogicComp
{
	AEX_RTTI_DECL(GrenadeLogic, LogicComp);
public:
	// Object TranformComp
	TransformComp* t;

	// Object RigidBody
	Rigidbody* rb;

	// Player stats to add to the combo
	PlayerStats* stats;

	// Whether the grenade has been initialized or not
	bool init = true;

	AEVec2 Velocity;

	AEVec2 p0;

	// In order to be able to destroy the grenade over time...
	float timer = 0.f;

	// Maximum time grenade is alive...
	float maxTime = 4.f;

	// Helper function creating an explosion
	void createExplosion();

	// Start is called before the first frame update
	virtual void Initialize();

	// Update is called once per frame
	virtual void Update();

	// OnGui() function
	virtual bool OnGui();

	// OnBulletCollision
	virtual void OnCollisionEvent(const CollisionEvent & collision);
	virtual void OnCollisionStarted(const CollisionStarted & collision);
	
	// FromJson specialized for this component
	virtual void FromJson(json & value);
	// ToJson specialized for this component
	virtual void ToJson(json & value);
};