#pragma once

#include "Composition\AEXGameObject.h"
#include "Components\AEXTransformComp.h"
#include "Components\Logic.h"
#include "Imgui\imgui.h"
#include "Components\SpriteComps.h"
#include "Player\Stats\PlayerStats.h"

using namespace AEX;


struct BulletLogic : public LogicComp
{
	AEX_RTTI_DECL(BulletLogic, LogicComp);

	// Object TranformComp
	TransformComp* t;

	// Sprite
	//SpriteComp* mySprite;

	// Player stats to add to the combo
	PlayerStats* stats;

	// Texture
	AEX::TResource<Texture>* bulletSprite;

	float myOrientation;

	float speed;

	AEVec2 initScale;
	AEVec2 maxScale;
	float scaleTimer;
	float timeToScale;

	// In order to be able to kill the bullet over time...
	float timer = 0.f;

	// Maximum time bullet is alive...
	float maxTime;

	// Supershot (does not follow the exact same logic)
	bool isSuperShot = false;

	// Start is called before the first frame update
	virtual void Initialize();

	// Update is called once per frame
	virtual void Update();

	// OnGui() function
	virtual bool OnGui();

	// OnBulletCollision
	virtual void OnCollisionEvent(const CollisionEvent & collision);
	
	// FromJson specialized for this component
	virtual void FromJson(json & value);
	// ToJson specialized for this component
	virtual void ToJson(json & value);

	// Added by Miguel as a quick solution so that Dani can test the entire thing
	static int mStationEnemiesKilled;
};