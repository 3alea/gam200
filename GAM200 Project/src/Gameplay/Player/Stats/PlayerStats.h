#pragma once

#include "Composition\AEXGameObject.h"
#include "Components\Logic.h"
#include "Imgui\imgui.h"

using namespace AEX;

class PlayerStats : public LogicComp
{
	AEX_RTTI_DECL(PlayerStats, LogicComp);
public:

	// Player lives
	int lives = 3;

	// Player direction when on tunnel
	int trayectoryDirection = 1;

	// Player grenades placeholder
	int grenades = 5;

	// Player is shooting
	bool isShooting = false;

	// Player can super shot
	bool canSuperShot = true;

	// Player super shot
	bool superShot = false;

	// Player direction
	bool isRight = true;

	// Player up direction
	bool isUp = false;

	// Player is on a cinematic
	// Avoid moving the player
	bool onCinematic = false;

	// Make the player go left or right in a cinematic
	bool goLeftOnCinematic = false;
	bool doNotMoveOnCinematic = false;

	void StartCinematic(bool move, bool moveRight = true);

	void StopCinematic();

	// Can duck/is not ducking boolean
	bool canDuck = true;

	// Is floating in the air
	bool isFloating = false;

	// Returns to the ground
	bool backToGround = false;

	// Is landing
	bool isLanding = false;

	// Is grounded boolean
	bool isGrounded = true;

	// Is double jumping
	bool isDoubleJumping = false;

	// Dash determiner
	bool dashChecker = true;

	// Can dash boolean
	bool canDash = true;

	// Is dashing boolean
	bool isDashing = false;

	// Boolean that makes sure to change sprite if moving
	bool isMoving = false;

	// Boolean that says that the player is holding the stand button
	bool isStanding = false;

	// Boolean that tells if the player is on the veins level
	bool onVeins = false;

	bool onVeinsTransition = false;

	// Direction to move
	AEVec2 walkDir;

	AEVec2 gravityDir;

	float xDiff;

	unsigned floorsOn;

	// Foot is colliding
	bool footColliding = false;

	// Invincibility boolean
	bool isInvincible = false;

	// IsHit boolean
	bool isHit = false;

	//Foot is colliding with a moving platform
	bool isCollidingWithMovingPlayform = false;

	// The amount that the platform is moving
	AEVec2 toMoveByFriction = AEVec2(0.f, 0.f);

	// Checkpoint - Maybe we don't need this!
	AEVec2 checkpointPosition = AEVec2(0.f, 0.f);

	bool comboReset = false;
	// Combo value (will be 0 every time the player respawns)
	int combo = 0;

	// Combo max time
	float maxComboTime = 2.f;

	// Combo timer
	float comboTimer = maxComboTime;

	// Super charge meter
	float chargeMeter = 0.f;

	// Super charge meter capacity
	float chargeMeterCap = 100.f;

	// Invincibility timer
	float inviciTimer = 0.f;

	// Invincibility cooldown
	float inviciCooldown = 2.f;

	// IsHit cooldown (lag)
	float isHitCooldown = 0.5f;

	// TEMPORARY shot timer
	float superShotSpawnTime = 0.5f;

	// Super timer
	float superTimer = 0.f;

	// Update is called once per frame
	virtual void Update();

	// OnGui() function
	virtual bool OnGui();

	// FromJson specialized for this component
	virtual void FromJson(json & value);
	// ToJson specialized for this component
	virtual void ToJson(json & value);
};