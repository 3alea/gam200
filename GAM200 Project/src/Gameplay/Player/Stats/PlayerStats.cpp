#include "PlayerStats.h"
#include "Platform\AEXTime.h"
#include "Editor\Editor.h"
#include "Platform\AEXInput.h"
#include "Components/AEXTransformComp.h"
using namespace AEX;
using namespace ImGui;

void PlayerStats::StartCinematic(bool move, bool moveRight)
{
	onCinematic = true;

	if (move)
	{
		goLeftOnCinematic = !moveRight;
		doNotMoveOnCinematic = false;
	}
	else
	{
		doNotMoveOnCinematic = true;
	}	
}

void PlayerStats::StopCinematic()
{
	onCinematic = false;
}

void PlayerStats::Update()
{
	if (combo != 0)
	{
		if (comboReset)
		{
			comboReset = false;
			comboTimer = 0.0f;
		}
		comboTimer += (f32)aexTime->GetFrameTime();
		if (comboTimer >= maxComboTime)
		{
			combo = 0;
			comboReset = true;
		}
	}

	// HACKS - LEVEL LOADING (USE CONTROL + ALT + NUMBER)
	// MAIN MENU #0
	//if (aexInput->KeyPressed(GLFW_KEY_LEFT_CONTROL) && aexInput->KeyPressed(GLFW_KEY_LEFT_ALT) && aexInput->KeyTriggered(GLFW_KEY_0))
	//{
	//	if (strcmp(Editor->levelToLoadName.c_str(), "Main Menu"))
	//	{
	//		Editor->levelToLoadName = "Main Menu";
	//		Editor->loadFromCertainLevel = true;
	//		return;
	//	}
	//}
	//
	//// TUTORIAL #1
	//if (aexInput->KeyPressed(GLFW_KEY_LEFT_CONTROL) && aexInput->KeyPressed(GLFW_KEY_LEFT_ALT) && aexInput->KeyTriggered(GLFW_KEY_1))
	//{
	//	if (strcmp(Editor->levelToLoadName.c_str(), "Heart(Tutorial)"))
	//	{
	//		Editor->levelToLoadName = "Heart(Tutorial)";
	//		Editor->loadFromCertainLevel = true;
	//		return;
	//	}
	//}
	//
	//// VEINS #2
	//if (aexInput->KeyPressed(GLFW_KEY_LEFT_CONTROL) && aexInput->KeyPressed(GLFW_KEY_LEFT_ALT) && aexInput->KeyTriggered(GLFW_KEY_2))
	//{
	//	if (strcmp(Editor->levelToLoadName.c_str(), "Veins"))
	//	{
	//		Editor->levelToLoadName = "Veins";
	//		Editor->loadFromCertainLevel = true;
	//		return;
	//	}
	//}
	//
	//// STATION #3
	//if (aexInput->KeyPressed(GLFW_KEY_LEFT_CONTROL) && aexInput->KeyPressed(GLFW_KEY_LEFT_ALT) && aexInput->KeyTriggered(GLFW_KEY_3))
	//{
	//	if (strcmp(Editor->levelToLoadName.c_str(), "Station"))
	//	{
	//		Editor->levelToLoadName = "Station";
	//		Editor->loadFromCertainLevel = true;
	//		return;
	//	}
	//}
	//
	//// CARS LEVEL 1 #4
	//if (aexInput->KeyPressed(GLFW_KEY_LEFT_CONTROL) && aexInput->KeyPressed(GLFW_KEY_LEFT_ALT) && aexInput->KeyTriggered(GLFW_KEY_4))
	//{
	//	if (strcmp(Editor->levelToLoadName.c_str(), "texture-repeat-test"))
	//	{
	//		Editor->levelToLoadName = "texture-repeat-test";
	//		Editor->loadFromCertainLevel = true;
	//		return;
	//	}
	//}
	//
	//// STOMACH PART 1 #5
	//if (aexInput->KeyPressed(GLFW_KEY_LEFT_CONTROL) && aexInput->KeyPressed(GLFW_KEY_LEFT_ALT) && aexInput->KeyTriggered(GLFW_KEY_5))
	//{
	//	if (strcmp(Editor->levelToLoadName.c_str(), "Stomach"))
	//	{
	//		Editor->levelToLoadName = "Stomach";
	//		Editor->loadFromCertainLevel = true;
	//		return;
	//	}
	//}
	//
	//// STOMACH PART 2 #6
	//if (aexInput->KeyPressed(GLFW_KEY_LEFT_CONTROL) && aexInput->KeyPressed(GLFW_KEY_LEFT_ALT) && aexInput->KeyTriggered(GLFW_KEY_6))
	//{
	//	if (strcmp(Editor->levelToLoadName.c_str(), "StomachPart2"))
	//	{
	//		Editor->levelToLoadName = "StomachPart2";
	//		Editor->loadFromCertainLevel = true;
	//		return;
	//	}
	//}
	//
	//// (STILL IN PROGRESS)
	//// BOILER ROOM #7
	//if (aexInput->KeyPressed(GLFW_KEY_LEFT_CONTROL) && aexInput->KeyPressed(GLFW_KEY_LEFT_ALT) && aexInput->KeyTriggered(GLFW_KEY_7))
	//{
	//	if (strcmp(Editor->levelToLoadName.c_str(), "Boiler room"))
	//	{
	//		Editor->levelToLoadName = "Boiler room";
	//		Editor->loadFromCertainLevel = true;
	//		return;
	//	}
	//}
	//
	//// LUNGS #8
	//if (aexInput->KeyPressed(GLFW_KEY_LEFT_CONTROL) && aexInput->KeyPressed(GLFW_KEY_LEFT_ALT) && aexInput->KeyTriggered(GLFW_KEY_8))
	//{
	//	if (strcmp(Editor->levelToLoadName.c_str(), "Lungs"))
	//	{
	//		Editor->levelToLoadName = "Lungs";
	//		Editor->loadFromCertainLevel = true;
	//		return;
	//	}
	//}
	//
	//// FINAL BOSS #9
	//if (aexInput->KeyPressed(GLFW_KEY_LEFT_CONTROL) && aexInput->KeyPressed(GLFW_KEY_LEFT_ALT) && aexInput->KeyTriggered(GLFW_KEY_9))
	//{
	//	if (strcmp(Editor->levelToLoadName.c_str(), "finalboss"))
	//	{
	//		Editor->levelToLoadName = "finalboss";
	//		Editor->loadFromCertainLevel = true;
	//		return;
	//	}
	//}

}

bool PlayerStats::OnGui()
{
	// Collapsing header - when clicked, opens up to show the rest of edited variables
	if (ImGui::CollapsingHeader("PlayerStats"))
	{
		// isChanged boolean to return if something has changed
		bool isChanged = false;

		// Text for the lives
		ImGui::Text("Lives");
		// Allow user to input lives from the editor
		if (ImGui::InputInt("Lives", &lives))
		{
			isChanged = true;
		}

		// Text for the grenades
		ImGui::Text("Grenade Number");
		// Allow user to input grenade number from the editor
		if (ImGui::InputInt("Grenade Number", &grenades))
		{
			isChanged = true;
		}

		// Player direction
		ImGui::Text("Looking Right");
		if(ImGui::Checkbox("Looking Right", &isRight))
		{
			isChanged = true;
		}

		// Player up direction
		ImGui::Text("Looking Up");
		if(ImGui::Checkbox("Looking Up", &isUp))
		{
			isChanged = true;
		}

		// Can duck/is not ducking boolean
		ImGui::Text("Can Duck");
		if(ImGui::Checkbox("Can Duck", &canDuck))
		{
			isChanged = true;
		}

		// Is grounded boolean
		ImGui::Text("On Ground");
		if(ImGui::Checkbox("On Ground", &isGrounded))
		{
			isChanged = true;
		}

		// Can dash boolean
		ImGui::Text("Can Dash");
		if(ImGui::Checkbox("Can Dash", &canDash))
		{
			isChanged = true;
		}

		// Is dashing boolean
		ImGui::Text("Is Dash");
		if(ImGui::Checkbox("Is Dash", &isDashing))
		{
			isChanged = true;
		}

		// Super charge meter
		ImGui::Text("Meter Charge");
		if(ImGui::InputFloat("Meter Charge", &chargeMeter))
		{
			isChanged = true;
		}

		// Super charge meter capacity
		ImGui::Text("Meter Charge Capacity");
		if(ImGui::InputFloat("Meter Charge Capacity", &chargeMeterCap))
		{
			isChanged = true;
		}

		// Invicibility cooldown
		ImGui::Text("Invicibility Cooldown");
		if (ImGui::InputFloat("Invicibility Cooldown", &inviciCooldown))
		{
			isChanged = true;
		}

		// isHit cooldown
		ImGui::Text("Hit Reaction Time");
		if (ImGui::InputFloat("Hit Reaction Time", &isHitCooldown))
		{
			isChanged = true;
		}

		return isChanged;
	}

	return false;
}

// FromJson specialized for this component
void PlayerStats::FromJson(json & value)
{
	this->IComp::FromJson(value);

	value["Lives"] >> lives;
	value["Grenade Number"] >> grenades;
	value["Looking Right"] >> isRight;
	value["Looking Up"] >> isUp;
	value["Can Duck"] >> canDuck;
	value["Can Dash"] >> canDash;
	value["Is Dashing"] >> isDashing;
	value["Is Grounded"] >> isGrounded;
	value["Charge Meter"] >> chargeMeter;
	value["Charge Meter Capacity"] >> chargeMeterCap;
	value["Hit Reaction Time"] >> isHitCooldown;
	value["Invicibility Cooldown"] >> inviciCooldown;
}

// ToJson specialized for this component
void PlayerStats::ToJson(json & value)
{
	this->IComp::ToJson(value);

	value["Lives"] << lives;
	value["Grenade Number"] << grenades;
	value["Looking Right"] << isRight;
	value["Looking Up"] << isUp;
	value["Can Duck"] << canDuck;
	value["Can Dash"] << canDash;
	value["Is Dashing"] << isDashing;
	value["Is Grounded"] << isGrounded;
	value["Charge Meter"] << chargeMeter;
	value["Charge Meter Capacity"] << chargeMeterCap;
	value["Hit Reaction Time"] << isHitCooldown;
	value["Invicibility Cooldown"] << inviciCooldown;
}