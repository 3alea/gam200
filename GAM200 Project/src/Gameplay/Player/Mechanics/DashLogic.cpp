#include "Components\SpineAnimation.h"
#include "Composition/AEXScene.h"
#include "Platform/AEXTime.h"
#include "Components/SpriteComps.h"
#include <iostream>
#include "DashLogic.h"
#include "Platform\AEXInput.h"

using namespace AEX;
using namespace ImGui;

void DashLogic::Initialize()
{
	stats = GetOwner()->GetComp<PlayerStats>();
	rb = GetOwner()->GetComp<Rigidbody>();
}

void DashLogic::Update()
{
	SpineAnimationComp* spineAnimation = GetOwner()->GetComp<SpineAnimationComp>();

	if (spineAnimation->CheckCurrentAnimationAtPostion("death", 0))
		return;

	if (stats->isRight)
	{
		dirMultiplyer = 1.f;
	}
	else
		dirMultiplyer = -1.f;

	if (stats->isDashing == true)
	{
		rb->mVelocity = AEVec2(dirMultiplyer * dashSpeed, 0.f);
		rb->mIgnoreGravity = true;
		dashTimer += (f32)aexTime->GetFrameTime();
		stats->canDash = false;
		if (dashTimer >= dashTime)
		{
			stats->isDashing = false;
			rb->mVelocity = AEVec2(0.f, 0.f);
			rb->mIgnoreGravity = false;
		}
	}
	if (stats->onCinematic)
		return;

	// When is the player dashing?
	if (stats->canDash == true && PLAYER_DASHING && stats->canDuck == true)
	{
		audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\player\\Dash.wav"), Voice::SFX);
		spineAnimation->ReloadSpineData();
		stats->isDashing = true;
		dashTimer = 0.f;
	}
}

bool DashLogic::OnGui()
{
	bool isChanged = false;

	// Text for the jump height
	ImGui::Text("Dash Time");
	// Allow user to input jump height from the editor
	if (ImGui::InputFloat("Dash Time", &dashTime))
	{
		isChanged = true;
	}

	// Horizontal speed declaration
	ImGui::Text("Dash Speed");
	if (ImGui::InputFloat("Dash Speed", &dashSpeed))
	{
		isChanged = true;
	}

	return isChanged;
}

void DashLogic::FromJson(json & value)
{
	//this->LogicComp::FromJson(value);
	this->IComp::FromJson(value);

	value["dashTime"] >> dashTime;
	value["dashSpeed"] >> dashSpeed;
}

void DashLogic::ToJson(json & value)
{
	//this->LogicComp::ToJson(value);
	this->IComp::ToJson(value);

	value["dashTime"] << dashTime;
	value["dashSpeed"] << dashSpeed;
}
