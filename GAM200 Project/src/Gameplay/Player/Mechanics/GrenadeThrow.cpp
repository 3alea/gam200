#include "Components\SpineAnimation.h"
#include "Composition/AEXScene.h"
#include "Platform/AEXTime.h"
#include "Platform\AEXInput.h"
#include "GrenadeThrow.h"
#include "Bullets\Grenade.h"
#include "Components/Collider.h"
#include "Player/Movement/PlayerController.h"
#include "AI/Easing.h"
#include "Physics/CollisionSystem.h"
#include <iostream>

using namespace AEX;
using namespace ImGui;

void GrenadeThrow::Initialize()
{
	// Aim object needed to know the direction the grenade is thrown
	aim = aexScene->FindObjectInANYSpace("aim");

	// If the aim exists when grenade is initialized...
	if (aim)
	{
		aimT = aim->GetComp<TransformComp>();
	}

	// Get the stats from the player
	stats = mOwner->GetComp<PlayerStats>();
	t = GetOwner()->GetComp<TransformComp>();

	nPoints = 100;

	mPoints.resize(nPoints);
	mNormalsUp.resize(nPoints);
	mNormalsDown.resize(nPoints);

	timeDiff = 3.0f / mPoints.size();

	mHalfgravity = mOwner->GetParentSpace()->GetGravity()->y / 2.0f;

	timePassed = 0.f;
	grenadeID = 0;

	v.x = 0.0f;
	v.y = 0.0f;
	timer = 0.0f;

	// Make sure the initial velocity is normalized
	InitialVelocity.NormalizeThis();
	duckInitialVelocity.NormalizeThis();

	// SHOOT GRENADE SPINE ANIMATION
	SpineAnimationComp* spineAnimation = GetOwner()->GetComp<SpineAnimationComp>();
}

void GrenadeThrow::Update()
{
	return;
	// If something was not loaded correctly...
	if (!aim || !stats)
	{
		Initialize();
	}
	// Start charging the shot
	if (CHARGE_GRENADE && canShoot && stats->grenades > 0)
	{
		// Get the direction of the grenade
		getInitialPosition();

		// Charge shot
		chargeShot();

		// Show trajectory
		graphTrajectory();
		
		//GfxMgr->DrawLine(DebugLine(AEVec4(1, 1, 1, 1), p0, p0 + v));

		// Direction vector which decides where the grenade will be thrown
		//AEVec2 direction;
		//
		//// Case that the player is not ducking
		//if (stats->canDuck)
		//{
		//	// Calculate orientation of the bullet
		//	// The direction/orientation = aim Translation - player Translation;
		//	direction = -AEVec2(t->mLocal.mTranslationZ.x, t->mLocal.mTranslationZ.y) + AEVec2(aimT->mLocal.mTranslationZ.x, aimT->mLocal.mTranslationZ.y);
		//}
		//// When ducking
		//else
		//{
		//	// The direction/orientation = aim Translation - player Translation;
		//	// In this case we do not consider the y axis, as the player may only shoot forwards
		//	direction = -AEVec2(t->mLocal.mTranslationZ.x, 0) + AEVec2(aimT->mLocal.mTranslationZ.x, 0);
		//}

		
	}

	// Disable grenades for the playtesting
	//if (RELEASE_GRENADE && canShoot && stats->grenades > 0 && !canShoot)
	//{
	//	// Reset the timer
	//	timer = 0.0f;
	//
	//	// Create object using archetypes!
	//	
	//	GameObject * grenade = mOwner->GetParentSpace()->InstantiateArchetype("grenade", aimT->mLocal.mTranslationZ);
	//	GrenadeLogic* logic = grenade->GetComp<GrenadeLogic>(); 
	//	logic->Velocity = v;
	//	logic->p0 = p0;
	//	logic->maxTime = grenadeMaxTime;
	//	
	//	// Decrease one from the number of grenades the player has
	//	stats->grenades--;
	//	
	//	// Set canShoot to false
	//	canShoot = false;
	//
	//	v.x = 0.0f;
	//	v.y = 0.0f;
	//}

	// SHOOT SPINE ANIMATION
	SpineAnimationComp* spineAnimation = GetOwner()->GetComp<SpineAnimationComp>();

	// Cooldown updater
	if (canShoot == false)
	{
		timePassed += (f32)aexTime->GetFrameTime();
		if (timePassed >= shootTime)
		{
			canShoot = true;
			timePassed = 0.f;
		}
	}
}

bool GrenadeThrow::OnGui()
{
	// Collapsing header - when clicked, opens up to show the rest of edited variables
	if (ImGui::CollapsingHeader("GrenadeThrow"))
	{
		// isChanged boolean to return if something has changed
		bool isChanged = false;

		// Text for the cooldown
		ImGui::Text("Grenade cooldown");
		// Allow user to input cooldown from the editor
		if (ImGui::InputFloat("Grenade cooldown", &shootTime))
		{
			isChanged = true;
		}

		ImGui::Text("Stand Up Right Launching Position");
		if (ImGui::InputFloat2("stand up R launch pos", standUpInitialPosR.v))
		{
			p0 = AEVec2(t->mLocal.mTranslationZ.x, t->mLocal.mTranslationZ.y) + standUpInitialPosR;
			isChanged = true;
		}

		ImGui::Text("Stand Up Left Launching Position");
		if (ImGui::InputFloat2("stand up L launch pos", standUpInitialPosL.v))
		{
			p0 = AEVec2(t->mLocal.mTranslationZ.x, t->mLocal.mTranslationZ.y) + standUpInitialPosL;

			isChanged = true;
		}

		ImGui::Text("Duck Right Launching Position");
		if (ImGui::InputFloat2("duck R launch pos", duckInitialPosR.v))
		{
			p0 = AEVec2(t->mLocal.mTranslationZ.x, t->mLocal.mTranslationZ.y) + duckInitialPosR;
			isChanged = true;
		}

		ImGui::Text("Duck Left Launching Position");
		if (ImGui::InputFloat2("duck L launch pos", duckInitialPosL.v))
		{
			p0 = AEVec2(t->mLocal.mTranslationZ.x, t->mLocal.mTranslationZ.y) + duckInitialPosL;
			isChanged = true;
		}

		// Text for the direction
		ImGui::Text("Grenade Stand Up Initial direction");
		// Allow user to input speed number from the editor
		if (ImGui::InputFloat2("Grenade stand up direction", InitialVelocity.v))
		{
			isChanged = true;
		}

		ImGui::Text("Grenade Duck Initial direction");
		// Allow user to input speed number from the editor
		if (ImGui::InputFloat2("Grenade duck direction", duckInitialVelocity.v))
		{
			isChanged = true;
		}

		ImGui::Text("Grenade Initial speed");
		// Allow user to input speed number from the editor
		if (ImGui::InputFloat("Grenade initial speed", &InitialSpeed))
		{
			isChanged = true;
		}

		ImGui::Text("Grenade Max Initial speed");
		// Allow user to input speed number from the editor
		if (ImGui::InputFloat("Grenade max initial speed", &MaxInitialSpeed))
		{
			isChanged = true;
		}

		// Max time
		ImGui::Text("Grenade max time");
		if (ImGui::InputFloat("Grenade max time", &timeToMaxCharge))
		{
			isChanged = true;
		}
		drawP0();

		graphHighTrajectory();
		graphLowTrajectory();
		return isChanged;
	}

	
	return false;
}

// FromJson specialized for this component
void GrenadeThrow::FromJson(json & value)
{
	this->IComp::FromJson(value);

	value["Grenade cooldown"] >> shootTime;
	value["Grenade Speed"] >> InitialSpeed;
	value["Grenade init max speed"] >> MaxInitialSpeed;
	value["time to max charge"] >> timeToMaxCharge;
	value["Grenade Time Alive"] >> grenadeMaxTime;
	value["duck initial v"] >> duckInitialVelocity;
	value["initial velocity"] >> InitialVelocity;
	value["stand up right pos"] >> standUpInitialPosR;
	value["stand up left pos"] >> standUpInitialPosL;
	value["duck right pos"] >> duckInitialPosR;
	value["duck left pos"] >> duckInitialPosL;
}

// ToJson specialized for this component
void GrenadeThrow::ToJson(json & value)
{
	this->IComp::ToJson(value);

	value["Grenade cooldown"] << shootTime;
	value["Grenade Speed"] << InitialSpeed;
	value["Grenade init max speed"] << MaxInitialSpeed;
	value["time to max charge"] << timeToMaxCharge;
	value["Grenade Time Alive"] << grenadeMaxTime;
	value["initial velocity"] << InitialVelocity;
	value["duck initial v"] << duckInitialVelocity;
	value["stand up right pos"] << standUpInitialPosR;
	value["stand up left pos"] << standUpInitialPosL;
	value["duck right pos"] << duckInitialPosR;
	value["duck left pos"] << duckInitialPosL;
}

void GrenadeThrow::getInitialPosition()
{
	// Player looking at the right
	if (stats->isRight)
	{
		// Starting point of the grenade when the player is up
		if (stats->canDuck)
		{
			p0 = AEVec2(t->mLocal.mTranslationZ.x, t->mLocal.mTranslationZ.y) + standUpInitialPosR;
			v0.x = InitialVelocity.x;
			v0.y = InitialVelocity.y;
		}

		// Starting point of the grenade when the player is down
		else
		{
			p0 = AEVec2(t->mLocal.mTranslationZ.x, t->mLocal.mTranslationZ.y) + duckInitialPosR;
			v0.x = duckInitialVelocity.x;
			v0.y = duckInitialVelocity.y;
		}
	}

	// Otherwise, the player is looking at the left
	else
	{
		// Starting point of the grenade when the player is up
		if (stats->canDuck)
		{
			p0 = AEVec2(t->mLocal.mTranslationZ.x, t->mLocal.mTranslationZ.y) + standUpInitialPosL;
			v0.x = -InitialVelocity.x;
			v0.y = InitialVelocity.y;
		}

		// Starting point of the grenade when the player is down
		else
		{
			p0 = AEVec2(t->mLocal.mTranslationZ.x, t->mLocal.mTranslationZ.y) + duckInitialPosL;
			v0.x = -duckInitialVelocity.x;
			v0.y = duckInitialVelocity.y;
		}
	}

	//	bool right = PLAYER_GOING_RIGHT;
	//
	//	// The player is aiming vertically
	//	if (stats->isUp && !right)
	//	{
	//		InitialVelocity.y = 1.0f;
	//		InitialVelocity.x = 0.0f;
	//
	//		p0Offset.x = 0.8f;
	//		p0Offset.y = 1.5f;
	//	}
	//
	//	// The player is aiming diagonally
	//	else if (right && stats->isUp)
	//	{
	//		InitialVelocity.y = 1.0f;
	//		InitialVelocity.x = 1.0f;
	//
	//		p0Offset.x = 1.7f;
	//		p0Offset.y = 1.1f;
	//	}
	//
	//	// The player is not aiming vertically
	//	else if (stats->canDuck)
	//	{
	//		InitialVelocity.x = 1.0f;
	//		InitialVelocity.y = 0.0f;
	//
	//		p0Offset.x = 2.1f;
	//		p0Offset.y = 0.21f;
	//	}
	//
	//	// Player is ducking
	//	else
	//	{
	//		InitialVelocity.x = 1.0f;
	//		InitialVelocity.y = 0.0f;
	//
	//		p0Offset.x = 2.1f;
	//		p0Offset.y = -0.2f;
	//	}
	//}
	//
	//// Player looking at the left
	//else
	//{
	//	bool left = PLAYER_GOING_LEFT;
	//
	//	// The player is aiming vertically
	//	if (stats->isUp && !left)
	//	{
	//		InitialVelocity.y = 1.0f;
	//		InitialVelocity.x = 0.0f;
	//
	//		p0Offset.x = -0.2f;
	//		p0Offset.y = 1.5f;
	//	}
	//
	//	// The player is aiming diagonally
	//	else if (left && stats->isUp)
	//	{
	//		InitialVelocity.y = 1.0f;
	//		InitialVelocity.x = -1.0f;
	//
	//		p0Offset.x = -1.1f;
	//		p0Offset.y = 1.1f;
	//	}
	//
	//	// The player is not aiming vertically
	//	else if (stats->canDuck)
	//	{
	//		InitialVelocity.x = -1.0f;
	//		InitialVelocity.y = 0.0f;
	//
	//		p0Offset.x = -1.5f;
	//		p0Offset.y = 0.21f;
	//	}
	//
	//	// Player is ducking
	//	else
	//	{
	//		InitialVelocity.x = -1.0f;
	//		InitialVelocity.y = 0.0f;
	//
	//		p0Offset.x = -1.5f;
	//		p0Offset.y = -0.2f;
	//	}
	//}
}

void GrenadeThrow::chargeShot()
{
	// Check if the velocity is not already fully charged
	if (timer <= timeToMaxCharge)
	{
		// Update the timer
		timer += (f32)aexTime->GetFrameTime();

		// Ease the timer
		easedTimer = EaseInOutQuad(timer / timeToMaxCharge);

		// Lerp the speed
		v = v0 * (InitialSpeed + ((MaxInitialSpeed - InitialSpeed) * easedTimer));
	}
	else
		v = v0 * MaxInitialSpeed;
}

void GrenadeThrow::graphTrajectory()
{
	// t0 already calculated. (p0)
	// Start from t1
	float t = timeDiff;

	// easing lambda function 
	auto ease = [] (f32 tn)
	{
		return -((2 * tn - 1)*(2 * tn - 1)) + 1;
	};

	mPoints[0] = p0;
	unsigned i = 1;
	// Calculate the points of the current trajectory
	for (; i < nPoints; ++i, t += timeDiff)
	{
		mPoints[i].x = p0.x + v.x * t;
		mPoints[i].y = p0.y + v.y * t + mHalfgravity * t * t;

		GfxMgr->DrawLine(DebugLine(AEVec4(1, 0, 0, 1), mPoints[i - 1], mPoints[i]));

		// If the trajectory hits a collider, stop drawing
		if (aexCollisionSystem->CollideLine(&mPoints[i - 1], &mPoints[i]))
			break;
		
	}

	// after we reach here, i+1 contains the number of points we have (could be equal to nPoints)
	// t is the time of impact or tmax (3 seconds). 
	f32 tmax = t;
	u32 maxPoints = min(i + 1, nPoints); // todo check if i +1 is ever going to be more than nPoints. 

	// draw normals
	f32 tn = 0.0f;
	f32 tn_step = tmax / (f32)(maxPoints-1);
	f32 nMax = 0.5f, nMin = 0.05f;
	for (i = 0; i < maxPoints-1; ++i, tn += tn_step) {
		auto p1 = mPoints[i];
		auto p2 = mPoints[i + 1];
		f32 nlength = Lerp(nMin, nMax, ease(tn/tmax));
		AEVec2 edge = (p2 - p1).Normalize() * nlength;
		AEVec2 perp = edge.Perp();

		mNormalsUp[i] = p1 + perp;
		mNormalsDown[i] = p1 - perp;
	}

	// last segment
	{
		auto p1 = mPoints[i - 1];
		auto p2 = mPoints[i];
		AEVec2 edge = (p2 - p1).Normalize() * nMin;
		AEVec2 perp = edge.Perp();
		 
		mNormalsUp[i] = p2+perp;
		mNormalsDown[i] = p2 - perp;
	}

	// draw the normals
	for (i = 0; i < maxPoints - 1; ++i) {

		GfxMgr->DrawLine(DebugLine(AEVec4(0, 1, 0, 1), mNormalsUp[i + 1], mNormalsUp[i]));
		GfxMgr->DrawLine(DebugLine(AEVec4(0, 0, 1, 1), mNormalsDown[i + 1], mNormalsDown[i]));
	}

	// generate mesh
	/*
		// initiailze
		GameObject * trajectory = GetOwner()->GetChild("Trajectory");
		Renderable * ren = trajectory->GetComponent<Renderable>();
		
		ren->Model->AddVertex(mNormal[Up]
	*/

}

void GrenadeThrow::graphLowTrajectory()
{
	float t = timeDiff;

	v = InitialVelocity.Normalize();
	AEVec2 perpVec;
	float angle;
	// Calculate the points of the current trajectory
	for (unsigned i = 0; i < nPoints; ++i, t += timeDiff)
	{
		mPoints[i].x = p0.x + (v * InitialSpeed).x * t;
		mPoints[i].y = p0.y + (v * InitialSpeed).y * t + mHalfgravity * t * t;

		if (i == 0)
		{

			angle = (mPoints[i] - p0).GetAngle();

			perpVec.FromAngle(angle + PI / 2.0f);

			perpVec *= 0.1f;

			GfxMgr->DrawLine(DebugLine(AEVec4(1, 0, 0, 1), p0, mPoints[i]));
			GfxMgr->DrawLine(DebugLine(AEVec4(0, 1, 0, 1), p0 + perpVec, mPoints[i] + perpVec));
			GfxMgr->DrawLine(DebugLine(AEVec4(0, 0, 1, 1), p0 - perpVec, mPoints[i] - perpVec));
		}
		else
		{

			angle = (mPoints[i] - mPoints[i - 1]).GetAngle();

			perpVec.FromAngle(angle + PI / 2.0f);

			perpVec *= 0.1f;

			GfxMgr->DrawLine(DebugLine(AEVec4(1, 0, 0, 1), mPoints[i - 1], mPoints[i]));
			GfxMgr->DrawLine(DebugLine(AEVec4(0, 1, 0, 1), mPoints[i - 1] + perpVec, mPoints[i] + perpVec));
			GfxMgr->DrawLine(DebugLine(AEVec4(0, 0, 1, 1), mPoints[i - 1] - perpVec, mPoints[i] - perpVec));
		}
	}
	v.x = 0;
	v.y = 0;
}

void GrenadeThrow::graphHighTrajectory()
{
	float t = timeDiff;
	AEVec2 perpVec;
	float angle;

	v = InitialVelocity.Normalize();
	for (unsigned i = 0; i < nPoints; ++i, t += timeDiff)
	{
		mPoints[i].x = p0.x + (v * MaxInitialSpeed).x * t;
		mPoints[i].y = p0.y + (v * MaxInitialSpeed).y * t + mHalfgravity * t * t;

		if (i == 0)
		{

			angle = (mPoints[i] - p0).GetAngle();

			perpVec.FromAngle(angle + PI / 2.0f);

			perpVec *= 0.1f;

			GfxMgr->DrawLine(DebugLine(AEVec4(1, 0, 0, 1), p0, mPoints[i]));
			GfxMgr->DrawLine(DebugLine(AEVec4(0, 1, 0, 1), p0 + perpVec, mPoints[i] + perpVec));
			GfxMgr->DrawLine(DebugLine(AEVec4(0, 0, 1, 1), p0 - perpVec, mPoints[i] - perpVec));
		}
		else
		{
			angle = (mPoints[i] - mPoints[i - 1]).GetAngle();

			perpVec.FromAngle(angle + PI / 2.0f);

			perpVec *= 0.1f;

			GfxMgr->DrawLine(DebugLine(AEVec4(1, 0, 0, 1), mPoints[i - 1], mPoints[i]));
			GfxMgr->DrawLine(DebugLine(AEVec4(0, 1, 0, 1), mPoints[i - 1] + perpVec, mPoints[i] + perpVec));
			GfxMgr->DrawLine(DebugLine(AEVec4(0, 0, 1, 1), mPoints[i - 1] - perpVec, mPoints[i] - perpVec));
		}
	}
	v.x = 0;
	v.y = 0;
}

void GrenadeThrow::drawP0()
{
	GfxMgr->DrawCircle(DebugCircle(p0, 0.2f, 5, AEVec4(1, 0, 0, 1)));
}
