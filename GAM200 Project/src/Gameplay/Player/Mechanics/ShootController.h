#pragma once
#include "Audio\AudioManager.h"
#include "Composition\AEXGameObject.h"
#include "Components\AEXTransformComp.h"
#include "Player/Stats/PlayerStats.h"
#include "Components\Rigidbody.h"
#include "Components\Logic.h"
#include "Imgui\imgui.h"
using namespace AEX;

#define PLAYER_SHOOTING (aexInput->KeyPressed(GLFW_KEY_Q) || aexInput->GamepadButtonPressed(Input::GamepadButtons::eGamepadButtonX))
#define PLAYER_SUPER_SHOT (aexInput->KeyPressed(GLFW_KEY_R) || aexInput->GamepadButtonPressed(Input::GamepadButtons::eGamepadButtonB))

class Follower;

class ShootController : public LogicComp
{
	AEX_RTTI_DECL(ShootController, LogicComp);
public:
	// Aim gameobject
	GameObject* aim;

	Follower* gun;

	// Object TranformComp
	TransformComp* t;

	// Aim TransformCom
	TransformComp* aimT;

	TransformComp* currentBulletT;

	// RigidBody access to remove gravity when shooting with super
	Rigidbody* rb;

	PlayerStats* stats;

	float shootTime;

	float timePassed;

	bool canShoot;

	float superTime;

	float superTimePassed;

	// Start is called before the first frame update
	virtual void Initialize();

	// Update is called once per frame
	virtual void Update();

	// OnGui() function
	virtual bool OnGui();
	
	// FromJson specialized for this component
	virtual void FromJson(json & value);
	// ToJson specialized for this component
	virtual void ToJson(json & value);
};