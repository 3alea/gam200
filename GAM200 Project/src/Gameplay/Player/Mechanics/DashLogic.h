#pragma once

#include "Composition\AEXGameObject.h"
#include "Components\Rigidbody.h"
#include "Player/Stats/PlayerStats.h"
#include "Components\Logic.h"
#include "Imgui\imgui.h"
#include "Audio\AudioManager.h"

using namespace AEX;

#define PLAYER_DASHING (aexInput->KeyTriggered(GLFW_KEY_W) || aexInput->GamepadButtonTriggered(Input::GamepadButtons::eGamepadButtonY))

class DashLogic : public LogicComp
{
	AEX_RTTI_DECL(DashLogic, LogicComp);
public:

	// Object PlayerStats
	PlayerStats* stats;

	// Object RigidBody
	Rigidbody* rb;

	float dirMultiplyer;

	float dashSpeed;

	float dashTime;

	float remWeight;

	// Start is called before the first frame update
	virtual void Initialize();

	// Update is called once per frame
	virtual void Update();

	// OnGui() function
	virtual bool OnGui();
	
	// FromJson specialized for this component
	virtual void FromJson(json & value);
	// ToJson specialized for this component
	virtual void ToJson(json & value);

private:
	float dashTimer;
	AEVec2 mDirection;
	bool canDash;
};