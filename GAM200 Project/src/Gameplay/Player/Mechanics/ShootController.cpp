#include "Components\SpineAnimation.h"
#include <spine\AnimationState.h>
#include "Composition/AEXScene.h"
#include "Platform/AEXTime.h"
#include "Platform\AEXInput.h"
#include "Components/SpriteComps.h"
#include "ShootController.h"
#include "Bullets/BulletLogic.h"
#include "Components/Collider.h"
#include "Follow\Follower.h"
#include <iostream>

using namespace AEX;
using namespace ImGui;

class spine::Skeleton;
class spine::SkeletonData;

void ShootController::Initialize()
{
	aim = aexScene->FindObjectInANYSpace("aim");
	gun = nullptr;
	if (aim)
	{
		aimT = aim->GetComp<TransformComp>();
		gun = aim->GetComp<Follower>();
	}

	stats = mOwner->GetComp<PlayerStats>();
	rb = GetOwner()->GetComp<Rigidbody>();
	t = GetOwner()->GetComp<TransformComp>();
	timePassed = 0.f;
	stats->canSuperShot = true;
	
	
}

void ShootController::Update()
{
	SpineAnimationComp* spineAnimation = GetOwner()->GetComp<SpineAnimationComp>();

	if (spineAnimation->CheckCurrentAnimationAtPostion("death", 0))
		return;

	// If the aim has not loaded correctly...
	if (!aim || !stats ||!gun)
	{
		Initialize();
	}

	if (PLAYER_SHOOTING && canShoot && !stats->onCinematic && !stats->isDashing && stats->canSuperShot)
	{
		audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\player\\Shot.mp3"), Voice::SFX)->SetVolume(0.3f);
		
		float bulletDir = gun->InitialVelocity.GetAngle();

		// Create object
		GameObject* bullet = mOwner->GetParentSpace()->InstantiateArchetype("bullet", aimT->mLocal.mTranslationZ, bulletDir);

		// Set canShoot to false
		canShoot = false;
	}

	if (PLAYER_SUPER_SHOT && stats->canSuperShot && stats->chargeMeter >= 30.f && !stats->onCinematic)
	{
		audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\player\\SuperShot.wav"), Voice::SFX);

		spineAnimation->ChangeAnimation(0, "Super shot", false, 1.5f);
		rb->mIgnoreGravity = true;
		stats->chargeMeter -= 33.f;
		stats->inviciTimer = 0.f;
		stats->canSuperShot = false;
	}

	// If supershot is triggered
	if (stats->superShot)
	{
		AEVec2 superDir = -AEVec2(t->mLocal.mTranslationZ.x, 0) + AEVec2(aimT->mLocal.mTranslationZ.x, 0);
		float orientation = superDir.GetAngle();
		GameObject* super = mOwner->GetParentSpace()->InstantiateArchetype("supershot", aimT->mLocal.mTranslationZ, orientation);
		stats->superShot = false;
	}

	if (canShoot == false)
	{
		timePassed += (f32)aexTime->GetFrameTime();
		if (timePassed >= shootTime)
		{
			canShoot = true;
			timePassed = 0.f;
		}
	}

	// Maintain animation until it is over
	if (!stats->canSuperShot && spineAnimation->CheckCurrentAnimationAtPostion("Super shot", 0) && !spineAnimation->GetAnimState()->getCurrent(0)->isComplete())
	{
		if (spineAnimation->GetAnimState()->getCurrent(0)->getTrackTime() >= spineAnimation->GetAnimState()->getCurrent(0)->getAnimationEnd() - (f32)aexTime->GetFrameTime())
		{
			stats->isInvincible = false;
			stats->isHit = false;
			stats->inviciTimer = 0.f;
			spineAnimation->mColor[3] = 1.0f;
		}
		return;
	}

	// SHOOT ANIMATION
	if (PLAYER_SHOOTING && !stats->onCinematic && !stats->isDashing)
	{
		spineAnimation->ChangeAnimation(2, "Shoot", true, 1.f/(shootTime * 6.f), 0.f);
		stats->isShooting = true;
	}
	else if(spineAnimation->CheckCurrentAnimationAtPostion("Shoot", 2) && spineAnimation->GetAnimState()->getCurrent(2)->isComplete())
	{
		spineAnimation->ClearAnimationAtPosition(2);
		stats->isShooting = false;
	}
}

bool ShootController::OnGui()
{
	// isChanged boolean to return if something has changed
	bool isChanged = false;

	// Collapsing header - when clicked, opens up to show the rest of edited variables
	if (ImGui::CollapsingHeader("PlayerController"))
	{
		// Text for the shoot time
		ImGui::Text("Shoot time");
		// Allow user to input shoot time
		if (ImGui::InputFloat("Shoot time", &shootTime))
		{
			isChanged = true;
		}
	}

	return isChanged;
}

// FromJson specialized for this component
void ShootController::FromJson(json & value)
{
	this->IComp::FromJson(value);

	value["Shoot time"] >> shootTime;
}

// ToJson specialized for this component
void ShootController::ToJson(json & value)
{
	this->IComp::ToJson(value);

	value["Shoot time"] << shootTime;
}