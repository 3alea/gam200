#pragma once

#include "Composition\AEXGameObject.h"
#include "Components\AEXTransformComp.h"
#include "Player/Stats/PlayerStats.h"
#include "Components\Logic.h"
#include "Imgui\imgui.h"

using namespace AEX;

#define CHARGE_GRENADE (aexInput->KeyPressed(GLFW_KEY_TAB) || aexInput->GamepadButtonPressed(Input::GamepadButtons::eGamepadButtonLeftBumper))
#define RELEASE_GRENADE (aexInput->KeyReleased(GLFW_KEY_TAB) || aexInput->GamepadButtonReleased(Input::GamepadButtons::eGamepadButtonLeftBumper))
class GrenadeThrow : public LogicComp
{
	AEX_RTTI_DECL(GrenadeThrow, LogicComp);
public:
	// Aim gameobject
	GameObject* aim;

	// Object TranformComp
	TransformComp* t;

	// Aim TransformCom
	TransformComp* aimT;

	// Player stats access
	PlayerStats* stats;

	// The timer used for grenade cooldown
	float timePassed;

	// The grenade cooldown
	float shootTime;

	// Used with the grenade cooldown to prevent throws
	bool canShoot;

	// Unique grenade id used to name bullets differently
	int grenadeID;

	/////// Stats to be passed to the grenades ///////

	// Grenade speed

	///////////Trajectory//////////
	std::vector<AEVec2>mPoints;
	std::vector<AEVec2>mNormalsUp;
	std::vector<AEVec2>mNormalsDown;
	float timeDiff;
	unsigned nPoints;
	float mHalfgravity;
	////////Modulus////////////
	float InitialSpeed;
	float MaxInitialSpeed;
	////////Modulus////////////

	////////Direction/////////
	AEVec2 InitialVelocity;
	AEVec2 duckInitialVelocity;

	AEVec2 v0;
	AEVec2 v;
	////////Direction/////////

	////////StartingPoint//////
	AEVec2 duckInitialPosR;
	AEVec2 duckInitialPosL;

	AEVec2 standUpInitialPosR;
	AEVec2 standUpInitialPosL;

	AEVec2 p0;
	/////////StartingPos//////

	float timer;
	float easedTimer;
	float timeToMaxCharge;

	// Grenade maximum time alive
	float grenadeMaxTime;

	//////////////////////////////////////////////////

	// Start is called before the first frame update
	virtual void Initialize();

	// Update is called once per frame
	virtual void Update();

	// OnGui() function
	virtual bool OnGui();
	
	// FromJson specialized for this component
	virtual void FromJson(json & value);
	// ToJson specialized for this component
	virtual void ToJson(json & value);

	void getInitialPosition();
	void chargeShot();
	void graphTrajectory();
	void graphLowTrajectory();
	void graphHighTrajectory();

	void drawP0();
};