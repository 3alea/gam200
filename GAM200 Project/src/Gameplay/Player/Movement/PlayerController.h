#pragma once

#include "Composition\AEXGameObject.h"
#include "Components\SpriteComps.h"
#include "Components\AEXTransformComp.h"
#include "Components\Logic.h"
#include "Components\Rigidbody.h"
#include "Components/Collider.h"
#include "Player/Stats/PlayerStats.h"
#include "Imgui\imgui.h"
#include "Audio\AudioManager.h"

#define RESPONSE_X_OFFSET 0.4f
#define RESPONSE_y_OFFSET 0.5f

#define LEFT_STICK_USED    (aexInput->GetGamepadStick(Input::GamepadSticks::eLeftStickXAxis) > RESPONSE_X_OFFSET || aexInput->GetGamepadStick(Input::GamepadSticks::eLeftStickXAxis) < -RESPONSE_X_OFFSET)
#define LEFT_STICK_X_VALUE (aexInput->GetGamepadStick(Input::GamepadSticks::eLeftStickXAxis))
#define PLAYER_GOING_RIGHT (aexInput->KeyPressed(GLFW_KEY_RIGHT)   || aexInput->GetGamepadStick(Input::GamepadSticks::eLeftStickXAxis)   >  RESPONSE_X_OFFSET)
#define PLAYER_GOING_LEFT  (aexInput->KeyPressed(GLFW_KEY_LEFT)    || aexInput->GetGamepadStick(Input::GamepadSticks::eLeftStickXAxis)   < -RESPONSE_X_OFFSET)
#define PLAYER_GOING_UP    (aexInput->KeyPressed(GLFW_KEY_UP)      || aexInput->GetGamepadStick(Input::GamepadSticks::eLeftStickYAxis)   < -RESPONSE_y_OFFSET)
#define PLAYER_GOING_DOWN  (aexInput->KeyPressed(GLFW_KEY_DOWN)    || aexInput->GetGamepadStick(Input::GamepadSticks::eLeftStickYAxis)   >  RESPONSE_y_OFFSET)
#define PLAYER_JUMPING     (aexInput->KeyTriggered(GLFW_KEY_SPACE) || aexInput->GamepadButtonTriggered(Input::GamepadButtons::eGamepadButtonA))
#define PLAYER_IS_MOVING   (PLAYER_GOING_RIGHT || PLAYER_GOING_LEFT || PLAYER_GOING_UP || PLAYER_GOING_DOWN || PLAYER_JUMPING)
#define PLAYER_LOCK_AIM    (aexInput->KeyPressed(GLFW_KEY_E)       || aexInput->GamepadButtonPressed(Input::GamepadButtons::eGamepadButtonRightBumper))

#define COLLIDER_SIZE_Y 1.75f
#define COLLIDER_DUCK_SIZE_Y 1.17f

using namespace AEX;

template <typename T>
class TResource;
class SpineAnimationComp;
class PlayerController : public LogicComp
{
	AEX_RTTI_DECL(PlayerController, LogicComp);
public:
	// Duck 
	void Shrink();

	// ... and regrow back
	void Grow();

	// For flipping the character
	float myOrientation = 0;

	// Player stats to modify
	PlayerStats* stats;

	// Player transform reference
	TransformComp* t;

	// RigidBody access
	Rigidbody* rb;

	// BoxCollider access
	BoxCollider* bc;

	// Main SpineAnimation
	SpineAnimationComp* spineAnimation;

	// Vertical speed declaration
	float vSpeed = 1.0f;

	// Horizontal speed declaration
	float hSpeed = 1.0f;

	// Final additional speed to add to the player
	float totalSpeed;

	AEVec2 speedDir;

	// Boolean to check if the player can double jump
	bool canDoubleJump;

	// Totally invinvible boolean for hack
	bool totallyInvincible = false;

	// Move in car levels to the left
	bool moveLeftAuto = false;
	f32 moveYPosAuto;

	// Start is called before the first frame update
	virtual void Initialize();

	// Update is called once per frame
	virtual void Update();

	// OnGui() function
	virtual bool OnGui();

	virtual void OnCollisionStarted(const CollisionStarted& collision);

	// FromJson specialized for this component
	virtual void FromJson(json & value);
	// ToJson specialized for this component
	virtual void ToJson(json & value);

	bool unai = false;
};