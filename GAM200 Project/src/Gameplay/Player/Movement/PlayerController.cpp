#include <spine\spine.h>
#include "Resources/AEXResourceManager.h"
#include "Platform\AEXInput.h"
#include "Platform/AEXTime.h"
#include "PlayerController.h"
#include "src/Engine/Graphics/GfxMgr.h"
#include "Components\IgnoreGravityEffect.h"
#include "Components\SpineAnimation.h"
#include "Editor\Editor.h"
#include "aexmath/AEXVec3.h"
#include "Activator/FadeInOutActivator.h"
using namespace AEX;
using namespace ImGui;

bool ActivateAudio = true;

void PlayerController::Shrink()
{
	stats->canDuck = false;

	// Move downwards
	//t->mWorld.mTranslationZ.y -= new scale/4;
	bc->mOffset.y = -.291666f;

	// Shrink on the y axis
	bc->mScale.y = COLLIDER_DUCK_SIZE_Y;
}

void PlayerController::Grow()
{
	stats->canDuck = true;

	// Grow on the y axis
	bc->mScale.y = COLLIDER_SIZE_Y;

	// Move upwards
	//t->mWorld.mTranslationZ.y += new scale/4;
	bc->mOffset.y = 0;
}

void PlayerController::Initialize()
{
	t = GetOwner()->GetComp<TransformComp>();
	rb = GetOwner()->GetComp<Rigidbody>();
	bc = GetOwner()->GetComp<BoxCollider>();
	// SPINE STUFF
	spineAnimation = GetOwner()->GetComp<SpineAnimationComp>();
	if (spineAnimation)
	{
		spineAnimation->ChangeAnimation(0, "Static 34", 0);
		myOrientation = spineAnimation->GetSkeleton()->getScaleX();
	}


	stats = GetOwner()->GetComp<PlayerStats>();

	if (stats)
	{
		stats->xDiff = 0;
	}
	canDoubleJump = false;
	stats->isMoving = false;
}

void PlayerController::Update()
{
	// Invincible hack
	if (aexInput->KeyPressed(GLFW_KEY_LEFT_CONTROL) && aexInput->KeyPressed(GLFW_KEY_LEFT_ALT) && aexInput->KeyTriggered(GLFW_KEY_I))
	{
		totallyInvincible = !totallyInvincible;
	}
	if (totallyInvincible)
	{
		stats->isInvincible = true;
	}

	if (moveLeftAuto)
	{
		t->mLocal.mTranslationZ.x -= 0.5f;
		t->mLocal.mTranslationZ.y  = moveYPosAuto;
		rb->mIgnoreGravity = true;

	}

	// Death controller
	if (stats->lives <= 0)
	{
		spineAnimation->ChangeAnimation(0, "death", false, 1.5f);
		if (spineAnimation->GetAnimState()->getCurrent(0)->isComplete())
		{
			GameObject* fadeout = mOwner->GetParentSpace()->FindObject("fadeoutactivate");

			GameObject * camHud = aexScene->FindObjectInANYSpace("CameraHUD");
			TransformComp * camHudTr = camHud->GetComp<TransformComp>();

			if (!fadeout)
			{
				fadeout = aexScene->FindSpace("Main")->InstantiateArchetype("fadeoutactivate", camHudTr->mLocal.mTranslationZ);
				FadeInOutActivator* fade = fadeout->GetComp<FadeInOutActivator>();
				fade->active = true;
				fade->done = false;
				fade->initialized = true;
				fadeout->GetComp<TransformComp>()->mLocal.mScale = { 500.0f, 500.0f };
				fade->sprite = fadeout->GetComp<SpriteComp>();
			}
			else
			{
				FadeInOutActivator* fade = fadeout->GetComp<FadeInOutActivator>();
				fade->active = true;
				fade->done = false;
				fade->initialized = true;
				fade->sprite = fadeout->GetComp<SpriteComp>();
			}
				
			if (fadeout->GetComp<FadeInOutActivator>()->sprite->mColor[3] >= 0.9f)
			{
				mOwner->GetParentSpace()->DestroyObject(fadeout);
				Editor->loadCheckpoint = true;
			}
		}
		return;
	}

	// Charge meter limit
	if (stats->chargeMeter >= stats->chargeMeterCap)
	{
		stats->chargeMeter = stats->chargeMeterCap;
	}

	if (!rb)
	{
		Initialize();
	}

	///////////////////////////////////////////
	//// IF SUPER SHOOTING, DO NOT UPDATE /////
	///////////////////////////////////////////
	if (spineAnimation->CheckCurrentAnimationAtPostion("Super shot", 0) && !spineAnimation->GetAnimState()->getCurrent(0)->isComplete())
	{
		stats->superTimer += (f32)aexTime->GetFrameTime();
		rb->mVelocity.y = 0.f;
		stats->isInvincible = true;
		spineAnimation->mColor[3] = 1.f;
		if (stats->superTimer >= stats->superShotSpawnTime)
		{
			stats->superShot = true;
			stats->superTimer = 0.f;
			rb->mIgnoreGravity = false;
		}
		return;
	}
	else if (spineAnimation->CheckCurrentAnimationAtPostion("Super shot", 0) && spineAnimation->GetAnimState()->getCurrent(0)->isComplete())
		stats->isInvincible = false;
	else
	{
		// the flag to false and is messing the walk on slopes component.
		stats->superTimer = 0.f;
		stats->canSuperShot = true;
	}

	//std::cout << aexInput->GamepadButtonTriggered(Input::GamepadButtons::eGamepadButtonA) << std::endl;
	if (stats->footColliding == true)
	{
		stats->isGrounded = true;
		stats->footColliding = false;
	}
	//if (mOwner->GetParentSpace()->GetGravity()->y != -70.f)
	//{
	//	mOwner->GetParentSpace()->SetGravity(AEVec2(0.f, -70.f));
	//	rb->SetGravity(mOwner->GetParentSpace()->GetGravity());
	//}

	// Speed on the x axis
	float xSpeed = t->mLocal.mTranslationZ.x;

	// Speed on the y axis
	float ySpeed = t->mLocal.mTranslationZ.y;

	speedDir.x = 0; speedDir.y = 0;

	if (!stats->onCinematic)
	{
		// Player looking up checker
		if (PLAYER_GOING_UP && !stats->onCinematic)
		{
			stats->isUp = true;
		}
		else
			stats->isUp = false;

		// Horizontal speed control
		if (PLAYER_GOING_RIGHT && stats->isDashing == false)
		{
			// Remove landing bug
			stats->isLanding = false;
			stats->backToGround = false;

			// TEMPORARY OFFSET CHANGER OF COLLIDER
			mOwner->GetComp<BoxCollider>()->mOffset.x = 0.7f;

			stats->isRight = true;
			if (stats->canDuck && !stats->onCinematic)
			{
				if (PLAYER_LOCK_AIM && stats->isGrounded)
				{
					speedDir.x = 0; speedDir.y = 0;
					totalSpeed = 0;
					stats->isMoving = false;
				}
				else
				{
					if (aexInput->isGamepadActive() && LEFT_STICK_USED)
					{
						totalSpeed = hSpeed * LEFT_STICK_X_VALUE;
						speedDir = stats->walkDir * LEFT_STICK_X_VALUE;
					}
					else
					{
						speedDir = stats->walkDir;
						totalSpeed = hSpeed;
					}
					// Sprite controller
				}
				// SHOULD NOT MOVE
				if (!PLAYER_LOCK_AIM)
				{
					stats->isMoving = true;
				}
			}
		}
		else if (PLAYER_GOING_LEFT && stats->isDashing == false)
		{
			// Remove landing bug
			stats->isLanding = false;
			stats->backToGround = false;

			// TEMPORARY OFFSET CHANGER OF COLLIDER
			mOwner->GetComp<BoxCollider>()->mOffset.x = -0.7f;

			stats->isRight = false;
			if (stats->canDuck)
			{
				if (PLAYER_LOCK_AIM && stats->isGrounded)
				{
					speedDir.x = 0; speedDir.y = 0;
					totalSpeed = 0;
					stats->isMoving = false;
				}
				else
				{
					if (aexInput->isGamepadActive() && LEFT_STICK_USED)
					{
						totalSpeed = hSpeed * LEFT_STICK_X_VALUE;
						speedDir = stats->walkDir * LEFT_STICK_X_VALUE;
					}
					else
					{
						speedDir = -stats->walkDir;
						totalSpeed = -hSpeed;
					}
				}
				// SHOULD NOT MOVE
				if (!PLAYER_LOCK_AIM)
				{
					stats->isMoving = true;
				}
			}
		}
		else
		{
			speedDir.x = 0; speedDir.y = 0;
			totalSpeed = 0;
			stats->isMoving = false;
		}

		// SHOULD NOT MOVE
		if (PLAYER_LOCK_AIM)
		{
			stats->isMoving = false;
			stats->isStanding = true;
		}
		else
		{
			stats->isStanding = false;
		}
	}

	else
	{
		if (stats->doNotMoveOnCinematic)
		{
			speedDir.x = 0; speedDir.y = 0;
			totalSpeed = 0;
			stats->isMoving = false;
		}

		// Player has to go left
		else if (stats->goLeftOnCinematic)
		{
			speedDir = -stats->walkDir * 0.7f;
			totalSpeed = -hSpeed / 10.0f;
			stats->isRight = false;
			stats->isMoving = true;
		}

		// Player has to go right
		else
		{
			speedDir = stats->walkDir * 0.7f;
			totalSpeed = hSpeed / 10.0f;
			stats->isRight = true;
			stats->isMoving = true;
		}
	}

	////////////////////////////////////////////
	// PLAYER STOPPING COMPLETELY CONDITIONS: //
	////////////////////////////////////////////
	// Invincible
	if (stats->isInvincible)
	{
		stats->inviciTimer += (f32)aexTime->GetFrameTime();

		spineAnimation->mColor[3] = 0.6f + 0.4f * cos(9.81f * stats->inviciTimer);

		if (stats->inviciTimer >= stats->inviciCooldown)
		{
			stats->isInvincible = false;
			stats->inviciTimer = 0.f;
			spineAnimation->mColor[3] = 1.0f;
		}
	}

	if (stats->isHit)
	{
		spineAnimation->ChangeAnimation(0, "Damage", true, 1.f, 0.2f);
		stats->isInvincible = true;
		if (stats->inviciTimer >= stats->isHitCooldown)
		{
			stats->isHit = false;
		}
		return;
	}

	/////////////////////////////////
	// ! Horizontal speed update ! //
	/////////////////////////////////
	if (!unai)
		t->mLocal.mTranslationZ = AEVec3(xSpeed + stats->xDiff + speedDir.x * (f32)aexTime->GetFrameTime() + (stats->toMoveByFriction.x * (f32)aexTime->GetFrameTime()), ySpeed + speedDir.y * (f32)aexTime->GetFrameTime() + (stats->toMoveByFriction.y * (f32)aexTime->GetFrameTime()), t->mLocal.mTranslationZ.z);
	else							   
		t->mLocal.mTranslationZ = AEVec3(xSpeed + totalSpeed + (stats->toMoveByFriction.x * (f32)aexTime->GetFrameTime()), ySpeed + (stats->toMoveByFriction.y * (f32)aexTime->GetFrameTime()), t->mLocal.mTranslationZ.z);
	/////////////////////////////////
	/////////////////////////////////
	if (!stats->onCinematic)
	{
		// When done moving, set toMoveByFriction to 0 so that the player does not keep the momentum when jumping.
		stats->toMoveByFriction.x = 0.f;
		stats->toMoveByFriction.y = 0.f;

		// Player ducking checker - done after movement
		if (PLAYER_GOING_DOWN)
		{
			if (stats->isGrounded && stats->canDuck == true && stats->isDashing == false)
			{
				audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\player\\Crouch.wav"), Voice::SFX);
				speedDir.x = 0; speedDir.y = 0;
				totalSpeed = 0;
				Shrink();
			}
		}
		else if (stats->canDuck == false)
		{
			Grow();
		}

		// Double jump - order of jump and double jump matters
		if (PLAYER_JUMPING && stats->isGrounded == false && canDoubleJump && stats->isDashing == false)
		{
			audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\player\\Jump.wav"), Voice::SFX);
			rb->mVelocity = AEVec2(0.0f, vSpeed);
			canDoubleJump = false;
			spineAnimation->ClearAnimationAtPosition(0);
		}

		// Jump
		else if (PLAYER_JUMPING && stats->canDuck == true && stats->isDashing == false)
		{
			if (stats->isGrounded)
			{
				audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\player\\Jump.wav"), Voice::SFX);
				stats->gravityDir.x = 0; stats->gravityDir.y = 0;
				rb->mIgnoreGravity = false;
				rb->mVelocity = AEVec2(0.0f, vSpeed);
				canDoubleJump = true;
			}
		}
	}

	////////////////////////////////////////////////////////
	/////////////// ANIMATIONS CONTROLLER //////////////////
	////////////////////////////////////////////////////////
	// SPINE ANIMATION CONTROLLER
	if ((stats->isMoving == true && stats->isGrounded == true && stats->canDuck == true) || (stats->onCinematic && !stats->doNotMoveOnCinematic))
	{
		// PLAYER IS RUNNING
		spineAnimation->ChangeAnimation(0, "Run", true, 1.f, 0.2f);
		stats->isFloating = false;
	}
	else if (stats->isGrounded && stats->isDashing == false && stats->canDuck == true)
	{
		// If the player touches the ground, land
		if (stats->backToGround)
		{
			stats->isLanding = true;
			stats->isFloating = false;
		}

		// PLAYER LANDS
		if (stats->isLanding)
		{
			spineAnimation->ChangeAnimation(0, "Caida", false, 1.4f, 0.2f);

			// If it stops landing, play the idle animation
			if (spineAnimation->GetAnimState()->getCurrent(0)->isComplete())
			{
				stats->isLanding = false;
			}
		}
		else
		{
			// PLAYER IS IDLE
			if (!stats->isShooting)
			{
				spineAnimation->ChangeAnimation(0, "idle", true, 1.f, 0.2f);
			}
			// PLAYER IS STANDING AND SHOOTING
			else if (!stats->isDashing)
			{
				spineAnimation->ChangeAnimation(0, "Static", true, 1.f, 0.2f);
				spineAnimation->ChangeAnimation(1, "Aim", true, 1.f, 0.2f);
			}

		}

		stats->backToGround = false;
	}

	// PLAYER IS JUMPING & DOUBLE JUMPING
	if (stats->isGrounded == false && stats->isDashing == false)
	{
		if (!stats->isFloating)
		{
			spineAnimation->ChangeAnimation(0, "Jump_Inicial", false, 1.f, 0.2f);
		}


		// If the player is falling for too long...
		if (spineAnimation->GetAnimState()->getCurrent(0) && spineAnimation->GetAnimState()->getCurrent(0)->isComplete())
		{
			stats->isFloating = true;
		}
		// FALLING
		if (stats->isFloating == true)
		{
			spineAnimation->ChangeAnimation(0, "Falling loop", true, 0.8f, 0.2f);
		}

	}

	// PLAYER CROUCHING
	if (stats->canDuck == false && stats->isGrounded == true)
	{
		spineAnimation->ChangeAnimation(0, "Crouch", false, 1.f, 0.2f);
		stats->isLanding = false;
	}

	// PLAYER DASHING
	if (stats->isDashing == true)
	{
		spineAnimation->EmptyAnimationAtPosition(1);
		spineAnimation->EmptyAnimationAtPosition(2);
		spineAnimation->ChangeAnimation(0, "Dash", false, 2.f, 0.f);
	}
	// Completes dashing animation
	else
	{
		// If animation is also done
		if (spineAnimation->GetAnimState()->getCurrent(0)->getAnimation()->getName() == "Dash")
		{
			if (!stats->isGrounded)
				stats->isFloating = true;
		}
	}

	// PLAYER HAS BEEN HIT
	if (stats->isHit)
	{
		stats->isInvincible = true;
		if (stats->inviciTimer >= stats->isHitCooldown)
		{
			stats->isHit = false;
		}
	}
	static int counter = 0;

	// AIM
	if (PLAYER_IS_MOVING && !stats->onCinematic && !stats->isDashing)
	{
		if (stats->isGrounded && stats->canDuck)
		{
			spineAnimation->ChangeAnimation(1, "Aim", true, 1.f, 0.2f);
		}
		else
		{
			spineAnimation->ChangeAnimation(1, "Aim34", true, 1.f, 0.2f);
		}
	}
	else
	{
		if (!stats->isShooting)
			spineAnimation->ClearAnimationAtPosition(1);
	}

	// DIRECTION CONTROLLER
	if (stats->isRight == true)
	{
		spineAnimation->GetSkeleton()->setScaleX(myOrientation);
	}
	else
	{
		spineAnimation->GetSkeleton()->setScaleX(-myOrientation);
	}
	////////////////////////////////////////////////////////
	//////////////END OF ANIMATION CONTROLLER///////////////
	////////////////////////////////////////////////////////

	// So that grounded is updated
	stats->isGrounded = false;
	stats->isCollidingWithMovingPlayform = false;
}

bool PlayerController::OnGui()
{
	// isChanged boolean to return if something has changed
	bool isChanged = false;

	// Collapsing header - when clicked, opens up to show the rest of edited variables
	if (ImGui::CollapsingHeader("PlayerController"))
	{
		// Text for the jump height
		ImGui::Text("Jump Height");
		// Allow user to input jump height from the editor
		if (ImGui::InputFloat("Jump Height", &vSpeed))
		{
			isChanged = true;
		}

		// Horizontal speed declaration
		ImGui::Text("Horizontal Speed");
		if (ImGui::InputFloat("Horizontal Speed", &hSpeed))
		{
			isChanged = true;
		}
	}

	return isChanged;
}

void PlayerController::OnCollisionStarted(const CollisionStarted & collision)
{
	if (!stats->onVeins)
		return;

	if (collision.otherObject->GetComp<ICollider>()->collisionGroup == "movingFloor")
	{
		moveLeftAuto = true;
		bc->mIsGhost = true;
		moveYPosAuto = t->mLocal.mTranslationZ.y;
	}

	if (collision.otherObject->GetComp<ICollider>()->collisionGroup == "Acid")
		stats->lives = 0;
}

// FromJson specialized for this component
void PlayerController::FromJson(json & value)
{
	this->IComp::FromJson(value);

	value["Jump Height"] >> vSpeed;
	value["Running Speed"] >> hSpeed;
}

// ToJson specialized for this component
void PlayerController::ToJson(json & value)
{
	this->IComp::ToJson(value);

	value["Jump Height"] << vSpeed;
	value["Running Speed"] << hSpeed;
}
