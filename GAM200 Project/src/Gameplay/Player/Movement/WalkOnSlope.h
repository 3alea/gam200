#pragma once
#include "Components\Logic.h"
#include "Player\Stats\PlayerStats.h"

namespace AEX
{
	class Rigidbody;

	class WalkOnSlope : public LogicComp
	{
		AEX_RTTI_DECL(WalkOnSlope, LogicComp);

		public:
			WalkOnSlope();
			virtual void Initialize();
			virtual void Update();
			virtual void OnCollisionStarted(const CollisionStarted& collision);
			virtual void OnCollisionEnded(const CollisionEnded& collision);

		private:
			float* playerRot;
			Rigidbody* rb;

			float mRot;
			PlayerStats* mStats;

	};
}
