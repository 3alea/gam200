#pragma once
#include "Composition\AEXGameObject.h"
#include "Player/Stats/PlayerStats.h"
#include "Components\Logic.h"
#include "Imgui\imgui.h"

using namespace AEX;

class FootLogic : public LogicComp
{
	AEX_RTTI_DECL(FootLogic, LogicComp);
public:
	// Player
	GameObject* player;

	// RigidBody
	PlayerStats* stats;

	// Start is called before the first frame update
	virtual void Initialize();

	// Update is called once per frame
	virtual void Update();

	// OnCollision started
	virtual void OnCollisionStarted(const CollisionStarted & collision);

	// OnCollision event
	virtual void OnCollisionEvent(const CollisionEvent & collision);

	// OnGui() function
	virtual bool OnGui();

	// FromJson specialized for this component
	virtual void FromJson(json & value);
	// ToJson specialized for this component
	virtual void ToJson(json & value);
};