#include "Composition/AEXScene.h"
#include "Platform/AEXTime.h"
#include "FootLogic.h"
#include "Environment/PlatformController.h"
#include "Components/Rigidbody.h"
#include "Bullets\BulletLogic.h"
#include "Enemy\EnemyBullet.h"
#include <iostream>

using namespace AEX;
using namespace ImGui;

void FootLogic::Initialize()
{
	player = aexScene->FindObjectInANYSpace("player");
	if (player)
	{
		stats = player->GetComp<PlayerStats>();
	}
}

void FootLogic::Update()
{
	// If does not load correctly...
	if (!player)
	{
		Initialize();
	}
}

void FootLogic::OnCollisionStarted(const CollisionStarted & collision)
{
	// Touches the ground
	if (collision.otherObject->GetBaseNameString() != "player")
	{
		if(!stats->footColliding)
			stats->backToGround = true;
	}

	if (ICollider* c = collision.otherObject->GetComp<ICollider>())
	{
		if (c->collisionGroup == "movingFloor")
			stats->lives = 0;
	}
}

void FootLogic::OnCollisionEvent(const CollisionEvent & collision)
{
	if (collision.otherObject->GetBaseNameString() != "player"
		&& !collision.otherObject->GetComp<BulletLogic>() && !collision.otherObject->GetComp<EnemyBullet>()
		&& collision.otherObject->GetComp<ICollider>()->collisionGroup != "Activator")
	{
 		stats->footColliding = true;
		stats->isGrounded = true;

		if (stats->dashChecker)
			stats->canDash = true;
	}

	Rigidbody* ting;
	
	if (collision.otherObject->GetComp<PlatformController>())
	{
		ting = collision.otherObject->GetComp<Rigidbody>();
		stats->isCollidingWithMovingPlayform = true;
		stats->toMoveByFriction.x = ting->mVelocity.x;
		stats->toMoveByFriction.y = ting->mVelocity.y;
	}
}

bool FootLogic::OnGui()
{
	return false;
}

// FromJson specialized for this component
void FootLogic::FromJson(json & value)
{
	this->LogicComp::FromJson(value);
}

// ToJson specialized for this component
void FootLogic::ToJson(json & value)
{
	this->LogicComp::ToJson(value);
}