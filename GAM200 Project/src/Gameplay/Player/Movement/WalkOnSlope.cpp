#include "WalkOnSlope.h"
#include "Environment/VeinsPlatforms/VeinsPlatforms.h"
#include "Composition\AEXGameObject.h"
#include "Components\AEXTransformComp.h"
#include "Components\Collider.h"
#include "Composition/AEXSpace.h"
#include "Player/Movement/PlayerController.h"
#include "Platform/AEXInput.h"
#include "Environment/Elevator/Elevator.h"
namespace AEX
{
	WalkOnSlope::WalkOnSlope()
		: playerRot(NULL)
		, mStats(NULL)
	{}

	// Get the rigid body, orientation and stats of the player
	void WalkOnSlope::Initialize()
	{
		// Get a pointer to the player
		GameObject* player = mOwner->GetParentSpace()->FindObject("player");

		// Get the transform component
		TransformComp* t = player->GetComp<TransformComp>();

		// Rigid body of the player
		rb = player->GetComp<Rigidbody>();

		if (rb)
			rb->mIgnoreGravity = false;

		if (t)
			playerRot = &t->mLocal.mOrientation;

		mStats = player->GetComp<PlayerStats>();

		if (mStats)
		{
			mStats->floorsOn = 0;
			mStats->gravityDir = AEVec2(0,0);
		}
	}

	// If the player is on the floor, make sure it sticks to it
	void WalkOnSlope::Update()
	{
		// If not initialized
		if (!playerRot || !mStats || !rb)
		{
			Initialize();
			return;
		}

		if (PLAYER_JUMPING && !mStats->onCinematic)
			mStats->gravityDir = AEVec2(0, 0);

		// Stick the player to the ground
		rb->AddForce(mStats->gravityDir);

		//std::cout << "GRAVITY : " << rb->mIgnoreGravity << std::endl;
	}

	// Get the walking vector of the player and the direction of the gravity perpendicular to the floor
	void WalkOnSlope::OnCollisionStarted(const CollisionStarted & collision)
	{
		// The player is on the floor
		if (collision.otherObject->GetComp<ICollider>()->collisionGroup == "Floor")
		{
			// Avoid sliding when the floor is rotated
			rb->mIgnoreGravity = true;

			rb->mAcceleration = AEVec2(0,0);
			rb->mVelocity = AEVec2(0, 0);

			mStats->floorsOn++;

			// Get the rotation of the floor
			mRot = collision.otherObject->GetComp<TransformComp>()->mLocal.mOrientation;

			// Get the walking vector
			mStats->walkDir = AEVec2(cos(mRot), sin(mRot)) * 8;

			float perp_rot;

			perp_rot = mRot - PI / 2;

			

			if (collision.otherObject->GetComp<VeinsPlatforms>())
			{
				// Gravity to push the player on the floor
				mStats->gravityDir = AEVec2(cos(perp_rot), sin(perp_rot)) * 100;
			}
			
			else
			{
				// Gravity to push the player on the floor
				mStats->gravityDir = AEVec2(cos(perp_rot), sin(perp_rot)) * 10;
			}

			if (Elevator* e = collision.otherObject->GetComp<Elevator>())
			{
				if (!e->done && e->active && e->doneWaiting && !e->up)
					mStats->gravityDir = AEVec2(0, -1) * 500;
			}


			// Set the rotaion of the player
			*playerRot = mRot;
		}
	}

	void WalkOnSlope::OnCollisionEnded(const CollisionEnded & collision)
	{
		if (collision.otherObject == nullptr)
			return;

		ICollider* c = collision.otherObject->GetComp<ICollider>();
		// The player is on the floor
		if (c && c->collisionGroup == "Floor")
		{
			mStats->floorsOn--;
			if (mStats->floorsOn == 0)
			{
				mStats->gravityDir = AEVec2(0, 0);
				rb->mIgnoreGravity = false;
			}
		}
	}
}