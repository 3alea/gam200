#include "EnemyBullet.h"
#include "Player/Stats/PlayerStats.h"
#include "src/Engine/Editor/Editor.h"
#include "src/Engine/Platform/AEXTime.h"
#include "Resources\AEXResourceManager.h"
#include "Player\Movement\PlayerController.h"
#include "src/Gameplay/Enemy/EnemyBugger.h"
#include "src\Gameplay\Enemy\EnemyGrounded.h"
#include "src\Gameplay\Bullets\BulletLogic.h"
#include "Activator/EnemyActivator.h"
#include "Environment/Checkpoint.h"
#include "CameraLogic/CameraChanger.h"
#include "Follow\ChaseObject.h"
namespace AEX
{

	EnemyBullet::EnemyBullet(TransformComp  * transform, const unsigned index, f32 BulletSpeed, const char * enemyname, TransformComp * PlayerTransform_, int what_bugger)
	{
		audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\Enemies\\Bullets\\EnemyShot.wav"), Voice::SFX)->SetVolume(0.3f);
		
		std::string s = "bullet";
		name = std::to_string(index) + s.c_str();

		GameObject * enemybullet = aexScene->FindSpace("Main")->CreateGameObject(name.c_str());

		TransformComp * currentenemybullet;

		enemybullet->AddComp(new TransformComp());
		currentenemybullet = enemybullet->GetComp<TransformComp>();
		currentenemybullet->mLocal.mTranslationZ = transform->mLocal.mTranslationZ - AEVec3(0.f, 0.f, 0.1f);
		currentenemybullet->mWorld.mTranslationZ = transform->mWorld.mTranslationZ;
		currentenemybullet->mLocal.mOrientation = transform->mLocal.mOrientation;

		if (enemyname == "grounded")
		{
			AEVec2 BulletInitialsDirection = AEVec2(PlayerTransform_->mLocal.mTranslationZ.x, PlayerTransform_->mLocal.mTranslationZ.y) -
		    AEVec2(currentenemybullet->mLocal.mTranslationZ.x, currentenemybullet->mLocal.mTranslationZ.y);
			
			currentenemybullet->mLocal.mTranslationZ.y = transform->mLocal.mTranslationZ.y - 1;
			currentenemybullet->mWorld.mTranslationZ.y = transform->mWorld.mTranslationZ.y - 1;

			if (BulletInitialsDirection.x < 0)
			{
				currentenemybullet->mLocal.mTranslationZ.x = currentenemybullet->mLocal.mTranslationZ.x - 1;
				currentenemybullet->mWorld.mTranslationZ.x = currentenemybullet->mWorld.mTranslationZ.x - 1;
			}
			else
			{
				currentenemybullet->mLocal.mTranslationZ.x = currentenemybullet->mLocal.mTranslationZ.x + 1;
				currentenemybullet->mWorld.mTranslationZ.x = currentenemybullet->mWorld.mTranslationZ.x + 1;
			}
		}

		if (enemyname == "mitosis" || enemyname == "flying")
		{
			currentenemybullet->mLocal.mScale = transform->mLocal.mScale;
			currentenemybullet->mWorld.mScale = transform->mWorld.mScale;
		}
		else if (enemyname == "bugger")
		{
			currentenemybullet->mLocal.mScale = transform->mLocal.mScale/2;
			currentenemybullet->mWorld.mScale = transform->mWorld.mScale/2;
			currentenemybullet->mLocal.mTranslationZ.z = currentenemybullet->mLocal.mTranslationZ.z + 1;
			currentenemybullet->mWorld.mTranslationZ.z = currentenemybullet->mWorld.mTranslationZ.z + 1;
		}
		else if (enemyname == "boss")
		{
			currentenemybullet->mLocal.mScale = AEVec2(1.f, 1.f);
			currentenemybullet->mWorld.mScale = AEVec2(1.f, 1.f);
		}
		else
		{
			currentenemybullet->mLocal.mScale = AEVec2(1.f, 1.f);
			currentenemybullet->mWorld.mScale = AEVec2(1.f, 1.f);

		}

		enemybullet->AddComp(new SpriteComp());
		enemybullet->AddComp(new BoxCollider());
		enemybullet->AddComp(new Rigidbody());
		enemybullet->AddComp(new BulletComp(enemyname));

		enemybullet->GetComp<BoxCollider>()->mScale = enemybullet->GetComp<TransformComp>()->mLocal.mScale;
		enemybullet->GetComp<BoxCollider>()->mIsGhost = true;
		enemybullet->GetComp<BoxCollider>()->collisionGroup = "EnemyBullet";
		enemybullet->GetComp<SpriteComp>()->mTex = BulletSprite;
		
		enemybullet->GetComp<BulletComp>()->EnemySpeed = BulletSpeed;
		enemybullet->GetComp<BulletComp>()->what_bugger = what_bugger;
		enemybullet->GetComp<Rigidbody>()->mIgnoreGravity = true;
	}

	void EnemyBullet::FromJson(json & value)
	{
		this->GameObject::FromJson(value);
		
	}
	void EnemyBullet::ToJson(json & value)
	{
		this->GameObject::ToJson(value);
	}

	void BulletComp::OnCollisionEvent(const CollisionEvent & collision)
	{
		if (collision.otherObject->GetBaseNameString() == "player")
		{
			//std::cout << "COLLISION Playwe: " << std::endl;
			PlayerStats* stats = collision.otherObject->GetComp<PlayerStats>();
			if (!stats->isInvincible && !stats->isHit)
			{
				stats->lives--;
				stats->isHit = true;
			}

			mOwner->GetParentSpace()->DestroyObject(mOwner);
		}
		else if (collision.otherObject->GetBaseNameString() != "player"
			&& collision.otherObject->GetBaseNameString() != "foot"
			&& !collision.otherObject->GetComp<BulletLogic>()
			&& !collision.otherObject->GetComp<EnemyActivator>()
			&& !collision.otherObject->GetComp<Checkpoint>()
			&& !collision.otherObject->GetComp<CameraChanger>()
			&& collision.otherObject->GetComp<ICollider>()->collisionGroup != "EnemyBullet"
			&& collision.otherObject->GetComp<ICollider>()->collisionGroup != "Enemy"
			&& collision.otherObject->GetComp<ICollider>()->collisionGroup != "Activator"
			&& collision.otherObject->GetComp<ICollider>()->collisionGroup != "bugger"
			&& (!collision.otherObject->GetComp<ChaseObject>() || (collision.otherObject->GetComp<ChaseObject>() && collision.otherObject->GetComp<ChaseObject>()->mLives > 0)))
		{
			mOwner->GetParentSpace()->DestroyObject(mOwner);
		}
	}

	BulletComp::BulletComp(std::string enemyname) : Enemyname(enemyname)
	{

	}
	void BulletComp::Initialize()
	{
		BulletsStartTimeToDestroy = 4.0f;
		BulletsTimeToDestroy = BulletsStartTimeToDestroy;
		Initialized = false;
	}

	void BulletComp::Update()
	{
		this->Enemy::Update();

		if (!Initialized)
		{
			if (Enemyname == "grounded")
			{
				BulletDir = AEVec2(PlayerTransform->mLocal.mTranslationZ.x, PlayerTransform->mLocal.mTranslationZ.y) -
					AEVec2(mOwner->GetComp<TransformComp>()->mLocal.mTranslationZ.x, mOwner->GetComp<TransformComp>()->mLocal.mTranslationZ.y);

				if (BulletDir.x < 0)
				{
					mOwner->GetComp<SpriteComp>()->flipX = true;
				}

			}

			if (Enemyname != "grounded" && Enemyname != "boss")
			{
				BulletDir = AEVec2(PlayerTransform->mLocal.mTranslationZ.x, PlayerTransform->mLocal.mTranslationZ.y) -
					AEVec2(mOwner->GetComp<TransformComp>()->mLocal.mTranslationZ.x, mOwner->GetComp<TransformComp>()->mLocal.mTranslationZ.y);
				if (Enemyname == "bugger")
				{
				
				if (what_bugger == 2)
				{
					BulletDir.x = BulletDir.x * cos(15.0f * 3.14f / 180.0f) + BulletDir.y * -sin(15.0f  * 3.14f / 180.0f);
					BulletDir.y = BulletDir.x * sin(15.0f  * 3.14f / 180.0f) + BulletDir.y * cos(15.0f  * 3.14f / 180.0f);
				}
				else if (what_bugger == 3)
				{
					BulletDir.x = BulletDir.x * cos(-15.0f  * 3.14f / 180.0f) + BulletDir.y * -sin(-15.0f  * 3.14f / 180.0f);
					BulletDir.y = BulletDir.x * sin(-15.0f  * 3.14f / 180.0f) + BulletDir.y * cos(-15.0f * 3.14f / 180.0f);
				}
				}
				

				f32 angle = acos(-BulletDir.x / -BulletDir.Length());

				if (BulletDir.y > 0)
					mOwner->GetComp<TransformComp>()->mLocal.mOrientation = angle;

				else
					mOwner->GetComp<TransformComp>()->mLocal.mOrientation = -angle;

			}
			if (Enemyname == "boss")
			{
				if (what_bugger == 1)
				{
					BulletDir.x = 0.f;
					BulletDir.y = -1.f;
				}
				else if (what_bugger == 2)
				{
					BulletDir = AEVec2(1, 0);
					BulletDir.x = BulletDir.x * cos(-60.0f * 3.14f / 180.0f) + BulletDir.y * -sin(-60.0f  * 3.14f / 180.0f);
					BulletDir.y = BulletDir.x * sin(-60.0f  * 3.14f / 180.0f) + BulletDir.y * cos(-60.0f  * 3.14f / 180.0f);
				}
				else if (what_bugger == 3)
				{
					BulletDir = AEVec2(-1, 0);
					BulletDir.x = BulletDir.x * cos(60.0f  * 3.14f / 180.0f) + BulletDir.y * -sin(60.0f  * 3.14f / 180.0f);
					BulletDir.y = BulletDir.x * sin(60.0f  * 3.14f / 180.0f) + BulletDir.y * cos(60.0f * 3.14f / 180.0f);
				}
				else if (what_bugger == 4)
				{
					BulletDir = AEVec2(0, -1);
					BulletDir.x = BulletDir.x * cos(-20.f * 3.14f / 180.0f) + BulletDir.y * -sin(-20.f * 3.14f / 180.0f);
					BulletDir.y = BulletDir.x * sin(-20.f * 3.14f / 180.0f) + BulletDir.y * cos(-20.f * 3.14f / 180.0f);
				}
				else if (what_bugger == 5)
				{
					BulletDir = AEVec2(0, -1);
					BulletDir.x = BulletDir.x * cos(20.f  * 3.14f / 180.0f) + BulletDir.y * -sin(20.f * 3.14f / 180.0f);
					BulletDir.y = BulletDir.x * sin(20.f * 3.14f / 180.0f) + BulletDir.y * cos(20.f * 3.14f / 180.0f);
				}
				f32 angle = acos(-BulletDir.x / -BulletDir.Length());

				if (BulletDir.y > 0)
					mOwner->GetComp<TransformComp>()->mLocal.mOrientation = angle;

				else
					mOwner->GetComp<TransformComp>()->mLocal.mOrientation = -angle;
			}
			Initialized = true;
		}

		
		if (Enemyname == "grounded")
		{
			mOwner->GetComp<TransformComp>()->mLocal.mTranslationZ.x += EnemySpeed * (BulletDir.Normalize().x < 0.f ? -1.f : 1.f);
		}

		else
		{
			mOwner->GetComp<TransformComp>()->mLocal.mTranslationZ += BulletDir.Normalize() * EnemySpeed;
		}

		if (BulletsTimeToDestroy <= 0)
		{
			mOwner->GetParentSpace()->DestroyObject(mOwner);
			BulletsTimeToDestroy = BulletsStartTimeToDestroy;
		}

		else
			BulletsTimeToDestroy -= (f32)aexTime->GetFrameTime();

	}

	void BulletComp::FromJson(json & value)
	{
		this->Enemy::FromJson(value);

	}
	void BulletComp::ToJson(json & value)
	{
		this->Enemy::ToJson(value);
	}
}