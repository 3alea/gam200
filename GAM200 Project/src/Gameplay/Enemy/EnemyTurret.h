#pragma once

#include "Enemy.h"
#include "EnemyBullet.h"
#include <string>

namespace AEX
{
	class EnemyTurret;

	struct SM_EnemyTurret_Resting : public State
	{
		SM_EnemyTurret_Resting(const char * name);

		void LogicUpdate();

		PathComponent * mPath;

		f32 time;

		bool Initialized;

		AEVec2 pos;

		EnemyTurret * save;
	};

	struct SM_EnemyTurret_Moving : public State
	{
		SM_EnemyTurret_Moving(const char * name);

		void LogicEnter();
		void LogicUpdate();

		EnemyTurret * save;
	};

	struct SM_EnemyTurret_Aiming : public State
	{
		SM_EnemyTurret_Aiming(const char * name);

		void LogicEnter();
		void LogicUpdate();

		EnemyTurret * save;
	};

	struct SM_EnemyTurret_Shooting : public State
	{
		SM_EnemyTurret_Shooting(const char * name);

		void LogicUpdate();

		unsigned i;

		bool FirstShot = true;

		EnemyTurret * save;
	};

	struct SM_EnemyTurret_Recharging : public State
	{
		SM_EnemyTurret_Recharging(const char * name);

		void LogicEnter();
		void LogicUpdate();

		unsigned i;

		bool Initialized = false;

		EnemyTurret * save;
		f64 time;
	};

	struct SM_EnemyTurret_Die : public State
	{
		SM_EnemyTurret_Die(const char * name);

		void LogicEnter();

		void LogicUpdate();

		EnemyTurret * save;

	private:
		f32 deathTimer = 0.0f;
		f32 deathTime = 1.0f;
	};

	class EnemyTurret : public Enemy, public Actor
	{
		AEX_RTTI_DECL(EnemyTurret, Enemy)

	public:

		void Initialize();
		void Update();

		bool OnGui();

		void SM_Init();

		virtual void OnCollisionEvent(const CollisionEvent& collision);

		void FromJson(json & value);
		void ToJson(json & value);

		f64 StartTimeBetweenShots;

		f64 TimeBetweenShots;

		int ShotCountToRecharge;
		int SaveShotCountToRecharge;
		float aimDistance;
		float reachDistance;
		// Starting position on the x to return to
		float startingX;
		float runSpeed;

		float myOrientation = 0.f;

		f64 EnemyTimeToRecharge;
		f64 SaveEnemyTimeToRecharge;
	};
}