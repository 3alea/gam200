#include "Components\SpineAnimation.h"
#include <spine\AnimationState.h>
#include <spine\Skeleton.h>
#include <spine\Bone.h>
#include "EnemyKamikaze.h"
#include "Player/Stats/PlayerStats.h"
#include "src/Engine/Editor/Editor.h"
#include "AI/Path2D.h"
#include "Enemy/WaveSystem/Spawner.h"

namespace AEX
{
	SM_KamikazeEnemy_Resting::SM_KamikazeEnemy_Resting(const char * name) : State(name)
	{
		
	}

	void SM_KamikazeEnemy_Resting::LogicUpdate()
	{
	}

	SM_KamikazeEnemy_Moving::SM_KamikazeEnemy_Moving(const char * name) : State(name)
	{
		Initialized = false;
	}

	void SM_KamikazeEnemy_Moving::LogicEnter()
	{

	}

	void SM_KamikazeEnemy_Moving::LogicUpdate()
	{
		if(!Initialized)
		{ 
			save = static_cast<EnemyKamikaze*>(mActor);
			mPath = save->mOwner->GetComp<PathComponent>();
			time = 0.0f;
			Initialized = true;
		}

		if (save->enemy_activator)
		{
			save->spineAnimation->ChangeAnimation(0, "Aim", true);
			save->spineAnimation->ChangeAnimation(1, "Move", true, 1.f, 0.5f);

			time += (f32)aexTime->GetFrameTime();
			if (mPath == NULL)
				CreateDummyPath();

			f32 tn = (time / mPath->Duration()) * (time / mPath->Duration()); // ease in
			AEVec2 & pos = mPath->SampleParameter(tn);
			save->EnemyTransform->SetPosition3D(AEVec3(pos.x,pos.y, save->EnemyTransform->mLocal.mTranslationZ.z));
			
			if (time >= mPath->Duration())
			{
				time = 0.0f;
			}

			//f32 distance = fabs((save->PlayerTransform->mLocal.mTranslationZ - save->EnemyTransform->mLocal.mTranslationZ).Length());
			if (save->Shoot_activator)
			{
				mOwnerStateMachine->ChangeState("SM_KamikazeEnemy_Aim");
			}
		}
	}

	void SM_KamikazeEnemy_Moving::CreateDummyPath()
	{
		AEVec3 pos = save->mOwner->GetComp<TransformComp>()->mWorld.mTranslationZ;
		mPath = new PathComponent;
		mPath->Push(0.0f, AEVec2(pos.x, pos.y));
		AEVec2 initpos = mPath->LastPoint();
		mPath->Push(1.5f, AEVec2(pos.x - 4.f, pos.y));
		mPath->Push(2.5f, AEVec2(initpos.x - 1.f, initpos.y - 1.5f));
		mPath->Push(3.2f, initpos);
	}

	SM_Kamikaze_Aim::SM_Kamikaze_Aim(const char * name) : State(name)
	{
		save = static_cast<EnemyKamikaze*>(mActor);
		if(save)
			maxTimePassed = save->maxTimePassed;
	}

	void SM_Kamikaze_Aim::LogicUpdate()
	{
		if (!save)
		{
			save = static_cast<EnemyKamikaze*>(mActor);
			maxTimePassed = save->maxTimePassed;
		}

		if (save->mLives <= 0)
		{
			mOwnerStateMachine->ChangeState("SM_Kamikaze_Die");
		}

		// Pass time
		timePassed += (f32)aexTime->GetFrameTime();

		// Change animation to aim animation
		save->spineAnimation->ChangeAnimation(0, "Aim", true);
		save->spineAnimation->ChangeAnimation(1, "Target", true, 1.f, 0.5f);

		// Get the direction
		AEVec2 Direction = AEVec2(save->PlayerTransform->mLocal.mTranslationZ.x, save->PlayerTransform->mLocal.mTranslationZ.y) -
			AEVec2(save->EnemyTransform->mLocal.mTranslationZ.x, save->EnemyTransform->mLocal.mTranslationZ.y);

		save->EnemyTransform->mLocal.mOrientation = Direction.GetAngle();

		if (timePassed >= maxTimePassed)
		{
			timePassed = 0.f;
			save->doneOnce = true;
			mOwnerStateMachine->ChangeState("SM_Kamikaze_MoveTowardsPlayer");
		}
	}

	SM_Kamikaze_MoveTowardsPlayer::SM_Kamikaze_MoveTowardsPlayer(const char * name) : State(name)
	{
		rotated = false;
		save = static_cast<EnemyKamikaze*>(mActor);
		if (save)
			save->rb->mVelocity = AEVec2(-cos(save->EnemyTransform->mLocal.mOrientation) * save->EnemySpeed, -sin(save->EnemyTransform->mLocal.mOrientation) * save->EnemySpeed);
	}

	void SM_Kamikaze_MoveTowardsPlayer::LogicUpdate()
	{
		if (!save)
		{
			save = static_cast<EnemyKamikaze*>(mActor);
			save->rb->mVelocity = AEVec2(cos(save->EnemyTransform->mLocal.mOrientation) * save->EnemySpeed, sin(save->EnemyTransform->mLocal.mOrientation) * save->EnemySpeed);
		}

		if (save->mLives <= 0)
		{
			mOwnerStateMachine->ChangeState("SM_Kamikaze_Die");
		}

		if (save->doneOnce)
		{
			save->rb->mVelocity = AEVec2(cos(save->EnemyTransform->mLocal.mOrientation) * save->EnemySpeed, sin(save->EnemyTransform->mLocal.mOrientation) * save->EnemySpeed);
			save->doneOnce = false;
		}

		// Get the direction
		AEVec2 Direction = AEVec2(save->PlayerTransform->mLocal.mTranslationZ.x, save->PlayerTransform->mLocal.mTranslationZ.y) -
			AEVec2(save->EnemyTransform->mLocal.mTranslationZ.x, save->EnemyTransform->mLocal.mTranslationZ.y);

		save->EnemyTransform->mLocal.mOrientation = Direction.GetAngle();

		save->spineAnimation->ChangeAnimation(0, "Aim", true);
		save->spineAnimation->ChangeAnimation(1, "Attack", false, save->animationSpeed, 0.f);

		save->InvencibleEnemy = false;

		// Remove the speed so that there is a bit of lag

		if (abs(save->rb->mVelocity.x) >= 0.f)
		{
			bool x = save->rb->mVelocity.x < 0.f ? false : true;
			if (x)
				save->rb->mVelocity.x -= 0.1f;
			else
				save->rb->mVelocity.x += 0.1f;
		}

		if (abs(save->rb->mVelocity.y) >= 0.f)
		{
			bool y = save->rb->mVelocity.y < 0.f ? false : true;
			if (y)
				save->rb->mVelocity.y -= 0.1f;
			else
				save->rb->mVelocity.y += 0.1f;
		}

		if (abs(save->rb->mVelocity.x) <= 0.1f && abs(save->rb->mVelocity.y) <= 0.1f)
		{
			mOwnerStateMachine->ChangeState("SM_KamikazeEnemy_Aim");
		}
	}

	SM_Kamikaze_Die::SM_Kamikaze_Die(const char * name) : State(name)
	{
		
	}

	void SM_Kamikaze_Die::LogicUpdate()
	{
		EnemyKamikaze* save = static_cast<EnemyKamikaze*>(mActor);

		save->GetOwner()->GetParentSpace()->DestroyObject(save->GetOwner());
		save->spineAnimation->ChangeAnimation(1, "Death", false);
		if(save->spineAnimation->GetAnimState()->getCurrent(1)->isComplete())
			save->GetOwner()->GetParentSpace()->DestroyObject(save->GetOwner());
	}

	void EnemyKamikaze::SM_Init()
	{
		mBrain[0].AddState(new SM_KamikazeEnemy_Resting("SM_KamikazeEnemy_Resting"));
		mBrain[0].AddState(new SM_KamikazeEnemy_Moving("SM_KamikazeEnemy_Moving"));
		mBrain[0].AddState(new SM_Kamikaze_Aim("SM_KamikazeEnemy_Aim"));
		mBrain[0].AddState(new SM_Kamikaze_MoveTowardsPlayer("SM_Kamikaze_MoveTowardsPlayer"));
		mBrain[0].AddState(new SM_Kamikaze_Die("SM_Kamikaze_Die"));

		mBrain[0].SetInitState("SM_KamikazeEnemy_Moving");
		mBrain[0].ChangeState(mBrain[0].mInitialState);
		mBrain[0].mCurrentState = mBrain[0].mInitialState;
	}

	void EnemyKamikaze::Initialize()
	{
		this->Enemy::Initialize();

		SM_Init();

		spineAnimation = mOwner->GetComp<SpineAnimationComp>();
		rb = mOwner->GetComp<Rigidbody>();

		Activate = false;
	}
	void EnemyKamikaze::Update()
	{
		if (!spineAnimation)
		{
			spineAnimation = mOwner->GetComp<SpineAnimationComp>();
		}
		this->Enemy::Update();
		Actor::Update();
	}
	void EnemyKamikaze::ShutDown()
	{
	}

	bool EnemyKamikaze::OnGui()
	{
		bool isChanged = false;

		if (ImGui::InputInt("Lives", &mLives))
		{
			isChanged = true;
		}
		
		if (ImGui::InputFloat("Aim time", &maxTimePassed))
		{
			isChanged = true;
		}
		
		//if (ImGui::InputFloat("Distance not attack player", &DistanceEnemyNotAttack))
		//{
		//	isChanged = true;
		//}

		if (ImGui::InputFloat("Speed", &EnemySpeed))
		{
			isChanged = true;
		}

		if (ImGui::InputFloat("Animation Speed", &animationSpeed))
		{
			isChanged = true;
		}

		//ImGui::InputFloat("Dist to activate", &Radius_to_activate);
		//ImGui::InputFloat("Dist to attack", &Radius_to_attack);
		//ImGui::InputFloat("Distance STOP attack player", &Radius_to_STOP_attacking);
		ImGui::InputDouble("Time First Shot", &TimeFirstShot);
		return isChanged;
	}

	void EnemyKamikaze::OnCollisionEvent(const CollisionEvent & collision)
	{
		if (enemy_activator)
		{
			if (collision.otherObject->GetComp<ICollider>()->collisionGroup  == "Floor")
			{
				mLives = 0;

				if (mLives <= 0)
				{
					spineAnimation->ChangeAnimation(1, "Death", false);
					if (spineAnimation->GetAnimState()->getCurrent(1)->isComplete())
					{
						// Added by Miguel for wave system
						if (mOwner->mSpawnerOwner != nullptr)
							mOwner->mSpawnerOwner->EnemyKilled();

						mOwner->GetParentSpace()->DestroyObject(mOwner);
					}
				}
			}
			if (collision.otherObject->GetBaseNameString() == "player")
			{
				PlayerStats* stats = collision.otherObject->GetComp<PlayerStats>();

				if (!stats->isInvincible)
				{
					stats->lives--;
					stats->isHit = true;
				}
				mLives = 0;

				if (mLives <= 0)
				{
					spineAnimation->ChangeAnimation(1, "Death", false);
					if (spineAnimation->GetAnimState()->getCurrent(1)->isComplete())
					{
						// Added by Miguel for wave system
						if (mOwner->mSpawnerOwner != nullptr)
							mOwner->mSpawnerOwner->EnemyKilled();

						mOwner->GetParentSpace()->DestroyObject(mOwner);
					}
				}
			}

			return;
		}
	}

	void EnemyKamikaze::FromJson(json & value)
	{
		this->Enemy::FromJson(value);

		value["Aim time"] >> maxTimePassed;
		value["Animation speed"] >> animationSpeed;

		if (value["Enemy Activated"] != nullptr)
			Activate = value["Enemy Activated"].get<bool>();
		
	}
	void EnemyKamikaze::ToJson(json & value)
	{
		this->Enemy::ToJson(value);

		value["Aim time"] << maxTimePassed;
		value["Animation speed"] << animationSpeed;

		value["Enemy Activated"] = Activate;
	}

}