#include "EnemyTurret.h"
#include "Player/Stats/PlayerStats.h"
#include "Components\SpineAnimation.h"
#include "src/Engine/Editor/Editor.h"
#include "Enemy/WaveSystem/Spawner.h"
#include <spine\AnimationState.h>
#include <spine\Skeleton.h>

namespace AEX
{
	SM_EnemyTurret_Resting::SM_EnemyTurret_Resting(const char * name) : State(name)
	{
		Initialized = false;
	}

	void SM_EnemyTurret_Resting::LogicUpdate()
	{
		if (!Initialized)
		{
			save = static_cast<EnemyTurret*>(mActor);
			save->InvencibleEnemy = true;
			time = 0.0f;
			Initialized = true;
		}

		time += (f32)aexTime->GetFrameTime();

		if (save->Shoot_activator)
		{
			save->startingX = save->EnemyTransform->mLocal.mTranslationZ.x;
			mOwnerStateMachine->ChangeState("SM_EnemyTurret_Moving");
		}
	}

	SM_EnemyTurret_Moving::SM_EnemyTurret_Moving(const char * name) : State(name)
	{

	}

	void SM_EnemyTurret_Moving::LogicEnter()
	{
		save = static_cast<EnemyTurret*>(mActor);
	}
	void SM_EnemyTurret_Moving::LogicUpdate()
	{
		SpineAnimationComp* spine = save->mOwner->GetComp<SpineAnimationComp>();
		// Update the animation and invincibility
		save->InvencibleEnemy = false;
		spine->ChangeAnimation(0, "Walk", true, 1.f, 0.2f);

		// Flip if the player is in the other direction!
		float DirectionEnemy = save->PlayerTransform->mLocal.mTranslationZ.x - save->EnemyTransform->mLocal.mTranslationZ.x;

		if (DirectionEnemy < 0)
			spine->GetSkeleton()->setScaleX(save->myOrientation);
		else
			spine->GetSkeleton()->setScaleX(-save->myOrientation);

		// Get the player to enemy position distance
		float distance = abs(DirectionEnemy);

		// If the distance is too high, go back to starting position
		if (distance > save->aimDistance)
		{
			// Choose the correct direction!
			float step = save->EnemyTransform->mLocal.mTranslationZ.x - save->startingX > 0.f ? -save->EnemySpeed : save->EnemySpeed;
			save->EnemyTransform->mLocal.mTranslationZ.x += step;
		}
		// If the distance is high but the enemy can still see the player, go towards the player
		else if (distance > save->reachDistance)
		{
			// Choose the correct direction!
			float step = save->PlayerTransform->mLocal.mTranslationZ.x - save->EnemyTransform->mLocal.mTranslationZ.x < 0.f ? -save->runSpeed : save->runSpeed;
			save->EnemyTransform->mLocal.mTranslationZ.x += step;
		}
		// If the enemy is very close, just keep on shooting
		else
		{
			mOwnerStateMachine->ChangeState("SM_EnemyTurret_Aiming");
		}
	}

	SM_EnemyTurret_Aiming::SM_EnemyTurret_Aiming(const char * name) : State(name)
	{

	}

	void SM_EnemyTurret_Aiming::LogicEnter()
	{
		save = static_cast<EnemyTurret*>(mActor);
	}
	void SM_EnemyTurret_Aiming::LogicUpdate()
	{
		// If lives reach 0, die
		if (save->mLives <= 0)
		{
			mOwnerStateMachine->ChangeState("SM_EnemyTurret_Die");
		}

		save->mOwner->GetComp<SpineAnimationComp>()->ChangeAnimation(0, "Attack_Shoot", false, 1.f);
		if (save->mOwner->GetComp<SpineAnimationComp>()->GetAnimState()->getCurrent(0)->isComplete())
		{
			mOwnerStateMachine->ChangeState("SM_EnemyTurret_Shooting");
		}
	}

	SM_EnemyTurret_Shooting::SM_EnemyTurret_Shooting(const char * name) : State(name)
	{
		i = 0;
	}

	void SM_EnemyTurret_Shooting::LogicUpdate()
	{
		save = static_cast<EnemyTurret*>(mActor);

		// If lives reach 0, die
		if (save->mLives <= 0)
		{
			mOwnerStateMachine->ChangeState("SM_EnemyTurret_Die");
		}

		save->InvencibleEnemy = false;
		save->mOwner->GetComp<SpineAnimationComp>()->ChangeAnimation(0, "Shooting_Loop", true, 1.f / (2.f * (f32)save->StartTimeBetweenShots));
		float distance = fabs((save->PlayerTransform->mLocal.mTranslationZ - save->EnemyTransform->mLocal.mTranslationZ).Length());

		if (FirstShot)
		{
			save->EnemyTimeToRecharge = save->SaveEnemyTimeToRecharge;
			save->ShotCountToRecharge = save->SaveShotCountToRecharge;
			FirstShot = false;
		}
		//Shoot
		if (save->ShotCountToRecharge > 0)
		{
			if (save->TimeBetweenShots <= 0)
			{
				EnemyBullet(save->EnemyTransform, i, save->EnemyBulletSpeed, "grounded", save->PlayerTransform);

				i++;

				save->TimeBetweenShots = save->StartTimeBetweenShots;

				save->ShotCountToRecharge--;
			}

			else
				save->TimeBetweenShots -= aexTime->GetFrameTime();
		}
		//Recharge
		else
		{
			save->TimeBetweenShots = 0.f;
			save->ShotCountToRecharge = save->SaveShotCountToRecharge;
			mOwnerStateMachine->ChangeState("SM_EnemyTurret_Recharging");
		}
	}

	SM_EnemyTurret_Recharging::SM_EnemyTurret_Recharging(const char * name) : State(name)
	{
	}

	void SM_EnemyTurret_Recharging::LogicEnter()
	{
		save = static_cast<EnemyTurret*>(mActor);
	}

	void SM_EnemyTurret_Recharging::LogicUpdate()
	{
		// If lives reach 0, die
		if (save->mLives <= 0)
		{
			mOwnerStateMachine->ChangeState("SM_EnemyTurret_Die");
		}

		save->mOwner->GetComp<SpineAnimationComp>()->ChangeAnimation(0, "Recovery", false, 1.f);
		if (save->mOwner->GetComp<SpineAnimationComp>()->GetAnimState()->getCurrent(0)->isComplete())
		{
			mOwnerStateMachine->ChangeState("SM_EnemyTurret_Moving");
		}
	}

	SM_EnemyTurret_Die::SM_EnemyTurret_Die(const char * name) : State(name)
	{

	}

	void SM_EnemyTurret_Die::LogicEnter()
	{
		EnemyTurret* save = static_cast<EnemyTurret*>(mActor);

		// Store the pointer to the comp to delete
		BoxCollider* collider = save->mOwner->GetComp<BoxCollider>();
		save->mOwner->RemoveComp(collider, true);
	}

	void SM_EnemyTurret_Die::LogicUpdate()
	{
		EnemyTurret* save = static_cast<EnemyTurret*>(mActor);

		// Set animation to death animation
		save->mOwner->GetComp<SpineAnimationComp>()->ChangeAnimation(0, "Dead", false, 1.f);

		// Destroy if the animation is over
		if (save->mOwner->GetComp<SpineAnimationComp>()->GetAnimState()->getCurrent(0)->isComplete())
		{
			if (deathTimer >= deathTime)
			{
				// Added by Miguel for wave system
				if (save->mOwner->mSpawnerOwner != nullptr)
					save->mOwner->mSpawnerOwner->EnemyKilled();

				save->mOwner->GetParentSpace()->DestroyObject(save->mOwner);
			}
			else
				deathTimer += (f32)aexTime->GetFrameTime();

			Renderable* mRender = save->mOwner->GetComp<Renderable>();
			mRender->mColor[3] = Lerp(1.f, 0.f, deathTimer / deathTime);
		}

		// Added by Miguel for wave system
		if (save->mOwner->mSpawnerOwner != nullptr)
			save->mOwner->mSpawnerOwner->EnemyKilled();
	}

	void EnemyTurret::SM_Init()
	{
		mBrain[0].AddState(new SM_EnemyTurret_Resting("SM_EnemyTurret_Resting"));
		mBrain[0].AddState(new SM_EnemyTurret_Moving("SM_EnemyTurret_Moving"));
		mBrain[0].AddState(new SM_EnemyTurret_Aiming("SM_EnemyTurret_Aiming"));
		mBrain[0].AddState(new SM_EnemyTurret_Shooting("SM_EnemyTurret_Shooting"));
		mBrain[0].AddState(new SM_EnemyTurret_Recharging("SM_EnemyTurret_Recharging"));
		mBrain[0].AddState(new SM_EnemyTurret_Die("SM_EnemyTurret_Die"));

		mBrain[0].SetInitState("SM_EnemyTurret_Resting");
		mBrain[0].ChangeState(mBrain[0].mInitialState);
		mBrain[0].mCurrentState = mBrain[0].mInitialState;
	}

	void EnemyTurret::Initialize()
	{
		this->Enemy::Initialize();

		Activate = false;

		SM_Init();

		EnemyTimeToRecharge = 0.f;
		ShotCountToRecharge = 2;
		if (mOwner->GetComp<SpineAnimationComp>())
			myOrientation = mOwner->GetComp<SpineAnimationComp>()->GetSkeleton()->getScaleX();

	}
	void EnemyTurret::Update()
	{
		if (enemy_activator)
		{
			Actor::Update();
		}
		this->Enemy::Update();

	}
	bool EnemyTurret::OnGui()
	{
		bool isChanged = false;

		if (ImGui::InputInt("Lives", &mLives))
		{
			isChanged = true;
		}

		if (ImGui::InputDouble("Time Between Shots", &StartTimeBetweenShots))
		{
			isChanged = true;
		}

		if (ImGui::InputDouble("Time First Shot", &TimeFirstShot))
		{
			isChanged = true;
		}

		if (ImGui::InputFloat("Enemy Speed", &EnemySpeed))
		{
			isChanged = true;
		}

		if (ImGui::InputFloat("Run Speed", &runSpeed))
		{
			isChanged = true;
		}

		if (ImGui::InputFloat("Enemy Bullet Speed", &EnemyBulletSpeed))
		{
			isChanged = true;
		}

		if (ImGui::InputDouble("Time to recharge", &SaveEnemyTimeToRecharge))
		{

			isChanged = true;
		}

		if (ImGui::InputInt("Shots to recharge", &SaveShotCountToRecharge))
		{
			isChanged = true;
		}

		if (ImGui::InputFloat("Distance to stop shooting", &aimDistance)) { isChanged = true; }

		if (ImGui::InputFloat("Distance to stop aiming", &reachDistance)) { isChanged = true; }

		return isChanged;
	}

	void EnemyTurret::OnCollisionEvent(const CollisionEvent & collision)
	{
		if (enemy_activator)
		{
			if (collision.otherObject->GetBaseNameString() == "player")
			{
				PlayerStats* stats = collision.otherObject->GetComp<PlayerStats>();

				if (!stats->isInvincible)
				{
					stats->lives--;
					stats->isHit = true;
				}
			}
		}
	}

	void EnemyTurret::FromJson(json & value)
	{
		this->Enemy::FromJson(value);
		value["Time Between Shots"] >> TimeBetweenShots;
		value["Start Time Between Shots"] >> StartTimeBetweenShots;
		value["First shot time"] >> TimeFirstShot;
		value["Shots before recharge"] >> SaveShotCountToRecharge;
		value["Save Time to recharge"] >> SaveEnemyTimeToRecharge;
		value["Run speed"] >> runSpeed;
		value["aimDistance"] >> aimDistance;
		value["reachDistance"] >> reachDistance;
	}
	void EnemyTurret::ToJson(json & value)
	{
		this->Enemy::ToJson(value);

		value["Time Between Shots"] << TimeBetweenShots;
		value["Start Time Between Shots"] << StartTimeBetweenShots;
		value["First shot time"] << TimeFirstShot;
		value["Shots before recharge"] << SaveShotCountToRecharge;
		value["Save Time to recharge"] << SaveEnemyTimeToRecharge;
		value["Run speed"] << runSpeed;
		value["aimDistance"] << aimDistance;
		value["reachDistance"] << reachDistance;
	}
}