#include "EnemyGrounded.h"
#include "Player/Stats/PlayerStats.h"
#include "Components\SpineAnimation.h"
#include <spine\AnimationState.h>
#include <spine\Skeleton.h>
#include "src/Engine/Editor/Editor.h"
#include "AI/Path2D.h"
#include "Enemy/WaveSystem/Spawner.h"
#include "Enemy/WaveSystem/BoilerRoomWaveSystem.h"

namespace AEX
{
	SM_GroundedEnemy_Resting::SM_GroundedEnemy_Resting(const char * name) : State(name)
	{
		Initialized = false;
	}

	void SM_GroundedEnemy_Resting::LogicUpdate()
	{
		if (!Initialized)
		{
			save = static_cast<EnemyGrounded*>(mActor);
			save->InvencibleEnemy = true;
			mPath = save->mOwner->GetComp<PathComponent>();
			time = 0.0f;
			Initialized = true;
		}

		time += (f32)aexTime->GetFrameTime();

		if (time >= 0.5f)
		{
			mOwnerStateMachine->ChangeState("SM_GroundedEnemy_Moving");
		}

		save->mOwner->GetComp<SpineAnimationComp>()->ChangeAnimation(0, "Idle", true, 1.f);
	}

	SM_GroundedEnemy_Moving::SM_GroundedEnemy_Moving(const char * name) : State(name)
	{
		
	}

	void SM_GroundedEnemy_Moving::LogicUpdate()
	{
		save = static_cast<EnemyGrounded*>(mActor);

		// If lives reach 0, die
		if (save->mLives <= 0)
		{
			mOwnerStateMachine->ChangeState("SM_GroundedEnemy_Die");
		}

		save->InvencibleEnemy = false;
		save->mOwner->GetComp<SpineAnimationComp>()->ChangeAnimation(0, "Walk", true, 1.f);

		// Flip if the player is in the other direction!
		float DirectionEnemy = save->PlayerTransform->mLocal.mTranslationZ.x - save->EnemyTransform->mLocal.mTranslationZ.x;

		if (DirectionEnemy < 0)
			save->mOwner->GetComp<SpineAnimationComp>()->GetSkeleton()->setScaleX(save->myOrientation);
		else
			save->mOwner->GetComp<SpineAnimationComp>()->GetSkeleton()->setScaleX(-save->myOrientation);

		f32 distance = fabs((save->PlayerTransform->mLocal.mTranslationZ - save->EnemyTransform->mLocal.mTranslationZ).Length());
		
		if (save->Shoot_activator)
			mOwnerStateMachine->ChangeState("SM_GroundedEnemy_Aiming");
		if (!save->enemy_activator)
			mOwnerStateMachine->ChangeState("SM_GroundedEnemy_Resting");

	}

	SM_GroundedEnemy_Aiming::SM_GroundedEnemy_Aiming(const char * name) : State(name)
	{
		
	}

	void SM_GroundedEnemy_Aiming::LogicUpdate()
	{
		save = static_cast<EnemyGrounded*>(mActor);
		// If lives reach 0, die
		if (save->mLives <= 0)
		{
			mOwnerStateMachine->ChangeState("SM_GroundedEnemy_Die");
		}

		save->mOwner->GetComp<SpineAnimationComp>()->ChangeAnimation(0, "Attack_Shoot", false, 2.f);
		if (save->mOwner->GetComp<SpineAnimationComp>()->GetAnimState()->getCurrent(0)->isComplete())
		{
			mOwnerStateMachine->ChangeState("SM_GroundedEnemy_Shooting");
		}
	}

	SM_GroundedEnemy_Unaiming::SM_GroundedEnemy_Unaiming(const char * name) : State(name)
	{

	}

	void SM_GroundedEnemy_Unaiming::LogicUpdate()
	{
		save = static_cast<EnemyGrounded*>(mActor);

		// If lives reach 0, die
		if (save->mLives <= 0)
		{
			mOwnerStateMachine->ChangeState("SM_GroundedEnemy_Die");
		}

		save->mOwner->GetComp<SpineAnimationComp>()->ChangeAnimation(0, "Recovery", false, 0.5f);
		if (save->mOwner->GetComp<SpineAnimationComp>()->GetAnimState()->getCurrent(0)->isComplete())
		{
			mOwnerStateMachine->ChangeState("SM_GroundedEnemy_Moving");
		}
	}

	SM_GroundedEnemy_Shooting::SM_GroundedEnemy_Shooting(const char * name) : State(name)
	{
		i = 0;
	}

	void SM_GroundedEnemy_Shooting::LogicUpdate()
	{
		save = static_cast<EnemyGrounded*>(mActor);

		// If lives reach 0, die
		if (save->mLives <= 0)
		{
			mOwnerStateMachine->ChangeState("SM_GroundedEnemy_Die");
		}

		save->InvencibleEnemy = false;
		save->mOwner->GetComp<SpineAnimationComp>()->ChangeAnimation(0, "Shooting_Loop", true, 1.f/(2.f * (f32)save->StartTimeBetweenShots));
		float distance = fabs((save->PlayerTransform->mLocal.mTranslationZ - save->EnemyTransform->mLocal.mTranslationZ).Length());

		if (!save->Shoot_activator)
		{
			mOwnerStateMachine->ChangeState("SM_GroundedEnemy_Unaiming");
		}
		else
		{
			if (FirstShot)
			{
				save->EnemyTimeToRecharge = save->SaveEnemyTimeToRecharge;
				save->ShotCountToRecharge = save->SaveShotCountToRecharge;
				save->TimeBetweenShots = save->TimeFirstShot;
				FirstShot = false;
			}
			//Shoot
			if (save->ShotCountToRecharge > 0)
			{
				// If the player has passed the enemy, recharge
				if (save->PlayerTransform->mLocal.mTranslationZ.x - save->EnemyTransform->mLocal.mTranslationZ.x > 0)
				{
					save->ShotCountToRecharge = 0;
				}

				if (save->TimeBetweenShots <= 0)
				{
					EnemyBullet(save->EnemyTransform, i, save->EnemyBulletSpeed, "grounded", save->PlayerTransform);

					i++;

					save->TimeBetweenShots = save->StartTimeBetweenShots;

					save->ShotCountToRecharge--;
				}

				else
					save->TimeBetweenShots -= aexTime->GetFrameTime();
			}
			//Recharge
			else
			{
				save->EnemyTimeToRecharge = save->SaveEnemyTimeToRecharge;
				save->ShotCountToRecharge = save->SaveShotCountToRecharge;
				mOwnerStateMachine->ChangeState("SM_GroundedEnemy_Unaiming");
				save->TimeBetweenShots = 0.f;
			}

		}
	}

	SM_GroundedEnemy_Die::SM_GroundedEnemy_Die(const char * name) : State(name)
	{
		
	}

	void SM_GroundedEnemy_Die::LogicEnter()
	{
		EnemyGrounded* save = static_cast<EnemyGrounded*>(mActor);

		// Store the pointer to the comp to delete
		BoxCollider* collider = save->mOwner->GetComp<BoxCollider>();
		save->mOwner->RemoveComp(collider, true);

		audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\Enemies\\EnemyGrounded\\GroundedDeath1.wav"), Voice::SFX)->SetVolume(0.2f);
	}

	void SM_GroundedEnemy_Die::LogicUpdate()
	{
		EnemyGrounded* save = static_cast<EnemyGrounded*>(mActor);
		// Set animation to death animation
		save->mOwner->GetComp<SpineAnimationComp>()->ChangeAnimation(0, "Dead", false, 1.f);

		// Destroy if the animation is over
		if (save->mOwner->GetComp<SpineAnimationComp>()->GetAnimState()->getCurrent(0)->isComplete())
		{
			if (deathTimer >= deathTime)
			{
				// Added by Miguel for wave system
				if (save->mOwner->mSpawnerOwner != nullptr)
					save->mOwner->mSpawnerOwner->EnemyKilled();

 				if (aexScene->GetBaseNameString() == "Boiler room")
					--BoilerRoomWaveSystem::mGroundedsOnScreen;

				save->mOwner->GetParentSpace()->DestroyObject(save->mOwner);
			}
			else
				deathTimer += (f32)aexTime->GetFrameTime();

			Renderable* mRender = save->mOwner->GetComp<Renderable>();
			mRender->mColor[3] = Lerp(1.f, 0.f, deathTimer / deathTime);
		}
	}

	void EnemyGrounded::SM_Init()
	{
		mBrain[0].AddState(new SM_GroundedEnemy_Resting("SM_GroundedEnemy_Resting"));
		mBrain[0].AddState(new SM_GroundedEnemy_Moving("SM_GroundedEnemy_Moving"));
		mBrain[0].AddState(new SM_GroundedEnemy_Aiming("SM_GroundedEnemy_Aiming"));
		mBrain[0].AddState(new SM_GroundedEnemy_Shooting("SM_GroundedEnemy_Shooting"));
		mBrain[0].AddState(new SM_GroundedEnemy_Unaiming("SM_GroundedEnemy_Unaiming"));
		mBrain[0].AddState(new SM_GroundedEnemy_Die("SM_GroundedEnemy_Die"));

		mBrain[0].SetInitState("SM_GroundedEnemy_Resting");
		mBrain[0].ChangeState(mBrain[0].mInitialState);
		mBrain[0].mCurrentState = mBrain[0].mInitialState;
	}

	void EnemyGrounded::Initialize()
	{
		this->Enemy::Initialize();

		Activate = false;

		SM_Init();
		 
		//SaveEnemyTimeToRecharge = 0.5f;
		//SaveShotCountToRecharge = 4;

		if(mOwner->GetComp<SpineAnimationComp>())
			myOrientation = mOwner->GetComp<SpineAnimationComp>()->GetSkeleton()->getScaleX();
	}
	void EnemyGrounded::Update()
	{
		if (myOrientation == 0.f)
			Initialize();

		if(mBrain[0].mCurrentState->mName == "SM_GroundedEnemy_Resting")
		{
			myState = EnemyState::eResting;
		}
		else if (mBrain[0].mCurrentState->mName == "SM_GroundedEnemy_Moving")
		{
			myState = EnemyState::eMoving;
		}
		else if (mBrain[0].mCurrentState->mName == "SM_GroundedEnemy_Shooting")
		{
			myState = EnemyState::eShooting;
		}
		else if (mBrain[0].mCurrentState->mName == "SM_GroundedEnemy_Die")
		{
			myState = EnemyState::eDying;
		}
		else if (mBrain[0].mCurrentState->mName == "SM_GroundedEnemy_Aiming")
		{
			myState = EnemyState::eAiming;
		}
		else
		{
			myState = EnemyState::eUnaiming;
		}

		if (enemy_activator)
		{
			Actor::Update();
		}
		this->Enemy::Update();

	}
	bool EnemyGrounded::OnGui()
	{
		bool isChanged = false;

		if (ImGui::InputInt("Lives", &mLives))
		{
			isChanged = true;
		}

		if (ImGui::InputDouble("Time Between Shots", &StartTimeBetweenShots))
		{
			isChanged = true;
		}

		if (ImGui::InputDouble("Time First Shot", &TimeFirstShot))
		{
			isChanged = true;
		}

		//if (ImGui::InputFloat("Distance activate enemy", &DistanceEnemyActivate))
		//{
		//	isChanged = true;
		//}

		//if (ImGui::InputFloat("Dist to attack", &DistanceEnemyAttack))
		//{
		//	isChanged = true;
		//}
		//
		//if (ImGui::InputFloat("Distance not attack player", &DistanceEnemyNotAttack))
		//{
		//	isChanged = true;
		//}

		if (ImGui::InputFloat("Enemy Speed", &EnemySpeed))
		{
			isChanged = true;
		}

		if (ImGui::InputFloat("Enemy Bullet Speed", &EnemyBulletSpeed))
		{
			isChanged = true;
		}

		if (ImGui::InputDouble("Time to recharge", &SaveEnemyTimeToRecharge))
		{
			
			isChanged = true;
		}

		if (ImGui::InputInt("Shots to recharge", &SaveShotCountToRecharge))
		{
			isChanged = true;
		}


		//ImGui::InputFloat("Dist to activate", &Radius_to_activate);
		//ImGui::InputFloat("Dist to attack", &Radius_to_attack);
		//ImGui::InputFloat("Distance STOP attack player", &Radius_to_STOP_attacking);
		return isChanged;
	}

	void EnemyGrounded::OnCollisionEvent(const CollisionEvent & collision)
	{
		if (enemy_activator)
		{
			if (collision.otherObject->GetBaseNameString() == "player")
			{
				PlayerStats* stats = collision.otherObject->GetComp<PlayerStats>();

				if (!stats->isInvincible)
				{
					stats->lives--;
					stats->isHit = true;
				}
			}
		}
	}

	void EnemyGrounded::FromJson(json & value)
	{
		this->Enemy::FromJson(value);
		value["Time Between Shots"] >> TimeBetweenShots;
		value["Start Time Between Shots"] >> StartTimeBetweenShots;
		value["First shot time"] >> TimeFirstShot;
		value["Shots before recharge"] >> SaveShotCountToRecharge;
		value["Save Time to recharge"] >> SaveEnemyTimeToRecharge;
	}
	void EnemyGrounded::ToJson(json & value)
	{
		this->Enemy::ToJson(value);

		value["Time Between Shots"] << TimeBetweenShots;
		value["Start Time Between Shots"] << StartTimeBetweenShots;
		value["First shot time"] << TimeFirstShot;
		value["Shots before recharge"] << SaveShotCountToRecharge;
		value["Save Time to recharge"] << SaveEnemyTimeToRecharge;
	}
}