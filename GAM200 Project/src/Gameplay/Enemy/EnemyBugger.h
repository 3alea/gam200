#pragma once
#include "Enemy.h"

// Forward declare spine animation class
class SpineAnimationComp;

using namespace AEX;

class EnemyBugger : public Enemy, public Actor
{
	AEX_RTTI_DECL(EnemyBugger, Enemy)

public:
	EnemyBugger();
	~EnemyBugger();

	void Initialize();
	void Update();
	void ShutDown();
	bool OnGui();

	void SM_Init();

	void FromJson(json & value);
	void ToJson(json & value);

	virtual void OnCollisionEvent(const CollisionEvent& collision);

	StateMachine* state_machine;

	// Spine animation
	SpineAnimationComp* spineAnimation;
	TransformComp* t;

	PathComponent* path;
	PathFollower* FollowandStop;
	f32 distance;
	f32 time_to_shoot;

	f32 dist_respect_player;
	bool shield = true;
	bool initialized = false;
	bool falling = false;
	bool onFloor = false;
};

struct Bugger_Resting : public State
{
	Bugger_Resting(const char * name);

	void LogicEnter();
	void LogicUpdate();
	void LogicExit();
	bool Initialized;

	EnemyBugger* the_bugger;
};

struct Bugger_Descending : public State
{
	Bugger_Descending(const char * name);

	void LogicEnter();
	void LogicUpdate();
	void LogicExit();
	bool Initialized;

	EnemyBugger* the_bugger; 
	PathComponent* descending_path;
};							

struct Bugger_is_Down : public State
{
	Bugger_is_Down(const char * name);

	void LogicEnter();
	void LogicUpdate();
	void LogicExit();
	bool Initialized;

	EnemyBugger* the_bugger;
	f32 timer;
};

struct Bugger_Shooting : public State // unprotected
{
	Bugger_Shooting(const char * name);

	void LogicEnter();
	void LogicUpdate();
	void LogicExit();
	bool Initialized;
	
	EnemyBugger* the_bugger;
	f32 timer;
	bool shoot;
	int i;
};

struct Bugger_Ascending : public State
{
	Bugger_Ascending(const char * name);

	void LogicEnter();
	void LogicUpdate();
	void LogicExit();
	bool Initialized;

	EnemyBugger* the_bugger;
	PathComponent* ascending_path;
};
struct Acid : public State
{
	Acid(const char * name);

	void LogicEnter();
	void LogicUpdate();
	void LogicExit();
	bool Initialized;

	EnemyBugger* the_bugger;
	PathComponent* ascending_path;
};