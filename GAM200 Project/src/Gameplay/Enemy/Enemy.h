#pragma once

#include "Imgui/imgui.h"
#include "Components/Logic.h"
#include "Composition/AEXGameObject.h"
#include "Composition/AEXObjectManager.h"
#include "src/Engine/Composition/AEXScene.h"
#include "src/Engine/Platform/AEXTime.h"
#include "src/Engine/Components/SpriteComps.h"
#include "src/Engine/Physics/CollisionSystem.h"
#include "src/Engine/Math/Collisions.h"
#include "src/Engine/Components/Collider.h"
#include "src/Engine/AI/StateMachine.h"
#include "src/Engine/AI/Easing.h"
#include "src/Engine/AI/Path2D.h"
#include "Graphics/GfxMgr.h"
#include "Audio\AudioManager.h"

namespace AEX
{
	class TransformComp;
	class Enemy : public LogicComp
	{
		AEX_RTTI_DECL(Enemy, LogicComp)

		public:
			Enemy();

			void Initialize();
			void Update();
			void EditorUpdate();

			virtual void SM_Init() {}
			virtual void SM_Update() {}

			AEVec3 Movement(AEVec3 EnemyPosition, AEVec3 PlayerPosition, f32 Speed);
			virtual void OnCollisionEvent(const CollisionEvent& collision) {}

			void FromJson(json & value);
			void ToJson(json & value);
			bool Activate;

			TransformComp * EnemyTransform;
			TransformComp * PlayerTransform;
			Rigidbody* rb;

			f32 EnemySpeed;
			f32 EnemyBulletSpeed;
			f32 DistanceEnemyActivate;
			f32 DistanceEnemyAttack;
			f32 DistanceEnemyNotAttack;

			f32 BulletsTimeToDestroy;
			f32 BulletsStartTimeToDestroy;

			f32 damageTime = 0.0f;
			bool ActivateDamage = false;

			bool InvencibleEnemy = false;
			bool doneOnce = false;
			int mLives;
			AEVec3 initpos;

			//unai
			bool enemy_activator = false;
			bool Shoot_activator = false;
			f32 time;
			f32 time_when_activated;
			f64 TimeFirstShot;
			f32 TimetoDesactivate;
			f32 time_when_desactivation_started;
			bool desactivationprocess;
			bool just_got_out_of_screen;
			bool going_back_home;
			//f32 Radius_to_activate;
			//f32 Radius_to_attack;
			//f32 Radius_to_STOP_attacking;
			//DebugCircle circle_to_active;
			//DebugCircle circle_to_STOP_attacking;
			//DebugCircle circle_to_attack;
			

	};

}