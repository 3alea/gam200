#include "EnemyBugger.h"
#include "Player/Stats/PlayerStats.h"
#include "Components\SpineAnimation.h"
#include <spine\AnimationState.h>
#include <spine\Skeleton.h>
#include "Composition\AEXScene.h"
#include "src/Engine/AI/StateMachine.h"
#include "Bullets/BulletLogic.h"
#include "EnemyBullet.h"
#include "Enemy/WaveSystem/Spawner.h"

using namespace AEX;

EnemyBugger::EnemyBugger() {
	state_machine = new StateMachine(this);
	
	//create path and path follower
	//path = NULL;
	FollowandStop = new PathFollower;
	FollowandStop->SetFollowPolicy(PathFollower::FollowAndStop);
	time_to_shoot = 2.0f;
}

EnemyBugger::~EnemyBugger() {

}

void EnemyBugger::Initialize() {
	Enemy::Initialize();
	SM_Init();
	PlayerTransform = aexScene->FindObjectInANYSpace("player")->GetComp<TransformComp>();
	//DistanceEnemyAttack = 8.0f;
	//distance = 2.0f;

	//mOwner->AddComp(path);
	
}

void EnemyBugger::SM_Init()
{
	state_machine->AddState(new Bugger_Resting("Bugger_Resting"));
	state_machine->AddState(new Bugger_Descending("Bugger_Descending"));
	state_machine->AddState(new Bugger_is_Down("Bugger_is_Down"));
	state_machine->AddState(new Bugger_Shooting("Bugger_Shooting"));
	state_machine->AddState(new Bugger_Ascending("Bugger_Ascending"));
	state_machine->AddState(new Acid("Acid"));

	state_machine->SetInitState("Bugger_Resting");
	state_machine->ChangeState(state_machine->mInitialState);
	state_machine->mCurrentState = state_machine->mInitialState;
}

void EnemyBugger::Update() {

	Enemy::Update();
	if (!initialized)
	{
		mOwner->AddIfNotRepeatedComybyComp(FollowandStop);
		spineAnimation = mOwner->GetComp<SpineAnimationComp>();
		t = mOwner->GetComp<TransformComp>();
		initialized = true;
	}

	if (mLives <= 0)
	{
		// Added by Miguel for wave system
		if (mOwner->mSpawnerOwner != nullptr)
			mOwner->mSpawnerOwner->EnemyKilled();

		state_machine->ChangeState("Acid");

		if (!onFloor)
		{
			if (!falling)
				spineAnimation->ChangeAnimation(0, "Death_Empieza", false);
			else
				spineAnimation->ChangeAnimation(0, "Death_Falling_Loop", false);

			if (spineAnimation->GetAnimState()->getCurrent(0)->isComplete())
			{
				Rigidbody* rg = new Rigidbody();
				rg->mDrag = 0.25f;
				mOwner->AddComp(rg);
				rb = rg;
				falling = true;
			}
		}
		else
		{
			t->mLocal.mTranslationZ.z = 1.f;
			spineAnimation->ChangeAnimation(0, "Death_End", false);
			spineAnimation->ChangeAnimation(1, "acido", false);
		}
	}
	
	state_machine->Update();
}

void EnemyBugger::OnCollisionEvent(const CollisionEvent& collision) {
	
	if (collision.otherObject->GetBaseNameString() == "player")
	{
		PlayerStats* stats = collision.otherObject->GetComp<PlayerStats>();
	
		if (!stats->isInvincible)
		{
			stats->lives--;
			stats->isHit = true;
		}
	}
	else if (collision.otherObject->GetComp<ICollider>()->collisionGroup == "Enemy")
	{
		collision.otherObject->GetComp<Enemy>()->mLives = 0;
	}

	if (BulletLogic * logic = collision.otherObject->GetComp<BulletLogic>())
	{
		if (shield)
			mLives = mLives;
		if (Shoot_activator)
		{
			audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\Enemies\\EnemyBugger\\BuggerHit.wav"), Voice::SFX)->SetVolume(0.5);
			mLives--;
		}
	}

	if (!strcmp(collision.otherObject->GetComp<ICollider>()->collisionGroup.c_str(), "Floor"))
	{
		rb->mIgnoreGravity = true;
		rb->mVelocity.y = 0.f;
		rb->mAcceleration.y = 0.f;
		
		if(path)
			mOwner->RemoveComp(path, true);

		if(FollowandStop)
			mOwner->RemoveComp(FollowandStop, true);

		spineAnimation->offset = AEVec2(-0.15f, 3.5f);
	}
	if (falling)
		onFloor = true;
}

void EnemyBugger::ShutDown() {
	mOwner->RemoveComp(FollowandStop, false);
}

bool EnemyBugger::OnGui(){

	ImGui::InputInt("Lives", &mLives);

	//ImGui::InputFloat("Dist to attack", &DistanceEnemyAttack);

	ImGui::InputFloat("Distance up and down", &distance);
	ImGui::InputDouble("Time First Shot", &TimeFirstShot);
	//ImGui::InputFloat("Dist to activate", &Radius_to_activate);
	//ImGui::InputFloat("Dist to attack", &Radius_to_attack);


	return false;
}

void EnemyBugger::FromJson(json & value)
{
	this->Enemy::FromJson(value);
	value["Distance enemy to attack"] >> DistanceEnemyAttack;
	value["Distance"] >> distance;
	value["Time to shoot"] >> time_to_shoot;
}
void EnemyBugger::ToJson(json & value)
{
	this->Enemy::ToJson(value);
	value["Distance enemy to attack"] << DistanceEnemyAttack;
	value["Distance"] << distance;
	value["Time to shoot"] << time_to_shoot;

}

// BUGGER STATES /////////////////////////////////////////////////////////////////////////////////////////////

Bugger_Resting::Bugger_Resting(const char* name) : State(name) {
	Initialized = false;
}

void Bugger_Resting::LogicEnter() {
	Initialized = false;
}

void Bugger_Resting::LogicUpdate() {

	if (!Initialized)
	{
		the_bugger = static_cast<EnemyBugger*>(mActor);
		Initialized = true;
	}
	if (the_bugger->Shoot_activator)
		LogicExit();
}

void Bugger_Resting::LogicExit() {
	mOwnerStateMachine->ChangeState("Bugger_Descending");
}

Bugger_Descending::Bugger_Descending(const char* name) : State(name) {
	descending_path = new PathComponent;
}

void Bugger_Descending::LogicEnter() {
	Initialized = false;

}

void Bugger_Descending::LogicUpdate() {
	if (!Initialized)
	{
		the_bugger = static_cast<EnemyBugger*>(mActor);
		Initialized = true;
		descending_path->Clear();
		descending_path->Push(0.0f, AEVec2(the_bugger->mOwner->GetComp<TransformComp>()->GetPosition3D().x, the_bugger->mOwner->GetComp<TransformComp>()->GetPosition3D().y));
		descending_path->Push(0.65f, AEVec2(descending_path->LastPoint().x, descending_path->LastPoint().y - the_bugger->distance));
		descending_path->GetPath().mEaseFunction = EaseOutQuad;
		the_bugger->mOwner->AddComp(descending_path);
	}
	the_bugger->shield = true;
	the_bugger->spineAnimation->ChangeAnimation(0, "Open", false);

	if (descending_path->Duration() < the_bugger->FollowandStop->GetTime()) //path finsihed
	{
		the_bugger->FollowandStop->SetTime(0.0f);
		LogicExit();
	}
}

void Bugger_Descending::LogicExit() {
	the_bugger->mOwner->RemoveComp(descending_path, false);
	mOwnerStateMachine->ChangeState("Bugger_Shooting");
}

Bugger_is_Down::Bugger_is_Down(const char* name) : State(name) {
}

void Bugger_is_Down::LogicEnter() {
	Initialized = false;
}

void Bugger_is_Down::LogicUpdate() {
	if (!Initialized)
	{
		the_bugger = static_cast<EnemyBugger*>(mActor);
		Initialized = true;
		the_bugger->shield = true;
		timer = 0.0f;
		the_bugger->mOwner->GetComp<TransformComp>()->SetScale(AEVec2(1.0f, 1.0f));
		the_bugger->mOwner->GetComp<BoxCollider>()->mScale = AEVec2(1.0f, 1.0f);
	}
	the_bugger->shield = true;
	timer += (f32)aexTime->GetFrameTime();
	// here with a timer(i guess) it should go changing between is down and shooting
	if (timer > the_bugger->time_to_shoot)
	{
		the_bugger->spineAnimation->ChangeAnimation(0, "Open", false);
		if(the_bugger->spineAnimation->GetAnimState()->getCurrent(0)->isComplete())
			mOwnerStateMachine->ChangeState("Bugger_Shooting");
	}
		

	if (the_bugger->Shoot_activator == false) //if the player is far
		mOwnerStateMachine->ChangeState("Bugger_Ascending");
}

void Bugger_is_Down::LogicExit() {
}

Bugger_Shooting::Bugger_Shooting(const char* name) : State(name) {
	i = 0;
}

void Bugger_Shooting::LogicEnter() {
	Initialized = false;
	
}

void Bugger_Shooting::LogicUpdate() {
	if (!Initialized)
	{
		the_bugger = static_cast<EnemyBugger*>(mActor);
		the_bugger->shield = false; //enemmy can be damaged
		timer = 0.0f;
		Initialized = true; 
		the_bugger->mOwner->GetComp<TransformComp>()->SetScale(AEVec2(2.0f, 2.0f));
		the_bugger->mOwner->GetComp<BoxCollider>()->mScale = AEVec2(2.0f, 2.0f);
		shoot = false;
	}
	the_bugger->shield = false;
	// here with a timer(i guess) it should go changing between is down and shooting
	timer += (f32)aexTime->GetFrameTime();

	if (!shoot)
	{
		the_bugger->spineAnimation->ChangeAnimation(0, "shoot", false);
		if (timer > 0.5f)
		{
			EnemyBullet(the_bugger->EnemyTransform, i, 0.1f, "bugger", the_bugger->PlayerTransform, 1);
			i++;
			EnemyBullet(the_bugger->EnemyTransform, i, 0.1f, "bugger", the_bugger->PlayerTransform, 2);
			i++;
			EnemyBullet(the_bugger->EnemyTransform, i, 0.1f, "bugger", the_bugger->PlayerTransform, 3);
			i++;

			shoot = true;
		}
	}

	if (timer > the_bugger->time_to_shoot)// time to shoot and also to be damaged
	{
		the_bugger->spineAnimation->ChangeAnimation(0, "Close", false);
		if (the_bugger->spineAnimation->GetAnimState()->getCurrent(0)->isComplete())
		{
			mOwnerStateMachine->ChangeState("Bugger_is_Down");
		}
	}
}

void Bugger_Shooting::LogicExit() {
	
}

Bugger_Ascending::Bugger_Ascending(const char* name) : State(name) {
	ascending_path = new PathComponent;
}

void Bugger_Ascending::LogicEnter() {
	Initialized = false;
}
void Bugger_Ascending::LogicUpdate() {
	if (!Initialized)
	{
		the_bugger = static_cast<EnemyBugger*>(mActor);
		the_bugger->shield = true;
		Initialized = true;
		ascending_path->Clear();
		ascending_path->Push(0.1f, AEVec2(the_bugger->mOwner->GetComp<TransformComp>()->GetPosition3D().x, the_bugger->mOwner->GetComp<TransformComp>()->GetPosition3D().y));
		ascending_path->Push(1.2f, AEVec2(ascending_path->LastPoint().x, ascending_path->LastPoint().y + the_bugger->distance));
		ascending_path->GetPath().mEaseFunction = EaseInQuad;
		the_bugger->mOwner->AddComp(ascending_path);
	}
	the_bugger->shield = true;
	the_bugger->mOwner->GetComp<SpineAnimationComp>()->ChangeAnimation(0, "Close", false);
	if (ascending_path->Duration() < the_bugger->FollowandStop->GetTime()) //path finsihed
	{
		the_bugger->FollowandStop->SetTime(0.0f);
		LogicExit();
	}
}

void Bugger_Ascending::LogicExit() {
	the_bugger->mOwner->RemoveComp(ascending_path, false);
	mOwnerStateMachine->ChangeState("Bugger_Resting");
}


Acid::Acid(const char* name) : State(name) {
	//i = 0;
}

void Acid::LogicEnter() {
	Initialized = false;
}
void Acid::LogicUpdate() {
	if (!Initialized)
	{
		the_bugger = static_cast<EnemyBugger*>(mActor);
		the_bugger->shield = true;
		the_bugger->mOwner->GetComp<TransformComp>()->SetScale(AEVec2(1.0f, 0.1f));
		the_bugger->mOwner->GetComp<BoxCollider>()->mScale = AEVec2(1.0f, 0.1f);
		the_bugger->mOwner->GetComp<BoxCollider>()->mOffset = AEVec2(0.0f, -1.5f);
		Initialized = true;
	}

	//changesprite

	//maybe
	// if collision with floor change sprite

}

void Acid::LogicExit() {

}


