#include  "EnemySpawner.h"
#include "Composition/ComponentList.h"
#include "Composition/AEXSpace.h"
#include "Bullets/BulletLogic.h"
#include "Player/Stats/PlayerStats.h"
#include "Utilities/RNG.h"
#include <cstdlib> //srand
#include "EnemyMitosis.h"
#include "Components\SpineAnimation.h"
#include <spine\AnimationState.h>
#include "src\Gameplay\Enemy\EnemyFlying.h"
#include "Enemy/WaveSystem/Spawner.h"

EnemySpawner::EnemySpawner()
{
	//exitPath = new PathComponent;
	//upPath = new PathComponent;
	//rightPath = new PathComponent;
	//leftPath = new PathComponent;
	verticalBOT = new PathComponent;
	verticalTOP = new PathComponent;
	//exitPath->changey = false;
	//upPath->changey = false;
	//rightPath->changey = true;
	//leftPath->changey = true;
	verticalTOP->changex = true;
	verticalBOT->changex = true;
	mLives = 7;
	//selectedpath = 0;
	time_to_spawn = 5.0f;

	mitosis_shoot_timer = 0.2f;
	mitosis_bullet_speed = 0.15f;

	//mitosis_attack_range = 16.0f;
}

EnemySpawner::~EnemySpawner()
{
	//...
}

void EnemySpawner::Initialize()
{
	EnemySpeed = 0.0f;
	EnemyTransform = mOwner->GetComp<TransformComp>();
	rb = mOwner->GetComp<Rigidbody>();
	current_time = 4.0f;
	count = 0;
	spineAnimation = mOwner->GetComp<SpineAnimationComp>();
}

void EnemySpawner::Update()
{
	Enemy::Update();
	//DeadMitosis();
	if (enemy_activator)
	{
		// If the lives reach 0, kill this enemy and do not update
		if (mLives <= 0)
		{
			// Set animation to death animation 
			spineAnimation->ChangeAnimation(0, "Death", false, 1.f, 0.3f);

			if (deathTimer >= deathTime)
			{
				// Added by Miguel for wave system
				if (mOwner->mSpawnerOwner != nullptr)
					mOwner->mSpawnerOwner->EnemyKilled();

				mOwner->GetParentSpace()->DestroyObject(mOwner);
			}
			else
				deathTimer += (f32)aexTime->GetFrameTime();

			Renderable* mRender = mOwner->GetComp<Renderable>();
			mRender->mColor[3] = Lerp(1.f, 0.f, deathTimer / deathTime);
		}
		else if (Shoot_activator)
		{
			if (current_time >= time_to_spawn)
			{
				if (count < 2)
				{
					spineAnimation->ChangeAnimation(0, "Spit", true, 1.f, 0.3f);
					// Allow spawning
					canSpawn = true;
					if (spineAnimation->GetAnimState()->getCurrent(0)->isComplete())
					{
						count++;
						current_time = 0.0f;   // reset timer
						passedTime = 0.f;
						canSpawn = false;
					}
				}

				// If about to spawn, add to the timer
				if (canSpawn)
				{
					passedTime += (f32)aexTime->GetFrameTime();
				}
				
				// Spawn in the appropriate time
				if (passedTime >= spawnTime)
				{
					SpawnEnemy();
					canSpawn = false;
					passedTime = 0.f;
				}
			}
			else
			{
				spineAnimation->ChangeAnimation(0, "Walk", true, 1.f, 0.3f);
				EnemyTransform->mLocal.mTranslationZ.x -= 0.01f;
			}
		}
	}
	current_time += (f32)aexTime->GetFrameTime();
	//PathLogic();
}

void EnemySpawner::SpawnEnemy()
{
	GameObject* mitosis = new GameObject;
	std::string s = "mitosis" + std::to_string(count);
	mitosis->SetBaseName(s.c_str());
	CreateMitosis(mitosis);
	mOwner->AddChild(mitosis);
 }

void EnemySpawner::CreateMitosis(GameObject * mitosis)
{
	
	mitosis = mOwner->GetParentSpace()->InstantiateArchetype("mitosis", EnemyTransform->mLocal.mTranslationZ + offset);
	EnemyMitosis* mytosis = mitosis->GetComp<EnemyMitosis>();
	mytosis->spawner = this;
	mytosis->TimeFirstShot = 1.0f;
	mitosis->GetComp<TransformComp>()->SetScale(mitosis->GetComp<TransformComp>()->GetScale() / 2);

	mytosis->EnemyBulletSpeed = mitosis_bullet_speed;
	mytosis->mTimeBetweenShots = mitosis_shoot_timer;

	// Create a random value
	float angle = RNG->GetFloat(3.f * PI / 4.f , PI);

	// Add a starting force to the mitosis
	mitosis->GetComp<Rigidbody>()->AddForce(AEVec2(cos(angle) * spawnForce, sin(angle) * spawnForce));

	// Set the mitosis' separation to the cosine/sine of that angle
	mytosis->seperationForce.x = 0.f;
	mytosis->seperationForce.y = cos(angle) * spawnForce * 0.5f;

	//PathCreator();

	mitosis->GetComp<EnemyMitosis>()->mitcount = count;
	//mitosis->AddComp(exitPath);
	//PathFollower* pfollow = new PathFollower;
	//pfollow->SetFollowPolicy(PathFollower::FollowAndStop);
	//mitosis->AddComp(pfollow);

	all_mitosis.push_back(mitosis);
}

void EnemySpawner::PathLogic()
{
	//FOR_EACH(it, all_mitosis)
	//{
	//	if ((*it)->GetComp<PathFollower>() == NULL)
	//	{
	//		continue;
	//	}
	//	
	//	if ((*it)->GetComp<PathComponent>() == NULL)
	//	{
	//		continue;
	//	}
	//		
	//
	//	PathComponent* CurrentPath = (*it)->GetComp<PathComponent>();
	//
	//	if ((*it)->GetComp<PathFollower>()->GetTime() > CurrentPath->Duration())
	//	{
	//		(*it)->GetComp<PathFollower>()->SetTime(0.0f);
	//
	//		if ((*it)->GetComp<EnemyMitosis>()->mitcount % 2 == 0)
	//		{
	//			if (CurrentPath == exitPath)
	//			{
	//				(*it)->RemoveComp(exitPath, false);
	//				(*it)->AddComp(upPath);
	//			}
	//			else if (CurrentPath == upPath)
	//			{
	//				(*it)->RemoveComp(upPath, false);
	//				(*it)->AddComp(rightPath);
	//			}
	//			else if (CurrentPath == rightPath)
	//			{
	//				(*it)->RemoveComp(rightPath, false);
	//				(*it)->AddComp(leftPath);
	//			}
	//			else if (CurrentPath == leftPath)
	//			{
	//				(*it)->RemoveComp(leftPath, false);
	//				(*it)->AddComp(rightPath);
	//			}
	//		}
	//		else if ((*it)->GetComp<EnemyMitosis>()->mitcount % 2 == 1)
	//		{
	//			if (CurrentPath == exitPath)
	//			{
	//				(*it)->RemoveComp(exitPath, false);
	//				(*it)->AddComp(verticalBOT);
	//			}
	//			else if (CurrentPath == verticalBOT && (*it)->GetComp<PathFollower>()->GetPolicy() == PathFollower::FollowAndStop)
	//			{
	//				(*it)->GetComp<PathFollower>()->SetFollowPolicy(PathFollower::PingPong);
	//			}
	//		}
	//	}
	//}

}

void EnemySpawner::PathCreator()
{
	////Path 1  exit
	//exitPath->Push(0.0f, AEVec2(mOwner->GetComp<TransformComp>()->GetPosition3D().x - 1.0f, mOwner->GetComp<TransformComp>()->GetPosition3D().y + 0.8f));
	//exitPath->Push(0.95f, AEVec2(exitPath->LastPoint().x - 2.0f, exitPath->LastPoint().y + 2.f));
	//exitPath->GetPath().mEaseFunction = EaseOutQuad;
	////Path 2 up
	//upPath->Push(0.0f, AEVec2(exitPath->LastPoint().x, exitPath->LastPoint().y));
	//upPath->Push(1.0f, AEVec2(upPath->LastPoint().x - 5.0f, upPath->LastPoint().y + 3.0f));
	//upPath->GetPath().mEaseFunction = EaseInOutQuad;
	////Path 3 right
	//rightPath->Push(0.0f, AEVec2(upPath->LastPoint().x, upPath->LastPoint().y));
	//rightPath->Push(3.2f, AEVec2(rightPath->LastPoint().x + 7.0f, rightPath->LastPoint().y));
	//rightPath->GetPath().mEaseFunction = EaseOutQuad;
	////Path 4 right
	//leftPath->Push(0.0f, AEVec2(rightPath->LastPoint().x, rightPath->LastPoint().y));
	//leftPath->Push(3.2f, AEVec2(leftPath->LastPoint().x - 7.0f, leftPath->LastPoint().y));
	//leftPath->GetPath().mEaseFunction = EaseOutQuad;
	//
	////Path 2.2 right
	//verticalBOT->Push(1.0f, AEVec2(exitPath->LastPoint().x, exitPath->LastPoint().y));
	//verticalBOT->Push(2.0f, AEVec2(verticalBOT->LastPoint().x, verticalBOT->LastPoint().y + 4.0f));
	//verticalBOT->Push(3.0f, AEVec2(verticalBOT->LastPoint().x, verticalBOT->LastPoint().y - 4.0f));
	//verticalBOT->GetPath().mEaseFunction = EaseInOutQuad;

}

PathComponent* EnemySpawner::PickPath()
{
	return exitPath;
}


bool EnemySpawner::OnGui()
{
	bool isChanged = false;

	if (ImGui::InputFloat("Spawn time", &time_to_spawn)) { isChanged = true; }

	if (ImGui::InputFloat("Spawn force", &spawnForce)) { isChanged = true; }

	if (ImGui::InputFloat("Real spawn time", &spawnTime)) { isChanged = true; }

	if (ImGui::InputFloat2("Spawning offset", offsetTemp))
	{
		offset.x = offsetTemp[0];
		offset.y = offsetTemp[1];
		isChanged = true;
	}

	if (ImGui::InputInt("Lives", &mLives))
	{
		isChanged = true;
	}

	if (ImGui::InputInt("Path", &selectedpath))
	{
		isChanged = true;
	}

	ImGui::InputDouble("Time First Shot", &TimeFirstShot);

	ImGui::InputFloat("Mitosis shoot timer", &mitosis_shoot_timer);

	ImGui::InputFloat("Mitosis bullet speed", &mitosis_bullet_speed);
	return isChanged;
}

void EnemySpawner::OnCollisionEvent(const CollisionEvent & collision)
{
	if (enemy_activator)
	{

		if (BulletLogic * logic = collision.otherObject->GetComp<BulletLogic>())
		{
			collision.otherObject->GetParentSpace()->DestroyObject(collision.otherObject);

			mLives--;
		}

		if (collision.otherObject->GetBaseNameString() == "player")
		{
			PlayerStats* stats = collision.otherObject->GetComp<PlayerStats>();

			if (!stats->isInvincible)
			{
				stats->lives--;
				stats->isHit = true;
			}
		}

	}
}

void EnemySpawner::EraseMitosis(EnemyMitosis* toDestroy)
{
	for (auto it = all_mitosis.begin(); it != all_mitosis.end(); it++)
	{
		if ((*it)->GetComp<EnemyMitosis>() == toDestroy)
		{
			//(*it)->RemoveComp((*it)->GetComp<PathFollower>(), false);
			//(*it)->RemoveComp((*it)->GetComp<PathComponent>(), false);
			//dead_mitosis.push_back(*it);
			all_mitosis.erase(it);
			count--;
			return;
		}
	}
}

void EnemySpawner::FromJson(json & value)
{
	this->Enemy::FromJson(value);

	value["Time to spawn"] >> time_to_spawn;
	value["Spawner lives"] >> spawnerlives;
	value["Time between shoot"] >> mitosis_shoot_timer;
	value["Mitosis Bullet speed"] >> mitosis_bullet_speed;
	value["OffsetX"] >> offset.x;
	value["Offsety"] >> offset.y;
	value["RealSpawnTime"] >> spawnTime;
	value["spawnForce"] >> spawnForce;
}

void EnemySpawner::ToJson(json & value)
{
	this->Enemy::ToJson(value);

	value["Time to spawn"] << time_to_spawn;
	value["Spawner lives"] << spawnerlives;
	value["Time between shoot"] << mitosis_shoot_timer;
	value["Mitosis Bullet speed"] << mitosis_bullet_speed;
	value["OffsetX"] << offset.x;
	value["Offsety"] << offset.y;
	value["RealSpawnTime"] << spawnTime;
	value["spawnForce"] << spawnForce;
}

FlyingSpawner::FlyingSpawner()
{
	spawn_time = 5.f;
}
void FlyingSpawner::Initialize()
{
	timer = 0.f;
	owner_transform = &mOwner->GetComp<TransformComp>()->mWorld;
	flying_shoot_timer = 0.2f;
	flying_bullet_speed = 0.15f;
	count = 0;
	activate = false;
}

void FlyingSpawner::Update()
{
	if (activate)
	{
		timer += (f32)aexTime->GetFrameTime();

		if (timer > spawn_time)
		{
			Spawn();
			timer = 0.f;
			//count++;
		}
	}
	for (auto it = all_flying.begin(); it != all_flying.end(); )
	{
		if ((*it)->mbAlive = false)
			it = all_flying.erase(it);
		else
			it++;
	}
}

void FlyingSpawner::Spawn()
{
	GameObject* flying = mOwner->GetParentSpace()->InstantiateArchetype("flying", AEVec3(owner_transform->mTranslationZ.x, owner_transform->mTranslationZ.y, owner_transform->mTranslationZ.z));
	flying->GetComp<EnemyFlying>()->EnemyBulletSpeed = flying_bullet_speed;
	flying->GetComp<EnemyFlying>()->StartTimeBetweenShots = flying_shoot_timer;
	all_flying.push_back(flying);
}

bool FlyingSpawner::OnGui()
{
	ImGui::InputFloat("Spawn time", &spawn_time);

	ImGui::InputFloat("Flying shoot timer", &flying_shoot_timer);

	ImGui::InputFloat("Flying bullet speed", &flying_bullet_speed);

	ImGui::Checkbox("Activate", &activate);

	return false;
}

void FlyingSpawner::FromJson(json & value)
{
	this->Enemy::FromJson(value);

	value["Time to spawn"] >> spawn_time;
	value["Time between shoot"] >> flying_shoot_timer;
	value["Flying Bullet speed"] >> flying_bullet_speed;
}

void FlyingSpawner::ToJson(json & value)
{
	this->Enemy::ToJson(value);

	value["Time to spawn"] << spawn_time;
	value["Time between shoot"] << flying_shoot_timer;
	value["Flying Bullet speed"] << flying_bullet_speed;

}
