#include "Components/AEXTransformComp.h"
#include "Enemy.h"
#include "Components/SpineAnimation.h"

namespace AEX
{
	Enemy::Enemy()
	{
		TimeFirstShot = 0.0f;
		//Radius_to_attack = 8.0f;
		//Radius_to_STOP_attacking = 8.0f;
		//Radius_to_activate = 8.0f;
		//circle_to_attack.numVertices = 25;
		//circle_to_STOP_attacking.numVertices = 25;
		TimetoDesactivate = 1.0f; // 1 second to deasctivate
	}
	void Enemy::Initialize()
	{
		EnemyTransform = mOwner->GetComp<TransformComp>();
		
	}

	void Enemy::Update()
	{	
		if (doneOnce == false)
		{
			GameObject * found = aexScene->FindObjectInANYSpace("player");
			ASSERT(found, "The gameobject with name player was not found")
			PlayerTransform = found->GetComp<TransformComp>();
			EnemyTransform = mOwner->GetComp<TransformComp>();
			initpos = EnemyTransform->GetPosition3D();
			doneOnce = true;
			time = 0.0f;
			just_got_out_of_screen = true;
			desactivationprocess = false;
			going_back_home = false;
		}

		time += (f32)aexTime->GetFrameTime();

		//ENEMY ACTIVATE IF THEY ARE IN CAMERA
		if (mOwner->IsGameObjectInScreen())
		{
			enemy_activator = true;
			desactivationprocess = false;
			going_back_home = false;
			just_got_out_of_screen = true;
		}
		if (mOwner->IsGameObjectInScreen() == false && enemy_activator && just_got_out_of_screen )
		{
			desactivationprocess = true;

			time_when_desactivation_started = time;
			just_got_out_of_screen = false;
		}
		if (desactivationprocess)
		{
			if (time - time_when_desactivation_started > TimetoDesactivate)
			{
				enemy_activator = false;
				desactivationprocess = false; // already desactivated
				just_got_out_of_screen = true;
				going_back_home = true;
			}
		}

		if (enemy_activator == false)
			Shoot_activator = false;

		if (time >= TimeFirstShot && enemy_activator)
			Shoot_activator = true;

		if (mLives && ActivateDamage)
		{
			Renderable* renderable = mOwner->GetComp<Renderable>();
			damageTime += (f32)aexTime->GetFrameTime();
			f32 redVariance = sin(12.5f * damageTime);
			if (redVariance > 0)
			{
				renderable->mColor[1] = 1.0f - redVariance;
				renderable->mColor[2] = 1.0f - redVariance;
			}
			else
			{
				renderable->mColor[1] = 1.0f;
				renderable->mColor[2] = 1.0f;
				damageTime = 0.0f;
				ActivateDamage = false;
			}
		}

	//	// go back to the initial pos
	//	if (going_back_home)
	//	{
	//		EnemyTransform->SetPosition3D(EnemyTransform->GetPosition3D() += Movement(EnemyTransform->GetPosition3D(), initpos, EnemySpeed/3));
	//		if (abs((initpos - EnemyTransform->GetPosition3D()).Length()) < 0.1f)
	//			going_back_home = false;
 	//	}
		//enemy to activate
		//if (StaticPointToStaticCircle(&AEVec2(PlayerTransform->GetPosition3D().x, PlayerTransform->GetPosition3D().y), &AEVec2(EnemyTransform->GetPosition3D().x,
		//	EnemyTransform->GetPosition3D().y), Radius_to_activate))
		//{
		//	enemy_activator = true;
		//}
		//else
		//	enemy_activator = false;
		//
		////Enemy to attack
		//if (StaticPointToStaticCircle(&AEVec2(PlayerTransform->GetPosition3D().x, PlayerTransform->GetPosition3D().y), &AEVec2(EnemyTransform->GetPosition3D().x,
		//	EnemyTransform->GetPosition3D().y), Radius_to_attack))
		//{
		//	Shoot_activator = true;
		//}
		//
		//if (!StaticPointToStaticCircle(&AEVec2(PlayerTransform->GetPosition3D().x, PlayerTransform->GetPosition3D().y), &AEVec2(EnemyTransform->GetPosition3D().x,
		//	EnemyTransform->GetPosition3D().y), Radius_to_STOP_attacking))
		//{
		//	Shoot_activator = false;
		//}
	}

	void Enemy::EditorUpdate()
	{
		//circle_to_attack.color = AEVec4(240.0f/255, 0.0f, 40.f/255, 1.0f);
		//circle_to_attack.radius = Radius_to_attack * 2;
		//circle_to_attack.pos = AEVec2(mOwner->GetComp<TransformComp>()->GetPosition3D().x, mOwner->GetComp<TransformComp>()->GetPosition3D().y);
		//circle_to_attack.numVertices = Radius_to_attack * 13;
		//GfxMgr->DrawCircle(circle_to_attack);
		//
		//if (Radius_to_STOP_attacking > Radius_to_attack) // radius to stop attacking should always be bigger
		//{
		//	circle_to_STOP_attacking.color = AEVec4(.0f, 150.f/255, 128.f / 255, 1.0f);
		//	circle_to_STOP_attacking.radius = Radius_to_STOP_attacking * 2;
		//	circle_to_STOP_attacking.pos = AEVec2(mOwner->GetComp<TransformComp>()->GetPosition3D().x, mOwner->GetComp<TransformComp>()->GetPosition3D().y);
		//	circle_to_STOP_attacking.numVertices = Radius_to_STOP_attacking * 13;
		//	GfxMgr->DrawCircle(circle_to_STOP_attacking);
		//}
		//else
		//	Radius_to_STOP_attacking = Radius_to_attack; // if not radius to stop attacking is provided
		//
		//if (Radius_to_activate > Radius_to_attack) 
		//{
		//	circle_to_active.color = AEVec4(.0f, 0.f, 240.f / 255, 1.0f);
		//	circle_to_active.radius = Radius_to_activate * 2;
		//	circle_to_active.pos = AEVec2(mOwner->GetComp<TransformComp>()->GetPosition3D().x, mOwner->GetComp<TransformComp>()->GetPosition3D().y);
		//	circle_to_active.numVertices = Radius_to_activate * 13;
		//	GfxMgr->DrawCircle(circle_to_active);
		//}
		//else
		//	Radius_to_activate = Radius_to_attack; // if not active radius is provided
	}

	AEVec3 Enemy::Movement(AEVec3 EnemyPosition, AEVec3 PlayerPosition, f32 Speed)
	{
		AEVec3 Distance = PlayerPosition - EnemyPosition;
		f32 length = sqrt(Distance.x * Distance.x + Distance.y * Distance.y);
		Distance /= length;
		
		if (length <= Speed || length == 0.0f)
			return PlayerPosition;

		return Distance * Speed;
	}


	void Enemy::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);
		value["Enemy Speed"] >> EnemySpeed;

		if (value["Distance Enemy Attack Player"] != nullptr)
		value["Distance Enemy Attack Player"] >> DistanceEnemyAttack;

		if (value["Distance Enemy Not Attack Player"] != nullptr)
			value["Distance Enemy Not Attack Player"] >> DistanceEnemyNotAttack;

		if (value["Enemy Lives"] != nullptr)
			value["Enemy Lives"] >> mLives;

		if (value["Enemy Bullet Speed"] != nullptr)
			value["Enemy Bullet Speed"] >> EnemyBulletSpeed;
		if (value["First shot time"] != nullptr)
			value["First shot time"] >> TimeFirstShot;
		if (value["Time to desactivate"] != nullptr)
			value["Time to desactivate"] << TimetoDesactivate;

		//if (value["Radius to attack"] != nullptr)
		//	value["Radius to attack"] >> Radius_to_attack;
		//
		//if (value["Radius to STOP attack"] != nullptr)
		//	value["Radius to STOP attack"] >> Radius_to_STOP_attacking;
		//
		//if (value["Radius to activate"] != nullptr)
		//	value["Radius to activate"] >> Radius_to_activate;
	}

	void Enemy::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);
		value["Enemy Speed"] << EnemySpeed;
		value["Enemy Bullet Speed"] << EnemyBulletSpeed; 
		value["Enemy Lives"] << mLives;
		value["Distance Enemy Attack Player"] << DistanceEnemyAttack;
		value["Distance Enemy Not Attack Player"] << DistanceEnemyNotAttack;
		value["First shot time"] << TimeFirstShot;
		value["Time to desactivate"] << TimetoDesactivate;

		//value["Radius to attack"] << Radius_to_attack;
		//value["Radius to STOP attack"] << Radius_to_STOP_attacking;
		//value["Radius to activate"] << Radius_to_activate;
	}
}