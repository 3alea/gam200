#include "EnemyMitosis.h"
#include "AI/Path2D.h"
#include "Bullets/BulletLogic.h"
#include "Composition/AEXSpace.h"
#include "Components/SpineAnimation.h"
#include "spine/AnimationState.h"
#include "EnemyBullet.h"
#include "Enemy/WaveSystem/Spawner.h"

namespace AEX
{
	Flying::Flying(const char * name) : State(name)
	{
	}

	void Flying::LogicEnter()
	{
		Initialized = false;
	}

	void Flying::LogicUpdate()
	{
		if (!Initialized)
		{
			mMitosis = static_cast<EnemyMitosis*>(mActor);
			mPath = mMitosis->mOwner->GetComp<PathComponent>();
			timer = 0.0f;
			Initialized = true;
		}
		if (mMitosis->Shoot_activator)
		{
			mOwnerStateMachine->ChangeState("ShootingAtPlayer");
		}
	}

	ShootingAtPlayer::ShootingAtPlayer(const char * name) : State(name)
	{
		i = 0;
	}

	void ShootingAtPlayer::LogicEnter()
	{
		mMitosis = static_cast<EnemyMitosis*>(mActor);
	}

	void ShootingAtPlayer::LogicUpdate()
	{
		if (mMitosis->mLives <= 0)
		{
			if(mMitosis->shouldSplit)
				mOwnerStateMachine->ChangeState("Separate");
			else
				mOwnerStateMachine->ChangeState("Death");
			return;
		}

		if (!mMitosis)
		{
			mMitosis = static_cast<EnemyMitosis *>(mActor);
		}

		mMitosis->spineAnimation->ChangeAnimation(0, "Idle", true, mMitosis->animationSpeed, 0.3f);

		float distance = fabs((mMitosis->PlayerTransform->mLocal.mTranslationZ - mMitosis->EnemyTransform->mLocal.mTranslationZ).Length());

		if (!mMitosis->Shoot_activator)
		{
			mOwnerStateMachine->ChangeState("Flying");
		}
		else
		{
			if (FirstShot)
			{
				mMitosis->CurrentTime = (f32)mMitosis->TimeFirstShot;
				FirstShot = false;
			}
				
			if (mMitosis->CurrentTime <= 0)
			{
				EnemyBullet(mMitosis->EnemyTransform, i, mMitosis->EnemyBulletSpeed,"mitosis", mMitosis->PlayerTransform);

				i++;

				mMitosis->CurrentTime = mMitosis->mTimeBetweenShots;

			}

			else
				mMitosis->CurrentTime -= (f32)aexTime->GetFrameTime();
		}
	}

	Separate::Separate(const char * name) : State(name)
	{

	}

	void Separate::LogicEnter()
	{
		mMitosis = static_cast<EnemyMitosis*>(mActor);
	}

	void Separate::LogicUpdate()
	{

		audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\EnemyMitosis\\MitosisDivide.wav"), Voice::SFX);

		if (!mMitosis)
		{
			mMitosis = static_cast<EnemyMitosis*>(mActor);
		}

		mMitosis->spineAnimation->ChangeAnimation(0, "Before Separation", false, mMitosis->animationSpeed, 0.3f);
		if (mMitosis->spineAnimation->GetAnimState()->getCurrent(0)->isComplete())
		{
			// Create another mitosis
			GameObject * otherMitosis = aexScene->FindSpace("Main")->InstantiateArchetype("mitosis", mMitosis->t->mLocal.mTranslationZ);

			// Sanity check
			if (otherMitosis == nullptr)
				ASSERT(0, "The archetype could not be instantiated.")
			
			// Update the transform scales
			mMitosis->t->mLocal.mScale = mMitosis->divisionScale;
			otherMitosis->GetComp<TransformComp>()->mLocal.mScale = mMitosis->divisionScale;

			// Update the collider scale
			mMitosis->bc->mScale = mMitosis->divisionScale * 1.25f;
			otherMitosis->GetComp<BoxCollider>()->mScale = mMitosis->divisionScale * 1.25f;

			// Update the animation scales
			otherMitosis->GetComp<SpineAnimationComp>()->Size = mMitosis->divisionScale / 100.f;
			mMitosis->spineAnimation->Size = mMitosis->divisionScale / 100.f;
			otherMitosis->GetComp<SpineAnimationComp>()->ReloadSpineData();
			mMitosis->spineAnimation->ReloadSpineData();

			// Update the state of the other mitosis
			otherMitosis->GetComp<EnemyMitosis>()->mBrain[0].ChangeState("SpawnLeft");

			// Add a velocity
			mMitosis->rb->AddForce(mMitosis->seperationForce);
			otherMitosis->GetComp<Rigidbody>()->AddForce(-mMitosis->seperationForce);

			// Make sure the new mitosis we spawn has at least 1 life
			mMitosis->mLives = 1;

			// Update this mitosis' state
			mOwnerStateMachine->ChangeState("SpawnRight");

			mMitosis->shouldSplit = false;
			otherMitosis->GetComp<EnemyMitosis>()->shouldSplit = false;
		}
	}

	SpawnLeft::SpawnLeft(const char * name) : State(name)
	{
	}

	void SpawnLeft::LogicEnter()
	{
		mMitosis = static_cast<EnemyMitosis*>(mActor);
		mMitosis->spineAnimation->ChangeAnimation(0, "Separation L", false, mMitosis->animationSpeed, 0.3f);
	}

	void SpawnLeft::LogicUpdate()
	{
		if (!mMitosis)
		{
			mMitosis = static_cast<EnemyMitosis*>(mActor);
		}

		mMitosis->spineAnimation->ChangeAnimation(0, "Separation L", false, mMitosis->animationSpeed, 0.3f);

		if (mMitosis->spineAnimation->GetAnimState()->getCurrent(0)->isComplete())
		{
			mMitosis->mOwner->GetComp<EnemyMitosis>()->mBrain[0].ChangeState("ShootingAtPlayer");
		}
	}

	SpawnRight::SpawnRight(const char * name) : State(name)
	{
	}

	void SpawnRight::LogicEnter()
	{
		mMitosis = static_cast<EnemyMitosis*>(mActor);
		mMitosis->spineAnimation->ChangeAnimation(0, "Separation R", false, mMitosis->animationSpeed, 0.3f);
	}

	void SpawnRight::LogicUpdate()
	{
		if (!mMitosis)
		{
			mMitosis = static_cast<EnemyMitosis*>(mActor);
		}

		mMitosis->spineAnimation->ChangeAnimation(0, "Separation R", false, mMitosis->animationSpeed, 0.3f);

		if (mMitosis->spineAnimation->GetAnimState()->getCurrent(0)->isComplete())
		{
			mMitosis->mOwner->GetComp<EnemyMitosis>()->mBrain[0].ChangeState("ShootingAtPlayer");
		}
	}

	Death::Death(const char * name) : State(name)
	{
		mMitosis = static_cast<EnemyMitosis*>(mActor);
	}

	void Death::LogicEnter()
	{
		mMitosis = static_cast<EnemyMitosis*>(mActor);
		mMitosis->spineAnimation->ChangeAnimation(0, "Death", false, mMitosis->animationSpeed, 0.3f);
	}

	void Death::LogicUpdate()
	{
		if (!mMitosis)
		{
			mMitosis = static_cast<EnemyMitosis*>(mActor);
		}

		mMitosis->spineAnimation->ChangeAnimation(0, "Death", false, mMitosis->animationSpeed, 0.3f);

		if (mMitosis->spineAnimation->GetAnimState()->getCurrent(0)->isComplete())
		{
			// Added by Miguel for wave system
			if (mMitosis->mOwner->mSpawnerOwner != nullptr)
				mMitosis->mOwner->mSpawnerOwner->EnemyKilled();

			if(mMitosis->spawner)
				mMitosis->spawner->EraseMitosis(mMitosis);
			mMitosis->GetOwner()->GetParentSpace()->DestroyObject(mMitosis->GetOwner());
		}
	}

	EnemyMitosis::EnemyMitosis()
	{
		spawner = NULL;
	}

	void EnemyMitosis::Initialize()
	{
		this->Enemy::Initialize();
		SM_Init();

		// To make sure json values work
		sepTemp[0] = seperationForce.x;
		sepTemp[1] = seperationForce.y;
		divisionTemp[0]	= divisionScale.x;
		divisionTemp[1]	= divisionScale.y;

		enemy_activator = false;
		rb = mOwner->GetComp<Rigidbody>();
		t = mOwner->GetComp<TransformComp>();
		bc = mOwner->GetComp<BoxCollider>();
		spineAnimation = mOwner->GetComp<SpineAnimationComp>();
	}

	void EnemyMitosis::Update()
	{
		if (mMitosisCount > 0)
		{
			enemy_activator = true;
		}

		this->Enemy::Update();
		Actor::Update();
	}

	void EnemyMitosis::ShutDown()
	{

	}

	bool EnemyMitosis::OnGui()
	{
		bool isChanged = false;

		if (ImGui::InputInt("Lives", &mLives))
		{
			isChanged = true;
		}

		if (ImGui::InputFloat("Bullet Speed", &EnemyBulletSpeed))
		{
			isChanged = true;
		}

		if (ImGui::InputScalar("Time Between Shots", ImGuiDataType_Float, &mTimeBetweenShots, &bulletSpeedStep, &bulletSpeedStepBig))
		{
			isChanged = true;
		}

		if (ImGui::InputDouble("Time First Shot", &TimeFirstShot))
		{
			isChanged = true;
		}

		if (ImGui::InputFloat2("Force at separation", sepTemp))
		{
			seperationForce.x = sepTemp[0];
			seperationForce.y = sepTemp[1];
			isChanged = true;
		}

		if (ImGui::InputFloat2("New mitosis scales", divisionTemp))
		{
			divisionScale.x = divisionTemp[0];
			divisionScale.y = divisionTemp[1];
			isChanged = true;
		}

		if (ImGui::InputFloat("Enemy Speed", &EnemySpeed))
		{
			isChanged = true;
		}

		if (ImGui::InputFloat("Animation speed", &animationSpeed))
		{
			isChanged = true;
		}

		if (ImGui::BeginCombo("Ease Function", mEaseFuncName))
		{
			if (ImGui::Selectable("EaseLinear"))
			{
				mEaseFunction = &EaseLinear;
				mEaseFuncName = "EaseLinear";
				isChanged = true;
			}
			if (ImGui::Selectable("EaseInQuad"))
			{
				mEaseFunction = &EaseInQuad;
				mEaseFuncName = "EaseInQuad";
				isChanged = true;
			}
			if (ImGui::Selectable("EaseOutQuad"))
			{
				mEaseFunction = &EaseOutQuad;
				mEaseFuncName = "EaseOutQuad";
				isChanged = true;
			}
			if (ImGui::Selectable("EaseInOutQuad"))
			{
				mEaseFunction = &EaseInOutQuad;
				mEaseFuncName = "EaseInOutQuad";
				isChanged = true;
			}

			ImGui::EndCombo();
		}

		return isChanged;
	}

	void EnemyMitosis::SM_Init()
	{
		State* flyingState = new Flying("Flying");
		ShootingAtPlayer* shootingState = new ShootingAtPlayer("ShootingAtPlayer");

		mBrain[0].AddState(new Flying("Flying"));
		mBrain[0].AddState(new ShootingAtPlayer("ShootingAtPlayer"));
		mBrain[0].AddState(new Separate("Separate"));
		mBrain[0].AddState(new SpawnLeft("SpawnLeft"));
		mBrain[0].AddState(new SpawnRight("SpawnRight"));
		mBrain[0].AddState(new Death("Death"));

		mBrain[0].SetInitState("Flying");
		mBrain[0].ChangeState("Flying");
		mBrain[0].mCurrentState = mBrain[0].mInitialState;
	}

	void EnemyMitosis::FromJson(json & value)
	{
		this->Enemy::FromJson(value);

		value["Time Between Shots"] >> mTimeBetweenShots;
		value["Force at separation1"] >> seperationForce.x;
		value["Force at separation2"] >> seperationForce.y;
		value["Seperated scale"] >> divisionScale;
		value["Animation speed"] >> animationSpeed;
		value["First shot time"] >> TimeFirstShot;
	}

	void EnemyMitosis::ToJson(json & value)
	{
		this->Enemy::ToJson(value);

		value["Time Between Shots"] << mTimeBetweenShots;
		value["Seperated scale"] << divisionScale;
		value["Force at separation1"] << seperationForce.x;
		value["Force at separation2"] << seperationForce.y;
		value["First shot time"] << TimeFirstShot;
		value["Animation speed"] << animationSpeed;
	}

	void EnemyMitosis::OnCollisionEvent(const CollisionEvent& collision)
	{
		if (BulletLogic * logic = collision.otherObject->GetComp<BulletLogic>())
		{
			if (Shoot_activator)
			{
				mLives--;
			}
		}
	}
}