#include "EnemyFlying.h"
#include "Player/Stats/PlayerStats.h"
#include "Components\SpineAnimation.h"
#include <spine\AnimationState.h>
#include <spine\Skeleton.h>
#include "src/Engine/Editor/Editor.h"
#include "Enemy/WaveSystem/Spawner.h"

namespace AEX
{
	SM_FlyingEnemy_Resting::SM_FlyingEnemy_Resting(const char * name) : State(name)
	{

	}

	void SM_FlyingEnemy_Resting::LogicUpdate()
	{
	}

	SM_FlyingEnemy_Moving::SM_FlyingEnemy_Moving(const char * name) : State(name)
	{
		Initialized = false;
	}

	void SM_FlyingEnemy_Moving::LogicEnter()
	{
		save = static_cast<EnemyFlying*>(mActor);
		save->spineAnimation = save->GetOwner()->GetComp<SpineAnimationComp>();
	}

	void SM_FlyingEnemy_Moving::LogicUpdate()
	{
		EnemyFlying * save = static_cast<EnemyFlying*>(mActor);

		if (!Initialized)
		{
			//mPath = save->mOwner->GetComp<PathComponent>();
			Initialized = true;
			time = 0.0f;
		}

		//AEVec2 & pos = mPath->SampleTime(time);
		//save->EnemyTransform->SetPosition3D(AEVec3(pos.x, pos.y, save->EnemyTransform->mLocal.mTranslationZ.z));

		//if (time >= mPath->Duration())
		//{
			//time = 0.0f;
		//}
		//time += (f32)aexTime->GetFrameTime();

		//f32 distance = fabs((save->PlayerTransform->mLocal.mTranslationZ - save->EnemyTransform->mLocal.mTranslationZ).Length());

		if (save->Shoot_activator)
		{
			mOwnerStateMachine->ChangeState("SM_FlyingEnemy_Shooting");
		}
	}

	SM_FlyingEnemy_Shooting::SM_FlyingEnemy_Shooting(const char * name) : State(name)
	{
		i = 0;
		rotated = false;
	}

	void SM_FlyingEnemy_Shooting::LogicEnter()
	{
		save = static_cast<EnemyFlying*>(mActor);
	}

	void SM_FlyingEnemy_Shooting::LogicUpdate()
	{
		// Change animation to follow + shoot
		if (save->mOwner->GetComp<SpineAnimationComp>())
		{
			save->shootDelay -= (f32)aexTime->GetFrameTime();
			save->mOwner->GetComp<SpineAnimationComp>()->ChangeAnimation(0, "Aim", false, 1.f);
			if(save->shootDelay <= 0.f)
				save->mOwner->GetComp<SpineAnimationComp>()->ChangeAnimation(1, "Shoot", true, 1.f / (2.f * (f32)save->StartTimeBetweenShots));
		}

		if (!save->Shoot_activator)
		{
			mOwnerStateMachine->ChangeState("SM_FlyingEnemy_Moving");
		}
		else
		{
			if (FirstShot)
			{
				save->TimeBetweenShots = save->TimeFirstShot;
				FirstShot = false;
			}

			Direction = AEVec2(save->PlayerTransform->mLocal.mTranslationZ.x, save->PlayerTransform->mLocal.mTranslationZ.y) -
				AEVec2(save->EnemyTransform->mLocal.mTranslationZ.x, save->EnemyTransform->mLocal.mTranslationZ.y);

			save->EnemyTransform->mLocal.mOrientation = Direction.GetAngle();

			save->EnemyTransform->mLocal.mTranslationZ += save->Movement(save->EnemyTransform->mLocal.mTranslationZ, AEVec3(save->PlayerTransform->mLocal.mTranslationZ.x, save->PlayerTransform->mLocal.mTranslationZ.y + 2.0f, save->PlayerTransform->mLocal.mTranslationZ.z), save->EnemySpeed);

			if (save->TimeBetweenShots <= 0)
			{
				EnemyBullet(save->EnemyTransform, i, save->EnemyBulletSpeed,"flying", save->PlayerTransform);
				
				i++;

				save->TimeBetweenShots = save->StartTimeBetweenShots;
			}

			else
				save->TimeBetweenShots -= (f32)aexTime->GetFrameTime();
		}
		// If lives reach 0, die
		if (save->mLives <= 0)
		{
			
			mOwnerStateMachine->ChangeState("SM_FlyingEnemy_Die");
		}
	}

	SM_FlyingEnemy_Die::SM_FlyingEnemy_Die(const char * name) : State(name)
	{
		save = static_cast<EnemyFlying*>(mActor);
	}

	void SM_FlyingEnemy_Die::LogicEnter()
	{
		save = static_cast<EnemyFlying*>(mActor);
	}

	void SM_FlyingEnemy_Die::LogicUpdate()
	{		

		if (save->mOwner->GetComp<SpineAnimationComp>())
		{
			save->mOwner->GetComp<SpineAnimationComp>()->ChangeAnimation(1, "Death", false, 1.f);
			if (save->mOwner->GetComp<SpineAnimationComp>()->GetAnimState()->getCurrent(1)->isComplete())
			{
				if (save->GetOwner()->mSpawnerOwner != nullptr)
				{
					save->GetOwner()->mSpawnerOwner->EnemyKilled();
				}
				save->GetOwner()->GetParentSpace()->DestroyObject(save->GetOwner());
			}
		}
			
		
	}

	void EnemyFlying::SM_Init()
	{
		mBrain[0].AddState(new SM_FlyingEnemy_Resting("SM_FlyingEnemy_Resting"));
		mBrain[0].AddState(new SM_FlyingEnemy_Moving("SM_FlyingEnemy_Moving"));
		mBrain[0].AddState(new SM_FlyingEnemy_Shooting("SM_FlyingEnemy_Shooting"));
		mBrain[0].AddState(new SM_FlyingEnemy_Die("SM_FlyingEnemy_Die"));

		mBrain[0].SetInitState("SM_FlyingEnemy_Moving");
		mBrain[0].ChangeState(mBrain[0].mInitialState);
		mBrain[0].mCurrentState = mBrain[0].mInitialState;
	}

	void EnemyFlying::Initialize()
	{
		this->Enemy::Initialize();

		enemy_activator = false;
		mOnCinematic = false;

		SM_Init();
		if (mOwner->GetComp<SpineAnimationComp>() == nullptr)
			init = false;
		else
			shootDelay = mOwner->GetComp<SpineAnimationComp>()->startTime;
	}
	void EnemyFlying::Update()
	{
		this->Enemy::Update();
		
		//if(going_back_home)
		//	std::cout << "aa:" << EnemyTransform->mWorld.mTranslationZ.x << " , " << EnemyTransform->mWorld.mTranslationZ.y << std::endl;
		if (mOnCinematic == false && enemy_activator)
		{
			Actor::Update();
		}
		if (init == false)
		{
			if(mOwner->GetComp<SpineAnimationComp>())
				shootDelay = mOwner->GetComp<SpineAnimationComp>()->startTime;
			init = true;
		}
	}
	bool EnemyFlying::OnGui()
	{
		bool isChanged = false;

		if (ImGui::InputInt("Lives", &mLives))
		{
			isChanged = true;
		}

		if (ImGui::InputDouble("Time Between Shots", &StartTimeBetweenShots))
		{
			isChanged = true;
		}

		if (ImGui::InputDouble("Time First Shot", &TimeFirstShot))
		{
			isChanged = true;
		}

		//if (ImGui::InputFloat("Dist to attack", &DistanceEnemyAttack))
		//{
		//	isChanged = true;
		//}

		//if (ImGui::InputFloat("Distance not attack player", &DistanceEnemyNotAttack))
		//{
		//	isChanged = true;
		//}

		if (ImGui::InputFloat("Enemy Speed", &EnemySpeed))
		{
			isChanged = true;
		}

		if (ImGui::InputFloat("Enemy Bullet Speed", &EnemyBulletSpeed))
		{
			isChanged = true;
		}
		ImGui::InputDouble("Time First Shot", &TimeFirstShot);
		//ImGui::InputFloat("Dist to activate", &Radius_to_activate);
		//ImGui::InputFloat("Dist to attack", &Radius_to_attack);
		//ImGui::InputFloat("Distance STOP attack player", &Radius_to_STOP_attacking);

		return isChanged;
	}

	void EnemyFlying::OnCollisionEvent(const CollisionEvent & collision)
	{
		if (enemy_activator)
		{
			if (collision.otherObject->GetBaseNameString() == "player")
			{
				PlayerStats* stats = collision.otherObject->GetComp<PlayerStats>();

				if (!stats->isInvincible)
				{
					stats->lives--;
					stats->isHit = true;
				}
			}
		}
	}

	void EnemyFlying::FromJson(json & value)
	{
		this->Enemy::FromJson(value);
		value["Time Between Shots"] >> TimeBetweenShots;
		value["Start Time Between Shots"] >> StartTimeBetweenShots;
		//value["First shot time"] >> TimeFirstShot;
	}
	void EnemyFlying::ToJson(json & value)
	{
		this->Enemy::ToJson(value);

		value["Time Between Shots"] << TimeBetweenShots;
		value["Start Time Between Shots"] << StartTimeBetweenShots;
		value["First shot time"] << TimeFirstShot;
	}
}