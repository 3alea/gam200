#pragma once

#include "Enemy.h"
#include "EnemyBullet.h"
#include <string>

namespace AEX
{
	class EnemyGrounded;

	struct SM_GroundedEnemy_Resting : public State
	{
		SM_GroundedEnemy_Resting(const char * name);

		void LogicUpdate();

		PathComponent * mPath;

		f32 time;

		bool Initialized;

		AEVec2 pos;

		EnemyGrounded * save;
	};

	struct SM_GroundedEnemy_Moving : public State
	{
		SM_GroundedEnemy_Moving(const char * name);

		void LogicUpdate();

		EnemyGrounded * save;
	};

	struct SM_GroundedEnemy_Aiming : public State
	{
		SM_GroundedEnemy_Aiming(const char * name);

		void LogicUpdate();

		EnemyGrounded * save;
	};

	struct SM_GroundedEnemy_Shooting : public State
	{
		SM_GroundedEnemy_Shooting(const char * name);

		void LogicUpdate();

		unsigned i;

		bool FirstShot = true;

		EnemyGrounded * save;
	};

	struct SM_GroundedEnemy_Unaiming : public State
	{
		SM_GroundedEnemy_Unaiming(const char * name);

		void LogicUpdate();

		EnemyGrounded * save;
	};

	struct SM_GroundedEnemy_Die : public State
	{
		SM_GroundedEnemy_Die(const char * name);

		void LogicEnter();

		void LogicUpdate();

		EnemyGrounded * save;

	private:
		f32 deathTimer = 0.0f;
		f32 deathTime = 1.0f;
	};

	class EnemyGrounded : public Enemy, public Actor
	{
		AEX_RTTI_DECL(EnemyGrounded, Enemy)

	public:

		void Initialize();
		void Update();

		bool OnGui();

		void SM_Init();

		virtual void OnCollisionEvent(const CollisionEvent& collision);

		void FromJson(json & value);
		void ToJson(json & value);

		f64 StartTimeBetweenShots;

		f64 TimeBetweenShots;


		int ShotCountToRecharge;
		int SaveShotCountToRecharge;

		float myOrientation = 0.f;
		
		f64 EnemyTimeToRecharge;
		f64 SaveEnemyTimeToRecharge;

		enum EnemyState{eResting, eMoving, eAiming, eShooting, eUnaiming, eDying};

		EnemyState myState;
	};
}