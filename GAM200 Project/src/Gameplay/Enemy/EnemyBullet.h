#pragma once

#include "Enemy.h"
#include "Audio\AudioManager.h"
#include <string>


namespace AEX
{
	class EnemyBullet: public GameObject
	{
		public:
		
		EnemyBullet() = default;
		~EnemyBullet() = default;

		EnemyBullet(TransformComp*, const unsigned, f32, const char *, TransformComp * PlayerTransform_, int what_bugger = 0);

		 void FromJson(json & value);
		 void ToJson(json & value);

		private:

		std::string name;
		TResource<Texture>* BulletSprite = aexResourceMgr->getResource<Texture>("data\\Textures\\enemybullet.png");

	};

	class BulletComp : public Enemy
	{
		AEX_RTTI_DECL(BulletComp, Enemy)

	public:
		BulletComp(std::string enemyname);
		void Initialize();
		void Update();
		
		virtual void OnCollisionEvent(const CollisionEvent& collision);

		void FromJson(json & value);
		void ToJson(json & value);

		int what_bugger = 0;
		std::string Enemyname;

	private:

		bool Initialized;
		AEVec2 BulletDir;	

	};
}