#pragma once
#include "Enemy.h"
#include "EnemySpawner.h"
#include "AI/Raycast.h"

class SpineAnimationComp;

namespace AEX
{
	class PathComponent;
	class EnemyMitosis;

	struct Flying : public State
	{
		Flying(const char * name);

		void LogicEnter();
		void LogicUpdate();

		PathComponent * mPath;
		TransformComp * mTransform;
		EnemyMitosis * mMitosis;

		Path2D MitosisPath;

		bool Initialized = false;

		f32 time;

		AEVec2 pos;

		f32 timer = 0.0f;
	};

	struct ShootingAtPlayer : public State
	{
		ShootingAtPlayer(const char * name);

		void LogicEnter();
		void LogicUpdate();

		TransformComp * mTransform;
		EnemyMitosis * mMitosis;
		bool canShoot = true;
		unsigned i;


		bool FirstShot = true;
	};

	struct Separate : public State
	{
		Separate(const char * name);

		void LogicEnter();
		void LogicUpdate();

		EnemyMitosis * mMitosis;
	};

	struct SpawnLeft : public State
	{
		SpawnLeft(const char * name);

		void LogicEnter();
		void LogicUpdate();

		EnemyMitosis * mMitosis;
	};

	struct SpawnRight : public State
	{
		SpawnRight(const char * name);

		void LogicEnter();
		void LogicUpdate();

		EnemyMitosis * mMitosis;
	};

	struct Death : public State
	{
		Death(const char * name);

		void LogicEnter();
		void LogicUpdate();

		EnemyMitosis * mMitosis;
	};

	class EnemyMitosis : public Enemy, public Actor
	{
		AEX_RTTI_DECL(EnemyMitosis, Enemy);

	public:
		EnemyMitosis();
		// Logic comp functions
		void Initialize();
		void Update();				// Here we will call the update of actor
		void ShutDown();

		bool OnGui();

		// Add all the states and state machines
		void SM_Init();

		void OnCollisionEvent(const CollisionEvent& collision);

		void FromJson(json & value);
		void ToJson(json & value);

		// Useful comps
		Rigidbody* rb;
		TransformComp* t;
		BoxCollider* bc;

		EaseFunc mEaseFunction = EaseLinear;
		const char * mEaseFuncName = "EaseLinear";

		f32 mTimeBetweenShots = 1.0f;
		f32 CurrentTime = mTimeBetweenShots;
		f64 TimeFirstShot = 0.0f;
		float divisionTemp[2];
		AEVec2 divisionScale = { 0.7f, 0.9f };

		float sepTemp[2];
		AEVec2 seperationForce = { 1.f, 0.f };

		// Not to be serialized
		unsigned int mitosis_count = 0;
		unsigned int mMitosisCount = 0;
		f32 lerpTimer = 0.0f;
		EnemySpawner* spawner;
		int mitcount;
		bool shouldSplit = true;

		// Spine animation of the enemy
		SpineAnimationComp * spineAnimation;

		// Animation speeds
		float animationSpeed = 1.f;

	private:
		f32 bulletSpeedStep = 0.05f;
		f32 bulletSpeedStepBig = 0.5f;
	};
}