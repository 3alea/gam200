#pragma once
#include "Enemy.h"
#include "AI/Path2D.h"

// Forward declare spine animation class
class SpineAnimationComp;

namespace AEX
{
	class EnemyMitosis;
}

class EnemySpawner : public Enemy
{
	AEX_RTTI_DECL(EnemySpawner, Enemy)

public:
	EnemySpawner();
	~EnemySpawner();
	void Initialize();
	void Update();
	bool OnGui();

	void SpawnEnemy();
	void PathCreator();
	PathComponent* PickPath();
	void PathLogic();
	void CreateMitosis(GameObject* mitosis);
	void OnCollisionEvent(const CollisionEvent& collision);
	void EraseMitosis(EnemyMitosis* toDestroy);

	bool once_activated_for_ever_activated = false;
	void FromJson(json & value);
	void ToJson(json & value);
	int is_spawning = 0;
	int initlives;
	f32 mitosis_bullet_speed;
	f32	mitosis_shoot_timer;

	// Spawning offset
	AEVec2 offset;
	float offsetTemp[2];

	// Spawning force
	float spawnForce = 0.f;

	// Spawning time
	float passedTime = 0.f;
	float spawnTime = 0.f;
	bool canSpawn = false;

	std::vector<GameObject*> all_mitosis;
private:
	f32 time_to_spawn;
	int spawnerlives = 7;
	f32 current_time;
	int count;
	int selectedpath;
	f32 deathTimer = 0.0f;
	f32 deathTime = 1.0f;

	//Paths
	PathComponent* exitPath;
	PathComponent* upPath;
	PathComponent* rightPath;
	PathComponent* leftPath;
	PathComponent* verticalTOP;
	PathComponent* verticalBOT;

	SpineAnimationComp* spineAnimation;
};

class FlyingSpawner : public Enemy
{
public:
	FlyingSpawner();
	void Initialize();
	void Update();
	void Spawn();
	Transform* owner_transform;
	f32 timer;
	f32 spawn_time;
	bool OnGui();
	void FromJson(json & value);
	void ToJson(json & value);
	f32 flying_shoot_timer;
	f32 flying_bullet_speed;
	std::vector<GameObject*> all_flying;
	bool activate;
	int count = 0;
};