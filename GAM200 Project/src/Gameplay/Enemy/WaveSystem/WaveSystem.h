#pragma once

#include "Components/Logic.h"
#include <functional>


namespace AEX
{
	class WaveSystem : public LogicComp
	{
		AEX_RTTI_DECL(WaveSystem, LogicComp);

	public:

		virtual void Initialize();
		virtual void Update();
		virtual void Shutdown();

		virtual bool OnGui();

		virtual void FromJson(json & value);
		virtual void ToJson(json & value);

		int mEnemiesKilled = 0;

	protected:

		std::vector<std::function<void()>> mInitializers;
		std::vector<std::function<void()>> mUpdaters;

		virtual void OnAllWavesEnded();

	private:

		int mNumberOfWaves = 0;
		std::vector<int> mNumberOfEnemies;
		//std::vector<std::string> mArchetypeNames;

		int mCurrentWave = 0;
		std::vector<bool> mCalledInitializeOnce;

		int mPreviousWaveKillCount = 0;
	};


	// THIS IS A EXAMPLE OF HOW TO USE THE WAVE SYSTEM
	class ExampleWaveSystem : public WaveSystem
	{
		AEX_RTTI_DECL(ExampleWaveSystem, WaveSystem);

	public:

		void Initialize();
		void Update();
		void Shutdown();

		// YOU SHOULD BASICALLY CREATE ONE INITIALIZE AND UPDATE FUNCTION PER WAVE, (like the ones below)
		// AND THEN COPY THE INITIALIZE AND UPDATE THAT I AM DOING ABOVE

		void Wave1Initialize();
		void Wave1Update();

		void Wave2Initialize();
		void Wave2Update();

		virtual void OnAllWavesEnded();

	private:

	};
}