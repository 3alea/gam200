#include "BoilerRoomWaveSystem.h"
#include "Enemy/WaveSystem/Spawner.h"
#include "Composition/AEXScene.h"
#include "Components/SpineAnimation.h"
#include "spine/AnimationState.h"
#include "CameraLogic/CameraMovement.h"
#include "BoilerRoom/PuzzleLogic.h"


namespace AEX
{
	int BoilerRoomWaveSystem::mGroundedsOnScreen = 0;

	void BoilerRoomWaveSystem::Initialize()
	{
		mInitializers.push_back(std::bind(&AEX::BoilerRoomWaveSystem::Wave1Initialize, this));

		mUpdaters.push_back(std::bind(&AEX::BoilerRoomWaveSystem::Wave1Update, this));

		WaveSystem::Initialize();

		if (GameObject* p = mOwner->GetParentSpace()->FindObject("player"))
		{
			if (PlayerStats* s = p->GetComp<PlayerStats>())
			{
				s->lives = 0;
			}
		}

		mGroundedsOnScreen = 0;
	}
	void BoilerRoomWaveSystem::Update()
	{
		WaveSystem::Update();
	}
	void BoilerRoomWaveSystem::Shutdown()
	{

	}

	void BoilerRoomWaveSystem::Wave1Initialize()
	{
		GameObject * spawnerLeftObj = aexScene->FindObjectInANYSpace("SpawnerLeft");
		GameObject * spawnerRightObj = aexScene->FindObjectInANYSpace("SpawnerRight");

		if (spawnerLeftObj)
		{
			mSpawnerLeft = spawnerLeftObj->GetComp<Spawner>();
			mSpawnerLeftTr = spawnerLeftObj->GetComp<TransformComp>();
		}
		
		if (spawnerRightObj)
		{
			mSpawnerRight = spawnerRightObj->GetComp<Spawner>();
			mSpawnerRightTr = spawnerRightObj->GetComp<TransformComp>();
		}

		if (mSpawnerLeft)
			mSpawnerLeft->StopSpawning();

		if (mSpawnerRight)
			mSpawnerRight->StopSpawning();

		GameObject * leftLimitObj = aexScene->FindObjectInANYSpace("left wall");
		GameObject * rightLimitObj = aexScene->FindObjectInANYSpace("RightLimit");

		if (leftLimitObj)
			mLeftLimitTr = leftLimitObj->GetComp<TransformComp>();

		if (rightLimitObj)
			mRightLimitTr = rightLimitObj->GetComp<TransformComp>();

		GameObject * doorObj = aexScene->FindObjectInANYSpace("Door");

		if (doorObj)
			mDoorAnim = doorObj->GetComp<SpineAnimationComp>();

		if (mDoorAnim)
		{
			// Not sure why this works, but it works!
			//mDoorAnim->GetAnimState()->getCurrent(0)->setTrackTime(mDoorAnim->GetAnimState()->getCurrent(0)->getAnimationStart());
			//mDoorAnim->SetDt(0.0f);
			mDoorAnim->SetPauseState(true);
		}

		GameObject * camObj = aexScene->FindObjectInANYSpace("camera");

		if (camObj)
			mCamMovement = camObj->GetComp<CameraMovement>();
	}
	void BoilerRoomWaveSystem::Wave1Update()
	{
		if (mSpawnerLeft->mOwner->IsGameObjectInScreen())
			mSpawnerLeft->StopSpawning();
		else
			mSpawnerLeft->ContinueSpawning();

		if (mSpawnerRight->mOwner->IsGameObjectInScreen() || mGroundedsOnScreen >= 1)
			mSpawnerRight->StopSpawning();
		else
			mSpawnerRight->ContinueSpawning();
	}
	void BoilerRoomWaveSystem::OnAllWavesEnded()
	{
		//std::cout << "BOILER ROOM: All Waves Ended\n";
	}
}