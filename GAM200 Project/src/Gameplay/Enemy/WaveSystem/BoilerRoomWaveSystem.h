#pragma once

#include "Enemy/WaveSystem/WaveSystem.h"


class SpineAnimationComp;

namespace AEX
{
	class TransformComp;
	class Spawner;
	class CameraMovement;

	class BoilerRoomWaveSystem : public WaveSystem
	{
		AEX_RTTI_DECL(BoilerRoomWaveSystem, WaveSystem);

	public:

		void Initialize();
		void Update();
		void Shutdown();

		void Wave1Initialize();
		void Wave1Update();
		virtual void OnAllWavesEnded();

		TransformComp * mLeftLimitTr = nullptr;
		TransformComp * mRightLimitTr = nullptr;

		TransformComp * mSpawnerLeftTr = nullptr;
		TransformComp * mSpawnerRightTr = nullptr;

		Spawner * mSpawnerLeft = nullptr;
		Spawner * mSpawnerRight = nullptr;

		SpineAnimationComp * mDoorAnim = nullptr;
		CameraMovement * mCamMovement = nullptr;

		static int mGroundedsOnScreen;
	};
}