#include "WaveSystem.h"
#include <Imgui/imgui.h>
#include <iostream>


namespace AEX
{
	void WaveSystem::Initialize()
	{
		mEnemiesKilled = 0;										// Initialize the counter of enemies killed
		mPreviousWaveKillCount = 0;								// Initialize the counter of enemies killed until the previous wave
		mCurrentWave = 0;										// Initialize the counter to keep track of which wave we are in
		mCalledInitializeOnce.resize(mNumberOfWaves, false);	// Resize the bools to call initialize to the size we need, and initialize them to false
	}
	void WaveSystem::Update()
	{
		// If we have finished all the waves, stop updating
		if (mCurrentWave == mNumberOfWaves)
			return;

		if (mCalledInitializeOnce[0] == false)
		{
			if (mInitializers[0] != nullptr)
			{
				mInitializers[0]();						// Call Initialize of the first wave
				mCalledInitializeOnce[0] = true;		// Set its bool to true
			}
		}

		if (mCurrentWave + 1 == mNumberOfWaves && mEnemiesKilled == mNumberOfEnemies[mCurrentWave] + mPreviousWaveKillCount)
		{
			OnAllWavesEnded();
			mCurrentWave++;
			return;
		}

		// If the current number of enemies killed is equal to the number of enemies we have killed until this wave,
		// plus the number of enemies that we need to kill on this wave, and we have only done this once, and the current
		// wave is not the last.
		if (mEnemiesKilled == mNumberOfEnemies[mCurrentWave] + mPreviousWaveKillCount && mCurrentWave + 1 < mNumberOfWaves
			&& mCalledInitializeOnce[mCurrentWave + 1] == false)
		{
			if (mInitializers[mCurrentWave] != nullptr)
			{
				mPreviousWaveKillCount += mNumberOfEnemies[mCurrentWave];	// Update the counter of enemies killed until the last wave
				mCurrentWave++;												// Go to the next wave
				mInitializers[mCurrentWave]();								// Call Initialize on the next wave (actually, the current one, because we just updated the counter)
				mCalledInitializeOnce[mCurrentWave] = true;					// Set the boolean that ensures this is only done once to true
			}
		}

		if (mUpdaters[mCurrentWave] != nullptr)
			mUpdaters[mCurrentWave]();	// Call the Update function of the current wave
	}
	void WaveSystem::Shutdown()
	{

	}

	void WaveSystem::OnAllWavesEnded()
	{

	}

	bool WaveSystem::OnGui()
	{
		bool isChanged = false;

		if (ImGui::InputInt("Number Of Waves", &mNumberOfWaves))
		{
			// Resize the number of enemies vector to fit into the number of waves
			mNumberOfEnemies.resize(mNumberOfWaves, 0);
			mCalledInitializeOnce.resize(mNumberOfWaves, false);
			//mArchetypeNames.resize(mNumberOfWaves, "");

			isChanged = true;
		}

		ImGui::NewLine();

		for (int i = 0; i < mNumberOfWaves; i++)
		{
			if (ImGui::InputInt((std::string("Enemies In Wave ") + std::to_string(i + 1)).c_str(), &mNumberOfEnemies[i]))
			{
				isChanged = true;
			}

			/*char archName[64];
			strcpy_s(archName, mArchetypeNames[i].c_str());

			if (ImGui::InputText((std::string("Archetype Wave ") + std::to_string(i + 1)).c_str(), archName, IM_ARRAYSIZE(archName)))
			{
				mArchetypeNames[i] = archName;
				isChanged = true;
			}*/

			//ImGui::NewLine();
		}

		return isChanged;
	}

	void WaveSystem::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);

		value["Number Of Waves"] >> mNumberOfWaves;

		json & numberOfEnemiesVal = value["Number Of Enemies"];
		FOR_EACH(jsonIt, numberOfEnemiesVal)
			mNumberOfEnemies.push_back(*jsonIt);

		/*int i = 1;
		json & archetypeNamesVal = value["Archetype Names"];
		FOR_EACH(jsonIt, archetypeNamesVal)
		{
			std::string tempName;
			(*jsonIt)[(std::string("Name ") + std::to_string(i)).c_str()] >> tempName;
			mArchetypeNames.push_back(tempName);
			i++;
		}*/
	}
	void WaveSystem::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);

		value["Number Of Waves"] << mNumberOfWaves;

		FOR_EACH(it, mNumberOfEnemies)
			value["Number Of Enemies"].push_back(*it);

		/*int i = 1;
		FOR_EACH(it, mArchetypeNames)
		{
			json archVal;
			archVal[(std::string("Name ") + std::to_string(i)).c_str()] << *it;
			value["Archetype Names"].push_back(archVal);
			i++;
		}*/
	}



	void ExampleWaveSystem::Initialize()
	{
		mInitializers.push_back(std::bind(&AEX::ExampleWaveSystem::Wave1Initialize, this));
		mInitializers.push_back(std::bind(&AEX::ExampleWaveSystem::Wave2Initialize, this));

		mUpdaters.push_back(std::bind(&AEX::ExampleWaveSystem::Wave1Update, this));
		mUpdaters.push_back(std::bind(&AEX::ExampleWaveSystem::Wave2Update, this));

		WaveSystem::Initialize();
	}
	void ExampleWaveSystem::Update()
	{
		WaveSystem::Update();
	}
	void ExampleWaveSystem::Shutdown()
	{

	}

	void ExampleWaveSystem::Wave1Initialize()
	{
		std::cout << "Wave 1 initialize\n";
	}
	void ExampleWaveSystem::Wave1Update()
	{
		std::cout << "Wave 1 update\n";
	}

	void ExampleWaveSystem::Wave2Initialize()
	{
		std::cout << "Wave 2 initialize\n";
	}
	void ExampleWaveSystem::Wave2Update()
	{
		std::cout << "Wave 2 update\n";
	}

	void ExampleWaveSystem::OnAllWavesEnded()
	{
		std::cout << "All waves ended\n";
	}
}