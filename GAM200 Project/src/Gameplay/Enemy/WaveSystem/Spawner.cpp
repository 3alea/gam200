#include "Spawner.h"
#include "Platform/AEXTime.h"
#include "Composition/AEXScene.h"
#include "Enemy/WaveSystem/WaveSystem.h"
#include "Enemy/EnemyGrounded.h"
#include "Enemy/WaveSystem/BoilerRoomWaveSystem.h"
#include <Imgui/imgui.h>


namespace AEX
{
	void Spawner::Initialize()
	{
		mTimer = 0.0f;
		mGlobalTimer = 0.0f;
		mFirstSpawnDone = false;
		mObjectsSpawned = 0;
		mStopSpawning = false;

		mMainSpace = aexScene->FindSpace("Main");
		mTr = mOwner->GetComp<TransformComp>();

		GameObject * waveSystemObj = aexScene->FindObjectInANYSpace(mWaveSystemName);

		if (waveSystemObj != nullptr)
			mWaveSystem = waveSystemObj->GetComp<WaveSystem>();
	}
	void Spawner::Update()
	{
		if (mInitOnce == false)
		{
			Initialize();
			mInitOnce = true;
		}

		if (mStopSpawning)
			return;

		if ((mSpawnPolicy == ObjectLimit && mObjectsSpawned < mNumberOfObjectsToSpawn) ||
			(mSpawnPolicy == TimeLimit && mGlobalTimer < mTotalTimeToSpawn) || (mSpawnPolicy == Infinitely))
			SpawnLogic();

		mGlobalTimer += (f32)aexTime->GetFrameTime();
	}
	void Spawner::Shutdown()
	{

	}


	void Spawner::SpawnLogic()
	{
		if (mTimer > mTimeForFirstSpawn && mFirstSpawnDone == false)
		{
			if (mMainSpace != nullptr && mTr != nullptr)
			{
				GameObject * newObj = mMainSpace->InstantiateArchetype(mArchetypeName, mTr->mLocal.mTranslationZ);
				newObj->mSpawnerOwner = this;
				
				// If this is the boiler room and the enemy spawned is an enemy grounded
				if (aexScene->GetBaseNameString() == "Boiler room" && newObj->GetComp<EnemyGrounded>())
					BoilerRoomWaveSystem::mGroundedsOnScreen++;
			}

			mObjectsSpawned++;
			mTimer = 0.0f;
			mFirstSpawnDone = true;
		}

		if (mTimer > mTimeBetweenSpawns && mFirstSpawnDone)
		{
			if (mMainSpace != nullptr && mTr != nullptr)
			{
				GameObject * newObj = mMainSpace->InstantiateArchetype(mArchetypeName, mTr->mLocal.mTranslationZ);
				newObj->mSpawnerOwner = this;

				// If this is the boiler room and the enemy spawned is an enemy grounded
				if (aexScene->GetBaseNameString() == "Boiler room" && newObj->GetComp<EnemyGrounded>())
					BoilerRoomWaveSystem::mGroundedsOnScreen++;
			}

			mObjectsSpawned++;
			mTimer = 0.0f;
		}

		mTimer += (f32)aexTime->GetFrameTime();
	}

	void Spawner::EnemyKilled()
	{
		if (mWaveSystem != nullptr)
			mWaveSystem->mEnemiesKilled++;
	}

	void Spawner::StopSpawning()
	{
		mStopSpawning = true;
	}
	void Spawner::ContinueSpawning()
	{
		mStopSpawning = false;
	}

	void Spawner::ChangeArchetypeToSpawn(const char * newArchName)
	{
		strcpy_s(mArchetypeName, newArchName);
	}


	bool Spawner::OnGui()
	{
		bool isChanged = false;

		if (ImGui::InputText("Wave System Name", mWaveSystemName, IM_ARRAYSIZE(mWaveSystemName)))
			isChanged = true;

		if (ImGui::InputText("Archetype Name", mArchetypeName, IM_ARRAYSIZE(mArchetypeName)))
			isChanged = true;

		if (ImGui::InputFloat("Time For First Spawn", &mTimeForFirstSpawn))
			isChanged = true;
		if (ImGui::InputFloat("Time Between Spawns", &mTimeBetweenSpawns))
			isChanged = true;


		if (ImGui::RadioButton("Spawn Indefinitely", &mPolicy, 0))
		{
			mSpawnPolicy = Infinitely;
			isChanged = true;
		}
		ImGui::SameLine();
		if (ImGui::RadioButton("Time Limit", &mPolicy, 1))
		{
			mSpawnPolicy = TimeLimit;
			isChanged = true;
		}
		ImGui::SameLine();
		if (ImGui::RadioButton("Object Limit", &mPolicy, 2))
		{
			mSpawnPolicy = ObjectLimit;
			isChanged = true;
		}
		
		
		if (mPolicy == (int)TimeLimit)
		{
			if (ImGui::InputFloat("Time Limit", &mTotalTimeToSpawn, 0.1f, 0.5f))
			{
				if (mTotalTimeToSpawn < 0)
					mTotalTimeToSpawn = 0.0f;

				isChanged = true;
			}
		}
		if (mPolicy == (int)ObjectLimit)
		{
			if (ImGui::InputInt("Object Limit", &mNumberOfObjectsToSpawn))
				isChanged = true;
		}

		return isChanged;
	}

	void Spawner::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);

		std::string waveSystemName;
		value["Wave System Name"] >> waveSystemName;
		strcpy_s(mWaveSystemName, waveSystemName.c_str());

		std::string archName;
		value["Archetype Name"] >> archName;
		strcpy_s(mArchetypeName, archName.c_str());

		value["Time For First Spawn"] >> mTimeForFirstSpawn;
		value["Time Between Spawns"] >> mTimeBetweenSpawns;

		value["Spawn Policy"] >> mPolicy;
		mSpawnPolicy = SpawnPolicy(mPolicy);

		value["Total Time To Spawn"] >> mTotalTimeToSpawn;
		value["Number Of Objects To Spawn"] >> mNumberOfObjectsToSpawn;
	}
	void Spawner::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);

		std::string waveSystemName = mWaveSystemName;
		value["Wave System Name"] << waveSystemName;

		std::string archName = mArchetypeName;
		value["Archetype Name"] << archName;

		value["Time For First Spawn"] << mTimeForFirstSpawn;
		value["Time Between Spawns"] << mTimeBetweenSpawns;

		value["Spawn Policy"] << mPolicy;

		value["Total Time To Spawn"] << mTotalTimeToSpawn;
		value["Number Of Objects To Spawn"] << mNumberOfObjectsToSpawn;
	}
}