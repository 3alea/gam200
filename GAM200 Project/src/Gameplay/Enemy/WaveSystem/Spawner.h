#pragma once

#include "Components/Logic.h"


namespace AEX
{
	class Space;
	class TransformComp;
	class WaveSystem;


	class Spawner : public LogicComp
	{
		AEX_RTTI_DECL(Spawner, LogicComp);

	public:

		enum SpawnPolicy
		{
			Infinitely,
			TimeLimit,
			ObjectLimit
		};

		virtual void Initialize();
		virtual void Update();
		virtual void Shutdown();

		virtual bool OnGui();

		virtual void FromJson(json & value);
		virtual void ToJson(json & value);

		// IMPORTANT: CALL THIS FUNCTION WHEN AN ENEMY IS KILLED
		void EnemyKilled();

		void StopSpawning();
		void ContinueSpawning();

		void ChangeArchetypeToSpawn(const char * newArchName);

	private:
		void SpawnLogic();

		// Not serializable
		f32 mTimer = 0.0f;
		f32 mGlobalTimer = 0.0f;
		bool mFirstSpawnDone = false;
		int mObjectsSpawned = 0;

		Space * mMainSpace = nullptr;
		TransformComp * mTr = nullptr;
		bool mInitOnce = false;

		WaveSystem * mWaveSystem = nullptr;

		bool mStopSpawning = false;


		// Serializable
		char mArchetypeName[64] = "";
		char mWaveSystemName[64] = "";

		f32 mTimeForFirstSpawn = 0.0f;
		f32 mTimeBetweenSpawns = 5.0f;

		SpawnPolicy mSpawnPolicy = Infinitely;
		int mPolicy = 0;
		f32 mTotalTimeToSpawn = 0.0f;
		int mNumberOfObjectsToSpawn = 0;
	};
}