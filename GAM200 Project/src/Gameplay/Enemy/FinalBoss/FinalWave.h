#pragma once
#include "src/Gameplay/Enemy/Enemy.h"


class FinalWave : public Enemy, public Actor
{
	AEX_RTTI_DECL(FinalWave, Enemy)
public:
	FinalWave();
	~FinalWave();

	void SM_Init();
	void Initialize();
	void Update();
	void ShutDown();

	bool OnGui();
	void FromJson(json & value);
	void ToJson(json & value);



	StateMachine* state_machine;
};

struct FlyingWave : public State
{
	FlyingWave(const char* name);
	void LogicEnter();
	void LogicUpdate();
	void LogicExit();
	bool Initialized = false;
	f32 timer;
	int all_enemies_count;

private:
	std::vector<GameObject*> all_enemies;
	FinalWave* mFinalWave;
	GameObject* fSpawner1;
	GameObject* fSpawner2;
	GameObject* fSpawner3;
};

struct BigMitosisWave : public State
{
	BigMitosisWave(const char* name);
	void LogicEnter();
	void LogicUpdate();
	void LogicExit();
	bool Initialized = false;
	GameObject* mitosis1;
	GameObject* mitosis2;

	//quick fix
	f32 timer;

private:
	std::vector<GameObject*> all_enemies;
	FinalWave* mFinalWave;
};

struct Wave3 : public State
{
	//Wave3(const char* name);
	//void LogicEnter();
	//void LogicUpdate();
	//void LogicExit();

private:
	std::vector<GameObject*> all_enemies;
};