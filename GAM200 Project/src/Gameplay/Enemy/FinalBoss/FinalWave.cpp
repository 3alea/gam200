#include "FinalWave.h"
#include "src/Gameplay/Enemy/EnemySpawner.h"
#include "../EnemyMitosis.h"
#include "../EnemyFlying.h"

#pragma region CLASS
FinalWave::FinalWave()
{
	state_machine = new StateMachine(this);
}

FinalWave::~FinalWave()
{
	delete state_machine;
}

void FinalWave::Initialize()
{
	SM_Init();
}

void FinalWave::SM_Init()
{
	state_machine->AddState(new FlyingWave("FlyingWave"));
	state_machine->AddState(new BigMitosisWave("BigMitosisWave"));


	state_machine->SetInitState("FlyingWave");
	state_machine->ChangeState(state_machine->mInitialState);
	state_machine->mCurrentState = state_machine->mInitialState;
}

void FinalWave::Update()
{
	state_machine->Update();
}

void FinalWave::ShutDown()
{

}

bool FinalWave::OnGui()
{
	return false;
}

void FinalWave::FromJson(json& value)
{
	Enemy::FromJson(value);
}

void FinalWave::ToJson(json& value)
{
	Enemy::ToJson(value);
}

#pragma endregion

#pragma region STATES
FlyingWave::FlyingWave(const char* name) : State(name)
{
	
}

void FlyingWave::LogicEnter()
{
}

void FlyingWave::LogicUpdate()
{
	if (!Initialized)
	{
		mFinalWave = static_cast<FinalWave*>(mActor);
		fSpawner1 = mFinalWave->mOwner->GetParentSpace()->InstantiateArchetype("flyingspawner", AEVec3(mFinalWave->mOwner->GetComp<TransformComp>()->mWorld.mTranslationZ.x, mFinalWave->mOwner->GetComp<TransformComp>()->mWorld.mTranslationZ.y, mFinalWave->mOwner->GetComp<TransformComp>()->mWorld.mTranslationZ.z));
		fSpawner2 = mFinalWave->mOwner->GetParentSpace()->InstantiateArchetype("flyingspawner", AEVec3(mFinalWave->mOwner->GetComp<TransformComp>()->mWorld.mTranslationZ.x - 10.f, mFinalWave->mOwner->GetComp<TransformComp>()->mWorld.mTranslationZ.y, mFinalWave->mOwner->GetComp<TransformComp>()->mWorld.mTranslationZ.z));
		fSpawner3 = mFinalWave->mOwner->GetParentSpace()->InstantiateArchetype("flyingspawner", AEVec3(mFinalWave->mOwner->GetComp<TransformComp>()->mWorld.mTranslationZ.x + 10.f, mFinalWave->mOwner->GetComp<TransformComp>()->mWorld.mTranslationZ.y, mFinalWave->mOwner->GetComp<TransformComp>()->mWorld.mTranslationZ.z));
		fSpawner1->GetComp<FlyingSpawner>()->activate = true;
		fSpawner2->GetComp<FlyingSpawner>()->activate = true;
		fSpawner3->GetComp<FlyingSpawner>()->activate = true;
		fSpawner1->GetComp<FlyingSpawner>()->spawn_time = 9.f;
		fSpawner2->GetComp<FlyingSpawner>() ->spawn_time = 9.f;
		fSpawner3->GetComp<FlyingSpawner>() ->spawn_time = 9.f;
		fSpawner1->GetComp<FlyingSpawner>()->flying_shoot_timer = 4.f;
		fSpawner2->GetComp<FlyingSpawner>()->flying_shoot_timer = 4.f;
		fSpawner3->GetComp<FlyingSpawner>()-> flying_shoot_timer = 4.f;
		fSpawner1->GetComp<FlyingSpawner>()->flying_bullet_speed = 0.12f;
		fSpawner2->GetComp<FlyingSpawner>()->flying_bullet_speed = 0.12f;
		fSpawner3->GetComp<FlyingSpawner>()->flying_bullet_speed = 0.12f;
		all_enemies.push_back(fSpawner1);
		all_enemies.push_back(fSpawner2);
		all_enemies.push_back(fSpawner3);
		Initialized = true;
		timer = 0.f;
	}

	all_enemies_count = fSpawner1->GetComp<FlyingSpawner>()->all_flying.size() + fSpawner2->GetComp<FlyingSpawner>()->all_flying.size() + fSpawner3->GetComp<FlyingSpawner>()->all_flying.size();
	//std::cout << all_enemies_count << std::endl;
	//if (timer == 0.f)
	//{
	//	for (auto it = all_enemies.begin(); it != all_enemies.end(); it++)
	//		(*it)->GetComp<FlyingSpawner>()->Update();
	//}

	if (fSpawner1->GetComp<FlyingSpawner>()->count > 1)
	{
		fSpawner1->GetComp<FlyingSpawner>()->activate = false;
		fSpawner2->GetComp<FlyingSpawner>()->activate = false;
		fSpawner3->GetComp<FlyingSpawner>()->activate = false;
		timer += (f32)aexTime->GetFrameTime();
	}
	//if (all_enemies_count == 0 && fSpawner1->GetComp<FlyingSpawner>()->count > 3)
	//	LogicExit();
	if (timer > 8)
		LogicExit();
}

void FlyingWave::LogicExit()
{
	Initialized = false;
	mFinalWave->state_machine->ChangeState("BigMitosisWave");
}

BigMitosisWave::BigMitosisWave(const char* name) :State(name) {}

void BigMitosisWave::LogicEnter() {}
void BigMitosisWave::LogicUpdate()
{
	if (!Initialized)
	{
		mFinalWave = static_cast<FinalWave*>(mActor);
		mitosis1 = mFinalWave->mOwner->GetParentSpace()->InstantiateArchetype("flying", AEVec3(mFinalWave->mOwner->GetComp<TransformComp>()->mWorld.mTranslationZ.x, mFinalWave->mOwner->GetComp<TransformComp>()->mWorld.mTranslationZ.y, mFinalWave->mOwner->GetComp<TransformComp>()->mWorld.mTranslationZ.z));
		mitosis2 = mFinalWave->mOwner->GetParentSpace()->InstantiateArchetype("flying", AEVec3(mFinalWave->mOwner->GetComp<TransformComp>()->mWorld.mTranslationZ.x - 10.f, mFinalWave->mOwner->GetComp<TransformComp>()->mWorld.mTranslationZ.y, mFinalWave->mOwner->GetComp<TransformComp>()->mWorld.mTranslationZ.z));
		
		mitosis1->GetComp<TransformComp>()->SetScale(AEVec2(4.f, 4.f));
		mitosis2->GetComp<TransformComp>()->SetScale(AEVec2(4.f, 4.f));
		mitosis1->GetComp<BoxCollider>()->mScale = mitosis1->GetComp<TransformComp>()->GetScale();
		mitosis2->GetComp<BoxCollider>()->mScale = mitosis2->GetComp<TransformComp>()->GetScale();
		all_enemies.push_back(mitosis1);
		all_enemies.push_back(mitosis2);
		Initialized = true;
	}
	return;
	//for (auto it = all_enemies.begin(); it != all_enemies.end(); it++)
	//{
	//	EnemyFlying* ef = (*it)->GetComp<EnemyFlying>();
	//	if (ef)
	//		ef->Update();
	//}

}

void BigMitosisWave::LogicExit()
{
	Initialized = false;
	//end
}

#pragma endregion