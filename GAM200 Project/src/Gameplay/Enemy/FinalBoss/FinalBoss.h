#pragma once
#include "src/Gameplay/Enemy/Enemy.h"
#include "Components\SpineAnimation.h"
#include "CameraLogic/CameraShaker.h"

class FinalBoss : public Enemy, public Actor
{
	AEX_RTTI_DECL(FinalBoss, Enemy)
public:
	FinalBoss();
	~FinalBoss();

	void SM_Init();
	void Initialize();
	void Update();
	void ShutDown();

	void OnCollisionEvent(const CollisionEvent& collision);
	bool OnGui();
	void FromJson(json & value);
	void ToJson(json & value);
	void UpdatePunches();
	PathComponent* path1;
	SpineAnimationComp* spineAnimation;
	GameObject * cam;
	CameraShaker* cs;
	AEVec3 camerapos;
	void CameraShake();
	bool byebossbar;
	bool crazy;
	//ATTACKS
	AEVec2 HorizontalPunchLogic();
	void HorizontalPunch(AEVec2 launchPos);
	bool horizontal_punched;
	void Warning(AEVec2 pos);
	bool warned;
	void ThunderLogic();
	f32 last_thunder;
	f32 mWarning_time;
	void VerticalPunch();
	AEVec2 HorizontalLaunchPosition();
	AEVec2 VerticalLaunchPosition();
	void Shoot();
	f32 mLast_shoot;
	f32 mTimeBTWShoots;
	int i = 0;
	f32 unai;

	f32 lastpunch;
	f32 mipadre;
	//Saving level logic
	bool once_second_half = false;
	std::vector<GameObject*> all_punches;
	std::vector<GameObject*> all_warnings;
	StateMachine* state_machine;
	bool Initialized;
	AEVec2 mCamWidth;
	AEVec2 mCamHeight;
	bool mDeath;
	enum  SuperState { Tutorial, First_Half, Middle, Second_Half };
	SuperState Superstate;
	f32 timer;
	bool mShield;
};
class LifeTime : public LogicComp
{
	AEX_RTTI_DECL(LifeTime, LogicComp);
public:
	f32 timer;
	f32 time_to_die;
	void Initialize();
	void Update();
	bool OnGui();
	void FromJson(json & value);
	void ToJson(json & value);
};

struct Entry : public State
{
	Entry(const char* name) : State(name) { Initialized = false; }
	void LogicEnter();
	void LogicUpdate();
	void LogicExit();

private:
	f32 timer;
	FinalBoss* mFinalBoss;
	bool Initialized;
};
struct ByeBye : public State
{
	ByeBye(const char* name) : State(name) { Initialized = false; }
	void LogicEnter();
	void LogicUpdate();
	void LogicExit();

private:
	f32 timer;
	FinalBoss* mFinalBoss;
	bool Initialized;
};

struct Death : public State
{
	Death(const char* name) : State(name) { Initialized = false; }
	void LogicEnter();
	void LogicUpdate();
	void LogicExit();
	bool do_once;
	bool thunder;
	f32 lastflash;
private:
	f32 timer;
	FinalBoss* mFinalBoss;
	bool Initialized;
};

struct Rest : public State
{
	Rest(const char* name) : State(name) { Initialized = false; count = 0; }
	void LogicEnter();
	void LogicUpdate();
	void LogicExit();
	int count;

private:
	f32 timer;
	FinalBoss* mFinalBoss;
	bool Initialized;
};

class PunchLogic :public LogicComp
{
	AEX_RTTI_DECL(PunchLogic, LogicComp);
public:
	PunchLogic() { is_vertical = false; sound_sounded = false; }
	void OnCollisionEvent(const CollisionEvent & collision);
	bool is_vertical;
	bool sound_sounded;
};

struct Punches
{
	int mVerticalPunches;
	float mTimebtwVertical;
	float mLastVerticalPunch;
	int mHorizontalPunches;
	float mTimebtwHorizontal;
	float mLastHorizontalPunch;
};

struct Tuto : public Punches, public State
{
	Tuto(const char* name) : State(name) { Initialized = false; }
	void LogicEnter();
	void LogicUpdate();
	void LogicExit();

private:
	f32 timer;
	FinalBoss* mFinalBoss;
	bool Initialized;
	AEVec2 HorizontalLaunchPos;
};

struct RandomState : public Punches, public State
{
	RandomState(const char* name) : State(name) { Initialized = false; }
	void LogicEnter();
	void LogicUpdate();
	void LogicExit();

private:
	f32 timer;
	FinalBoss* mFinalBoss;
	bool Initialized;
	AEVec2 HorizontalLaunchPos;
};
struct HoriPunch
{
	HoriPunch();
	bool on_Path1;
	bool on_Middle;
	bool on_Path2;
	f32 punched_1;
	GameObject* go;
	bool check;
};
struct HorizontalPunches : public Punches, public State
{
	HorizontalPunches(const char* name) : State(name) { Initialized = false; }
	void LogicEnter();
	void LogicUpdate();
	void LogicExit();
	void FirstPunch();
	void Middle(HoriPunch* hp);
	void SecondPunch(HoriPunch* hp);
	AEVec2 SpawnPoint();
	void CameraShake();
	void CheckLives();
	bool stop;
private:
	f32 timer;
	FinalBoss* mFinalBoss;
	bool Initialized;
	AEVec2 HorizontalLaunchPos;
	std::vector<HoriPunch*> all_punches;
};

struct FromLeft2Right : public Punches, public State
{
	FromLeft2Right(const char* name) : State(name) {
		Initialized = false; mOffset = 0.f;
		the_End = false;
	}
	void LogicEnter();
	void LogicUpdate();
	void LogicExit();
	AEVec2 ComputeStartingPoint();
	std::vector<GameObject*> this_punches;
	bool the_End;
	bool mLastPunchFInished;
	void CameraShake();

private:
	f32 timer;
	FinalBoss* mFinalBoss;
	bool Initialized;
	f32 mOffset;
	f32 time_the_end;
};

struct FromOut2Center : public Punches, public State
{
	FromOut2Center(const char* name) : State(name) {
		Initialized = false; mOffset = 0.f;
		the_End = false;
	}
	void LogicEnter();
	void LogicUpdate() ;
	void LogicExit() ;
	void ComputeOffset();
	std::vector<GameObject*> this_punches;
	bool the_End;
	bool mLastPunchFInished;

private:
	f32 timer;
	AEVec2 mMiddlePoint;
	FinalBoss* mFinalBoss;
	bool Initialized;
	f32 mOffset;
	f32 time_the_end;
};

struct FromRight2Left : public Punches, public State
{
	FromRight2Left(const char* name) : State(name) { Initialized = false; }
	void LogicEnter();
	void LogicUpdate();
	void LogicExit();
	AEVec2 ComputeStartingPoint();
	std::vector<GameObject*> this_punches;
	bool the_End;
	bool mLastPunchFInished;
	void CameraShake();

private:
	f32 timer;
	FinalBoss* mFinalBoss;
	bool Initialized;
	f32 mOffset;
	f32 time_the_end;
};

struct TargetPunches
{
	TargetPunches();
	GameObject* go;
	int RandomTargets;
	int mTargetCounts;
	f32 time_to_punch;
	bool punched;
	int label;
	bool done;
	bool anticipation;
};

struct TargetPlayer : public Punches, public State
{
	TargetPlayer(const char* name) : State(name) { Initialized = false; }
	void LogicEnter() ;
	void LogicUpdate() ;
	void LogicExit() ;
	void TargetthePlayer(TargetPunches* punch);
	void Punch(TargetPunches* punch);
	bool targetted = false;
	void RandomFake();
	void CameraShake();

	TargetPunches* Tpunch1;
	TargetPunches* Tpunch2;
	TargetPunches* Tpunch3;
	TargetPunches* MAX_T;
	std::vector<TargetPunches*> all_punches;
private:
	f32 timer;
	f32 delay;
	FinalBoss* mFinalBoss;
	bool Initialized;
	AEVec2 mPlayerPos;
	int MAXRandomTargets;
	TransformComp* mPlayerTransform;
};

struct BossBulletStruct : public LogicComp
{
	AEX_RTTI_DECL(BossBulletStruct, LogicComp);
public:
	void OnCollisionEvent(const CollisionEvent& collision);
};