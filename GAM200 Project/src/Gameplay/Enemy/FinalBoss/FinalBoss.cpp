#include "FinalBoss.h"
#include "src/Engine/Graphics/WindowMgr.h"
#include "src/Engine/Utilities/RNG.h"
#include "Bullets/BulletLogic.h"
#include "src/Gameplay/Environment/Background/StationZoomOut.h"
#include "src/Gameplay/Enemy/EnemyBullet.h"
#include <spine\AnimationState.h>
#include "Audio\AudioManager.h"

#pragma region FINALBOSSCLASS

FinalBoss::FinalBoss()
{
	state_machine = new StateMachine(this); //set the actor
	Initialized = false;
	path1 = new PathComponent;
}

FinalBoss::~FinalBoss()
{
	delete state_machine;
}

void FinalBoss::SM_Init()
{
	state_machine->AddState(new Entry("Entry"));
	state_machine->AddState(new ByeBye("ByeBye"));
	state_machine->AddState(new Rest("Rest"));
	state_machine->AddState(new Tuto("Tuto"));
	state_machine->AddState(new RandomState("RandomState")); 

	state_machine->AddState(new HorizontalPunches("HorizontalPunches"));
	state_machine->AddState(new Death("Death")); 
	state_machine->AddState(new FromLeft2Right("FromLeft2Right"));
	state_machine->AddState(new FromRight2Left("FromRight2Left"));
	state_machine->AddState(new FromOut2Center("FromOut2Center"));
	state_machine->AddState(new TargetPlayer("TargetPlayer"));
	state_machine->SetInitState("Entry");
	state_machine->ChangeState(state_machine->mInitialState);
	state_machine->mCurrentState = state_machine->mInitialState;
	Superstate = SuperState::Tutorial;
}

void FinalBoss::Initialize()
{
	SM_Init();
	mDeath = false;
	timer = 0.f;
	mWarning_time = 1000.f;
	horizontal_punched = false;
	mLast_shoot = 0.f;
	mTimeBTWShoots = 50.f;
	mShield = true;
	last_thunder = 0.f;
	once_second_half = false;
	byebossbar = false;
	crazy = false;
}

void FinalBoss::Update()
{
	StationZoomOut::shouldLockCamera = true; // to lock the camera
	if (!Initialized)
	{
		cam = aexScene->FindObjectInANYSpace("camera");
		if (cam == NULL)
			return;
		camerapos = cam->GetComp<TransformComp>()->GetPosition3D();
		cs = cam->GetComp<CameraShaker>();
		f32 Wwidht = (f32)WindowMgr->GetCurrentWindow()->GetWidth();
		f32 Wheight = (f32)WindowMgr->GetCurrentWindow()->GetHeight();

		f32 AspectRatio = Wwidht / Wheight;
		mCamWidth = AEVec2(cam->GetComp<TransformComp>()->GetPosition3D().x - (2 * AspectRatio * cam->GetComp<Camera>()->mViewRectangleSize) / 2,
			cam->GetComp<TransformComp>()->GetPosition3D().x + (2 * AspectRatio * cam->GetComp<Camera>()->mViewRectangleSize) / 2);
		mCamHeight = AEVec2(cam->GetComp<TransformComp>()->GetPosition3D().y - (2 * cam->GetComp<Camera>()->mViewRectangleSize) / 2,
			cam->GetComp<TransformComp>()->GetPosition3D().y + (2 * cam->GetComp<Camera>()->mViewRectangleSize) / 2);
		Initialized = true;
		//path logic
		path1->Push(0.f, AEVec2(mOwner->GetComp<TransformComp>()->GetPosition3D().x, mOwner->GetComp<TransformComp>()->GetPosition3D().y));
		path1->Push(4.f, AEVec2(mCamWidth.x + (mCamWidth.y - mCamWidth.x) / 3, mOwner->GetComp<TransformComp>()->GetPosition3D().y));
		path1->Push(12.f, AEVec2(mCamWidth.x + (mCamWidth.y - mCamWidth.x) / 3*2, mOwner->GetComp<TransformComp>()->GetPosition3D().y));
		path1->Push(15.7f, AEVec2(mOwner->GetComp<TransformComp>()->GetPosition3D().x, mOwner->GetComp<TransformComp>()->GetPosition3D().y));
		path1->changey = true;
		mOwner->AddComp(path1);
		spineAnimation = mOwner->GetComp<SpineAnimationComp>();
		unai = 0.f;
	}
	if (mLives <= 0)
		state_machine->ChangeState("Death");
	state_machine->Update();
	UpdatePunches();
	timer += (f32)aexTime->GetFrameTime();
	CameraShake();
	if(mLives > 0)
		ThunderLogic();
	if (Superstate == SuperState::Middle)
		mShield = true;
	cam->GetComp<TransformComp>()->SetPosition3D(camerapos);
	if (Superstate == SuperState::Middle && byebossbar == false)
	{
 		aexScene->FindObjectInANYSpace("BossBar")->RemoveComp(aexScene->FindObjectInANYSpace("BossBar")->GetComp <SpriteComp>());
		aexScene->FindObjectInANYSpace("BossBarBase")->RemoveComp(aexScene->FindObjectInANYSpace("BossBarBase")->GetComp <SpriteComp>());
		byebossbar = true;
	}
}

void FinalBoss::UpdatePunches()
{
	for (auto it = all_punches.begin(); it != all_punches.end(); )
	{
		if ((*it)->GetComp<PathFollower>()->GetTime() > (*it)->GetComp<PathComponent>()->Duration())
		{
			if((*it)->GetComp<PunchLogic>()->is_vertical)
				audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\FinalBoss\\FinalBossHit.wav"), Voice::SFX);

			mOwner->GetParentSpace()->DestroyObject(*it);
			it = all_punches.erase(it);
		}
		else
			it++;
	}
}

void FinalBoss::ShutDown()
{

}

void FinalBoss::OnCollisionEvent(const CollisionEvent& collision)
{
	if (mDeath)
		return;
	if (BulletLogic * logic = collision.otherObject->GetComp<BulletLogic>())
	{
		collision.otherObject->GetParentSpace()->DestroyObject(collision.otherObject);

		if(mShield == false)
			mLives--;

		if (mLives < 0)
		{
			state_machine->mCurrentState->LogicExit();
			state_machine->ChangeState("Death");
			mDeath = true;
		}
	}
}

void PunchLogic::OnCollisionEvent(const CollisionEvent& collision)
{
	if (collision.otherObject->GetBaseNameString() == "player")
	{
		PlayerStats* stats = collision.otherObject->GetComp<PlayerStats>();

		if (!stats->isInvincible)
		{
			stats->lives--;
			stats->isHit = true;
		}
	}
}

#pragma endregion

#pragma region ATTACKS

void FinalBoss::CameraShake()
{
	for (auto it = all_punches.begin(); it != all_punches.end(); it++)
	{
		if (AEVec2(aexScene->FindObjectInANYSpace("player")->GetComp<TransformComp>()->GetPosition3D().x, aexScene->FindObjectInANYSpace("player")->GetComp<TransformComp>()->GetPosition3D().y).Distance
		(AEVec2((*it)->GetComp<TransformComp>()->GetPosition3D().x, (*it)->GetComp<TransformComp>()->GetPosition3D().y)) < 7.f)
		{
			cs->mTrauma = 0.9f;
			if ((*it)->GetComp<PunchLogic>()->is_vertical == false)
			{
				if (!(*it)->GetComp<PunchLogic>()->sound_sounded)
				{
					audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\FinalBoss\\fastCar2.mp3"), Voice::SFX);
					(*it)->GetComp<PunchLogic>()->sound_sounded = true;
				}
			}
		}
	}
}

void FinalBoss::Shoot()
{
	bool five_shoots = false;
	if (mLives <= 38 && Superstate == SuperState::First_Half)
	{
		mTimeBTWShoots = 10.f;
		EnemyBulletSpeed = 0.15f;
	}

	if (Superstate == SuperState::Second_Half)
	{
		mTimeBTWShoots = 4.f;
		EnemyBulletSpeed = 0.2f;
		five_shoots = true;
	}
	if (Superstate == SuperState::Middle)
	{
		return;
	}

	//SHOOT
	if (timer - mLast_shoot > mTimeBTWShoots && mLives > 0 )
	{
		if (state_machine->mCurrentState->mName != "FromOut2Center" && state_machine->mCurrentState->mName != "ByeBye" 
			&& state_machine->mCurrentState->mName != "Entry" && state_machine->mCurrentState->mName != "Death")
		{
			EnemyBullet(mOwner->GetComp<TransformComp>(), i, EnemyBulletSpeed, "boss", NULL, 1);
			i++;
			EnemyBullet(mOwner->GetComp<TransformComp>(), i, EnemyBulletSpeed, "boss", NULL, 2);
			i++;
			EnemyBullet(mOwner->GetComp<TransformComp>(), i, EnemyBulletSpeed, "boss", NULL, 3);
			i++;
			if (five_shoots)
			{
				EnemyBullet(mOwner->GetComp<TransformComp>(), i, EnemyBulletSpeed, "boss", NULL, 4);
				i++;
				EnemyBullet(mOwner->GetComp<TransformComp>(), i, EnemyBulletSpeed, "boss", NULL, 5);
				i++;
			}

			mLast_shoot = timer;
		}
	}
}
void FinalBoss::ThunderLogic()
{
	if (!crazy)
	{
		if (timer - last_thunder > 3.f)
		{
			AEVec2 pos = AEVec2(RNG->GetFloat(mCamWidth.x + 2.5f, mCamWidth.y - 2.5f), RNG->GetFloat(mCamHeight.y - 10.f, mCamHeight.y - 1.f));
			GameObject* thunder = mOwner->GetParentSpace()->InstantiateArchetype("Thunder", AEVec3(pos.x, pos.y, -3.f));
			last_thunder = timer;

			if (Superstate == SuperState::Second_Half)
			{
				if (RNG->GetInt(0, 3) == 1) // flasazo
				{
					GameObject* flash = mOwner->GetParentSpace()->InstantiateArchetype("Flahsazo", AEVec3(mCamWidth.x + (mCamWidth.y - mCamWidth.x) / 2, mCamHeight.x + (mCamHeight.y - mCamHeight.x) / 2, 1.f));

					audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\FinalBoss\\Thunder07.wav"), Voice::SFX);
				}
			}
		}//flash
	}
	if (crazy)
	{
		if (timer - last_thunder > 0.3f)
		{
			AEVec2 pos = AEVec2(RNG->GetFloat(mCamWidth.x + 2.5f, mCamWidth.y - 2.5f), RNG->GetFloat(mCamHeight.y - 10.f, mCamHeight.y - 1.f));
			GameObject* thunder = mOwner->GetParentSpace()->InstantiateArchetype("Thunder", AEVec3(pos.x, pos.y, -3.f));

			audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\FinalBoss\\Thunder07.wav"), Voice::SFX);
			last_thunder = timer;
		}//flash
	}

}

AEVec2 FinalBoss::HorizontalPunchLogic()
{
	AEVec2 launchPos = HorizontalLaunchPosition();
	Warning(launchPos);
	return launchPos;
}

void FinalBoss::Warning(AEVec2 pos)
{
	mWarning_time = timer;
	if (pos.x == mCamWidth.x -2.f)
		pos.x += 2.5f;
	if (pos.x == mCamWidth.y + 2.f)
		pos.x -= 1.9f;

	GameObject* warning = mOwner->GetParentSpace()->InstantiateArchetype("Warning", AEVec3(pos.x, pos.y, 0));
	all_warnings.push_back(warning);
}

void FinalBoss::HorizontalPunch(AEVec2 launchPos)
{
	GameObject* h_punch = mOwner->GetParentSpace()->InstantiateArchetype("HorizontalPunch", AEVec3(launchPos.x, launchPos.y, 0));
	h_punch->GetComp< PunchLogic>()->is_vertical = false;
	PathComponent* path = new PathComponent;
	path->Push(0.f, AEVec2(launchPos.x, launchPos.y));
	if(launchPos.x == mCamWidth.x - 2.f)
	{
		path->Push(1.5f, AEVec2(path->LastPoint().x + (mCamWidth.y - mCamWidth.x) + 3.f, path->LastPoint().y));
		if(launchPos.y == mCamHeight.x + 0.8f)
			h_punch->GetComp<SpineAnimationComp>()->ChangeAnimation(0, "Random Horizontal OtherSide", false, 0.5f, .3f);
		else
			h_punch->GetComp<SpineAnimationComp>()->ChangeAnimation(0, "Random Horizontal Arriba Otherside", false, 0.5f, .3f);
	}
	else
	{
		path->Push(1.5f, AEVec2(path->LastPoint().x - (mCamWidth.y - mCamWidth.x) - 3.f, path->LastPoint().y));
		if (launchPos.y == mCamHeight.x + 0.8f)
			h_punch->GetComp<SpineAnimationComp>()->ChangeAnimation(0, "Barrida Horizontal Random", false, 0.5f, .3f);
		else
			h_punch->GetComp<SpineAnimationComp>()->ChangeAnimation(0, "Random Horizontal Arriba", false, 0.5f, .3f);
	}

	h_punch->AddComp(path);
	all_punches.push_back(h_punch);
}
AEVec2 FinalBoss::HorizontalLaunchPosition()
{
	AEVec2 randompos;
	//LEFT OR RIGHT
	if (RNG->Get0or1() == 0)
		randompos.x = mCamWidth.x - 2.f; // Min x (from the left)
	else
		randompos.x = mCamWidth.y + 2.f; // Max (from the right)

	// JUMP OR CROUCH
	if (RNG->Get0or1() == 0)
		randompos.y = mCamHeight.x + 0.8f; // to jump
	else
		randompos.y = mCamHeight.x + 2.7f; // to crouch

	return randompos;
}

void FinalBoss::VerticalPunch()
{
	AEVec2 launchPos = VerticalLaunchPosition();
	GameObject* v_punch = mOwner->GetParentSpace()->InstantiateArchetype("VerticalPunch", AEVec3(launchPos.x, launchPos.y, 0));
	v_punch->GetComp< PunchLogic>()->is_vertical = true;
	PathComponent* path = new PathComponent;
	v_punch->GetComp<SpineAnimationComp>()->ChangeAnimation(0, "Barrida Vertical Random", false, 0.35f, .3f);
	path->Push(0.f, AEVec2(launchPos.x, launchPos.y));
	path->Push(0.4f, AEVec2(path->LastPoint().x, path->LastPoint().y - (mCamHeight.y - mCamHeight.x - 1.5f)));
	v_punch->AddComp(path);
	all_punches.push_back(v_punch);
}
AEVec2 FinalBoss::VerticalLaunchPosition()
{
	AEVec2 randompos;

	randompos.x = RNG->GetFloat(mCamWidth.x + 1.f, mCamWidth.y - 1.f);
	randompos.y = mCamHeight.y; // max y

	return randompos;
}

#pragma endregion

#pragma region UTILS
bool FinalBoss::OnGui()
{
	ImGui::InputInt("Lives", &mLives);
	return false;
}

void FinalBoss::FromJson(json & value)
{
	Enemy::FromJson(value);
}

void FinalBoss::ToJson(json & value)
{
	Enemy::ToJson(value);
}

#pragma endregion

#pragma region STATES
void Entry::LogicEnter()
{
}

void Entry::LogicUpdate()
{
	if (!Initialized)
	{
		mFinalBoss = static_cast<FinalBoss*>(mActor);
		timer = 0.f;
		Initialized = true;
		//mFinalBoss->mOwner->RemoveComp(mFinalBoss->path1, false);
		mFinalBoss->spineAnimation->ChangeAnimation(0, "Getting Out Of The Darkness", false, 0.4f, .3f);
		mFinalBoss->mShield = true;
	}
	timer += (f32)aexTime->GetFrameTime();


	if (mFinalBoss->spineAnimation->GetAnimState()->getCurrent(0)->isComplete())
	{
		mFinalBoss->state_machine->ChangeState("Rest");
	}
}

void Entry::LogicExit()
{
	mFinalBoss->spineAnimation->ChangeAnimation(0, "idle", false, 1.f, 0.3f);
	//mFinalBoss->mOwner->AddComp(mFinalBoss->path1);
	if(mFinalBoss->Superstate == FinalBoss::SuperState::Tutorial)
		mFinalBoss->state_machine->ChangeState("Tuto");
	else
		mFinalBoss->state_machine->ChangeState("Rest");

	mFinalBoss->mShield = false;
	Initialized = false;
}

void ByeBye::LogicEnter()
{
}

void ByeBye::LogicUpdate()
{
	if (!Initialized)
	{
		mFinalBoss = static_cast<FinalBoss*>(mActor);
		timer = 0.f;
		Initialized = true;
		mFinalBoss->spineAnimation->ChangeAnimation(0, "Into The Darkness", false, 0.4f, 0.3f);
	}
	timer += (f32)aexTime->GetFrameTime();

	mFinalBoss->mOwner->GetComp<TransformComp>()->SetScale(mFinalBoss->mOwner->GetComp<TransformComp>()->GetScale() - AEVec2(0.06f, 0.06f));
	if (mFinalBoss->spineAnimation->GetAnimState()->getCurrent(0)->isComplete())
	{
		mFinalBoss->state_machine->ChangeState("Rest");
	}
}

void ByeBye::LogicExit()
{
	mFinalBoss->mShield = false;
	mFinalBoss->state_machine->ChangeState("RandomState");
	Initialized = false;
}

void Death::LogicEnter()
{
}

void Death::LogicUpdate()
{
	if (!Initialized)
	{
		mFinalBoss = static_cast<FinalBoss*>(mActor);
		timer = 0.f;
		Initialized = true;
		mFinalBoss->mOwner->RemoveComp(mFinalBoss->mOwner->GetComp<PathFollower>());
		do_once = false;
		mFinalBoss->crazy = true;
		thunder = false;
	}
  	timer += (f32)aexTime->GetFrameTime();
	if(timer < 7.f)
		mFinalBoss->cs->mTrauma = 0.6f;
	if(!thunder)
		mFinalBoss->ThunderLogic();

	if (timer - lastflash > 0.8f && timer < 5.f)
	{
		GameObject* flash = mFinalBoss->mOwner->GetParentSpace()->InstantiateArchetype("Flahsazo", AEVec3(mFinalBoss->mCamWidth.x + (mFinalBoss->mCamWidth.y - mFinalBoss->mCamWidth.x) / 2,
			mFinalBoss->mCamHeight.x + (mFinalBoss->mCamHeight.y - mFinalBoss->mCamHeight.x) / 2, 1.f));
		flash->GetComp<LifeTime>()->time_to_die = 0.3f;
		lastflash = timer;
	}

	if (timer > 5.5f && !do_once)
	{
		do_once = true;
		GameObject* flash = mFinalBoss->mOwner->GetParentSpace()->InstantiateArchetype("Flahsazo", AEVec3(mFinalBoss->mCamWidth.x + (mFinalBoss->mCamWidth.y - mFinalBoss->mCamWidth.x) / 2,
			mFinalBoss->mCamHeight.x + (mFinalBoss->mCamHeight.y - mFinalBoss->mCamHeight.x) / 2, 1.f));
		flash->GetComp<LifeTime>()->time_to_die = 5.5f;
		mFinalBoss->mOwner->RemoveComp(mFinalBoss->mOwner->GetComp<SpineAnimationComp>());
	}
	if (timer > 7.5f)
		thunder = true;
	if (timer > 21.f)
		aexScene->ChangeLevel("data/Levels/EndCredits.json");
}

void Death::LogicExit()
{
	Initialized = false;
}


void Rest::LogicEnter()
{

}

void Rest::LogicUpdate()
{
	if (!Initialized)
	{
		mFinalBoss = static_cast<FinalBoss*>(mActor);
		timer = 0.f;
		Initialized = true;
	}
	timer += (f32)aexTime->GetFrameTime();

	if (timer > 1.0f)
	{
		mFinalBoss->state_machine->ChangeState("TargetPlayer");
	}
}

void Rest::LogicExit() 
{
	Initialized = false;
	if (mFinalBoss->mLives <= 0)
	{
		mFinalBoss->state_machine->ChangeState("Death");
		return;
	}

	if (mFinalBoss->Superstate == FinalBoss::SuperState::Second_Half && !mFinalBoss->once_second_half)
	{
		mFinalBoss->once_second_half = true;
		aexScene->FindObjectInANYSpace("player")->GetComp<PlayerStats>()->lives = 3;
	}
	if (mFinalBoss->mLives <= 20 && mFinalBoss->Superstate == FinalBoss::SuperState::First_Half) 
	{
		audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\FinalBoss\\FinalBossDie.wav"), Voice::SFX);
		mFinalBoss->Superstate = FinalBoss::SuperState::Middle;
		mFinalBoss->state_machine->ChangeState("ByeBye");
		aexScene->FindObjectInANYSpace("player")->GetComp<PlayerStats>()->lives = 3;
		mFinalBoss->spineAnimation->ChangeAnimation(0, "Big Damage", false, 0.5f, 0.3f);
	}
	else
	{
		int r_num = RNG->GetInt(0, 5);
		switch (r_num)
		{
		case 0:
			mFinalBoss->state_machine->ChangeState("FromLeft2Right");
			break;
		case 1:
			mFinalBoss->state_machine->ChangeState("TargetPlayer"); 
			break;
		case 2:
			mFinalBoss->state_machine->ChangeState("FromRight2Left");
			break;
		case 3:
			mFinalBoss->state_machine->ChangeState("FromOut2Center");
			break; 
		case 4:
			mFinalBoss->state_machine->ChangeState("HorizontalPunches");
			break;
		}
	}
}

void Tuto::LogicEnter() {}
void Tuto::LogicUpdate()
{
	if (!Initialized)
	{
		mFinalBoss = static_cast<FinalBoss*>(mActor);
		timer = 0.f;
		Initialized = true;
		mLastHorizontalPunch = 0.f;
		mLastVerticalPunch = 0.f;
		//TO UPDATE
		mHorizontalPunches = 2;
		mVerticalPunches = 6;
		mTimebtwHorizontal = 4.5f;
		mTimebtwVertical = 2.f;
	}
	//HORIZONTAL PUNCH
	if (mHorizontalPunches > 0 && timer - mLastHorizontalPunch > mTimebtwHorizontal)
	{
		HorizontalLaunchPos = mFinalBoss->HorizontalPunchLogic();
		mHorizontalPunches--;
		mLastHorizontalPunch = timer;
		mFinalBoss->horizontal_punched = false;
	}

	if (mFinalBoss->mWarning_time + 1.f < mFinalBoss->timer && !mFinalBoss->horizontal_punched)
	{
		mFinalBoss->HorizontalPunch(HorizontalLaunchPos);
		mFinalBoss->horizontal_punched = true;
	}
	//VERTICAL PUNCH
	if (mVerticalPunches > 0 && timer - mLastVerticalPunch > mTimebtwVertical)
	{
		mFinalBoss->VerticalPunch();
		mVerticalPunches--;
		mLastVerticalPunch = timer;
	}

	timer += (f32)aexTime->GetFrameTime();

	if (mHorizontalPunches == 0 && mVerticalPunches == 0)
		LogicExit();
}
void Tuto::LogicExit()
{
	mFinalBoss->state_machine->ChangeState("Rest");
	mFinalBoss->Superstate = FinalBoss::SuperState::First_Half;
	Initialized = false;
}

void RandomState::LogicEnter() {}
void RandomState::LogicUpdate()
{
	if (!Initialized)
	{
		mFinalBoss = static_cast<FinalBoss*>(mActor);
		timer = 0.f;
		Initialized = true;
		mLastHorizontalPunch = 0.f;
		mLastVerticalPunch = 0.f;
		//TO UPDATE
		mHorizontalPunches = 6;
		mVerticalPunches = 18;
		mTimebtwHorizontal = 2.5f;
		mTimebtwVertical = 1.1f;
	}
	//HORIZONTAL PUNCH
	if (mHorizontalPunches > 0 && timer - mLastHorizontalPunch > mTimebtwHorizontal)
	{
		HorizontalLaunchPos = mFinalBoss->HorizontalPunchLogic();
		mHorizontalPunches--;
		mLastHorizontalPunch = timer;
		mFinalBoss->horizontal_punched = false;
	}

	if (mFinalBoss->mWarning_time + 1.f < mFinalBoss->timer && !mFinalBoss->horizontal_punched)
	{
		mFinalBoss->HorizontalPunch(HorizontalLaunchPos);
		mFinalBoss->horizontal_punched = true;
	}
	//VERTICAL PUNCH
	if (mVerticalPunches > 0 && timer - mLastVerticalPunch > mTimebtwVertical)
	{
		mFinalBoss->VerticalPunch();
		mVerticalPunches--;
		mLastVerticalPunch = timer;
	}

	timer += (f32)aexTime->GetFrameTime();

	if (mHorizontalPunches == 0 && mVerticalPunches == 0)
		LogicExit();
}
void RandomState::LogicExit()
{
	mFinalBoss->state_machine->ChangeState("Entry");
	mFinalBoss->Superstate = FinalBoss::SuperState::Second_Half;
	Initialized = false;
}

HoriPunch::HoriPunch()
{
	on_Path1 = false;
	on_Path2 = false;
	on_Middle = false;
	punched_1 = 100.f;
	check = false;
}
void HorizontalPunches::LogicEnter() {}
void HorizontalPunches::LogicUpdate()
{
	if (!Initialized)
	{
		mFinalBoss = static_cast<FinalBoss*>(mActor);
		timer = 0.f;
		Initialized = true;
		mLastHorizontalPunch = 0.f;
		mLastVerticalPunch = 0.f;
		stop = false;
		//TO UPDATE
		if (mFinalBoss->Superstate == FinalBoss::SuperState::First_Half)
		{
			mHorizontalPunches = 4;
			mTimebtwHorizontal = 4.f;
		}
		if (mFinalBoss->Superstate == FinalBoss::SuperState::Second_Half)
		{
			mHorizontalPunches = 6;
			mTimebtwHorizontal = 2.f;
		}
	}
	//HORIZONTAL PUNCH
	if (mHorizontalPunches > 0 && timer - mLastHorizontalPunch > mTimebtwHorizontal && !stop)
	{
		FirstPunch();
		mHorizontalPunches--;
		mLastHorizontalPunch = timer;
	}
	CheckLives();
	CameraShake();
	timer += (f32)aexTime->GetFrameTime();

	for (auto it = all_punches.begin(); it != all_punches.end(); it++)
	{
		if ((*it)->go->GetComp<PathFollower>()->GetTime() > (*it)->go->GetComp<PathComponent>()->Duration() && (*it)->on_Path1 && (*it)->check == false)
		{
			(*it)->punched_1 = timer;
			(*it)->check = true;
			audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\FinalBoss\\FinalBossHit.wav"), Voice::SFX);
		}
	}

	
	for (auto it = all_punches.begin(); it != all_punches.end(); it++)
	{
		if (timer - (*it)->punched_1 > 0.8f && (*it)->on_Middle && !(*it)->on_Path2)
			SecondPunch(*it);
		if (timer - (*it)->punched_1 < 0.8f &&  timer - (*it)->punched_1 > 0.5f && (*it)->on_Path1 && !(*it)->on_Middle)
		{
			if((*it)->go->GetComp<PathFollower>()->GetTime() > (*it)->go->GetComp<PathComponent>()->Duration())
				Middle((*it));
		}
	}

	if (all_punches.size() > 0)
	{
		if (all_punches.back()->go->GetComp<PathFollower>()->GetTime() > all_punches.back()->go->GetComp<PathComponent>()->Duration() && mHorizontalPunches == 0 && all_punches.back()->on_Path2)
		{
			LogicExit();
		}
	}
	if (all_punches.size() == 0 && stop)
		LogicExit();

}
void HorizontalPunches::CameraShake()
{
	for (auto it = all_punches.begin(); it != all_punches.end(); it++)
	{
		if ((*it)->go->GetComp<TransformComp>())
		{
			if (aexScene->FindObjectInANYSpace("player")->GetComp<TransformComp>())
			{
				if (AEVec2(aexScene->FindObjectInANYSpace("player")->GetComp<TransformComp>()->GetPosition3D().x, aexScene->FindObjectInANYSpace("player")->GetComp<TransformComp>()->GetPosition3D().y).Distance
				(AEVec2((*it)->go->GetComp<TransformComp>()->GetPosition3D().x, (*it)->go->GetComp<TransformComp>()->GetPosition3D().y)) < 7.f)
				{
					mFinalBoss->cs->mTrauma = 0.9f;
					if (!(*it)->go->GetComp<PunchLogic>()->sound_sounded)
					{
						audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\FinalBoss\\fastCar2.mp3"), Voice::SFX);
						(*it)->go->GetComp<PunchLogic>()->sound_sounded = true;
					}
				}
			}
		}
	}
}

void HorizontalPunches::CheckLives()
{
	if (mFinalBoss->mLives <= 20 && mFinalBoss->Superstate == FinalBoss::SuperState::First_Half)
	{
		stop = true;
		mHorizontalPunches = 0;
	}

}
AEVec2 HorizontalPunches::SpawnPoint()
{
	AEVec2 randompos;

	if (RNG->Get0or1()) // right side
	{
		randompos.x = RNG->GetFloat(mFinalBoss->mCamWidth.y - 6.f, mFinalBoss->mCamWidth.y - 1.f);
	}
	else // left side
	{
		randompos.x = RNG->GetFloat(mFinalBoss->mCamWidth.x + 1.f, mFinalBoss->mCamWidth.x + 6.f);
	}
	randompos.y = mFinalBoss->mCamHeight.y; // max y

	return randompos;
}

void HorizontalPunches::FirstPunch()
{
	AEVec2 launchPos = SpawnPoint();
	HoriPunch* hp = new HoriPunch;
	hp->go = mFinalBoss->mOwner->GetParentSpace()->InstantiateArchetype("VerticalPunch", AEVec3(launchPos.x, launchPos.y, 0));
	hp->go->GetComp< PunchLogic>()->is_vertical = true;
	PathComponent* path = new PathComponent;
	path->Push(0.f, AEVec2(launchPos.x, launchPos.y));
	path->Push(0.5f, AEVec2(path->LastPoint().x, path->LastPoint().y - (mFinalBoss->mCamHeight.y - mFinalBoss->mCamHeight.x - 1.4f)));
	if (path->LastPoint().x > mFinalBoss->mCamWidth.x + (mFinalBoss->mCamWidth.y - mFinalBoss->mCamWidth.x) / 2) // is it at the right or left
		hp->go->GetComp<SpineAnimationComp>()->ChangeAnimation(0, "Barrida Horizontal", false, 0.35f, .3f);
	else
		hp->go->GetComp<SpineAnimationComp>()->ChangeAnimation(0, "Barrida horizontal OtherSide", false, 0.35f, .3f);
	hp->go->AddComp(path);
	hp->on_Path1 = true;
	all_punches.push_back(hp);
}
void HorizontalPunches::Middle(HoriPunch* hp)
{
	hp->go->RemoveComp(hp->go->GetComp<PathComponent>(), false);
	hp->go->GetComp<PathFollower>()->timer = 0.f;
	PathComponent* path = new PathComponent;
	path->Push(0.f, AEVec2(hp->go->GetComp<TransformComp>()->GetPosition3D().x, hp->go->GetComp<TransformComp>()->GetPosition3D().y));
	if (path->LastPoint().x > mFinalBoss->mCamWidth.x + (mFinalBoss->mCamWidth.y - mFinalBoss->mCamWidth.x) / 2) // is it at the right or left
		path->Push(1.f, AEVec2(path->LastPoint().x + 3.f, path->LastPoint().y));
	else
		path->Push(1.f, AEVec2(path->LastPoint().x - 3.f, path->LastPoint().y));
	hp->go->AddComp(path);
	hp->on_Middle = true;
}
void HorizontalPunches::SecondPunch(HoriPunch* hp)
{
	hp->go->RemoveComp(hp->go->GetComp<PathComponent>(), false);
	hp->go->GetComp<PathFollower>()->timer = 0.f;
	PathComponent* path = new PathComponent;
	path->Push(0.f, AEVec2(hp->go->GetComp<TransformComp>()->GetPosition3D().x, hp->go->GetComp<TransformComp>()->GetPosition3D().y));
	if (path->LastPoint().x > mFinalBoss->mCamWidth.x + (mFinalBoss->mCamWidth.y - mFinalBoss->mCamWidth.x) / 2) // is it at the right or left
		path->Push(1.f, AEVec2(path->LastPoint().x - (mFinalBoss->mCamWidth.y - mFinalBoss->mCamWidth.x) - 2.f, path->LastPoint().y));
	else
		path->Push(1.f, AEVec2(path->LastPoint().x + (mFinalBoss->mCamWidth.y - mFinalBoss->mCamWidth.x) + 2.f, path->LastPoint().y));
	hp->go->AddComp(path);
	hp->on_Path2 = true;
}

void HorizontalPunches::LogicExit()
{
	mFinalBoss->state_machine->ChangeState("Rest");
	all_punches.resize(0);
	Initialized = false;
}

void FromLeft2Right::LogicEnter()
{
}

void FromLeft2Right::LogicUpdate()
{
	if (!Initialized)
	{
		mFinalBoss = static_cast<FinalBoss*>(mActor);
		timer = 0.f;
		mOffset = 0.f;
		Initialized = true;
		mLastVerticalPunch = 0.f;
		the_End = false;
		mLastPunchFInished = false;
		//TO UPDATE
		if(mFinalBoss->Superstate == FinalBoss::SuperState::First_Half)
			mTimebtwVertical = 0.4f;
		if (mFinalBoss->Superstate == FinalBoss::SuperState::Second_Half)
			mTimebtwVertical = 0.3f;
	}

	if (timer > mTimebtwVertical && the_End == false)
	{
		AEVec2 launchPos = ComputeStartingPoint();
		GameObject* v_punch = mFinalBoss->mOwner->GetParentSpace()->InstantiateArchetype("VerticalPunch", AEVec3(launchPos.x, launchPos.y, 0));
		v_punch->GetComp< PunchLogic>()->is_vertical = true;
		PathComponent* path = new PathComponent;
		v_punch->GetComp<SpineAnimationComp>()->ChangeAnimation(0, "Barrida Vertical Random", false, 0.35f, .3f);
		path->Push(0.f, AEVec2(launchPos.x, launchPos.y));
		path->Push(0.4f, AEVec2(path->LastPoint().x, path->LastPoint().y - (mFinalBoss->mCamHeight.y - mFinalBoss->mCamHeight.x - 1.5f)));
		v_punch->AddComp(path);
		this_punches.push_back(v_punch);
		timer = 0.f;
	}
	for (auto it = this_punches.begin(); it != this_punches.end();it++ )
	{
		if ((*it)->GetComp<PathFollower>()->GetTime() > (*it)->GetComp<PathComponent>()->Duration())
		{
			(*it)->RemoveComp((*it)->GetComp<BoxCollider>());
		}
	}
	CameraShake();
	timer += (f32)aexTime->GetFrameTime();
	if (mFinalBoss->mCamWidth.x + mOffset > mFinalBoss->mCamWidth.y - 6.0f)
	{
		the_End = true;

		if (this_punches.back()->GetComp<PathFollower>()->GetTime() > this_punches.back()->GetComp<PathComponent>()->Duration())
		{
			if (!mLastPunchFInished)
				time_the_end = timer;
			mLastPunchFInished = true;
			if (timer - time_the_end > 1.f) // un poco de delay para eliminar los punches
			{
				for (auto it = this_punches.begin(); it != this_punches.end(); )
				{
					mFinalBoss->mOwner->GetParentSpace()->DestroyObject(*it);
					it = this_punches.erase(it);
				}

				LogicExit();
			}
		}
	}
}
void FromLeft2Right::CameraShake()
{
	for (auto it = this_punches.begin(); it != this_punches.end(); it++)
	{
		if ((*it)->GetComp<TransformComp>())
		{
			if (aexScene->FindObjectInANYSpace("player")->GetComp<TransformComp>())
			{
				if (AEVec2(aexScene->FindObjectInANYSpace("player")->GetComp<TransformComp>()->GetPosition3D().x, aexScene->FindObjectInANYSpace("player")->GetComp<TransformComp>()->GetPosition3D().y).Distance
				(AEVec2((*it)->GetComp<TransformComp>()->GetPosition3D().x, (*it)->GetComp<TransformComp>()->GetPosition3D().y)) < 7.f)
					mFinalBoss->cs->mTrauma = 0.9f;
			}
		}
	}
}
AEVec2 FromLeft2Right::ComputeStartingPoint()
{
	AEVec2 StartingPoint;
	StartingPoint.x = mFinalBoss->mCamWidth.x + mOffset;
	StartingPoint.y = mFinalBoss->mCamHeight.y;
	mOffset += 3.f;
	return StartingPoint;
}
void FromLeft2Right::LogicExit()
{
	mFinalBoss->state_machine->ChangeState("Rest");
	this_punches.resize(0);
	Initialized = false;
}

void FromRight2Left::LogicEnter()
{
}

void FromRight2Left::LogicUpdate()
{
	if (!Initialized)
	{
		mFinalBoss = static_cast<FinalBoss*>(mActor);
		timer = 0.f;
		Initialized = true;
		mOffset = 0.f;
		mLastVerticalPunch = 0.f;
		the_End = false;
		mLastPunchFInished = false;
		//TO UPDATE
		if (mFinalBoss->Superstate == FinalBoss::SuperState::First_Half)
			mTimebtwVertical = 0.4f;
		if (mFinalBoss->Superstate == FinalBoss::SuperState::Second_Half)
			mTimebtwVertical = 0.3f;
	}

	if (timer > mTimebtwVertical && the_End == false)
	{
		AEVec2 launchPos = ComputeStartingPoint();
		GameObject* v_punch = mFinalBoss->mOwner->GetParentSpace()->InstantiateArchetype("VerticalPunch", AEVec3(launchPos.x, launchPos.y, 0));
		v_punch->GetComp< PunchLogic>()->is_vertical = true;
		PathComponent* path = new PathComponent;
		v_punch->GetComp<SpineAnimationComp>()->ChangeAnimation(0, "Barrida Vertical Random", false, 0.35f, .3f);
		path->Push(0.f, AEVec2(launchPos.x, launchPos.y));
		path->Push(0.4f, AEVec2(path->LastPoint().x, path->LastPoint().y - (mFinalBoss->mCamHeight.y - mFinalBoss->mCamHeight.x - 1.5f)));
		v_punch->AddComp(path);
		this_punches.push_back(v_punch);
		timer = 0.f;
	}
	for (auto it = this_punches.begin(); it != this_punches.end(); it++)
	{
		if ((*it)->GetComp<PathFollower>()->GetTime() > (*it)->GetComp<PathComponent>()->Duration())
		{
			(*it)->RemoveComp((*it)->GetComp<BoxCollider>());
		}
	}
	timer += (f32)aexTime->GetFrameTime();
	CameraShake();
	if (mFinalBoss->mCamWidth.y - mOffset < mFinalBoss->mCamWidth.x + 8.0f)
	{
		the_End = true;
		if (this_punches.back()->GetComp<PathFollower>()->GetTime() > this_punches.back()->GetComp<PathComponent>()->Duration())
		{
			if (!mLastPunchFInished)
				time_the_end = timer;
			mLastPunchFInished = true;
			if (timer - time_the_end > 1.f) // un poco de delay para eliminar los punches
			{
				for (auto it = this_punches.begin(); it != this_punches.end(); )
				{
					mFinalBoss->mOwner->GetParentSpace()->DestroyObject(*it);
					it = this_punches.erase(it);
				}

				LogicExit();
			}
		}
	}
}
void FromRight2Left::CameraShake()
{
	for (auto it = this_punches.begin(); it != this_punches.end(); it++)
	{
		if ((*it)->GetComp<TransformComp>())
		{
			if (aexScene->FindObjectInANYSpace("player")->GetComp<TransformComp>())
			{
				if (AEVec2(aexScene->FindObjectInANYSpace("player")->GetComp<TransformComp>()->GetPosition3D().x, aexScene->FindObjectInANYSpace("player")->GetComp<TransformComp>()->GetPosition3D().y).Distance
				(AEVec2((*it)->GetComp<TransformComp>()->GetPosition3D().x, (*it)->GetComp<TransformComp>()->GetPosition3D().y)) < 7.f)
					mFinalBoss->cs->mTrauma = 0.9f;
			}
		}
	}
}
AEVec2 FromRight2Left::ComputeStartingPoint()
{
	AEVec2 StartingPoint;
	StartingPoint.x = mFinalBoss->mCamWidth.y - mOffset;
	StartingPoint.y = mFinalBoss->mCamHeight.y;
	mOffset += 3.f;
	return StartingPoint;
}
void FromRight2Left::LogicExit()
{
	mFinalBoss->state_machine->ChangeState("Rest");
	Initialized = false;
}

void FromOut2Center::LogicEnter() {}


void FromOut2Center::LogicUpdate() {
	if(!Initialized)
	{
		mFinalBoss = static_cast<FinalBoss*>(mActor);
		timer = 0.f;
		Initialized = true;
		mOffset = (mFinalBoss->mCamWidth.y - mFinalBoss->mCamWidth.x) / 2 -4.f;//paq no empiece desde el fondo
		mLastVerticalPunch = 0.f;
		the_End = false;
		mLastPunchFInished = false;
		//TO UPDATE
		if (mFinalBoss->Superstate == FinalBoss::SuperState::First_Half)
			mTimebtwVertical = 0.25f;
		if (mFinalBoss->Superstate == FinalBoss::SuperState::Second_Half)
			mTimebtwVertical = 0.2f;
		mMiddlePoint = AEVec2(mFinalBoss->mCamWidth.x + (mFinalBoss->mCamWidth.y - mFinalBoss->mCamWidth.x) / 2, mFinalBoss->mCamHeight.y);
	}

	if (timer > mTimebtwVertical && the_End == false)
	{
		GameObject* v_punch = mFinalBoss->mOwner->GetParentSpace()->InstantiateArchetype("BossBullet", AEVec3(mMiddlePoint.x + mOffset, mFinalBoss->mOwner->GetComp<TransformComp>()->GetPosition3D().y + 9.f, 0), -3.14f/2);
		PathComponent* path = new PathComponent;
		path->Push(0.f, AEVec2(mMiddlePoint.x + mOffset, mFinalBoss->mOwner->GetComp<TransformComp>()->GetPosition3D().y + 9.f));
		path->Push(0.8f, AEVec2(path->LastPoint().x, path->LastPoint().y - (mFinalBoss->mCamHeight.y - mFinalBoss->mCamHeight.x)));
		v_punch->AddComp(path);
		this_punches.push_back(v_punch);
		if (mOffset > 0.5f) // middle punch
		{
			GameObject* v_punch2 = mFinalBoss->mOwner->GetParentSpace()->InstantiateArchetype("BossBullet", AEVec3(mMiddlePoint.x - mOffset, mFinalBoss->mOwner->GetComp<TransformComp>()->GetPosition3D().y + 9.0f, 0), -3.14f / 2);
			PathComponent* path2 = new PathComponent;
			path2->Push(0.f, AEVec2(mMiddlePoint.x - mOffset, mFinalBoss->mOwner->GetComp<TransformComp>()->GetPosition3D().y + 9.f));
			path2->Push(0.8f, AEVec2(path2->LastPoint().x, path2->LastPoint().y - (mFinalBoss->mCamHeight.y - mFinalBoss->mCamHeight.x)));
			v_punch2->AddComp(path2);
			this_punches.push_back(v_punch2);
		}
		mOffset -= 2.f;
		timer = 0.f;
	}

	timer += (f32)aexTime->GetFrameTime();
	if (mOffset < 0.0f)
	{
		the_End = true;
		if (this_punches.size() == 0)
		{
			mFinalBoss->state_machine->ChangeState("Rest");
		}
	}

	
	for (auto it = this_punches.begin(); it != this_punches.end(); )
	{
		if ((*it)->GetComp<PathFollower>()->GetTime() > (*it)->GetComp<PathComponent>()->Duration())
		{
			mFinalBoss->mOwner->GetParentSpace()->DestroyObject(*it);
			it = this_punches.erase(it);
		}
		else
			it++;
	}
	

}

void FromOut2Center::LogicExit() {
	mFinalBoss->state_machine->ChangeState("Rest");
	Initialized = false;
	this_punches.resize(0);
}

void TargetPlayer::LogicEnter()
{
}

void TargetPlayer::LogicUpdate()
{
	if (!Initialized)
	{
		mFinalBoss = static_cast<FinalBoss*>(mActor);
		Tpunch1 = NULL;
		Tpunch2 = NULL;
		Tpunch3 = NULL;
		timer = 0.f;
		delay = 0.1f;
		Initialized = true;
		mLastVerticalPunch = 0.f;
		mPlayerTransform = aexScene->FindObjectInANYSpace("player")->GetComp<TransformComp>();
		mPlayerPos = AEVec2(mPlayerTransform->GetPosition3D().x, mPlayerTransform->GetPosition3D().y);
		mFinalBoss->mShield = true;
		mFinalBoss->spineAnimation->ChangeAnimation(0, "Into The Darkness", false, 0.5f, 0.3f);
		//TO UPDATE
		if (mFinalBoss->Superstate == FinalBoss::SuperState::First_Half)
		{
			Tpunch1 = new TargetPunches;
			Tpunch1->go = mFinalBoss->mOwner->GetParentSpace()->InstantiateArchetype("VerticalPunch", AEVec3(mPlayerPos.x, mFinalBoss->mCamHeight.y - 7.f, 0));
			Tpunch1->go->GetComp<SpineAnimationComp>()->ChangeAnimation(0, "Targeting", true, 0.35f, .3f);
			Tpunch1->go->GetComp< PunchLogic>()->is_vertical = true;
			Tpunch1->RandomTargets = RNG->GetInt(2, 5);
			Tpunch1->label = 1;
			MAXRandomTargets = Tpunch1->RandomTargets;
			MAX_T = Tpunch1;
			all_punches.push_back(Tpunch1);
		}
		if (mFinalBoss->Superstate == FinalBoss::SuperState::Second_Half)
		{
			Tpunch1 = new TargetPunches;
			Tpunch1->go = mFinalBoss->mOwner->GetParentSpace()->InstantiateArchetype("VerticalPunch", AEVec3(mPlayerPos.x, mFinalBoss->mCamHeight.y - 5.f, -0.01f));
			Tpunch1->go->GetComp< PunchLogic>()->is_vertical = true;
			Tpunch1->label = 1;
			Tpunch1->go->GetComp<SpineAnimationComp>()->ChangeAnimation(0, "Targeting", true, 0.35f, .3f);
			Tpunch2 = new TargetPunches;
			Tpunch2->go = mFinalBoss->mOwner->GetParentSpace()->InstantiateArchetype("VerticalPunch", AEVec3(mPlayerPos.x, mFinalBoss->mCamHeight.y - 5.0f, 0.0f));
			Tpunch2->go->GetComp< PunchLogic>()->is_vertical = true;
			Tpunch2->label = 2;
			Tpunch2->go->GetComp<SpineAnimationComp>()->ChangeAnimation(0, "Targeting", true, 0.35f, .3f);
			Tpunch3 = new TargetPunches;
			Tpunch3->go = mFinalBoss->mOwner->GetParentSpace()->InstantiateArchetype("VerticalPunch", AEVec3(mPlayerPos.x, mFinalBoss->mCamHeight.y - 5.f, -0.02f));
			Tpunch3->go->GetComp< PunchLogic>()->is_vertical = true;
			Tpunch3->label = 3;
			Tpunch3->go->GetComp<SpineAnimationComp>()->ChangeAnimation(0, "Targeting", true, 0.35f, .3f);
			all_punches.push_back(Tpunch1);
			all_punches.push_back(Tpunch2);
			all_punches.push_back(Tpunch3);
			RandomFake();

			if (Tpunch1->RandomTargets >= Tpunch2->RandomTargets)
			{
				MAXRandomTargets = Tpunch1->RandomTargets;
				MAX_T = Tpunch1;
				if (Tpunch1->RandomTargets < Tpunch3->RandomTargets)
				{
					MAXRandomTargets = Tpunch3->RandomTargets;
					MAX_T = Tpunch3;
				}
			}
			else if(Tpunch2->RandomTargets >= Tpunch3->RandomTargets)
			{
				MAXRandomTargets = Tpunch2->RandomTargets;
				MAX_T = Tpunch2;
			}
			else
			{
				MAXRandomTargets = Tpunch3->RandomTargets;
				MAX_T = Tpunch3;
			}
		}
	}

	mPlayerPos = AEVec2(mPlayerTransform->GetPosition3D().x, mPlayerTransform->GetPosition3D().y);
	CameraShake();
	timer += (f32)aexTime->GetFrameTime();
	
	// delete the path if finsihed
	for (auto it = all_punches.begin(); it != all_punches.end(); it++)
	{
		if ((*it)->done)
			continue;
		if ((*it)->anticipation)
		{
			if ((*it)->go->GetComp<PathComponent>())
			{
				if ((*it)->go->GetComp<PathFollower>()->GetTime() > (*it)->go->GetComp<PathComponent>()->Duration() && !(*it)->punched)
				{
					(*it)->go->RemoveComp((*it)->go->GetComp<PathComponent>());
					(*it)->go->GetComp<PathFollower>()->timer = 0.f;
					PathComponent* path = new PathComponent;
					// PABAJO
					path->Push(0.f, AEVec2((*it)->go->GetComp<TransformComp>()->GetPosition3D().x, (*it)->go->GetComp<TransformComp>()->GetPosition3D().y));
					path->Push(0.37f, AEVec2(path->LastPoint().x, mFinalBoss->mCamHeight.y - (mFinalBoss->mCamHeight.y - mFinalBoss->mCamHeight.x - 1.5f)));
					(*it)->go->GetComp<SpineAnimationComp>()->ChangeAnimation(0, "Barrida Vertical Random", false, 0.35f, .3f);
					(*it)->go->AddComp(path);
					(*it)->punched = true;
				}
			}

			if ((*it)->punched)
			{
				if ((*it)->go->GetComp<PathFollower>()->GetTime() > (*it)->go->GetComp<PathComponent>()->Duration())
				{
					audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\FinalBoss\\FinalBossHit.wav"), Voice::SFX);
					(*it)->mTargetCounts++;
					(*it)->done = true;
					(*it)->go->RemoveComp((*it)->go->GetComp<BoxCollider>());
					continue;
				}
			}
		}
		else
		{
			if (timer > 1.8f && (*it)->mTargetCounts < (*it)->RandomTargets)
				TargetthePlayer(*it);

			if ((*it)->go)
			{
				if ((*it)->go->GetComp<PathComponent>())
				{
					if ((*it)->go->GetComp<PathFollower>()->GetTime() > (*it)->go->GetComp<PathComponent>()->Duration())
					{
						(*it)->go->RemoveComp((*it)->go->GetComp<PathComponent>(), false);
						(*it)->mTargetCounts++;
						(*it)->go->GetComp<PathFollower>()->timer = 0.f;
					}
				}
			}
			if ((*it)->mTargetCounts == (*it)->RandomTargets)
			{
				if ((*it)->time_to_punch == 0.f)
					(*it)->time_to_punch = timer;
			}

			if (timer - (*it)->time_to_punch > delay && (*it)->mTargetCounts == (*it)->RandomTargets && !(*it)->punched)
			{
				Punch(*it);
			}
		}
	}
	if (targetted) // reset timer after targetting the player
	{
		timer = 0.f;
		targetted = false;
	}

	if (MAX_T->mTargetCounts == MAXRandomTargets + 1)
		if(MAX_T->go->GetComp<SpineAnimationComp>()->GetAnimState()->getCurrent(0)->isComplete())
			mFinalBoss->state_machine->ChangeState("HorizontalPunches");
}

void TargetPlayer::TargetthePlayer(TargetPunches* punch)
{
	PathComponent* path1 = new PathComponent;
	path1->Push(0.f, AEVec2(punch->go->GetComp<TransformComp>()->GetPosition3D().x, punch->go->GetComp<TransformComp>()->GetPosition3D().y));
	switch (punch->label)
	{
	case 1:
		path1->Push(0.6f, AEVec2(mPlayerPos.x, path1->LastPoint().y));
		break;
	case 2:
		path1->Push(0.6f, AEVec2(mPlayerPos.x + 2.5f, path1->LastPoint().y));
		break;
	case 3:
		path1->Push(0.6f, AEVec2(mPlayerPos.x - 2.5f, path1->LastPoint().y));
		break;
	}
	punch->go->AddComp(path1);

	targetted = true;
}

void TargetPlayer::Punch(TargetPunches* punch)
{
	PathComponent* path = new PathComponent;
	//ANTINCIPASAO
	path->Push(0.f, AEVec2(punch->go->GetComp<TransformComp>()->GetPosition3D().x, punch->go->GetComp<TransformComp>()->GetPosition3D().y));
	path->Push(.2f, AEVec2(path->LastPoint().x, path->LastPoint().y + 4.f));
	punch->go->GetComp<SpineAnimationComp>()->ChangeAnimation(0, "Antincipacion", false, 1.f, .3f);
	
	punch->go->AddComp(path);
	punch->anticipation = true;
}
void TargetPlayer::CameraShake()
{
	for (auto it = all_punches.begin(); it != all_punches.end(); it++)
	{
		if ((*it)->done)
			continue;
		if ((*it)->go->GetComp<TransformComp>())
		{
			if (aexScene->FindObjectInANYSpace("player")->GetComp<TransformComp>())
			{
				if (AEVec2(aexScene->FindObjectInANYSpace("player")->GetComp<TransformComp>()->GetPosition3D().x, aexScene->FindObjectInANYSpace("player")->GetComp<TransformComp>()->GetPosition3D().y).Distance
				(AEVec2((*it)->go->GetComp<TransformComp>()->GetPosition3D().x, (*it)->go->GetComp<TransformComp>()->GetPosition3D().y)) < 7.f)
					mFinalBoss->cs->mTrauma = 0.9f;
			}
		}
	}
}

void TargetPlayer::RandomFake()
{
	if (RNG->Get0or1())
	{
		switch (RNG->GetInt(0, 3))
		{
		default:
			Tpunch1->RandomTargets = 3;
			Tpunch2->RandomTargets = 4;
			Tpunch3->RandomTargets = 3;
			break;
		case 2:
			Tpunch1->RandomTargets = 2;
			Tpunch2->RandomTargets = 2;
			Tpunch3->RandomTargets = 2;
			break;
		case 3:
			Tpunch1->RandomTargets = 3;
			Tpunch2->RandomTargets = 4;
			Tpunch3->RandomTargets = 3;
			break;
		}
	}
	else
	{
		switch (RNG->GetInt(0, 3))
		{
		default:
			Tpunch1->RandomTargets = 4;
			Tpunch2->RandomTargets = 4;
			Tpunch3->RandomTargets = 4;
			break;
		case 2:
			Tpunch1->RandomTargets = 2;
			Tpunch2->RandomTargets = 4;
			Tpunch3->RandomTargets = 2;
			break;
		case 3:
			Tpunch1->RandomTargets = 3;
			Tpunch2->RandomTargets = 2;
			Tpunch3->RandomTargets = 4;
			break;
		}
	}
}

void TargetPlayer::LogicExit()
{
	mFinalBoss->state_machine->ChangeState("Entry");
	mFinalBoss->mOwner->GetParentSpace()->DestroyObject(Tpunch1->go);
	if (Tpunch2)
	{
		mFinalBoss->mOwner->GetParentSpace()->DestroyObject(Tpunch2->go);
	}
	if (Tpunch3)
	{
		mFinalBoss->mOwner->GetParentSpace()->DestroyObject(Tpunch3->go);
	}
	Initialized = false;
}
#pragma endregion


TargetPunches::TargetPunches()
{
	mTargetCounts = 0;
	time_to_punch = 0.f;
	punched = false;
	go = new GameObject;
	done =  false;
	anticipation = false;
}

void LifeTime::Initialize()
{
	timer = 0.f;
}
void LifeTime::Update()
{
	timer += (f32)aexTime->GetFrameTime();
	if (timer > time_to_die)
	{
		mOwner->GetParentSpace()->DestroyObject(mOwner);
	}
}
bool LifeTime::OnGui()
{
	ImGui::InputFloat("Time to die", &time_to_die);
	return 0;
}

void LifeTime::FromJson(json& value)
{
	this->LogicComp::FromJson(value);
	value["Time to die"] >> time_to_die;
}
void LifeTime::ToJson(json& value)
{
	this->LogicComp::ToJson(value);
	value["Time to die"] << time_to_die;
}

void BossBulletStruct::OnCollisionEvent(const CollisionEvent& collision)
{
	if (collision.otherObject->GetBaseNameString() == "player")
	{
		PlayerStats* stats = collision.otherObject->GetComp<PlayerStats>();

		if (!stats->isInvincible)
		{
			stats->lives--;
			stats->isHit = true;
		}
	}
}