#pragma once

#include "Enemy.h"

class SpineAnimationComp;

namespace AEX
{
	class EnemyKamikaze;

	struct SM_KamikazeEnemy_Resting : public State
	{
		SM_KamikazeEnemy_Resting(const char * name);

		void LogicUpdate();
	};

	struct SM_KamikazeEnemy_Moving : public State
	{
		SM_KamikazeEnemy_Moving(const char * name);

		void LogicUpdate();

		void LogicEnter();

		PathComponent * mPath;
		void CreateDummyPath();
		
		f32 time;
		
		bool Initialized;		

		AEVec2 pos;

		EnemyKamikaze * save;
	};

	struct SM_Kamikaze_Aim : public State
	{
		SM_Kamikaze_Aim(const char * name);

		void LogicUpdate();

		float timePassed = 0.f;

		float maxTimePassed = 0.75f;

		EnemyKamikaze * save;
	};

	struct SM_Kamikaze_MoveTowardsPlayer : public State
	{
		SM_Kamikaze_MoveTowardsPlayer(const char * name);

		void LogicUpdate();

		bool rotated;

		AEVec2 Direction;

		EnemyKamikaze * save;
	};

	struct SM_Kamikaze_Die : public State
	{
		SM_Kamikaze_Die(const char * name);

		void LogicUpdate();
	};


	class EnemyKamikaze : public Enemy, public Actor
	{
		AEX_RTTI_DECL(EnemyKamikaze, Enemy)

	public:

		void Initialize();
		void Update();
		void ShutDown();
		bool OnGui();

		bool doneOnce;

		float maxTimePassed;
		float animationSpeed = 5.f;

		void SM_Init();

		virtual void OnCollisionEvent(const CollisionEvent& collision);

		void FromJson(json & value);
		void ToJson(json & value);

		EaseFunc mEaseFunction = EaseLinear;
		const char * mEaseFuncName = "EaseLinear";

		SpineAnimationComp* spineAnimation;
	};
}