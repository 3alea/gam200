#pragma once

#include "Enemy.h"
#include "EnemyBullet.h"
#include <string>

// Forward declare spine animation class
class SpineAnimationComp;

namespace AEX
{
	class EnemyFlying;

	struct SM_FlyingEnemy_Resting : public State
	{
		SM_FlyingEnemy_Resting(const char * name);

		void LogicUpdate();
	};

	struct SM_FlyingEnemy_Moving : public State
	{
		SM_FlyingEnemy_Moving(const char * name);

		void LogicUpdate();
		void LogicEnter();

		PathComponent * mPath;

		f32 time;

		bool Initialized;

		AEVec2 pos;

		EnemyFlying * save;
	};

	struct SM_FlyingEnemy_Shooting : public State
	{
		SM_FlyingEnemy_Shooting(const char * name);

		void LogicUpdate();
		void LogicEnter();

		unsigned i;

		bool rotated;

		AEVec2 Direction;

		bool FirstShot = true;

		EnemyFlying * save;
	};

	struct SM_FlyingEnemy_Die : public State
	{
		SM_FlyingEnemy_Die(const char * name);

		void LogicUpdate();
		void LogicEnter();

		EnemyFlying * save;
	};

	class EnemyFlying : public Enemy, public Actor
	{
		AEX_RTTI_DECL(EnemyFlying, Enemy)

	public:

		void Initialize();
		void Update();

		bool OnGui();

		void SM_Init();

		virtual void OnCollisionEvent(const CollisionEvent& collision);

		void FromJson(json & value);
		void ToJson(json & value);

		f64 StartTimeBetweenShots = 3.0f;

		f64 TimeBetweenShots = StartTimeBetweenShots;

		f64 TimeFirstShot = 0.5f;

		// Spine animation
		SpineAnimationComp* spineAnimation;

		// Added by Miguel
		bool mOnCinematic = false;

		// Shoot ANIMATION delay
		float shootDelay = 0.f;

		bool init = true;
	};
}