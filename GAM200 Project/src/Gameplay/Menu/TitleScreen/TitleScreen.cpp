#include "TitleScreen.h"
#include "Platform\AEXInput.h"
#include "Composition\AEXScene.h"


namespace AEX
{
	void TitleScreenLogic::Update()
	{
		if (aexInput->AnyKeyTriggered() || aexInput->MouseTriggered(Input::eMouseLeft)
			|| aexInput->MouseTriggered(Input::eMouseRight) || aexInput->MouseTriggered(Input::eMouseMiddle)
			|| aexInput->GamepadButtonTriggered(Input::eGamepadButtonA) || aexInput->GamepadButtonTriggered(Input::eGamepadButtonStart))
			aexScene->ChangeLevel("data/Levels/Main Menu.json");
	}

	void TitleScreenLogic::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);
	}

	void TitleScreenLogic::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);
	}
}