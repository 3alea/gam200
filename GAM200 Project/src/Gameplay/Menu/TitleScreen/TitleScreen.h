#pragma once
#include "Components\Logic.h"


namespace AEX
{

	class TitleScreenLogic : public LogicComp
	{
		AEX_RTTI_DECL(TitleScreenLogic, LogicComp);

	public:

		virtual void Update();

		void FromJson(json & value);
		void ToJson(json & value);

	private:
	
	};

}