#include "Fader.h"
#include "Graphics\GfxMgr.h"
#include "Platform/AEXTime.h"
#include "Components\SpriteComps.h"
#include "Composition/AEXScene.h"

namespace AEX
{
	void Fader::Initialize()
	{
		sprite = mOwner->GetComp<SpriteComp>();
		//sprite->mColor[3] = 1.0f;

		mTr = mOwner->GetComp<TransformComp>();

		GameObject * hudCamObj = aexScene->FindObjectInANYSpace("CameraHUD");

		if (hudCamObj)
			mCamTr = hudCamObj->GetComp<TransformComp>();
	}

	void Fader::Update()
	{
		if (mDoneOnce == false)
		{
			Initialize();
			mDoneOnce = true;
		}
		
		if (mCamTr)
		{
			mTr->mLocal.mTranslationZ = mCamTr->mLocal.mTranslationZ;
			mTr->mLocal.mTranslationZ.z = 499.0f;
		}
		
		sprite->mColor[3] -= (f32)aexTime->GetFrameTime() * 0.25f;
		if (sprite->mColor[3] <= 0.f)
		{
			mOwner->GetParentSpace()->DestroyObject(mOwner);
		}
	}

	void Fader::Shutdown()
	{

	}


	void Fader::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);
	}

	void Fader::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);
	}
}