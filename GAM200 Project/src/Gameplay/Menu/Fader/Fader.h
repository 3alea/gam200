#pragma once
#include "Components\Logic.h"

namespace AEX
{
	class TransformComp;
	class SpriteComp;

	class Fader : public LogicComp
	{
		AEX_RTTI_DECL(Fader, LogicComp);

	public:
		virtual void Initialize();
		virtual void Update();
		virtual void Shutdown();

		void FromJson(json & value);
		void ToJson(json & value);

		TransformComp * mTr = nullptr;
		TransformComp * mCamTr = nullptr;
		SpriteComp * sprite = nullptr;

		bool mDoneOnce = false;
	};
}