#pragma once

#include "Components/Logic.h"


class Camera;
class SpineAnimationComp;

namespace AEX
{
	class TransformComp;
	class SpriteComp;
	class Voice;

	class PlayButtonLogic : public LogicComp
	{
		AEX_RTTI_DECL(PlayButtonLogic, LogicComp);

	public:

		void Initialize();
		void Update();
		void Shutdown();

		bool OnGui();

		void FromJson(json & value);
		void ToJson(json & value);

		f32 mAlphaSpeed = 0.1f;

		// Not serializable
		TransformComp * tr = nullptr;
		SpriteComp * sprite = nullptr;
		SpriteComp * foregroundSprite = nullptr;
		SpineAnimationComp * mAnim = nullptr;
		//Camera * mCamera = nullptr;
		static bool mPlayClicked;
		bool doneOnce = false;

		Voice * mSelectedVoice = nullptr;
	};
}