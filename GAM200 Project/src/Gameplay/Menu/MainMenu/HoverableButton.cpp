#include "HoverableButton.h"
#include "Editor/Editor.h"
#include "Imgui/imgui.h"
#include "Graphics/GfxMgr.h"
#include "Player\Movement\PlayerController.h"
#include "Menu\MenuIncludes.h"
#include "spine\AnimationState.h"


namespace AEX
{
	HoverableButton::ButtonSelected HoverableButton::BS = HoverableButton::ButtonSelected::Play;

	HoverableButton::ButtonSelected HoverableButton::PrevButton = HoverableButton::ButtonSelected::Play;

	bool HoverableButton::mUsingGamePad = false;
	float HoverableButton::stickTimer = 0.0f;
	float HoverableButton::stickTime = 0.7f;
	bool HoverableButton::mTimerStarted = false;

	void HoverableButton::Initialize()
	{
		if (aexInput->isGamepadActive())
			mUsingGamePad = true;

		backgroundAnimation = nullptr;
		GameObject * obj = aexScene->FindObjectInANYSpace("background");

		if (obj == nullptr)
			return;

		BS = ButtonSelected::Play;
		PrevButton = ButtonSelected::Play;

		mTraverseTimer = 0.0f;

		backgroundAnimation = obj->GetComp<SpineAnimationComp>();
		backgroundAnimation->ChangeAnimation(0, "STARTGAME", false, 1.f);
	}

	void HoverableButton::Update()
	{
		if (backgroundAnimation == nullptr)
			Initialize();

		if (PlayButtonLogic::mPlayClicked)
			return;

		// Exit game only on the main menu if we are not on any of the options and ESC is triggered
		//if (aexInput->KeyTriggered(GLFW_KEY_ESCAPE) && OptionsButtonLogic::onOptions == false &&
		//	ControlsButtonLogic::onControls == false && CreditsButtonLogic::onCredits == false)
		//	Scene::bExitGame = true;

		AEVec2 & mouseDiff = aexInput->GetMouseMovement();


		if (mTimerStarted == false && MENU_GOING_UP && backgroundAnimation->GetAnimState()->getCurrent(0)->isComplete()
			&& ControlsButtonLogic::onControls == false && CreditsButtonLogic::onCredits == false &&
			OptionsButtonLogic::onOptions == false && ExitButtonLogic::mOnAreYouSure == false)
		{
			/*if (BS == HowToPlay)
			{
				mTraverseTimer = 0.0f;
				audiomgr->FreeThisVoice(mScrapeVoice);
				mPlayingScrape = false;
			}*/

			if (BS != HowToPlay)
				audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/Menu/MainMenu/Rotation/05 - Gun Quick Pistol Cock.wav"), Voice::SFX);

			//ExitButtonLogic::mCanShowAreYouSure = true;

			mUsingGamePad = true;
			mTimerStarted = true;

			if (BS == Play)
			{
				PrevButton = BS;
				BS = Options;
				UpdateAnimations();
			}
			else if (BS == Credits)
			{
				PrevButton = BS;
				BS = Play;
				UpdateAnimations();
			}
			else if (BS == Exit)
			{
				PrevButton = BS;
				BS = Credits;
				UpdateAnimations();
			}
			else if (BS == Options)
			{
				PrevButton = BS;
				BS = HowToPlay;
				UpdateAnimations();
			}
		}
		else if (mTimerStarted == false && MENU_GOING_DOWN && backgroundAnimation->GetAnimState()->getCurrent(0)->isComplete()
				&& ControlsButtonLogic::onControls == false && CreditsButtonLogic::onCredits == false &&
				OptionsButtonLogic::onOptions == false && ExitButtonLogic::mOnAreYouSure == false)
		{
			/*if (BS == Exit)
			{
				mTraverseTimer = 0.0f;
				audiomgr->FreeThisVoice(mScrapeVoice);
				mPlayingScrape = false;
			}*/

			if (BS != Exit)
				audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/Menu/MainMenu/Rotation/05 - Gun Quick Pistol Cock.wav"), Voice::SFX);

			mUsingGamePad = true;
			mTimerStarted = true;

			if (BS == Play)
			{
				PrevButton = BS;
				BS = Credits;
				UpdateAnimations();
			}
			else if (BS == Credits)
			{
				PrevButton = BS;
				BS = Exit;
				UpdateAnimations();
			}
			else if (BS == Options)
			{
				PrevButton = BS;
				BS = Play;
				UpdateAnimations();
			}
			else if (BS == HowToPlay)
			{
				PrevButton = BS;
				BS = Options;
				UpdateAnimations();
			}
		}
		else if (mouseDiff.x != 0 || mouseDiff.y != 0)
			mUsingGamePad = false;

		//ScrapeSoundLogic();

		if (mTimerStarted)
		{
			stickTimer += (f32)aexTime->GetFrameTime();

			if (stickTimer > stickTime)
			{
				stickTimer = 0.0f;
				mTimerStarted = false;
			}
		}

	}
	void HoverableButton::Shutdown()
	{

	}

	void HoverableButton::ScrapeSoundLogic()
	{
		if (mScrapeVoice && mScrapeVoice->IsValid() == false)
		{
			mPlayingScrape = false;
		}

		// While going up or down, update the timer
		if (MENU_GOING_UP || MENU_GOING_DOWN)
		{
			mTraverseTimer += (f32)aexTime->GetFrameTime();

			if (mTraverseTimer > SCRAPE_TIME && mPlayingScrape == false)
			{
				mScrapeVoice = audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/Menu/MainMenu/Rotation/Scrapes-and-Scratches_0005.wav"), Voice::SFX);
				mTraverseTimer = 0.0f;
				mPlayingScrape = true;
			}
		}
		// If we have stopped moving, reset the timer and stop the sound
		else
		{
			mTraverseTimer = 0.0f;
			audiomgr->FreeThisVoice(mScrapeVoice);
			mPlayingScrape = false;
		}
	}


	void HoverableButton::OnCollisionStarted(const CollisionStarted& collision)
	{
		if (PlayButtonLogic::mPlayClicked)
			return;

		if (mUsingGamePad)
			return;

		/*if (collision.otherObject->GetBaseNameString() == "mouse-collider")
		{
			TransformComp * tr = mOwner->GetComp<TransformComp>();
			BoxCollider * collider = mOwner->GetComp<BoxCollider>();
			AEVec2 & actualScale = tr->mLocal.mScale;
			actualScale += mHoverScaleOffset;
			collider->mScale += mHoverScaleOffset;

			if (collision.thisObject->GetBaseNameString() == "PlayButton")
			{
				if (BS == ButtonSelected::Play)
					return;
				PrevButton = BS;
				BS = ButtonSelected::Play;
			}
			else if (collision.thisObject->GetBaseNameString() == "OptionsButton")
			{
				if (BS == ButtonSelected::Options)
					return;
				PrevButton = BS;
				BS = ButtonSelected::Options;
			}
			else if (collision.thisObject->GetBaseNameString() == "CreditsButton")
			{
				if (BS == ButtonSelected::Credits)
					return;
				PrevButton = BS;
				BS = ButtonSelected::Credits;
			}
			else if (collision.thisObject->GetBaseNameString() == "ExitButton")
			{
				if (BS == ButtonSelected::Exit)
					return;
				PrevButton = BS;
				BS = ButtonSelected::Exit;
			}

			UpdateAnimations();
		}*/
	}

	void HoverableButton::OnCollisionEnded(const CollisionEnded& collision)
	{
		/*if (collision.otherObject->GetBaseNameString() == "mouse-collider")
		{
			TransformComp * tr = mOwner->GetComp<TransformComp>();
			BoxCollider * collider = mOwner->GetComp<BoxCollider>();
			AEVec2 & actualScale = tr->mLocal.mScale;
			actualScale -= mHoverScaleOffset;
			collider->mScale -= mHoverScaleOffset;
		}*/
	}


	bool HoverableButton::OnGui()
	{
		bool isChanged = false;

		if (ImGui::InputFloat2("Scale Offset", mHoverScaleOffset.v, 3))
		{
			isChanged = true;
		}

		return isChanged;
	}

	void HoverableButton::FromJson(json & value)
	{
		this->LogicComp::ToJson(value);

		value["Hover Scale Offset"] >> mHoverScaleOffset;
	}

	void HoverableButton::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);

		value["Hover Scale Offset"] << mHoverScaleOffset;
	}

	void HoverableButton::UpdateAnimations()
	{
		// Switch case changing between menu elements
		switch (BS)
		{
			// HOW TO PLAY
		case ButtonSelected::HowToPlay:
			backgroundAnimation->ChangeAnimation(0, "options_to_howtoplay", false, 6.0f);
			break;
			// OPTIONS
		case ButtonSelected::Options:
			if (PrevButton == ButtonSelected::Play)
			{
				backgroundAnimation->ChangeAnimation(0, "startgame_to_options", false, 6.0f);
			}
			else
			{
				backgroundAnimation->ChangeAnimation(0, "howtoplay_to_options", false, 6.0f);
			}

			break;

			// CREDITS
		case ButtonSelected::Credits:
			// If coming from exit, just play normal animation
			if (PrevButton == ButtonSelected::Exit)
			{
				backgroundAnimation->ChangeAnimation(0, "quitgame_to_credits", false, 6.0f);
			}
			else
			{
				// If coming from play, play transitional animation
				backgroundAnimation->ChangeAnimation(0, "startgame_to_credits", false, 6.0f);
			}
			break;

			// EXIT
		case ButtonSelected::Exit:
			backgroundAnimation->ChangeAnimation(0, "credits_to_quitgame", false, 6.0f);
			break;

			// PLAY
		case ButtonSelected::Play:
			// CREDITS BEFORE
			if (PrevButton == ButtonSelected::Credits)
			{
				backgroundAnimation->ChangeAnimation(0, "credits_to_startgame", false, 6.0f);
			}
			// OPTIONS BEFORE
			else if (PrevButton == ButtonSelected::Options)
			{
				backgroundAnimation->ChangeAnimation(0, "options_to_startgame", false, 6.0f);
			}
			// BEGINNING
			else
			{
				backgroundAnimation->ChangeAnimation(0, "STARTGAME", false, 6.0f);
			}
			break;
		default:
			break;
		}
	}
}