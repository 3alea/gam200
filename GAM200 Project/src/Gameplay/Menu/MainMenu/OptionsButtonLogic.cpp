#include "OptionsButtonLogic.h"
#include "Platform\AEXInput.h"
#include "Graphics\GfxMgr.h"
#include "Components\SpriteComps.h"
#include "Math\Collisions.h"
#include "Composition\AEXScene.h"
#include "Components\SpineAnimation.h"
#include "HoverableButton.h"
#include "Components/SpineAnimation.h"
#include "Platform/AEXTime.h"
#include <spine/AnimationState.h>


using namespace spine;


namespace AEX
{
	bool OptionsButtonLogic::onOptions = false;
	bool OptionsButtonLogic::optionsShown = false;
	float OptionsButtonLogic::mSoundBallTn = 1.0f;
	float OptionsButtonLogic::mMusicBallTn = 1.0f;
	bool OptionsButtonLogic::mIsVolumeOn = true;
	bool OptionsButtonLogic::mIsVSyncOn = true;
	bool OptionsButtonLogic::mIsFullScreenOn = true;
	OptionsButtonLogic::ResolutionSelected OptionsButtonLogic::mResolutionSelected = _1920x1080;

	void OptionsButtonLogic::Initialize()
	{
		tr = mOwner->GetComp<TransformComp>();
		sprite = mOwner->GetComp<SpriteComp>();

		GameObject * bg = aexScene->FindObjectInANYSpace("background");
		if (bg)
			mAnim = bg->GetComp<SpineAnimationComp>();

		GameObject * volumeOnOffObj = aexScene->FindObjectInANYSpace("on/off-volume");
		GameObject * resolutionsObj = aexScene->FindObjectInANYSpace("resolutions");
		GameObject * vsyncOnOffObj = aexScene->FindObjectInANYSpace("on/off-vsync");
		GameObject * fullscreenOnOffObj = aexScene->FindObjectInANYSpace("on/off-fullscreen");

		if (volumeOnOffObj)
			mOnOffVolume = volumeOnOffObj->GetComp<SpriteComp>();

		if (resolutionsObj)
			mResSprite = resolutionsObj->GetComp<SpriteComp>();

		if (vsyncOnOffObj)
			mOnOffVSync = vsyncOnOffObj->GetComp<SpriteComp>();

		if (fullscreenOnOffObj)
			mOnOffFullScreen = fullscreenOnOffObj->GetComp<SpriteComp>();

		//mIsVolumeOn = true;
		//mIsVSyncOn = true;
		//mIsFullScreenOn = false;

		onOptions = false;
		optionsShown = false;

		mStickTimer = 0.0f;
		mStickTime = MAIN_MENU_STICK_TIME;

		mOptionSelected = Volume;//NoneSelected;
		//mResolutionSelected = _1920x1080;

		//mSoundBallTn = 1.0f;
		//mMusicBallTn = 1.0f;

		mRotationTimer = 0.0f;
		mRotationBackTimer = 0.0f;

		GameObject * minBallObj = aexScene->FindObjectInANYSpace("SoundBallMin");
		GameObject * maxBallObj = aexScene->FindObjectInANYSpace("SoundBallMax");

		if (minBallObj && maxBallObj)
		{
			TransformComp * minBallTr = minBallObj->GetComp<TransformComp>();
			TransformComp * maxBallTr = maxBallObj->GetComp<TransformComp>();

			if (minBallTr && maxBallTr)
			{
				mBallMinX = minBallTr->mLocal.mTranslationZ.x;
				mBallMaxX = maxBallTr->mLocal.mTranslationZ.x;
			}
		}

		GameObject * soundBallObj = aexScene->FindObjectInANYSpace("SoundBall");
		GameObject * musicBallObj = aexScene->FindObjectInANYSpace("MusicBall");

		if (soundBallObj)
		{
			mSoundBallTr = soundBallObj->GetComp<TransformComp>();
			mSoundBallSprite = soundBallObj->GetComp<SpriteComp>();
		}
		if (musicBallObj)
		{
			mMusicBallTr = musicBallObj->GetComp<TransformComp>();
			mMusicBallSprite = musicBallObj->GetComp<SpriteComp>();
		}
	}

	void OptionsButtonLogic::Update()
	{
		if (doneOnce == false)
		{
			Initialize();
			doneOnce = true;
		}

		if (mStickTimer > 0.0f)
			mStickTimer += (f32)aexTime->GetFrameTime();

		if (mStickTimer > MAIN_MENU_STICK_TIME)
			mStickTimer = 0.0f;

		if (mRotationTimer > 0.0f)
			mRotationTimer += (f32)aexTime->GetFrameTime();

		if (mRotationBackTimer > 0.0f)
			mRotationBackTimer += (f32)aexTime->GetFrameTime();

		if (mRotationTimer > ROT_AUDIO_TIME)
		{
			audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Rotation/Bike_BrakesLoop1.wav"), Voice::SFX);
			mRotationTimer = 0.0f;
		}

		if (mRotationBackTimer > ROT_BACK_AUDIO_TIME)
		{
			audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Rotation/Bike_BrakesLoop1.wav"), Voice::SFX);
			mRotationBackTimer = 0.0f;
		}

		// Logic to select options button
		if (((ENTER && HoverableButton::BS == HoverableButton::ButtonSelected::Options) || (A_BUTTON && HoverableButton::BS == HoverableButton::ButtonSelected::Options))
			&& mAnim->GetAnimState()->getCurrent(0)->isComplete() && onOptions == false)
		{
			onOptions = true;

			audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Selection/Impacts_PROCESSED_001.wav"), Voice::SFX);

			mRotationTimer += (f32)aexTime->GetFrameTime();

			if (mAnim != nullptr)
			{
				mAnim->ReloadSpineData();
				mAnim->ChangeAnimation(0, "options_selection", false, 1.5f);
			}
		}

		// Logic to select each individual option
		if (optionsShown)// && mAnim->GetAnimState()->getCurrent(0)->isComplete())
		{
			if (MENU_GOING_UP && mStickTimer == 0.0f)
			{
				--mOptionSelected;
				mStickTimer += (f32)aexTime->GetFrameTime();
			}
			/*else */if (MENU_GOING_DOWN && mStickTimer == 0.0f)
			{
				++mOptionSelected;
				mStickTimer += (f32)aexTime->GetFrameTime();
			}

			UpdateOptionHovered();
		}

		// Logic to go back from options
		if ((ESC || B_BUTTON) && onOptions && mAnim->GetAnimState()->getCurrent(0)->isComplete())
		{
			optionsShown = false;

			mRotationBackTimer += (f32)aexTime->GetFrameTime();

			if (mAnim != nullptr)
			{
				mAnim->ReloadSpineData();
				mAnim->ChangeAnimation(0, "options_to_mainmenu", false, 1.5f);
			}
		}

		if (mAnim->CheckCurrentAnimationAtPostion("options_to_mainmenu", 0) && mAnim->GetAnimState()->getCurrent(0)->isComplete())
			onOptions = false;

		if (mAnim->CheckCurrentAnimationAtPostion("options_selection", 0) && mAnim->GetAnimState()->getCurrent(0)->isComplete())
			optionsShown = true;

		// Show or hide the options png depending on whether we are in the options menu
		if (optionsShown)
		{
			sprite->mColor[3] += (f32)aexTime->GetFrameTime();//= 1.0f;
			mOnOffVolume->mColor[3] += (f32)aexTime->GetFrameTime();//= 1.0f;
			mResSprite->mColor[3] += (f32)aexTime->GetFrameTime();//= 1.0f;
			mOnOffVSync->mColor[3] += (f32)aexTime->GetFrameTime();//= 1.0f;
			mOnOffFullScreen->mColor[3] += (f32)aexTime->GetFrameTime();//= 1.0f;
			mSoundBallSprite->mColor[3] += (f32)aexTime->GetFrameTime();
			mMusicBallSprite->mColor[3] += (f32)aexTime->GetFrameTime();

			if (mOnOffVolume->mColor[3] > 1.0f)
				mOnOffVolume->mColor[3] = 1.0f;

			if (mResSprite->mColor[3] > 1.0f)
				mResSprite->mColor[3] = 1.0f;

			if (mOnOffVSync->mColor[3] > 1.0f)
				mOnOffVSync->mColor[3] = 1.0f;

			if (mOnOffFullScreen->mColor[3] > 1.0f)
				mOnOffFullScreen->mColor[3] = 1.0f;

			if (mSoundBallSprite->mColor[3] > 1.0f)
				mSoundBallSprite->mColor[3] = 1.0f;

			if (mMusicBallSprite->mColor[3] > 1.0f)
				mMusicBallSprite->mColor[3] = 1.0f;
		}
		else
		{
			sprite->mColor[3] = 0.0f;
			mOnOffVolume->mColor[3] = 0.0f;
			mResSprite->mColor[3] = 0.0f;
			mOnOffVSync->mColor[3] = 0.0f;
			mOnOffFullScreen->mColor[3] = 0.0f;
			mSoundBallSprite->mColor[3] = 0.0f;
			mMusicBallSprite->mColor[3] = 0.0f;
		}

		/*if (aexInput->MouseTriggered(Input::eMouseLeft))
		{
			AEVec2 & mousePos = GfxMgr->mCameraList.front()->PointToWorld(aexInput->GetGLFWMousePos());

			AEVec2 thisPos(tr->mLocal.mTranslationZ.x, tr->mLocal.mTranslationZ.y);

			AEVec2 texSize = sprite ? AEVec2(static_cast<f32>(sprite->mTex->get()->GetWidth()) / 100.0f, static_cast<f32>(sprite->mTex->get()->GetHeight() / 100.0f)) : AEVec2(1.0f, 1.0f);
			AEVec2 scale(tr->mLocal.mScale.x * texSize.x, tr->mLocal.mScale.y * texSize.y);

			if (StaticPointToStaticRect(&mousePos, &thisPos, scale.x, scale.y))
			{
				mOptionsClicked = true;
				GameObject * bg = aexScene->FindObjectInANYSpace("background");
				assert(bg != nullptr);
				SpineAnimationComp * anim = bg->GetComp<SpineAnimationComp>();
				assert(anim != nullptr);
				//anim->ChangeAnimation(0, "enchufado_options", false, 1.0f);
			}
		}*/
	}

	void OptionsButtonLogic::Shutdown()
	{

	}


	void OptionsButtonLogic::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);
	}

	void OptionsButtonLogic::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);
	}

	void OptionsButtonLogic::UpdateOptionHovered()
	{
		if (onOptions)
		{
			//if (mOptionSelected == NoneSelected)
			//{
			//	sprite->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/Options/menu_no_light.png");
			//}
			/*else */if (mOptionSelected == Volume)
			{
				// Here change the lighted textures of the left
				sprite->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/Options/volume_on.png");

				// Here change the lighted textures of the right
				if (ENTER || A_BUTTON)
				{
					mIsVolumeOn = !mIsVolumeOn;

					audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/OptionSelection/14 - Gun Insert Magazine Into Ak-47.wav"), Voice::SFX);

					// Logic to mute/unmute
					audiomgr->SetMuteStateForAll(!mIsVolumeOn);
				}
			}
			else if (mOptionSelected == Sound)
			{
				// Here change the lighted textures of the left
				sprite->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/Options/sound_on.png");

				// Logic to change the ball
				if (MENU_GOING_RIGHT)// && mStickTimer == 0.0f)
				{
					mSoundBallTn += BAR_FACTOR;
					mSoundBallTn = AEX::Clamp(mSoundBallTn, 0.0f, 1.0f);
					//mStickTimer += (f32)aexTime->GetFrameTime();

					audiomgr->SetSFXVolume(mSoundBallTn);

					if ((mBarVoice == nullptr || mBarVoice->IsValid() == false) && mSoundBallTn != 1.0f)
						mBarVoice = audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/Bar/43 - Pneumatic Door Mechanism Hiss #2.wav"), Voice::SFX);
						
					if (mSoundBallTn == 1.0f && mBarVoice)
					{
						audiomgr->FreeThisVoice(mBarVoice);
						mBarVoice = nullptr;
					}
				}
				if (MENU_GOING_LEFT)// && mStickTimer == 0.0f)
				{
					mSoundBallTn -= BAR_FACTOR;
					mSoundBallTn = AEX::Clamp(mSoundBallTn, 0.0f, 1.0f);
					//mStickTimer += (f32)aexTime->GetFrameTime();

					audiomgr->SetSFXVolume(mSoundBallTn);

					if ((mBarVoice == nullptr || (mBarVoice && mBarVoice->IsValid() == false)) && mSoundBallTn != 0.0f)
						mBarVoice = audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/Bar/43 - Pneumatic Door Mechanism Hiss #2.wav"), Voice::SFX);
						
					if (mSoundBallTn == 0.0f && mBarVoice)
					{
						audiomgr->FreeThisVoice(mBarVoice);
						mBarVoice = nullptr;
					}
				}
			}
			else if (mOptionSelected == Music)
			{
				// Here change the lighted textures of the left
				sprite->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/Options/music_on.png");

				// Logic to change the ball
				if (MENU_GOING_RIGHT)// && mStickTimer == 0.0f)
				{
					mMusicBallTn += BAR_FACTOR;
					mMusicBallTn = AEX::Clamp(mMusicBallTn, 0.0f, 1.0f);
					//mStickTimer += (f32)aexTime->GetFrameTime();

					audiomgr->SetMusicVolume(mMusicBallTn);

					if ((mBarVoice == nullptr || (mBarVoice && mBarVoice->IsValid() == false)) && mMusicBallTn != 1.0f)
						mBarVoice = audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/Bar/43 - Pneumatic Door Mechanism Hiss #2.wav"), Voice::SFX);
					
					if (mMusicBallTn == 1.0f && mBarVoice)
					{
						audiomgr->FreeThisVoice(mBarVoice);
						mBarVoice = nullptr;
					}
				}
				if (MENU_GOING_LEFT)// && mStickTimer == 0.0f)
				{
					mMusicBallTn -= BAR_FACTOR;
					mMusicBallTn = AEX::Clamp(mMusicBallTn, 0.0f, 1.0f);
					//mStickTimer += (f32)aexTime->GetFrameTime();

					audiomgr->SetMusicVolume(mMusicBallTn);

					if ((mBarVoice == nullptr || (mBarVoice && mBarVoice->IsValid() == false)) && mMusicBallTn != 0.0f)
						mBarVoice = audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/Bar/43 - Pneumatic Door Mechanism Hiss #2.wav"), Voice::SFX);
						
					if (mMusicBallTn == 0.0f && mBarVoice)
					{
						audiomgr->FreeThisVoice(mBarVoice);
						mBarVoice = nullptr;
					}
				}
			}
			else if (mOptionSelected == Resolution)
			{
				// Here change the lighted textures of the left
				sprite->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/Options/resolution_on.png");

				// Here change the lighted textures of the right
				if (ENTER || A_BUTTON)
				{
					++mResolutionSelected;
					UpdateActualResolution();
				}
			}
			else if (mOptionSelected == VSync)
			{
				// Here change the lighted textures of the left
				sprite->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/Options/vsync_on.png");

				// Here change the lighted textures of the right
				if (ENTER || A_BUTTON)
				{
					mIsVSyncOn = !mIsVSyncOn;
					GfxMgr->SetVSync(mIsVSyncOn);
					audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/OptionSelection/14 - Gun Insert Magazine Into Ak-47.wav"), Voice::SFX);
				}
			}
			else if (mOptionSelected == FullScreen)
			{
				// Here change the lighted textures of the left
				sprite->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/Options/fullscreen_on.png");

				// Here change the lighted textures of the right
				if (ENTER || A_BUTTON)
				{
					audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/OptionSelection/14 - Gun Insert Magazine Into Ak-47.wav"), Voice::SFX);
					mIsFullScreenOn = !mIsFullScreenOn;
					WindowMgr->GetCurrentWindow()->SetFullScreen(mIsFullScreenOn);
				}
			}

			mSoundBallTr->mLocal.mTranslationZ.x = AEX::Lerp<f32>(mBallMinX, mBallMaxX, mSoundBallTn);
			mMusicBallTr->mLocal.mTranslationZ.x = AEX::Lerp<f32>(mBallMinX, mBallMaxX, mMusicBallTn);

			UpdateOnOffLights(mOnOffVolume, mOnOffVSync, mOnOffFullScreen);
			UpdateResolutions(mResSprite);
		}

		if (optionsShown == false || (mOptionSelected != Sound && mOptionSelected != Music) ||
			(MENU_GOING_LEFT == false && MENU_GOING_RIGHT == false) || (MENU_GOING_LEFT && MENU_GOING_RIGHT))
		{
			if (mBarVoice)
			{
				audiomgr->FreeThisVoice(mBarVoice);
				mBarVoice = nullptr;
			}
		}
	}

	void OptionsButtonLogic::UpdateOnOffLights(SpriteComp * volumeOnOff, SpriteComp * vsyncOnOff, SpriteComp * fullscreenOnOff)
	{
		if (volumeOnOff)
		{
			if (mIsVolumeOn)
				volumeOnOff->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/Options/on.png");
			else
				volumeOnOff->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/Options/off.png");
		}

		if (vsyncOnOff)
		{
			if (mIsVSyncOn)
				vsyncOnOff->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/Options/on.png");
			else
				vsyncOnOff->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/Options/off.png");
		}

		if (fullscreenOnOff)
		{
			if (mIsFullScreenOn)
				fullscreenOnOff->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/Options/on.png");
			else
				fullscreenOnOff->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/Options/off.png");
		}
	}

	void OptionsButtonLogic::UpdateResolutions(SpriteComp * resoultionsSprite)
	{
		if (OptionsButtonLogic::mResolutionSelected == OptionsButtonLogic::_1920x1080)
		{
			if (resoultionsSprite)
				resoultionsSprite->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/Options/1920_lighted.png");
		}
		else if (OptionsButtonLogic::mResolutionSelected == OptionsButtonLogic::_1600x900)
		{
			if (resoultionsSprite)
				resoultionsSprite->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/Options/1600_lighted.png");
		}
		else if (OptionsButtonLogic::mResolutionSelected == OptionsButtonLogic::_1280x720)
		{
			if (resoultionsSprite)
				resoultionsSprite->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/Options/1280_lighted.png");
		}
	}

	void OptionsButtonLogic::UpdateActualResolution()
	{
		if (mIsFullScreenOn)
		{
			WindowMgr->GetCurrentWindow()->SetFullScreen(false);
			mIsFullScreenOn = false;
		}

		if (OptionsButtonLogic::mResolutionSelected == OptionsButtonLogic::_1920x1080)
		{
			WindowMgr->GetCurrentWindow()->SetResolution(Resolution::eRes_1920x1080);
		}
		else if (OptionsButtonLogic::mResolutionSelected == OptionsButtonLogic::_1600x900)
		{
			WindowMgr->GetCurrentWindow()->SetResolution(Resolution::eRes_1600x900);
		}
		else if (OptionsButtonLogic::mResolutionSelected == OptionsButtonLogic::_1280x720)
		{
			WindowMgr->GetCurrentWindow()->SetResolution(Resolution::eRes_1280x720);
		}

		audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/OptionSelection/14 - Gun Insert Magazine Into Ak-47.wav"), Voice::SFX);
	}


	// Prefix increment
	OptionsButtonLogic::OptionSelected & operator++(OptionsButtonLogic::OptionSelected & option)
	{
		if (/*option == OptionsButtonLogic::OptionSelected::NoneSelected || */(int)option == NUMBER_OF_OPTIONS - 1)
		{
			option = OptionsButtonLogic::OptionSelected(0);//1);
			audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/Menu/MainMenu/Options/OptionTraversal/Typewriter letter key long 01 rode.wav"), Voice::SFX);
			return option;
		}

		option = OptionsButtonLogic::OptionSelected((int)option + 1);
		audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/Menu/MainMenu/Options/OptionTraversal/Typewriter letter key long 01 rode.wav"), Voice::SFX);
		return option;
	}

	// Postfix increment
	OptionsButtonLogic::OptionSelected operator++(OptionsButtonLogic::OptionSelected & option, int)
	{
		OptionsButtonLogic::OptionSelected copy = option;
		++option;
		return copy;
	}

	// Prefix decrement
	OptionsButtonLogic::OptionSelected & operator--(OptionsButtonLogic::OptionSelected & option)
	{
		//if (option == OptionsButtonLogic::OptionSelected::NoneSelected)
		//{
		//	option = OptionsButtonLogic::OptionSelected(1);
		//	return option;
		//}

		if ((int)option == 0)//1)
		{
			option = OptionsButtonLogic::OptionSelected(NUMBER_OF_OPTIONS - 1);
			audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/Menu/MainMenu/Options/OptionTraversal/Typewriter letter key long 01 rode.wav"), Voice::SFX);
			return option;
		}

		option = OptionsButtonLogic::OptionSelected((int)option - 1);
		audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/Menu/MainMenu/Options/OptionTraversal/Typewriter letter key long 01 rode.wav"), Voice::SFX);
		return option;
	}

	// Postfix decrement
	OptionsButtonLogic::OptionSelected operator--(OptionsButtonLogic::OptionSelected & option, int)
	{
		OptionsButtonLogic::OptionSelected copy = option;
		--option;
		return copy;
	}



	// Prefix increment
	OptionsButtonLogic::ResolutionSelected & operator++(OptionsButtonLogic::ResolutionSelected & option)
	{
		if ((int)option == NUMBER_OF_RESOLUTIONS - 1)
		{
			option = OptionsButtonLogic::ResolutionSelected(0);
			return option;
		}

		option = OptionsButtonLogic::ResolutionSelected((int)option + 1);
		return option;
	}

	// Postfix increment
	OptionsButtonLogic::ResolutionSelected operator++(OptionsButtonLogic::ResolutionSelected & option, int)
	{
		OptionsButtonLogic::ResolutionSelected copy = option;
		++option;
		return copy;
	}
}