#include "PlayButtonLogic.h"
#include "Platform/AEXInput.h"
#include "Editor/Editor.h"
#include "Components/SpriteComps.h"
#include "Components\SpineAnimation.h"
#include <spine/AnimationState.h>
#include "HoverableButton.h"
#include "Core\AEXSystem.h"
#include "Environment/Background/StationZoomOut.h"


namespace AEX
{
	bool PlayButtonLogic::mPlayClicked = false;

	void PlayButtonLogic::Initialize()
	{
		tr = mOwner->GetComp<TransformComp>();
		sprite = mOwner->GetComp<SpriteComp>();
		mPlayClicked = false;
		//doneOnce = false;

		GameObject * camObj = aexScene->FindObjectInANYSpace("camera");

		GameObject * bg = aexScene->FindObjectInANYSpace("background");
		
		if (bg != nullptr)
			mAnim = bg->GetComp<SpineAnimationComp>();

		GameObject * foreground = aexScene->FindObjectInANYSpace("foreground");
		
		if (foreground != nullptr)
			foregroundSprite = foreground->GetComp<SpriteComp>();

		StationZoomOut::canSkipCinematic = false;
	}

	void PlayButtonLogic::Update()
	{
		if (doneOnce == false)
		{
			Initialize();
			doneOnce = true;
		}

		/*if (aexInput->MouseTriggered(Input::eMouseLeft))
		{
			AEVec2 & mousePos = GfxMgr->mCameraList.front()->PointToWorld(aexInput->GetGLFWMousePos());

			AEVec2 thisPos(tr->mLocal.mTranslationZ.x, tr->mLocal.mTranslationZ.y);

			AEVec2 texSize = sprite ? AEVec2(static_cast<f32>(sprite->mTex->get()->GetWidth()) / 100.0f, static_cast<f32>(sprite->mTex->get()->GetHeight() / 100.0f)) : AEVec2(1.0f, 1.0f);
			AEVec2 scale(tr->mLocal.mScale.x * texSize.x, tr->mLocal.mScale.y * texSize.y);

			if (StaticPointToStaticRect(&mousePos, &thisPos, scale.x, scale.y))
			{
				mPlayClicked = true;
				GameObject * bg = aexScene->FindObjectInANYSpace("background");
				assert(bg != nullptr);
				SpineAnimationComp * anim = bg->GetComp<SpineAnimationComp>();
				assert(anim != nullptr);
				anim->ChangeAnimation(0, "enchufado_play", false, 1.0f);
			}
		}
		else */if (((ENTER && HoverableButton::BS == HoverableButton::ButtonSelected::Play) || (A_BUTTON && HoverableButton::BS == HoverableButton::ButtonSelected::Play))
					&& mAnim && mAnim->GetAnimState()->getCurrent(0)->isComplete() && mPlayClicked == false)
		{
			mPlayClicked = true;
			
			mSelectedVoice = audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/Menu/MainMenu/Selection/Impacts_DESIGNED_084.wav"), Voice::SFX);

			if (mAnim != nullptr)
			{
				mAnim->ReloadSpineData();
				mAnim->ChangeAnimation(0, "startgame_selection", false, 5.0f);
				aexScene->bFadeTransition = true;
				foregroundSprite->mColor[3] = 0.0f;
			}

			/*TransformComp * tr = foregroundSprite->mOwner->GetComp<TransformComp>();
			if (tr)
			{
				GameObject * cam = aexScene->FindObjectInANYSpace("camera");
				TransformComp * camTr = nullptr;

				if (cam)
					camTr = cam->GetComp<TransformComp>();

				if (camTr)
				{
					tr->mLocal.mTranslationZ.x = camTr->mLocal.mTranslationZ.x;
					tr->mLocal.mTranslationZ.y = camTr->mLocal.mTranslationZ.y;
				}
			}*/
		}

		if (mPlayClicked)
		{
			GameObject * foreground = aexScene->FindObjectInANYSpace("foreground");
			TransformComp * tr = foreground->GetComp<TransformComp>();
			GameObject * cam = aexScene->FindObjectInANYSpace("camera");
			TransformComp * camTr = cam->GetComp<TransformComp>();

			tr->mLocal.mTranslationZ = camTr->mLocal.mTranslationZ;

			foregroundSprite->mColor[3] += (f32)aexTime->GetFrameTime() * mAlphaSpeed;

			if (foregroundSprite->mColor[3] >= 1.0f)
			{
				mPlayClicked = false;
				Scene::bFadeTransition = true;
				GameObject* fadeout = aexScene->FindObjectInANYSpace("fadeout");
				if (fadeout && aexScene->bFadeTransition == true)
				{
					fadeout->GetComp<SpriteComp>()->mColor[3] = 1.f;
					aexScene->bFadeTransition = false;
				}
				aexScene->ChangeLevel("data/Levels/Heart(Tutorial).json");
			}
		}
	}

	void PlayButtonLogic::Shutdown()
	{

	}


	bool PlayButtonLogic::OnGui()
	{
		bool isChanged = false;

		if (ImGui::InputFloat("Alpha Speed", &mAlphaSpeed))
		{
			isChanged = true;
		}

		return isChanged;
	}


	void PlayButtonLogic::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);

		value["Alpha Speed"] >> mAlphaSpeed;
	}

	void PlayButtonLogic::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);

		value["Alpha Speed"] << mAlphaSpeed;
	}
}