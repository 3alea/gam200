#pragma once
#include "Components\Logic.h"


namespace AEX
{
	class TransformComp;
	class SpriteComp;

	class CreditsButtonLogic : public LogicComp
	{
		AEX_RTTI_DECL(CreditsButtonLogic, LogicComp);

	public:

		virtual void Initialize();
		virtual void Update();
		virtual void Shutdown();

		void FromJson(json & value);
		void ToJson(json & value);

		bool OnGui();

		TransformComp * tr = nullptr;
		SpriteComp * sprite = nullptr;

		AEVec2 mInitialCreditsPos;
		TransformComp * mStartCreditsTr = nullptr;
		SpriteComp * mStartCreditsSprite = nullptr;
		TransformComp * mEndCreditsTr = nullptr;
		f32 mCreditsTimer = 0.0f;
		f32 mCreditsTime;

		f32 mFadeFactor;

		bool mCreditsClicked = false;
		bool mBackToMainMenu = false;
		bool mExitedEarly = false;

		static bool onCredits;

		bool doneOnce = false;

	private:

	};
}