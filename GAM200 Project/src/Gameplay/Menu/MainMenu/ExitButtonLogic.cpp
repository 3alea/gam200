#include "ExitButtonLogic.h"
#include "Platform/AEXInput.h"
#include "Components/SpriteComps.h"
#include "Editor/Editor.h"
#include "HoverableButton.h"
#include "Components/SpineAnimation.h"
#include <spine/AnimationState.h>


namespace AEX
{
	bool ExitButtonLogic::mOnAreYouSure = false;
	//bool ExitButtonLogic::mCanShowAreYouSure = true;

	void ExitButtonLogic::Initialize()
	{
		tr = mOwner->GetComp<TransformComp>();
		sprite = mOwner->GetComp<SpriteComp>();

		GameObject * areYouSureObj = aexScene->FindObjectInANYSpace("AreYouSure");

		mStickTimer = 0.0f;
		mOnAreYouSure = false;
		mGoBackTimer = 0.0f;
		//mCanShowAreYouSure = true;

		if (areYouSureObj)
			mAreYouSureSprite = areYouSureObj->GetComp<SpriteComp>();

		GameObject * bg = aexScene->FindObjectInANYSpace("background");

		if (bg)
			mBgAnim = bg->GetComp<SpineAnimationComp>();
	}

	void ExitButtonLogic::Update()
	{
		if (mDoneOnce == false)
		{
			Initialize();
			mDoneOnce = true;
		}

		/*if (aexInput->MouseTriggered(Input::eMouseLeft))
		{
			AEVec2 & mousePos = GfxMgr->mCameraList.front()->PointToWorld(aexInput->GetGLFWMousePos());

			AEVec2 thisPos(tr->mLocal.mTranslationZ.x, tr->mLocal.mTranslationZ.y);

			AEVec2 texSize = sprite ? AEVec2(static_cast<f32>(sprite->mTex->get()->GetWidth()) / 100.0f, static_cast<f32>(sprite->mTex->get()->GetHeight() / 100.0f)) : AEVec2(1.0f, 1.0f);
			AEVec2 scale(tr->mLocal.mScale.x * texSize.x, tr->mLocal.mScale.y * texSize.y);

			if (StaticPointToStaticRect(&mousePos, &thisPos, scale.x, scale.y))
			{
				Scene::bExitGame = true;
			}
		}*/

		if (mStickTimer > STICK_TIME)
			mStickTimer = 0.0f;

		if (mGoBackTimer > STICK_TIME)
		{
			mGoBackTimer = 0.0f;
			mOnAreYouSure = false;
		}

		if (mStickTimer > 0.0f)
			mStickTimer += (f32)aexTime->GetFrameTime();

		if (mGoBackTimer > 0.0f)
			mGoBackTimer += (f32)aexTime->GetFrameTime();

		if (mOnAreYouSure)
		{
			AreYouSureLogic();
		}

		if ((ENTER || A_BUTTON) && HoverableButton::BS == HoverableButton::Exit
			/*&& mBgAnim->GetAnimState()->getCurrent(0)->isComplete()*/ && mOnAreYouSure == false)
		{
			//mBgAnim->ReloadSpineData();
			//mBgAnim->ChangeAnimation(0, "quitgame_selection", false, 5.0f);
			//audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/Menu/MainMenu/Selection/Impacts_PROCESSED_001.wav"), Voice::SFX);
			//mCanShowAreYouSure = true;
			audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/OptionSelection/14 - Gun Insert Magazine Into Ak-47.wav"), Voice::SFX);
			mAreYouSureSprite->mColor[3] = 1.0f;
			mOnAreYouSure = true;
		}

		/*if (mBgAnim->CheckCurrentAnimationAtPostion("quitgame_selection", 0) && mBgAnim->GetAnimState()->getCurrent(0)->isComplete()
			&& mOnAreYouSure == false && mCanShowAreYouSure)
		{
			mOnAreYouSure = true;
			mAreYouSureSprite->mColor[3] = 1.0f;
		}*/
	}

	void ExitButtonLogic::AreYouSureLogic()
	{
		if (mGoBackTimer > 0.0f)
			return;

		if (MENU_GOING_LEFT && mStickTimer == 0.0f)
		{
			mStickTimer += (f32)aexTime->GetFrameTime();

			mAreYouSure = ++mAreYouSure % 2;
			
			audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/OptionTraversal/Typewriter letter key long 01 rode.wav"), Voice::SFX);
		}
		if (MENU_GOING_RIGHT && mStickTimer == 0.0f)
		{
			mStickTimer += (f32)aexTime->GetFrameTime();

			if (mAreYouSure == 0)
				mAreYouSure = 1;
			else
				--mAreYouSure;

			audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/OptionTraversal/Typewriter letter key long 01 rode.wav"), Voice::SFX);
		}

		if (mAreYouSure == 0)
		{
			mAreYouSureSprite->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/PauseMenu/areyousure_NO.png");
		}
		else if (mAreYouSure == 1)
		{
			mAreYouSureSprite->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/PauseMenu/areyousure_YES.png");
		}

		if (B_BUTTON || ESC)
		{
			//mOnAreYouSure = false;
			mAreYouSureSprite->mColor[3] = 0.0f;
			mAreYouSure = 0;
			mGoBackTimer += (f32)aexTime->GetFrameTime();
			//mCanShowAreYouSure = false;
		}

		if (A_BUTTON || ENTER)
		{
			if (mAreYouSure == 0)
			{
				//mOnAreYouSure = false;
				mAreYouSureSprite->mColor[3] = 0.0f;
				mAreYouSure = 0;
				mGoBackTimer += (f32)aexTime->GetFrameTime();
				//mCanShowAreYouSure = false;

				audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/OptionSelection/14 - Gun Insert Magazine Into Ak-47.wav"), Voice::SFX);
			}
			else if (mAreYouSure == 1)
			{
				Scene::bExitGame = true;
			}
		}
	}

	void ExitButtonLogic::Shutdown()
	{

	}

	void ExitButtonLogic::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);
	}
	void ExitButtonLogic::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);
	}
}