#pragma once

#include "Components/Logic.h"
#include "Components\SpineAnimation.h"
#include "Platform\AEXInput.h"

#define SCRAPE_TIME 0.01f

#define MENU_X_RESPONSE 0.5f
#define MENU_Y_RESPONSE 0.5f

#define MENU_GOING_UP    (aexInput->GamepadButtonPressed/*Triggered*/(Input::GamepadButtons::eGamepadButtonDPadUp) || aexInput->KeyPressed(GLFW_KEY_UP)      || aexInput->GetGamepadStick(Input::GamepadSticks::eLeftStickYAxis)   < -MENU_Y_RESPONSE)
#define MENU_GOING_DOWN  (aexInput->GamepadButtonPressed/*Triggered*/(Input::GamepadButtons::eGamepadButtonDPadDown) || aexInput->KeyPressed(GLFW_KEY_DOWN)    || aexInput->GetGamepadStick(Input::GamepadSticks::eLeftStickYAxis)   >  MENU_Y_RESPONSE)
#define MENU_GOING_RIGHT (aexInput->GamepadButtonPressed/*Triggered*/(Input::GamepadButtons::eGamepadButtonDPadRight) || aexInput->KeyPressed(GLFW_KEY_RIGHT)      || aexInput->GetGamepadStick(Input::GamepadSticks::eLeftStickXAxis)   > MENU_Y_RESPONSE)
#define MENU_GOING_LEFT  (aexInput->GamepadButtonPressed/*Triggered*/(Input::GamepadButtons::eGamepadButtonDPadLeft) || aexInput->KeyPressed(GLFW_KEY_LEFT)    || aexInput->GetGamepadStick(Input::GamepadSticks::eLeftStickXAxis)   <  -MENU_Y_RESPONSE)
#define A_BUTTON		 (aexInput->GamepadButtonTriggered(Input::GamepadButtons::eGamepadButtonA))
#define B_BUTTON		 (aexInput->GamepadButtonTriggered(Input::GamepadButtons::eGamepadButtonB))
#define ESC				 (aexInput->KeyTriggered(GLFW_KEY_ESCAPE))
#define ENTER			 (aexInput->KeyTriggered(GLFW_KEY_ENTER))


namespace AEX
{
	class Voice;

	class HoverableButton : public LogicComp
	{

		AEX_RTTI_DECL(HoverableButton, LogicComp);

	public:
		SpineAnimationComp* backgroundAnimation;
		
		enum ButtonSelected
		{
			HowToPlay,
			Options,
			Play,
			Credits,
			Exit
		};

		static ButtonSelected BS;
		static ButtonSelected PrevButton;

		void Initialize();
		void Update();
		void Shutdown();

		void OnCollisionStarted(const CollisionStarted& collision);
		void OnCollisionEnded(const CollisionEnded& collision);

		bool OnGui();

		void FromJson(json & value);
		void ToJson(json & value);

		void UpdateAnimations();

		Voice * mScrapeVoice = nullptr;
		f32 mTraverseTimer = 0.0f;
		bool mPlayingScrape = false;

		bool mhoveredOnce = false;
		AEVec2 mHoverScaleOffset = AEVec2(1.0f, 1.0f);
		static bool mUsingGamePad;

		static float stickTimer;
		static float stickTime;
		static bool mTimerStarted;

		void ScrapeSoundLogic();
	};
}