#include "ControlsButtonLogic.h"
#include "Components\SpineAnimation.h"
#include "HoverableButton.h"
#include "spine\AnimationState.h"
#include "Platform\AEXTime.h"
#include "OptionsButtonLogic.h"


namespace AEX
{
	bool ControlsButtonLogic::onControls = false;

	void ControlsButtonLogic::Initialize()
	{
		onControls = false;
		mControlsClicked = false;
		doneOnce = false;
		mAnim = mOwner->GetComp<SpineAnimationComp>();

		mRotationTimer = 0.0f;
		mRotationBackTimer = 0.0f;
	}
	void ControlsButtonLogic::Update()
	{
		if (doneOnce == false)
		{
			Initialize();
			doneOnce = true;
		}

		if (mRotationTimer > 0.0f)
			mRotationTimer += (f32)aexTime->GetFrameTime();

		if (mRotationBackTimer > 0.0f)
			mRotationBackTimer += (f32)aexTime->GetFrameTime();

		if (mRotationTimer > ROT_AUDIO_TIME)
		{
			mRotationTimer = 0.0f;
			audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Rotation/Bike_BrakesLoop1.wav"), Voice::SFX);
		}

		if (mRotationBackTimer > ROT_BACK_AUDIO_TIME)
		{
			mRotationBackTimer = 0.0f;
			audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Rotation/Bike_BrakesLoop1.wav"), Voice::SFX);
		}

		if (((ENTER && HoverableButton::BS == HoverableButton::ButtonSelected::HowToPlay) || (A_BUTTON && HoverableButton::BS == HoverableButton::ButtonSelected::HowToPlay))
			&& mAnim->GetAnimState()->getCurrent(0)->isComplete() && mControlsClicked == false && onControls == false)
		{
			onControls = true;
			mControlsClicked = true;

			audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/Menu/MainMenu/Selection/Impacts_PROCESSED_001.wav"), Voice::SFX);

			mRotationTimer += (f32)aexTime->GetFrameTime();

			if (mAnim != nullptr)
			{
				mAnim->ReloadSpineData();
				mAnim->ChangeAnimation(0, "howtoplay_selection", false, 1.5f);
			}
		}

		if ((ESC || B_BUTTON) && onControls && mAnim->GetAnimState()->getCurrent(0)->isComplete())
		{
			mRotationBackTimer += (f32)aexTime->GetFrameTime();

			if (mAnim != nullptr)
			{
				mAnim->ReloadSpineData();
				mAnim->ChangeAnimation(0, "howtoplay_to_mainmenu", false, 1.5f);
			}
		}

		if (mAnim->CheckCurrentAnimationAtPostion("howtoplay_to_mainmenu", 0) && mAnim->GetAnimState()->getCurrent(0)->isComplete())
			onControls = false;

		if (mControlsClicked == true && mAnim->GetAnimState()->getCurrent(0)->isComplete())
		{
			mControlsClicked = false;
		}
	}
	void ControlsButtonLogic::Shutdown()
	{

	}

	bool ControlsButtonLogic::OnGui()
	{
		bool isChanged = false;

		return isChanged;
	}

	void ControlsButtonLogic::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);
	}
	void ControlsButtonLogic::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);
	}
}