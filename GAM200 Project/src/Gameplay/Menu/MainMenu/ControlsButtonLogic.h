#pragma once
#include "Components\Logic.h"



class SpineAnimationComp;

namespace AEX
{
	class ControlsButtonLogic : public LogicComp
	{
		AEX_RTTI_DECL(ControlsButtonLogic, LogicComp);

	public:

		void Initialize();
		void Update();
		void Shutdown();

		bool OnGui();

		void FromJson(json & value);
		void ToJson(json & value);

		SpineAnimationComp * mAnim = nullptr;
		bool mControlsClicked = false;
		static bool onControls;

		bool doneOnce = false;

		f32 mRotationTimer = 0.0f;
		f32 mRotationBackTimer = 0.0f;

	private:
	};
}