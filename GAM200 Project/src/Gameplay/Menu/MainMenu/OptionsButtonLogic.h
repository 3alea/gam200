#pragma once
#include "Components\Logic.h"


#define MAIN_MENU_STICK_TIME 0.2f
#define BAR_FACTOR 0.01f

#define ROT_AUDIO_TIME 0.6f
#define ROT_BACK_AUDIO_TIME 0.25f


class SpineAnimationComp;

namespace AEX
{
	class TransformComp;
	class SpriteComp;
	class Voice;

	const unsigned NUMBER_OF_OPTIONS = 6;//7;
	const unsigned NUMBER_OF_RESOLUTIONS = 3;

	class OptionsButtonLogic : public LogicComp
	{
		AEX_RTTI_DECL(OptionsButtonLogic, LogicComp);

	public:

		enum OptionSelected
		{
			//NoneSelected,
			Volume,
			Sound,
			Music,
			Resolution,
			VSync,
			FullScreen
		};

		enum ResolutionSelected
		{
			_1920x1080,
			_1600x900,
			_1280x720
		};

		void Initialize();
		void Update();
		void Shutdown();

		void FromJson(json & value);
		void ToJson(json & value);

		TransformComp * tr = nullptr;
		SpriteComp * sprite = nullptr;

		bool mOptionsClicked = false;

		static bool onOptions;
		static bool optionsShown;

		SpineAnimationComp * mAnim = nullptr;

		bool doneOnce = false;

		OptionSelected mOptionSelected = Volume;//NoneSelected;
		static ResolutionSelected mResolutionSelected;

		SpriteComp * mOnOffVolume = nullptr;
		SpriteComp * mResSprite = nullptr;
		SpriteComp * mOnOffVSync = nullptr;
		SpriteComp * mOnOffFullScreen = nullptr;

		TransformComp * mSoundBallTr = nullptr;
		TransformComp * mMusicBallTr = nullptr;
		SpriteComp * mSoundBallSprite = nullptr;
		SpriteComp * mMusicBallSprite = nullptr;

		float mBallMaxX;
		float mBallMinX;
		static float mSoundBallTn;		// In range [0, 1]. Can only be 0.1, 0.2, 0.3 etc
		static float mMusicBallTn;		// In range [0, 1]. Can only be 0.1, 0.2, 0.3 etc

		static bool mIsVolumeOn;
		static bool mIsVSyncOn;
		static bool mIsFullScreenOn;

		static void UpdateOnOffLights(SpriteComp * volumeOnOff, SpriteComp * vsyncOnOff, SpriteComp * fullscreenOnOff);
		static void UpdateResolutions(SpriteComp * resoultionsSprite);
		static void UpdateActualResolution();

		f32 mRotationTimer = 0.0f;
		f32 mRotationBackTimer = 0.0f;

		Voice * mBarVoice = nullptr;

	private:

		f32 mStickTimer = 0.0f;
		f32 mStickTime;

		void UpdateOptionHovered();
	};

	// Prefix increment
	OptionsButtonLogic::OptionSelected & operator++(OptionsButtonLogic::OptionSelected & option);

	// Postfix increment
	OptionsButtonLogic::OptionSelected operator++(OptionsButtonLogic::OptionSelected & option, int);

	// Prefix decrement
	OptionsButtonLogic::OptionSelected & operator--(OptionsButtonLogic::OptionSelected & option);

	// Postfix decrement
	OptionsButtonLogic::OptionSelected operator--(OptionsButtonLogic::OptionSelected & option, int);


	// Prefix increment
	OptionsButtonLogic::ResolutionSelected & operator++(OptionsButtonLogic::ResolutionSelected & option);

	// Postfix increment
	OptionsButtonLogic::ResolutionSelected operator++(OptionsButtonLogic::ResolutionSelected & option, int);
}