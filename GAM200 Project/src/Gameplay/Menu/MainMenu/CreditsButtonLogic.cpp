#include "CreditsButtonLogic.h"
#include "Platform\AEXInput.h"
#include "Graphics\GfxMgr.h"
#include "Components\SpriteComps.h"
#include "Math\Collisions.h"
#include "Composition\AEXScene.h"
#include "Components\SpineAnimation.h"
#include "HoverableButton.h"
#include "spine\AnimationState.h"
#include "Platform\AEXTime.h"

using namespace spine;

namespace AEX
{
	bool CreditsButtonLogic::onCredits = false;

	void CreditsButtonLogic::Initialize()
	{
		mCreditsTimer = 0.0f;
		onCredits = false;
		mExitedEarly = false;

		tr = mOwner->GetComp<TransformComp>();
		sprite = mOwner->GetComp<SpriteComp>();

		GameObject * endCredisObj = aexScene->FindObjectInANYSpace("credits-text-end");
		GameObject * startCreditsObj = aexScene->FindObjectInANYSpace("credits-text");

		if (endCredisObj)
			mEndCreditsTr = endCredisObj->GetComp<TransformComp>();

		if (startCreditsObj)
		{
			mStartCreditsTr = startCreditsObj->GetComp<TransformComp>();
			mStartCreditsSprite = startCreditsObj->GetComp<SpriteComp>();

			if (mStartCreditsTr)
			{
				mInitialCreditsPos.x = mStartCreditsTr->mLocal.mTranslationZ.x;
				mInitialCreditsPos.y = mStartCreditsTr->mLocal.mTranslationZ.y;
			}
		}
	}

	void CreditsButtonLogic::Update()
	{
		if (doneOnce == false)
		{
			Initialize();
			doneOnce = true;
		}

		GameObject * bg = aexScene->FindObjectInANYSpace("background");
		SpineAnimationComp * anim = nullptr;

		if (bg != nullptr)
			anim = bg->GetComp<SpineAnimationComp>();

		// Mouse input now shouldn't be implemented
		/*if (aexInput->MouseTriggered(Input::eMouseLeft))
		{
			AEVec2 & mousePos = GfxMgr->mCameraList.front()->PointToWorld(aexInput->GetGLFWMousePos());

			AEVec2 thisPos(tr->mLocal.mTranslationZ.x, tr->mLocal.mTranslationZ.y);

			AEVec2 texSize = sprite ? AEVec2(static_cast<f32>(sprite->mTex->get()->GetWidth()) / 100.0f, static_cast<f32>(sprite->mTex->get()->GetHeight() / 100.0f)) : AEVec2(1.0f, 1.0f);
			AEVec2 scale(tr->mLocal.mScale.x * texSize.x, tr->mLocal.mScale.y * texSize.y);

			if (StaticPointToStaticRect(&mousePos, &thisPos, scale.x, scale.y))
			{
				mCreditsClicked = true;
				if (anim != nullptr)
					anim->ChangeAnimation(0, "credits_selection", false, 1.0f);
			}
		}
		else */if (((ENTER && HoverableButton::BS == HoverableButton::ButtonSelected::Credits) || (A_BUTTON && HoverableButton::BS == HoverableButton::ButtonSelected::Credits))
			&& anim->GetAnimState()->getCurrent(0)->isComplete() && mCreditsClicked == false && anim->mColor[3] == 1.0f)
		{
			onCredits = true;
			mCreditsClicked = true;
			mStartCreditsSprite->mColor[3] = 1.0f;

			audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/Menu/MainMenu/Selection/Impacts_PROCESSED_001.wav"), Voice::SFX);

			if (anim != nullptr)
			{
				anim->ReloadSpineData();
				anim->ChangeAnimation(0, "credits_selection", false, 2.0f);
			}
		}

		if (mCreditsClicked && anim->GetAnimState()->getCurrent(0)->isComplete())
		{
			if (mBackToMainMenu == false)
				anim->mColor[3] -= (f32)aexTime->GetFrameTime() * mFadeFactor;

			if (anim->mColor[3] < 0 || mExitedEarly)
			{
				AEVec2 endPos(mEndCreditsTr->mLocal.mTranslationZ.x, mEndCreditsTr->mLocal.mTranslationZ.y);

				f32 tn = mCreditsTimer / mCreditsTime;

				if (tn > 1)
				{
					// Credits finished
					mCreditsTimer = 0.0f;
					mCreditsClicked = false;

					tn = 1;
				}

				AEVec2 lerpResult = AEVec2::Lerp(endPos, mInitialCreditsPos, tn);

				//mStartCreditsTr->mLocal.mTranslationZ.x = lerpResult.x;
				mStartCreditsTr->mLocal.mTranslationZ.y = lerpResult.y;

				mCreditsTimer += (f32)aexTime->GetFrameTime();

				if (mBackToMainMenu == false)
					anim->mColor[3] = 0;

				if ((ESC || B_BUTTON) && mExitedEarly == false)
				{
					mExitedEarly = true;
					mBackToMainMenu = true;
				}

				if (mExitedEarly)
					mStartCreditsSprite->mColor[3] -= (f32)aexTime->GetFrameTime() * (mFadeFactor + 0.25f);

				if (mCreditsClicked == false)
					mBackToMainMenu = true;
			}

			//anim->ChangeAnimation(0, "credits_goingup", false, 1.0f);
		}

		if (mBackToMainMenu)
		{
			anim->mColor[3] += (f32)aexTime->GetFrameTime() * mFadeFactor;

			if (anim->mColor[3] > 1)
			{
				mBackToMainMenu = false;
				onCredits = false;
				anim->mColor[3] = 1.0f;

				if (mExitedEarly)
				{
					mStartCreditsTr->mLocal.mTranslationZ.x = mInitialCreditsPos.x;
					mStartCreditsTr->mLocal.mTranslationZ.y = mInitialCreditsPos.y;

					mCreditsTimer = 0.0f;
					mCreditsClicked = false;
					mExitedEarly = false;
				}
			}
		}
	}

	void CreditsButtonLogic::Shutdown()
	{

	}


	bool CreditsButtonLogic::OnGui()
	{
		bool isChanged = false;

		if (ImGui::InputFloat("Fade Factor", &mFadeFactor))
		{
			isChanged = true;
		}
		if (ImGui::InputFloat("Credits Time", &mCreditsTime))
		{
			isChanged = true;
		}

		return isChanged;
	}

	void CreditsButtonLogic::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);

		value["Credits Time"] >> mCreditsTime;
		value["Fade factor"] >> mFadeFactor;
	}

	void CreditsButtonLogic::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);

		value["Credits Time"] << mCreditsTime;
		value["Fade factor"] << mFadeFactor;
	}
}