#pragma once

#include "Components/Logic.h"

#define STICK_TIME 0.3f

class SpineAnimationComp;

namespace AEX
{
	class TransformComp;
	class SpriteComp;

	class ExitButtonLogic : public LogicComp
	{
		AEX_RTTI_DECL(ExitButtonLogic, LogicComp);

	public:

		void Initialize();
		void Update();
		void Shutdown();

		void FromJson(json & value);
		void ToJson(json & value);

		void AreYouSureLogic();

		bool mDoneOnce = false;

		unsigned mAreYouSure = 0; // No is 0. Yes is 1.

		f32 mStickTimer = 0.0f;
		f32 mGoBackTimer = 0.0f;

		static bool mOnAreYouSure;

		//static bool mCanShowAreYouSure;

		SpineAnimationComp * mBgAnim = nullptr;

	private:
		TransformComp * tr = nullptr;
		SpriteComp * sprite = nullptr;

		SpriteComp * mAreYouSureSprite = nullptr;
	};
}