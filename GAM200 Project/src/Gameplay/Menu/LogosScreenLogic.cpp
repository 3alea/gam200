#include "LogosScreenLogic.h"
#include "Components/SpriteComps.h"
#include "Composition/AEXScene.h"
#include "Platform/AEXTime.h"


namespace AEX
{
	void LogosScreenLogic::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);

		value["Speed Till Max Alpha"] >> mSpeedTillMaxAlpha;
		value["Time In Max Alpha"] >> mTimeInMaxAlpha;
		value["Speed Till Min Alpha"] >> mSpeedTillMinAlpha;
	}
	void LogosScreenLogic::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);

		value["Speed Till Max Alpha"] << mSpeedTillMaxAlpha;
		value["Time In Max Alpha"] << mTimeInMaxAlpha;
		value["Speed Till Min Alpha"] << mSpeedTillMinAlpha;
	}

	bool LogosScreenLogic::OnGui()
	{
		bool isChanged = false;

		if (ImGui::InputFloat("Speed Till Max Alpha", &mSpeedTillMaxAlpha))
			isChanged = true;
		if (ImGui::InputFloat("Time In Max Alpha", &mTimeInMaxAlpha))
			isChanged = true;
		if (ImGui::InputFloat("Speed Till Min Alpha", &mSpeedTillMinAlpha))
			isChanged = true;

		return isChanged;
	}

	void LogosScreenLogic::Initialize()
	{
		mTimer = 0.0f;
		mLogosShown = 0;

		GameObject * dpLogoObj = aexScene->FindObjectInANYSpace("digipenLogo");

		if (dpLogoObj)
			mDigipenLogo = dpLogoObj->GetComp<SpriteComp>();

		GameObject * doxaLogoObj = aexScene->FindObjectInANYSpace("doxaLogo");

		if (doxaLogoObj)
			mDoxaLogo = doxaLogoObj->GetComp<SpriteComp>();

		GameObject * fmodLogoObj = aexScene->FindObjectInANYSpace("fmodLogo");

		if (fmodLogoObj)
			mFmodLogo = fmodLogoObj->GetComp<SpriteComp>();

		GameObject * spineLogoObj = aexScene->FindObjectInANYSpace("spineLogo");

		if (spineLogoObj)
			mSpineLogo = spineLogoObj->GetComp<SpriteComp>();
	}
	void LogosScreenLogic::Update()
	{
		if (doneOnce == false)
		{
			Initialize();
			doneOnce = true;
		}

		UpdateLogosAlpha();
	}
	void LogosScreenLogic::Shutdown()
	{

	}

	void LogosScreenLogic::UpdateLogosAlpha()
	{
		if (mLogosShown == 0)
		{
			ActualAlphaUpdater(mDigipenLogo);
		}
		else if (mLogosShown == 1)
		{
			ActualAlphaUpdater(mDoxaLogo);
		}
		else if (mLogosShown == 2)
		{
			ActualAlphaUpdater(mFmodLogo);
		}
		else if (mLogosShown == 3)
		{
			ActualAlphaUpdater(mSpineLogo);
		}
		else if (mLogosShown == 4)
		{
			aexScene->ChangeLevel("data/Levels/Main Menu.json");
		}
	}

	void LogosScreenLogic::ActualAlphaUpdater(SpriteComp * sprite)
	{
		// Increasing alpha
		if (sprite->mColor[3] < 1.0f && mTimer == 0.0f)
		{
			sprite->mColor[3] += (f32)aexTime->GetFrameTime() * mSpeedTillMaxAlpha;
		}
		// Alpha stays constant
		else if (sprite->mColor[3] >= 1.0f && mTimer < mTimeInMaxAlpha)
		{
			sprite->mColor[3] = 1.0f;
			mTimer += (f32)aexTime->GetFrameTime();
		}
		// Alpha decreases
		else if (mTimer >= mTimeInMaxAlpha)
		{
			sprite->mColor[3] -= (f32)aexTime->GetFrameTime() * mSpeedTillMinAlpha;

			if (sprite->mColor[3] <= 0)
			{
				sprite->mColor[3] = 0.0f;
				mTimer = 0.0f;
				++mLogosShown;
			}
		}
	}
}