#pragma once

#include "Components/Logic.h"


namespace AEX
{
	class SpriteComp;

	class LogosScreenLogic : public LogicComp
	{
		AEX_RTTI_DECL(LogosScreenLogic, LogicComp);

	public:

		void FromJson(json & value);
		void ToJson(json & value);

		bool OnGui();

		virtual void Initialize();
		virtual void Update();
		virtual void Shutdown();
		
		SpriteComp * mDigipenLogo = nullptr;
		SpriteComp * mDoxaLogo = nullptr;
		SpriteComp * mFmodLogo = nullptr;
		SpriteComp * mSpineLogo = nullptr;

		bool doneOnce = false;

		unsigned mLogosShown = 0;
		f32 mTimer = 0.0f;
		f32 mSpeedTillMaxAlpha = 1.0f;
		f32 mTimeInMaxAlpha = 1.0f;
		f32 mSpeedTillMinAlpha = 1.0f;

		void UpdateLogosAlpha();

	private:

		void ActualAlphaUpdater(SpriteComp * sprite);
	};
}