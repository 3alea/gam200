#include "MouseFollower.h"
#include "Composition/AEXGameObject.h"
#include "Editor/Editor.h"
#include "Graphics/GfxMgr.h"
#include "Components/Collider.h"


namespace AEX
{
	void MouseFollower::Initialize()
	{
		tr = mOwner->GetComp<TransformComp>();
		collider = mOwner->GetComp<BoxCollider>();
	}

	void MouseFollower::Update()
	{
		Camera * cam = GfxMgr->mCameraList.front();

		AEVec2 & mousePos = cam->PointToWorld(aexInput->GetGLFWMousePos());

		tr->mLocal.mTranslationZ.x = mousePos.x;
		tr->mLocal.mTranslationZ.y = mousePos.y;

		/*Transform trans;

		trans.mOrientation = 0.0f;
		trans.mScale = collider->mRealScale;
		trans.mTranslationZ = tr->mLocal.mTranslationZ;

		GfxMgr->DrawRectangle(trans, AEVec4(0, 1, 0, 1));*/
	}

	void MouseFollower::Shutdown()
	{

	}


	bool MouseFollower::OnGui()
	{
		bool isChanged = false;

		return isChanged;
	}


	void MouseFollower::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);
	}

	void MouseFollower::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);
	}
}