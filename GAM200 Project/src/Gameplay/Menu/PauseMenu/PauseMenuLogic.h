#pragma once

#include "Components/PausedLogic.h"

#define STICK_TIME 0.2f
#define SELECTION_TIME 0.25f


class PlayerStats;

namespace AEX
{
	const unsigned NUMBER_OF_BUTTONS = 6;
	const unsigned NUMBER_OF_OPTIONS_PAUSE = 6;

	class SpriteComp;
	class TransformComp;
	class Voice;

	class PauseMenuLogic : public PausedLogicComp
	{
		AEX_RTTI_DECL(PauseMenuLogic, PausedLogicComp);

	public:

		enum ButtonSelected
		{
			Resume,
			LastCheckpoint,
			HowToPlay,
			RestartLevel,
			Options,
			Exit,
		};

		enum AreYouSure
		{
			No,
			Yes
		};

		enum OptionSelected
		{
			Volume,
			Sound,
			Music,
			Resolution,
			VSync,
			FullScreen
		};

		void Initialize();
		void Update();
		void Shutdown();

		void FromJson(json & value);
		void ToJson(json & value);

		static ButtonSelected BS;
		static bool onStart;

		static AreYouSure ruSure;

		SpriteComp * mSprite = nullptr;
		PlayerStats * mPlayerStats = nullptr;

		SpriteComp * mHowToPlaySprite = nullptr;

		SpriteComp * mOptionsSprite = nullptr;
		SpriteComp * mOnOffVolumeSprite = nullptr;
		SpriteComp * mResolutionsSprite = nullptr;
		SpriteComp * mOnOffVSyncSprite = nullptr;
		SpriteComp * mOnOffFullScreenSprite = nullptr;

		TransformComp * mLeftBallTr = nullptr;
		TransformComp * mRightBallTr = nullptr;
		TransformComp * mSoundBallTr = nullptr;
		TransformComp * mMusicBallTr = nullptr;
		SpriteComp * mSoundBallSprite = nullptr;
		SpriteComp * mMusicBallSprite = nullptr;
		
		f32 mStickTimer = 0.0f;
		static f32 mSelectionTimer;

		static bool mAreYouSureActive;
		static bool mHowToPlayActive;
		static bool mOptionsActive;

		OptionSelected mOptionSelected = Volume;

		Voice * mBarVoice = nullptr;

		void UpdateButtonSelected();
		void UpdateOptionSelected();

		void CreateOptionMenu();
	};

	// Prefix increment
	PauseMenuLogic::ButtonSelected & operator++(PauseMenuLogic::ButtonSelected & option);

	// Postfix increment
	PauseMenuLogic::ButtonSelected operator++(PauseMenuLogic::ButtonSelected & option, int);

	// Prefix decrement
	PauseMenuLogic::ButtonSelected & operator--(PauseMenuLogic::ButtonSelected & option);

	// Postfix decrement
	PauseMenuLogic::ButtonSelected operator--(PauseMenuLogic::ButtonSelected & option, int);


	// Prefix increment
	PauseMenuLogic::OptionSelected & operator++(PauseMenuLogic::OptionSelected & option);

	// Postfix increment
	PauseMenuLogic::OptionSelected operator++(PauseMenuLogic::OptionSelected & option, int);

	// Prefix decrement
	PauseMenuLogic::OptionSelected & operator--(PauseMenuLogic::OptionSelected & option);

	// Postfix decrement
	PauseMenuLogic::OptionSelected operator--(PauseMenuLogic::OptionSelected & option, int);
}