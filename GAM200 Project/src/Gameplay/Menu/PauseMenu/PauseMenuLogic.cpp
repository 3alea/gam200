#include "PauseMenuLogic.h"
#include "Platform/AEXInput.h"
#include "Menu/MainMenu/HoverableButton.h"
#include "Platform/AEXTime.h"
#include "Editor/Editor.h"
#include "Menu/MainMenu/OptionsButtonLogic.h"
//#include "Components/SpriteComps.h"
//#include "Resources/AEXResourceManager.h"


namespace AEX
{
	PauseMenuLogic::ButtonSelected PauseMenuLogic::BS = PauseMenuLogic::ButtonSelected::Resume;
	bool PauseMenuLogic::onStart = false;
	PauseMenuLogic::AreYouSure PauseMenuLogic::ruSure = PauseMenuLogic::AreYouSure::No;
	f32 PauseMenuLogic::mSelectionTimer = 0.0f;

	bool PauseMenuLogic::mAreYouSureActive = false;
	bool PauseMenuLogic::mHowToPlayActive = false;
	bool PauseMenuLogic::mOptionsActive = false;

	void PauseMenuLogic::Initialize()
	{
		BS = Resume;
		ruSure = No;
		onStart = false;
		mStickTimer = 0.0f;
		mSelectionTimer = 0.0f;
		mAreYouSureActive = false;
		mHowToPlayActive = false;
		mOptionsActive = false;

		mOptionSelected = Volume;

		mSprite = mOwner->GetComp<SpriteComp>();

		GameObject * player = aexScene->FindObjectInANYSpace("player");
		GameObject * howToPlayObj = aexScene->FindObjectInANYSpace("howtoplay");
		
		if (player != nullptr)
			mPlayerStats = player->GetComp<PlayerStats>();

		if (howToPlayObj != nullptr)
			mHowToPlaySprite = howToPlayObj->GetComp<SpriteComp>();

		GameObject * leftBallObj = aexScene->FindObjectInANYSpace("LeftBall");
		GameObject * rightBallObj = aexScene->FindObjectInANYSpace("RightBall");

		if (leftBallObj && rightBallObj)
		{
			mLeftBallTr = leftBallObj->GetComp<TransformComp>();
			mRightBallTr = rightBallObj->GetComp<TransformComp>();
		}

		audiomgr->SetPauseStateForAll(true);

		aexScene->mOnPauseMenu = true;
	}
	void PauseMenuLogic::Update()
	{
		// If the game is paused
		if (aexScene->mGamePaused)	// Now that I have added the PauseLogicComps, this is not necessary, but I'll leave it just in case as it already works
		{
			// Update the timer only when the stick has been moved
			if (mStickTimer > 0.0f)
				mStickTimer += (f32)aexTime->GetFrameTime();

			// Update the timer only when sth has been selected
			if (mSelectionTimer > 0.0f)
				mSelectionTimer += (f32)aexTime->GetFrameTime();

			// Reset the stick timer
			if (mStickTimer > STICK_TIME)
				mStickTimer = 0.0f;

			// Reset the selection timer
			if (mSelectionTimer > SELECTION_TIME)
				mSelectionTimer = 0.0f;

			// If going up, and the stick timer has finished
			if (MENU_GOING_UP && mStickTimer == 0.0f &&
				mAreYouSureActive == false && mHowToPlayActive == false && mOptionsActive == false)
			{
				--BS;
				mStickTimer += (f32)aexTime->GetFrameTime();
			}

			// If going down and the stick timer has finished
			if (MENU_GOING_DOWN && mStickTimer == 0.0f &&
				mAreYouSureActive == false && mHowToPlayActive == false && mOptionsActive == false)
			{
				++BS;
				mStickTimer += (f32)aexTime->GetFrameTime();
			}

			// Logic to move through the options menu
			if (MENU_GOING_UP && mStickTimer == 0.0f && mOptionsActive)
			{
				--mOptionSelected;
				mStickTimer += (f32)aexTime->GetFrameTime();
			}
			if (MENU_GOING_DOWN && mStickTimer == 0.0f && mOptionsActive)
			{
				++mOptionSelected;
				mStickTimer += (f32)aexTime->GetFrameTime();
			}
			
			// Update the buttons' textures
			UpdateButtonSelected();
		}
	}
	void PauseMenuLogic::Shutdown()
	{

	}

	void PauseMenuLogic::UpdateButtonSelected()
	{
		// Only perform logic of the pause main menu if we are not in are you sure screen, in the how to play screen, or in the options screen
		if (mAreYouSureActive == false && mHowToPlayActive == false && mOptionsActive == false)
		{
			// If the button selected is resume
			if (BS == Resume)
			{
				mSprite->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/PauseMenu/ingame_pause_RESUME.png");

				// Unpause game
				if ((ENTER || A_BUTTON) && mSelectionTimer == 0.0f)
				{
					audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/OptionSelection/14 - Gun Insert Magazine Into Ak-47.wav"), Voice::SFX);
					aexScene->PauseOrUnPause();
				}
			}
			// If the button selected is last checkpoint
			else if (BS == LastCheckpoint)
			{
				mSprite->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/PauseMenu/ingame_pause_lASTCHECKPOINT.png");

				// Open AreYouSure screen
				if ((ENTER || A_BUTTON) && mSelectionTimer == 0.0f)
				{
					audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/OptionSelection/14 - Gun Insert Magazine Into Ak-47.wav"), Voice::SFX);
					mAreYouSureActive = true;
					mSelectionTimer += (f32)aexTime->GetFrameTime();
				}
			}
			// If the button selected is the how to play
			else if (BS == HowToPlay)
			{
				mSprite->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/PauseMenu/ingame_pause_HOWTO.png");

				// Open the How To Play box
				if ((ENTER || A_BUTTON) && mSelectionTimer == 0.0f)
				{
					audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/OptionSelection/14 - Gun Insert Magazine Into Ak-47.wav"), Voice::SFX);
					//mSprite->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/Options/how_to_no_chains.png");
					mSprite->mColor[3] = 0.0f;
					mHowToPlayActive = true;
					mSelectionTimer += (f32)aexTime->GetFrameTime();
				}
			}
			// If the button selected is the restart level
			else if (BS == RestartLevel)
			{
				mSprite->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/PauseMenu/ingame_pause_RESTARTLEVEL.png");

				// Open Are You Sure screen
				if ((ENTER || A_BUTTON) && mSelectionTimer == 0.0f)
				{
					audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/OptionSelection/14 - Gun Insert Magazine Into Ak-47.wav"), Voice::SFX);
					mAreYouSureActive = true;
					mSelectionTimer += (f32)aexTime->GetFrameTime();
				}
			}
			// If the button selected is options
			else if (BS == Options)
			{
				mSprite->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/PauseMenu/ingame_pause_OPTIONS.png");

				// Open Options screen
				if ((ENTER || A_BUTTON) && mSelectionTimer == 0.0f)
				{
					audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/OptionSelection/14 - Gun Insert Magazine Into Ak-47.wav"), Voice::SFX);
					//mSprite->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/Options/volume_on.png");
					mSprite->mColor[3] = 0.0f;
					CreateOptionMenu();
					mOptionsActive = true;
					mSelectionTimer += (f32)aexTime->GetFrameTime();
				}
			}
			// If the button selected is exit (to main menu)
			else if (BS == Exit)
			{
				mSprite->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/PauseMenu/ingame_pause_EXIT.png");

				// Open Are You Sure screen
				if ((ENTER || A_BUTTON) && mSelectionTimer == 0.0f)
				{
					audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/OptionSelection/14 - Gun Insert Magazine Into Ak-47.wav"), Voice::SFX);
					mAreYouSureActive = true;
					mSelectionTimer += (f32)aexTime->GetFrameTime();
				}
			}
		}

		// Logic to go back from the How To Play screen
		if (mHowToPlayActive)
		{
			if (mHowToPlaySprite)
				mHowToPlaySprite->mColor[3] = 1.0f;

			if ((ESC || B_BUTTON) && mSelectionTimer == 0.0f)
			{
				mHowToPlayActive = false;
				mSprite->mColor[3] = 1.0f;
				mSelectionTimer += (f32)aexTime->GetFrameTime();
			}
		}
		else if (mHowToPlaySprite)
			mHowToPlaySprite->mColor[3] = 0.0f;

		// Logic of the options menu
		if (mOptionsActive)
		{
			if (mOptionsSprite && mOnOffVolumeSprite && mResolutionsSprite && mOnOffVSyncSprite && mOnOffFullScreenSprite &&
				mSoundBallSprite && mMusicBallSprite)
			{
				mOptionsSprite->mColor[3] = 1.0f;
				mOnOffVolumeSprite->mColor[3] = 1.0f;
				mResolutionsSprite->mColor[3] = 1.0f;
				mOnOffVSyncSprite->mColor[3] = 1.0f;
				mOnOffFullScreenSprite->mColor[3] = 1.0f;
				mSoundBallSprite->mColor[3] = 1.0f;
				mMusicBallSprite->mColor[3] = 1.0f;
			}

			UpdateOptionSelected();

			if ((ESC || B_BUTTON) && mSelectionTimer == 0.0f)
			{
				mOptionsActive = false;
				mSprite->mColor[3] = 1.0f;
				mSelectionTimer += (f32)aexTime->GetFrameTime();
			}
		}
		else if (mOptionsSprite && mOnOffVolumeSprite && mResolutionsSprite && mOnOffVSyncSprite && mOnOffFullScreenSprite &&
				mSoundBallSprite && mMusicBallSprite)
		{
			mOptionsSprite->mColor[3] = 0.0f;
			mOnOffVolumeSprite->mColor[3] = 0.0f;
			mResolutionsSprite->mColor[3] = 0.0f;
			mOnOffVSyncSprite->mColor[3] = 0.0f;
			mOnOffFullScreenSprite->mColor[3] = 0.0f;
			mSoundBallSprite->mColor[3] = 0.0f;
			mMusicBallSprite->mColor[3] = 0.0f;
		}

		// Logic of the Are You Sure Screen
		if (mAreYouSureActive)
		{
			// To move from no to yes and viceversa
			if (MENU_GOING_LEFT && mStickTimer == 0.0f)
			{
				audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/OptionTraversal/Typewriter letter key long 01 rode.wav"), Voice::SFX);
				ruSure = AreYouSure(((int)ruSure + 1) % 2);
				mStickTimer += (f32)aexTime->GetFrameTime();
			}
			if (MENU_GOING_RIGHT && mStickTimer == 0.0f)
			{
				audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/OptionTraversal/Typewriter letter key long 01 rode.wav"), Voice::SFX);

				// Have to hardcode it due to negative numbers
				if (ruSure == No)
					ruSure = Yes;
				else
					ruSure = No;
				mStickTimer += (f32)aexTime->GetFrameTime();
			}

			// If the Are You Sure is on No
			if (ruSure == No)
			{
				mSprite->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/PauseMenu/areyousure_NO.png");

				// To exit the Are You Sure screen
				if ((ENTER || A_BUTTON) && mSelectionTimer == 0.0f)
				{
					audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/OptionSelection/14 - Gun Insert Magazine Into Ak-47.wav"), Voice::SFX);
					mAreYouSureActive = false;
					mSelectionTimer += (f32)aexTime->GetFrameTime();
				}
			}
			// If the Are You Sure is on Yes
			else if (ruSure == Yes)
			{
				mSprite->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/PauseMenu/areyousure_YES.png");

				// If Yes is selected
				if ((ENTER || A_BUTTON) && mSelectionTimer == 0.0f)
				{
					// If we are on LastCheckpoint, load from last checkpoint
					if (BS == LastCheckpoint)
					{
						audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/OptionSelection/14 - Gun Insert Magazine Into Ak-47.wav"), Voice::SFX);
						aexScene->SetPauseState(false);
						mPlayerStats->lives = 0;
						mAreYouSureActive = false;
					}
					// If we are on RestartLevel, reload the level
					else if (BS == RestartLevel)
					{
						audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/OptionSelection/14 - Gun Insert Magazine Into Ak-47.wav"), Voice::SFX);
						aexScene->SetPauseState(false);

						Editor->levelToLoadName = aexScene->GetBaseName();
						Editor->loadFromCertainLevel = true;
						mAreYouSureActive = false;
						//std::string path = "data/Levels/";
						//path += aexScene->GetBaseName();
						//path += ".json";
						//aexScene->ChangeLevel(path);
					}
					// If we are on Exit, exit to the main menu
					else if (BS == Exit)
					{
						audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/OptionSelection/14 - Gun Insert Magazine Into Ak-47.wav"), Voice::SFX);
						aexScene->SetPauseState(false);
						aexScene->ChangeLevel("data/Levels/Main Menu.json");
						mAreYouSureActive = false;
					}
				}
			}

			// To exit the Are You Sure screen
			if ((ESC || B_BUTTON) && mSelectionTimer == 0.0f)
			{
				mAreYouSureActive = false;
				ruSure = No;
				mSelectionTimer += (f32)aexTime->GetFrameTime();
			}
		}
	}

	void PauseMenuLogic::UpdateOptionSelected()
	{
		// If the option selected is volume
		if (mOptionSelected == Volume)
		{
			mOptionsSprite->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/Options/volume_on.png");

			// Mute/Unmute
			if ((ENTER || A_BUTTON) && mSelectionTimer == 0.0f)
			{
				mSelectionTimer += (f32)aexTime->GetFrameTime();
				audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/OptionSelection/14 - Gun Insert Magazine Into Ak-47.wav"), Voice::SFX);
				OptionsButtonLogic::mIsVolumeOn = !OptionsButtonLogic::mIsVolumeOn;
				//OptionsButtonLogic::UpdateOnOffLights(mOnOffVolumeSprite, mOnOffVSyncSprite, mOnOffFullScreenSprite);
				audiomgr->SetMuteStateForAll(!OptionsButtonLogic::mIsVolumeOn);
			}
		}
		// If the option selected is sound
		else if (mOptionSelected == OptionSelected::Sound)
		{
			mOptionsSprite->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/Options/sound_on.png");

			// Logic to control the sfx volume
			if (MENU_GOING_RIGHT)
			{
				OptionsButtonLogic::mSoundBallTn += BAR_FACTOR;
				OptionsButtonLogic::mSoundBallTn = AEX::Clamp(OptionsButtonLogic::mSoundBallTn, 0.0f, 1.0f);

				audiomgr->SetSFXVolume(OptionsButtonLogic::mSoundBallTn);

				if ((mBarVoice == nullptr || mBarVoice->IsValid() == false) && OptionsButtonLogic::mSoundBallTn != 1.0f)
					mBarVoice = audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/Bar/43 - Pneumatic Door Mechanism Hiss #2.wav"), Voice::SFX);

				if (OptionsButtonLogic::mSoundBallTn == 1.0f && mBarVoice)
				{
					audiomgr->FreeThisVoice(mBarVoice);
					mBarVoice = nullptr;
				}
			}
			if (MENU_GOING_LEFT)
			{
				OptionsButtonLogic::mSoundBallTn -= BAR_FACTOR;
				OptionsButtonLogic::mSoundBallTn = AEX::Clamp(OptionsButtonLogic::mSoundBallTn, 0.0f, 1.0f);

				audiomgr->SetSFXVolume(OptionsButtonLogic::mSoundBallTn);

				if ((mBarVoice == nullptr || (mBarVoice && mBarVoice->IsValid() == false)) && OptionsButtonLogic::mSoundBallTn != 0.0f)
					mBarVoice = audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/Bar/43 - Pneumatic Door Mechanism Hiss #2.wav"), Voice::SFX);

				if (OptionsButtonLogic::mSoundBallTn == 0.0f && mBarVoice)
				{
					audiomgr->FreeThisVoice(mBarVoice);
					mBarVoice = nullptr;
				}
			}
		}
		// If the option selected is the music
		else if (mOptionSelected == Music)
		{
			mOptionsSprite->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/Options/music_on.png");

			// Logic to control the music volume
			if (MENU_GOING_RIGHT)
			{
				OptionsButtonLogic::mMusicBallTn += BAR_FACTOR;
				OptionsButtonLogic::mMusicBallTn = AEX::Clamp(OptionsButtonLogic::mMusicBallTn, 0.0f, 1.0f);

				audiomgr->SetMusicVolume(OptionsButtonLogic::mMusicBallTn);

				if ((mBarVoice == nullptr || (mBarVoice && mBarVoice->IsValid() == false)) && OptionsButtonLogic::mMusicBallTn != 1.0f)
					mBarVoice = audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/Bar/43 - Pneumatic Door Mechanism Hiss #2.wav"), Voice::SFX);

				if (OptionsButtonLogic::mMusicBallTn == 1.0f && mBarVoice)
				{
					audiomgr->FreeThisVoice(mBarVoice);
					mBarVoice = nullptr;
				}
			}
			if (MENU_GOING_LEFT)
			{
				OptionsButtonLogic::mMusicBallTn -= BAR_FACTOR;
				OptionsButtonLogic::mMusicBallTn = AEX::Clamp(OptionsButtonLogic::mMusicBallTn, 0.0f, 1.0f);

				audiomgr->SetMusicVolume(OptionsButtonLogic::mMusicBallTn);

				if ((mBarVoice == nullptr || (mBarVoice && mBarVoice->IsValid() == false)) && OptionsButtonLogic::mMusicBallTn != 0.0f)
					mBarVoice = audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/Bar/43 - Pneumatic Door Mechanism Hiss #2.wav"), Voice::SFX);

				if (OptionsButtonLogic::mMusicBallTn == 0.0f && mBarVoice)
				{
					audiomgr->FreeThisVoice(mBarVoice);
					mBarVoice = nullptr;
				}
			}
		}
		// If the option selected is the resolution
		else if (mOptionSelected == Resolution)
		{
			mOptionsSprite->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/Options/resolution_on.png");

			// Logic to change resolution
			if ((ENTER || A_BUTTON) && mSelectionTimer == 0.0f)
			{
				mSelectionTimer += (f32)aexTime->GetFrameTime();
				++OptionsButtonLogic::mResolutionSelected;
				OptionsButtonLogic::UpdateActualResolution();
			}
		}
		// If the option selected if the vsync
		else if (mOptionSelected == VSync)
		{
			mOptionsSprite->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/Options/vsync_on.png");

			// Enable/Disable vsync
			if ((ENTER || A_BUTTON) && mSelectionTimer == 0.0f)
			{
				mSelectionTimer += (f32)aexTime->GetFrameTime();
				OptionsButtonLogic::mIsVSyncOn = !OptionsButtonLogic::mIsVSyncOn;
				//OptionsButtonLogic::UpdateOnOffLights(mOnOffVolumeSprite, mOnOffVSyncSprite, mOnOffFullScreenSprite);
				GfxMgr->SetVSync(OptionsButtonLogic::mIsVSyncOn);
				audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/OptionSelection/14 - Gun Insert Magazine Into Ak-47.wav"), Voice::SFX);
			}
		}
		// If the option selected is the fullscreen
		else if (mOptionSelected == FullScreen)
		{
			mOptionsSprite->mTex = aexResourceMgr->getResource<Texture>("data/Textures/Menu/Options/fullscreen_on.png");

			// Activate/disactivate fullscreen
			if ((ENTER || A_BUTTON) && mSelectionTimer == 0.0f)
			{
				mSelectionTimer += (f32)aexTime->GetFrameTime();
				OptionsButtonLogic::mIsFullScreenOn = !OptionsButtonLogic::mIsFullScreenOn;
				//OptionsButtonLogic::UpdateOnOffLights(mOnOffVolumeSprite, mOnOffVSyncSprite, mOnOffFullScreenSprite);
				audiomgr->Play(aexResourceMgr->getResource<AEX::Sound>("data/Audio/Menu/MainMenu/Options/OptionSelection/14 - Gun Insert Magazine Into Ak-47.wav"), Voice::SFX);
				WindowMgr->GetCurrentWindow()->SetFullScreen(OptionsButtonLogic::mIsFullScreenOn);
			}
		}

		mSoundBallTr->mLocal.mTranslationZ.x = AEX::Lerp<f32>(mLeftBallTr->mLocal.mTranslationZ.x, mRightBallTr->mLocal.mTranslationZ.x, OptionsButtonLogic::mSoundBallTn);
		mMusicBallTr->mLocal.mTranslationZ.x = AEX::Lerp<f32>(mLeftBallTr->mLocal.mTranslationZ.x, mRightBallTr->mLocal.mTranslationZ.x, OptionsButtonLogic::mMusicBallTn);

		OptionsButtonLogic::UpdateOnOffLights(mOnOffVolumeSprite, mOnOffVSyncSprite, mOnOffFullScreenSprite);
		OptionsButtonLogic::UpdateResolutions(mResolutionsSprite);

		if (mOptionsActive == false || (mOptionSelected != Sound && mOptionSelected != Music) ||
			(MENU_GOING_LEFT == false && MENU_GOING_RIGHT == false) || (MENU_GOING_LEFT && MENU_GOING_RIGHT))
		{
			if (mBarVoice)
			{
				audiomgr->FreeThisVoice(mBarVoice);
				mBarVoice = nullptr;
			}
		}
	}

	void PauseMenuLogic::CreateOptionMenu()
	{
		// More than one on/off and balls created to avoid problems because of identical names

		GameObject * optionsObj = aexScene->FindObjectInANYSpace("OptionsPauseMenu");
		GameObject * volumeOnOffObj = aexScene->FindObjectInANYSpace("VolumeOnOff");
		GameObject * resolutionsObj = aexScene->FindObjectInANYSpace("PauseResolutions");
		GameObject * vsyncOnOffObj = aexScene->FindObjectInANYSpace("VSyncOnOff");
		GameObject * fullscreenOnOffObj = aexScene->FindObjectInANYSpace("FullScreenOnOff");

		GameObject * leftBallObj = aexScene->FindObjectInANYSpace("LeftBall");
		GameObject * rightBallObj = aexScene->FindObjectInANYSpace("RightBall");
		GameObject * soundBallObj = aexScene->FindObjectInANYSpace("SoundBallPause");
		GameObject * musicBallObj = aexScene->FindObjectInANYSpace("MusicBallPause");

		GameObject * camObj = aexScene->FindObjectInANYSpace("CameraHUD");
		TransformComp * camTr = nullptr;

		if (camObj)
			camTr = camObj->GetComp<TransformComp>();

		// If it doesn't exist, instantiate archetype
		if (camTr && !optionsObj && !volumeOnOffObj && !resolutionsObj && !vsyncOnOffObj && !fullscreenOnOffObj && !leftBallObj && !rightBallObj && !soundBallObj && !musicBallObj)
		{
			optionsObj = aexScene->mSpaces.front()->InstantiateArchetype("PauseOptions", AEVec3(camTr->mLocal.mTranslationZ.x, camTr->mLocal.mTranslationZ.y + 0.8f, 405.0f));
			TransformComp * optionsTr = nullptr;
			if (optionsObj)
				optionsTr = optionsObj->GetComp<TransformComp>();

			volumeOnOffObj = aexScene->mSpaces.front()->InstantiateArchetype("VolumeOnOff", AEVec3(optionsTr->mLocal.mTranslationZ.x, optionsTr->mLocal.mTranslationZ.y, 410.0f));
			TransformComp * volumeTr = nullptr;
			if (volumeOnOffObj)
				volumeTr = volumeOnOffObj->GetComp<TransformComp>();

			resolutionsObj = aexScene->mSpaces.front()->InstantiateArchetype("PauseResolutions", AEVec3(volumeTr->mLocal.mTranslationZ.x, volumeTr->mLocal.mTranslationZ.y, 410.0f));

			vsyncOnOffObj = aexScene->mSpaces.front()->InstantiateArchetype("VSyncOnOff", AEVec3(volumeTr->mLocal.mTranslationZ.x, volumeTr->mLocal.mTranslationZ.y - 4.0f, 410.0f));

			fullscreenOnOffObj = aexScene->mSpaces.front()->InstantiateArchetype("FullScreenOnOff", AEVec3(volumeTr->mLocal.mTranslationZ.x, volumeTr->mLocal.mTranslationZ.y - 5.0f, 410.0f));

			leftBallObj = aexScene->mSpaces.front()->InstantiateArchetype("LeftBall", AEVec3(volumeTr->mLocal.mTranslationZ.x + 0.5f, volumeTr->mLocal.mTranslationZ.y + 0.27f, 410.0f));
			rightBallObj = aexScene->mSpaces.front()->InstantiateArchetype("RightBall", AEVec3(volumeTr->mLocal.mTranslationZ.x + 2.64f, volumeTr->mLocal.mTranslationZ.y + 0.27f, 410.0f));

			if (leftBallObj && rightBallObj)
			{
				mLeftBallTr = leftBallObj->GetComp<TransformComp>();
				mRightBallTr = rightBallObj->GetComp<TransformComp>();
			}

			soundBallObj = aexScene->mSpaces.front()->InstantiateArchetype("SoundBallPause", AEVec3(mRightBallTr->mLocal.mTranslationZ.x, mRightBallTr->mLocal.mTranslationZ.y, 410.0f));
			musicBallObj = aexScene->mSpaces.front()->InstantiateArchetype("MusicBallPause", AEVec3(mRightBallTr->mLocal.mTranslationZ.x, mRightBallTr->mLocal.mTranslationZ.y - 1.0f, 410.0f));

			if (soundBallObj && musicBallObj)
			{
				mSoundBallTr = soundBallObj->GetComp<TransformComp>();
				mMusicBallTr = musicBallObj->GetComp<TransformComp>();
				mSoundBallSprite = soundBallObj->GetComp<SpriteComp>();
				mMusicBallSprite = musicBallObj->GetComp<SpriteComp>();
			}

			if (optionsObj)
				mOptionsSprite = optionsObj->GetComp<SpriteComp>();

			if (volumeOnOffObj)
				mOnOffVolumeSprite = volumeOnOffObj->GetComp<SpriteComp>();

			if (resolutionsObj)
				mResolutionsSprite = resolutionsObj->GetComp<SpriteComp>();

			if (vsyncOnOffObj)
				mOnOffVSyncSprite = vsyncOnOffObj->GetComp<SpriteComp>();

			if (fullscreenOnOffObj)
				mOnOffFullScreenSprite = fullscreenOnOffObj->GetComp<SpriteComp>();
		}
	}


	void PauseMenuLogic::FromJson(json & value)
	{
		this->PausedLogicComp::FromJson(value);
	}
	void PauseMenuLogic::ToJson(json & value)
	{
		this->PausedLogicComp::ToJson(value);
	}


	// Prefix increment
	PauseMenuLogic::ButtonSelected & operator++(PauseMenuLogic::ButtonSelected & option)
	{
		if ((int)option == NUMBER_OF_BUTTONS - 1)
		{
			option = PauseMenuLogic::ButtonSelected(0);
			audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/Menu/MainMenu/Options/OptionTraversal/Typewriter letter key long 01 rode.wav"), Voice::SFX);
			return option;
		}

		option = PauseMenuLogic::ButtonSelected((int)option + 1);
		audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/Menu/MainMenu/Options/OptionTraversal/Typewriter letter key long 01 rode.wav"), Voice::SFX);
		return option;
	}

	// Postfix increment
	PauseMenuLogic::ButtonSelected operator++(PauseMenuLogic::ButtonSelected & option, int)
	{
		PauseMenuLogic::ButtonSelected copy = option;
		++option;
		return copy;
	}

	// Prefix decrement
	PauseMenuLogic::ButtonSelected & operator--(PauseMenuLogic::ButtonSelected & option)
	{
		if ((int)option == 0)
		{
			option = PauseMenuLogic::ButtonSelected(NUMBER_OF_BUTTONS - 1);
			audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/Menu/MainMenu/Options/OptionTraversal/Typewriter letter key long 01 rode.wav"), Voice::SFX);
			return option;
		}

		option = PauseMenuLogic::ButtonSelected((int)option - 1);
		audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/Menu/MainMenu/Options/OptionTraversal/Typewriter letter key long 01 rode.wav"), Voice::SFX);
		return option;
	}

	// Postfix decrement
	PauseMenuLogic::ButtonSelected operator--(PauseMenuLogic::ButtonSelected & option, int)
	{
		PauseMenuLogic::ButtonSelected copy = option;
		--option;
		return copy;
	}



	// Prefix increment
	PauseMenuLogic::OptionSelected & operator++(PauseMenuLogic::OptionSelected & option)
	{
		if ((int)option == NUMBER_OF_OPTIONS_PAUSE - 1)
		{
			option = PauseMenuLogic::OptionSelected(0);
			audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/Menu/MainMenu/Options/OptionTraversal/Typewriter letter key long 01 rode.wav"), Voice::SFX);
			return option;
		}

		option = PauseMenuLogic::OptionSelected((int)option + 1);
		audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/Menu/MainMenu/Options/OptionTraversal/Typewriter letter key long 01 rode.wav"), Voice::SFX);
		return option;
	}

	// Postfix increment
	PauseMenuLogic::OptionSelected operator++(PauseMenuLogic::OptionSelected & option, int)
	{
		PauseMenuLogic::OptionSelected copy = option;
		++option;
		return copy;
	}

	// Prefix decrement
	PauseMenuLogic::OptionSelected & operator--(PauseMenuLogic::OptionSelected & option)
	{
		if ((int)option == 0)
		{
			option = PauseMenuLogic::OptionSelected(NUMBER_OF_OPTIONS_PAUSE - 1);
			audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/Menu/MainMenu/Options/OptionTraversal/Typewriter letter key long 01 rode.wav"), Voice::SFX);
			return option;
		}

		option = PauseMenuLogic::OptionSelected((int)option - 1);
		audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/Menu/MainMenu/Options/OptionTraversal/Typewriter letter key long 01 rode.wav"), Voice::SFX);
		return option;
	}

	// Postfix decrement
	PauseMenuLogic::OptionSelected operator--(PauseMenuLogic::OptionSelected & option, int)
	{
		PauseMenuLogic::OptionSelected copy = option;
		--option;
		return copy;
	}
}