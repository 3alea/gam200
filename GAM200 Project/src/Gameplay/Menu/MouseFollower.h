#pragma once

#include "Components/Logic.h"


namespace AEX
{
	class TransformComp;
	class BoxCollider;

	class MouseFollower : public LogicComp
	{
		AEX_RTTI_DECL(MouseFollower, LogicComp);

	public:

		void Initialize();
		void Update();
		void Shutdown();

		bool OnGui();

		void FromJson(json & value);
		void ToJson(json & value);

		TransformComp * tr = nullptr;
		BoxCollider * collider = nullptr;
	};
}