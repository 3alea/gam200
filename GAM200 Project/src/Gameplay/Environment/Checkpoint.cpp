#include "Composition\AEXGameObject.h"
#include "Composition/AEXScene.h"
#include "Components\SpriteComps.h"
#include "Checkpoint.h"
#include "Editor\Editor.h"

using namespace AEX;
using namespace ImGui;

void Checkpoint::Initialize()
{
	aexScene->bFadeTransition = true;
}

void Checkpoint::Update()
{
	GameObject* fadeout = aexScene->FindObjectInANYSpace("fadeout");
	if (fadeout && aexScene->bFadeTransition == true)
	{
		fadeout->GetComp<SpriteComp>()->mColor[3] = 1.f;
		aexScene->bFadeTransition = false;
	}
}

void Checkpoint::OnCollisionEvent(const CollisionEvent& collision)
{
	if (collision.otherObject->GetBaseNameString() == "player")
	{
		// Transform storing
		AEVec3 t = mOwner->GetComp<TransformComp>()->mLocal.mTranslationZ;

		mOwner->GetParentSpace()->DestroyObject(mOwner);
		Editor->SaveToLevel(t.x, t.y);
	}
}

bool Checkpoint::OnGui()
{
	return false;
}

// FromJson specialized for this component
void Checkpoint::FromJson(json & value)
{
	this->IComp::FromJson(value);
}

// ToJson specialized for this component
void Checkpoint::ToJson(json & value)
{
	this->IComp::ToJson(value);
}