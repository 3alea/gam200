#include "SmasherLogic.h"
#include "Components/AEXTransformComp.h"
#include "Imgui\imgui.h"
#include "Components/Rigidbody.h"
#include "AI/Easing.h"
#include "Components/AEXTransformComp.h"
#include "Composition/AEXGameObject.h"
#include "Platform/AEXTime.h"
#include "Player/Stats/PlayerStats.h"
#include "CameraLogic/CameraShaker.h"
#include "Resources/AEXResourceManager.h"
#include "Audio/AudioManager.h"

namespace AEX
{
	Smasher::Smasher()
		: SMASHING(true)
		, rising(false)
		, DownTime(1)
		, UpTime(1)
		, mSmashRange(1)
		, smasherT(NULL)
		, WaitDownTime(0)
		, WaitUpTime(0)
		, isUp(false)
	{}

	void Smasher::Initialize()
	{
		timer = 0;
		SMASHING = true;
		waiting = false;
		rising = false;

		player = nullptr;
		shaker = nullptr;

		if (GameObject* cam = mOwner->GetParentSpace()->FindObject("camera"))
		{
			shaker = cam->GetComp<CameraShaker>();
		}

		if (GameObject* p = mOwner->GetParentSpace()->FindObject("player"))
		{
			player = p->GetComp<TransformComp>();
		}

		if (smasherT == NULL)
			smasherT = mOwner->GetComp<TransformComp>();

		if (smasherT)
		{
			smasherIdlePos = smasherT->mLocal.mTranslationZ.y;
			smasherDownPos = smasherIdlePos - mSmashRange;

			///////////////////SMASHING/////////////////////
			SMASHINGPath.Clear();
			SMASHINGPath.Push(0, AEVec2(0, smasherIdlePos));
			SMASHINGPath.Push(1, AEVec2(0, smasherDownPos));
			///////////////////SMASHING/////////////////////

			///////////////////RISING/////////////////////
			risingPath.Clear();
			risingPath.Push(0, AEVec2(0, smasherDownPos));
			risingPath.Push(1, AEVec2(0, smasherIdlePos));
			///////////////////RISING/////////////////////
		}
	}
	
	void Smasher::Update()
	{
		if (!smasherT || !player || !shaker)
		{
			Initialize();
			return;
		}

		/////////////////WAITING///////////////////
		if (waiting)
		{
			timer += (f32)aexTime->GetFrameTime();

			if (timer > timeToWait)
			{
				timer = 0;
				waiting = false;

				if (isUp)
					SMASHING = true;
				else
					rising = true;
			}
			return; // End of "state"
		}
		/////////////////WAITING///////////////////

		////////////////SMASHING///////////////////
		if (SMASHING)
		{
			easedTime = EaseInQuad(timer / DownTime);
			smasherT->mLocal.mTranslationZ.y = SMASHINGPath.SampleTime(easedTime).y;
			timer += (f32)aexTime->GetFrameTime();

			if (timer > DownTime)
			{
				smasherT->mLocal.mTranslationZ.y = smasherDownPos;
				timer = 0;
				timeToWait = WaitDownTime;
				waiting = true;
				SMASHING = false;
				isUp = false;

				float dx = std::abs(smasherT->mLocal.mTranslationZ.x - player->mLocal.mTranslationZ.x);
				if (dx <= 20.0f)
				{
					// Play sound
					audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\Stomach1\\ImpactSmasher.wav"), Voice::SFX);

					// Camera shake
					shaker->mTrauma = 1.0f;
				}
			}

			return; // End of "state"
		}
		/////////////////SMASHING///////////////////

		//////////////////RISING////////////////////
		if (rising)
		{
			easedTime = EaseInOutQuad(timer / UpTime);
			smasherT->mLocal.mTranslationZ.y = risingPath.SampleTime(easedTime).y;
			timer += (f32)aexTime->GetFrameTime();

			if (timer > UpTime)
			{
				smasherT->mLocal.mTranslationZ.y = smasherIdlePos;
				timer = 0;
				timeToWait = WaitUpTime;
				waiting = true;
				rising = false;
				isUp = true;
			}

			return; // End of "state"
		}
		//////////////////RISING////////////////////
	}
	
	void Smasher::OnCollisionStarted(const CollisionStarted & collision)
	{
		// Time to sink
		if (collision.otherObject->GetBaseNameString() == "player")
		{
			// kill the player?
			collision.otherObject->GetComp<PlayerStats>()->lives = 0;
		}
	}
	
	bool Smasher::OnGui()
	{
		bool changed = false;

		ImGui::Text("Smashing Range");
		if (ImGui::InputFloat("Smashing Range", &mSmashRange))
		{
			ChangeSmashPath();
			changed = true;
		}
		ImGui::Text("Time to go down");
		if (ImGui::InputFloat("DOWN", &DownTime))
		{
			ChangeSmashPath();
			changed = true;
		}
		ImGui::Text("Time to stay down");
		if (ImGui::InputFloat("stay down", &WaitDownTime))
		{
			changed = true;
		}
		ImGui::Text("Time to go up");
		if (ImGui::InputFloat("UP", &UpTime))
		{
			changed = true;
		}
		ImGui::Text("Time to stay up");
		if (ImGui::InputFloat("stay up", &WaitUpTime))
		{
			changed = true;
		}
	
		return changed;
	}

	void Smasher::ChangeSmashPath()
	{
		Initialize();
	}
	void Smasher::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);
		value["SMASHER range"]     >> mSmashRange;

		value["Time to go DOWN"]   >> DownTime;
		value["Time to fo UP"]     >> UpTime;

		value["Time to wait up"]   >> WaitUpTime;
		value["Time to wait down"] >> WaitDownTime;
	}
	void Smasher::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);
		value["SMASHER range"]     << mSmashRange;

		value["Time to go DOWN"]   << DownTime;
		value["Time to fo UP"]     << UpTime;

		value["Time to wait up"]   << WaitUpTime;
		value["Time to wait down"] << WaitDownTime;
	}
}
