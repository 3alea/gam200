#pragma once
#include "Components\Logic.h"
#include "AI/Path2D.h"

class SpineAnimationComp;

namespace AEX
{
	class Rigidbody;
	class TransformComp;
	class CameraShaker;

	class Smasher : public LogicComp//, public Actor
	{
	    AEX_RTTI_DECL(Smasher, LogicComp);
		
		public:
			Smasher();

			TransformComp* player;

			CameraShaker* shaker;

			virtual void Initialize();
			virtual void Update();
			virtual void OnCollisionStarted(const CollisionStarted& collision);
			virtual bool OnGui();
			void ChangeSmashPath();
			
			//void SM_Init();
			
			// FromJson specialized for this component
			virtual void FromJson(json & value);
			// ToJson specialized for this component
			virtual void ToJson(json & value);

			TransformComp* smasherT;
			float smasherIdlePos;
			float smasherDownPos;
			float timer;
			float easedTime;
			float timeToWait;
			bool isUp;

			///////////STATES OF THE SMASHER/////////
			bool waiting;
			bool rising;
			bool SMASHING;
			///////////STATES OF THE SMASHER/////////

			////////////FOR THE EDITOR//////////////
			float mSmashRange;
			float UpTime;
			float DownTime;
			float WaitUpTime;
			float WaitDownTime;
			////////////FOR THE EDITOR//////////////


			Path2D risingPath;
			Path2D SMASHINGPath;
			// This object's animation comp
			SpineAnimationComp* spineAnimation;
	};
}
