#include "PlatformOnAboveActivator.h"
#include "Composition/AEXGameObject.h"
#include "Composition/AEXScene.h"

void PlatformOnAboveActivator::Initialize()
{
	tr = mOwner->GetComp<TransformComp>();
	col = mOwner->GetComp<BoxCollider>();
	GameObject* foot = aexScene->FindObjectInANYSpace("foot");

	if (foot)
		footTr = foot->GetComp<TransformComp>();
}

void PlatformOnAboveActivator::Update()
{
	if (!tr || !col || !footTr)
	{
		Initialize();
		
		if (!tr || !col || !footTr)
			return;
	}

	float val1 = tr->mLocal.mTranslationZ.y + tr->mLocal.mScale.y / 2.f;
	float val2 = footTr->mLocal.mTranslationZ.y + 10.f * footTr->mLocal.mScale.y;

	if (val1 < val2)
		col->mIsGhost = false;
	else
		col->mIsGhost = true;
}

void PlatformOnAboveActivator::Shutdown()
{

}


void PlatformOnAboveActivator::FromJson(json & value)
{
	this->LogicComp::FromJson(value);
}

void PlatformOnAboveActivator::ToJson(json & value)
{
	this->LogicComp::ToJson(value);
}
