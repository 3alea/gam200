#pragma once
#include "Components\Logic.h"
#include "Cholesterol\BlockedCar.h"

using namespace AEX;

struct BulletLogic;

struct CholesterolLogic : public LogicComp
{
	AEX_RTTI_DECL(CholesterolLogic, LogicComp);

	CholesterolLogic() { mTotalLives = 20; }
	void Initialize();
	void Update();

	virtual void OnCollisionEvent(const CollisionEvent& collision);

	void FromJson(json & value);
	void ToJson(json & value);

	bool OnGui();

	s32 mTotalLives;
	s32 mLives;

	// if (bala colisiona con colesterol)
		//mOwner->GetComp<SpriteComp>()->mTex = aexResourceMgr->getResource("data\\Textures\\")

	AEX::TResource<Texture> * mFirstTex = nullptr;
	AEX::TResource<Texture> * mSecondTex = nullptr;
	AEX::TResource<Texture> * mThirdTex = nullptr;
	AEX::TResource<Texture> * mFourthTex = nullptr;

	bool mActivateCars = false;
	char mCarName1[10];
	char mCarName2[10];
	char mCarName3[10];
	char mCarName4[10];
	char mCarName5[10];

	f32 mParticleSpawnInterval = 0.25f;

	s32 mToSecondPercent = 80;
	s32 mToThirdPercent = 60;
	s32 mToFourthPercent = 40;
	s32 mToFifthPercent = 20;

private:
	f32 mParticleCurrentTime = 0.0f;
};