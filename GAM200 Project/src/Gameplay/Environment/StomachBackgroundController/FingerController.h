#pragma once
#include "./Components/Logic.h"
#include "Components/AEXTransformComp.h"
#include "../FloatingPlatform/FloatingPlatformLogic.h"
using namespace AEX;

// Forward declare to use
class AcidController;

class FingerController : public LogicComp
{
	AEX_RTTI_DECL(FingerController, LogicComp)

public:
	virtual void Initialize();
	virtual void Update();

	// Acid reference
	AcidController* acid;
	// Transform reference
	FloatingPlatform* t;

	virtual bool OnGui();
	// FromJson specialized for this component
	virtual void FromJson(json & value);
	// ToJson specialized for this component
	virtual void ToJson(json & value);
private:
	f32 mSpeed;
};