#pragma once
#include "./Components/Logic.h"
using namespace AEX;

// Forward declare to use
class AcidController;
class SpineAnimationComp;

class BGController : public LogicComp
{
	AEX_RTTI_DECL(BGController, LogicComp)

public:
	virtual void Initialize();
	virtual void Update();

	// Acid reference
	AcidController* acid;
	// Spine animation reference
	SpineAnimationComp* spineAnimation;

	virtual bool OnGui();
	// FromJson specialized for this component
	virtual void FromJson(json & value);
	// ToJson specialized for this component
	virtual void ToJson(json & value);
private:
	f32 mSpeed;
};