#include "BGController.h"
#include "Composition/AEXScene.h"
#include "Components/SpineAnimation.h"
#include "../AcidController.h"

void BGController::Initialize()
{
	acid = aexScene->FindObjectInANYSpace("AcidHazard")->GetComp<AcidController>();
	spineAnimation = mOwner->GetComp<SpineAnimationComp>();
}

void BGController::Update()
{
	spineAnimation->ChangeAnimation(0, "animation2", false, mSpeed);
	if (acid->myState == AcidController::State::eRising)
	{
		spineAnimation->playBackwards = false;
		spineAnimation->SetPauseState(false);
	}
	else if (acid->myState == AcidController::State::eFalling)
	{
		spineAnimation->playBackwards = true;
		spineAnimation->SetPauseState(false);
	}
	else
		spineAnimation->SetPauseState(true);
}

bool BGController::OnGui()
{
	bool changed = false;

	if (ImGui::InputFloat("Animation speed", &mSpeed)) changed = true;

	return changed;
}

void BGController::FromJson(json & value)
{
	LogicComp::FromJson(value);

	value["AnimationSpeed"] >> mSpeed;
}

void BGController::ToJson(json & value)
{
	LogicComp::ToJson(value);

	value["AnimationSpeed"] << mSpeed;
}
