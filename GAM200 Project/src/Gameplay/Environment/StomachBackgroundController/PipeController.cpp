#include "PipeController.h"
#include "Composition/AEXScene.h"
#include "Platform/AEXTime.h"
#include "Components/SpineAnimation.h"
#include <spine/AnimationState.h>
#include "../AcidController.h"

void PipeController::Initialize()
{
	acid = aexScene->FindObjectInANYSpace("AcidHazard")->GetComp<AcidController>();
	spineAnimation = mOwner->GetComp<SpineAnimationComp>();
	spineAnimation->SetSkeletonSize(AEVec2(evenSmaller / 10.f, evenSmaller / 10.f));
	Initialized = false;
	switch (num)
	{
	case 2:
		spineAnimation->ChangeAnimation(0, "animation2", false, mSpeed);
		break;
	case 3:
		spineAnimation->ChangeAnimation(0, "animation3", false, mSpeed);
		break;
	case 0:
		spineAnimation->ChangeAnimation(0, "animation", false, mSpeed);
		break;
	}
	spineAnimation->Update();
	spineAnimation->SetPauseState(true);
}

void PipeController::Update()
{
	if (!acid)
		acid = aexScene->FindObjectInANYSpace("AcidHazard")->GetComp<AcidController>();

	switch (num)
	{
	case 2:
		spineAnimation->ChangeAnimation(0, "animation2", false, mSpeed);
		break;
	case 3:
		if (acid->myState == AcidController::State::eRising)
		{
			if (spineAnimation->GetAnimState()->getTracks()[0]->getTrackTime() + (f32)aexTime->GetFrameTime() < spineAnimation->GetAnimState()->getTracks()[0]->getAnimationEnd())
			{
				spineAnimation->SetPauseState(false);
				spineAnimation->ChangeAnimation(0, "animation3", false, mSpeed);
			}
			else
				spineAnimation->SetPauseState(true);
		}
		else if (acid->myState == AcidController::State::eFalling)
		{
			if (spineAnimation->GetAnimState()->getTracks()[0]->getTrackTime() - (f32)aexTime->GetFrameTime() > 0.f)
			{
				spineAnimation->SetPauseState(false);
				spineAnimation->ChangeAnimation(0, "animation3", false, mSpeed);
			}
			else
				spineAnimation->SetPauseState(true);
		}
		break;
	case 0:
		spineAnimation->ChangeAnimation(0, "animation", false, mSpeed);
		break;
	}

	if (acid->myState == AcidController::State::eRising)
	{
		if (!Initialized)
		{
			spineAnimation->GetAnimState()->getTracks()[0]->setTrackTime(spineAnimation->startTime);
			Initialized = true;
		}
		spineAnimation->playBackwards = false;
		spineAnimation->SetPauseState(false);
	}
	else if (acid->myState == AcidController::State::eFalling)
	{
		if (!Initialized)
		{
			spineAnimation->GetAnimState()->getTracks()[0]->setTrackTime(spineAnimation->startTime);
		}
		spineAnimation->playBackwards = true;
		spineAnimation->SetPauseState(false);
		
	}
	else
	{
		spineAnimation->SetPauseState(true);
		Initialized = false;
	}
	spineAnimation->SetSkeletonSize(AEVec2(evenSmaller / 10.f, evenSmaller / 10.f));
}

bool PipeController::OnGui()
{
	bool changed = false;

	if (ImGui::InputFloat("Rising speed", &mSpeed)) changed = true;

	if (ImGui::InputFloat("Even smaller size", &evenSmaller)) changed = true;

	if (ImGui::InputInt("Animation number", &num)) changed = true;

	return changed;
}

void PipeController::FromJson(json & value)
{
	LogicComp::FromJson(value);

	value["RisingSpeed"] >> mSpeed;
	value["EvenSmaller"] >> evenSmaller;
	value["Num"] >> num;
}

void PipeController::ToJson(json & value)
{
	LogicComp::ToJson(value);

	value["RisingSpeed"] << mSpeed;
	value["EvenSmaller"] << evenSmaller;
	value["Num"] << num;
}
