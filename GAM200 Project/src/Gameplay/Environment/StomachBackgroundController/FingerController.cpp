#include "FingerController.h"
#include "Composition/AEXScene.h"
#include "../AcidController.h"

void FingerController::Initialize()
{
	acid = aexScene->FindObjectInANYSpace("AcidHazard")->GetComp<AcidController>();
	t = mOwner->GetComp<FloatingPlatform>();
}

void FingerController::Update()
{
	if (!acid)
		Initialize();
	if (acid->myState == AcidController::State::eRising)
	{
		t->platformIdlePos += mSpeed;
		t->RestartData();
	}
	else if (acid->myState == AcidController::State::eFalling)
	{
		t->platformIdlePos -= mSpeed;
		t->RestartData();
	}
		
}

bool FingerController::OnGui()
{
	bool changed = false;

	if (ImGui::InputFloat("Rising speed", &mSpeed)) changed = true;

	return changed;
}

void FingerController::FromJson(json & value)
{
	LogicComp::FromJson(value);

	value["RisingSpeed"] >> mSpeed;
}

void FingerController::ToJson(json & value)
{
	LogicComp::ToJson(value);

	value["RisingSpeed"] << mSpeed;
}
