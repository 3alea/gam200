#pragma once
#include "./Components/Logic.h"
using namespace AEX;

// Forward declare to use
class AcidController;
class SpineAnimationComp;

class PipeController : public LogicComp
{
	AEX_RTTI_DECL(PipeController, LogicComp)

public:
	virtual void Initialize();
	virtual void Update();

	// Acid reference
	AcidController* acid;
	// Animation reference
	SpineAnimationComp* spineAnimation;

	virtual bool OnGui();
	// FromJson specialized for this component
	virtual void FromJson(json & value);
	// ToJson specialized for this component
	virtual void ToJson(json & value);

	// Animation chooser
	int num = 0;
	// Even smaller size to adjust correctly
	float evenSmaller = 0.010f;	
	// Initialized bool
	bool Initialized = false;
private:
	f32 mSpeed;
};