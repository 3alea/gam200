#pragma once
#include "Composition\AEXGameObject.h"
#include "Components/Rigidbody.h"
#include "Components\Logic.h"
#include "Imgui\imgui.h"

using namespace AEX;

class AcidController : public LogicComp
{
	AEX_RTTI_DECL(AcidController, LogicComp);
public:
	// Transform of this object
	TransformComp* t;

	// State (falling/rising)
	enum struct State{eFalling, eRising, eResting};

	// Acid's state
	State myState = State::eResting;

	// Min height
	float minHeight = 0.f;

	// Max height
	float maxHeight = 0.f;

	// Rise speed
	float riseSpeed = 2.f;

	// Amount of distance acid travels upwards/downwards
	float distance = 2.f;

	// Wave speed
	float waveSpeed = 2.f;

	// Angle to make the acid go up/down
	float angle = 0.f;

	// Start is called before the first frame update
	virtual void Initialize();

	// Update is called once per frame
	virtual void Update();

	// Rising logic
	void Rise();

	// Falling logic
	void Fall();

	// Collision - if touches player, kill him
	void OnCollisionEvent(const CollisionEvent & collision);

	// OnGui() function
	virtual bool OnGui();

	// FromJson specialized for this component
	virtual void FromJson(json & value);
	// ToJson specialized for this component
	virtual void ToJson(json & value);
};