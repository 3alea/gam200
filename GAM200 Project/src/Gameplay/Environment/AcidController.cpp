#include "Composition/AEXScene.h"
#include "Platform/AEXTime.h"
#include "AcidController.h"
#include "Player\Stats\PlayerStats.h"
#include <iostream>

using namespace AEX;
using namespace ImGui;

void AcidController::Initialize()
{
	t = GetOwner()->GetComp<TransformComp>();
}

void AcidController::Update()
{
	if (myState == State::eFalling)
	{
		Fall();
	}

	if (myState == State::eRising)
	{
		Rise();
	}

	// Update position of the acid
	angle += (f32)aexTime->GetFrameTime();

	// Update position
	t->mWorld.mTranslationZ.y += sin(angle * waveSpeed) * distance;
}

void AcidController::Rise()
{
	if (t->mLocal.mTranslationZ.y >= maxHeight)
	{
		//std::cout << "Height is: " << t->mLocal.mTranslationZ.y << std::endl;
		myState = State::eResting;
		return;
	}
	t->mLocal.mTranslationZ.y += riseSpeed;
}

void AcidController::Fall()
{
	if (t->mLocal.mTranslationZ.y <= minHeight)
	{
		//std::cout << "Height is: " << t->mLocal.mTranslationZ.y << std::endl;
		myState = State::eResting;
		return;
	}
	t->mLocal.mTranslationZ.y -= riseSpeed;
}

void AcidController::OnCollisionEvent(const CollisionEvent & collision)
{
	if (PlayerStats* stats = collision.otherObject->GetComp<PlayerStats>())
	{
		stats->lives = 0;
	}
}

bool AcidController::OnGui()
{
	// isChanged boolean to return if something has changed
	bool isChanged = false;

	// Collapsing header - when clicked, opens up to show the rest of edited variables
	if (ImGui::CollapsingHeader("AcidController"))
	{
		// Wave amount
		ImGui::Text("Wave amount");
		// Allow user to input wave height from the editor
		if (ImGui::InputFloat("Wave amount", &distance))
			isChanged = true;

		// Speed controller
		ImGui::Text("Wave speed");
		if (ImGui::InputFloat("Wave speed", &waveSpeed))
			isChanged = true;

		// Min height
		ImGui::Text("Min height");
		if (ImGui::InputFloat("Min height", &minHeight))
			isChanged = true;

		// Max height
		ImGui::Text("Max height");
		if (ImGui::InputFloat("Max height", &maxHeight))
			isChanged = true;

		// Rise speed
		ImGui::Text("Rise speed when the acid rises: ");
		if (ImGui::InputFloat("Rise speed", &riseSpeed))
			isChanged = true;
	}

	return isChanged;
}

// FromJson specialized for this component
void AcidController::FromJson(json & value)
{
	this->IComp::FromJson(value);

	value["Wave amount"] >> distance;
	value["Wave speed"] >> waveSpeed;
	value["Rise speed"] >> riseSpeed;
	value["Max height"] >> maxHeight;
	value["Min height"] >> minHeight;
}

// ToJson specialized for this component
void AcidController::ToJson(json & value)
{
	this->IComp::ToJson(value);

	value["Wave amount"] << distance;
	value["Wave speed"] << waveSpeed;
	value["Rise speed"] << riseSpeed;
	value["Max height"] << maxHeight;
	value["Min height"] << minHeight;
}