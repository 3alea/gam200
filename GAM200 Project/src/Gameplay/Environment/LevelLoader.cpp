#include "Composition\AEXGameObject.h"
#include "Composition/AEXScene.h"
#include "LevelLoader.h"
#include "Editor\Editor.h"
#include "Audio/AudioManager.h"

using namespace AEX;
using namespace ImGui;

void LevelLoader::Initialize()
{
	mCollidedWithPlayer = false;
	player = nullptr;
	main = nullptr;
	foreground = nullptr;
	tr = nullptr;
	sprite = nullptr;
}

void LevelLoader::Update()
{
	if (mCollidedWithPlayer && mUseFadeInOut)
	{
		if (sprite->mColor[3] >= 1.0f)
		{
			sprite->mOwner->GetParentSpace()->DestroyObject(sprite->mOwner);
			mCollidedWithPlayer = false;
			Scene::bFadeTransition = mUseFadeInOut;
			std::string levelToLoad = "data/Levels/";
			levelToLoad += levelName;
			levelToLoad += ".json";
			aexScene->ChangeLevel(levelToLoad);
		}

		sprite->mColor[3] += (f32)aexTime->GetFrameTime() * 0.25f;
	}
}

void LevelLoader::OnCollisionStarted(const CollisionStarted& collision)
{
	if (collision.otherObject->GetBaseNameString() == "player" && mCollidedWithPlayer == false)
	{
		if (mUseFadeInOut)
		{
			mCollidedWithPlayer = true;
			player = collision.otherObject;

			main = aexScene->mSpaces.front();
			GameObject * foreground = aexScene->FindObjectInANYSpace("foreground exit");

			if (foreground == nullptr)
			{
				foreground = main->AddObject("foreground exit");
			}

			tr = new TransformComp;
			sprite = new SpriteComp;
			//follow = new FollowAnObject;

			foreground->AddComp(tr);
			foreground->AddComp(sprite);

			TransformComp * playerTr = player->GetComp<TransformComp>();

			tr->mLocal.mTranslationZ = playerTr->mLocal.mTranslationZ;
			tr->mLocal.mTranslationZ.z = 400.0f;
			tr->mLocal.mScale.x = 400.0f;
			tr->mLocal.mScale.y = 200.0f;

			sprite->mColor[0] = 0.0f;
			sprite->mColor[1] = 0.0f;
			sprite->mColor[2] = 0.0f;
			sprite->mColor[3] = 0.0f;
		}
		else if (mCollidedWithPlayer == false)
		{
			mCollidedWithPlayer = true;
			Editor->levelToLoadName = levelName;
			Editor->loadFromCertainLevel = true;
		}
	}
}

bool LevelLoader::OnGui()
{
	bool changed = false;

	if (ImGui::InputText("Level to load", level, IM_ARRAYSIZE(level)))
	{
		levelName = level;
		changed = true;
	}
	if (ImGui::Checkbox("Use fade in/out", &mUseFadeInOut))
	{
		changed = true;
	}

	return changed;
}

// FromJson specialized for this component
void LevelLoader::FromJson(json & value)
{
	this->LogicComp::FromJson(value);

	levelName = value["Level Name"].get<std::string>();
	strcpy_s(level, levelName.c_str());

	value["Use fade in/out"] >> mUseFadeInOut;
}

// ToJson specialized for this component
void LevelLoader::ToJson(json & value)
{
	this->LogicComp::ToJson(value);

	value["Level Name"] = levelName;
	value["Use fade in/out"] << mUseFadeInOut;
}