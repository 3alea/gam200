#pragma once
#include "./Components/Logic.h"
#include "Components/AEXTransformComp.h"
using namespace AEX;

class RisingAir : public LogicComp
{
	AEX_RTTI_DECL(RisingAir, LogicComp)

	virtual void Initialize();
	virtual void Update();
	virtual void ShutDown();

	virtual bool OnGui();
	// FromJson specialized for this component
	virtual void FromJson(json & value);
	//// ToJson specialized for this component
	virtual void ToJson(json & value);
	virtual void OnCollisionEvent(const CollisionEvent & collision);
	virtual void OnCollisionEnded(const CollisionEnded& collision);
	

	TransformComp * ownerTransform;
	TransformComp * PlayerTransform;
	f32 minDistance;
	f32 Distance;
	AEVec2 air_direction;
	f32 air_force;
	f32 air_force_small;
	f32 timer;
	bool ignoreforce = false;
};