#include "RisingAir.h"
#include "src/Engine/Composition/AEXScene.h"
#include "src/Engine/Math/Collisions.h"
#include "src\Engine\Components\Rigidbody.h"
#include "src/Engine/Imgui/imgui.h"
#include "src/Engine/Platform/AEXTime.h"

void RisingAir::Initialize()
{
	ownerTransform = mOwner->GetComp<TransformComp>();
	Distance = 25;
	minDistance = 10;
	air_direction = AEVec2(0.0f, 1.0f);
	air_direction.NormalizeThis();
	//air_force = 8.0f;
	timer = 0.0f;
}

void RisingAir::Update()
{
	ignoreforce = false;

	if (timer > 4.0f && timer < 4.1f)
	{
		ignoreforce = true;
	}
	if (timer > 4.1f)
		timer = 0.0f;

	PlayerTransform = aexScene->FindObjectInANYSpace("player")->GetComp<TransformComp>();
}

void RisingAir::ShutDown()
{
}

bool RisingAir::OnGui()
{
	ImGui::InputFloat("Air force Big", &air_force);
	ImGui::InputFloat("Air force Small", &air_force_small);
	return false;
}


// FromJson specialized for this component
void RisingAir::FromJson(json & value)
{
	this->LogicComp::FromJson(value);

	value["airForceBig"] >> air_force;
	value["airForceSmall"] >> air_force_small;
}
//// ToJson specialized for this component
void RisingAir::ToJson(json & value)
{
	this->LogicComp::ToJson(value);

	value["airForceBig"] << air_force;
	value["airForceSmall"] << air_force_small;
}

void RisingAir::OnCollisionEvent(const CollisionEvent & collision)
{
	if (!strcmp(collision.otherObject->GetBaseName(), "player"))
	{
		timer += (f32)aexTime->GetFrameTime();

		aexScene->FindObjectInANYSpace("player")->GetComp<Rigidbody>()->AddForce(air_direction * air_force);
		aexScene->FindObjectInANYSpace("player")->GetComp<Rigidbody>()->mInvMass = 11.0f;

		if (!ignoreforce)
		{
			if (PlayerTransform->GetPosition3D().y > ownerTransform->GetPosition3D().y + ownerTransform->GetScale().y / 3.5f)
			{
				air_force = air_force;
			}
			else
				air_force = air_force;
		}
		else
			air_force = air_force_small;
	}
}

void RisingAir::OnCollisionEnded(const CollisionEnded & collision)
{
	if (!strcmp(collision.otherObject->GetBaseName(), "player"))
	{
		aexScene->FindObjectInANYSpace("player")->GetComp<Rigidbody>()->mInvMass = 1.0f; //reset the inv mass
	}
}

