#include "MovingBackgroundActivator.h"
#include "Imgui/imgui.h"
#include "Composition/AEXScene.h"
#include "BackgroundMover.h"
#include "Player/Stats/PlayerStats.h"
#include "Platform/AEXTime.h"
#include "AI/Easing.h"
#include "Components/AEXTransformComp.h"
#include "Components/Collider.h"
#include "Activator/KillerCarsActivator.h"
namespace AEX
{
	void MovingBackgroundActivator::Initialize()
	{
		if (GameObject* p = mOwner->GetParentSpace()->FindObject("player"))
		{
			stats = p->GetComp<PlayerStats>();
		}
		b1 = nullptr;
		b2 = nullptr;
		cam = nullptr;
		firstUpdate = true;
		if (GameObject* blackbar1 = mOwner->GetParentSpace()->FindObject("blackBar1"))
		{
			b1 = blackbar1->GetComp<TransformComp>();
		}
		if (GameObject* blackbar2 = mOwner->GetParentSpace()->FindObject("blackBar2"))
		{
			b2 = blackbar2->GetComp<TransformComp>();
		}
		if (GameObject* c = mOwner->GetParentSpace()->FindObject("cinematicCamera"))
		{
			cam = c->GetComp<TransformComp>();
		}
		if (GameObject* block = mOwner->GetParentSpace()->FindObject("rightBlock"))
		{
			righBlock = block->GetComp<ICollider>();
		}
		if (GameObject* floor = mOwner->GetParentSpace()->FindObject("lava"))
		{
			movingFloor = floor->GetComp<ICollider>();
		}
		carActivator = nullptr;
		if (GameObject* ka = mOwner->GetParentSpace()->FindObject("kca"))
		{
			carActivator = ka->GetComp<KillerCarsActivator>();
		}

		timer = 0;
		cinematic = false;
		up = false;
		wait = false;
		down = false;
		//donOnceStart = false;
		doneOnce = false;
	}

	void MovingBackgroundActivator::Update()
	{
		if (!stats || !b1 || !b2 || !cam || !righBlock || !movingFloor || !carActivator)
		{
			Initialize();
			return;
		}

		if (firstUpdate)
		{
			b1->mLocal.mTranslationZ.y = cam->mLocal.mTranslationZ.y + 10;
			b2->mLocal.mTranslationZ.y = cam->mLocal.mTranslationZ.y - 10;

			firstUpdate = false;
		}
		
		if (cinematic)
		{
			b1->mLocal.mTranslationZ.x = cam->mLocal.mTranslationZ.x;
			b2->mLocal.mTranslationZ.x = cam->mLocal.mTranslationZ.x;

			// Start cinematic mode
			if (up)
			{
				if (timer <= 2)
				{
					timer += (f32)aexTime->GetFrameTime();
					float easedTime = EaseInOutQuad(timer / 2.f);
					// Lerp the black bars to a cinematic mode
					b1->mLocal.mTranslationZ.y = cam->mLocal.mTranslationZ.y + 7.5f + (cam->mLocal.mTranslationZ.y + 6.f - (cam->mLocal.mTranslationZ.y + 7.5f)) * easedTime;
					b2->mLocal.mTranslationZ.y = cam->mLocal.mTranslationZ.y - 7.5f + (cam->mLocal.mTranslationZ.y - 6.f - (cam->mLocal.mTranslationZ.y - 7.5f)) * easedTime;
				}
				else
				{
					up = false;
					wait = true;
					timer = 0;
				}
			}
			
			// Wait for the cinematic to end
			if (wait)
			{
				float max;

				if (mOwner->GetBaseNameString() == "platform")
				{
					max = 6.0f;

					if (carActivator->level2)
						max = 8.0f;
				}
				else
				{
					max = 4.0f;
					carActivator->active = true;
					movingFloor->collisionGroup = "none";
				}


				if (carActivator->level2)
					if (timer >= 6.0f)
						carActivator->active = true;

				if (timer <= max)
					timer += (f32)aexTime->GetFrameTime();
				else
				{
					wait = false;
					down = true;
					up = false;
					timer = 0;

					stats->StopCinematic();
				}
			}
			
			if (down)
			{
				if (timer <= 2)
				{

					timer += (f32)aexTime->GetFrameTime();
					float easedTime = EaseInOutQuad(timer / 2.f);

					// Lerp the black bars to a cinematic mode
					b1->mLocal.mTranslationZ.y = (cam->mLocal.mTranslationZ.y + 6.f) + (cam->mLocal.mTranslationZ.y + 7.5f - (cam->mLocal.mTranslationZ.y + 6.f)) * easedTime;
					b2->mLocal.mTranslationZ.y = (cam->mLocal.mTranslationZ.y - 6.f) + (cam->mLocal.mTranslationZ.y - 7.5f - (cam->mLocal.mTranslationZ.y - 6.f)) * easedTime;
				}
				else 
				{
					cinematic = false;

					doneOnce = true;

					timer = 0;
				}
			}
		}
	}

	void MovingBackgroundActivator::Shutdown()
	{

	}

	void MovingBackgroundActivator::OnCollisionStarted(const CollisionStarted& collision)
	{

		if (collision.otherObject->GetBaseNameString() == "foot")
		{

			GameObject * obj = aexScene->FindObjectInANYSpace(mBackgroundName);
			GameObject * obj2 = aexScene->FindObjectInANYSpace("overlay");


			if (obj == nullptr || obj2 == nullptr)
			{
				std::cout << "The background with name " << mBackgroundName << " could not be activated\n";
				return;
			}

			BackgroundMover * bgMover = obj->GetComp<BackgroundMover>();
			BackgroundMover * bgMover2 = obj2->GetComp<BackgroundMover>();


			if (bgMover == nullptr)
			{
				std::cout << "The obj with name " << mBackgroundName << " does not have a BackgroundMover component\n";
				return;
			}

			if (mIsStart == 0)
			{
				stats->onVeinsTransition = true;
				bgMover->AccelerateBackground();
				bgMover2->AccelerateBackground();
			}
			else if (mIsStart == 1)
			{
				bgMover2->DecelerateBackground();
				bgMover->DecelerateBackground();
			}
			else if (mIsStart == 2)
			{

				bgMover2->StopBackground();
				bgMover->StopBackground();
			}

			if (!doneOnce && collision.otherObject->GetBaseNameString() == "foot" && !cinematic)
			{
				if (mOwner->GetBaseNameString() == "platform")
					movingFloor->collisionGroup = "movingFloor";

				righBlock->collisionGroup = "Activator";
				cinematic = true;
				//std::cout << "activate" << std::endl;
				// Paralize the player
				stats->StartCinematic(false);

				up = true;
				wait = false;
				down = false;
			}
		}
	}

	bool MovingBackgroundActivator::OnGui()
	{
		bool isChanged = false;

		if (ImGui::InputText("BG Name", mBackgroundName, IM_ARRAYSIZE(mBackgroundName)))
			isChanged = true;

		ImGui::RadioButton("Accelerate", &mIsStart, 0);
			//std::cout << "Accelerate clicked : "<<mIsStart << std::endl;
		ImGui::SameLine();
		ImGui::RadioButton("Deccelerate", &mIsStart, 1);
		ImGui::SameLine();
		ImGui::RadioButton("Stop", &mIsStart, 2);
			//std::cout << "Deccelerate clicked : " << mIsStart << std::endl;

		return isChanged;
	}

	void MovingBackgroundActivator::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);

		std::string name;
		value["Background Name"] >> name;
		strncpy(mBackgroundName, name.c_str(), 64);

		value["Is Start"] >> mIsStart;
	}
	void MovingBackgroundActivator::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);

		std::string name = mBackgroundName;
		value["Background Name"] << mBackgroundName;
		value["Is Start"] << mIsStart;
	}
}