#pragma once

#include "Components\Logic.h"


namespace AEX
{
	class VeinMovingPlatform : public LogicComp
	{
		AEX_RTTI_DECL(VeinMovingPlatform, LogicComp);

	public:

		virtual void Initialize();
		virtual void Update();
		virtual void Shutdown();

		void FromJson(json & value);
		void ToJson(json & value);

		void OnCollisionStarted(const CollisionStarted& collision);

	private:
		
	};
}