#include "StationZoomOut.h"
#include "Composition\AEXGameObject.h"
#include "Components\Camera.h"
#include "Composition\AEXScene.h"
#include "Components\AEXTransformComp.h"
#include "Imgui\imgui.h"
#include "Platform\AEXTime.h"
#include "Components\SpriteComps.h"
#include "Components\Collider.h"
#include "Player\Movement\PlayerController.h"
#include "Follow/FollowAnObject.h"
#include "AI\Path2D.h"
#include "Graphics\GfxMgr.h"
#include "Enemy\EnemyFlying.h"
#include "Player/Stats/PlayerStats.h"
#include "Follow/ChaseObject.h"
#include "Bullets/BulletLogic.h"
#include "CameraLogic/CameraShaker.h"
#include "Enemy/WaveSystem/Spawner.h"
#include "Platform/AEXInput.h"
#include "Editor/Editor.h"


namespace AEX
{
	bool StationZoomOut::shouldLockCamera = false;
	bool StationZoomOut::mMaxZoomReached = false;
	bool StationZoomOut::onCinematic = false;
	int StationZoomOut::numberOfParticles = 7;
	bool StationZoomOut::mStartFadeToBlack = false;
	bool StationZoomOut::mInFirstShake = false;
	bool StationZoomOut::mFirstShakeFinished = false;
	bool StationZoomOut::mStartSecondShake = false;
	bool StationZoomOut::canSkipCinematic = false;

	void StationZoomOut::Initialize()
	{
		GameObject * cam = aexScene->FindObjectInANYSpace("camera");

		shouldLockCamera = false;
		mOriginalView = 5.2f;
		mZoomTimer = 0.0f;
		mCollidedOnce = false;
		mMaxZoomReached = false;

		mStartBars = false;
		shouldStartZoomingOut = false;
		mBarsGoingAway = false;
		
		onCinematic = false;
		numberOfParticles = 7;
		mStartFadeToBlack = false;

		mInFirstShake = false;
		mFirstShakeFinished = false;
		mFirstShakeTimer = 0.0f;

		BulletLogic::mStationEnemiesKilled = 0;
		ChaseObject::mParticleIdGenerator = 0;

		mBarsTimer = 0.0f;
		mBarsTime = 2.0f;

		skipTimer = 0.0f;
		mStartSkipping = false;
		mCinematicSkipped = false;
		mCinematicSkippingFinished = false;
		mSkipTimerFinished = false;

		mEntranceAudio1Played = false;
		mEntranceAudio2Played = false;
		mEntranceAudio3Played = false;
		mEntranceAudio4Played = false;
		mEntranceAudio5Played = false;
		mEntranceAudio6Played = false;
		mEntranceAudio7Played = false;

		GameObject * player = aexScene->FindObjectInANYSpace("player");
		mBg = aexScene->FindObjectInANYSpace(mBgName);
		GameObject * colObj = aexScene->FindObjectInANYSpace(colliderToActivate);
		GameObject * camLeftObj = aexScene->FindObjectInANYSpace(mCamLeftColName);
		GameObject * camRightObj = aexScene->FindObjectInANYSpace(mCamRightColName);
		GameObject * blackTop = aexScene->FindObjectInANYSpace("BlackBarTop");//mBlackTopName);
		GameObject * blackBot = aexScene->FindObjectInANYSpace("BlackBarBottom");//mBlackBotName);
		GameObject * blackTopCopy = aexScene->FindObjectInANYSpace("BlackBarTop copy");//mBlackTopName);
		GameObject * blackBotCopy = aexScene->FindObjectInANYSpace("BlackBarBottom copy");//mBlackBotName);
		GameObject * blackTopStationCopy = aexScene->FindObjectInANYSpace("BlackBarTopStation copy");//mBlackTopName);
		GameObject * blackBotStationCopy = aexScene->FindObjectInANYSpace("BlackBarBottomStation copy");//mBlackBotName);

		GameObject * enemy1 = aexScene->FindObjectInANYSpace("Flying01");
		GameObject * enemy2 = aexScene->FindObjectInANYSpace("Flying02");
		GameObject * enemy3 = aexScene->FindObjectInANYSpace("Flying03");
		GameObject * enemy4 = aexScene->FindObjectInANYSpace("Flying04");
		GameObject * enemy5 = aexScene->FindObjectInANYSpace("Flying05");
		GameObject * enemy6 = aexScene->FindObjectInANYSpace("Flying06");
		GameObject * enemy7 = aexScene->FindObjectInANYSpace("Flying07");

		GameObject * foreground = aexScene->FindObjectInANYSpace("foreground");

		if (foreground)
			mForegroundSprite = foreground->GetComp<SpriteComp>();
		
		if (player == nullptr || cam == nullptr/* || mBg == nullptr*/)
		{
			std::cout << "CAMERA OR PLAYER NOT FOUND\n";
			return;
		}

		mPlayerTr = player->GetComp<TransformComp>();
		mPlayerController = player->GetComp<PlayerController>();
		mCamTr = cam->GetComp<TransformComp>();
		mCam = cam->GetComp<Camera>();
		mCamShake = cam->GetComp<CameraShaker>();

		if (mBg != nullptr)
		{
			mBgTr = mBg->GetComp<TransformComp>();
			mBgSprite = mBg->GetComp<SpriteComp>();
		}

		if (colObj != nullptr)
			mStationLeftCollider = colObj->GetComp<BoxCollider>();

		if (camLeftObj != nullptr)
			mCameraLeftCollider = camLeftObj->GetComp<BoxCollider>();

		if (camRightObj != nullptr)
			mCameraRightCollider = camRightObj->GetComp<BoxCollider>();

		if (blackTop != nullptr)
			mBlackTopTr = blackTop->GetComp<TransformComp>();

		if (blackBot != nullptr)
			mBlackBotTr = blackBot->GetComp<TransformComp>();

		if (blackTopCopy)
			mBlackTopCopyTr = blackTopCopy->GetComp<TransformComp>();

		if (blackBotCopy)
			mBlackBotCopyTr = blackBotCopy->GetComp<TransformComp>();

		if (blackTopStationCopy)
			mBlackTopStationCopyTr = blackTopStationCopy->GetComp<TransformComp>();

		if (blackBotStationCopy)
			mBlackBotStationCopyTr = blackBotStationCopy->GetComp<TransformComp>();

		if (enemy1)
			mEnemy1PathFollower = enemy1->GetComp<PathFollower>();

		if (enemy2)
			mEnemy2PathFollower = enemy2->GetComp<PathFollower>();

		if (enemy3)
			mEnemy3PathFollower = enemy3->GetComp<PathFollower>();

		if (enemy4)
			mEnemy4PathFollower = enemy4->GetComp<PathFollower>();

		if (enemy5)
			mEnemy5PathFollower = enemy5->GetComp<PathFollower>();

		if (enemy6)
			mEnemy6PathFollower = enemy6->GetComp<PathFollower>();

		if (enemy7)
			mEnemy7PathFollower = enemy7->GetComp<PathFollower>();

		mBgTexSize = mBgSprite ? AEVec2(static_cast<f32>(mBgSprite->mTex->get()->GetWidth()) / 100.0f, static_cast<f32>(mBgSprite->mTex->get()->GetHeight() / 100.0f)) : AEVec2(1.0f, 1.0f);
		

		GameObject * spawnerObj1 = aexScene->FindObjectInANYSpace("Spawner01");
		GameObject * spawnerObj2 = aexScene->FindObjectInANYSpace("Spawner02");
		GameObject * spawnerObj3 = aexScene->FindObjectInANYSpace("Spawner03");

		if (spawnerObj1)
			mSpawner1 = spawnerObj1->GetComp<Spawner>();

		if (spawnerObj2)
			mSpawner2 = spawnerObj2->GetComp<Spawner>();

		if (spawnerObj3)
			mSpawner3 = spawnerObj3->GetComp<Spawner>();
	}
	

	void StationZoomOut::Update()
	{
		if (doneOnce == false)
		{
			Initialize();
			doneOnce = true;

			if (mForegroundSprite)
				mForegroundSprite->mColor[3] = 0.0f;

			if (mEnemy1PathFollower)
			{
				mEnemy1PathFollower->ResetPosition();
				mEnemy1Chaser = mEnemy1PathFollower->mOwner->GetComp<ChaseObject>();
				mEnemy1Chaser->mOnCinematic = true;
			}

			if (mEnemy2PathFollower)
			{
				mEnemy2PathFollower->ResetPosition();
				mEnemy2Chaser = mEnemy2PathFollower->mOwner->GetComp<ChaseObject>();
				mEnemy2Chaser->mOnCinematic = true;
			}

			if (mEnemy3PathFollower)
			{
				mEnemy3PathFollower->ResetPosition();
				mEnemy3Chaser = mEnemy3PathFollower->mOwner->GetComp<ChaseObject>();
				mEnemy3Chaser->mOnCinematic = true;
			}

			if (mEnemy4PathFollower)
			{
				mEnemy4PathFollower->ResetPosition();
				mEnemy4Chaser = mEnemy4PathFollower->mOwner->GetComp<ChaseObject>();
				mEnemy4Chaser->mOnCinematic = true;
			}

			if (mEnemy5PathFollower)
			{
				mEnemy5PathFollower->ResetPosition();
				mEnemy5Chaser = mEnemy5PathFollower->mOwner->GetComp<ChaseObject>();
				mEnemy5Chaser->mOnCinematic = true;
			}

			if (mEnemy6PathFollower)
			{
				mEnemy6PathFollower->ResetPosition();
				mEnemy6Chaser = mEnemy6PathFollower->mOwner->GetComp<ChaseObject>();
				mEnemy6Chaser->mOnCinematic = true;
			}

			if (mEnemy7PathFollower)
			{
				mEnemy7PathFollower->ResetPosition();
				mEnemy7Chaser = mEnemy7PathFollower->mOwner->GetComp<ChaseObject>();
				mEnemy7Chaser->mOnCinematic = true;
			}

			if (mSpawner1)
				mSpawner1->StopSpawning();

			if (mSpawner2)
				mSpawner2->StopSpawning();

			if (mSpawner3)
				mSpawner3->StopSpawning();
		}

		if (mStartBars)
		{
			mBarsTimer += (f32)aexTime->GetFrameTime();

			f32 tn = mBarsTimer / mBarsTime;

			if (tn > 1)
			{
				tn = 1;
				shouldStartZoomingOut = true;
				mStartBars = false;
				mBarsTimer = 0.0f;
			}

			f32 easedTn = EaseLinear(tn);

			AEVec2 topCopyPos(mBlackTopCopyTr->mLocal.mTranslationZ.x, mBlackTopCopyTr->mLocal.mTranslationZ.y);
			AEVec2 botCopyPos(mBlackBotCopyTr->mLocal.mTranslationZ.x, mBlackBotCopyTr->mLocal.mTranslationZ.y);

			AEVec2 topLerp = AEVec2::Lerp(topCopyPos, mOriginalBlackTopPos, easedTn);
			AEVec2 botLerp = AEVec2::Lerp(botCopyPos, mOriginalBlackBotPos, easedTn);

			mBlackTopTr->mLocal.mTranslationZ.x = topLerp.x;
			mBlackTopTr->mLocal.mTranslationZ.y = topLerp.y;

			mBlackBotTr->mLocal.mTranslationZ.x = botLerp.x;
			mBlackBotTr->mLocal.mTranslationZ.y = botLerp.y;
		}

		if (shouldStartZoomingOut)
		{
			FollowAnObject * newCompTop = new FollowAnObject;
			FollowAnObject * newCompBot = new FollowAnObject;
			
			strcpy_s(newCompTop->followedName, "camera");
			strcpy_s(newCompBot->followedName, "camera");
			newCompTop->mOffset.y = 6.0f;
			newCompBot->mOffset.y = -6.0f;
			newCompTop->mParentToCamera = true;
			newCompBot->mParentToCamera = true;

			mBlackTopTr->mOwner->AddComp(newCompTop);
			mBlackBotTr->mOwner->AddComp(newCompBot);

			shouldStartZoomingOut = false;
		}

		if (shouldLockCamera && mBlackBarsActivator == false && mCinematicSkipped == false)
		{
			f32 posTn = (mPlayerTr->mLocal.mTranslationZ.x - mPlayerOriginalPos.x) / (mBgTexSize.x * mBgTr->mLocal.mScale.x * mMaxViewScalar);
			f32 timeTn = mZoomTimer / mZoomTime;

			/*if (posTn < 0)
			{
				if (mMaxZoomReached)
					posTn = 0;
				else
				{
					shouldLockCamera = false;
					return;
				}
			}*/

			if (/*posTn*/timeTn > 1)
			{
				/*posTn*/timeTn = 1;

				//if (mMaxZoomReached == false && mStationLeftCollider != nullptr)
				//	mStationLeftCollider->mIsGhost = false;

				mInFirstShake = true;

				//mMaxZoomReached = true;
			}

			if (mMaxZoomReached)
				posTn = 1;

			f32 easedTn = mEaseFn(timeTn/*posTn*/);

			AEVec2 bgPos(mBgTr->mLocal.mTranslationZ.x, mBgTr->mLocal.mTranslationZ.y);
			AEVec2 camPos(mCamLockPos.x, mCamLockPos.y);

			AEVec2 posLerp = AEVec2::Lerp(bgPos, camPos, easedTn/*posTn*/);
			mCamTr->mLocal.mTranslationZ.x = posLerp.x;
			mCamTr->mLocal.mTranslationZ.y = posLerp.y;

			AEVec2 viewLerp = AEVec2::Lerp(AEVec2(mViewFinal, 0.0f), AEVec2(mOriginalView, 0.0f), easedTn/*posTn*/);
			mCam->mViewRectangleSize = viewLerp.x;

			mZoomTimer += (f32)aexTime->GetFrameTime();
		}

		if (mInFirstShake && mFirstShakeFinished == false && mBlackBarsActivator == false)
		{
			mFirstShakeTimer += (f32)aexTime->GetFrameTime();

			if (mFirstShakeTimer > 1.0f && mFirstShakeTimer < (mFirstShakeTime - 2.0f))
				mCamShake->mTrauma = 1.0f;

			if (mFirstShakeTimer > mFirstShakeTime)
			{
				mFirstShakeFinished = true;
				mMaxZoomReached = true;
				mFirstShakeTimer = 0.0f;
			}
		}

		if (mMaxZoomReached)
		{
			if (mEnemy1PathFollower)
				mEnemy1PathFollower->ContinueFollowing();

			if (mEnemy2PathFollower)
				mEnemy2PathFollower->ContinueFollowing();

			if (mEnemy3PathFollower)
				mEnemy3PathFollower->ContinueFollowing();

			if (mEnemy4PathFollower)
				mEnemy4PathFollower->ContinueFollowing();

			if (mEnemy5PathFollower)
				mEnemy5PathFollower->ContinueFollowing();

			if (mEnemy6PathFollower)
				mEnemy6PathFollower->ContinueFollowing();

			if (mEnemy7PathFollower)
				mEnemy7PathFollower->ContinueFollowing();

			PlayEnemiesEntranceAudios();
		}
		else
		{
			if (mEnemy1PathFollower)
				mEnemy1PathFollower->StopFollowing();

			if (mEnemy2PathFollower)
				mEnemy2PathFollower->StopFollowing();

			if (mEnemy3PathFollower)
				mEnemy3PathFollower->StopFollowing();

			if (mEnemy4PathFollower)
				mEnemy4PathFollower->StopFollowing();

			if (mEnemy5PathFollower)
				mEnemy5PathFollower->StopFollowing();

			if (mEnemy6PathFollower)
				mEnemy6PathFollower->StopFollowing();

			if (mEnemy7PathFollower)
				mEnemy7PathFollower->StopFollowing();
		}

		// If the zoomout has finished and all the enemies have appeared
		if (mMaxZoomReached && mEnemy7PathFollower->HasFinishedPath() && mBarsGoingAway == false)
		{
			mOriginalBlackTopStationPos.x = mBlackTopTr->mLocal.mTranslationZ.x;
			mOriginalBlackTopStationPos.y = mBlackTopTr->mLocal.mTranslationZ.y;

			mOriginalBlackTopStationPos.x = mBlackBotTr->mLocal.mTranslationZ.x;
			mOriginalBlackBotStationPos.y = mBlackBotTr->mLocal.mTranslationZ.y;

			mBlackTopTr->mOwner->RemoveCompType(FollowAnObject::TYPE());
			mBlackBotTr->mOwner->RemoveCompType(FollowAnObject::TYPE());

			mBlackTopTr->mLocal.mTranslationZ.x = mOriginalBlackTopStationPos.x;
			mBlackTopTr->mLocal.mTranslationZ.y = mOriginalBlackTopStationPos.y;
			
			mBlackBotTr->mLocal.mTranslationZ.x = mOriginalBlackBotStationPos.x;
			mBlackBotTr->mLocal.mTranslationZ.y = mOriginalBlackBotStationPos.y;

			mBarsGoingAway = true;
		}

		if (mBarsGoingAway)
		{
			// tn computation
			mBarsTimer += (f32)aexTime->GetFrameTime();

			f32 tn = mBarsTimer / mBarsTime;

			if (tn > 1)
			{
				tn = 1;
				//mBarsGoingAway = false;
				mMaxZoomReached = false;

				mEnemy1PathFollower->StopFollowing();
				mEnemy2PathFollower->StopFollowing();
				mEnemy3PathFollower->StopFollowing();
				mEnemy4PathFollower->StopFollowing();
				mEnemy5PathFollower->StopFollowing();
				mEnemy6PathFollower->StopFollowing();
				mEnemy7PathFollower->StopFollowing();

				mEnemy1Chaser->mOnCinematic = false;
				mEnemy2Chaser->mOnCinematic = false;
				mEnemy3Chaser->mOnCinematic = false;
				mEnemy4Chaser->mOnCinematic = false;
				mEnemy5Chaser->mOnCinematic = false;
				mEnemy6Chaser->mOnCinematic = false;
				mEnemy7Chaser->mOnCinematic = false;

				if (mSpawner1)
					mSpawner1->ContinueSpawning();

				if (mSpawner2)
					mSpawner2->ContinueSpawning();

				if (mSpawner3)
					mSpawner3->ContinueSpawning();

				onCinematic = false;

				// mEnemy1Flying->mOnCinematic = false;
				// mEnemy2Flying->mOnCinematic = false;
				// mEnemy3Flying->mOnCinematic = false;
				// mEnemy4Flying->mOnCinematic = false;
				// mEnemy5Flying->mOnCinematic = false;
				// mEnemy6Flying->mOnCinematic = false;
				// mEnemy7Flying->mOnCinematic = false;

				mPlayerController->stats->StopCinematic();

				// Here the cinematic has finished

				canSkipCinematic = true;
			}

			f32 easedTn = EaseLinear(tn);


			// Lerp the black bars
			AEVec2 topCopyPos(mBlackTopStationCopyTr->mLocal.mTranslationZ.x, mBlackTopStationCopyTr->mLocal.mTranslationZ.y);
			AEVec2 botCopyPos(mBlackBotStationCopyTr->mLocal.mTranslationZ.x, mBlackBotStationCopyTr->mLocal.mTranslationZ.y);

			AEVec2 topLerp = AEVec2::Lerp(topCopyPos, mOriginalBlackTopStationPos, easedTn - 0.6f < 0 ? 0.0f : easedTn - 0.6f);
			AEVec2 botLerp = AEVec2::Lerp(botCopyPos, mOriginalBlackBotStationPos, easedTn - 0.6f < 0 ? 0.0f : easedTn - 0.6f);

			mBlackTopTr->mLocal.mTranslationZ.x = topLerp.x;
			mBlackTopTr->mLocal.mTranslationZ.y = topLerp.y;

			mBlackBotTr->mLocal.mTranslationZ.x = botLerp.x;
			mBlackBotTr->mLocal.mTranslationZ.y = botLerp.y;

			// Lerp the camera view
			AEVec2 lerpResult = AEVec2::Lerp(AEVec2(mViewAfterBars, 0), AEVec2(mViewFinal, 0), easedTn);

			mCam->mViewRectangleSize = lerpResult.x;
		}

		if (mStartSecondShake)
		{
			mFirstShakeTimer += (f32)aexTime->GetFrameTime();

			if (mFirstShakeTimer > 1.0f && mFirstShakeTimer < (mFirstShakeTime - 0.5f))
				mCamShake->mTrauma = 1.0f;

			if (mFirstShakeTimer > mFirstShakeTime)
			{
				mFirstShakeFinished = true;
				mMaxZoomReached = true;
				mFirstShakeTimer = 0.0f;
			}
		}

		/*if (canSkipCinematic && onCinematic && mSkipTimerFinished == false && mCinematicSkippingFinished == false && (aexInput->GamepadButtonPressed(Input::eGamepadButtonA) || aexInput->GamepadButtonPressed(Input::eGamepadButtonB) || aexInput->KeyPressed(GLFW_KEY_ENTER)))
		{
			skipTimer += (f32)aexTime->GetFrameTime();

			if (skipTimer > SKIP_TIME)
			{
				mStartSkipping = true;
				mSkipTimerFinished = true;
			}
		}
		else if (canSkipCinematic && onCinematic && mSkipTimerFinished == false)
		{
			skipTimer -= (f32)aexTime->GetFrameTime();

			if (skipTimer < 0.0f)
				skipTimer = 0.0f;
		}

		if (mStartSkipping && mCinematicSkippingFinished == false)
		{
			mForegroundSprite->mColor[3] += (f32)aexTime->GetFrameTime();

			if (mForegroundSprite->mColor[3] >= 1.0f)
			{
				SkipCinematic();
				mStartSkipping = false;
			}
		}

		if (mCinematicSkipped && mCinematicSkippingFinished == false)
		{
			mForegroundSprite->mColor[3] -= (f32)aexTime->GetFrameTime();

			if (mForegroundSprite->mColor[3] <= 0.0f)
			{
				mForegroundSprite->mColor[3] = 0.0f;
				mCinematicSkippingFinished = true;
				//mCamTr->mLocal.mTranslationZ = mBgTr->mLocal.mTranslationZ;
				//mCinematicSkipped = false;
			}
		}*/

		if (mStartFadeToBlack && mForegroundSprite)
		{
			mForegroundSprite->mColor[3] += (f32)aexTime->GetFrameTime() * 0.25f;

			if (mForegroundSprite->mColor[3] >= 1.0f)
			{
				Editor->loadFromCertainLevel = true;
				Editor->levelToLoadName = "Boiler room";
				//aexScene->ChangeLevel("data/Levels/Boiler room.json");
				//aexScene->bFadeTransition = true;
				mStartFadeToBlack = false;
			}
		}
	}
	
	void StationZoomOut::Shutdown()
	{
		StationZoomOut::shouldLockCamera = false;
	}

	void StationZoomOut::PlayEnemiesEntranceAudios()
	{
		if (mEnemy1Chaser->mOwner->IsGameObjectInScreen() && mEntranceAudio1Played == false)
		{
			audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/Station/EnemiesAppearance/Whoosh_BigJump3.wav"), Voice::SFX);
			mEntranceAudio1Played = true;
		}

		if (mEnemy2Chaser->mOwner->IsGameObjectInScreen() && mEntranceAudio2Played == false)
		{
			audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/Station/EnemiesAppearance/Whoosh_BigJump3.wav"), Voice::SFX);
			mEntranceAudio2Played = true;
		}

		if (mEnemy3Chaser->mOwner->IsGameObjectInScreen() && mEntranceAudio3Played == false)
		{
			audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/Station/EnemiesAppearance/Whoosh_BigJump3.wav"), Voice::SFX);
			mEntranceAudio3Played = true;
		}

		if (mEnemy4Chaser->mOwner->IsGameObjectInScreen() && mEntranceAudio4Played == false)
		{
			audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/Station/EnemiesAppearance/Whoosh_BigJump3.wav"), Voice::SFX);
			mEntranceAudio4Played = true;
		}

		if (mEnemy5Chaser->mOwner->IsGameObjectInScreen() && mEntranceAudio5Played == false)
		{
			audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/Station/EnemiesAppearance/Whoosh_BigJump3.wav"), Voice::SFX);
			mEntranceAudio5Played = true;
		}

		if (mEnemy6Chaser->mOwner->IsGameObjectInScreen() && mEntranceAudio6Played == false)
		{
			audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/Station/EnemiesAppearance/Whoosh_BigJump3.wav"), Voice::SFX);
			mEntranceAudio6Played = true;
		}

		if (mEnemy7Chaser->mOwner->IsGameObjectInScreen() && mEntranceAudio7Played == false)
		{
			audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/Station/EnemiesAppearance/Whoosh_BigJump3.wav"), Voice::SFX);
			mEntranceAudio7Played = true;
		}
	}

	void StationZoomOut::SkipCinematic()
	{
		GameObject * zoomer = aexScene->FindObjectInANYSpace("StationCameraLocker");

		if (zoomer && mBlackBarsActivator == false)
		{
			TransformComp * zoomerTr = zoomer->GetComp<TransformComp>();

			if (zoomerTr)
			{
				mCollidedOnce = true;
				mStationLeftCollider->mIsGhost = false;
				mCameraLeftCollider->mIsGhost = false;
				mCameraRightCollider->mIsGhost = false;
				mPlayerController->stats->StartCinematic(false);
				mPlayerTr->mLocal.mTranslationZ.x = zoomerTr->mLocal.mTranslationZ.x;
				mPlayerTr->mLocal.mTranslationZ.y = zoomerTr->mLocal.mTranslationZ.y - 1.0f;
				mCamTr->mLocal.mTranslationZ.x = mBgTr->mLocal.mTranslationZ.x;
				mCamTr->mLocal.mTranslationZ.y = mBgTr->mLocal.mTranslationZ.y;
				mCam->mViewRectangleSize = mViewFinal;
				mFirstShakeFinished = true;
				mMaxZoomReached = true;
				shouldLockCamera = true;
				mFirstShakeFinished = true;
			}
		}

		mCinematicSkipped = true;
	}
	
	bool StationZoomOut::OnGui()
	{
		bool isChanged = false;

		if (ImGui::Checkbox("Bars Activator", &mBlackBarsActivator))
		{
			isChanged = true;
		}

		if (ImGui::InputFloat("MaxView", &mViewFinal))
		{
			isChanged = true;
		}
		if (ImGui::InputFloat("ZoomTime", &mZoomTime))
		{
			isChanged = true;
		}
		if (ImGui::InputFloat("View After Bars", &mViewAfterBars))
		{
			isChanged = true;
		}

		/*if (ImGui::InputFloat("MaxViewScalar", &mMaxViewScalar))
		{
			isChanged = true;
		}*/
		/*if (ImGui::InputFloat("TimeToMax", &mLerpTime))
		{
			isChanged = true;
		}*/

		if (ImGui::InputText("BG To Zoom Out To", mBgName, IM_ARRAYSIZE(mBgName)))
		{
			isChanged = true;
		}

		/*if (ImGui::InputText("Top Black Bar", mBlackTopName, IM_ARRAYSIZE(mBlackTopName)))
		{
			isChanged = true;
		}
		if (ImGui::InputText("Bottom Black Bar", mBlackBotName, IM_ARRAYSIZE(mBlackBotName)))
		{
			isChanged = true;
		}*/

		if (ImGui::InputText("Camera Left Collider", mCamLeftColName, IM_ARRAYSIZE(mCamLeftColName)))
		{
			isChanged = true;
		}
		if (ImGui::InputText("Camera Right Collider", mCamRightColName, IM_ARRAYSIZE(mCamRightColName)))
		{
			isChanged = true;
		}

		if (ImGui::InputText("ColliderToActivate", colliderToActivate, IM_ARRAYSIZE(colliderToActivate)))
		{
			isChanged = true;
		}

		if (ImGui::BeginCombo("EaseFunction", mEaseName.c_str()))
		{

			if (ImGui::Selectable("EaseLinear"))
			{
				mEaseName = "EaseLinear";
				mEaseId = 0;
				mEaseFn = EaseLinear;
				isChanged = true;
			}
			if (ImGui::Selectable("EaseOutQuad"))
			{
				mEaseName = "EaseOutQuad";
				mEaseId = 1;
				mEaseFn = EaseOutQuad;
				isChanged = true;
			}
			if (ImGui::Selectable("EaseInQuad"))
			{
				mEaseName = "EaseInQuad";
				mEaseId = 2;
				mEaseFn = EaseInQuad;
				isChanged = true;
			}
			if (ImGui::Selectable("EaseInOutQuad"))
			{
				mEaseName = "EaseInOutQuad";
				mEaseId = 3;
				mEaseFn = EaseInOutQuad;
				isChanged = true;
			}

			ImGui::EndCombo();
		}

		if (ImGui::InputFloat("First Shake Time", &mFirstShakeTime))
		{
			isChanged = true;
		}

		return isChanged;
	}
	
	void StationZoomOut::OnCollisionStarted(const CollisionStarted& collision)
	{
		if (collision.otherObject == nullptr)
			return;

		if (collision.otherObject->GetBaseNameString() == "player" && mBlackBarsActivator == false && mCollidedOnce == false &&
			mPlayerTr->mLocal.mTranslationZ.x < mOwner->GetComp<TransformComp>()->mLocal.mTranslationZ.x)
		{
			shouldLockCamera = true;
			mCamLockPos = mCamTr->mLocal.mTranslationZ;
			mOriginalView = mCam->mViewRectangleSize;
			//mPlayerOriginalPos = mPlayerTr->mLocal.mTranslationZ;

			if (mStationLeftCollider != nullptr)
				mStationLeftCollider->mIsGhost = false;

			if (mCameraLeftCollider != nullptr)
				mCameraLeftCollider->mIsGhost = false;
			if (mCameraRightCollider != nullptr)
				mCameraRightCollider->mIsGhost = false;

			mPlayerController->stats->StartCinematic(false);

			mCollidedOnce = true;
			//if (mPlayerController != nullptr)
			//	mPlayerController->totalSpeed = 0;
		}
		else if (collision.otherObject->GetBaseNameString() == "player" && mBlackBarsActivator == true && mCollidedOnce == false)
		{
			mStartBars = true;
			mCollidedOnce = true;
			mOriginalBlackTopPos = AEVec2(mBlackTopTr->mLocal.mTranslationZ.x, mBlackTopTr->mLocal.mTranslationZ.y);
			mOriginalBlackBotPos = AEVec2(mBlackBotTr->mLocal.mTranslationZ.x, mBlackBotTr->mLocal.mTranslationZ.y);
			mPlayerStats = mPlayerController->stats;
			mPlayerStats->StartCinematic(true, true);
			onCinematic = true;
		}
	}

	/*void StationZoomOut::OnCollisionEnded(const CollisionEnded& collision)
	{
		if (collision.otherObject == nullptr)
			return;

		if (collision.otherObject->GetBaseNameString() == "player" &&
			mPlayerTr->mLocal.mTranslationZ.x > mOwner->GetComp<TransformComp>()->mLocal.mTranslationZ.x)
		{
			shouldLockCamera = true;
			mCamLockPos = mCamTr->mLocal.mTranslationZ;
			mOriginalView = mCam->mViewRectangleSize;
			mPlayerOriginalPos = mPlayerTr->mLocal.mTranslationZ;
			mStationLeftCollider->mIsGhost = true;
		}
	}*/


	void StationZoomOut::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);

		value["First Shake Time"] >> mFirstShakeTime;

		value["View Final"] >> mViewFinal;
		value["Lerp Time"] >> mLerpTime;

		value["Ease Id"] >> mEaseId;

		if (mEaseId == 0)
		{
			mEaseName = "EaseLinear";
			mEaseFn = EaseLinear;
		}
		else if (mEaseId == 1)
		{
			mEaseName = "EaseOutQuad";
			mEaseFn = EaseOutQuad;
		}
		else if (mEaseId == 2)
		{
			mEaseName = "EaseInQuad";
			mEaseFn = EaseInQuad;
		}
		else if (mEaseId == 3)
		{
			mEaseName = "EaseInOutQuad";
			mEaseFn = EaseInOutQuad;
		}

		std::string col;
		value["Collider To Activate"] >> col;
		strcpy_s(colliderToActivate, col.c_str());

		std::string bg;
		value["Background Name"] >> bg;
		strcpy_s(mBgName, bg.c_str());

		value["Max View Scalar"] >> mMaxViewScalar;

		value["Zoom Time"] >> mZoomTime;

		std::string camLeft;
		value["Cam Left Collider Name"] >> camLeft;
		strcpy_s(mCamLeftColName, camLeft.c_str());

		std::string camRight;
		value["Cam Right Collider Name"] >> camRight;
		strcpy_s(mCamRightColName, camRight.c_str());

		value["Black Bars Activator"] >> mBlackBarsActivator;

		value["View After Bars"] >> mViewAfterBars;

		/*std::string blackTop;
		value["Black Top Name"] >> blackTop;
		strcpy_s(mBlackTopName, blackTop.c_str());

		std::string blackBot;
		value["Black Bottom Name"] >> blackBot;
		strcpy_s(mBlackBotName, blackBot.c_str());*/
	}
	void StationZoomOut::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);

		value["First Shake Time"] << mFirstShakeTime;

		value["View Final"] << mViewFinal;
		value["Lerp Time"] << mLerpTime;

		value["Ease Id"] << mEaseId;

		std::string col = colliderToActivate;
		value["Collider To Activate"] << col;

		std::string bg = mBgName;
		value["Background Name"] << bg;

		value["Max View Scalar"] << mMaxViewScalar;
		
		value["Zoom Time"] << mZoomTime;

		std::string camLeftCol = mCamLeftColName;
		value["Cam Left Collider Name"] << camLeftCol;
		
		std::string camRightCol = mCamRightColName;
		value["Cam Right Collider Name"] << camRightCol;

		value["Black Bars Activator"] << mBlackBarsActivator;

		/*std::string blackTop = mBlackTopName;
		value["Black Top Name"] << blackTop;

		std::string blackBot = mBlackBotName;
		value["Black Bottom Name"] << blackBot;*/

		value["View After Bars"] << mViewAfterBars;
	}
}