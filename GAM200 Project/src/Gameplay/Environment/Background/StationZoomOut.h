#pragma once

#include "Components\Logic.h"
#include "AI\Easing.h"
#include "Enemy/WaveSystem/WaveSystem.h"


#define SKIP_TIME 0.6f


class Camera;
class PlayerController;


class PlayerStats;

namespace AEX
{
	class TransformComp;
	class SpriteComp;
	class BoxCollider;
	class PathFollower;
	class EnemyFlying;
	class ChaseObject;
	class CameraShaker;
	class Spawner;

	class StationZoomOut : public LogicComp
	{
		AEX_RTTI_DECL(StationZoomOut, LogicComp);

	public:

		void Initialize();
		void Update();
		void Shutdown();

		bool OnGui();

		void FromJson(json & value);
		void ToJson(json & value);

		void OnCollisionStarted(const CollisionStarted& collision);
		//void OnCollisionEnded(const CollisionEnded& collision);

		static bool shouldLockCamera;

		TransformComp * mPlayerTr = nullptr;
		PlayerController * mPlayerController = nullptr;
		TransformComp * mCamTr = nullptr;

		GameObject * mBg = nullptr;
		TransformComp * mBgTr = nullptr;
		SpriteComp * mBgSprite = nullptr;
		AEVec2 mBgTexSize;
		char mBgName[64];

		Camera * mCam = nullptr;
		CameraShaker * mCamShake = nullptr;
		AEVec3 mCamLockPos;
		AEVec3 mPlayerOriginalPos;

		bool doneOnce = false;

		// For controlling time of zoom out
		f32 mZoomTimer = 0.0f;
		f32 mZoomTime;

		// The maximum view of the camera
		f32 mViewFinal;
		f32 mMaxViewScalar;

		// The original view of the camera before zoom out, although this is
		// initialized to 5.4, it is stored on collision with player
		f32 mOriginalView = 5.4f;

		// ???
		f32 mLerpTime;

		// The zoom out easing function
		EaseFunc mEaseFn;
		int mEaseId;
		std::string mEaseName;

		// Loop to determine if the max zoom has been reached
		static bool mMaxZoomReached;

		// Only start zoom out on the first collision with StationZoomOut
		bool mCollidedOnce = false;

		// The left collider of the station so that the player won't be able to escape
		BoxCollider * mStationLeftCollider = nullptr;
		char colliderToActivate[32];

		// BoxCollider that follows the camera in the left
		BoxCollider * mCameraLeftCollider = nullptr;
		char mCamLeftColName[64];

		// BoxCollider that follows the camera in the right
		BoxCollider * mCameraRightCollider = nullptr;
		char mCamRightColName[64];

		TransformComp * mBlackTopTr = nullptr;
		TransformComp * mBlackTopCopyTr = nullptr;
		//char mBlackTopName[64];

		TransformComp * mBlackBotTr = nullptr;
		TransformComp * mBlackBotCopyTr = nullptr;
		//char mBlackBotName[64];

		TransformComp * mBlackTopStationCopyTr = nullptr;
		TransformComp * mBlackBotStationCopyTr = nullptr;

		f32 mBarsTimer = 0.0f;
		f32 mBarsTime;

		bool shouldStartZoomingOut = false;

		bool mStartBars = false;
		bool mBlackBarsActivator;

		bool mBarsGoingAway = false;

		AEVec2 mOriginalBlackTopPos;
		AEVec2 mOriginalBlackBotPos;

		AEVec2 mOriginalBlackTopStationPos;
		AEVec2 mOriginalBlackBotStationPos;

		PathFollower * mEnemy1PathFollower = nullptr;
		PathFollower * mEnemy2PathFollower = nullptr;
		PathFollower * mEnemy3PathFollower = nullptr;
		PathFollower * mEnemy4PathFollower = nullptr;
		PathFollower * mEnemy5PathFollower = nullptr;
		PathFollower * mEnemy6PathFollower = nullptr;
		PathFollower * mEnemy7PathFollower = nullptr;

		//EnemyFlying * mEnemy1Flying = nullptr;
		//EnemyFlying * mEnemy2Flying = nullptr;
		//EnemyFlying * mEnemy3Flying = nullptr;
		//EnemyFlying * mEnemy4Flying = nullptr;
		//EnemyFlying * mEnemy5Flying = nullptr;
		//EnemyFlying * mEnemy6Flying = nullptr;
		//EnemyFlying * mEnemy7Flying = nullptr;

		ChaseObject * mEnemy1Chaser = nullptr;
		ChaseObject * mEnemy2Chaser = nullptr;
		ChaseObject * mEnemy3Chaser = nullptr;
		ChaseObject * mEnemy4Chaser = nullptr;
		ChaseObject * mEnemy5Chaser = nullptr;
		ChaseObject * mEnemy6Chaser = nullptr;
		ChaseObject * mEnemy7Chaser = nullptr;

		f32 mViewAfterBars;

		PlayerStats * mPlayerStats = nullptr;

		static bool onCinematic;

		static int numberOfParticles;

		static bool mStartFadeToBlack;

		static bool mInFirstShake;
		static bool mFirstShakeFinished;

		static bool mStartSecondShake;

		f32 mFirstShakeTimer = 0.0f;
		f32 mFirstShakeTime;

		SpriteComp * mForegroundSprite = nullptr;


		Spawner * mSpawner1 = nullptr;
		Spawner * mSpawner2 = nullptr;
		Spawner * mSpawner3 = nullptr;

		static bool canSkipCinematic;
		f32 skipTimer = 0.0f;
		bool mStartSkipping = false;
		bool mCinematicSkipped = false;
		bool mCinematicSkippingFinished = false;

		bool mSkipTimerFinished = false;

	private:

		void SkipCinematic();


		void PlayEnemiesEntranceAudios();
		bool mEntranceAudio1Played = false;
		bool mEntranceAudio2Played = false;
		bool mEntranceAudio3Played = false;
		bool mEntranceAudio4Played = false;
		bool mEntranceAudio5Played = false;
		bool mEntranceAudio6Played = false;
		bool mEntranceAudio7Played = false;
	};
}