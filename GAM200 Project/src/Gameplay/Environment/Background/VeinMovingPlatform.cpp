#include "VeinMovingPlatform.h"
#include "Composition\AEXGameObject.h"
#include "Platform\AEXTime.h"


namespace AEX
{
	void VeinMovingPlatform::Initialize()
	{

	}

	void VeinMovingPlatform::Shutdown()
	{

	}

	void VeinMovingPlatform::Update()
	{
		TransformComp * tr = mOwner->GetComp<TransformComp>();

		if (tr != nullptr)
		{

			tr->mLocal.mTranslationZ.x -= (f32)aexTime->GetFrameTime() * 24.0f;

		}
	}

	void VeinMovingPlatform::OnCollisionStarted(const CollisionStarted& collision)
	{
		/*if (collision.otherObject->GetBaseNameString() == "player")
		{

		}*/
	}

	void VeinMovingPlatform::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);
	}

	void VeinMovingPlatform::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);
	}
}