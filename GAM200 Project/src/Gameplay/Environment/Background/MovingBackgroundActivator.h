#pragma once
#include "Components/Logic.h"

class PlayerStats;
namespace AEX
{
	class KillerCarsActivator;
	class ICollider;
	class TransformComp;
	class MovingBackgroundActivator : public LogicComp
	{
	public:

		void Initialize();
		void Update();
		void Shutdown();

		void OnCollisionStarted(const CollisionStarted& collision);

		bool OnGui();

		PlayerStats* stats = nullptr;
		KillerCarsActivator* carActivator = nullptr;

		void FromJson(json & value);
		void ToJson(json & value);


	private:
		char mBackgroundName[64];
		int mIsStart = 0;
		float timer;
		bool cinematic;
		bool up;
		bool wait;
		bool down;
		bool doneOnce;
		bool firstUpdate;

		TransformComp* b1;
		TransformComp* b2;
		TransformComp* cam;
		ICollider* righBlock;
		ICollider* movingFloor;
	};
}