#include "BackgroundMover.h"
#include "Components\SpriteComps.h"
#include "Composition\AEXGameObject.h"
#include "Platform\AEXTime.h"
#include "Imgui\imgui.h"
#include "AI/Easing.h"
#include "Composition/AEXGameObject.h"
#include "Composition/AEXSpace.h"
#include "Environment\Background\StationZoomOut.h"
#include "BoilerRoom/PuzzleLogic.h"

namespace AEX
{
	void BackgroundMover::Initialize()
	{
		StationZoomOut::shouldLockCamera = false;
		PuzzleLogic::lockCamera = false;
		if (GameObject* p = mOwner->GetParentSpace()->FindObject("player"))
		{
			stats = p->GetComp<PlayerStats>();
		}
		maxSpeedReached = false;
	}
	void BackgroundMover::Shutdown()
	{

	}

	void BackgroundMover::Update()
	{
		if (!stats)
			Initialize();
		SpriteComp * sprite = mOwner->GetComp<SpriteComp>();

		if (sprite != nullptr && mStart && (!mEnd && !mGradualEnd))
		{
			mTimer = mTimer > mTimeToMaxSpeed ? mTimeToMaxSpeed : mTimer;
			f32 tn = mTimer / mTimeToMaxSpeed;

			tn = mAccelerateFn(tn);

			if (mTimer >= mTimeToMaxSpeed)
			{
				mSpeed = mMaxSpeed;
				maxSpeedReached = true;
				//stats->onVeins = true;
				//stats->onVeinsTransition = false;
			}
			else
				mSpeed = AEVec2::Lerp(AEVec2(mMaxSpeed, 0), AEVec2(mInitialSpeed, 0), tn).x;

			sprite->mTexOffset.x += (f32)aexTime->GetFrameTime() * mSpeed;

			mTimer += (f32)aexTime->GetFrameTime();
		}

		if (sprite != nullptr && mGradualEnd)
		{
			mEndTimer = mTimer > mEndTime ? mEndTime : mEndTimer;
			f32 tn = mEndTimer / mEndTime;

			tn = mDeccelerateFn(tn);

			if (mEndTimer >= mEndTime)
			{
				mSpeed = 0.0f;
				//stats->onVeins = true;
				//stats->onVeinsTransition = false;
			}
			else
				mSpeed = AEVec2::Lerp(AEVec2(0.0f, 0.0f), AEVec2(mMaxSpeed, 0), tn).x;

			sprite->mTexOffset.x += (f32)aexTime->GetFrameTime() * mSpeed;

			mEndTimer += (f32)aexTime->GetFrameTime();
		}
	}


	bool BackgroundMover::OnGui()
	{
		bool isChanged = false;

		if (ImGui::InputFloat("Initial Speed", &mInitialSpeed))
			isChanged = true;
		if (ImGui::InputFloat("Max Speed", &mMaxSpeed))
			isChanged = true;
		if (ImGui::InputFloat("Time Till Max", &mTimeToMaxSpeed))
			isChanged = true;

		std::string easeFnStr = "EaseFunction";
		std::string easeLinearStr = "EaseLinear";
		std::string easeInStr = "EaseInQuad";
		std::string easeOutStr = "EaseOutQuad";
		std::string easeInOutStr = "EaseInOutQuad";

		if (ImGui::BeginCombo(easeFnStr.c_str(), mAccelerateEasing.c_str()))
		{
			if (ImGui::Selectable(easeLinearStr.c_str()))
			{
				mAccelerateEasing = easeLinearStr;
				mAccelerateEasingID = 0;
				mAccelerateFn = EaseLinear;
				isChanged = true;
			}
			if (ImGui::Selectable(easeInStr.c_str()))
			{
				mAccelerateEasing = easeInStr;
				mAccelerateEasingID = 1;
				mAccelerateFn = EaseInQuad;
				isChanged = true;
			}
			if (ImGui::Selectable(easeOutStr.c_str()))
			{
				mAccelerateEasing = easeOutStr;
				mAccelerateEasingID = 2;
				mAccelerateFn = EaseOutQuad;
				isChanged = true;
			}
			if (ImGui::Selectable(easeInOutStr.c_str()))
			{
				mAccelerateEasing = easeInOutStr;
				mAccelerateEasingID = 3;
				mAccelerateFn = EaseInOutQuad;
				isChanged = true;
			}

			ImGui::EndCombo();
		}

		ImGui::NewLine();

		if (ImGui::InputFloat("Time To Stop", &mEndTime))
			isChanged = true;
		if (ImGui::BeginCombo((easeFnStr + "##1").c_str(), mDeccelerateEasing.c_str()))
		{
			if (ImGui::Selectable((easeLinearStr + "##1").c_str()))
			{
				mDeccelerateEasing = easeLinearStr;
				mDeccelerateEasingID = 0;
				mDeccelerateFn = EaseLinear;
				isChanged = true;
			}
			if (ImGui::Selectable((easeInStr + "##1").c_str()))
			{
				mDeccelerateEasing = easeInStr;
				mDeccelerateEasingID = 1;
				mDeccelerateFn = EaseInQuad;
				isChanged = true;
			}
			if (ImGui::Selectable((easeOutStr + "##1").c_str()))
			{
				mDeccelerateEasing = easeOutStr;
				mDeccelerateEasingID = 2;
				mDeccelerateFn = EaseOutQuad;
				isChanged = true;
			}
			if (ImGui::Selectable((easeInOutStr + "##1").c_str()))
			{
				mDeccelerateEasing = easeInOutStr;
				mDeccelerateEasingID = 3;
				mDeccelerateFn = EaseInOutQuad;
				isChanged = true;
			}

			ImGui::EndCombo();
		}


		return isChanged;
	}


	void BackgroundMover::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);

		value["Initial Speed"] >> mInitialSpeed;
		value["Max Speed"] >> mMaxSpeed;
		value["Time To Max Speed"] >> mTimeToMaxSpeed;
		value["Time To Stop"] >> mEndTime;
		value["Accelerate Easing ID"] >> mAccelerateEasingID;
		value["Deccelerate Easing ID"] >> mDeccelerateEasingID;


		if (mAccelerateEasingID == 0)
		{
			mAccelerateEasing = "EaseLinear";
			mAccelerateFn = EaseLinear;
		}
		else if (mAccelerateEasingID == 1)
		{
			mAccelerateEasing = "EaseInQuad";
			mAccelerateFn = EaseInQuad;
		}
		if (mAccelerateEasingID == 2)
		{
			mAccelerateEasing = "EaseOutQuad";
			mAccelerateFn = EaseOutQuad;
		}
		else if (mAccelerateEasingID == 3)
		{
			mAccelerateEasing = "EaseInOutQuad";
			mAccelerateFn = EaseInOutQuad;
		}


		if (mDeccelerateEasingID == 0)
		{
			mDeccelerateEasing = "EaseLinear";
			mDeccelerateFn = EaseLinear;
		}
		else if (mDeccelerateEasingID == 1)
		{
			mDeccelerateEasing = "EaseInQuad";
			mDeccelerateFn = EaseInQuad;
		}
		if (mDeccelerateEasingID == 2)
		{
			mDeccelerateEasing = "EaseOutQuad";
			mDeccelerateFn = EaseOutQuad;
		}
		else if (mDeccelerateEasingID == 3)
		{
			mDeccelerateEasing = "EaseInOutQuad";
			mDeccelerateFn = EaseInOutQuad;
		}


		mSpeed = mInitialSpeed;
	}

	void BackgroundMover::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);

		value["Initial Speed"] << mInitialSpeed;
		value["Max Speed"] << mMaxSpeed;
		value["Time To Max Speed"] << mTimeToMaxSpeed;
		value["Time To Stop"] << mEndTime;
		value["Accelerate Easing ID"] << mAccelerateEasingID;
		value["Deccelerate Easing ID"] << mDeccelerateEasingID;
	}

	void BackgroundMover::AccelerateBackground()
	{
		mStart = true;
	}

	void BackgroundMover::StopBackground()
	{
		mEnd = true;
	}

	void BackgroundMover::DecelerateBackground()
	{
		mGradualEnd = true;
	}
}