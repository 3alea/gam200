#pragma once

#include "Components\Logic.h"
#include "Player/Stats/PlayerStats.h"
#include "AI\Easing.h"

namespace AEX
{
	class BackgroundMover : public LogicComp
	{
		AEX_RTTI_DECL(BackgroundMover, LogicComp);

	public:

		virtual void Initialize();
		virtual void Update();
		virtual void Shutdown();

		bool OnGui();

		void FromJson(json & value);
		void ToJson(json & value);

		void AccelerateBackground();
		void StopBackground();
		void DecelerateBackground();

		PlayerStats* stats = nullptr;

		// Serializable
		float mInitialSpeed = 0.0f;
		float mTimeToMaxSpeed = 10.0f;
		float mMaxSpeed = 1.0f;
		f32 mEndTime = 10.0f;
		u32 mAccelerateEasingID = 0;
		u32 mDeccelerateEasingID = 0;


		

		// Not serializable
		float mTimer = 0.0f;
		bool mStart = false;
		float mSpeed = mInitialSpeed;

		bool mEnd = false;
		bool mGradualEnd = false;

		bool maxSpeedReached;

		f32 mEndTimer = 0.0f;

		std::string mAccelerateEasing;
		std::string mDeccelerateEasing;
		EaseFunc mAccelerateFn = EaseInQuad;
		EaseFunc mDeccelerateFn = EaseOutQuad;
	};
}