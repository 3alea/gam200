#pragma once
#include "./Components/Logic.h"
#include "Components/AEXTransformComp.h"
using namespace AEX;

// Forward declare SpineAnimation
class SpineAnimationComp;

class AlveolusLogic : public LogicComp
{
	AEX_RTTI_DECL(AlveolusLogic, LogicComp)

public:
	virtual void Initialize();
	virtual void Update();
	virtual void ShutDown();

	virtual bool OnGui();
	// FromJson specialized for this component
	virtual void FromJson(json & value);
	//// ToJson specialized for this component
	virtual void ToJson(json & value);
	//virtual void OnCollisionEvent(const CollisionEvent& collision);

	virtual void OnCollisionStarted(const CollisionStarted & collision);

	// This object's spine animation
	SpineAnimationComp* spineAnimation;

private:
	f32 mJumpForce;
};