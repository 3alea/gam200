#pragma once
#include "Components\Logic.h"
#include "Imgui\imgui.h"

using namespace AEX;

class Checkpoint : public LogicComp
{
	AEX_RTTI_DECL(Checkpoint, LogicComp);
public:
	// Start is called before the first frame update
	virtual void Initialize();

	// Update is called once per frame
	virtual void Update();

	// OnGui() function
	virtual bool OnGui();

	// Collision started event
	virtual void OnCollisionEvent(const CollisionEvent& collision);

	// FromJson specialized for this component
	virtual void FromJson(json & value);
	// ToJson specialized for this component
	virtual void ToJson(json & value);
};