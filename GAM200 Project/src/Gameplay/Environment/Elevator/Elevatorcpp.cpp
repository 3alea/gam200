#include "Elevator.h"
#include "Composition/AEXGameObject.h"
#include "Components/AEXTransformComp.h"
#include "Composition/AEXSpace.h"
#include "Player/Stats/PlayerStats.h"
#include "AI/Easing.h"
#include "Platform/AEXTime.h"
#include "Environment/FloatingPlatform/FloatingPlatformLogic.h"
#include "Player/Stats/PlayerStats.h"
#include "Components/Rigidbody.h"
//#include "components/ParticleEmitter.h"
//#include "CameraLogic/CameraShaker.h"
//#include "Resources/AEXResourceManager.h"
//#include "Audio/AudioManager.h"

namespace AEX
{
	void Elevator::Initialize()
	{
		initialized = false;
		timer = 0.0f;
		t = nullptr;
		fp = nullptr;
		t = mOwner->GetComp<TransformComp>();
		if (t)
			origin = t->mLocal.mTranslationZ.y;
		fp = mOwner->GetComp<FloatingPlatform>();
		firstDist = true;
	}
	void Elevator::Update()
	{
		if (!initialized)
		{
			if (!t || !fp)
			{
				Initialize();
				return;
			}
			else
			{
				initialized = true;
			}
		}

		if (done || !active)
			return;

		if (!doneWaiting)
		{
			timer += (f32)aexTime->GetFrameTime();

			if (timer > wait)
			{
				doneWaiting = true;
				timer = 0.0f;
				fp->active = false;
				origin = t->mLocal.mTranslationZ.y;
			}
			else
				return;
		}

		if (firstDist)
		{
			timer += (f32)aexTime->GetFrameTime();
			float tn;
		
			if (up)
			{
				tn = EaseInQuad(timer / time1);
				t->mLocal.mTranslationZ.y = origin + dist1 * tn;
			}
			else
			{
				tn = EaseInQuad(timer / time1);
				t->mLocal.mTranslationZ.y = origin - dist1 * tn;
			}
		
			// Finished
			if (timer >= time1)
			{
				if (up)
					origin = origin + dist1;
				else
					origin = origin - dist1;
		
				timer = 0.0f;
		
				firstDist = false;
			}
		}
		
		else
		{
			timer += (f32)aexTime->GetFrameTime();
		
			float tn = EaseOutQuad(timer / time2);
		
			if (up)
				t->mLocal.mTranslationZ.y = origin + dist2 * tn;
			else
				t->mLocal.mTranslationZ.y = origin - dist2 * tn;
		
			// Finished
			if (timer >= time2)
			{
				if (up)
					origin = origin + dist2;
				else
					origin = origin - dist2;
		
				timer = 0.0f;
				done = true;
				fp->active = true;
				fp->recalculatePaths();
				
				active = false;

				mOwner->GetParentSpace()->FindObject("player")->GetComp<PlayerStats>()->StopCinematic();
			}
		}
	}

	bool Elevator::OnGui()
	{
		bool changed = false;
		if (ImGui::InputFloat("dist1", &dist1))
		{
			changed = true;
		}
		if (ImGui::InputFloat("dist2", &dist2))
		{
			changed = true;
		}
		if (ImGui::InputFloat("time1", &time1))
		{
			changed = true;
		}
		if (ImGui::InputFloat("time2", &time2))
		{
			changed = true;
		}
		if (ImGui::InputFloat("wait", &wait))
		{
			changed = true;
		}
		if (ImGui::Checkbox("up", &up))
		{
			changed = true;
		}
		return changed;
	}
	void Elevator::OnCollisionStarted(const CollisionStarted & collision)
	{

		if (active || done)
			return;
		if (collision.otherObject->GetBaseNameString() == "foot")
		{
			active = true;
		}
	}
	void Elevator::OnCollisionEnded(const CollisionEnded & collision)
	{
		if (done && collision.otherObject->GetBaseNameString() == "player")
		{
			collision.otherObject->GetComp<PlayerStats>()->gravityDir = AEVec2(0,0);
			
			if (collision.otherObject->GetComp<Rigidbody>()->mVelocity.y < 0)
				collision.otherObject->GetComp<Rigidbody>()->mVelocity.y = 0;
		}
	}
	void Elevator::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);
		value["time1"] >> time1;
		value["time2"] >> time2;
		value["dist1"] >> dist1;
		value["dist2"] >> dist2;
		value["up"] >> up;
		value["wait"] >> wait;
	}
	void Elevator::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);
		value["time1"] << time1;
		value["time2"] << time2;
		value["dist1"] << dist1;
		value["dist2"] << dist2;
		value["up"] << up;
		value["wait"] << wait;
	}
}