#pragma once
#include "Components\Logic.h"

namespace AEX
{
	class TransformComp;
	class FloatingPlatform;

	class Elevator : public LogicComp
	{
		AEX_RTTI_DECL(Elevator, LogicComp);

	public:
		virtual void Initialize();

		// Update is called once per frame
		virtual void Update();

		// Points to the transform of the elevator
		TransformComp* t;
		FloatingPlatform* fp;

		float timer;
		float time1;
		float time2;
		float dist1;
		float dist2;
		float origin;
		float wait;

		bool doneWaiting = false;

		bool up;

		bool done = false;
		bool active = false;
		bool firstDist = true;
		bool initialized;

		// OnGui() function
		virtual bool OnGui();

		void OnCollisionStarted(const CollisionStarted& collision);
		void OnCollisionEnded(const CollisionEnded& collision);

		void FromJson(json & value);
		void ToJson(json & value);
	};
}