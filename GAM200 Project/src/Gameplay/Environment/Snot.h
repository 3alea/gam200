#pragma once

#include "./Components/Logic.h"
#include "Components/AEXTransformComp.h"

using namespace AEX;

class Snot : public LogicComp
{
	AEX_RTTI_DECL(Snot, LogicComp)

	virtual void Initialize();
	virtual void Update();
	virtual void ShutDown();

	void Reset();

	// FromJson specialized for this component
	//virtual void FromJson(json & value);
	//// ToJson specialized for this component
	//virtual void ToJson(json & value);
	//virtual void OnCollisionEvent(const CollisionEvent& collision);
	virtual bool OnGui() ;

	TransformComp * ownerTransform;
	TransformComp * PlayerTransform;

	f32 vertical_speed;
	f32 horizontal_speed; 

	bool prev_collision = false;
	bool on_reset = false;
	f32 timer;

	// OnCollision
	virtual void OnCollisionEvent(const CollisionEvent & collision);
	virtual void OnCollisionEnded(const CollisionEnded& collision);
};