#include "Alveolus.h"
#include "Composition/AEXScene.h"
#include "Components/Rigidbody.h"
#include "Components/SpineAnimation.h"

void AlveolusLogic::Initialize()
{
	spineAnimation = mOwner->GetComp<SpineAnimationComp>();
}

void AlveolusLogic::Update()
{
	if (!spineAnimation)
		Initialize();
}

void AlveolusLogic::ShutDown()
{
}

bool AlveolusLogic::OnGui()
{
	bool changed = false;

	if (ImGui::InputFloat("Jump Force", &mJumpForce, 0.1f, 0.2f, 3)) changed = true;

	return changed;
}

void AlveolusLogic::FromJson(json & value)
{
	LogicComp::FromJson(value);

	value["JumpForce"] >> mJumpForce;
}

void AlveolusLogic::ToJson(json & value)
{
	LogicComp::ToJson(value);

	value["JumpForce"] << mJumpForce;
}

void AlveolusLogic::OnCollisionStarted(const CollisionStarted & collision)
{
	GameObject* playerGO = aexScene->FindObjectInANYSpace("player");
	if (!strcmp(collision.otherObject->GetBaseName(), "foot") && playerGO->GetComp<Rigidbody>()->mVelocity.y <= 0)
	{
		f32 orientation = this->GetOwner()->GetComp<TransformComp>()->mLocal.mOrientation;
		AEVec2 jumpDir; jumpDir.FromAngle(PI/2.0f + orientation);
		playerGO->GetComp<Rigidbody>()->mVelocity = jumpDir * mJumpForce;
		spineAnimation->ChangeAnimation(0, "bote", false);
	}
}
