#include "Snot.h"
#include "src/Engine/Composition/AEXScene.h"
#include "src/Engine/Math/Collisions.h"
#include "src\Engine\Components\Rigidbody.h"
#include "src/Gameplay/Player/Movement/PlayerController.h"
#include "src/Engine/Platform/AEXTime.h"

void Snot::Initialize()
{
	ownerTransform = mOwner->GetComp<TransformComp>();
	vertical_speed = 13.5f;
	horizontal_speed = 0.055f;
	timer = 0.0f;
}

void Snot::Update()
{
	GameObject* playerGO = aexScene->FindObjectInANYSpace("player");
	PlayerTransform = playerGO->GetComp<TransformComp>();

	if (on_reset)
	{
		playerGO->GetComp<PlayerStats>()->dashChecker = false;
		playerGO->GetComp<PlayerStats>()->canDash = false;
		Reset();
	}

	if (on_reset && timer > 1.5f)
	{
		playerGO->GetComp<PlayerController>()->hSpeed = 0.1f;
		playerGO->GetComp<PlayerController>()->vSpeed = 20.0f;
		on_reset = false;
		playerGO->GetComp<PlayerController>()->unai = false;
		playerGO->GetComp<PlayerStats>()->dashChecker = true;
		timer = 0.0f;
	}
	//std::cout << aexScene->FindObjectInANYSpace("player")->GetComp<PlayerController>()->hSpeed;
}

void Snot::Reset()
{
	timer += (f32)aexTime->GetFrameTime();
	//aexScene->FindObjectInANYSpace("player")->GetComp<PlayerController>()->hSpeed += 0.00005f;
	//aexScene->FindObjectInANYSpace("player")->GetComp<PlayerController>()->vSpeed += 0.005f;

}

void Snot::ShutDown()
{

}

bool Snot::OnGui()
{
	ImGui::InputFloat("Verical speed", &vertical_speed);
	ImGui::InputFloat("Horizontal speed", &horizontal_speed);
	return false;
}

void Snot::OnCollisionEvent(const CollisionEvent & collision)
{
	//f32 kaktua = aexScene->FindObjectInANYSpace("player")->GetComp<PlayerController>()->vSpeed;
	//std::cout << kaktua;
	////aexScene->FindObjectInANYSpace("player")->GetComp<Rigidbody>()->
	if (!strcmp(collision.otherObject->GetBaseName(), "foot"))
	{
		GameObject* playerGO = aexScene->FindObjectInANYSpace("player");
		PlayerController* playerController = playerGO->GetComp<PlayerController>();
		playerController->unai = true;
		playerController->hSpeed = horizontal_speed;
		playerController->vSpeed = vertical_speed;
		prev_collision = true;
		timer = 0.0f;
		playerGO->GetComp<PlayerStats>()->dashChecker = false;
		playerGO->GetComp<PlayerStats>()->canDash = false;
	}
}

void Snot::OnCollisionEnded(const CollisionEnded & collision)
{
	if (!strcmp(collision.otherObject->GetBaseName(), "foot"))
	{
		if (prev_collision)
			on_reset = true;
		prev_collision = false;
	}
}
