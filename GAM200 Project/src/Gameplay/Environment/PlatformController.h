#pragma once
#include "Composition\AEXGameObject.h"
#include "Components/Rigidbody.h"
#include "Components\Logic.h"
#include "Imgui\imgui.h"

using namespace AEX;

class PlatformController : public LogicComp
{
	AEX_RTTI_DECL(PlatformController, LogicComp);
public:
	// RigidBody
	Rigidbody* rb;

	// Horizontal Speed
	float horizontalSpeed;

	// Vertical Speed
	float verticalSpeed;

	// Moving Time
	float moveTime;

	// Bools for the moving platforms of the acid (in order to activate them)
	bool isActivable = false;
	bool isMoving = false;

	// Bool to determine if the player has activated the moving platform
	bool activated = true;

	// Bool to determine direction
	bool switched;

	// Multiplyer depending on switched
	float direction = 1.f; // Not serialized

	// Bool to determine if the platform is idle
	bool isIdle = true;

	// Idle time before changing direction
	float idleTime;

	// Timer
	float timer;

	// Final velocity
	AEVec2 toAdd;

	// Start is called before the first frame update
	virtual void Initialize();

	// Update is called once per frame
	virtual void Update();

	// OnGui() function
	virtual bool OnGui();

	// OnBulletCollision
	virtual void OnCollisionEvent(const CollisionEvent & collision);
	virtual void OnCollisionEnded(const CollisionEnded& collision);

	// FromJson specialized for this component
	virtual void FromJson(json & value);
	// ToJson specialized for this component
	virtual void ToJson(json & value);
};