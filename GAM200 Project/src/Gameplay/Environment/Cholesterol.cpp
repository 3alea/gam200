#include <experimental/filesystem>
#include "Imgui/imgui.h"
#include "Composition/AEXSpace.h"
#include "Graphics\Texture.h"
#include "Cholesterol.h"

#include "Components\SpriteComps.h"
#include "Components\ParticleEmitter.h"
#include "Bullets\BulletLogic.h"
#include "Enemy/EnemyKamikaze.h"

#include "Platform\AEXTime.h"

using namespace AEX;
namespace fs = std::experimental::filesystem;

void CholesterolLogic::Initialize()
{
	mLives = mTotalLives;
	mFirstTex = aexResourceMgr->getResource<Texture>("data\\Textures\\colesterol\\Colesterol_Animation_01.png");
	mSecondTex = aexResourceMgr->getResource<Texture>("data\\Textures\\colesterol\\Colesterol_Animation_02.png");
	mThirdTex = aexResourceMgr->getResource<Texture>("data\\Textures\\colesterol\\Colesterol_Animation_03.png");
	mFourthTex = aexResourceMgr->getResource<Texture>("data\\Textures\\colesterol\\Colesterol_Animation_04.png");
}

void CholesterolLogic::Update()
{
	if (mParticleCurrentTime >= mParticleSpawnInterval)
		mOwner->GetComp<ParticleEmitter>()->mEmitProperties->mbParticlesEnabled = false;
	else
		mParticleCurrentTime += (f32)aexTime->GetFrameTime();
}

void CholesterolLogic::OnCollisionEvent(const CollisionEvent & collision)
{
	if (collision.otherObject && (collision.otherObject->GetComp<BulletLogic>() != nullptr || collision.otherObject->GetComp<EnemyKamikaze>() != nullptr))
	{
		if (collision.otherObject->GetComp<EnemyKamikaze>() != nullptr)
			aexScene->FindSpace("Main")->DestroyObject(collision.otherObject);

		mLives--;
		mOwner->GetComp<ParticleEmitter>()->mEmitProperties->mbParticlesEnabled = true;
		mParticleCurrentTime = 0.0f;

		//std::cout << "Lives : " << mLives << std::endl;

		if (mLives == 0)
		{
			if (mActivateCars)
			{
				aexScene->FindObjectInANYSpace(mCarName1)->GetComp<BlockedCar>()->Activate();
				aexScene->FindObjectInANYSpace(mCarName2)->GetComp<BlockedCar>()->Activate();
				aexScene->FindObjectInANYSpace(mCarName3)->GetComp<BlockedCar>()->Activate();
				aexScene->FindObjectInANYSpace(mCarName4)->GetComp<BlockedCar>()->Activate();
				aexScene->FindObjectInANYSpace(mCarName5)->GetComp<BlockedCar>()->Activate();
			}

			mOwner->RemoveComp(mOwner->GetComp<ParticleEmitter>(), true);
			mOwner->GetParentSpace()->DestroyObject(mOwner);
		}
		else
		{
			SpriteComp* currSprite = mOwner->GetComp<SpriteComp>();
			f32 percent = (f32)mLives / mTotalLives * 100.f;

			//std::cout << "Percent : " << percent << std::endl;

			if (percent < mToFourthPercent)
				currSprite->mTex = mFourthTex;
			else if (percent < mToThirdPercent)
				currSprite->mTex = mThirdTex;
			else if (percent < mToSecondPercent)
				currSprite->mTex = mSecondTex;
			else
				currSprite->mTex = mFirstTex;
		}
	}
}

#include <cstring>
void CholesterolLogic::FromJson(json & value)
{
	this->LogicComp::FromJson(value);

	value["ToSecondPercent"] >> mToSecondPercent;
	value["ToThirdPercent"] >> mToThirdPercent;
	value["ToFourthPercent"] >> mToFourthPercent;

	value["ActivateCars"] >> mActivateCars;

	std::string car1, car2, car3, car4, car5;
	value["Car1Name"] >> car1;
	value["Car2Name"] >> car2;
	value["Car3Name"] >> car3;
	value["Car4Name"] >> car4;
	value["Car5Name"] >> car5;
	strcpy_s(mCarName1, car1.c_str());
	strcpy_s(mCarName2, car2.c_str());
	strcpy_s(mCarName3, car3.c_str());
	strcpy_s(mCarName4, car4.c_str());
	strcpy_s(mCarName5, car5.c_str());


	value["Lives"] >> mTotalLives;
}

void CholesterolLogic::ToJson(json & value)
{
	this->LogicComp::ToJson(value);

	value["ActivateCars"] << mActivateCars;

	std::string car1(mCarName1);
	std::string car2(mCarName2);
	std::string car3(mCarName3);
	std::string car4(mCarName4);
	std::string car5(mCarName5);
	value["Car1Name"] << car1;
	value["Car2Name"] << car2;
	value["Car3Name"] << car3;
	value["Car4Name"] << car4;
	value["Car5Name"] << car5;

	value["ToSecondPercent"] << mToSecondPercent;
	value["ToThirdPercent"] << mToThirdPercent;
	value["ToFourthPercent"] << mToFourthPercent;

	value["Lives"] << mTotalLives;
}

bool CholesterolLogic::OnGui()
{
	bool changed = false;

	ImGui::InputInt("Cholesterol lives", &mTotalLives, 1, 2);

	ImGui::Checkbox("Activate cars", &mActivateCars);

	if (mActivateCars)
	{
		ImGui::InputText("car1", mCarName1, 10);
		ImGui::InputText("car2", mCarName2, 10);
		ImGui::InputText("car3", mCarName3, 10);
		ImGui::InputText("car4", mCarName4, 10);
		ImGui::InputText("car5", mCarName5, 10);
	}

	ImGui::SliderInt("ToSecondPercent", &mToSecondPercent, 0, 100);
	ImGui::SliderInt("ToThirdPercent", &mToThirdPercent, 0, 100);
	ImGui::SliderInt("ToFourthPercent", &mToFourthPercent, 0, 100);

	return changed;
}
