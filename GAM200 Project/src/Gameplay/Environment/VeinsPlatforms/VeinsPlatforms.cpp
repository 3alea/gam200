#include "VeinsPlatforms.h"
#include "Composition\AEXGameObject.h"
#include "Platform\AEXTime.h"
#include "Imgui\imgui.h"
#include "Composition/AEXSpace.h"
#include <time.h>
#include "Components/ParticleEmitter.h"
#include "Environment/FloatingPlatform/FloatingPlatformLogic.h"
#include "Components/Collider.h"
#include "Player/Movement/PlayerController.h"
#include "Platform/AEXInput.h"
#include "Audio/AudioManager.h"
#include "Resources/AEXResourceManager.h"
#include "Resources/AEXResourceManager.h"
#include "Audio/AudioManager.h"
#include "Utilities/RNG.h"
namespace AEX
{
	VeinsPlatforms::VeinsPlatforms()
		: introductionPlatform(false)
		, idle(true)
		, accelerating(true)
		, introduced(false)
		, wait(true)
		, stats(nullptr)
	{

	}

	void VeinsPlatforms::Initialize()
	{
		doneOnce = false;
		timer = 0;
		easedTime = 0;

		wait = true;
		playedOnce = false;

		platformT = mOwner->GetComp<TransformComp>();
		float finalX = platformT->mLocal.mTranslationZ.x;

		canDoubleJump = nullptr;
		anim = nullptr;

		randTime = f32(rand() % 5);

		if (mOwner->GetBaseNameString() == "platform")
			randTime = 0;

		GameObject* go = mOwner->GetParentSpace()->FindObject("player");

		GameObject* go2 = mOwner->GetParentSpace()->FindObject("overlay");

		if (go2)
		{
			bgMover = go2->GetComp<BackgroundMover>();
		}

		anim = mOwner->GetComp<SpineAnimationComp>();

		if (go)
		{
			stats = go->GetComp<PlayerStats>();
			if (PlayerController* g = go->GetComp<PlayerController>())
			{
				canDoubleJump = &g->canDoubleJump;
			}
		}

		if (platformT)
		{
			//////////////////////////IDLE///////////////////////////////////
			acceleratePath.Push(0, AEVec2(finalX - movingRange, 0));
			acceleratePath.Push(1, AEVec2(finalX + movingRange, 0));
			//////////////////////////IDLE///////////////////////////////////
		}

		if (introductionPlatform)
		{
			idle = false;
			introduced = false;

			if (platformT)
			{

				introductionPath.Push(0, AEVec2(finalX + 50, 0));
				introductionPath.Push(1, AEVec2(finalX - movingRange, 0));
			}
		}

		else
		{
			idle = true;
		}

		mySteam = nullptr;

		// Find the steam particle emitter
		if (GameObject* smoke = mOwner->GetParentSpace()->FindObject(steamName))
		{
			mySteam = smoke->GetComp<ParticleEmitter>();
		}

		failTimer = 0.0f;
		failing = false;
		engineTrouble = false;
		broken = false;
		initBroken = false;
		fallingTimer = 0.0f;

		isGhost = false;

		deactivated = false;
		broken = false;
	}

	void VeinsPlatforms::Update()
	{
		if (!platformT || !stats || !bgMover)
		{
			Initialize();
			return;
		}

		if (failingPlatform && (!mySteam || !canDoubleJump || !anim))
		{
			Initialize();
			return;
		}

		if (failingPlatform)
		{
			if (deactivated)
				return;

			// Car starts falling and shifting slightly to the left
			if (broken)
			{
				// Do this only once, when the car breaks
				if (!initBroken)
				{
					// Stop floating
					mOwner->RemoveComp(mOwner->GetComp<FloatingPlatform>());

					// Get the position where the car has stoped working
					breakPos.x = platformT->mLocal.mTranslationZ.x;
					breakPos.y = platformT->mLocal.mTranslationZ.y;

					// Calculate the destination
					destination.x = platformT->mLocal.mTranslationZ.x + destination.x;
					destination.y = platformT->mLocal.mTranslationZ.y + destination.y;

					xDiff = 0;
					// Do this only once
					initBroken = true;
				}

				// Update the falling timer
				fallingTimer += (f32)aexTime->GetFrameTime();

				float easedTimer = EaseInQuad(fallingTimer / timeToFall);

				// Move the car down and to the left
				platformT->mLocal.mTranslationZ.y = breakPos.y + (destination.y - breakPos.y) * easedTimer;
				platformT->mLocal.mTranslationZ.x = breakPos.x + (destination.x - breakPos.x) * easedTimer;

				anim->carRotation = 0 - 90 * easedTimer;

				// Make the collider ghost
				if (!isGhost)
				{
					*canDoubleJump = true;
					if (fallingTimer >= 1.0f || PLAYER_JUMPING)
					{
						mOwner->GetComp<ICollider>()->mIsGhost = true;
						isGhost = true;
					}
				}

				// Finished falling. Keep deactivated until the end of the level
				if (fallingTimer >= timeToFall)
				{
					deactivated = true;
				}
				return;
			}
		}

		if (introductionPlatform)
		{
			if (!bgMover->maxSpeedReached)
			{
				if (doneOnce == false)
				{
					platformT->mLocal.mTranslationZ.x += 100;
					doneOnce = true;
					timer = 0;
				}
				return;
			}
		}

		// Random time to avoid the platforms moving exactly the same
		if (!introductionPlatform && doneOnce == false)
		{
			timer += (f32)aexTime->GetFrameTime();
			if (timer >= randTime)
			{

				doneOnce = true;
			}
			else
				return;
		}
		
		// Wait for some time between accelerating and decelerating
		if (wait)
		{
			timer += (f32)aexTime->GetFrameTime();

			if (idle)
			{
				if (timer >= 0.5)
				{
					wait = false;
					timer = 0;
				}
				else
					return;
			}
			if (timer >= timeToIntroduce)
			{
				wait = false;
				timer = 0;
			}
			else
				return;
		}

		if (introductionPlatform && !introduced)
			Introduce();

		if (idle)
		{
			Idle();
		}

		// Start timer to fail
		if (failing)
		{
			failTimer += (f32)aexTime->GetFrameTime();

			if (failTimer >= timeToFail)
			{
				engineTrouble = true;
				failing = false;
				failTimer = 0;
				smokeTimer = 0;
				mySteam->mEmitProperties->mbParticlesEnabled = false;
				smokeVaryingTime = timeWithNoSmoke.x;
			}
		}

		// Display the smoke blink feedback
		if (engineTrouble)
		{
			failTimer += (f32)aexTime->GetFrameTime();
			smokeTimer += (f32)aexTime->GetFrameTime();

			if (smokeTimer >= smokeVaryingTime)
			{
				if (smokeVaryingTime == timeWithNoSmoke.x)
					smokeVaryingTime = timeWithNoSmoke.y;
				else
					smokeVaryingTime = timeWithNoSmoke.x;

				smokeTimer = 0;
				mySteam->mEmitProperties->mbParticlesEnabled = !mySteam->mEmitProperties->mbParticlesEnabled;
			}

			if (failTimer >= troubleTime)
			{
				smokeTimer = 0;
				engineTrouble = false;
				mySteam->mEmitProperties->mbParticlesEnabled = false;
				broken = true;
			}
		}
	}

	void VeinsPlatforms::Idle()
	{
		if (accelerating)
		{
			// Update timer, ease the time, update the position of the platform
			easedTime = EaseInOutQuad(timer / timeToAccelerate);
			prev = platformT->mLocal.mTranslationZ.x;
			platformT->mLocal.mTranslationZ.x = acceleratePath.SampleTime(easedTime).x;
			xDiff = platformT->mLocal.mTranslationZ.x - prev;
			timer += (f32)aexTime->GetFrameTime();

			// Check the idle going down timer
			if (timer >= timeToAccelerate)
			{
				timer = 0;
				wait = true;
				xDiff = 0;

				// Now the platform goes up
				accelerating = false;
			}
		}

		else
		{
			// Update timer, ease the time, update the position of the platform
			easedTime = EaseInOutQuad(timer / timeToDecelerate);
			prev = platformT->mLocal.mTranslationZ.x;

			platformT->mLocal.mTranslationZ.x = acceleratePath.SampleTime(1.0f - easedTime).x;
			xDiff = platformT->mLocal.mTranslationZ.x - prev;

			timer += (f32)aexTime->GetFrameTime();

			// Check the idle going down timer
			if (timer >= timeToDecelerate)
			{
				timer = 0;
				xDiff = 0;

				wait = true;
				// Now the platform goes up
				accelerating = true;
			}
		}
	}

	void VeinsPlatforms::Introduce()
	{
		// Update timer, ease the time, update the position of the platform
		easedTime = EaseInOutQuad(timer / 1);
		platformT->mLocal.mTranslationZ.x = introductionPath.SampleTime(easedTime).x;
		timer += (f32)aexTime->GetFrameTime();

		if (timer >= 0.5 && !playedOnce)
		{
			float rand = RNG->GetFloat(0,3);
			if (rand == 0)
				audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\car level\\fastCar1.mp3"), Voice::SFX);
			else if (rand == 2)
				audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\car level\\fastCar2.mp3"), Voice::SFX);
			else
				audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\car level\\fastCar3.mp3"), Voice::SFX);

			
			playedOnce = true;
		}

		// Check the idle going down timer
		if (timer >= 1)
		{
			timer = 0;
			introduced = true;
			idle = true;
		}
	}

	void VeinsPlatforms::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);
		value["back speed"] >> backSpeed;
		value["moving range"] >> movingRange;
		value["accelerate time"] >> timeToAccelerate;
		value["decelerate time"] >> timeToDecelerate;
		value["introduction platform"] >> introductionPlatform;
		value["time to introduce"] >> timeToIntroduce;
		std::string name;
		value["smoke name"] >> name;
		strcpy_s(steamName, name.c_str());
		value["time to fail"] >> timeToFail;
		value["engine trounle"] >> troubleTime;
		value["no smoke time"] >> timeWithNoSmoke;
		value["falling time"] >> timeToFall;
		value["falling rotation"] >> rotateWhenFalling;
		value["destination fall"] >> destination;
		value["should fall"] >> failingPlatform;
	}

	void VeinsPlatforms::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);

		value["back speed"] << backSpeed;
		value["moving range"] << movingRange;
		value["accelerate time"] << timeToAccelerate;
		value["decelerate time"] << timeToDecelerate;
		value["introduction platform"] << introductionPlatform;
		value["time to introduce"] << timeToIntroduce;
		
		std::string name = steamName;
		value["smoke name"] << name;

		value["time to fail"] << timeToFail;
		value["engine trounle"] << troubleTime;
		value["no smoke time"] << timeWithNoSmoke;
		value["falling time"] << timeToFall;
		value["falling rotation"] << rotateWhenFalling;
		value["destination fall"] << destination;
		value["should fall"] << failingPlatform;

	}
	bool VeinsPlatforms::OnGui()
	{
		bool changed = false;

		ImGui::Text("name of the smoke's particle emitter");
		if (ImGui::InputText("name", steamName, IM_ARRAYSIZE(steamName)))
		{
			Initialize();
			changed = true;
		}

		ImGui::Text("Back Speed");
		if (ImGui::InputFloat("back speed", &backSpeed))
		{
			changed = true;
		}

		ImGui::Text("moving range");
		if (ImGui::InputFloat("moving range", &movingRange))
		{
			changed = true;
		}

		ImGui::Text("accelerate time");
		if (ImGui::InputFloat("Accelerate time", &timeToAccelerate))
		{
			changed = true;
		}

		ImGui::Text("decelerate time");
		if (ImGui::InputFloat("Decelerate Time", &timeToDecelerate))
		{
			changed = true;
		}

		ImGui::Text("introduction platform");
		if (ImGui::Checkbox("Intro Platform", &introductionPlatform))
		{
			changed = true;
		}
		ImGui::Text("time to intro");
		if (ImGui::InputFloat("Intro time", &timeToIntroduce))
		{
			changed = true;
		}

		ImGui::Text("failing platform");
		if (ImGui::Checkbox("platfor should fail", &failingPlatform))
		{
			changed = true;
		}

		if (failingPlatform)
		{
			ImGui::Text("time to fail");
			if (ImGui::InputFloat("activates on collision with player", &timeToFail))
			{
				changed = true;
			}

			ImGui::Text("smoke failing time");
			if (ImGui::InputFloat("smoke feedback time", &troubleTime))
			{
				changed = true;
			}

			ImGui::Text("smoke interval");
			if (ImGui::InputFloat2("smoke feedback interval", timeWithNoSmoke.v))
			{
				changed = true;
			}

			ImGui::Text("time to fall");
			if (ImGui::InputFloat("time it takes to fall", &timeToFall))
			{
				changed = true;
			}

			ImGui::Text("Where to fall");
			if (ImGui::InputFloat2("destination", destination.v))
			{
				changed = true;
			}

			
		}return changed;
	}
	void VeinsPlatforms::OnCollisionEvent(const CollisionEvent & collision)
	{
		if (collision.otherObject->GetBaseNameString() == "foot")
		{
			stats->xDiff = xDiff;
			//std::cout << xDiff << std::endl;
		}
	}
	void VeinsPlatforms::OnCollisionEnded(const CollisionEnded & collision)
	{
		if (collision.otherObject->GetBaseNameString() == "foot")
		{
			stats->xDiff = 0;
		}
	}

	void VeinsPlatforms::OnCollisionStarted(const CollisionStarted & collision)
	{
		if (collision.otherObject->GetBaseNameString() == "foot")
		{
			// Platform has to fail and has to start giving feedback
			if (failingPlatform && !failing && !engineTrouble)
			{
				failing = true;
				failTimer = 0.0f;
			}

			audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\car level\\carBump.mp3"), Voice::SFX)->SetVolume(0.3f);
		}
	}
		
}
