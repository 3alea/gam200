#pragma once
#include "Components\Logic.h"
#include "AI/Path2D.h"
#include "Components\AEXTransformComp.h"
#include "Player/Stats/PlayerStats.h"
#include "Environment\Background\BackgroundMover.h"
#include "Components/SpineAnimation.h"
class ParticleEmitter;
class SpineAnimationComp;

namespace AEX
{
	class VeinsPlatforms : public LogicComp
	{
		AEX_RTTI_DECL(VeinsPlatforms, LogicComp);

	public:
		VeinsPlatforms();

		virtual void Initialize();
		virtual void Update();
		void Idle();
		void Introduce();

		TransformComp* platformT;

		AEVec2 idlePos;

		PlayerStats* stats;

		///////////////////////////Falling car///////////////////////
		BackgroundMover* bgMover;
		ParticleEmitter* mySteam;
		char steamName[256];

		bool failingPlatform;

		// True when the car starts to fail
		bool failing;

		// True when the smoke starts to blink
		bool engineTrouble;

		bool broken;
		bool initBroken;

		bool rotateWhenFalling;

		float timeToFall;
		float fallingTimer;

		bool isGhost;

		bool* canDoubleJump;

		SpineAnimationComp* anim;

		float deactivated;

		// position when the car starts to fall
		AEVec2 breakPos;
		AEVec2 destination;

		float timeToFail;
		float failTimer;
		float troubleTime;
		float smokeTimer;
		float smokeVaryingTime;
		AEVec2 timeWithNoSmoke;
		///////////////////////////Falling car///////////////////////

		float randTime;

		// FromJson specialized for this component
		virtual void FromJson(json & value);
		// ToJson specialized for this component
		virtual void ToJson(json & value);

		virtual bool OnGui();

		float timeToAppear;
		float timeToDisappear;
		float backSpeed;
		float movingRange;

		bool doneOnce;

		bool playedOnce;

		bool accelerating;

		// Special platform
		bool introductionPlatform;
		bool wait;
		bool introduced;
		float timeToIntroduce;
		Path2D introductionPath;

		// States
		bool idle;


		float timer;
		float easedTime;

		float timeToAccelerate;
		float timeToDecelerate;

		virtual void OnCollisionEvent(const CollisionEvent& collision);
		void OnCollisionEnded(const CollisionEnded& collision);
		virtual void OnCollisionStarted(const CollisionStarted& collision);

		float xDiff;
		float prev;

		Path2D acceleratePath;
		Path2D appearPath;
	};
}