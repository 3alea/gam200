#pragma once
#include "Components\Logic.h"
#include "Imgui\imgui.h"
#include "Components\SpriteComps.h"

using namespace AEX;

class AEX::GameObject;
class AEX::Space;
class AEX::TransformComp;

class LevelLoader : public LogicComp
{
	AEX_RTTI_DECL(LevelLoader, LogicComp);
public:
	// Start is called before the first frame update
	virtual void Initialize();

	// Update is called once per frame
	virtual void Update();

	// Char pre-converter
	char level[32] = "";

	// Variable storing the name of the level to load
	std::string levelName;

	// OnGui() function
	virtual bool OnGui();

	// Collision started event
	void OnCollisionStarted(const CollisionStarted& collision);

	// FromJson specialized for this component
	virtual void FromJson(json & value);
	// ToJson specialized for this component
	virtual void ToJson(json & value);

	// To be serialized
	bool mUseFadeInOut = true;

	// Not to be serialized
	bool mCollidedWithPlayer = false;
	GameObject * player = nullptr;
	Space * main = nullptr;
	GameObject * foreground = nullptr;
	TransformComp * tr = nullptr;
	SpriteComp * sprite = nullptr;
};