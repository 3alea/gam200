#pragma once
#include "Components/Logic.h"
#include "Components\AEXTransformComp.h"
using namespace AEX;

struct BoxSpawner : public LogicComp
{
	AEX_RTTI_DECL(BoxSpawner, LogicComp)

	virtual void Initialize();
	virtual void Update();

	void SpawnBox();

	// FromJson specialized for this component
	virtual void FromJson(json & value);
	// ToJson specialized for this component
	virtual void ToJson(json & value);

	virtual bool OnGui();
private:
	f32 time_to_spawn;
	f32 timer;
	TransformComp* owner_transform;
};
