#include "Box.h"
#include "Resources/AEXResourceManager.h"
#include "Composition\AEXGameObject.h"
#include "Components/Collider.h"
#include "Composition/AEXScene.h"

void Box::Initialize() {}

void Box::Update() {}

void Box::OnCollisionStarted(const CollisionStarted & collision)
{
	if (collision.otherObject->GetComp<ICollider>()->collisionGroup == "Acid")
		mOwner->GetParentSpace()->DestroyObject(mOwner);
}

void Box::ToJson(json & value)
{
	this->IComp::ToJson(value);
}

void Box::FromJson(json & value)
{
	this->IComp::FromJson(value);
}