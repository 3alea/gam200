#include "BoxSpawner.h"
#include "src/Engine/Composition/AEXComposition.h"
#include "src/Engine/Composition/AEXSpace.h"
#include "src/Engine/Platform/AEXTime.h"
#include "src/Engine/Imgui/imgui.h"
#include "src\Engine\Math/Collisions.h"
#include "src/Engine/Composition/AEXScene.h"
#include "Player/Stats/PlayerStats.h"
#include "src/Engine/Components/Collider.h"
#include "src/Engine/Components/SpriteComps.h"


void BoxSpawner::Initialize()
{
	owner_transform = GetOwner()->GetComp<TransformComp>();
	timer = time_to_spawn;    
}

void BoxSpawner::Update()
{
	if (timer >= time_to_spawn)
		SpawnBox();
	timer += (f32)aexTime->GetFrameTime();
}

void BoxSpawner::SpawnBox()
{
	GameObject* newBox = mOwner->GetParentSpace()->InstantiateArchetype("box", owner_transform->mLocal.mTranslationZ);
	timer = 0.0f;
}

bool BoxSpawner::OnGui()
{
	if (ImGui::InputFloat("Spawn time", &time_to_spawn)) { return true; }
	return false;
}

void BoxSpawner::FromJson(json & value)
{
	LogicComp::FromJson(value);
	value["time_to_spawn"] >> time_to_spawn;
}

void BoxSpawner::ToJson(json & value)
{
	LogicComp::ToJson(value);
	value["time_to_spawn"] << time_to_spawn;
}
