#pragma once
#include "Components/Logic.h"
using namespace AEX;

struct Box : public LogicComp
{
	AEX_RTTI_DECL(Box, LogicComp)

	virtual void Initialize();
	virtual void Update();

	virtual void OnCollisionStarted(const CollisionStarted & collision);

	void FromJson(json & value);
	void ToJson(json & value);
};