#include "Composition/AEXScene.h"
#include "Platform/AEXTime.h"
#include "PlatformController.h"
#include <iostream>

using namespace AEX;
using namespace ImGui;

void PlatformController::Initialize()
{
	rb = GetOwner()->GetComp<Rigidbody>();
	isIdle = true;
	// This is so that the platform is activated as soon as it is turned on
	timer = idleTime - (f32)aexTime->GetFrameTime();
}

void PlatformController::Update()
{
	if (!rb)
		Initialize();

	// Add to timer if the player has landed on the platform
	if (activated)
	{
		timer += (f32)aexTime->GetFrameTime();
	}
	
	if (isActivable && !isMoving && !switched && isIdle)
	{
		timer = (timer >= idleTime) ? idleTime - EPSILON : timer;
		//std::cout << timer << std::endl;
	}

	// Be idle if idle
	if (isIdle)
	{
		// Set velocity to 0
		rb->mVelocity = AEVec2{ 0.f, 0.f };

		if (timer >= idleTime)
		{
			isIdle = false;
			switched = !switched;
			timer = 0.f;
		}
	}
	else
	{
		// Move corresponding to direction
		rb->mVelocity = AEVec2(horizontalSpeed * direction, verticalSpeed * direction);

		if (timer >= moveTime)
		{
			isIdle = true;
			timer = 0.f;
		}
	}

	if (switched)
		direction = -1.f;
	else
		direction = 1.f;

	//std::cout << timer << std::endl;
}

bool PlatformController::OnGui()
{
	// Collapsing header - when clicked, opens up to show the rest of edited variables
	if (ImGui::CollapsingHeader("PlatformController"))
	{
		// isChanged boolean to return if something has changed
		bool isChanged = false;

		// Horizontal speed
		ImGui::Text("Is Acid Moving Platform");
		if (ImGui::Checkbox("Is Acid Moving Platform", &isActivable)) isChanged = true;

		// Horizontal speed
		ImGui::Text("Horizontal Speed");
		if (ImGui::InputFloat("Horizontal Speed", &horizontalSpeed)) isChanged = true;

		// Vertical speed
		ImGui::Text("Vertical Speed");
		if (ImGui::InputFloat("Vertical Speed", &verticalSpeed)) isChanged = true;

		// Move time
		ImGui::Text("Move Time");
		if (ImGui::InputFloat("Move Time", &moveTime)) isChanged = true;

		// Idle time
		ImGui::Text("Idle Time");
		if (ImGui::InputFloat("Idle Time", &idleTime)) isChanged = true;

		return isChanged;
	}

	return false;
}

void PlatformController::OnCollisionEvent(const CollisionEvent & collision)
{
	if (!strcmp(collision.otherObject->GetBaseName(), "foot") && isActivable)
		isMoving = true;
}

void PlatformController::OnCollisionEnded(const CollisionEnded & collision)
{
	if (!strcmp(collision.otherObject->GetBaseName(), "foot") && isActivable)
		isMoving = false;
}

// FromJson specialized for this component
void PlatformController::FromJson(json & value)
{
	this->IComp::FromJson(value);

	value["isActivable"] >> isActivable;
	value["horizontalSpeed"] >> horizontalSpeed;
	value["verticalSpeed"] >> verticalSpeed;
	value["moveTime"] >> moveTime;
	value["idleTime"] >> idleTime;
}

// ToJson specialized for this component
void PlatformController::ToJson(json & value)
{
	this->IComp::ToJson(value);

	value["isActivable"] << isActivable;
	value["horizontalSpeed"] << horizontalSpeed;
	value["verticalSpeed"] << verticalSpeed;
	value["moveTime"] << moveTime;
	value["idleTime"] << idleTime;
}