#pragma once
#include "./Components/Logic.h"
#include "Components/AEXTransformComp.h"
#include "Components/Collider.h"

using namespace AEX;

class PlatformOnAboveActivator : public LogicComp
{
	AEX_RTTI_DECL(PlatformOnAboveActivator, LogicComp);

public:
	virtual void Initialize();
	virtual void Update();
	virtual void Shutdown();

	void FromJson(json & value);
	void ToJson(json & value);

private:
	TransformComp*  footTr = nullptr;
	TransformComp* tr = nullptr;
	BoxCollider*  col = nullptr;
};