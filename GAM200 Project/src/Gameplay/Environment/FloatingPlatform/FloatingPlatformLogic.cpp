#include "FloatingPlatformLogic.h"
#include "Components/AEXTransformComp.h"
#include "Imgui\imgui.h"
#include "Components/Rigidbody.h"
#include "AI/Easing.h"
#include <iostream>
#include "Player/Movement/PlayerController.h"
#include "Platform/AEXInput.h"
#include "Composition\AEXSpace.h"
#include <stdlib.h>

namespace AEX
{
	FloatingPlatform::FloatingPlatform()
		: idle(true)
		, timer(0)
		, goingDown(true)
		, IdleDownTime(0)
		, IdleUpTime(0)
		, mIdlRange(0)
		, sinking(false)
		, calculatedPathOnce(false)
		, sinkRange(0)
		, sinkTime(0)
		, sinkedRange(0)
		, sinkedUpTime(0)
		, sinkedDownTime(0)
		, recoil(false)
		, recoilRange(0)
		, recoilTime(0)
		, playerOnPlatform(false)
		, rising(false)
		, stats(nullptr)
	{}

	void FloatingPlatform::Initialize()
	{
		active = true;
		timer = 0;
		idle = true;
		sinking = false;
		sinked = false;
		recoil = false;
		playerOnPlatform = false;
		doNotStart = true;
		doNotStartTimer = 0.0f;
		doNotStartTime = f32(rand() % 5);

		if (mOwner->GetBaseNameString() == "platform")
			doNotStartTime = 0;

		platformT = mOwner->GetComp<TransformComp>();

		GameObject* go = mOwner->GetParentSpace()->FindObject("player");

		if (go)
			stats = go->GetComp<PlayerStats>();

		if (platformT)
		{
			platformIdlePos = platformT->mLocal.mTranslationZ.y;
			platformSinkedPos = platformIdlePos - sinkRange;

			//////////////////////////IDLE///////////////////////////////////
			idlePathDown.Push(0, AEVec2(0, platformIdlePos + mIdlRange));
			idlePathDown.Push(1, AEVec2(0, platformIdlePos - mIdlRange));

			idlePathUp.Push(0, AEVec2(0, platformIdlePos - mIdlRange));
			idlePathUp.Push(1, AEVec2(0, platformIdlePos + mIdlRange));
			//////////////////////////IDLE///////////////////////////////////

			/////////////////////////SINKED//////////////////////////////////
			sinkedPathUp.Push(0, AEVec2(0, platformSinkedPos - sinkedRange));
			sinkedPathUp.Push(1, AEVec2(0, platformSinkedPos + sinkedRange));

			sinkedPathDown.Push(0, AEVec2(0, platformSinkedPos + sinkedRange));
			sinkedPathDown.Push(1, AEVec2(0, platformSinkedPos - sinkedRange));
			/////////////////////////SINKED//////////////////////////////////


			//SM_Init();
		}
	}
	
	void FloatingPlatform::Update()
	{
		if (!platformT || !stats)
			Initialize();

		if (doNotStart)
		{
			doNotStartTimer += (f32)aexTime->GetFrameTime();

			if (doNotStartTimer >= doNotStartTime)
			{
				doNotStart = false;
			}
			else
				return;
		}

		if (!active)
			return;

		// Check if the player jumps out of the platform
		if (playerOnPlatform && PLAYER_JUMPING && !stats->onCinematic)
		{
			recoil = true;
			idle = false;
			sinking = false;
			sinked = false;
			calculatedPathOnce = false;
			timer = 0;
			playerOnPlatform = false;
			//std::cout << "RECOIL" << std::endl;
		}

		/////////////////////////IDLE////////////////////////
		if (idle)
		{
			// GOING DOWN
			if (goingDown == true)
			{
				// Update timer, ease the time, update the position of the platform
				easedTime = EaseInOutQuad(timer / IdleDownTime);
				platformT->mLocal.mTranslationZ.y = idlePathDown.SampleTime(easedTime).y;
				timer += (f32)aexTime->GetFrameTime();

				// Check the idle going down timer
				if (timer >= IdleDownTime)
				{
					timer = 0;

					// Now the platform goes up
					goingDown = false;
				}
			}

			// GOING UP
			// Update timer, ease the time, update the position of the platform
			else
			{
				easedTime = EaseInOutQuad(timer / IdleUpTime);
				platformT->mLocal.mTranslationZ.y = idlePathUp.SampleTime(easedTime).y;
				timer += (f32)aexTime->GetFrameTime();

				// Check the idle going up timer
				if (timer >= IdleUpTime)
				{
					timer = 0;

					// Now the platform goes down
					goingDown = true;
				}
			}

			return; // End the "State"
		}
		/////////////////////////IDLE////////////////////////

		///////////////////////SINKING///////////////////////
		if (sinking)
		{
			// Calculate the sink path
			if (calculatedPathOnce == false)
			{
				sinkPath.Clear();
				// Path goes from current position to sinked posision
				sinkPath.Push(0, AEVec2(0, platformT->mLocal.mTranslationZ.y));
				sinkPath.Push(1, AEVec2(0, platformSinkedPos - sinkedRange));
				calculatedPathOnce = true;
			}

			// Ease the timer
			easedTime = EaseOutQuad(timer / sinkTime);

			// Move the platform in the y axis
			platformT->mLocal.mTranslationZ.y = sinkPath.SampleTime(easedTime).y;

			// Update the timer
			timer += (f32)aexTime->GetFrameTime();

			// Check if the platform has reached the end of the path
			if (timer >= sinkTime)
			{
				timer = 0;

				sinking = false;
				sinked = true;
				calculatedPathOnce = false;
				goingDown = false;
				//std::cout << "SINKED" << std::endl;
			}

			return; // End the "State"
		}
		///////////////////////SINKING///////////////////////

		////////////////////////SINKED///////////////////////
		if (sinked)
		{
			// GOING DOWN
			if (goingDown == true)
			{
				// Update timer, ease the time, update the position of the platform
				easedTime = EaseInOutQuad(timer / sinkedDownTime);
				platformT->mLocal.mTranslationZ.y = sinkedPathDown.SampleTime(easedTime).y;
				timer += (f32)aexTime->GetFrameTime();

				// Check the idle going down timer
				if (timer >= sinkedDownTime)
				{
					timer = 0;

					// Now the platform goes up
					goingDown = false;
				}
			}

			// GOING UP
			// Update timer, ease the time, update the position of the platform
			else
			{
				easedTime = EaseInOutQuad(timer / sinkedUpTime);
				platformT->mLocal.mTranslationZ.y = sinkedPathUp.SampleTime(easedTime).y;
				timer += (f32)aexTime->GetFrameTime();

				// Check the idle going up timer
				if (timer >= sinkedUpTime)
				{
					timer = 0;

					// Now the platform goes down
					goingDown = true;
				}
			}

			return; // End the "State"
		}
		////////////////////////SINKED///////////////////////

		////////////////////////RECOIL///////////////////////
		if (recoil)
		{
			if (calculatedPathOnce == false)
			{
				recoilPath.Clear();
				// From current y pos
				recoilPath.Push(0, AEVec2(0, platformT->mLocal.mTranslationZ.y));
				recoilPath.Push(1, AEVec2(0, platformT->mLocal.mTranslationZ.y - recoilRange));
				calculatedPathOnce = true;
			}

			// Ease the timer
			easedTime = EaseOutQuad(timer / recoilTime);

			// Move the platform in the y axis
			platformT->mLocal.mTranslationZ.y = recoilPath.SampleTime(easedTime).y;

			// Update the timer
			timer += (f32)aexTime->GetFrameTime();

			// Check if the platform has reached the end of the path
			if (timer >= recoilTime)
			{
				recoil = false;
				timer = 0;
				calculatedPathOnce = false;
				rising = true;
				//std::cout << "RISE" << std::endl;
			}

			return; // End the "State"
		}
		////////////////////////RECOIL///////////////////////

		////////////////////////RISING///////////////////////
		if (rising)
		{
			if (calculatedPathOnce == false)
			{
				risingPath.Clear();
				// From current y pos
				risingPath.Push(0, AEVec2(0, platformT->mLocal.mTranslationZ.y));
				risingPath.Push(1, AEVec2(0, platformIdlePos + mIdlRange));
				calculatedPathOnce = true;
			}

			// Ease the timer
			easedTime = EaseInOutQuad(timer / sinkTime);

			// Move the platform in the y axis
			platformT->mLocal.mTranslationZ.y = risingPath.SampleTime(easedTime).y;

			// Update the timer
			timer += (f32)aexTime->GetFrameTime();

			// Check if the platform has reached the end of the path
			if (timer >= sinkTime)
			{
				timer = 0;
				calculatedPathOnce = false;
				rising = false;
				idle = true;
				goingDown = true;
				//std::cout << "IDLE" << std::endl;
			}

			return; // End the "State"
		}
		////////////////////////RISING///////////////////////
	}
	
	void FloatingPlatform::OnCollisionStarted(const CollisionStarted & collision)
	{
		// Time to sink
		if (collision.otherObject->GetBaseNameString() == "foot")
		{
			//std::cout << "collision started" << std::endl;
			//float rot = mOwner->GetComp<TransformComp>()->mLocal.mOrientation;
			//
			//rot -= PI / 2;
			//
			//stats->gravityDir = AEVec2(cos(rot), sin(rot)) * 10;

			playerOnPlatform = true;

			if (idle == true)
			{
				sinking = true;
				idle = false;
				timer = 0;
				//std::cout << "SINK" << std::endl;
			}

			if (rising == true)
			{
				rising = false;
				sinking = true;
				timer = 0;
				calculatedPathOnce = false;
				//std::cout << "SINK" << std::endl;
			}
		}
	}
	
	void FloatingPlatform::OnCollisionEnded(const CollisionEnded & collision)
	{
		// Time to rise
 		if (collision.otherObject->GetBaseNameString() == "foot")
		{
			//std::cout << "collision ended" << std::endl;
			if (playerOnPlatform == true)
			{
				rising = true;
				idle = false;
				sinked = false;
				sinking = false;
				recoil = false;
				playerOnPlatform = false;
				calculatedPathOnce = false;
				//std::cout << "RISE" << std::endl;
				timer = 0;
			}
		}
	}
	
	bool FloatingPlatform::OnGui()
	{
		bool changed = false;

		/////////////////////////IDLE////////////////////////
		ImGui::Text("Idle Options:");
		ImGui::Text("Floating Range");
		if (ImGui::InputFloat("Floating Range", &mIdlRange))
		{
			ChangeIdlPath();
			changed = true;
		}
		ImGui::Text("Time to go down");
		if (ImGui::InputFloat("Time to go down", &IdleDownTime))
		{
			changed = true;
		}
		ImGui::Text("Time to go up");
		if (ImGui::InputFloat("Time to go up", &IdleUpTime))
		{
			changed = true;
		}
		/////////////////////////IDLE////////////////////////

		///////////////////////SINKING///////////////////////
		ImGui::Text("\nSinking Options:");
		ImGui::Text("Sinking Range");
		if (ImGui::InputFloat("Sinking Range", &sinkRange))
		{
			changed = true;
		}
		ImGui::Text("Time to sink");
		if (ImGui::InputFloat("Time to sink", &sinkTime))
		{
			changed = true;
		}
		///////////////////////SINKING///////////////////////

		////////////////////////SINKED///////////////////////
		ImGui::Text("\nSinked Options:");
		ImGui::Text("Floating range");
		if (ImGui::InputFloat("sinked range", &sinkedRange))
		{
			ChangeSinkedPath();
			changed = true;
		}
		ImGui::Text("Time to go down");
		if (ImGui::InputFloat("sinked down time", &sinkedDownTime))
		{
			changed = true;
		}
		ImGui::Text("Time to go up");
		if (ImGui::InputFloat("sinked up time", &sinkedUpTime))
		{
			changed = true;
		}
		////////////////////////SINKED///////////////////////

		////////////////////////RECOIL///////////////////////
		ImGui::Text("Recoil Options:");
		ImGui::Text("Recoil range");
		if (ImGui::InputFloat("recoil range", &recoilRange))
		{
			changed = true;
		}
		ImGui::Text("Recoil Time");
		if (ImGui::InputFloat("recoil time", &recoilTime))
		{
			changed = true;
		}
		////////////////////////RECOIL///////////////////////
		return changed;
	}

	void FloatingPlatform::ChangeIdlPath()
	{
		idlePathUp.Clear();
		idlePathUp.Push(0, AEVec2(platformT->mLocal.mTranslationZ.x, platformT->mLocal.mTranslationZ.y - mIdlRange));
		idlePathUp.Push(1, AEVec2(platformT->mLocal.mTranslationZ.x, platformT->mLocal.mTranslationZ.y + mIdlRange));

		idlePathDown.Clear();
		idlePathDown.Push(0, AEVec2(platformT->mLocal.mTranslationZ.x, platformT->mLocal.mTranslationZ.y + mIdlRange));
		idlePathDown.Push(1, AEVec2(platformT->mLocal.mTranslationZ.x, platformT->mLocal.mTranslationZ.y - mIdlRange));
	}

	void FloatingPlatform::ChangeSinkedPath()
	{
		sinkedPathUp.Clear();
		sinkedPathUp.Push(0, AEVec2(0, platformSinkedPos - sinkedRange));
		sinkedPathUp.Push(1, AEVec2(0, platformSinkedPos + sinkedRange));

		sinkedPathDown.Clear();
		sinkedPathDown.Push(0, AEVec2(0, platformSinkedPos + sinkedRange));
		sinkedPathDown.Push(1, AEVec2(0, platformSinkedPos - sinkedRange));
	}

	void FloatingPlatform::recalculatePaths()
	{
		platformIdlePos = platformT->mLocal.mTranslationZ.y;
		platformSinkedPos = platformIdlePos - sinkRange;

		goingDown = false;
		recoil = false;
		sinking = false;
		idle = true;
		sinked = false;
		rising = false;
		calculatedPathOnce = false;
		sinkedPathUp.Clear();
		sinkedPathDown.Clear();
		idlePathUp.Clear();
		idlePathDown.Clear();
		timer = IdleUpTime / 2.0f;
		//////////////////////////IDLE///////////////////////////////////
		idlePathDown.Push(0, AEVec2(0, platformIdlePos + mIdlRange));
		idlePathDown.Push(1, AEVec2(0, platformIdlePos - mIdlRange));

		idlePathUp.Push(0, AEVec2(0, platformIdlePos - mIdlRange));
		idlePathUp.Push(1, AEVec2(0, platformIdlePos + mIdlRange));
		//////////////////////////IDLE///////////////////////////////////

		/////////////////////////SINKED//////////////////////////////////
		sinkedPathUp.Push(0, AEVec2(0, platformSinkedPos - sinkedRange));
		sinkedPathUp.Push(1, AEVec2(0, platformSinkedPos + sinkedRange));

		sinkedPathDown.Push(0, AEVec2(0, platformSinkedPos + sinkedRange));
		sinkedPathDown.Push(1, AEVec2(0, platformSinkedPos - sinkedRange));
		/////////////////////////SINKED//////////////////////////////////
	}

	void FloatingPlatform::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);

		value["idle range"]               >> mIdlRange;      // Idle parameters
		value["Time to go down"]          >> IdleDownTime;   // Idle parameters
		value["Time to fo up"]            >> IdleUpTime;	 // Idle parameters

		value["sink range"]               >> sinkRange;      // Sink parameters
		value["Time to sink"]             >> sinkTime;       // Sink parameters

		value["sinked range"]			  >> sinkedRange;	 // Sinked parameters
		value["time to go up (sinked)"]   >> sinkedUpTime;   // Sinked parameters
		value["time to go down (sinked)"] >> sinkedDownTime; // Sinked parameters

		value["recoil range"]             >> recoilRange;    // Recoil parameters
		value["recoil time"]              >> recoilTime;     // Recoil parameters
	}
	void FloatingPlatform::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);

		value["idle range"]				  << mIdlRange;      // Idle parameters
		value["Time to go down"]		  << IdleDownTime;   // Idle parameters
		value["Time to fo up"]			  << IdleUpTime;	 // Idle parameters
										  				     
		value["sink range"]				  << sinkRange;      // Sink parameters
		value["Time to sink"]			  << sinkTime;       // Sink parameters

		value["sinked range"]			  << sinkedRange;	 // Sinked parameters
		value["time to go up (sinked)"]   << sinkedUpTime;	 // Sinked parameters
		value["time to go down (sinked)"] << sinkedDownTime; // Sinked parameters

		value["recoil range"]			  << recoilRange;    // Recoil parameters
		value["recoil time"]			  << recoilTime;     // Recoil parameters
	}
	void FloatingPlatform::RestartData()
	{
		platformSinkedPos = platformIdlePos - sinkRange;
		sinkedPathUp.Clear();
		sinkedPathDown.Clear();
		idlePathUp.Clear();
		idlePathDown.Clear();

		//////////////////////////IDLE///////////////////////////////////
		idlePathDown.Push(0, AEVec2(0, platformIdlePos + mIdlRange));
		idlePathDown.Push(1, AEVec2(0, platformIdlePos - mIdlRange));

		idlePathUp.Push(0, AEVec2(0, platformIdlePos - mIdlRange));
		idlePathUp.Push(1, AEVec2(0, platformIdlePos + mIdlRange));
		//////////////////////////IDLE///////////////////////////////////

		/////////////////////////SINKED//////////////////////////////////
		sinkedPathUp.Push(0, AEVec2(0, platformSinkedPos - sinkedRange));
		sinkedPathUp.Push(1, AEVec2(0, platformSinkedPos + sinkedRange));

		sinkedPathDown.Push(0, AEVec2(0, platformSinkedPos + sinkedRange));
		sinkedPathDown.Push(1, AEVec2(0, platformSinkedPos - sinkedRange));
		/////////////////////////SINKED//////////////////////////////////
	}
}
