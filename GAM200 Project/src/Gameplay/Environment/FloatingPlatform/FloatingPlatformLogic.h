#pragma once
#include "Components\Logic.h"
#include "src/Engine/AI/StateMachine.h"
#include "AI/Path2D.h"
#include "Player\Stats\PlayerStats.h"

namespace AEX
{
	class Rigidbody;
	class TransformComp;
	class FloatingPlatform;

	//struct Floating_Idle : public State
	//{
	//	Floating_Idle(const char* name);
	//
	//	virtual void LogicUpdate();
	//
	//	FloatingPlatform* platform;
	//
	//	Path2D idlePath;
	//
	//	unsigned int time;
	//};

	class FloatingPlatform : public LogicComp//, public Actor
	{
	    AEX_RTTI_DECL(FloatingPlatform, LogicComp);
		
		public:
			FloatingPlatform();

			virtual void Initialize();
			virtual void Update();
			virtual void OnCollisionStarted(const CollisionStarted& collision);
			virtual void OnCollisionEnded(const CollisionEnded& collision);
			virtual bool OnGui();
			void ChangeIdlPath();
			void ChangeSinkedPath();
			void recalculatePaths();
			//void SM_Init();
			
			// FromJson specialized for this component
			virtual void FromJson(json & value);
			// ToJson specialized for this component
			virtual void ToJson(json & value);

			TransformComp* platformT;
			float platformIdlePos;
			float platformSinkedPos;
			bool calculatedPathOnce;
			float timer;
			float easedTime;
			bool playerOnPlatform;
			PlayerStats* stats;

			///////////STATES OF THE PLATFORM/////////
			bool goingDown;
			bool idle;
			bool sinking;
			bool sinked;
			bool recoil;
			bool rising;
			bool doNotStart;
			float doNotStartTime;
			float doNotStartTimer;
			///////////STATES OF THE PLATFORM/////////

			////////////FOR THE EDITOR//////////////
			float mIdlRange;      // Idle parameters
			float IdleUpTime;	  // Idle parameters
			float IdleDownTime;	  // Idle parameters
			
			float sinkRange;      // Sinking parameters
			float sinkTime;	      // Sinking parameters

			float sinkedRange;    // Sinked parameters
			float sinkedUpTime;	  // Sinked parameters
			float sinkedDownTime; // Sinked parameters

			float recoilRange;    // Recoil parameters
			float recoilTime;     // Recoil parameters
			////////////FOR THE EDITOR//////////////
			bool active = true;

			Path2D idlePathUp;
			Path2D idlePathDown;
			Path2D sinkPath;
			Path2D sinkedPathUp;
			Path2D sinkedPathDown;
			Path2D recoilPath;
			Path2D risingPath;

			// PABS //
			void RestartData();

	};
}
