#pragma once
#include "Components\Logic.h"
#include "Components\AEXTransformComp.h"
#include "Components\SpriteComps.h"
#include "Player\Stats\PlayerStats.h"
#include "Imgui\imgui.h"

using namespace AEX;

class LiveController : public LogicComp
{
	AEX_RTTI_DECL(LiveController, LogicComp);
public:
	// This object's translation
	TransformComp* t;

	// GameObject array of lives
	GameObject* lives[3];

	// Player stats to get the live number
	PlayerStats* stats;

	// Boolean determining if it is the starting frame
	bool start = true;

	// Lives counter updated every frame
	int playerLives = 3;

	// Offset between lives
	float offset = 1.f;

	// Start is called before the first frame update
	virtual void Initialize();

	// Update is called once per frame
	virtual void Update();

	// OnGui() function
	virtual bool OnGui();

	// FromJson specialized for this component
	virtual void FromJson(json & value);
	// ToJson specialized for this component
	virtual void ToJson(json & value);
};
