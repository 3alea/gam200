#pragma once

#include "Components\Logic.h"


namespace AEX
{
	class SpriteComp;

	class HUDFader : public LogicComp
	{
		AEX_RTTI_DECL(HUDFader, LogicComp);

	public:

		void Initialize();
		void Update();
		void Shutdown();

		bool OnGui();

		void FromJson(json & value);
		void ToJson(json & value);

		SpriteComp * mSprite = nullptr;
		f32 mSpeed = 1.0f;

		bool doneOnce = false;

	private:

	};
}