#pragma once
#include "Components/Logic.h"
#include "Enemy/Enemy.h"

using namespace AEX;

class BossBarLogic : public LogicComp
{
	AEX_RTTI_DECL(BossBarLogic, LogicComp);
public:
	void Initialize();
	void Update();
	void Shutdown();

	bool OnGui();

	void FromJson(json & value);
	void ToJson(json & value);

private:
	Enemy* pEnemy;
	TransformComp* pTrans;
	TransformComp* pEnemyTrans;

	std::string mEnemyName;
	AEVec2 mOffset;
	bool mbStatic = false;
	bool mbStationaryBar = false;

	AEVec2 mOriginalSize;
	int mTotalLives;
	f32 currentSize;
	f32 left;
	f32 barOffset;
};