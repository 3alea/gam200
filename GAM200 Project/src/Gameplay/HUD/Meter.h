#pragma once
#include "Components\Logic.h"
#include "Player\Stats\PlayerStats.h"
#include "Follow\FollowAnObject.h"
//#include "Components\AEXTransformComp.h"
//#include "Imgui\imgui.h"

using namespace AEX;

class Meter : public LogicComp
{
	AEX_RTTI_DECL(Meter, LogicComp);
public:
	// This object's transform to edit size
	TransformComp* t;

	// To get parameters
	PlayerStats* stats;

	// FollowAnObject special behaviour editor
	FollowAnObject* followComp = nullptr;

	// Meter current's size
	float currentSize = 0.f;

	// Offset
	AEVec2 offset = AEVec2(0.f, 0.f);

	// Meter's offset from the origin
	float ratio = 1.f;

	// Offset of the meter
	float meterOffset = 0.f;

	// Size in box lengths
	float boxLength = 2.f;

	// Start is called before the first frame update
	virtual void Initialize();

	// Update is called once per frame
	virtual void Update();

	// OnGui() function
	virtual bool OnGui();

	// FromJson specialized for this component
	virtual void FromJson(json & value);
	// ToJson specialized for this component
	virtual void ToJson(json & value);
};
