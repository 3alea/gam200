#include "Composition/AEXScene.h"
#include "Composition\AEXGameObject.h"
#include "Meter.h"

using namespace AEX;
using namespace ImGui;

void Meter::Initialize()
{
	t = mOwner->GetComp<TransformComp>();
	stats = aexScene->FindObjectInANYSpace("player")->GetComp<PlayerStats>();

	followComp = mOwner->GetComp<FollowAnObject>();

	if (followComp != nullptr)
		followComp->meterOffset.y = offset.y;
}

void Meter::Update()
{
	if (!stats || !followComp)
	{
		Initialize();
	}
	// Ratio
	ratio = stats->chargeMeter / stats->chargeMeterCap;

	// Size
	currentSize = boxLength * ratio;
	t->mWorld.mScale.x = currentSize;

	// Meter offset
	meterOffset = offset.x - ((1.f - ratio) * offset.x);
	if (followComp)
		followComp->meterOffset.x = meterOffset;
}

bool Meter::OnGui()
{
	// isChanged boolean to return if something has changed
	bool isChanged = false;

	// Collapsing header - when clicked, opens up to show the rest of edited variables
	if (ImGui::CollapsingHeader("Meter"))
	{
		// Scale
		ImGui::Text("Meter offset");

		float v[2] = {offset.x, offset.y};

		if (ImGui::InputFloat2("offset", v))
		{
			isChanged = true;
			offset.x = v[0];
			offset.y = v[1];
		}
			

		// boxLength
		ImGui::Text("Box length (length relative to box)");

		if (ImGui::InputFloat("Box length", &boxLength))
			isChanged = true;
	}

	return isChanged;
}

// FromJson specialized for this component
void Meter::FromJson(json & value)
{
	this->IComp::FromJson(value);

	value["Max size"] >> offset;
	value["Box length"] >> boxLength;
}

// ToJson specialized for this component
void Meter::ToJson(json & value)
{
	this->IComp::ToJson(value);

	value["Max size"] << offset;
	value["Box length"] << boxLength;
}
