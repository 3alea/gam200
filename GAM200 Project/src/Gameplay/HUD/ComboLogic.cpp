#include "ComboLogic.h"
#include <cstring>

void ComboDisplayLogic::Initialize()
{
	mComboHUD = nullptr;
	mStats = nullptr;
	mComboHUD = mOwner->GetComp<SpriteText>();

	GameObject* pGO = aexScene->FindObjectInANYSpace("player");

	if (pGO)
		mStats = pGO->GetComp<PlayerStats>();
}

void ComboDisplayLogic::Update()
{
	if (!mStats || !mComboHUD)
		Initialize();

	std::string comboHUD;

	comboHUD = std::to_string(mStats->combo/2);
	strcpy(mComboHUD->mText, comboHUD.c_str());

	f32 comboTimerNorm = mStats->comboTimer / mStats->maxComboTime;

	mComboHUD->mColor[3] = 1 - (comboTimerNorm * comboTimerNorm * comboTimerNorm);
}

bool ComboDisplayLogic::OnGui()
{
	return false;
}

void ComboDisplayLogic::FromJson(json & value)
{
	this->IComp::FromJson(value);
}

void ComboDisplayLogic::ToJson(json & value)
{
	this->IComp::ToJson(value);
}
