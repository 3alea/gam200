#pragma once
#include "Components\Logic.h"
#include "Components\SpriteComps.h"
#include "Imgui\imgui.h"

using namespace AEX;

class HeartLogic : public LogicComp
{
	AEX_RTTI_DECL(HeartLogic, LogicComp);
public:
	// Sprite to alter the alpha
	SpriteComp* sprite;

	// Used to kill this object
	bool willDie = true;

	// Disappear speed
	float disappearSpeed = 2.f;

	// Start is called before the first frame update
	virtual void Initialize();

	// Update is called once per frame
	virtual void Update();

	// OnGui() function
	virtual bool OnGui();

	// FromJson specialized for this component
	virtual void FromJson(json & value);
	// ToJson specialized for this component
	virtual void ToJson(json & value);
};
