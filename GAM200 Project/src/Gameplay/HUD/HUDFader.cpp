#include "HUDFader.h"
#include "Composition\AEXGameObject.h"
#include "Components\SpriteComps.h"
#include "Platform\AEXTime.h"
#include "Environment\Background\StationZoomOut.h"
#include <Imgui\imgui.h>


namespace AEX
{
	void HUDFader::Initialize()
	{
		mSprite = mOwner->GetComp<SpriteComp>();
	}
	void HUDFader::Update()
	{
		if (doneOnce == false)
		{
			Initialize();
			doneOnce = true;
		}

		if (mSprite != nullptr)
		{
			// If on cinematic, make the hud disappear with a fade
			if (StationZoomOut::onCinematic)
			{
				mSprite->mColor[3] -= (f32)aexTime->GetFrameTime() * mSpeed;

				if (mSprite->mColor[3] < 0)
					mSprite->mColor[3] = 0;
			}
			// Else, make it appear again with a fade
			else
			{
				mSprite->mColor[3] += (f32)aexTime->GetFrameTime() * mSpeed;

				if (mSprite->mColor[3] > 1)
					mSprite->mColor[3] = 1;
			}
		}
	}
	void HUDFader::Shutdown()
	{

	}

	bool HUDFader::OnGui()
	{
		bool isChanged = false;

		if (ImGui::InputFloat("Speed", &mSpeed))
		{
			isChanged = true;
		}

		return isChanged;
	}

	void HUDFader::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);

		value["Speed"] >> mSpeed;
	}
	void HUDFader::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);

		value["Speed"] << mSpeed;
	}
}