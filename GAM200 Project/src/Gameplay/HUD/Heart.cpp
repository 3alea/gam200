#include "Composition/AEXScene.h"
#include "Composition\AEXGameObject.h"
#include "Platform/AEXTime.h"
#include "Heart.h"
#include "Environment\Background\StationZoomOut.h"

using namespace AEX;
using namespace ImGui;

void HeartLogic::Initialize()
{
	sprite = GetOwner()->GetComp<SpriteComp>();
}

void HeartLogic::Update()
{
	// Temporary solution added by Miguel so that the hud can disappear on cinematics
	if (StationZoomOut::onCinematic)
		return;

	// Lower the alpha if the heart will disappear
	if (willDie)
	{
		// If within range...
		if(sprite->mColor[3] > 0.f)
			sprite->mColor[3] -= 1.f * disappearSpeed / 255.f;
	}
	else
	{
		if(sprite->mColor[3] < 1.f)
			sprite->mColor[3] += 1.f * disappearSpeed / 255.f;
	}

	//// If you can no longer see the heart, destroy it
	//if (sprite->mColor[3] <= 0.f)
	//{
	//	mOwner->GetParentSpace()->DestroyObject(mOwner);
	//}
}

bool HeartLogic::OnGui()
{
	// isChanged boolean to return if something has changed
	bool isChanged = false;

	// Collapsing header - when clicked, opens up to show the rest of edited variables
	if (ImGui::CollapsingHeader("Heart"))
	{
		// Wave amount
		ImGui::Text("Heart disappear speed (how fast it will disappear)");
		// Allow user to input wave height from the editor
		if (ImGui::InputFloat("Disappear speed", &disappearSpeed))
			isChanged = true;
	}

	return isChanged;
}

// FromJson specialized for this component
void HeartLogic::FromJson(json & value)
{
	this->IComp::FromJson(value);

	value["Disappear speed"] >> disappearSpeed;
}

// ToJson specialized for this component
void HeartLogic::ToJson(json & value)
{
	this->IComp::ToJson(value);

	value["Disappear speed"] << disappearSpeed;
}
