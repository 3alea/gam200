#include "BossBarLogic.h"

void BossBarLogic::Initialize()
{
	pEnemy = nullptr;
	pTrans = mOwner->GetComp<TransformComp>();

	GameObject* pGO = aexScene->FindObjectInANYSpace(mEnemyName.c_str());

	if (pGO)
	{
		pEnemyTrans = pGO->GetComp<TransformComp>();
		mOriginalSize = pTrans->mLocal.mScale;

		if (Enemy* tEnemyComp = pGO->GetComp<Enemy>() )
		{
			pEnemy = tEnemyComp;
			mTotalLives = pEnemy->mLives;
			left = -mOriginalSize.x / 2.0f;
		}
	}
}

void BossBarLogic::Update()
{
	if (!pEnemy || !pTrans)
		Initialize();

	currentSize = mOriginalSize.x * pEnemy->mLives / mTotalLives;
	barOffset = left + currentSize / 2.0f;

	if (!pEnemy || pEnemy->mLives <= 0)
	{
		mOwner->GetParentSpace()->DestroyObject(mOwner);
		return;
	}

	AEVec3 enemyOff = mbStationaryBar ? AEVec3() : pEnemyTrans->mLocal.mTranslationZ;
	if (mbStatic)
		pTrans->mLocal.mTranslationZ = enemyOff + AEVec3(mOffset.x, mOffset.y, pTrans->mLocal.mTranslationZ.z);
	else
		pTrans->mLocal.mTranslationZ = enemyOff + AEVec3(mOffset.x + barOffset, mOffset.y, pTrans->mLocal.mTranslationZ.z);

	pTrans->mLocal.mScale = mbStatic ? mOriginalSize : AEVec2(currentSize, pTrans->mLocal.mScale.y);
}

void BossBarLogic::Shutdown()
{
}

bool BossBarLogic::OnGui()
{
	bool changed = false;

	if (ImGui::Checkbox("Static Bar", &mbStatic)) changed = true;
	if (ImGui::Checkbox("Stationary Bar", &mbStationaryBar)) changed = true;

	if (ImGui::InputFloat2("Bar Offset", mOffset.v, 3)) changed = true;

	char enemyName[256];
	strcpy(enemyName, mEnemyName.c_str());
	if (ImGui::InputText("Enemy Name", enemyName, IM_ARRAYSIZE(enemyName)))
	{
		changed = true;
		mEnemyName = std::string(enemyName);
	}
	return changed;
}

void BossBarLogic::FromJson(json & value)
{
	this->IComp::FromJson(value);

	value["Stationary"] >> mbStationaryBar;
	value["Static"] >> mbStatic;
	value["EnemyName"] >> mEnemyName;
	value["BarOffset"] >> mOffset;
}

void BossBarLogic::ToJson(json & value)
{
	this->IComp::ToJson(value);

	value["Stationary"] << mbStationaryBar;
	value["Static"] << mbStatic;
	value["EnemyName"] << mEnemyName;
	value["BarOffset"] << mOffset;
}
