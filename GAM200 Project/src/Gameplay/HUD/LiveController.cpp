#include "Composition/AEXScene.h"
#include "Composition\AEXGameObject.h"
#include "Platform/AEXTime.h"
#include "Follow\FollowAnObject.h"
#include "LiveController.h"
#include "Heart.h"

using namespace AEX;
using namespace ImGui;

void LiveController::Initialize()
{
	t = mOwner->GetComp<TransformComp>();
	stats = aexScene->FindObjectInANYSpace("player")->GetComp<PlayerStats>();
	if(stats)
		playerLives = stats->lives;
}

void LiveController::Update()
{
	if (!stats)
	{
		Initialize();
	}
	// If the objects do not exist yet
	if (start)
	{
		// Create the number of hearts of the player
		for (int counter = playerLives; counter > 0; counter--)
		{
			// Calculate position of the heart
			AEVec3 pos = t->mLocal.mTranslationZ + AEVec3(counter * offset, 0.f, 0.f);

			// Create a heart in that position
			lives[counter - 1] = mOwner->GetParentSpace()->InstantiateArchetype("Heart", pos);
			lives[counter - 1]->GetComp<FollowAnObject>()->mOffset.x = counter * offset;
		}
		start = false;
	}

	// Update the number of lives
	playerLives = stats->lives;

	// Walker
	int counter = 0;

	// For loop going through all of the hearts. Set them all to die.
	while (lives[counter] != nullptr && counter < 3)
	{
		if(lives[counter])
			lives[counter]->GetComp<HeartLogic>()->willDie = true;
		counter++;
	}

	// For loop going only up to the number of hearts which are alive
	for (int walker = 0; walker < playerLives; walker++)
	{
		if(lives[walker])
			lives[walker]->GetComp<HeartLogic>()->willDie = false;
	}
}

bool LiveController::OnGui()
{
	// isChanged boolean to return if something has changed
	bool isChanged = false;

	// Collapsing header - when clicked, opens up to show the rest of edited variables
	if (ImGui::CollapsingHeader("Live controller"))
	{
		// Wave amount
		ImGui::Text("The offset between hearts");
		// Allow user to input wave height from the editor
		if (ImGui::InputFloat("Offset", &offset))
			isChanged = true;
	}

	return isChanged;
}

// FromJson specialized for this component
void LiveController::FromJson(json & value)
{
	this->IComp::FromJson(value);

	value["Offset"] >> offset;
	//value["Start"] >> start;
}

// ToJson specialized for this component
void LiveController::ToJson(json & value)
{
	this->IComp::ToJson(value);

	value["Offset"] << offset;
	value["Start"] << start;
}
