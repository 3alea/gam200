#include "Components/Logic.h"
#include "Enemy/Enemy.h"

using namespace AEX;

class ComboDisplayLogic : public LogicComp
{
	AEX_RTTI_DECL(ComboDisplayLogic, LogicComp);
public:
	void Initialize();
	void Update();

	bool OnGui();

	void FromJson(json & value);
	void ToJson(json & value);

	SpriteText* mComboHUD;
	PlayerStats* mStats;
};