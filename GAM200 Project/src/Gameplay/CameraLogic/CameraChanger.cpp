#include "CameraChanger.h"
#include "Composition/AEXScene.h"
#include "Player/Stats/PlayerStats.h"
#include "Graphics\GfxMgr.h"
#include "Graphics/WindowMgr.h"
#include "CameraLogic/CameraMovement.h"
#include "Components/AEXTransformComp.h"
#include "Components/Camera.h"
#include "Components/Collider.h"
namespace AEX
{
	void CameraChanger::Initialize()
	{
		GameObject* cam = aexScene->FindObjectInANYSpace("camera");

		t = mOwner->GetComp<TransformComp>();

		camLogic = nullptr;
		camera = nullptr;
		camPos = nullptr;

		freeFall = false;

		if (cam)
		{
			camLogic = cam->GetComp<CameraMovement>();
			camera = cam->GetComp<Camera>();
			camPos = camera->mOwner->GetComp<TransformComp>();
		}
	}

	void CameraChanger::Update()
	{
		if (!camLogic || !t)
			Initialize();
	}

	void CameraChanger::OnCollisionStarted(const CollisionStarted& collision)
	{
		if (!camLogic || DoneOnce)
			return;

		std::string name;

		if (veinSpecialCases && !TubeToDown)
			name = "foot";
		else
			name = "player";

		if (collision.otherObject->GetBaseNameString() == name)
		{
			// The camera is already transitioning
			if (camLogic->transitionAxis && camLogic->verticalCam == vertical)
				return;

			// Reset variables
			camLogic->justLerpZoom = false;
			camLogic->lerpZoom = false;
			camLogic->transitionTimer = 0;
			camLogic->calculatedOnce = false;

			// Just have to change the viewport
			if (justZoom)
			{
				camLogic->destinationZoom = newScale;
				camLogic->originZoom = camera->mViewRectangleSize;
				camLogic->lerpZoom = true;
				camLogic->justLerpZoom = true;
				camLogic->transitionAxis = true;
				camLogic->transitionTime = timeToChange;
				return;
			}

			// Change to a vertical camera
			if (vertical)
			{
				camLogic->camUp = camUp;
				camLogic->verticalCam = true;
				camLogic->freeFall = freeFall;
				camLogic->xDestination = t->mLocal.mTranslationZ.x;
				camLogic->xOrigin = camPos->mLocal.mTranslationZ.x;
				camLogic->lockX = false;

				if (veinSpecialCases)
				{
					if (veinsToNormal)
					{
						TransformComp* playerPos = mOwner->GetParentSpace()->FindObject("player")->GetComp<TransformComp>();
						playerPos->mLocal.mTranslationZ.z = -50.0f;
						TransformComp* e = mOwner->GetParentSpace()->FindObject("elevator1")->GetComp<TransformComp>();
						playerPos->mLocal.mTranslationZ.x = e->mLocal.mTranslationZ.x - 1.0f;
						e->mLocal.mTranslationZ.z = -51.0f;
						e->mLocal.mScale.x = 2.0f;
						e->mLocal.mScale.y = 1.7f;
						ICollider* c = mOwner->GetParentSpace()->FindObject("elevator1")->GetComp<ICollider>();
						c->mScale.x = 4;
					}
				}
			}

			// Change to a horizontal camera
			else
			{
				camLogic->freeFall = false;
				camLogic->verticalCam = false;
				camLogic->yDestination = t->mLocal.mTranslationZ.y;
				camLogic->yOrigin = camPos->mLocal.mTranslationZ.y;

				if (veinSpecialCases)
				{
					if (veinsToNormal)
					{
						camLogic->yDestination = t->mLocal.mTranslationZ.y + 2.7f;
						camLogic->lockX = false;
						camLogic->changeToNormalCam();
						camLogic->xMax = t->mLocal.mTranslationZ.x + 6.0f;
						camLogic->xMin = t->mLocal.mTranslationZ.x - 6.0f;
						camLogic->cameraOffset = t->mLocal.mTranslationZ.x - mOwner->GetParentSpace()->FindObject("player")->GetComp<TransformComp>()->mLocal.mTranslationZ.x;
						camLogic->camRight = true;
						DoneOnce = true;
						//t->mLocal.mTranslationZ.y = 500.0f;
					}
					else if (TubeToDown)
					{
						camLogic->lockX = false;
						camLogic->xMax = t->mLocal.mTranslationZ.x + 81.0f;
						camLogic->xMin = t->mLocal.mTranslationZ.x - 1.0f;
						camLogic->cameraOffset = 4.0f;
						camLogic->camRight = true;
						DoneOnce = true;
					}
					else if (TubeToUp)
					{
						camLogic->changeVeinCam();
						camLogic->t->mLocal.mTranslationZ.y = -4.48f;
						camLogic->lockXVeins = 210.0f;
						camLogic->lock = false;
						camLogic->veinsOffset = 1.0f;
						camLogic->cam->mViewRectangleSize = newScale;
						TransformComp* playerPos = mOwner->GetParentSpace()->FindObject("player")->GetComp<TransformComp>();
						playerPos->mLocal.mTranslationZ.z = 10.0f;
						TransformComp* e = mOwner->GetParentSpace()->FindObject("elevator2")->GetComp<TransformComp>();
						playerPos->mLocal.mTranslationZ.x = e->mLocal.mTranslationZ.x - 1.0f;
						e->mLocal.mTranslationZ.z = 4.0f;
						e->mLocal.mScale.x = 2.6f;
						e->mLocal.mScale.y = 1.45f;
						ICollider* c = mOwner->GetParentSpace()->FindObject("elevator2")->GetComp<ICollider>();
						c->mScale.x = 9;
					}
				}
			}

			// Change also the viewport
			if (newScale != 0)
			{
				camLogic->destinationZoom = newScale;
				camLogic->originZoom = camera->mViewRectangleSize;
				camLogic->lerpZoom = true;
			}

			// Just change the axis
			else
				camLogic->lerpZoom = false;

			camLogic->transitionAxis = true;
			camLogic->transitionTime = timeToChange;
		}
	}

	bool CameraChanger::OnGui()
	{
		// Collapsing header - when clicked, opens up to show the rest of edited variables
		if (ImGui::CollapsingHeader("CameraChanger"))
		{
			// isChanged boolean to return if something has changed
			bool isChanged = false;

			ImGui::Text("veins special camera changer");
			// Allow user to input lives from the editor
			if (ImGui::Checkbox("special camChanger", &veinSpecialCases))
			{
				isChanged = true;
			}

			if (veinSpecialCases)
			{
				// Special case
				ImGui::Text("veins cam to normal cam");
				// Allow user to input lives from the editor
				if (ImGui::Checkbox("veinsTransition", &veinsToNormal))
				{
					isChanged = true;
				}
				ImGui::Text("tube to the under artery");
				// Allow user to input lives from the editor
				if (ImGui::Checkbox("1", &TubeToDown))
				{
					isChanged = true;
				}
				ImGui::Text("tube to the upper vein");
				// Allow user to input lives from the editor
				if (ImGui::Checkbox("2", &TubeToUp))
				{
					isChanged = true;
				}
			}

			// Text for the horizontalBlock
			ImGui::Text("vertical camera");
			// Allow user to input lives from the editor
			if (ImGui::Checkbox("isVertical", &vertical))
			{
				isChanged = true;
			}

			if (vertical)
			{
				ImGui::Text("camera up");
				// Allow user to input lives from the editor
				if (ImGui::Checkbox("up", &camUp))
				{
					isChanged = true;
				}

				ImGui::Text("freeFall");
				if (ImGui::Checkbox("free fall", &freeFall))
				{
					isChanged = true;
				}
			}

			// new camera size
			if (ImGui::Checkbox("Change Only Viewport", &justZoom))
			{
				isChanged = true;
			}

			// new camera size
			if (ImGui::InputFloat("new viewport value", &newScale))
			{
				isChanged = true;
			}

			// new camera size
			if (ImGui::InputFloat("Transition Time", &timeToChange))
			{
				isChanged = true;
			}

			AEVec2 p0;
			AEVec2 p1;

			if (vertical)
			{
				p0 = AEVec2(t->mLocal.mTranslationZ.x, t->mLocal.mTranslationZ.y - 100);
				p1 = AEVec2(t->mLocal.mTranslationZ.x, t->mLocal.mTranslationZ.y + 100);
			}
			else
			{
				p0 = AEVec2(t->mLocal.mTranslationZ.x - 100, t->mLocal.mTranslationZ.y);
				p1 = AEVec2(t->mLocal.mTranslationZ.x + 100, t->mLocal.mTranslationZ.y);
			}

			GfxMgr->DrawLine(DebugLine(AEVec4(1, 0, 0, 1), p0, p1));

			AEVec2 viewRecPos = AEVec2(t->mLocal.mTranslationZ.x, t->mLocal.mTranslationZ.y);

			float aspectRatio = (f32)WindowMgr->GetCurrentWindow()->GetWidth() / (f32)WindowMgr->GetCurrentWindow()->GetHeight();

			GfxMgr->DrawRectangle(Transform(viewRecPos, AEVec2(2 * newScale*aspectRatio, 2 * newScale), 0.0f), AEVec4(0.0f, 0.0f, 1.0f, 1.0f));

			return isChanged;
		}

		// Added by Miguel to solve warning. Should it return false?
		return false;
	}

	// FromJson specialized for this component
	void CameraChanger::FromJson(json & value)
	{
		this->IComp::FromJson(value);

		value["vertical"] >> vertical;
		value["newScale"] >> newScale;
		value["justZoom"] >> justZoom;
		value["time to change"] >> timeToChange;
		value["camera up"] >> camUp;
		value["freeFall"] >> freeFall;
		value["veins"] >> veinsToNormal;
		value["veins1"] >> TubeToDown;
		value["veins2"] >> TubeToUp;
		value["vein special cases"] >> veinSpecialCases;
	}

	// ToJson specialized for this component
	void CameraChanger::ToJson(json & value)
	{
		this->IComp::ToJson(value);

		value["vertical"] << vertical;
		value["newScale"] << newScale;
		value["justZoom"] << justZoom;
		value["time to change"] << timeToChange;
		value["camera up"] << camUp;
		value["freeFall"] << freeFall;
		value["veins"] << veinsToNormal;
		value["veins1"] << TubeToDown;
		value["veins2"] << TubeToUp;
		value["vein special cases"] << veinSpecialCases;
	}
}