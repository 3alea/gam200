#pragma once
#include "Components\Logic.h"
#include "Imgui\imgui.h"
#include "CameraLogic/CameraMovement.h"
#include "Components/AEXTransformComp.h"
#include "Components/Camera.h"

namespace AEX
{
	class CameraChanger : public LogicComp
	{
		AEX_RTTI_DECL(CameraChanger, LogicComp);
	public:

		// Change to vertical
		bool vertical;

		bool freeFall = false;

		bool veinsToNormal = false;

		bool TubeToDown = false;
		bool TubeToUp = false;

		bool veinSpecialCases = false;

		// New scale of the camera if it were to change size
		float newScale;

		bool changeViewport;
		bool justZoom;
		
		bool camUp;

		bool DoneOnce = false;

		float timeToChange;

		// Camera Follower component needed to change values
		CameraMovement* camLogic;

		// Object TranformComp
		TransformComp* t;
		TransformComp* camPos;

		// Camera of the game
		Camera* camera;

		// Start is called before the first frame update
		virtual void Initialize();

		// Update overriden so that variables are set correctly
		virtual void Update();

		// OnCollision event
		virtual void OnCollisionStarted(const CollisionStarted& collision);

		// OnGui() function
		virtual bool OnGui();

		// FromJson specialized for this component
		virtual void FromJson(json & value);

		// ToJson specialized for this component
		virtual void ToJson(json & value);
	};
}