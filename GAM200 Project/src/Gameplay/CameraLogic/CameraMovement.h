#pragma once

#include "Components\Logic.h"
#include "Imgui\imgui.h"
#include "Platform/AEXInput.h"

class PlayerStats;
class Camera;

namespace AEX
{
	#define DPAD_LEFT     (aexInput->GamepadButtonTriggered(Input::GamepadButtons::eGamepadButtonDPadLeft))
	#define DPAD_RIGHT    (aexInput->GamepadButtonTriggered(Input::GamepadButtons::eGamepadButtonDPadRight))
	#define SUPER_SHOT    (aexInput->GamepadButtonTriggered(Input::GamepadButtons::eGamepadButtonB) || aexInput->KeyPressed(GLFW_KEY_R))
	#define R_STICK_RIGHT (aexInput->GetGamepadStick(Input::GamepadSticks::eRightStickXAxis) >  0.5)
	#define R_STICK_LEFT  (aexInput->GetGamepadStick(Input::GamepadSticks::eRightStickXAxis) <  -0.5)

	class TransformComp;

	class CameraMovement : public LogicComp
	{
		AEX_RTTI_DECL(CameraMovement, LogicComp);
	public:

		bool camUp = true;
		bool camRight = true;

		///////////////////////Change camera side/////////////////////
		bool changeSide = false;
		float changeTimer;
		float timeToChange;
		float camPlayerDiff;
		///////////////////////Change camera side/////////////////////

		// Object TranformComp
		TransformComp* t;
		// Position to follow
		TransformComp* pos;
		// Player stats to check player's state
		PlayerStats* stats;

		float angle;
		void update_angle();

		//---------------------------------------------------

		///////////////////Camera changer/////////////////////
		float cameraYoffset;
		float cameraOffset;
		bool verticalCam;
		float yDestination;
		float yOrigin;
		bool freeFall;

		float calculatedOnce = false;
		float dist;

		float xDestination;
		float xOrigin;
		bool transitionAxis;
		float destinationZoom;
		float originZoom;
		bool lerpZoom;
		bool justLerpZoom;
		float transitionTimer;
		float transitionTime;
		float easedTime;
		Camera* cam;

		void lerpZoomCam();
		void lerpCamAxis();

		///////////////////Camera changer/////////////////////

		//----------------------------------------------------

		//////////////////Veins Camera///////////////////////
		bool transition;
		float veinsOffset;

		float lastPos;

		float lockXVeins;

		bool lock;

		void UpdaterVeinsCam();

		void changeToNormalCam();
		void changeVeinCam();

		void UpdaterVeinsTransition();
		//////////////////Veins Camera///////////////////////

		//---------------------------------------------------

		/////////////////Regular Camera//////////////////////
		void UpdateCamPos();
		/////////////////Regular Camera//////////////////////

		/////////////////Camera Blocker//////////////////////
		float xMax;
		float yMax;
		float xMin;
		float yMin;
		bool lockX;
		bool lockLeft;
		bool lockRight;
		bool lockUp;
		bool lockDown;
		bool lockY;
		void check_limits();
		/////////////////Camera Blocker//////////////////////

		/////////////////Sin shake effect//////////////////////
		float a;
		float sinSpeed;
		/////////////////Sin shake effect//////////////////////

		//---------------------------------------------------

		// Start is called before the first frame update
		virtual void Initialize();

		// Update is called once per frame
		virtual void Update();

		// OnGui() function
		virtual bool OnGui();

		// FromJson specialized for this component
		virtual void FromJson(json & value);
		// ToJson specialized for this component
		virtual void ToJson(json & value);
	};
}
