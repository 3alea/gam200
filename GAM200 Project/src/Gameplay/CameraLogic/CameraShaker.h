#pragma once

#include "Components/Logic.h"


class Camera;

namespace AEX
{
	class TransformComp;

	class CameraShaker : public LogicComp
	{
		AEX_RTTI_DECL(CameraShaker, LogicComp);

	public:

		void Initialize();
		void Update();
		void Shutdown();

		bool OnGui();

		void FromJson(json & value);
		void ToJson(json & value);

		bool doneOnce = false;

		TransformComp * mCamTr = nullptr;
		Camera * mCam = nullptr;
		
		//TransformComp * mShakeCamTr = nullptr;

		bool mTranslationalShake = true;		// Determines if the shake should move the camera position
		bool mRotationalShake = false;			// Determines if the shake should rotate the camera

		f32 maxX = 0.1f;						// The maximum offset on x axis for the camera translation
		f32 maxY = 0.1f;						// The maximum offset on y axis for the camera translation
		f32 maxAngle = (0.002f * PI) / 180.0f;					// The maximum offset on the angle for the camera rotation

		// This value should be normalized [0, 1]. Whenever you want to shake
		// the camera, just add a value in the range [0, 1] to the trauma.
		// You can use the above variables for better control of the shake.
		f32 mTrauma = 1.0f;

		// To save the original position and orientation of the camera before doing the shake
		AEVec2 mPosBeforeShake;
		f32 mRotBeforeShake;
		bool mSavedPosOnce = false;

		// Not sure why I had this, but I will leave it just in case
		//f32 timer = 0.0f;

	private:

	};
}