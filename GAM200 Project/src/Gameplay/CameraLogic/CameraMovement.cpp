#include "CameraMovement.h"
#include "Platform/AEXTime.h"
//#include "AI/Easing.h"
#include "Environment/Background/StationZoomOut.h"
#include "Composition/AEXScene.h"
#include "Player/Stats/PlayerStats.h"
#include "Components/Camera.h"
#include "Components/AEXTransformComp.h"
#include "BoilerRoom/PuzzleLogic.h"

namespace AEX
{
	void CameraMovement::update_angle()
	{
		// Update to add sin (shaky effect)
		if (angle >= 2 * PI)
			angle = 0.0f;
		else
			angle += (f32)aexTime->GetFrameTime() * sinSpeed;
	}
	void CameraMovement::lerpZoomCam()
	{
		cam->mViewRectangleSize = originZoom + (destinationZoom - originZoom) * easedTime;
	}

	void CameraMovement::lerpCamAxis()
	{
		// Lerp the x axis
		if (verticalCam)
		{
			t->mLocal.mTranslationZ.x = xOrigin + (xDestination - xOrigin) * easedTime;

			if (!calculatedOnce)
			{
				dist = t->mLocal.mTranslationZ.y - pos->mLocal.mTranslationZ.y;
				calculatedOnce = true;
			}

			yOrigin = pos->mLocal.mTranslationZ.y + dist;

			if (camUp)
				yDestination = pos->mLocal.mTranslationZ.y + cameraYoffset;
			else
				yDestination = pos->mLocal.mTranslationZ.y - cameraYoffset;
			
			t->mLocal.mTranslationZ.y = yOrigin + (yDestination - yOrigin) * easedTime;

		}

		// Lerp the y axis
		else
		{
			t->mLocal.mTranslationZ.y = yOrigin + (yDestination - yOrigin) * easedTime;
			if (!calculatedOnce)
			{
				dist = t->mLocal.mTranslationZ.x - pos->mLocal.mTranslationZ.x;
				calculatedOnce = true;
			}

			xOrigin = pos->mLocal.mTranslationZ.x + dist;
			
			if (camRight)
				xDestination = pos->mLocal.mTranslationZ.x + cameraOffset;
			else
				xDestination = pos->mLocal.mTranslationZ.x - cameraOffset;
			
			t->mLocal.mTranslationZ.x = xOrigin + (xDestination - xOrigin) * easedTime;
		}
	}

	void CameraMovement::UpdaterVeinsCam()
	{
		t->mLocal.mTranslationZ.y = -4.48f;

		if (lock)
		{
			t->mLocal.mTranslationZ.x = lastPos;
			return;
		}

		// Get the distance from the player to the camera
		float dx = t->mLocal.mTranslationZ.x - pos->mLocal.mTranslationZ.x;

		// Move forward
			//t->mLocal.mTranslationZ.x += veinsVel * aexTime->GetFrameTime();

		// Otherwise, the player is getting away, catch up
		if (dx < veinsOffset && t->mLocal.mTranslationZ.x < lockXVeins)
		{
			t->mLocal.mTranslationZ.x += (pos->mLocal.mTranslationZ.x + veinsOffset - t->mLocal.mTranslationZ.x) * 0.1f;//pos->mLocal.mTranslationZ.x + veinsOffset;
			lastPos = t->mLocal.mTranslationZ.x;
		}

		if (t->mLocal.mTranslationZ.x >= lockXVeins)
		{
			lock = true;
			lastPos = t->mLocal.mTranslationZ.x = lastPos;
		}
	}

	void CameraMovement::changeToNormalCam()
	{
		stats->onVeins = false;
	}

	void CameraMovement::changeVeinCam()
	{
		stats->onVeins = true;
	}

	void CameraMovement::UpdaterVeinsTransition()
	{
		// Get the distance from the player to the camera
		float dx = t->mLocal.mTranslationZ.x - pos->mLocal.mTranslationZ.x;

		// Lerp
		t->mLocal.mTranslationZ.x += (pos->mLocal.mTranslationZ.x + veinsOffset + 1.0f - t->mLocal.mTranslationZ.x) * 0.5f * (f32)aexTime->GetFrameTime();

		if (dx >= veinsOffset + 0.5)
		{
			stats->onVeinsTransition = false;
			stats->onVeins = true;
		}
	}

	void CameraMovement::UpdateCamPos()
	{
		float xPlayer = pos->mLocal.mTranslationZ.x;
		float yPlayer = pos->mLocal.mTranslationZ.y;

		check_limits();

		// Need to transition
		if (transitionAxis)
		{
			// Update the timer
			transitionTimer += (f32)aexTime->GetFrameTime();

			// Still transitioning
			if (transitionTimer <= transitionTime)
			{
				easedTime = EaseInOutQuad(transitionTimer / transitionTime);

				// The transition only need a zoom interpolation
				if (justLerpZoom)
					lerpZoomCam();

				// Need to change from axis
				else
				{
					// Check if the current transition is asking
					// to lerp the camera's viewport
					if (lerpZoom)
						lerpZoomCam();

					// Change the axis of the camera
					lerpCamAxis();

					return;
				}
			}

			// Transition ended
			else
			{
				if (verticalCam)
					t->mLocal.mTranslationZ.x = xDestination;
				else
					t->mLocal.mTranslationZ.y = yDestination;
				angle = 0;
				transitionTimer = 0;
				transitionAxis = false;
				calculatedOnce = false;
			}
		}

		else
		{
			if (!verticalCam && !lockX)
			{
				if (changeSide)
				{
					changeTimer += (f32)aexTime->GetFrameTime();

					float easedTimer = EaseInOutQuad(changeTimer / timeToChange);

					float originX = pos->mLocal.mTranslationZ.x + camPlayerDiff;
					
					float destinationX;

					if (camRight)
						destinationX = pos->mLocal.mTranslationZ.x + cameraOffset;
					else
						destinationX = pos->mLocal.mTranslationZ.x - cameraOffset;
					
					t->mLocal.mTranslationZ.x = originX + (destinationX - originX) * easedTimer;

					if (changeTimer >= timeToChange)
					{
						changeTimer = 0.0f;
						changeSide = false;
					}
					else
						return;
				}

				// Cam is on the right
				if (camRight)
					t->mLocal.mTranslationZ.x += ((xPlayer + cameraOffset) - t->mLocal.mTranslationZ.x) * 0.2f;

				// Cam is on the left
				else if (!camRight)
					t->mLocal.mTranslationZ.x += ((xPlayer - cameraOffset) - t->mLocal.mTranslationZ.x) * 0.2f;

			}

			if (verticalCam && !lockY)
			{
				if (camUp)
				{
					if (freeFall)
						t->mLocal.mTranslationZ.y = yPlayer + cameraYoffset;
					else
					{
						t->mLocal.mTranslationZ.y += ((yPlayer + cameraYoffset) - t->mLocal.mTranslationZ.y) * 0.2f;
					}
				}
					
				else
				{
					if (freeFall)
						t->mLocal.mTranslationZ.y = (yPlayer - cameraYoffset);
					else
						t->mLocal.mTranslationZ.y += ((yPlayer - cameraYoffset) - t->mLocal.mTranslationZ.y) * 0.2f;
				}
			}
		}

		// Only change the camera if not in transition
		if (!verticalCam && !stats->onCinematic && !transitionAxis && !lockX)
		{
			if (DPAD_LEFT || R_STICK_LEFT)
			{
				if (camRight)
				{
					changeTimer = 0;
					changeSide = true;
					camPlayerDiff = t->mLocal.mTranslationZ.x - pos->mLocal.mTranslationZ.x;
				}

				camRight = false;
				stats->isRight = false;
			}

			if (DPAD_RIGHT || R_STICK_RIGHT)
			{
				if (!camRight)
				{
					changeSide = true;
					changeTimer = 0;
					camPlayerDiff = t->mLocal.mTranslationZ.x - pos->mLocal.mTranslationZ.x;
				}
				camRight = true;
				stats->isRight = true;
			}
		}
	}
	void CameraMovement::check_limits()
	{
		float xCam = t->mLocal.mTranslationZ.x;
		float yCam = t->mLocal.mTranslationZ.y;
		if (lockX)
		{
			float dist;

			if (lockLeft)
			{
				t->mLocal.mTranslationZ.x = xMin;
				xCam = xMin;

				if (camRight)
				{
					dist = xCam - pos->mLocal.mTranslationZ.x;

					if (dist <= cameraOffset - 0.1f)
					{
						lockLeft = false;
						lockX = false;
					}
				}
				else
				{
					

					dist = pos->mLocal.mTranslationZ.x - xCam;
					if (dist >= cameraOffset + 0.1f)
					{
						lockLeft = false;
						lockX = false;
					}
				}
			}
			else
			{
				t->mLocal.mTranslationZ.x = xMax;
				xCam = xMax;

				dist = pos->mLocal.mTranslationZ.x - xCam;

				if (dist <= -cameraOffset - 0.1f)
				{
					lockLeft = false;
					lockX = false;
					camRight = true;
				}
			}
		}
		else
		{
			// Check camera limits
			if (xCam < xMin)
			{
				changeSide = false;
				lockLeft = true;
				lockX = true;
			}
			else if (xCam > xMax)
			{
				changeSide = false;

				lockRight = true;
				lockX = true;
			}
			else
			{
				lockX = false;
				lockRight = false;
				lockLeft = false;
			}
		}

		if (lockY)
		{

		}
		else
		{
			if (yCam < yMin)
			{
				lockDown = true;
				lockY = true;
			}
			else if (yCam > yMax)
			{
				lockUp = true;
				lockY = true;
			}
		}
	}
	void CameraMovement::Initialize()
	{
		lockX = false;
		lockY = false;
		pos = nullptr;
		t = nullptr;
		cam = nullptr;
		camUp = false;
		lock = false;
		changeTimer = 0;

		GameObject* player = aexScene->FindObjectInSpace("player", "Main");
		if (player)
		{
			pos = player->GetComp<TransformComp>();
			stats = player->GetComp<PlayerStats>();
			stats->onVeins = false;
			stats->onVeinsTransition = false;
		}

		if (GameObject* c = aexScene->FindObjectInSpace("camera", "Main"))
		{
			cam = c->GetComp<Camera>();\
				//std::cout << "camera viewrect" << cam->mViewRectangleSize << std::endl;
		}

		t = GetOwner()->GetComp<TransformComp>();

		if (t && pos)
		{
			t->mLocal.mTranslationZ.x = pos->mLocal.mTranslationZ.x;

			//std::cout << "camera pos x" << t->mLocal.mTranslationZ.x << std::endl;

			yDestination = t->mLocal.mTranslationZ.y;
		}

		verticalCam = false;
		camRight = true;
		transitionAxis = false;
		transitionTimer = 0;
		transition = false;
		timeToChange = 0.8f;
	}
	void CameraMovement::Update()
	{
		if (!pos || !t || !cam)
		{
			Initialize();
			return;
		}

		// ADDED BY MIGUEL
		if (StationZoomOut::shouldLockCamera == true || PuzzleLogic::lockCamera == true)
		{
			//std::cout << "SHOULD LOCK CAMERA TRUE\n";
			return;
		}

		// Update the angle for the sin effect
		update_angle();

		// Player is on the veins
		if (stats->onVeins)
		{
			//std::cout << t->mLocal.mTranslationZ.x << std::endl;
			UpdaterVeinsCam();
		}

		// Camera is transitioning to the veins
		else if (stats->onVeinsTransition)
		{
			UpdaterVeinsTransition();
		}

		// General camera logic
		else
		{
			//std::cout << t->mLocal.mTranslationZ.x << std::endl;
			UpdateCamPos();
		}

		if (!verticalCam && !transitionAxis)
		{
			// Reset y for the sin effect
			t->mLocal.mTranslationZ.y = yDestination;
			// Sin shake
			t->mLocal.mTranslationZ.y += a * Sin(angle);
		}
	}
	bool CameraMovement::OnGui()
	{
		bool isChanged = false;

		// Camera Offset
		ImGui::Text("Camera x Offset");
		if (ImGui::InputFloat("camera x Offset", &cameraOffset)) isChanged = true;

		ImGui::Text("Camera change time");
		if (ImGui::InputFloat("time to change", &timeToChange)) isChanged = true;

		ImGui::Text("Camera y Offset");
		if (ImGui::InputFloat("camera y Offset", &cameraYoffset)) isChanged = true;
		
		ImGui::Text("Camera Veins Offset");
		if (ImGui::InputFloat("Camera veins Offset", &veinsOffset)) isChanged = true;

		ImGui::Text("Amplitud of the sin effect");
		if (ImGui::InputFloat("1", &a)) isChanged = true;

		ImGui::Text("Speed of the sin effect");
		if (ImGui::InputFloat("2", &sinSpeed)) isChanged = true;

		ImGui::Text("veins max X");
		if (ImGui::InputFloat("0", &lockXVeins)) isChanged = true;

		ImGui::Text("Max X");
		if (ImGui::InputFloat("3", &xMax)) isChanged = true;

		ImGui::Text("Max Y");
		if (ImGui::InputFloat("4", &yMax)) isChanged = true;

		ImGui::Text("Min X");
		if (ImGui::InputFloat("5", &xMin)) isChanged = true;

		ImGui::Text("Min Y");
		if (ImGui::InputFloat("6", &yMin)) isChanged = true;


		return isChanged;
	}
	void CameraMovement::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);
		value["cameraOffset"] >> cameraOffset;
		value["camerayOffset"] >> cameraYoffset;
		value["veins offset"] >> veinsOffset;
		value["maxMinX"] >> xMax;
		value["maxMinY"] >> yMax;
		value["minX"] >> xMin;
		value["minY"] >> yMin;
		value["sinShake"] >> a;
		value["sinSpeed"] >> sinSpeed;
		value["changeTime"] >> timeToChange;
		value["lock x veins"] >> lockXVeins;
	}
	void CameraMovement::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);
		value["cameraOffset"] << cameraOffset;
		value["camerayOffset"] << cameraYoffset;
		value["veins offset"] << veinsOffset;
		value["maxMinX"] << xMax;
		value["maxMinY"] << yMax;
		value["minX"] << xMin;
		value["minY"] << yMin;
		value["sinShake"] << a;
		value["sinSpeed"] << sinSpeed;
		value["changeTime"] << timeToChange;
		value["lock x veins"] << lockXVeins;
	}
}

