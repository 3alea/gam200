#include "CameraShaker.h"
#include "Components/AEXTransformComp.h"
#include "Composition/AEXGameObject.h"
#include "Platform/AEXTime.h"
#include "Platform/AEXInput.h"
#include "Components/Camera.h"
#include "Utilities/RNG.h"
#include "Environment/Background/StationZoomOut.h"
#include <Imgui/imgui.h>


namespace AEX
{
	void CameraShaker::Initialize()
	{
		// Traume should be initialized to 0, this is just for testing
		mTrauma = 0.0f;
		mSavedPosOnce = false;

		//timer = 0.0f;

		mCamTr = mOwner->GetComp<TransformComp>();
		mCam = mOwner->GetComp<Camera>();

		//GameObject * shakeCam = aexScene->FindObjectInANYSpace("ShakeCamera");

		//if (shakeCam)
		//	mShakeCamTr = shakeCam->GetComp<TransformComp>();
	}

	void CameraShaker::Update()
	{
		if (doneOnce == false)
		{
			Initialize();

			if (mCam && mCamTr)
			{
				// Initialize the original pos variables
				mPosBeforeShake.x = mCamTr->mLocal.mTranslationZ.x;
				mPosBeforeShake.y = mCamTr->mLocal.mTranslationZ.y;
				mRotBeforeShake = mCam->mRotation;
			}

			doneOnce = true;
		}

		//if (aexInput->KeyPressed(GLFW_KEY_SPACE))
		//	mTrauma += 0.1f;

		//timer += (f32)aexTime->GetFrameTime();

		if (mTrauma == 0 && mSavedPosOnce == true)
			mSavedPosOnce = false;

		if (mTrauma != 0 && mSavedPosOnce == false)
		{
			mPosBeforeShake.x = mCamTr->mLocal.mTranslationZ.x;
			mPosBeforeShake.y = mCamTr->mLocal.mTranslationZ.y;
			mRotBeforeShake = mCam->mRotation;

			mSavedPosOnce = true;
		}

		// Code for camera shake
		if (mTrauma > 1)
			mTrauma = 1.0f;

		if (mCamTr && mCam)// && mShakeCamTr)
		{
			if (mTranslationalShake)
			{
				float dirX = 1.0f;
				float dirY = 1.0f;

				float camShakeX = maxX * mTrauma * mTrauma * RNG->GetFloat(-1.0f, 1.0f);
				float camShakeY = maxY * mTrauma * mTrauma * RNG->GetFloat(-1.0f, 1.0f);

				if (((mCamTr->mLocal.mTranslationZ.x + camShakeX) > (mPosBeforeShake.x + maxX)) ||
					((mCamTr->mLocal.mTranslationZ.x + camShakeX) < (mPosBeforeShake.x - maxX)))
					dirX = -1.0f;
				else// if (mCamTr->mLocal.mTranslationZ.x + camShakeX < mCamTr->mLocal.mTranslationZ.x - maxX)
					dirX = 1.0f;

				if (((mCamTr->mLocal.mTranslationZ.y + camShakeY) > (mPosBeforeShake.y + maxY)) ||
					((mCamTr->mLocal.mTranslationZ.y + camShakeY) < (mPosBeforeShake.y - maxY)))
					dirY = -1.0f;
				else// if (mCamTr->mLocal.mTranslationZ.x + camShakeX < mCamTr->mLocal.mTranslationZ.x - maxX)
					dirY = 1.0f;

				mCamTr->mLocal.mTranslationZ.x += dirX * camShakeX;
				mCamTr->mLocal.mTranslationZ.y += dirY * camShakeY;

				//mShakeCamTr->mLocal.mTranslationZ.x = mCamTr->mLocal.mTranslationZ.x + camShakeX;
				//mShakeCamTr->mLocal.mTranslationZ.y = mCamTr->mLocal.mTranslationZ.y + camShakeY;
			}

			if (mRotationalShake)
			{
				float angleDir = 1.0f;

				float camShakeAngle = maxAngle * mTrauma * mTrauma * RNG->GetFloat(-1.0f, 1.0f);

				if (((mCam->mRotation + camShakeAngle) > (mRotBeforeShake + camShakeAngle)) ||
					((mCam->mRotation + camShakeAngle) < (mRotBeforeShake - camShakeAngle)))
					angleDir = -1.0f;

				mCam->mRotation += angleDir * camShakeAngle;
				//mShakeCamTr->mLocal.mOrientation = mCamTr->mLocal.mOrientation + camShakeAngle;
			}
		}

		//if (mTrauma > 0)
		mTrauma -= (f32)aexTime->GetFrameTime() * 0.5f;

		if (mTrauma < 0)
		{
			mTrauma = 0.0f;

			/*if (StationZoomOut::mFirstShakeFinished)
			{
				mCamTr->mLocal.mTranslationZ.x = mPosBeforeShake.x;
				mCamTr->mLocal.mTranslationZ.y = mPosBeforeShake.y;
				mCam->mRotation = mRotBeforeShake;
			}*/
		}
	}

	void CameraShaker::Shutdown()
	{

	}


	bool CameraShaker::OnGui()
	{
		bool isChanged = false;

		if (ImGui::InputFloat("Max X", &maxX))
		{
			isChanged = true;
		}
		if (ImGui::InputFloat("Max Y", &maxY))
		{
			isChanged = true;
		}
		if (ImGui::InputFloat("Max Angle", &maxAngle))
		{
			maxAngle = (maxAngle * PI) / 180.0f;
			isChanged = true;
		}

		if (ImGui::Checkbox("Translational", &mTranslationalShake))
		{
			isChanged = true;
		}
		if (ImGui::Checkbox("Rotational", &mRotationalShake))
		{
			isChanged = true;
		}

		return isChanged;
	}


	void CameraShaker::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);

		value["Maximum X"] >> maxX;
		value["Maximum Y"] >> maxY;
		value["Maximum Angle"] >> maxAngle;

		value["Translational Shake"] >> mTranslationalShake;
		value["Rotational Shake"] >> mRotationalShake;
	}

	void CameraShaker::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);

		value["Maximum X"] << maxX;
		value["Maximum Y"] << maxY;
		value["Maximum Angle"] << maxAngle;

		value["Translational Shake"] << mTranslationalShake;
		value["Rotational Shake"] << mRotationalShake;
	}
}