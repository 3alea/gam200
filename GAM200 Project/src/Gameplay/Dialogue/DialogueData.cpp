#include "DialogueData.h"
#include <fstream>
#include <iostream>

DialogueData::DialogueData(const char * filename)
{
	std::ifstream tDialogueFile;
	tDialogueFile.open(filename); // open file

	if (!tDialogueFile)
	{
		// Print an error and exit
		std::cout << "DialogueSystem ERROR : File unsuccessfully read!" << std::endl;
		return;
	}
	
	while (!tDialogueFile.eof())
	{
		std::string temp;
		std::getline(tDialogueFile, temp);
		mDialogues.push(Dialogue(temp, Dummy()));
	}
}
