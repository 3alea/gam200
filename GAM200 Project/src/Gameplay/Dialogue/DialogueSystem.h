#pragma once
#include "DialogueData.h"

class Camera;

class DialogueSystem : public SpriteText
{
	AEX_RTTI_DECL(DialogueSystem, SpriteText)

	DialogueSystem() {}
	~DialogueSystem();

	virtual void Initialize() override;
	virtual void Update() override;
	virtual void Render(Camera* cam) override;
	virtual void Shutdown() override;

	void StartDialogue();
	void EndDialogue();

	void Enable(bool enable) { mEnabled = enable; }

	bool OnGui();

	void FromJson(json & value);
	void ToJson(json & value);
		
private:
	std::string mDialoguePath;
	DialogueData mDialogueData;
	DialogueData::Dialogue mCurrentDialogue;

	f32 mDialogueSpeed = 0.1f;
	bool mEnabled;

	u32 mStringIterator = 0u;
	f32 mCurrentTime = 0.0f;
};