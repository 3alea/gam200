#include "Components\SpriteText.h"
#include "DialogueSystem.h"
#include "Imgui/imgui.h"
#include "Utilities/AEXAssert.h"
#include "Platform/AEXTime.h"
#include "Editor/Editor.h"

#include <experimental/filesystem>

namespace fs = std::experimental::filesystem;

DialogueSystem::~DialogueSystem()
{
	
}

void DialogueSystem::Initialize()
{
	SpriteText::Initialize();
}

void DialogueSystem::Update()
{
	SpriteText::Update();

	if (Editorstate->IsPlaying())
	{
		if (mCurrentTime >= mDialogueSpeed)
		{
			mCurrentTime = 0.0f;

			if (mStringIterator < mCurrentDialogue.first.size())
			{
				mText[mStringIterator] = mCurrentDialogue.first[mStringIterator];
				mText[mStringIterator + 1] = '\0';
				mStringIterator++;
			}
			else if (!mDialogueData.mDialogues.empty())
			{
				for (u32 i = 0; i < mStringIterator; i++)
					mText[i] = 0;
				mStringIterator = 0u;
				mCurrentDialogue = mDialogueData.mDialogues.front();
				mDialogueData.mDialogues.pop();
			}
			else
				EndDialogue();
		}
		else
			mCurrentTime += (f32)aexTime->GetFrameTime();
	}
}

void DialogueSystem::Render(Camera * cam)
{
	SpriteText::Render(cam);
}

void DialogueSystem::Shutdown()
{
	SpriteText::Shutdown();
}

void DialogueSystem::StartDialogue()
{
}

void DialogueSystem::EndDialogue()
{
}

bool DialogueSystem::OnGui()
{
	bool changed = false;

	float col[4] = { mColor[0], mColor[1], mColor[2], mColor[3] };
	if (ImGui::ColorEdit4("Text Color", col))
		for (int i = 0; i < 4; i++)
			mColor[i] = col[i];

	// Miguel
	static std::string actualName_Font = "Select Here...##1";
	static std::string actualName_Dia = "Select Here...##2";
	static bool isPathShown = false;
	std::string previewName_Font;
	std::string previewName_Dia;

	ImGui::Checkbox("Show Paths", &isPathShown);

	// To solve bug where currently selected texture wouldn't change
	// to only the name as soon as you unclicked the checkbox
	if (!isPathShown)
	{
		previewName_Font = aexResourceMgr->GetNameWithExtension(actualName_Font);
		previewName_Dia = aexResourceMgr->GetNameWithExtension(actualName_Dia);
	}
	else
	{
		previewName_Font = actualName_Font;
		previewName_Font = actualName_Dia;
	}

	if (ImGui::BeginCombo("Dialogue Font", previewName_Font.c_str()))
	{
		try {
			fs::path textureDir = fs::path("data\\Fonts");

			int count = 0;
			for (auto dirIt = fs::recursive_directory_iterator(textureDir);
				dirIt != fs::recursive_directory_iterator();
				dirIt++, ++count)
			{
				//if (dirIt->is_directory())
				//continue;
				if (fs::is_directory(dirIt->path()))
					continue;

				if (dirIt->path().extension().string() == ".meta")
					continue;

				std::string currentFileName;

				if (isPathShown)
					currentFileName = dirIt->path().relative_path().string();
				else
					currentFileName = dirIt->path().filename().string();

				if (ImGui::Selectable((currentFileName + "##" + std::to_string(count)).c_str()))
				{
					actualName_Font = dirIt->path().relative_path().string().c_str();
					mFont = aexResourceMgr->getResource<Font>(actualName_Font.c_str());
				}
			}
		} CATCH_FS_ERROR

		ImGui::EndCombo();
	}

	if (ImGui::BeginCombo("Dialogue File", previewName_Dia.c_str()))
	{
		try {
			fs::path textureDir = fs::path("data\\Dialogues");

			int count = 0;
			for (auto dirIt = fs::recursive_directory_iterator(textureDir);
				dirIt != fs::recursive_directory_iterator();
				dirIt++, ++count)
			{
				//if (dirIt->is_directory())
				//continue;
				if (fs::is_directory(dirIt->path()))
					continue;

				if (dirIt->path().extension().string() == ".meta")
					continue;

				std::string currentFileName;

				if (isPathShown)
					currentFileName = dirIt->path().relative_path().string();
				else
					currentFileName = dirIt->path().filename().string();

				if (ImGui::Selectable((currentFileName + "##" + std::to_string(count)).c_str()))
				{
					actualName_Dia = dirIt->path().relative_path().string().c_str();
					mDialoguePath = actualName_Dia;
					std::cout << "DialogueSystem : opening " << mDialoguePath << "..." << std::endl;
					mDialogueData = DialogueData(actualName_Dia.c_str());
					mCurrentDialogue = mDialogueData.mDialogues.front();
					mDialogueData.mDialogues.pop();
					mStringIterator = 0u;
				}
			}
		} CATCH_FS_ERROR

		ImGui::EndCombo();
	}

	ImGui::InputFloat("Dialogue Speed", &mDialogueSpeed, 0.025f, 0.05f, 3);

	return changed;
}

void DialogueSystem::FromJson(json & value)
{
	try {
		this->Renderable::FromJson(value);

		value["Dialogue Path"] >> mDialoguePath;
		mDialogueData = DialogueData(mDialoguePath.c_str());

		value["Dialogue Speed"] >> mDialogueSpeed;
		value["Dialogue Enabled"] >> mEnabled;

		std::string metaFile;
		value["Meta File"] >> metaFile;

		fs::path metaPath(metaFile);
		json val;

		if (!fs::exists(metaPath))
		{
			metaPath.replace_extension(".ttf");

			mFont = aexResourceMgr->getResource<Font>(metaPath.string().c_str());
			mFont->ToJson(val);
			return;
		}

		LoadFromFile(val, metaFile);

		std::string path;
		val["Relative Path"] >> path;

		mFont = aexResourceMgr->getResource<Font>(path.c_str());

	} CATCH_FS_ERROR
}

void DialogueSystem::ToJson(json & value)
{
	this->Renderable::ToJson(value);

	value["Dialogue Path"] << mDialoguePath;

	value["Dialogue Speed"] << mDialogueSpeed;
	value["Dialogue Enabled"] << mEnabled;

	mFont->ToJson(value);
}
