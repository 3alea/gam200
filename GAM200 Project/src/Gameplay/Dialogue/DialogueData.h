#pragma once
#include "Core/AEXBase.h"
#include <string>
#include <queue>

struct Dummy // Dummy struct to later implement audio
{

};

struct DialogueData
{
	AEX_RTTI_DECL(DialogueData, AEX::IBase)

	using Dialogue = std::pair<std::string, Dummy>;

	DialogueData(const char* filename = "data\\Dialogues\\test.dia");


public:
	std::queue<Dialogue> mDialogues;
};