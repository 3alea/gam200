#include "HeartBeat.h"
#include "Composition/AEXGameObject.h"
#include "Components/AEXTransformComp.h"
#include "Composition/AEXSpace.h"
#include "components/ParticleEmitter.h"
#include "Player/Stats/PlayerStats.h"
#include "AI/Easing.h"
#include "Platform/AEXTime.h"
#include "CameraLogic/CameraShaker.h"
#include "Resources/AEXResourceManager.h"
#include "Audio/AudioManager.h"

namespace AEX
{
	void HeartBeat::Initialize()
	{
		shaker = nullptr;

		if (GameObject* cam = mOwner->GetParentSpace()->FindObject("camera"))
		{
			shaker = cam->GetComp<CameraShaker>();
		}
	}
	void HeartBeat::Update()
	{
		if (!shaker)
		{
			Initialize();
			return;
		}

		if (!activated)
			return;

		if (!doneOnce)
		{
			audiomgr->Loop(aexResourceMgr->getResource<Sound>("data\\Audio\\BoilerRoom\\HeartBeat.mp3"), Voice::SFX);
			doneOnce = true;
		}

		timer += (f32)aexTime->GetFrameTime();

		if (timer >= timeToHB1 && !doneHB1)
		{
			shaker->mTrauma = 0.8f;
			doneHB1 = true;
		}

		if (timer >= timeToHB2)
		{
			shaker->mTrauma = 0.8f;
			timer = 0;
			doneHB1 = false;
		}
	}

	bool HeartBeat::OnGui()
	{
		bool changed = false;
		if (ImGui::InputFloat("time to heart beat 1", &timeToHB1))
		{
			changed = true;
		}

		if (ImGui::InputFloat("time to heart beat 2", &timeToHB2))
		{
			changed = true;
		}
		return changed;
	}
	void HeartBeat::OnCollisionStarted(const CollisionStarted & collision)
	{
	}
	void HeartBeat::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);
		value["hb1"] >> timeToHB1;
		value["hb2"] >> timeToHB2;
	}
	void HeartBeat::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);
		value["hb1"] << timeToHB1;
		value["hb2"] << timeToHB2;
	}
}