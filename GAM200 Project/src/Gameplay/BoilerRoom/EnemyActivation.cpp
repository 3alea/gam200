#include "EnemyActivation.h"
#include "Enemy/Enemy.h"
#include "BoilerRoom/PuzzleLogic.h"

namespace AEX
{
	void EnemyActivation::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);
	}
	void EnemyActivation::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);
	}

	void EnemyActivation::Initialize()
	{
		mEnemy = mOwner->GetComp<Enemy>();
	}
	void EnemyActivation::Update()
	{
		if (PuzzleLogic::mDoorOpening)
		{
			mEnemy->Activate = false;
			mEnemy->enemy_activator = false;
			mEnemy->Shoot_activator = false;
			return;
		}
		if (mEnemy)
		{
			mEnemy->Activate = true;
			mEnemy->enemy_activator = true;
			mEnemy->Shoot_activator = true;
		}
	}
	void EnemyActivation::Shutdown()
	{

	}
}