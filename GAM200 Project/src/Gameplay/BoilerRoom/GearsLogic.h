#pragma once

#include "Components/Logic.h"


class SpineAnimationComp;
class ParticleEmitter;

namespace AEX
{
	class BoxCollider;

	class GearsLogic : public LogicComp
	{
		AEX_RTTI_DECL(GearsLogic, LogicComp);

	public:

		virtual void Initialize();
		virtual void Update();
		virtual void Shutdown();

		void FromJson(json & value);
		void ToJson(json & value);

		bool OnGui();

		void OnCollisionStarted(const CollisionStarted& collision);

		SpineAnimationComp * mGearsAnim = nullptr;
		ParticleEmitter * mParticleEmitter = nullptr;

		int mBulletsImpacted = 0;
		int mMaxBulletsToBreak = 6;

		float mParticleTimer = 0.0f;
		float mParticleTime = 0.5f;

		bool mInitializedOnce = false;

		BoxCollider * mBigCollider = nullptr;
		BoxCollider * mSmallCollider = nullptr;

	private:

	};
}