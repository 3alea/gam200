#include "GearsLogic.h"
#include "Composition/AEXGameObject.h"
#include "Bullets/BulletLogic.h"
#include "Composition/AEXSpace.h"
#include "Components/ParticleEmitter.h"
#include "Platform/AEXTime.h"
#include "Components/SpineAnimation.h"
#include <spine/AnimationState.h>
#include "PuzzleLogic.h"
#include "Components/Collider.h"

namespace AEX
{
	void GearsLogic::Initialize()
	{
		mBulletsImpacted = 0;
		mParticleTimer = 0.0f;

		mGearsAnim = mOwner->GetComp<SpineAnimationComp>();

		GameObject * particleObj = aexScene->FindObjectInANYSpace("e01");

		if (particleObj)
			mParticleEmitter = particleObj->GetComp<ParticleEmitter>();

		GameObject * bigColObj = aexScene->FindObjectInANYSpace("fuelle");
		GameObject * smallColObj = aexScene->FindObjectInANYSpace("FuelleSmallCollider");

		if (bigColObj)
			mBigCollider = bigColObj->GetComp<BoxCollider>();

		if (smallColObj)
			mSmallCollider = smallColObj->GetComp<BoxCollider>();

		if (mBigCollider)
		{
			mBigCollider->mIsGhost = true;
			mBigCollider->collisionGroup = "none";
		}

		if (mSmallCollider)
			mSmallCollider->mIsGhost = false;
	}
	void GearsLogic::Update()
	{
		if (mInitializedOnce == false)
		{
			Initialize();
			mInitializedOnce = true;
		}

		if (mGearsAnim->CheckCurrentAnimationAtPostion("Blow Colesterol", 0) && mGearsAnim->GetAnimState()->getCurrent(0)->isComplete())
		{
			mGearsAnim->ChangeAnimation(0, "going on", true, 1.0f);
		}

		if (mParticleTimer > 0.0f)
		{
			mParticleTimer += (f32)aexTime->GetFrameTime();

			if (mBulletsImpacted <= mMaxBulletsToBreak)
				mParticleEmitter->mEmitProperties->mbParticlesEnabled = true;
		}

		if (mParticleTimer > mParticleTime)
		{
			mParticleEmitter->mEmitProperties->mbParticlesEnabled = false;
			mParticleTimer = 0.0f;
		}
	}
	void GearsLogic::Shutdown()
	{

	}

	bool GearsLogic::OnGui()
	{
		bool isChanged = false;

		if (ImGui::InputInt("Hits To Break", &mMaxBulletsToBreak))
		{
			isChanged = true;
		}
		if (ImGui::InputFloat("Particle Time", &mParticleTime))
		{
			isChanged = true;
		}

		return isChanged;
	}

	void GearsLogic::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);

		value["Max Bullets To Break"] >> mMaxBulletsToBreak;
		value["Particle Time"] >> mParticleTime;
	}
	void GearsLogic::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);

		value["Max Bullets To Break"] << mMaxBulletsToBreak;
		value["Particle Time"] << mParticleTime;
	}

	void GearsLogic::OnCollisionStarted(const CollisionStarted& collision)
	{
		// If it has collided with a bullet from the player
		if (BulletLogic * bulletLogic = collision.otherObject->GetComp<BulletLogic>())
		{
			++mBulletsImpacted;

			if (mBulletsImpacted <= mMaxBulletsToBreak)
				mParticleEmitter->mEmitProperties->mbParticlesEnabled = true;

			if (mParticleTimer == 0.0f)
				mParticleTimer += (f32)aexTime->GetFrameTime();
		}

		if (mBulletsImpacted == mMaxBulletsToBreak)
		{
			mGearsAnim->ChangeAnimation(0, "Blow Colesterol", false, 1.0f);
			mSmallCollider->mIsGhost = true;
			mSmallCollider->collisionGroup = "none";
			mBigCollider->mIsGhost = false;
			mBigCollider->collisionGroup = "Floor";
			PuzzleLogic::active = true;
		}
	}
}