#pragma once
#include "Components\Logic.h"

namespace AEX
{
	class TransformComp;
	class CameraShaker;

	template <typename T> class TResource;

	class HeartBeat : public LogicComp
	{
		AEX_RTTI_DECL(HeartBeat, LogicComp);

	public:
		virtual void Initialize();

		// Update is called once per frame
		virtual void Update();

		CameraShaker* shaker;
		bool activated = false;

		float timer = 0.0f;
		float doneHB1 = false;
		float timeToHB1 = 1.0f;
		float timeToHB2 = 1.0f;
		bool doneOnce = false;

		// OnGui() function
		virtual bool OnGui();

		void OnCollisionStarted(const CollisionStarted& collision);

		void FromJson(json & value);
		void ToJson(json & value);
	};
}