#pragma once
#include "Components\Logic.h"
#include "src/Engine/AI/StateMachine.h"
#include "AI/Path2D.h"
#include "Player\Stats\PlayerStats.h"

class SpineAnimationComp;

namespace AEX
{
	class Rigidbody;
	class TransformComp;
	class FloatingPlatform;
	class CinematicActivator;
	class CameraShaker;

	//struct Floating_Idle : public State
	//{
	//	Floating_Idle(const char* name);
	//
	//	virtual void LogicUpdate();
	//
	//	FloatingPlatform* platform;
	//
	//	Path2D idlePath;
	//
	//	unsigned int time;
	//};

	class PuzzleLogic : public LogicComp//, public Actor
	{
	    AEX_RTTI_DECL(PuzzleLogic, LogicComp);
		
		public:
			PuzzleLogic();

			virtual void Initialize();
			virtual void Update();
			virtual void Shutdown();
			virtual void OnCollisionStarted(const CollisionStarted& collision);
			virtual void OnCollisionEnded(const CollisionEnded& collision);
			virtual bool OnGui();
			void ChangeIdlPath();
			void ChangeSinkedPath();
			void recalculatePaths();
			//void SM_Init();
			
			// FromJson specialized for this component
			virtual void FromJson(json & value);
			// ToJson specialized for this component
			virtual void ToJson(json & value);

			TransformComp* platformT;
			float platformIdlePos;
			float platformSinkedPos;
			bool calculatedPathOnce;
			float timer;
			float easedTime;
			bool playerOnPlatform;
			PlayerStats* stats;

			///////////STATES OF THE PLATFORM/////////
			bool goingDown;
			bool idle;
			bool sinking;
			bool sinked;
			bool recoil;
			bool rising;
			///////////STATES OF THE PLATFORM/////////

			////////////FOR THE EDITOR//////////////
			float sinkRange;      // Sinking parameters
			float sinkTime;	      // Sinking parameters

			float recoilRange;    // Recoil parameters
			float recoilTime;     // Recoil parameters
			////////////FOR THE EDITOR//////////////
			static bool active;

			Path2D sinkPath;
			Path2D risingPath;

			SpineAnimationComp * mFuelleAnim = nullptr;
			bool mPlayedDownOnce = false;
			bool mPlayedUpOnce = false;

			SpineAnimationComp * mMachineAnim = nullptr;
			bool mCanAvivateFire = true;
			int mFireLevel = 0;

			bool doneOnce = false;
			bool doneOnce2 = false;

			static bool lockCamera;

			PlayerStats * mStats = nullptr;
			GameObject * mCinematicActivator = nullptr;
			bool mColliderAdded = false;
			bool mOpenDoorActive = false;

			SpineAnimationComp * mDoorAnim = nullptr;

			TransformComp * mCamTr = nullptr;
			CameraShaker * mShaker = nullptr;

			float mCameraRightOffset = 0.0f;
			float mOriginalX = 0.0f;

			float mCamMoveTimer = 0.0f;
			float mCamMoveTime = 4.0f;

			bool mDoorOpened = false;
			static bool mDoorOpening;

			GameObject * mCinematicEnder = nullptr;
			bool mFinishedDoorCinematic = false;

			void UpdateMachineAnimations();
			void OpenDoorLogic();
	};
}
