#include "PuzzleLogic.h"
#include "Components/AEXTransformComp.h"
#include "Imgui\imgui.h"
#include "Components/Rigidbody.h"
#include "Components/SpineAnimation.h"
#include "AI/Easing.h"
#include <iostream>
#include "Player/Movement/PlayerController.h"
#include "Platform/AEXInput.h"
#include "Composition\AEXSpace.h"
#include <stdlib.h>
#include <spine/AnimationState.h>
#include "Environment/Background/StationZoomOut.h"
#include "Activator/CinematicActivator.h"
#include "CameraLogic/CameraShaker.h"
#include "BoilerRoom/HeartBeat.h"


namespace AEX
{
	bool PuzzleLogic::active = false;
	bool PuzzleLogic::lockCamera = false;
	bool PuzzleLogic::mDoorOpening = false;

	PuzzleLogic::PuzzleLogic()
		: idle(true)
		, timer(0)
		, goingDown(true)
		, sinking(false)
		, calculatedPathOnce(false)
		, sinkRange(0)
		, sinkTime(0)
		, recoil(false)
		, recoilRange(0)
		, recoilTime(0)
		, playerOnPlatform(false)
		, rising(false)
		, stats(nullptr)
	{}

	void PuzzleLogic::Initialize()
	{
		active = false;
		timer = 0;
		idle = true;
		sinking = false;
		sinked = false;
		recoil = false;
		playerOnPlatform = false;

		platformT = mOwner->GetComp<TransformComp>();

		GameObject* go = mOwner->GetParentSpace()->FindObject("player");

		if (go)
			stats = go->GetComp<PlayerStats>();

		if (platformT)
		{
			platformIdlePos = platformT->mLocal.mTranslationZ.y;
			platformSinkedPos = platformIdlePos - sinkRange;
		}

		GameObject * fuelleObj = aexScene->FindObjectInANYSpace("FuelleAnim");

		if (fuelleObj)
			mFuelleAnim = fuelleObj->GetComp<SpineAnimationComp>();

		mPlayedDownOnce = false;
		mPlayedUpOnce = false;

		GameObject * machineObj = aexScene->FindObjectInANYSpace("Machine");

		if (machineObj)
			mMachineAnim = machineObj->GetComp<SpineAnimationComp>();

		mCanAvivateFire = true;
		mFireLevel = 0;

		lockCamera = false;
		StationZoomOut::shouldLockCamera = false;

		GameObject * player = aexScene->FindObjectInANYSpace("player");

		if (player)
			mStats = player->GetComp<PlayerStats>();

		mCinematicActivator = aexScene->FindObjectInANYSpace("CinematicActivator");

		mColliderAdded = false;

		mOpenDoorActive = false;
		//if (cinematicActivatorObj)
		//	mCinematicActivator = cinematicActivatorObj->GetComp<CinematicActivator>();

		GameObject * doorObj = aexScene->FindObjectInANYSpace("Door");

		if (doorObj)
			mDoorAnim = doorObj->GetComp<SpineAnimationComp>();

		GameObject * camObj = aexScene->FindObjectInANYSpace("camera");

		if (camObj)
		{
			mCamTr = camObj->GetComp<TransformComp>();
			mShaker = camObj->GetComp<CameraShaker>();
		}

		mDoorOpened = false;
		mDoorOpening = false;

		mCinematicEnder = aexScene->FindObjectInANYSpace("CinematicEnder");

		mFinishedDoorCinematic = false;
	}
	
	void PuzzleLogic::Update()
	{
		if (!platformT || !stats || !mFuelleAnim || !mStats)
			Initialize();

		if (doneOnce == false)
		{
			// Set initial state of the machine
			mMachineAnim->ChangeAnimation(0, "Fire Starting", false, 1.0f);
			mMachineAnim->GetAnimState()->getCurrent(0)->setTrackTime(mMachineAnim->GetAnimState()->getCurrent(0)->getAnimationStart());
			mMachineAnim->SetDt(0.0f);
			mMachineAnim->SetPauseState(true);

			// Set initial state of the fuelle
			mFuelleAnim->ChangeAnimation(0, "Compressed", false, 1.0f);
			//mFuelleAnim->GetAnimState()->getCurrent(0)->setTrackTime(mMachineAnim->GetAnimState()->getCurrent(0)->getAnimationStart());
			//mFuelleAnim->SetDt(0.0f);
			//mFuelleAnim->SetPauseState(true);

			doneOnce = true;
		}

		if (!active)
			return;

		if (doneOnce2 == false)
		{
			// Put initial animation
			mFuelleAnim->ChangeAnimation(0, "Up", false, 1.0f);
			mFuelleAnim->SetPauseState(false);

			doneOnce2 = true;
		}

		//if (mFuelleAnim->CheckCurrentAnimationAtPostion("Down", 0) && mFuelleAnim->GetAnimState()->getCurrent(0)->isComplete())
		//	mFuelleAnim->ChangeAnimation(0, "Up", false, 1.0f);

		// Check if the player jumps out of the platform
		if (playerOnPlatform && PLAYER_JUMPING && !stats->onCinematic)
		{
			recoil = true;
			idle = false;
			sinking = false;
			sinked = false;
			calculatedPathOnce = false;
			timer = 0;
			playerOnPlatform = false;
			//std::cout << "RECOIL" << std::endl;
		}

		if (mOpenDoorActive)
			OpenDoorLogic();

		if (mFireLevel == 4 && mColliderAdded == false)
		{
			lockCamera = true;
			mStats->StartCinematic(false);

			/*BoxCollider * collider = new BoxCollider;
			collider->mIsGhost = true;
			collider->mRealScale = { 3.0f, 0.5f };
			mCinematicActivator->AddComp(collider);*/
			mCinematicActivator->GetComp<CinematicActivator>()->StartCinematic();
			mColliderAdded = true;

			mOriginalX = mCamTr->mLocal.mTranslationZ.x;

			aexScene->FindObjectInANYSpace("WaveSystem")->GetComp<HeartBeat>()->activated = true;
			aexScene->FindObjectInANYSpace("Door")->GetComp<BoxCollider>()->mIsGhost = true;

			mOpenDoorActive = true;
		}

		/////////////////////////IDLE////////////////////////
		if (idle || sinked)
			return;
		/////////////////////////IDLE////////////////////////

		///////////////////////SINKING///////////////////////
		if (sinking)
		{
			mPlayedUpOnce = false;

			if (mPlayedDownOnce == false)
			{
				mFuelleAnim->ChangeAnimation(0, "Down", false, 1.0f, 1.0f);
				audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/BoilerRoom/FuelleDownSfx.mp3"), Voice::SFX);
				mPlayedDownOnce = true;
			}

			// Calculate the sink path
			if (calculatedPathOnce == false)
			{
				sinkPath.Clear();
				// Path goes from current position to sinked posision
				sinkPath.Push(0, AEVec2(0, platformT->mLocal.mTranslationZ.y));
				sinkPath.Push(1, AEVec2(0, platformSinkedPos));
				calculatedPathOnce = true;
			}

			// Ease the timer
			easedTime = EaseOutQuad(timer / sinkTime);

			// Move the platform in the y axis
			platformT->mLocal.mTranslationZ.y = sinkPath.SampleTime(easedTime).y;

			// Update the timer
			timer += (f32)aexTime->GetFrameTime();

			// Check if the platform has reached the end of the path
			if (timer >= sinkTime)
			{
				timer = 0;

				sinking = false;
				sinked = true;
				calculatedPathOnce = false;
				goingDown = false;
				//std::cout << "SINKED" << std::endl;

				audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/BoilerRoom/FuelleImpactSfx.mp3"), Voice::SFX);

				if (mCanAvivateFire)
				{
					//mMachineAnim->ChangeAnimation(0, )
					UpdateMachineAnimations();
					mCanAvivateFire = false;
				}
			}

			return; // End the "State"
		}
		///////////////////////SINKING///////////////////////

		////////////////////////RECOIL///////////////////////
		if (recoil)
		{
			// Update the timer
 			timer += (f32)aexTime->GetFrameTime();

			// Check if the platform has reached the end of the path
			if (timer >= recoilTime)
			{
				recoil = false;
				timer = 0;
				calculatedPathOnce = false;
				rising = true;
			}

			return; // End the "State"
		}
		////////////////////////RECOIL///////////////////////

		////////////////////////RISING///////////////////////
		if (rising)
		{
			mPlayedDownOnce = false;

			if (mPlayedUpOnce == false)
			{
				mFuelleAnim->ChangeAnimation(0, "Up", false, 1.0f, 1.0f);
				audiomgr->Play(aexResourceMgr->getResource<Sound>("data/Audio/BoilerRoom/FuelleUp.mp3"), Voice::SFX);
				mPlayedUpOnce = true;
			}

			if (calculatedPathOnce == false)
			{
				risingPath.Clear();
				// From current y pos
				risingPath.Push(0, AEVec2(0, platformT->mLocal.mTranslationZ.y));
				risingPath.Push(1, AEVec2(0, platformIdlePos));
				calculatedPathOnce = true;
			}

			// Ease the timer
			easedTime = EaseInOutQuad(timer / sinkTime);

			// Move the platform in the y axis
			platformT->mLocal.mTranslationZ.y = risingPath.SampleTime(easedTime).y;

			// Update the timer
			timer += (f32)aexTime->GetFrameTime();

			if (timer > 0.8f)
				mCanAvivateFire = true;

			// Check if the platform has reached the end of the path
			if (timer >= sinkTime)
			{
				timer = 0;
				calculatedPathOnce = false;
				rising = false;
				idle = true;
				goingDown = true;
				//std::cout << "IDLE" << std::endl;
			}

			return; // End the "State"
		}
		////////////////////////RISING///////////////////////
	}

	void PuzzleLogic::Shutdown()
	{
		lockCamera = false;
	}

	void PuzzleLogic::UpdateMachineAnimations()
	{
		mMachineAnim->SetPauseState(false);

		if (mFireLevel == 0)
		{
			mMachineAnim->ChangeAnimation(0, "Starting", true, 1.0f, 1.0f);
			mMachineAnim->ChangeAnimation(1, "Fire Starting", false, 1.0f, 1.0f);
		}
		else if (mFireLevel == 1)
		{
			mMachineAnim->ChangeAnimation(0, "Faster", true, 1.0f, 1.0f);
			mMachineAnim->ChangeAnimation(1, "FireLit", true, 1.0f, 1.0f);
		}
		else if (mFireLevel == 2)
		{
			mMachineAnim->ChangeAnimation(0, "Even Faster", true, 1.0f, 1.0f);
		}
		else if (mFireLevel == 3)
		{
			mMachineAnim->ChangeAnimation(0, "Full Going On", true, 1.0f, 1.0f);
			mMachineAnim->ChangeAnimation(1, "Fire Full Power", true, 1.0f, 1.0f);
		}

		++mFireLevel;

		if (mFireLevel > 4)
			mFireLevel = 4;
	}

	void PuzzleLogic::OpenDoorLogic()
	{
		if (mFinishedDoorCinematic)
			return;

		if (mDoorOpened)
		{
			float tn = mCamMoveTimer / mCamMoveTime;
			float easedTn = EaseInOutQuad(tn);

			float lerpResult = AEX::Lerp<float>(mOriginalX + mCameraRightOffset, mOriginalX, easedTn);

			mCamTr->mLocal.mTranslationZ.x = lerpResult;

			mCamMoveTimer += (f32)aexTime->GetFrameTime();

			if (mCamMoveTimer > mCamMoveTime)
			{
				mCamMoveTimer = mCamMoveTime;
				/*BoxCollider * collider = new BoxCollider;
				collider->mIsGhost = true;
				collider->mRealScale = { 3.0f, 3.0f };
				mCinematicEnder->AddComp(collider);*/
				mCinematicActivator->GetComp<CinematicActivator>()->EndCinematic();
				mFinishedDoorCinematic = true;
				mStats->StopCinematic();
				lockCamera = false;
			}

			return;
		}

		if (mDoorAnim->CheckCurrentAnimationAtPostion("Open", 0) && mDoorAnim->GetAnimState()->getCurrent(0)->isComplete())
		{
			mDoorOpening = false;
			mDoorOpened = true;
			mCamMoveTimer = 0.0f;
			return;
		}

		if (mDoorOpening)
			return;

		float tn = mCamMoveTimer / mCamMoveTime;
		float easedTn = EaseInOutQuad(tn);

		float lerpResult = AEX::Lerp<float>(mOriginalX, mOriginalX + mCameraRightOffset, easedTn);

		mCamTr->mLocal.mTranslationZ.x = lerpResult;

		mCamMoveTimer += /*mCamDir * */(f32)aexTime->GetFrameTime();

		if (mCamMoveTimer > mCamMoveTime)
		{
			mDoorAnim->SetPauseState(false);
			mCamMoveTimer = mCamMoveTime;
			mShaker->mTrauma = 0.7f;
			mDoorAnim->ChangeAnimation(0, "Open", false, 1.0f);
			mDoorOpening = true;
			mCinematicActivator->RemoveCompType(BoxCollider::TYPE());
		}
	}
	
	void PuzzleLogic::OnCollisionStarted(const CollisionStarted & collision)
	{
		// Time to sink
		if (collision.otherObject->GetBaseNameString() == "foot")
		{
			playerOnPlatform = true;

			if (idle == true)
			{
				sinking = true;
				idle = false;
				timer = 0;
				//std::cout << "SINK" << std::endl;
			}

			if (rising == true)
			{
				rising = false;
				sinking = true;
				timer = 0;
				calculatedPathOnce = false;
				//std::cout << "SINK" << std::endl;
			}

			//mFuelleAnim->ChangeAnimation(0, "Down", false, 1.0f);
		}
	}
	
	void PuzzleLogic::OnCollisionEnded(const CollisionEnded & collision)
	{
		// Time to rise
 		if (collision.otherObject->GetBaseNameString() == "foot")
		{
			//std::cout << "collision ended" << std::endl;
			if (playerOnPlatform == true)
			{
				rising = true;
				idle = false;
				sinked = false;
				sinking = false;
				recoil = false;
				playerOnPlatform = false;
				calculatedPathOnce = false;
				//std::cout << "RISE" << std::endl;
				timer = 0;
			}
		}
	}
	
	bool PuzzleLogic::OnGui()
	{
		bool changed = false;

		///////////////////////SINKING///////////////////////
		ImGui::Text("\nSinking Options:");
		ImGui::Text("Sinking Range");
		if (ImGui::InputFloat("Sinking Range", &sinkRange))
		{
			changed = true;
		}
		ImGui::Text("Time to sink");
		if (ImGui::InputFloat("Time to sink", &sinkTime))
		{
			changed = true;
		}
		///////////////////////SINKING///////////////////////

		////////////////////////RECOIL///////////////////////
		ImGui::Text("Recoil Options:");
		ImGui::Text("Recoil range");
		if (ImGui::InputFloat("recoil range", &recoilRange))
		{
			changed = true;
		}
		ImGui::Text("Recoil Time");
		if (ImGui::InputFloat("recoil time", &recoilTime))
		{
			changed = true;
		}
		////////////////////////RECOIL///////////////////////

		ImGui::NewLine();

		if (ImGui::InputFloat("Camera Right Offset", &mCameraRightOffset))
			changed = true;

		if (ImGui::InputFloat("Move Time", &mCamMoveTime))
			changed = true;

		return changed;
	}

	void PuzzleLogic::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);

		value["sink range"]   >> sinkRange;      // Sink parameters
		value["Time to sink"] >> sinkTime;       // Sink parameters

		value["recoil range"]  >> recoilRange;    // Recoil parameters
		value["recoil time"]   >> recoilTime;     // Recoil parameters

		value["Camera Right Offset"] >> mCameraRightOffset;
		value["Camera Move Time"] >> mCamMoveTime;
	}
	void PuzzleLogic::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);
										  				     
		value["sink range"]	 << sinkRange;      // Sink parameters
		value["Time to sink"] << sinkTime;       // Sink parameters

		value["recoil range"] << recoilRange;    // Recoil parameters
		value["recoil time"] << recoilTime;     // Recoil parameters

		value["Camera Right Offset"] << mCameraRightOffset;
		value["Camera Move Time"] << mCamMoveTime;
	}
}
