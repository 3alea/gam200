#pragma once

#include "Components/Logic.h"

namespace AEX
{
	class Enemy;

	class EnemyActivation : public LogicComp
	{
		AEX_RTTI_DECL(EnemyActivation, LogicComp);

	public:

		void FromJson(json & value);
		void ToJson(json & value);

		virtual void Initialize();
		virtual void Update();
		virtual void Shutdown();

		Enemy * mEnemy = nullptr;

		bool doneOnce;

	private:
	};
}