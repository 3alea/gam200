#include "FollowAnObject.h"
#include "Composition/AEXScene.h"
#include "Components\Camera.h"
#include "Graphics\WindowMgr.h"

namespace AEX
{
	FollowAnObject::FollowAnObject() : followerT(NULL), followedT(NULL), mOffset(AEVec2(0,0)) {}

	void FollowAnObject::ChangePos()
	{
		if (followerT && followedT)
		{
			followerT->mLocal.mTranslationZ.x = followedT->mLocal.mTranslationZ.x + mOffset.x;
			followerT->mLocal.mTranslationZ.y = followedT->mLocal.mTranslationZ.y + mOffset.y;
			followerT->mWorld.mTranslationZ.x = followedT->mWorld.mTranslationZ.x + mOffset.x;
			followerT->mWorld.mTranslationZ.y = followedT->mWorld.mTranslationZ.y + mOffset.y;

		}
	}

	void FollowAnObject::Initialize()
	{
		followerT = mOwner->GetComp<TransformComp>();
		mMyPrevPos = followerT->mLocal.mTranslationZ;

		GameObject* go;
		go = aexScene->FindObjectInANYSpace(followedName);

		if (go)
		{
			followedT = go->GetComp<TransformComp>();
			mTargetPrevPos = followedT->mLocal.mTranslationZ;
		}

		GameObject * camObj = aexScene->FindObjectInANYSpace("camera");

		if (camObj)
		{
			f32 aspect = (f32)WindowMgr->GetCurrentWindow()->GetWidth() / WindowMgr->GetCurrentWindow()->GetHeight();

			AEVec2 size = followerT->mLocal.mScale;

			camera = camObj->GetComp<Camera>();
			mNormalizedOffset = AEVec2(mOffset.x / (camera->mViewRectangleSize * aspect), mOffset.y / camera->mViewRectangleSize);
			mNormalizedMeterOffset = AEVec2(meterOffset.x / (camera->mViewRectangleSize * aspect), meterOffset.y / camera->mViewRectangleSize);
			mNormalizedSize = AEVec2(size.x / (camera->mViewRectangleSize * aspect), size.y / camera->mViewRectangleSize);
		}
		else
		{
			mNormalizedOffset = mOffset;
			mNormalizedMeterOffset = meterOffset;
		}
	}

	void FollowAnObject::Update()
	{
		// Check for a wrong initialization
		if (!followedT || !followerT || !camera)
		{
			if (aexScene->FindObjectInANYSpace(followedName))
			{
				Initialize();
			}

			// No object with that name
			else
				return;
		}

		if (mbUseProportions && GetOwner()->GetComp<Camera>() == NULL)
		{
			// get the current position of the followed
			auto diff = followedT->mLocal.mTranslationZ - mTargetPrevPos;
			auto myMove = diff * mMoveRatio;
			followerT->mLocal.mTranslationZ.x += myMove.x;
			followerT->mLocal.mTranslationZ.y += myMove.y;
			mTargetPrevPos = followedT->mLocal.mTranslationZ;
			return;
		}

		// Update the position of the follower
		if (mParentToCamera)
		{
			Camera * camera = aexScene->FindObjectInANYSpace("camera")->GetComp<Camera>();

			f32 aspect = (f32)WindowMgr->GetCurrentWindow()->GetWidth() / WindowMgr->GetCurrentWindow()->GetHeight();

			AEVec2 actualOffset = AEVec2(mNormalizedOffset.x * camera->mViewRectangleSize * aspect, mNormalizedOffset.y * camera->mViewRectangleSize);
			AEVec2 actualMeterOffset = AEVec2((meterOffset.x * (camera->mViewRectangleSize / 5.f)) + (mNormalizedMeterOffset.x * camera->mViewRectangleSize * aspect), mNormalizedMeterOffset.y * camera->mViewRectangleSize);
			AEVec2 actualSize = AEVec2(mNormalizedSize.x * camera->mViewRectangleSize * aspect, mNormalizedSize.y * camera->mViewRectangleSize);

			followerT->mLocal.mScale = actualSize;
			followerT->mLocal.mTranslationZ.x = followedT->mLocal.mTranslationZ.x + actualOffset.x + actualMeterOffset.x;/* *((f32)WindowMgr->GetCurrentWindow()->GetWidth() / (f32)WindowMgr->GetCurrentWindow()->GetHeight()) + mOffset.x + meterOffset.x;*/
			followerT->mLocal.mTranslationZ.y = followedT->mLocal.mTranslationZ.y + actualOffset.y + actualMeterOffset.y;/* +mOffset.y + meterOffset.y;*/
		}
		else
		{
			followerT->mLocal.mTranslationZ.x = followedT->mLocal.mTranslationZ.x + mOffset.x + meterOffset.x;
			followerT->mLocal.mTranslationZ.y = followedT->mLocal.mTranslationZ.y + mOffset.y + meterOffset.y;
		}
		
		if (mbUseLimits)
			CheckLimits();
	}

	void FollowAnObject::CheckLimits()
	{
		if (followerT->mLocal.mTranslationZ.x < mMinLimits.x)
			followerT->mLocal.mTranslationZ.x = mMinLimits.x;
		else if (followerT->mLocal.mTranslationZ.x > mMaxLimits.x)
			followerT->mLocal.mTranslationZ.x = mMaxLimits.x;

		if (followerT->mLocal.mTranslationZ.y < mMinLimits.y)
			followerT->mLocal.mTranslationZ.y = mMinLimits.y;
		else if (followerT->mLocal.mTranslationZ.y > mMaxLimits.y)
			followerT->mLocal.mTranslationZ.y = mMaxLimits.y;
	}

	bool FollowAnObject::OnGui()
	{
		bool changed = false;

		if(ImGui::Checkbox("use proportions", &mbUseProportions)) changed = true;
		if (ImGui::InputFloat("move ratio", &mMoveRatio)) changed = true;

		ImGui::Text("Offset of the follower");
		if (ImGui::InputFloat2("Offset", mOffset.v))
		{
			Initialize();
			ChangePos();
			changed = true;
		}

		ImGui::Text("name of the object to follow");
		if (ImGui::InputText("name", followedName, IM_ARRAYSIZE(followedName)))
		{
			Initialize();
			changed = true;
		}

		if (ImGui::Checkbox("Parent To Camera", &mParentToCamera))
			changed = true;

		if (ImGui::Checkbox("Use Limits on Follow", &mbUseLimits))
			changed = true;

		if (mbUseLimits)
		{
			if (ImGui::InputFloat2("Minimum limits", mMinLimits.v, 3))
				changed = true;

			if (ImGui::InputFloat2("Maximum limits", mMaxLimits.v, 3))
				changed = true;
		}

		return changed;
	}

	void FollowAnObject::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);

		std::string name;
		value["followed name"] >> name;
		strcpy_s(followedName, name.c_str());
		value["mOffset"] >> mOffset;
		value["Use Proportions"] >> mbUseProportions;
		value["Move Ratio"] >> mMoveRatio;
		value["Parent To Camera"] >> mParentToCamera;

		value["UseLimits"] >> mbUseLimits;
		value["MinLimits"] >> mMinLimits;
		value["MaxLimits"] >> mMaxLimits;
	}

	void FollowAnObject::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);

		std::string name = followedName;
		value["followed name"] << name;

		value["Use Proportions"] << mbUseProportions;
		value["Move Ratio"] << mMoveRatio;
		value["mOffset"] << mOffset;
		
		value["Parent To Camera"] << mParentToCamera;

		value["UseLimits"] << mbUseLimits;
		value["MinLimits"] << mMinLimits;
		value["MaxLimits"] << mMaxLimits;
	}
}
