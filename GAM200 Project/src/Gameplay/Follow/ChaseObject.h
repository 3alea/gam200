#pragma once

//#include "Components/Logic.h"
#include "Enemy/Enemy.h"

class ParticleEmitter;

namespace AEX
{
	class TransformComp;

	class ChaseObject : public Enemy//LogicComp
	{
		AEX_RTTI_DECL(ChaseObject, LogicComp);

	public:

		ChaseObject();

		void Initialize();
		void Update();
		void Shutdown();

		bool OnGui();

		void FromJson(json & value);
		void ToJson(json & value);

		void OnCollisionStarted(const CollisionStarted& collision);

		char mToChaseName[64];
		f32 mChaseSpeed;

		TransformComp * mOwnerTr = nullptr;
		TransformComp * mToChaseTr = nullptr;

		bool mOnCinematic = false;

		//int mLives;

		bool doneOnce = false;

		bool mDead = false;

		int mParticleId;
		static int mParticleIdGenerator;

		ParticleEmitter * mEmitter = nullptr;

	private:

	};
}