#pragma once
#include "./Components/Logic.h"
using namespace AEX;

struct CameraLimitActivator : public LogicComp
{
	AEX_RTTI_DECL(CameraLimitActivator, LogicComp)

public:
	virtual void Update();
	virtual void ShutDown();

	virtual bool OnGui();
	// FromJson specialized for this component
	virtual void FromJson(json & value);
	//// ToJson specialized for this component
	virtual void ToJson(json & value);

	virtual void OnCollisionEvent(const CollisionEvent& collision);

private:
	AEVec2 mMinLimits;
	AEVec2 mMaxLimits;
};