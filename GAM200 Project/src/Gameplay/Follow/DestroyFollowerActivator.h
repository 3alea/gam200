#pragma once
#include "./Components/Logic.h"
#include "Components/AEXTransformComp.h"
using namespace AEX;

class DestroyFollowerActivator : public LogicComp
{
	AEX_RTTI_DECL(DestroyFollowerActivator, LogicComp)

public:

	virtual void OnCollisionEvent(const CollisionEvent& collision);
};