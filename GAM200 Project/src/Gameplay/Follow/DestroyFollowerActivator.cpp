#include "DestroyFollowerActivator.h"
#include "Composition/AEXScene.h"
#include "FollowAnObject.h"

void DestroyFollowerActivator::OnCollisionEvent(const CollisionEvent & collision)
{
	if (!strcmp(collision.otherObject->GetBaseName(), "foot"))
		if (GameObject* cam = aexScene->FindObjectInANYSpace("camera"))
			if (FollowAnObject* followComp = cam->GetComp<FollowAnObject>())
				cam->RemoveComp(followComp);
}