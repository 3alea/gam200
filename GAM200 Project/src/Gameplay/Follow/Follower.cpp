#include "Composition/AEXScene.h"
#include "Platform/AEXTime.h"
#include "Follower.h"
#include "Platform/AEXInput.h"

using namespace AEX;
using namespace ImGui;

void Follower::OffsetUpdaterGunV2()
{
	bool right = stats->isRight;
	if (right)
	{
		// The player is aiming vertically
		if (stats->isUp && !PLAYER_GOING_RIGHT)
		{
			InitialVelocity.y = 1.0f;
			InitialVelocity.x = 0.0f;

			p0Offset = rightUp;

			p0Offset.x = 0.8f;
			p0Offset.y = 1.5f;
		}

		// The player is aiming diagonally
		else if (right && stats->isUp)
		{
			InitialVelocity.y = 1.0f;
			InitialVelocity.x = 1.0f;

			p0Offset = rightRightUp;

			p0Offset.x = 1.7f;
			p0Offset.y = 1.1f;
		}

		// The player is not aiming vertically
		else if (stats->canDuck)
		{
			InitialVelocity.x = 1.0f;
			InitialVelocity.y = 0.0f;

			p0Offset = rightRight;

			p0Offset.x = 2.1f;
			p0Offset.y = 0.21f;
		}

		// Player is ducking
		if(!stats->canDuck)
		{
			InitialVelocity.x = 1.0f;
			InitialVelocity.y = 0.0f;

			p0Offset = duckRight;

			p0Offset.x = 2.1f;
			p0Offset.y = -0.2f;
		}

		// If player is in the air and looking down
		if (!stats->isGrounded && PLAYER_GOING_DOWN)
		{
			InitialVelocity.x = 0.0f;
			InitialVelocity.y = -1.0f;

			p0Offset.x = rightUp.x;
			p0Offset.y = -rightUp.y;

			p0Offset.x = 0.8f;
			p0Offset.y = -1.5f;
		}
	}

	// Player looking at the left
	else if (bool left = !stats->isRight)
	{
		// The player is aiming vertically
		if (stats->isUp && !PLAYER_GOING_LEFT)
		{
			InitialVelocity.y = 1.0f;
			InitialVelocity.x = 0.0f;

			p0Offset = leftUp;
	
			p0Offset.x = -0.2f;
			p0Offset.y = 1.5f;
		}
	
		// The player is aiming diagonally
		else if (left && stats->isUp)
		{
			InitialVelocity.y = 1.0f;
			InitialVelocity.x = -1.0f;
			
			p0Offset = leftLeftUp;

			p0Offset.x = -1.1f;
			p0Offset.y = 1.1f;
		}
	
		// The player is not aiming vertically
		else if (stats->canDuck)
		{
			InitialVelocity.x = -1.0f;
			InitialVelocity.y = 0.0f;
			
			p0Offset = leftLeft;

			p0Offset.x = -1.5f;
			p0Offset.y = 0.21f;
		}
	
		// Player is ducking
		if(!stats->canDuck)
		{
			InitialVelocity.x = -1.0f;
			InitialVelocity.y = 0.0f;
			
			p0Offset = duckLeft;

			p0Offset.x = -1.5f;
			p0Offset.y = -0.2f;
		}

		// If player is in the air and looking down
		if (!stats->isGrounded && PLAYER_GOING_DOWN)
		{
			InitialVelocity.x = 0.0f;
			InitialVelocity.y = -1.0f;

			p0Offset.x = leftUp.x;
			p0Offset.y = -leftUp.y;

			p0Offset.x = -0.2f;
			p0Offset.y = -1.5f;
		}
	}

	t->mLocal.mTranslationZ.x = pos->mLocal.mTranslationZ.x + p0Offset.x;
	t->mLocal.mTranslationZ.y = pos->mLocal.mTranslationZ.y + p0Offset.y;
}

void Follower::OffsetUpdaterGun()
{
	// Ground updater
	if (stats->footColliding == true)
	{
		stats->isGrounded = true;
	}

	// If the player is looking towards the right
	if (stats->isRight)
		offset.x = offsetRememberX;
	else
		offset.x = -offsetRememberX;

	// If the player is looking up and not ducking
	if (stats->isUp && stats->canDuck)
	{
		// If the player is not moving
		if (!PLAYER_GOING_RIGHT && !PLAYER_GOING_LEFT)
		{
			offset.x = 0.f;
		}
		offset.y = offsetRememberY;
	}
	else
	{
		offset.y = 0.f;
	}

	// If player is in the air and looking down
	if (!stats->isGrounded && PLAYER_GOING_DOWN)
	{
		offset.x = 0.f;
		offset.y = -offsetRememberY;
	}
	else if (PLAYER_GOING_DOWN)
	{
		offset.y = duckOffset;
	}

	// Ground updater
	stats->isGrounded = false;
}

void Follower::Initialize()
{
	player = aexScene->FindObjectInSpace("player", "Main");
	if (player)
	{
		pos = player->GetComp<TransformComp>();
		stats = player->GetComp<PlayerStats>();
		stats->onVeins = false;
		stats->onVeinsTransition = false;
	}

	t = GetOwner()->GetComp<TransformComp>();

	if (player)
	{
		t->mLocal.mTranslationZ.x = pos->mLocal.mTranslationZ.x + 1;
	}
	else
		offsetRememberX = offset.x;

	// Remember the y's offset
	offsetRememberY = offset.y;
}

void Follower::Update()
{
	// If does not load correctly...
	if (!player)
	{
		Initialize();
	}

	// Conditionals
	if (isGun)
	{
		OffsetUpdaterGunV2();
	}

	else if (isFoot)
	{
		t->mLocal.mTranslationZ.y = pos->mLocal.mTranslationZ.y - 1.0f;
		
		if (stats->isRight)
			t->mLocal.mTranslationZ.x = pos->mLocal.mTranslationZ.x + 0.5f;
		else
			t->mLocal.mTranslationZ.x = pos->mLocal.mTranslationZ.x;

	}

	else
	{
		// Camera offset addition
		t->mLocal.mTranslationZ = pos->mLocal.mTranslationZ + offset + generalOffset;
	}
}

bool Follower::OnGui()
{
	// Collapsing header - when clicked, opens up to show the rest of edited variables
	if (ImGui::CollapsingHeader("Follower"))
	{
		// isChanged boolean to return if something has changed
		bool isChanged = false;

		// Player direction
		ImGui::Text("isGun");
		if(ImGui::Checkbox("isGun", &isGun))
		{
			isChanged = true;
		}

		ImGui::Text("isFoot");
		if (ImGui::Checkbox("foot", &isFoot))
		{
			isChanged = true;
		}

		// General offset
		float generaloffSet[2]{ generalOffset.x, generalOffset.y };
		ImGui::Text("General offset");
		if (ImGui::InputFloat2("General offset", generaloffSet)) isChanged = true;
		generalOffset.x = generaloffSet[0];
		generalOffset.y = generaloffSet[1];

		// Ducking offset
		ImGui::Text("Duck offset");
		if (ImGui::InputFloat("Duck offset", &duckOffset)) isChanged = true;

		ImGui::Text("Just for the gun");
		ImGui::Text("Diagonal Right Launching Position");
		if (ImGui::InputFloat2("Shooting diagonally right", rightRightUp.v))
		{
			isChanged = true;
		}

		ImGui::Text("Diagonal left Launching Position");
		if (ImGui::InputFloat2("Shooting diagonally left", leftLeftUp.v))
		{
			isChanged = true;
		}

		ImGui::Text("Shooting right duck");
		if (ImGui::InputFloat2("duck R launch pos", duckRight.v))
		{
			isChanged = true;
		}

		ImGui::Text("Shoting left duck");
		if (ImGui::InputFloat2("duck L launch pos", duckLeft.v))
		{
			isChanged = true;
		}

		ImGui::Text("Shooting left");
		if (ImGui::InputFloat2("left launch pos", leftLeft.v))
		{
			isChanged = true;
		}

		ImGui::Text("Shooting right");
		if (ImGui::InputFloat2("right launch pos", rightRight.v))
		{
			isChanged = true;
		}

		ImGui::Text("Shooting up (right)");
		if (ImGui::InputFloat2("right up launch pos", rightUp.v))
		{
			isChanged = true;
		}

		ImGui::Text("Shooting up (left)");
		if (ImGui::InputFloat2("left up launch pos", leftUp.v))
		{
			isChanged = true;
		}

		return isChanged;
	}

	// Added by Miguel to solve warning. Should it return false?
	return false;
}

// FromJson specialized for this component
void Follower::FromJson(json & value)
{
	this->IComp::FromJson(value);
	
	value["isGun"] >> isGun;
	value["offset"] >> offset;
	value["generalOffset"] >> generalOffset;
	value["duckOffset"] >> duckOffset;
	value["stand up right pos"] >> rightRightUp;
	value["stand up left pos"] >> leftLeftUp;
	value["duck right pos"] >> duckRight;
	value["duck left pos"] >> duckLeft;
	value["right up"] >> rightUp;
	value["left up"] >> leftUp;
	value["right stand up"] >> rightRight;
	value["sleft stand up"] >> leftLeft;
	value["foot"] >> isFoot;
}

// ToJson specialized for this component
void Follower::ToJson(json & value)
{
	this->IComp::ToJson(value);
	value["isGun"] << isGun;
	if (offsetRememberX != 0.f && offsetRememberY != 0.f)
		value["offset"] << AEVec2(offsetRememberX, offsetRememberY);
	else
		value["offset"] << offset;
	value["generalOffset"] << generalOffset;
	
	value["duckOffset"] << duckOffset;
	value["stand up right pos"] << rightRightUp;
	value["stand up left pos"] << leftLeftUp;
	value["duck right pos"] << duckRight;
	value["duck left pos"] << duckLeft;
	value["right up"] << rightUp;
	value["left up"] << leftUp;
	value["right stand up"] << rightRight;
	value["sleft stand up"] << leftLeft;
	value["foot"] << isFoot;
}