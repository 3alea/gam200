#include "CameraLimits.h"
#include "Composition/AEXScene.h"
#include "FollowAnObject.h"

void CameraLimitActivator::Update()
{
}

void CameraLimitActivator::ShutDown()
{
}

bool CameraLimitActivator::OnGui()
{
	bool changed = false;

	if (ImGui::InputFloat2("Minimum limits", mMinLimits.v, 3))
		changed = true;

	if (ImGui::InputFloat2("Maximum limits", mMaxLimits.v, 3))
		changed = true;

	return changed;
}

void CameraLimitActivator::FromJson(json & value)
{
	this->LogicComp::FromJson(value);

	value["MinLimits"] >> mMinLimits;
	value["MaxLimits"] >> mMaxLimits;
}

void CameraLimitActivator::ToJson(json & value)
{
	this->LogicComp::ToJson(value);

	value["MinLimits"] << mMinLimits;
	value["MaxLimits"] << mMaxLimits;
}

void CameraLimitActivator::OnCollisionEvent(const CollisionEvent & collision)
{
	if (!strcmp(collision.otherObject->GetBaseName(), "foot") || !strcmp(collision.otherObject->GetBaseName(), "player"))
	{
		if (GameObject* cam = aexScene->FindObjectInANYSpace("camera"))
		{
			if (FollowAnObject* followComp = cam->GetComp<FollowAnObject>())
			{
				followComp->mMaxLimits = mMaxLimits;
				followComp->mMinLimits = mMinLimits;
			}
		}
	}
}