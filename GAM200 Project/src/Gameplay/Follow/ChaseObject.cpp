#include "Components/SpineAnimation.h"
#include "ChaseObject.h"
#include "Components/AEXTransformComp.h"
#include "Composition/AEXScene.h"
#include "Player\Stats\PlayerStats.h"
#include "Bullets/BulletLogic.h"
#include "Editor/Editor.h"
#include "Enemy/WaveSystem/Spawner.h"
#include "Environment/Background/StationZoomOut.h"
#include <Imgui/imgui.h>

namespace AEX
{
	int ChaseObject::mParticleIdGenerator = 0;

	ChaseObject::ChaseObject()
	{
		// Particles names' initialization
		/*mParticleId = ++mParticleIdGenerator;
		std::string name = "Particles";
		name += std::to_string(mParticleId);
		SetBaseName(name.c_str());*/
	}

	void ChaseObject::Initialize()
	{
		Enemy::Initialize();

		mOnCinematic = false;
		//ActivateDamage = true;
		GameObject * toChaseObj = aexScene->FindObjectInANYSpace(mToChaseName);

		if (toChaseObj != nullptr)
			mToChaseTr = toChaseObj->GetComp<TransformComp>();

		mOwnerTr = mOwner->GetComp<TransformComp>();

		mDead = false;
	}
	void ChaseObject::Update()
	{
		if (doneOnce == false)
		{
			Initialize();
			doneOnce = true;
		}

		if (mDead == true)
		{
			//mOwnerTr->mLocal.mTranslationZ.z = -20.0f;
			return;
		}

		//BoxCollider * mBoxCol = mOwner->GetComp<BoxCollider>();
		//GfxMgr->DrawRectangle(Transform({ mBoxCol->GetOwnerPosition()->x, mBoxCol->GetOwnerPosition()->y }, mBoxCol->mRealScale, 0.0f), AEVec4(0.0f, 0.0f, 0.0f));

		if (mOnCinematic == false && mToChaseTr != nullptr && mDead == false)
		{
			AEVec2 & moveVec = AEVec2(mToChaseTr->mLocal.mTranslationZ.x, mToChaseTr->mLocal.mTranslationZ.y) - AEVec2(mOwnerTr->mLocal.mTranslationZ.x, mOwnerTr->mLocal.mTranslationZ.y);
			moveVec.NormalizeThis();

			mOwnerTr->mLocal.mTranslationZ.x += moveVec.x * mChaseSpeed;
			mOwnerTr->mLocal.mTranslationZ.y += moveVec.y * mChaseSpeed;
		}

		Enemy::Update();

		if (mLives && ActivateDamage)
		{
			SpineAnimationComp* renderable = mOwner->GetComp<SpineAnimationComp>();
			damageTime += (f32)aexTime->GetFrameTime();
			f32 redVariance = sin(12.5f * damageTime);
			if (redVariance > 0)
			{
				renderable->mColor[1] = 1.0f - redVariance;
				renderable->mColor[2] = 1.0f - redVariance;
			}
			else
			{
				renderable->mColor[1] = 1.0f;
				renderable->mColor[2] = 1.0f;
				damageTime = 0.0f;
				ActivateDamage = false;
			}
		}
	}
	void ChaseObject::Shutdown()
	{

	}

	bool ChaseObject::OnGui()
	{
		bool isChanged = false;

		if (ImGui::InputFloat("Chase Speed", &mChaseSpeed))
		{
			isChanged = true;
		}
		if (ImGui::InputText("To Chase Name", mToChaseName, IM_ARRAYSIZE(mToChaseName)))
		{
			isChanged = true;
		}

		if (ImGui::InputInt("Lives", &mLives))
		{
			isChanged = true;
		}

		return isChanged;
	}

	void ChaseObject::OnCollisionStarted(const CollisionStarted& collision)
	{
		if (collision.otherObject->GetBaseNameString() == "player" && mDead == false)
		{
			PlayerStats * stats = collision.otherObject->GetComp<PlayerStats>();

			if (stats->isInvincible)
				return;

			stats->lives--;
			stats->isHit = true;

			if (stats->lives == 0)
				BulletLogic::mStationEnemiesKilled = 0;
		}

		// This didn't work, it still crashed
		/*if (collision.otherObject->GetComp<BulletLogic>())
		{
			if (mLives <= 0)
			{
				BulletLogic::mStationEnemiesKilled++;

				if (mOwner->mSpawnerOwner)
					mOwner->mSpawnerOwner->EnemyKilled();

				if (BulletLogic::mStationEnemiesKilled == 25)//7)
				{
					// Load new level
					StationZoomOut::mStartFadeToBlack = true;
				}

				mOwner->GetParentSpace()->DestroyObject(mOwner);
			}
		}*/
	}

	void ChaseObject::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);

		value["Chase Speed"] >> mChaseSpeed;

		std::string name;
		value["To Chase Name"] >> name;
		strcpy_s(mToChaseName, name.c_str());

		value["Lives"] >> mLives;
	}
	void ChaseObject::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);

		value["Chase Speed"] << mChaseSpeed;

		std::string name = mToChaseName;
		value["To Chase Name"] << name;

		value["Lives"] << mLives;
	}
}