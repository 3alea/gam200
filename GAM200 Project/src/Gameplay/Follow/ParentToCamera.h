#pragma once

#include "Components\Logic.h"


namespace AEX
{
	class ParentToCamera : public LogicComp
	{
		AEX_RTTI_DECL(ParentToCamera, LogicComp);

	public:

		void Initialize();
		void Update();
		void Shutdown();

		void FromJson(json & value);
		void ToJson(json & value);

	private:

	};
}