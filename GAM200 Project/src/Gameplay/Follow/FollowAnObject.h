#pragma once
#include "Composition\AEXGameObject.h"
#include "Components\AEXTransformComp.h"
#include "Components\Logic.h"
#include "Imgui\imgui.h"

class Camera;
namespace AEX
{
	class FollowAnObject : public LogicComp
	{
		AEX_RTTI_DECL(FollowAnObject, LogicComp);

	public:
		FollowAnObject();
		void ChangePos();

		virtual void Initialize();
		virtual void Update();

		void CheckLimits();

		bool mbUseProportions = false;
		f32 mMoveRatio = 1.0f;
		AEVec3 mTargetPrevPos;
		AEVec3 mMyPrevPos;

		TransformComp* followerT;
		TransformComp* followedT;

		char followedName[256];
		
		AEVec2 mOffset;

		bool mbUseLimits = false;
		AEVec2 mMinLimits;
		AEVec2 mMaxLimits;

		// Special offset for meter
		AEVec2 meterOffset = AEVec2(0.f, 0.f);

		virtual bool OnGui();

		virtual void FromJson(json & value);
		virtual void ToJson(json & value);

		// ADDED BY MIGUEL
		Camera * camera = nullptr;
		bool mParentToCamera;
		AEVec2 mNormalizedOffset;
		AEVec2 mNormalizedMeterOffset;
		AEVec2 mNormalizedSize;
	};
}