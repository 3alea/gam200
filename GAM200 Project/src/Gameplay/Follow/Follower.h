#pragma once

#include "Components\AEXTransformComp.h"
#include "Components\Logic.h"
#include "Imgui\imgui.h"
#include "Player/Movement/PlayerController.h"

using namespace AEX;

class Follower : public LogicComp
{
	AEX_RTTI_DECL(Follower, LogicComp);
public:

	// Is it a gun?
	bool isGun = false;

	bool isFoot;

	// Offset of the follower
	AEVec2 generalOffset;

	// Offset for the gun
	AEVec2 offset;

	// Fucking offset
	float duckOffset;

	// Offset to remember when guiding camera direction
	float offsetRememberX = 0.f;
	
	// Offset to remember when guiding camera on the Y
	float offsetRememberY = 0.f;
	
	// Player game object
	GameObject* player;

	// Position to follow
	TransformComp* pos;

	// Object TranformComp
	TransformComp* t;

	// Player stats to check player's state
	PlayerStats* stats;

	void OffsetUpdaterGunV2();

	AEVec2 InitialVelocity;

	AEVec2 p0Offset;

	AEVec2 duckRight;
	AEVec2 duckLeft;

	AEVec2 leftUp;
	AEVec2 rightUp;

	AEVec2 rightRight;
	AEVec2 leftLeftUp;

	AEVec2 rightRightUp;
	AEVec2 leftLeft;

	// Offset updater for gun
	void OffsetUpdaterGun();

	// Start is called before the first frame update
	virtual void Initialize();

	// Update is called once per frame
	virtual void Update();

	// OnGui() function
	virtual bool OnGui();

	// FromJson specialized for this component
	virtual void FromJson(json & value);
	// ToJson specialized for this component
	virtual void ToJson(json & value);
};