#pragma once
#include "Components\Logic.h"
#include "src/Engine/AI/StateMachine.h"
#include "AI/Path2D.h"
#include "Player\Stats\PlayerStats.h"

namespace AEX
{

	class TransformComp;
	class CameraShaker;

	class BlockedCar : public LogicComp//, public Actor
	{
		AEX_RTTI_DECL(BlockedCar, LogicComp);

	public:
		BlockedCar();

		virtual void Initialize();
		virtual void Update();
		virtual void OnCollisionStarted(const CollisionStarted& collision);
		virtual void OnCollisionEnded(const CollisionEnded& collision);
		virtual bool OnGui();

		void Activate();

		// FromJson specialized for this component
		virtual void FromJson(json & value);
		// ToJson specialized for this component
		virtual void ToJson(json & value);

		TransformComp* platformT;
		float waitTimer;
		float waitTime;
		float timer;
		float easedTime;
		PlayerStats* stats;
		bool active = false;
		float origin;
		float dist;
		float time;

		bool doneOnce = false;

		// Points to the transform of the camera
		TransformComp* camPos;
		CameraShaker* shaker;
	};
}