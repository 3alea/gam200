#include "BlockedCar.h"
#include "Components/AEXTransformComp.h"
#include "Imgui\imgui.h"
#include "AI/Easing.h"
#include "Player/Movement/PlayerController.h"
#include "Platform/AEXInput.h"
#include "Composition\AEXSpace.h"
#include "CameraLogic/CameraShaker.h"
#include "Resources/AEXResourceManager.h"
#include "Audio/AudioManager.h"

namespace AEX
{
	BlockedCar::BlockedCar()
	{}

	void BlockedCar::Initialize()
	{
		doneOnce = false;
		platformT = nullptr;
		stats = nullptr;
		platformT = mOwner->GetComp<TransformComp>();
		timer = 0;
		if (platformT)
		{
			origin = platformT->mLocal.mTranslationZ.x;
		}
		if (GameObject* p = mOwner->GetParentSpace()->FindObject("player"))
		{
			stats = p->GetComp<PlayerStats>();
		}
		shaker = nullptr;
		camPos = nullptr;
		if (GameObject* cam = mOwner->GetParentSpace()->FindObject("camera"))
		{
			shaker = cam->GetComp<CameraShaker>();
			camPos = cam->GetComp<TransformComp>();
		}
		
	}

	void BlockedCar::Update()
	{
		if (!platformT || !stats || !camPos || !shaker)
		{
			Initialize();
			return;
		}

		if (!active)
			return;

		if (waitTimer < waitTime)
		{
			waitTimer += (f32)aexTime->GetFrameTime();
			return;
		}

		timer += (f32)aexTime->GetFrameTime();
		easedTime = EaseInOutQuad(timer / time);

		platformT->mLocal.mTranslationZ.x = origin + dist * easedTime;

		if (timer >= time)
		{
			active = false;
		}

		float camX = camPos->mLocal.mTranslationZ.x;
		float camY = camPos->mLocal.mTranslationZ.y;

		if (platformT->mLocal.mTranslationZ.x >= camX - 1.0f && platformT->mLocal.mTranslationZ.x <= camX + 1.0f)
		{
			shaker->mTrauma = 0.5f;
		}

		if (platformT->mLocal.mTranslationZ.x <= camX + 10.0f && !doneOnce)
		{
			audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\car level\\fastCarArtery.mp3"), Voice::SFX);
			doneOnce = true;
		}

	}

	void BlockedCar::OnCollisionStarted(const CollisionStarted & collision)
	{
		// Kill the player
		if (collision.otherObject->GetComp<ICollider>()->collisionGroup == "player")
		{
			stats->lives = 0;
		}
	}

	void BlockedCar::OnCollisionEnded(const CollisionEnded & collision)
	{
	}

	bool BlockedCar::OnGui()
	{
		bool changed = false;
		if (ImGui::InputFloat("distance", &dist))
		{
			changed = true;
		}
		if (ImGui::InputFloat("time", &time))
		{
			changed = true;
		}
		if (ImGui::InputFloat("wait time", &waitTime))
		{
			changed = true;
		}
		return changed;
	}

	void BlockedCar::Activate()
	{
		active = true;
		timer = 0;
		waitTimer = 0;
	}

	void BlockedCar::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);
		value["waitTime"] >> waitTime;
		value["time"] >> time;
		value["dist"] >> dist;
	}
	void BlockedCar::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);
		value["waitTime"] << waitTime;
		value["time"] << time;
		value["dist"] << dist;
	}
}