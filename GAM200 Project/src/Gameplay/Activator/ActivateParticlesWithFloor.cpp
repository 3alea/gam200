#include "ActivateParticlesWithFloor.h"
#include "Components\Collider.h"
#include "Audio/AudioManager.h"
#include "Resources/AEXResourceManager.h"

void AEX::ActivateParticles::Initialize()
{
	p = mOwner->GetComp<ParticleEmitter>();
	doneOnce = false;
}

void AEX::ActivateParticles::Update()
{
	if (!p)
		Initialize();
}

void AEX::ActivateParticles::OnCollisionStarted(const CollisionStarted & collision)
{
	if (collision.otherObject->GetComp<ICollider>()->collisionGroup == "Floor")
	{
		p->mEmitProperties->mbParticlesEnabled = true;
		audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\car level\\metalScraping.mp3"), Voice::SFX, false)->SetVolume(0.7f);
	}
}

void AEX::ActivateParticles::OnCollisionEnded(const CollisionEnded & collision)
{
	ICollider* c = collision.otherObject->GetComp<ICollider>();

	if (c && c->collisionGroup == "Floor")
	{
		p->mEmitProperties->mbParticlesEnabled = false;
	}
}
