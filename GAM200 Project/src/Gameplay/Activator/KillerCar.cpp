#include "KillerCar.h"
#include "Composition/AEXGameObject.h"
#include "Components/AEXTransformComp.h"
#include "Composition/AEXSpace.h"
#include "components/ParticleEmitter.h"
#include "Player/Stats/PlayerStats.h"
#include "AI/Easing.h"
#include "Platform/AEXTime.h"
#include "CameraLogic/CameraShaker.h"
#include "Resources/AEXResourceManager.h"
#include "Audio/AudioManager.h"

namespace AEX
{
	void KillerCar::Initialize()
	{
		DoneOnce = false;

		activated = false;

		initialized = false;

		cooldown = false;

		speedSound = nullptr;

		if (mOwner->GetBaseNameString() == "c1")
			speedSound = aexResourceMgr->getResource<Sound>("data\\Audio\\car level\\fastCar1.mp3");
		else if (mOwner->GetBaseNameString() == "c2")
			speedSound = aexResourceMgr->getResource<Sound>("data\\Audio\\car level\\fastCar2.mp3");
		else if (mOwner->GetBaseNameString() == "c3")
			speedSound = aexResourceMgr->getResource<Sound>("data\\Audio\\car level\\fastCar3.mp3");
		else
			speedSound = aexResourceMgr->getResource<Sound>("data\\Audio\\car level\\fastCar2.mp3");
		
		timer = 0.0f;

		t = nullptr;
		camPos = nullptr;
		shaker = nullptr;

		// Get the transform of the car
		t = mOwner->GetComp<TransformComp>();

		if (t)
		{
			yPos = t->mLocal.mTranslationZ.y;
		}

		// Get the tranform of the player
		if (GameObject* cam = mOwner->GetParentSpace()->FindObject("camera"))
		{
			camPos = cam->GetComp<TransformComp>();
		}

		// Find the steam particle emitter
		if (GameObject* smoke = mOwner->GetParentSpace()->FindObject(steamName))
		{
			mySteam = smoke->GetComp<ParticleEmitter>();
		}

		if (GameObject* cam = mOwner->GetParentSpace()->FindObject("camera"))
		{
			shaker = cam->GetComp<CameraShaker>();
		}
	}
	void KillerCar::Update()
	{
		if (!initialized)
		{
			if (!camPos || !t || !mySteam || !shaker)
			{
				Initialize();
				return;
			}
			else
			{
				initialized = true;

				// Move the car out of the players visibility
				t->mLocal.mTranslationZ.y = 10.0f;

				// Start with the engine turned off
				turnOffSteam();
			}
		}

		//audiomgr->Loop(aexResourceMgr->getResource<Sound>("data\\Audio\\car level\\endineLoop.mp3"));

		if (!activated || cooldown)
			return;

		float camX = camPos->mLocal.mTranslationZ.x;
		float camY = camPos->mLocal.mTranslationZ.y;


		t->mLocal.mTranslationZ.y = yPos;

		if (t->mLocal.mTranslationZ.x >= camX - 1.0f && t->mLocal.mTranslationZ.x <= camX + 1.0f && yPos - camY <= 4.0f)
		{
			shaker->mTrauma = mTrauma;
		}

		if (t->mLocal.mTranslationZ.x >= camX - 5.0f && !DoneOnce && yPos - camY <= 4.0f)
		{
			audiomgr->Play(speedSound, Voice::SFX);
			DoneOnce = true;
		}

		// Car has been activated
		float xOrigin = camX - 15.0f;
		float xDestination = camX + 20.0f;

		// Update the timer
		timer += (f32)aexTime->GetFrameTime();

		float easedTimer = EaseInOutQuad(timer / time);

		t->mLocal.mTranslationZ.x = xOrigin + (xDestination - xOrigin) * easedTimer;

		if (timer >= time)
		{
			timer = 0.0f;
			turnOffSteam();
			t->mLocal.mTranslationZ.y = 10.0f;
			cooldown = true;
			DoneOnce = false;
		}
	}
	void KillerCar::turnOffSteam()
	{
		mySteam->mEmitProperties->mbParticlesEnabled = false;
	}
	void KillerCar::turnOnSteam()
	{
		mySteam->mEmitProperties->mbParticlesEnabled = true;
	}
	bool KillerCar::OnGui()
	{
		bool changed = false;

		if (ImGui::InputText("smoke name", steamName, IM_ARRAYSIZE(steamName)))
		{
			Initialize();
			changed = true;
		}
		if (ImGui::InputFloat("trauma", &mTrauma))
		{
			changed = true;
		}
		return changed;
	}
	void KillerCar::OnCollisionStarted(const CollisionStarted & collision)
	{
		if (collision.otherObject->GetBaseNameString() == "player")
			collision.otherObject->GetComp<PlayerStats>()->lives = 0;
	}
	void KillerCar::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);

		std::string name;
		value["smoke name"] >> name;
		strcpy_s(steamName, name.c_str());
		value["trauma"] >> mTrauma;
	}
	void KillerCar::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);
		std::string name = steamName;
		value["smoke name"] << name;

		value["trauma"] << mTrauma;
	}
}