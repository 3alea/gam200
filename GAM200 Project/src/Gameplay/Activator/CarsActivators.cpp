#include "CarsActivators.h"
#include "Composition/AEXGameObject.h"
#include "Composition/AEXSpace.h"
#include "AI/Path2D.h"
#include "Components/SpineAnimation.h"
#include "Components/AEXTransformComp.h"
#include "Audio/AudioManager.h"
#include "CameraLogic/CameraShaker.h"
#include "Resources/AEXResourceManager.h"
#include "KillerCarsActivator.h"

void AEX::CarsActivator::Initialize()
{
	follower = nullptr;
	anim = nullptr;

	active = false;

	if (GameObject* car = mOwner->GetParentSpace()->FindObject("car1"))
	{
		anim = car->GetComp<SpineAnimationComp>();

		t = car->GetComp<TransformComp>();

		follower = car->GetComp<PathFollower>();
		if (follower)
		{
			follower->StopFollowing();
		}
	}

	shaker = nullptr;

	if (GameObject* cam = mOwner->GetParentSpace()->FindObject("camera"))
	{
		shaker = cam->GetComp<CameraShaker>();
	}

	player = nullptr;

	// Get the tranform of the player
	if (GameObject* p = mOwner->GetParentSpace()->FindObject("player"))
	{
		player = p->GetComp<TransformComp>();
	}

	DoneOnce = false;
}

void AEX::CarsActivator::Update()
{
	if (!follower || !anim || !t || !shaker || !player)
	{
		Initialize();
		return;
	}

	if (!active)
		return;

	else
		follower->ContinueFollowing();

	if (follower->timer <= 0.2)
		DoneOnce = false;

	float playerX = player->mLocal.mTranslationZ.x;

	if (t->mLocal.mTranslationZ.x >= playerX - 3.0f && t->mLocal.mTranslationZ.x <= playerX + 3.0f && !DoneOnce)
	{
		shaker->mTrauma = 0.7f;
		audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\car level\\fastCar2.mp3"), Voice::SFX);
		audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\car level\\enterTube.mp3"), Voice::SFX);

		DoneOnce = true;
	}
}

void AEX::CarsActivator::OnCollisionStarted(const CollisionStarted & collision)
{
	//if (collision.otherObject->GetBaseNameString() == "player")
	//{
	//	follower->ContinueFollowing();
	//	anim->carRotation = 20;
	//}
}

void AEX::CarsActivator::FromJson(json & value)
{
	this->LogicComp::FromJson(value);
}

void AEX::CarsActivator::ToJson(json & value)
{
	this->LogicComp::ToJson(value);
}
