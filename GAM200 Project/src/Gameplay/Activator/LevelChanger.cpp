#include "LevelChanger.h"
#include "Components\Collider.h"
#include "Composition\AEXGameObject.h"
#include "Editor\Editor.h"

namespace AEX
{
	void LevelChanger::Initialize()
	{
		
	}

	void LevelChanger::Update()
	{
		
	}

	bool LevelChanger::OnGui()
	{
		bool changed = false;

		if (ImGui::InputText("Level name", mLevelName, IM_ARRAYSIZE(mLevelName))) changed = true;

		return changed;
	}

	void LevelChanger::FromJson(json & value)
	{
		std::string tText;
		value["LevelName"] >> tText;
		strcpy(mLevelName, tText.c_str());
	}

	void LevelChanger::ToJson(json & value)
	{
		std::string tText(mLevelName);
		value["LevelName"] << tText;
	}

	void LevelChanger::OnCollisionStarted(const CollisionStarted & collision)
	{
		if (collision.otherObject->GetComp<ICollider>()->collisionGroup == "player")
		{
			Editor->loadFromCertainLevel = true;
			Editor->levelToLoadName = mLevelName;
		}
	}
}