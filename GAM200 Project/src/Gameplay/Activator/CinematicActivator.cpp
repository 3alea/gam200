#include "CinematicActivator.h"
#include "Composition/AEXGameObject.h"
#include "Components/AEXTransformComp.h"
#include "Composition/AEXSpace.h"
#include "Player/Stats/PlayerStats.h"
#include "AI/Easing.h"
#include "Platform/AEXTime.h"
#include "CameraLogic/CameraMovement.h"
#include "Activator/KillerCarsActivator.h"
//#include "components/ParticleEmitter.h"
//#include "CameraLogic/CameraShaker.h"
//#include "Resources/AEXResourceManager.h"
//#include "Audio/AudioManager.h"

namespace AEX
{
	void CinematicActivator::Initialize()
	{
		active = false;
		startCinematic = false;
		endCinematic = false;
		done = false;
		cam = nullptr;
		topBar = nullptr;
		botBar = nullptr;
		initialized = false;
		timer = 0.0f;
		stats = nullptr;
		// Get the tranform of the player
		if (GameObject* c = mOwner->GetParentSpace()->FindObject("cinematicCamera"))
		{
			cam = c->GetComp<TransformComp>();
		}

		// Find the steam particle emitter
		if (GameObject* t = mOwner->GetParentSpace()->FindObject("blackBar1"))
		{
			topBar = t->GetComp<TransformComp>();
		}

		if (GameObject* b = mOwner->GetParentSpace()->FindObject("blackBar2"))
		{
			botBar = b->GetComp<TransformComp>();
		}

		if (GameObject* p = mOwner->GetParentSpace()->FindObject("player"))
		{
			stats = p->GetComp<PlayerStats>();
		}
	}
	void CinematicActivator::Update()
	{
		if (!initialized)
		{
			if (!cam || !botBar || !topBar || !stats)
			{
				Initialize();
				return;
			}
			else
				initialized = true;
		}

		if (solveBug)
		{
			return;
		}

		// This will enter when the level starts, when the bars are completely down
		// or completely up
		if (!active || done)
			return;

		// Start the cinematic
		if (startCinematic)
		{
			//std::cout << "AAAA\n";

			timer += (f32)aexTime->GetFrameTime();

			float tn = EaseOutQuad(timer / timeToMove);

			topBar->mLocal.mTranslationZ.y = topOrigin + (topDest - topOrigin) * tn;
			botBar->mLocal.mTranslationZ.y = botOrigin + (botDest - botOrigin) * tn;

			// Finished
			if (timer >= timeToMove)
			{
				active = false;
				timer = 0.0f;
				done = true;
			}
		}

		// Otherwise, end the cinematic
		else
		{
			timer += (f32)aexTime->GetFrameTime();

			float tn = EaseInQuad(timer / timeToMove);

			topBar->mLocal.mTranslationZ.y = topOrigin + (topDest - topOrigin) * tn;
			botBar->mLocal.mTranslationZ.y = botOrigin + (botDest - botOrigin) * tn;

			// Finished
			if (timer >= timeToMove)
			{
				active = false;
				timer = 0.0f;
				done = true;
			}
		}
	}

	void CinematicActivator::StartCinematic()
	{
		timer = 0.0f;
		active = true;
		done = false;
		startCinematic = true;
		endCinematic = false;

		// Move the bars to their starting positions
		topBar->mLocal.mTranslationZ.x = cam->mLocal.mTranslationZ.x;
		botBar->mLocal.mTranslationZ.x = cam->mLocal.mTranslationZ.x;

		topOrigin = cam->mLocal.mTranslationZ.y + 7.5f;
		botOrigin = cam->mLocal.mTranslationZ.y - 7.5f;
		topDest = cam->mLocal.mTranslationZ.y + 6.0f;
		botDest = cam->mLocal.mTranslationZ.y - 6.0f;
		stats->StartCinematic(false);
	}

	void CinematicActivator::EndCinematic()
	{
		active = true;
		done = false;
		startCinematic = false;
		endCinematic = true;

		// Move the bars to their starting positions
		topBar->mLocal.mTranslationZ.x = cam->mLocal.mTranslationZ.x;
		botBar->mLocal.mTranslationZ.x = cam->mLocal.mTranslationZ.x;
		
		topOrigin = topBar->mLocal.mTranslationZ.y;
		botOrigin = botBar->mLocal.mTranslationZ.y;
		topDest = cam->mLocal.mTranslationZ.y + 7.5f;
		botDest = cam->mLocal.mTranslationZ.y - 7.5f;
	}

	bool CinematicActivator::OnGui()
	{
		bool changed = false;

		if (ImGui::Checkbox("activate cinematic", &activator))
		{
			changed = true;
		}

		if (ImGui::Checkbox("solveBug", &solveBug))
		{
			changed = true;
		}

		if (ImGui::InputFloat("time to move bars", &timeToMove))
			changed = true;

		return changed;
	}
	void CinematicActivator::OnCollisionStarted(const CollisionStarted & collision)
	{
		if (solveBug)
		{
			if (collision.otherObject->GetBaseNameString() == "player")
			{
				CameraMovement* camLogic = mOwner->GetParentSpace()->FindObject("camera")->GetComp<CameraMovement>();
		
				// Check that there is something wrong
				if (camLogic->xMax <= 2.0f)
				{
					TransformComp* reference = mOwner->GetParentSpace()->FindObject("camera changer 2")->GetComp<TransformComp>();
					camLogic->t->mLocal.mTranslationZ.y = reference->mLocal.mTranslationZ.y;
					camLogic->yDestination = reference->mLocal.mTranslationZ.y;
					camLogic->lockX = false;
					camLogic->xMax = reference->mLocal.mTranslationZ.x + 81.0f;
					camLogic->xMin = reference->mLocal.mTranslationZ.x - 1.0f;
					camLogic->cameraOffset = 4.0f;
					camLogic->camRight = true;
					mOwner->GetParentSpace()->FindObject("player")->GetComp<TransformComp>()->mLocal.mTranslationZ.z = -50.0f;
					mOwner->GetParentSpace()->FindObject("kca")->GetComp<KillerCarsActivator>()->active = true;
				}
			}
			return;
		}

		std::string name;

		if (activator)
			name = "foot";
		else
			name = "player";
 		if (collision.otherObject->GetBaseNameString() == name)
		{
			// The cinematic is already starting or finishing
			if (active || done)
				return;

			// Otherwise, need to start or end
			else
				active = true;

			// True if this is an activator (start the cinematic)
			startCinematic = activator;

			// Reset the timer
			timer = 0.0f;

			// Move the bars to their starting positions
			topBar->mLocal.mTranslationZ.x = cam->mLocal.mTranslationZ.x;
			botBar->mLocal.mTranslationZ.x = cam->mLocal.mTranslationZ.x;

			// Set the origin and destination
			if (startCinematic)
			{
				topOrigin = cam->mLocal.mTranslationZ.y + 7.5f;
				botOrigin = cam->mLocal.mTranslationZ.y - 7.5f;
				topDest   = cam->mLocal.mTranslationZ.y + 6.0f;
				botDest   = cam->mLocal.mTranslationZ.y - 6.0f;
				stats->StartCinematic(false);
			}
			else
			{
				topOrigin = topBar->mLocal.mTranslationZ.y;
				botOrigin = botBar->mLocal.mTranslationZ.y;
				topDest = cam->mLocal.mTranslationZ.y + 7.5f;
				botDest = cam->mLocal.mTranslationZ.y - 7.5f;
			}
		}

	}
	void CinematicActivator::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);

		value["startCinematic"] >> activator;
		value["timetomove"] >> timeToMove;
		value["solve bug"] >> solveBug;
	}
	void CinematicActivator::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);
		value["startCinematic"] << activator;
		value["timetomove"] << timeToMove;
		value["solve bug"] << solveBug;

	}
}