#pragma once
#include "Composition\AEXGameObject.h" // This solves collision bug
#include "Components\Logic.h"

namespace AEX
{
	class AcidActivator : public LogicComp
	{
		AEX_RTTI_DECL(AcidActivator, LogicComp)

	public:
		// Whether it activates or not
		bool Acid_direction;

		// The acids' new max/min height
		float maxHeight;
		float minHeight;

		void Initialize();
		void Update();
		
		void OnCollisionEvent(const CollisionEvent& collision);

		bool OnGui();

		void FromJson(json & value);
		void ToJson(json & value);

	private:

	    char EnemyToActivate[64] = "";
		bool ActivateEnemy;
		Transform * PlayerTransform;
		bool doneOnce;
	};
}