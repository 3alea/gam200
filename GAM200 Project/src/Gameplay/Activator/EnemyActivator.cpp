#include "EnemyActivator.h"

namespace AEX
{
	void EnemyActivator::Initialize()
	{
		ActivateEnemy = false;
		doneOnce = false;
	}
	void EnemyActivator::Update()
	{
		/*if (doneOnce == false)
		{
			GameObject * found = aexScene->FindObjectInANYSpace("player");
			ASSERT(found, "The gameobject with name player was not found")
				PlayerTransform = &found->GetComp<TransformComp>()->mLocal;
			doneOnce = true;
		}*/

		//PlayerTransform->mTranslationZ += AEVec3(0.1,0,0);
	}
	void EnemyActivator::ShutDown()
	{
		
	}

	bool EnemyActivator::OnGui()
	{
		bool changed = false;


		if(ImGui::InputText("Enemy", EnemyToActivate, IM_ARRAYSIZE(EnemyToActivate))) changed = true;
		

		return changed;
	}

	void EnemyActivator::OnCollisionEvent(const CollisionEvent& collision)
	{
		//std::cout << EnemyToActivate << std::endl;

		if (collision.otherObject->GetBaseNameString() == "player")
		{
			if (GameObject* go = aexScene->FindObjectInANYSpace(EnemyToActivate))
				go->GetComp<Enemy>()->Activate = true;

			mOwner->GetParentSpace()->DestroyObject(mOwner);
		}
	}

	void EnemyActivator::FromJson(json & value)
	{
		this->IComp::FromJson(value);

		if (value["Enemy Activated"] != nullptr)
			ActivateEnemy = value["Enemy Activated"].get<bool>();

		std::string enemy = EnemyToActivate;
		value["Enemy To Activate"] >> enemy;
		strcpy_s(EnemyToActivate, enemy.c_str());
	}
	void EnemyActivator::ToJson(json & value)
	{
		this->IComp::ToJson(value);

		value["Enemy Activated"] = ActivateEnemy;
		value["Enemy To Activate"] << EnemyToActivate;
	}
}