#pragma once
#include "Components\Logic.h"
#include "Components\ParticleEmitter.h"

class ParticleEmitter;

namespace AEX
{
	class ActivateParticles : public LogicComp
	{
		AEX_RTTI_DECL(ActivateParticles, LogicComp)
	public:
		virtual void Initialize();
		virtual void Update();

		ParticleEmitter* p;

		bool doneOnce;

		virtual void OnCollisionStarted(const CollisionStarted& collision);
		virtual void OnCollisionEnded(const CollisionEnded& collision);
	};
}