#pragma once
#include "Components\Logic.h"

class SpineAnimationComp;

namespace AEX
{
	class TransformComp;
	class PathFollower;
	class CameraShaker;
	class KillerCarsActivator;

	class CarsActivator : public LogicComp
	{
		AEX_RTTI_DECL(CarsActivator, LogicComp);

	public:
		virtual void Initialize();

		// Update is called once per frame
		virtual void Update();

		void OnCollisionStarted(const CollisionStarted& collision);

		bool active;

		TransformComp* t;
		TransformComp* player;
		CameraShaker* shaker;
		bool DoneOnce;
		PathFollower* follower;
		SpineAnimationComp* anim;

		void FromJson(json & value);
		void ToJson(json & value);
	};
}