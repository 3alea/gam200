#pragma once

#include "src/Gameplay/Enemy/Enemy.h"

namespace AEX
{
	class EnemyActivator : public LogicComp
	{
		AEX_RTTI_DECL(EnemyActivator, LogicComp)

	public:

		void Initialize();
		void Update();
		void ShutDown();

		bool OnGui();

		void OnCollisionEvent(const CollisionEvent& collision);

		void FromJson(json & value);
		void ToJson(json & value);

	private:

	    char EnemyToActivate[64] = "";
		bool ActivateEnemy;
		Transform * PlayerTransform;
		bool doneOnce;
	};
}