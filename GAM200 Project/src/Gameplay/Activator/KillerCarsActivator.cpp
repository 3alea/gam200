#include "KillerCarsActivator.h"
#include "Imgui\imgui.h"
#include "Composition/AEXGameObject.h"
#include "Platform/AEXTime.h"
#include "KillerCar.h"
#include "Composition/AEXSpace.h"
#include "Utilities/RNG.h"
#include "CarsActivators.h"
#include "Audio/AudioManager.h"
#include "Resources/AEXResourceManager.h"
#include "Utilities/RNG.h"

namespace AEX
{
	void KillerCarsActivator::Initialize()
	{
		generalTimer = 0;
		active = false;
		initialized = false;
		cooldown = false;
		finishedForLevel = false;
		c1 = nullptr;
		c2 = nullptr;
		c3 = nullptr;
		if (GameObject* car1 = mOwner->GetParentSpace()->FindObject("c1"))
			c1 = car1->GetComp<KillerCar>();
		if (GameObject* car2 = mOwner->GetParentSpace()->FindObject("c2"))
			c2 = car2->GetComp<KillerCar>();
		if (GameObject* car3 = mOwner->GetParentSpace()->FindObject("c3"))
			c3 = car3->GetComp<KillerCar>();

		ca = nullptr;

		if (GameObject* activator = mOwner->GetParentSpace()->FindObject("kca"))
			ca = activator->GetComp<CarsActivator>();

		warningEnded = false;
		cooldownTime = 0;
	}
	void KillerCarsActivator::Update()
	{
		if (!initialized)
		{
			if (!c1 || !c2 || !c3 || !ca)
			{
				Initialize();
				return;
			}
			else
			{
				initialized = true;
				next_round_cars();
			}
		}

		// Activate the car going through the tube
		if (finishedForLevel)
		{
			if (check_cars())
			{
				ca->active = true;
				finishedForLevel = false;
			}
		}

		if (active)
		{
			if (wait_cooldown())
				return;

			// Cooldown has finished. Need to alert the player that
			// high speed cars are coming
			if (level2 && !warningEnded)
			{
				manage_warnings();
				return;
			}

			// Update the general timer
			generalTimer += (f32)aexTime->GetFrameTime();

			// Activate the first car
			if ((moveOne || moveTwo || moveThree) && !c1->activated && generalTimer >= time1)
			{
				c1->cooldown = false;
				//std::cout << "activate car 1" << std::endl;
				c1->activated = true;
				c1->time = 1.0f;
				c1->turnOnSteam();
			}
			if ((moveTwo || moveThree) && !c2->activated && generalTimer >= time2)
			{
				//std::cout << "activate car 2" << std::endl;
				c2->cooldown = false;
				c2->activated = true;
				c2->time = 1.2f;
				c2->turnOnSteam();
			}
			if (moveThree && !c3->activated && generalTimer >= time3)
			{
				//std::cout << "activate car 3" << std::endl;

				c3->cooldown = false;
				c3->activated = true;
				c3->time = 1.0f;
				c3->turnOnSteam();
			}

			// All cars have been activated and finished
			if (all_cars_active() && check_cars())
			{
				//std::cout << "start cooldown" << std::endl;
				cooldown = true;
				next_round_cars();
				generalTimer = 0;
				cooldownTime = RNG->GetFloat(0.5f, 1.5f);
				warningEnded = false;

				c3->activated = false;
				c2->activated = false;
				c1->activated = false;
			}
		}
	}
	bool KillerCarsActivator::wait_cooldown()
	{
		if (cooldown)
		{
			generalTimer += (f32)aexTime->GetFrameTime();
			if (generalTimer >= cooldownTime)
			{
				//std::cout << "cooldown ended" << std::endl;
				generalTimer = 0;
				cooldown = false;

				return false;
			}
			return true;
		}
		else
			return false;
	}
	void KillerCarsActivator::next_round_cars()
	{
		int randomNum = RNG->GetInt(1, 4);

		if (randomNum == 1)
		{
			//std::cout << "round of 1 car" << std::endl;
			moveThree = false;
			moveTwo = false;
			moveOne = true;
		}
		else if(randomNum == 2)
		{
			//std::cout << "round of 2 car" << std::endl;

			moveThree = false;
			moveTwo = true;
			moveOne = false;
		}
		else
		{
			//std::cout << "round of 3 car" << std::endl;

			moveThree = true;
			moveTwo = false;
			moveOne = false;
		}
	}
	bool KillerCarsActivator::check_cars()
	{
		if (moveOne)
			return c1->cooldown;
		else if (moveTwo)
			return (c1->cooldown && c2->cooldown);
		else
			return (c1->cooldown && c2->cooldown && c3->cooldown);
	}
	bool KillerCarsActivator::all_cars_active()
	{
		if (moveOne)
			return c1->activated;
		else if (moveTwo)
			return (c1->activated && c2->activated);
		else
			return (c1->activated && c2->activated && c3->activated);
	}
	void KillerCarsActivator::manage_warnings()
	{
		if (moveOne)
			audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\car level\\carWarning1.mp3"), Voice::SFX);
		else if (moveTwo)
			audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\car level\\carWarning2.mp3"), Voice::SFX);
		else
			audiomgr->Play(aexResourceMgr->getResource<Sound>("data\\Audio\\car level\\carWarning3.mp3"), Voice::SFX);

		cooldown = true;
		cooldownTime = 0.7f;
		warningEnded = true;
	}
	bool KillerCarsActivator::OnGui()
	{
		bool changed = false;

		if (ImGui::Checkbox("activate cars", &shouldActivate))
		{
			changed = true;
		}

		if (ImGui::Checkbox("level 2", &level2))
		{
			changed = true;
		}

		if (ImGui::InputFloat("t1", &time1))
		{
			changed = true;
		}

		if (ImGui::InputFloat("t2", &time2))
		{
			changed = true;
		}

		if (ImGui::InputFloat("t3", &time3))
		{
			changed = true;
		}

		return changed;
	}

	void KillerCarsActivator::OnCollisionStarted(const CollisionStarted & collision)
	{
		if (collision.otherObject->GetBaseNameString() == "player")
		{
			active = shouldActivate;

			// Killer cars have been deactivated
			if (!active)
			{
				finishedForLevel = true;
			}
		}
	}

	void KillerCarsActivator::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);

		//value["all"] >> moveAll;
		//value["one"] >> moveOne;
		//value["two"] >> moveTwo;
		//value["three"] >> moveThree;
		value["t1"] >> time1;
		value["t2"] >> time2;
		value["t3"] >> time3;
		value["shouldActivate"] >> shouldActivate;
		value["level2"] >> level2;
	}
	void KillerCarsActivator::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);

		//value["all"] << moveAll;
		//value["one"] << moveOne;
		//value["two"] << moveTwo;
		//value["three"] << moveThree;
		value["t1"] << time1;
		value["t2"] << time2;
		value["t3"] << time3;
		value["shouldActivate"] << shouldActivate;
		value["level2"] << level2;
	}
}