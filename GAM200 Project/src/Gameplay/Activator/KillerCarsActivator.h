#pragma once
#include "Components\Logic.h"

namespace AEX
{
	class KillerCar;
	class CarsActivator;
	class KillerCarsActivator : public LogicComp
	{
		AEX_RTTI_DECL(KillerCarsActivator, LogicComp);

	public:
		virtual void Initialize();

		// Update is called once per frame
		virtual void Update();

		CarsActivator* ca;
		bool finishedForLevel;
		bool level2 = false;

		// Max of 4 cars
		KillerCar* c1;
		KillerCar* c2;
		KillerCar* c3;

		bool moveOne, moveTwo, moveThree;

		float time1, time2, time3;

		float generalTimer;

		bool warningEnded;

		bool active;
		bool initialized;
		bool cooldown = true;
		float cooldownTime;

		bool wait_cooldown();
		void next_round_cars();
		bool check_cars();
		bool all_cars_active();
		void manage_warnings();

		bool shouldActivate;

		// OnGui() function
		virtual bool OnGui();

		void OnCollisionStarted(const CollisionStarted& collision);

		void FromJson(json & value);
		void ToJson(json & value);
	};
}