#pragma once
#include "Components\Logic.h"

namespace AEX
{
	class LevelChanger : public LogicComp
	{
		AEX_RTTI_DECL(LevelChanger, LogicComp)
	public:
		virtual void Initialize();
		virtual void Update();

		bool OnGui();

		void FromJson(json & value);
		void ToJson(json & value);

		virtual void OnCollisionStarted(const CollisionStarted& collision);

	private:
		char mLevelName[256];
	};
}