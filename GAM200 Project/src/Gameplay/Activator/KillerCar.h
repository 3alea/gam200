#pragma once
#include "Components\Logic.h"

class ParticleEmitter;

namespace AEX
{
	class TransformComp;
	class CameraShaker;
	struct Sound;

	template <typename T> class TResource;

	class KillerCar : public LogicComp
	{
		AEX_RTTI_DECL(KillerCar, LogicComp);

	public:
		virtual void Initialize();

		// Update is called once per frame
		virtual void Update();

		// Points to the transform of the car
		TransformComp* t;
		// Points to the transform of the camera
		TransformComp* camPos;

		CameraShaker* shaker;

		float mTrauma;

		char steamName[256];
		ParticleEmitter* mySteam;

		float timer;
		float time;

		bool DoneOnce;

		TResource<Sound>* speedSound;

		float yPos;
		bool initialized;
		bool cooldown;

		bool activated;

		void turnOffSteam();
		void turnOnSteam();


		// OnGui() function
		virtual bool OnGui();

		void OnCollisionStarted(const CollisionStarted& collision);

		void FromJson(json & value);
		void ToJson(json & value);
	};
}