#include "FadeInOutActivator.h"
#include "Composition/AEXGameObject.h"
#include "Components/AEXTransformComp.h"
#include "Composition/AEXSpace.h"
#include "Player/Stats/PlayerStats.h"
#include "AI/Easing.h"
#include "Platform/AEXTime.h"
#include "Components/SpriteComps.h"
//#include "components/ParticleEmitter.h"
//#include "CameraLogic/CameraShaker.h"
//#include "Resources/AEXResourceManager.h"
//#include "Audio/AudioManager.h"

namespace AEX
{
	void FadeInOutActivator::Initialize()
	{
		timer = 0.0f;
		cam = nullptr;
		fade = nullptr;
		sprite = nullptr;
		initialized = false;
		active = false;
		done = false;
		// Get the tranform of the player
		if (GameObject* c = mOwner->GetParentSpace()->FindObject("cinematicCamera"))
		{
			cam = c->GetComp<TransformComp>();
		}

		// Find the steam particle emitter
		if (GameObject* f = mOwner->GetParentSpace()->FindObject("fade"))
		{
			fade = f->GetComp<TransformComp>();
			sprite = f->GetComp<SpriteComp>();
		}
	}
	void FadeInOutActivator::Update()
	{
		if (!initialized)
		{
			if (!cam || !fade || !sprite)
			{
				Initialize();
				return;
			}
			else
			{
				initialized = true;
			}
		}

		if (done || !active)
			return;

		if (fadeIn)
		{
			timer += (f32)aexTime->GetFrameTime();

			float tn = EaseOutQuad(timer / timeToFade);

			sprite->mColor[3] = 0.0f + 1.0f * tn;

			// Finished
			if (timer >= timeToFade)
			{
				sprite->mColor[3] = 2.0f;
				active = false;
				timer = 0.0f;
				done = true;
			}
		}
		
		else
		{
			timer += (f32)aexTime->GetFrameTime();

			float tn = EaseInQuad(timer / timeToFade);

			sprite->mColor[3] = 1.0f - 1.0f * tn;

			// Finished
			if (timer >= timeToFade)
			{
				sprite->mColor[3] = 0.0f;
				active = false;
				timer = 0.0f;
				done = true;
			}
		}
	}

	bool FadeInOutActivator::OnGui()
	{
		bool changed = false;

		if (ImGui::Checkbox("fadeIn", &fadeIn))
		{
			changed = true;
		}

		if (ImGui::InputFloat("time to move bars", &timeToFade))
			changed = true;

		return changed;
	}
	void FadeInOutActivator::OnCollisionStarted(const CollisionStarted & collision)
	{
		if (done || active)
			return;

		if (collision.otherObject->GetBaseNameString() == "player")
		{
			active = true;

			if (cam)
			{
				fade->mLocal.mTranslationZ.x = cam->mLocal.mTranslationZ.x;
				fade->mLocal.mTranslationZ.y = cam->mLocal.mTranslationZ.y;
			}
		}
	}
	void FadeInOutActivator::FromJson(json & value)
	{
		this->LogicComp::FromJson(value);
		value["fade in"] >> fadeIn;
		value["timetofade"] >> timeToFade;
	}
	void FadeInOutActivator::ToJson(json & value)
	{
		this->LogicComp::ToJson(value);

		value["fade in"] << fadeIn;
		value["timetofade"] << timeToFade;
	}
}