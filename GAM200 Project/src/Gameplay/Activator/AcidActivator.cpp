#include "Composition/AEXScene.h"     // mOwner
#include "AcidActivator.h"
#include "Environment\AcidController.h"
#include "Imgui\imgui.h"

namespace AEX
{
	void AcidActivator::Initialize() {}

	void AcidActivator::Update() {}

	bool AcidActivator::OnGui()
	{
		// Collapsing header - when clicked, opens up to show the rest of edited variables
		if (ImGui::CollapsingHeader("AcidActivator"))
		{
			// isChanged boolean to return if something has changed
			bool isChanged = false;

			// Horizontal speed
			ImGui::Text("Acid direction (on = up, off = down)");
			if (ImGui::Checkbox("Acid direction", &Acid_direction)) isChanged = true;

			// Y max change
			ImGui::Text("Acid new max height");
			if (ImGui::InputFloat("Acid maxY", &maxHeight)) isChanged = true;

			// Y min change
			ImGui::Text("Acid new min height");
			if (ImGui::InputFloat("Acid minY", &minHeight)) isChanged = true;

			return isChanged;
		}

		return false;
	}

	void AcidActivator::OnCollisionEvent(const CollisionEvent& collision)
	{
		// Collision with the player
		if (collision.otherObject->GetBaseNameString() == "player")
		{
			// Find the acid in the level
			GameObject* acid = mOwner->GetParentSpace()->FindObject("AcidHazard");

			AcidController* acidsController = acid->GetComp<AcidController>();

			Acid_direction == true ?
				acidsController->myState = AcidController::State::eRising
			    : acidsController->myState = AcidController::State::eFalling;

			Acid_direction == true ?
				acidsController->maxHeight = maxHeight
				: acidsController->minHeight = minHeight;

			mOwner->GetParentSpace()->DestroyObject(mOwner);
		}
	}

	void AcidActivator::FromJson(json & value)
	{
		this->IComp::FromJson(value);

		value["Acid Direction"] >> Acid_direction;
		value["Acid minY"] >> minHeight;
		value["Acid maxY"] >> maxHeight;
	}
	void AcidActivator::ToJson(json & value)
	{
		this->IComp::ToJson(value);

		value["Acid Direction"] << Acid_direction;
		value["Acid minY"] << minHeight;
		value["Acid maxY"] << maxHeight;
	}
}