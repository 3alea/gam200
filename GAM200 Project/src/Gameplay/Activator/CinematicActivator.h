#pragma once
#include "Components\Logic.h"

class PlayerStats;

namespace AEX
{
	class TransformComp;

	class CinematicActivator : public LogicComp
	{
		AEX_RTTI_DECL(CinematicActivator, LogicComp);

	public:
		virtual void Initialize();

		// Update is called once per frame
		virtual void Update();

		void StartCinematic();
		void EndCinematic();

		// Points to the transform of the top blackBar
		TransformComp* topBar;
		// Points to the transform of the bot blackBar
		TransformComp* botBar;
		// Points to the transform of the camera
		TransformComp* cam;
		PlayerStats* stats;

		// Time it takes the bars to go up or down
		float timeToMove;

		// Starting point to go up or down
		float topOrigin;
		float botOrigin;
		float topDest;
		float botDest;

		bool done;

		float timer;

		bool initialized;
		bool active;

		bool startCinematic;
		bool endCinematic;
		bool activator;

		bool solveBug = false;

		// OnGui() function
		virtual bool OnGui();

		void OnCollisionStarted(const CollisionStarted& collision);

		void FromJson(json & value);
		void ToJson(json & value);
	};
}