#pragma once
#include "Components\Logic.h"

namespace AEX
{
	class TransformComp;
	class SpriteComp;

	class FadeInOutActivator : public LogicComp
	{
		AEX_RTTI_DECL(FadeInOutActivator, LogicComp);

	public:
		virtual void Initialize();

		// Update is called once per frame
		virtual void Update();

		// Points to the transform of the black texture
		TransformComp* fade;
		// Points to the sprite of the fade
		SpriteComp* sprite;
		// Points to the transform of the camera
		TransformComp* cam;

		// Time it takes the bars to go up or down
		float timeToFade;

		bool done;

		float timer;

		bool initialized;
		bool active;

		bool fadeIn;

		// OnGui() function
		virtual bool OnGui();

		void OnCollisionStarted(const CollisionStarted& collision);

		void FromJson(json & value);
		void ToJson(json & value);
	};
}