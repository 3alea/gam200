#pragma once
#include "src\Engine\GameStates\AEXGameState.h"
// demo game state
class SpineDemo : public AEX::IGameState
{
	virtual void Initialize();
	virtual void LoadResources();
	virtual void Update();
	virtual void Render();
};