#include "SpineDemo.h"
#include "src/Engine/Graphics/SpineTexture.h"
#include "src\Engine\Components\SpineAnimation.h"
#include "src\Engine\Graphics\GfxMgr.h"
#include "src\Engine\Graphics\WindowMgr.h"
#include "src\Engine\Composition\AEXScene.h"
#include "src\Engine\Core\AEXGlobalVariables.h"
#include "src/Demos/ImGuiDemo/ImGuiDemo.h"
#include "Platform/AEXInput.h"

// Added array of spine datas to debug in this spine demo level
SpineData* gSpineData;

// Camera
Camera* editorCamera2;

void SpineDemo::Initialize()
{
	InitializeImGui();
	auto mainWin = WindowMgr->GetCurrentWindow();
	auto winWH = AEVec2(f32(mainWin->GetWidth()), f32(mainWin->GetHeight()));

	GfxMgr->SetViewport(0, 0, s32(winWH.x), s32(winWH.y));

	aexScene->Initialize();
	GameObject* parent = aexScene->FindSpace("Main")->CreateGameObject("Main Object");
	GameObject* hijito = aexScene->FindSpace("Main")->CreateGameObject("hijomio", parent);
	aexScene->FindSpace("Main")->CreateGameObject("hijomio", parent);
	aexScene->FindSpace("Main")->FindObject("Main Object")->AddComp(new SpineAnimationComp());

	// Loading done here because it requires object creation
	aexScene->FindSpace("Main")->FindObject("Main Object")->GetComp<SpineAnimationComp>()->Initialize();

	editorCamera2 = new Camera();
	editorCamera2->Initialize();
}

void SpineDemo::LoadResources()
{
	gSpineData = new SpineData(
		"data/Spine/goblins/export/goblins.atlas",
		"data/Spine/goblins/export/goblins-pro.json");
}

void SpineDemo::Update()
{

	AEVec2& camTrans = editorCamera2->mOffset;

	float cameraSpeed = 0.05f; // adjust accordingly
	if (glfwGetKey(*WindowMgr->GetCurrentWindow(), GLFW_KEY_W) == GLFW_PRESS)
		camTrans -= AEVec2(0.0f, 1.0f) * cameraSpeed;
	if (glfwGetKey(*WindowMgr->GetCurrentWindow(), GLFW_KEY_S) == GLFW_PRESS)
		camTrans += AEVec2(0.0f, 1.0f) * cameraSpeed;
	if (glfwGetKey(*WindowMgr->GetCurrentWindow(), GLFW_KEY_A) == GLFW_PRESS)
		camTrans += AEVec2(1.0f, 0.0f) * cameraSpeed;
	if (glfwGetKey(*WindowMgr->GetCurrentWindow(), GLFW_KEY_D) == GLFW_PRESS)
		camTrans -= AEVec2(1.0f, 0.0f) * cameraSpeed;

	if (editorCamera2->mViewRectangleSize - aexInput->GetMouseWheel() > 0.0f)
		editorCamera2->mViewRectangleSize -= aexInput->GetMouseWheel();

	GfxMgr->Update();
}

void SpineDemo::Render()
{
	GfxMgr->ClearFrameBuffer();
	aexScene->FindSpace("Main")->FindObject("Main Object")->GetComp<SpineAnimationComp>()->DebugRender();
	GfxMgr->RenderGOs();
	//GfxMgr->DrawAllLines(editorCamera2, true);
	GfxMgr->SwapBuffers();
}