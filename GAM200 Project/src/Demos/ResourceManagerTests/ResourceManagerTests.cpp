#include <iostream>
#include "Core/AEXSerialization.h"
#include "Resources/AEXResourceManager.h"
#include "ResourceManagerTests.h"
#include "Graphics/Texture.h"


namespace AEX
{
	/*std::string tst()
	{
		std::string subStr = "Hi!";
		return subStr;
	}*/

	void ResourceManagerTests::Initialize()
	{
		aexResourceMgr->getResource<Texture>("Linux.png");
		aexResourceMgr->ReleaseInstance();

		std::string ext = aexResourceMgr->GetExtension("HelloFile");
		std::string name = aexResourceMgr->GetNameWithExtension(" data\\Textures\\haha/triplehahaha\\Hello.json.hi");

		std::cout << "Name = " << name << std::endl;
		std::cout << "Extension = " << ext << std::endl;

		std::string path = PROJECT_DIR;
		path += "data\\Textures\\Linux.png";
		std::cout << path << std::endl;

		IResource * myResource = aexResourceMgr->getResource<Texture>("data\\Textures\\awesomeface.png");

		json value;
		myResource->ToJson(value);
		std::cout << std::setw(4) << value;

		std::cout << std::endl;
		std::cout << aexResourceMgr->GetNameWithoutNamespaces("AEX::Texture") << std::endl;
	}

	void ResourceManagerTests::Update()
	{

	}
}