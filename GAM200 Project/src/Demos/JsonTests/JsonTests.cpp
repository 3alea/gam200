#include "src/Engine/Composition/AEXScene.h"
#include "../Engine/GameStates/AEXGameState.h"
#include "JsonTests.h"
#include "src/Engine/Core/AEXSerialization.h"
#include <iostream>
#include "src/Engine/Utilities/AEXAssert.h"
#include "Components\AEXTransformComp.h"
#include "Composition\AEXFactory.h"
#include "Core\AEXRtti.h"
#include "Resources\AEXResourceManager.h"


namespace AEX
{
	void JsonTests::Initialize()
	{	// Operators tests
		//json value;
		//u8 myChar = 'S';
		/*s32 myInt = 55;
		f32 myFloat = 87.42f;*/
		/*const char * charPtr = "Hello there!";
		std::string str("How you doinn");

		std::cout << "Before ToJson operator: " << myChar << ", " << charPtr << ", " << str << std::endl;

		value["myChar"] << myChar;
		value["myConstCharPointer"] << charPtr;
		value["myString"] << str;

		SaveToFile(value, "data/Levels/operatortest.json");

		myChar = 'B';
		charPtr = "ByeBye";
		str = "Well then...";

		std::cout << "Vars changed to: " << myChar << ", " << charPtr << ", " << str << std::endl;

		value["myChar"] >> myChar;
		value["myConstCharPointer"] >> charPtr;
		value["myString"] >> str;

		std::cout << "After FromJson operator: " << myChar << ", " << charPtr << ", " << str << std::endl;*/


		TransformComp * myTransformComp = new TransformComp;
		myTransformComp->SetBaseName("TransformBefore");
		myTransformComp->SetPosition(AEVec2(25, 75));
		myTransformComp->SetScale(AEVec2(25, 75));
		myTransformComp->SetRotationAngle(45);

		json value;
		myTransformComp->ToJson(value);

		delete myTransformComp;

		aexFactory->Register<TransformComp>();

		IComp * myComp = dynamic_cast<IComp *>(aexFactory->Create(value["RTTI Type"].get<std::string>().c_str()));

		if (myComp != NULL)
		{
			//std::cout << "Dynamic cast successful" << std::endl;
			myComp->FromJson(value);
			//std::cout << "Second part" << std::endl;
			IComp * myComp2 = new TransformComp;
			myComp2->FromJson(value);
		}

		json value1;
		json value2;
		json value3;

		std::string first = "my friend";
		std::string second = "are you doing";
		std::string third = "man";
		std::string fourth = "up";

		value2["Hello"] = first;
		value2["How"] = second;
		value3["Hey"] = third;
		value3["What's"] = fourth;
		value1.push_back(value2);
		value1.push_back(value3);

		//std::cout << std::setw(4) << value1;

		//std::cout << std::endl;
		//std::cout << std::endl;

		FOR_EACH(jsonIt, value1)
		{
			//std::cout << (*jsonIt)["Hello"] << std::endl;
		}

		//std::cout << aexResourceMgr->GetPathNoExtension("data\\Textures\\Linux.png") << std::endl;

		/*Scene myScene;
		myScene.SetBaseName("MyScene");

		Space * mySpace = new Space;
		mySpace->SetBaseName("MySpace");

		GameObject * myGO = new GameObject;
		myGO->SetBaseName("MyGameObject");
		mySpace->AddObject(myGO);

		//GameObject * myChildGO = new GameObject;
		//myChildGO->SetBaseName("MyChildGameObject");
		//myGO->AddChild(myChildGO);

		GameObject * myGO2 = new GameObject;
		myGO2->SetBaseName("MySecondGameObject");
		mySpace->AddObject(myGO2);
		
		//myScene.mSpaces.push_back(mySpace);*/



		/*json value;
		aexScene->ToJson(value);
		ISerializable::SaveToFile(value, "Test.json");*/


		/*json value;
		ISerializable::LoadFromFile(value, "Test.json");
		aexScene->FromJson(value);*/



		//std::cout << std::setw(4) << value;

		//json value;

		/*ISerializable::LoadFromFile(value, "Levels/Test.json");
		Scene myScene;
		std::cout <<"Before FromJson call: "<< myScene.GetBaseName()<<std::endl;
		myScene.FromJson(value);
		std::cout << "After FromJson call: " << myScene.GetBaseName() << std::endl;*/

		//std::cout << std::setw(4) << value;


		/*std::ofstream outFile("Test.json");

		if (outFile.good() && outFile.is_open())
		{
			std::cout << "hey iou" << std::endl;
			json value;

			json value2;

			value2["Name"] = "SceneName";
			value["Scene"] = value2;


			outFile << std::setw(4) << value;

			std::cout << "how are ya" << std::endl;
		}*/
	}

	void JsonTests::Update()
	{
		
	}


	void UnaiTests::Initialize()
	{
		//Space* spa = new Space;
		//
		//GameObject* HUD = new GameObject;
		//HUD->SetBaseName("hud");
		//spa->AddObject(HUD);
		//
		//GameObject* TIMER = new GameObject;
		//TIMER->SetBaseName("timer");
		//spa->AddObject(TIMER);
		//
		//GameObject* kaka = new GameObject;
		//kaka->SetBaseName("kaka");
		//spa->AddObject(kaka);

	}
	void UnaiTests::Update()
	{
		
	}
}