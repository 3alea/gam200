#ifndef AEX_FACTORY_TESTS
#define AEX_FACTORY_TESTS

#include "../Engine/GameStates/AEXGameState.h"
#include "src\Engine\Composition\AEXGameObject.h"
#include "../Engine/Composition/AEXFactory.h"
#include <iostream>

namespace AEX
{
	class FactoryTests : public IGameState
	{
	public:
		void Initialize()
		{
			// Lets create a game object
			GameObject* goTest1 = aexFactory->Create<GameObject>();
			
			// Oh oh, a game object can't be created 
			// because there is not a creator
			
			// Lets register the class first
			aexFactory->Register<GameObject>();
			GameObject* goTest2 = aexFactory->Create<GameObject>();

			// Get the name of the object
			std::cout << goTest2->GetBaseName() << std::endl;


		}

		void Update()
		{

		}
	};
}

#endif