#pragma once
#include "Core/AEXSerialization.h"
#include "src\Engine\GameStates\AEXGameState.h"
#include "src\Engine\Platform\AEXTime.h"
#include <stack>

using namespace AEX;

// demo game state
class ImGuiDemo : public AEX::IGameState
{
	virtual void Initialize();
	virtual void LoadResources();
	virtual void Update();
	virtual void Render();
};

void InitializeImGui();
void DrawGui();