#include "src\Engine\Composition\AEXObjectManager.h"
#include "src\Engine\GameStates\AEXGameState.h"
#include "src\Engine\Graphics\GfxMgr.h"
#include "src\Engine\Graphics\WindowMgr.h"
#include "src\Engine\Components\AEXComponents.h"
#include "OpenGLDemo.h"
#include "src\Engine\AEX.h"
#include "src\Engine\Core\AEXGlobalVariables.h"

#include <iostream>

void OpenGLDemo::Initialize()
{
	inEditor = false;
	ObjMgr->Initialize();
}

void OpenGLDemo::LoadResources()
{
	
}

void OpenGLDemo::Update()
{
	Camera* globalCamera = dynamic_cast<Camera*>(ObjMgr->FindObject("lol")->GetComp("Camera"));
	AEVec2& camTrans = globalCamera->mOffset;
	Viewport& camVp = globalCamera->mCamViewport;

	float cameraSpeed = 0.05f; // adjust accordingly
	if (glfwGetKey(*WindowMgr->GetCurrentWindow(), GLFW_KEY_W) == GLFW_PRESS)
		camTrans += AEVec2(0.0f, 1.0f) * cameraSpeed;
	if (glfwGetKey(*WindowMgr->GetCurrentWindow(), GLFW_KEY_S) == GLFW_PRESS)
		camTrans -= AEVec2(0.0f, 1.0f) * cameraSpeed;
	if (glfwGetKey(*WindowMgr->GetCurrentWindow(), GLFW_KEY_A) == GLFW_PRESS)
		camTrans -= AEVec2(1.0f, 0.0f) * cameraSpeed;
	if (glfwGetKey(*WindowMgr->GetCurrentWindow(), GLFW_KEY_D) == GLFW_PRESS)
		camTrans += AEVec2(1.0f, 0.0f) * cameraSpeed;

	if (glfwGetKey(*WindowMgr->GetCurrentWindow(), GLFW_KEY_UP) == GLFW_PRESS)
		camVp += cameraSpeed;
	if (glfwGetKey(*WindowMgr->GetCurrentWindow(), GLFW_KEY_DOWN) == GLFW_PRESS)
		camVp -= cameraSpeed;

	ObjMgr->Update();
	GfxMgr->Update();
	WindowMgr->Update();
}

void OpenGLDemo::Render()
{
	GfxMgr->Render();
}

void OpenGLDemo::FreeResources()
{
}
