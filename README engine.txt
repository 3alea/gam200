﻿DOXA


Editor:

* Path component: You can add the component "Path", which will allow you to create a path for the gameobject through the editor.
		  You will have options to change the ease function for the object to traverse the path, and you will also be able
		  to change the time the gameobject takes in each of the segments.
* Path follower: You will also be able to add a component named "Path Follower", which will automatically make the gameobject follow
		 the path (if it has one) when the game starts, with the policy specified in the editor.
Editor Paths:
- If you have added the component path to a gameobject, you will be able to create points in this path, or edit other existing points
  in it, as long as you have the object the path belongs to selected.
- To do any path related action on the editor, you will always need to press the CTRL key at the same time as the rest of the keys/buttons.
- To create a new point in the path: Ctrl + Click on scroll wheel
- To move an existing point: Ctrl + Left Click and drag mouse
- To project an existing point(horizontal or vertical line): Ctrl + Shift + Left Click drag
- To move the path with the object it belongs to: For now, it is only possible by changing the position of the object through ImGui,
  instead of with the gizmos (as long as you have the "With Respect To Owner" checkbox selected).                                          

*Audio: You can test the positional sound by loading the audiotestlevel.json and playing it. You will see the player and a green box which is
the sound emitter.  

*Collision Groups: Colliders can be assigned a collision group. By default, a collider will not have a collision group assigned, this means that 
the collider will detect and resolve collision with any collider. To assign a collision group, you have to select an object in the editor with a collider.
In the object info window, click components, Collider and look for Collision group. If you want to create a new collision group, click file, new, collision group.
To change the collision policy of collision groups you will need to use the collision table.

*Collision table: To open the collision table, look for the Utilities window which should be at the right of the editor. Click Collision table.
The interface is self explanatory. To change the collision policy just click on an element of the table. (Ignore, Detect, Detect and resolve collision) 


        Gizmos: Use 1,2,3 buttons to change between translation, scale, rotation operation.
Select a game object with the right click and operate(translate, scale, rotate) with the left click, clicking in the boxes.

	Also, if you hold the 4 key when one gameobject is selected, snapping will be enabled. A green circle will appear in each of
corners of the texture. If you then left click in one of them and drag the mouse you will be able to snap the object vertex to
the vertex of another object.
        
        Keyboard shortcuts:
* Ctrl+S : Save level
* Ctrl+Alt+S : Save level as
* Ctrl+C : Copy object
* Ctrl+V : Paste object
* F5 : Play level / Go back to editor


(Undo/redo shortcuts are not implemented yet. However, they can be used by clicking in the undo/redo buttons located at the top-left corner of the editor)


Load textures:
All the pngs and jpgs inside the data/Textures folder will appear as an option inside the Sprite component to change the object’s texture to.
        
Load animations:
All the .anim files in the data/Animations folder will appear as an option inside the SpriteAnim component to change the object’s animation to.


Archetypes:
          Every object has an option to save itself to an archetype. First, you need to select the object you want. Then, in the object inspector, at the bottom, there will be a button to save to archetype. Once you click it, it will let you type the name of the archetype and save it. If you do this, the archetype will appear in the bottom-right window. To instantiate it, all you need to do is drag and drop it to the scene. In the archetypes window, you can also select the space you want to add the next archetypes to. Moreover, if you have updated the object, you can just click on save to archetype again, and if you save with the same name, the archetype will be updated.


GameObject/Space/Scene creation:
* GameObject: To create a gameobject, all you need to do is go to file, at the top-left of the editor, go to new, gameobject, and then you can select the space to add the object to.
IMPORTANT: Whenever you create a gameobject, before adding any other component, or saving the go to archetype, you need to add a Transform component.
* Space: You can create a new space the same way as with gameobjects.
* Scene: You can create a new scene the same way. Once you click on new->scene, a popup will appear that will ask you if you want to save the current scene, don’t save it, or cancel this operation all together.


Loaded resources:
         In the editor, you will have a small debug window. In this window, you can activate the option to show another window with all the loaded resources, divided into types, with each of their sizes in bytes. You can also select the option to preview textures.


Components:
* Transform component: The transform component contains information about the position, scale, and rotation of the game object it belongs to. You can change all this data through the editor.
* Sprite/SpriteAnimation components: These are the components that draw the object in the screen. Without one of them, the game object won't be drawn. Furthermore, Sprite renders a texture, while SpriteAnimation allows you to have an animation. You can change the different options of the animation through the editor.
* Camera component: Draws all the game objects which are inside its view rectangle. You can also change its options through the editor.
* Rigidbody component: This allows for a linear physics system. Gameobjects with this component will be affected by gravity, and all its options can be changed through the editor.
* Collider components: You can add a collider component to a gameobject, with the shape of a circle or of a rectangle. As always, all its properties can be modified through the editor.
* Logic components: these are the components used for the logic of the game. This wraps up player mechanics, enemy behaviour, etc.                                     

Prototype:


In order to run the prototype click File, Open Level and click “FinalLevel temp.json” 

+KeyBoard:

* Move with the left-right arrows
* Jump with the up arrow
* Crouch with the down arrow
* Jump with the space key
* Dash with the w key
* Shoot with the q key

+Controller:

 - Move           : Left Joystick
 - Jump           : A button
 - Dash           : Y button
 - Shoot          : Left-Back button OR X
 - Super Shot     : B button
 - Aim (not move) : Right-Back button
 - Pause          : Start (Right settings) button