﻿DOXA

HACKS:
o Press Control + Alt + number to traverse through levels. Does not work in main menu. Levels:
0) Main menu
1) Tutorial
2) Veins
3) Station
4) Cars level 1
5) Stomach Part 1
6) Stomach Part 2
7 - 9) Levels in development (lungs, boiler room and final boss)
o Press Control + Alt + I  to become invencible


Week 13:
- Added new sfx to car level.              
- Added a new car level(car level 2), similar to the first one but with new hazards. 
- Implemented a warning system for the new hazards on the car level, that warns the player using different sfx. 
- Sfx added to the menu.
- All menus finished (need to solve fullscreen crash though)
- Minor bugs involving the camera limits solved.
- Horizontal punches have warnings
- New final boss gameflow
- New targetplayer state
-Balance player and enemy sfx not to be annoying
- New sfx for enemies
- Reposition checkpoints and enemies not to respawn near them
- Finished designing the Lungs level, now starting to implement assets
- Platforms that can be accessed below are implemented (see lungs' trees)
- Implement new icon for the game
- Added clipping to Spine animations
- Made a new grounded enemy that follows the player if they get away
- Fixed dash spine Spine animation on the ground


Week 12:
 - Downloaded, edited and added new sfx to the car level. Also, added more camera shakes and speeding cars
    at the end of the level. Speeding cars are only "decorative" in the first car level,
    however they are capable of killing the player and will be used as a hazard in the next
    car level.
 - Minor problems involving the camera and shakes have been solved in the car level. Please, notify if you
    find something similar in other levels.
 - Started mounting the boiler room and lungs (you can see them in Levels in development).
 - Reworked Spawner (you can see the rework better in the second Stomach level)
 - Alveolus, y snot rework (air rising wip) (on Lungs)
 - Particles are now culled to improve performance in certain situations
 - Moving platforms now activate when the player steps on them
 - Some options implemented in the main menu
 - Progress with pause menu
 - Player can now duck to avoid grounded enemies' shots (only in Tutorials and Veins for now)
 - More enemies and with more lives in the station